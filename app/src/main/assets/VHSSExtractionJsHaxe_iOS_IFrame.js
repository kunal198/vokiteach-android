(function (console, $hx_exports, $global) { "use strict";
$hx_exports.com = $hx_exports.com || {};
$hx_exports.com.oddcast = $hx_exports.com.oddcast || {};
$hx_exports.com.oddcast.app = $hx_exports.com.oddcast.app || {};
$hx_exports.com.oddcast.app.vhss_extraction = $hx_exports.com.oddcast.app.vhss_extraction || {};
$hx_exports.com.oddcast.app.vhss_extraction.mobile_IFrame = $hx_exports.com.oddcast.app.vhss_extraction.mobile_IFrame || {};
var $hxClasses = {},$estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;yet
}
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
$hxClasses["EReg"] = EReg;
EReg.__name__ = ["EReg"];
EReg.prototype = {
	match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,matched: function(n) {
		if(this.r.m != null && n >= 0 && n < this.r.m.length) return this.r.m[n]; else throw new js__$Boot_HaxeError("EReg::matched");
	}
	,replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,__class__: EReg
};
var HxOverrides = function() { };
$hxClasses["HxOverrides"] = HxOverrides;
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.dateStr = function(date) {
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mi = date.getMinutes();
	var s = date.getSeconds();
	return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d < 10?"0" + d:"" + d) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
};
HxOverrides.strDate = function(s) {
	var _g = s.length;
	switch(_g) {
	case 8:
		var k = s.split(":");
		var d = new Date();
		d.setTime(0);
		d.setUTCHours(k[0]);
		d.setUTCMinutes(k[1]);
		d.setUTCSeconds(k[2]);
		return d;
	case 10:
		var k1 = s.split("-");
		return new Date(k1[0],k1[1] - 1,k1[2],0,0,0);
	case 19:
		var k2 = s.split(" ");
		var y = k2[0].split("-");
		var t = k2[1].split(":");
		return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
	default:
		throw new js__$Boot_HaxeError("Invalid date format : " + s);
	}
};
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
HxOverrides.indexOf = function(a,obj,i) {
	var len = a.length;
	if(i < 0) {
		i += len;
		if(i < 0) i = 0;
	}
	while(i < len) {
		if(a[i] === obj) return i;
		i++;
	}
	return -1;
};
HxOverrides.remove = function(a,obj) {
	var i = HxOverrides.indexOf(a,obj,0);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
};
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
var Lambda = function() { };
$hxClasses["Lambda"] = Lambda;
Lambda.__name__ = ["Lambda"];
Lambda.exists = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		if(f(x)) return true;
	}
	return false;
};
Lambda.count = function(it,pred) {
	var n = 0;
	if(pred == null) {
		var $it0 = $iterator(it)();
		while( $it0.hasNext() ) {
			var _ = $it0.next();
			n++;
		}
	} else {
		var $it1 = $iterator(it)();
		while( $it1.hasNext() ) {
			var x = $it1.next();
			if(pred(x)) n++;
		}
	}
	return n;
};
Lambda.empty = function(it) {
	return !$iterator(it)().hasNext();
};
var List = function() {
	this.length = 0;
};
$hxClasses["List"] = List;
List.__name__ = ["List"];
List.prototype = {
	add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,push: function(item) {
		var x = [item,this.h];
		this.h = x;
		if(this.q == null) this.q = x;
		this.length++;
	}
	,first: function() {
		if(this.h == null) return null; else return this.h[0];
	}
	,pop: function() {
		if(this.h == null) return null;
		var x = this.h[0];
		this.h = this.h[1];
		if(this.h == null) this.q = null;
		this.length--;
		return x;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,remove: function(v) {
		var prev = null;
		var l = this.h;
		while(l != null) {
			if(l[0] == v) {
				if(prev == null) this.h = l[1]; else prev[1] = l[1];
				if(this.q == l) this.q = prev;
				this.length--;
				return true;
			}
			prev = l;
			l = l[1];
		}
		return false;
	}
	,iterator: function() {
		return new _$List_ListIterator(this.h);
	}
	,__class__: List
};
var _$List_ListIterator = function(head) {
	this.head = head;
	this.val = null;
};
$hxClasses["_List.ListIterator"] = _$List_ListIterator;
_$List_ListIterator.__name__ = ["_List","ListIterator"];
_$List_ListIterator.prototype = {
	hasNext: function() {
		return this.head != null;
	}
	,next: function() {
		this.val = this.head[0];
		this.head = this.head[1];
		return this.val;
	}
	,__class__: _$List_ListIterator
};
Math.__name__ = ["Math"];
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = ["Reflect"];
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		haxe_CallStack.lastException = e;
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		return null;
	}
};
Reflect.setField = function(o,field,value) {
	o[field] = value;
};
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
};
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(f != "__id__" && f != "hx__closures__" && hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
};
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
};
Reflect.compare = function(a,b) {
	if(a == b) return 0; else if(a > b) return 1; else return -1;
};
Reflect.isEnumValue = function(v) {
	return v != null && v.__enum__ != null;
};
Reflect.deleteField = function(o,field) {
	if(!Object.prototype.hasOwnProperty.call(o,field)) return false;
	delete(o[field]);
	return true;
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = ["Std"];
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std["int"] = function(x) {
	return x | 0;
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
Std.parseFloat = function(x) {
	return parseFloat(x);
};
var StringBuf = function() {
	this.b = "";
};
$hxClasses["StringBuf"] = StringBuf;
StringBuf.__name__ = ["StringBuf"];
StringBuf.prototype = {
	add: function(x) {
		this.b += Std.string(x);
	}
	,addChar: function(c) {
		this.b += String.fromCharCode(c);
	}
	,addSub: function(s,pos,len) {
		if(len == null) this.b += HxOverrides.substr(s,pos,null); else this.b += HxOverrides.substr(s,pos,len);
	}
	,__class__: StringBuf
};
var StringTools = function() { };
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = ["StringTools"];
StringTools.htmlEscape = function(s,quotes) {
	s = s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
	if(quotes) return s.split("\"").join("&quot;").split("'").join("&#039;"); else return s;
};
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && HxOverrides.substr(s,0,start.length) == start;
};
StringTools.endsWith = function(s,end) {
	var elen = end.length;
	var slen = s.length;
	return slen >= elen && HxOverrides.substr(s,slen - elen,elen) == end;
};
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c > 8 && c < 14 || c == 32;
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
};
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) r++;
	if(r > 0) return HxOverrides.substr(s,0,l - r); else return s;
};
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
};
StringTools.lpad = function(s,c,l) {
	if(c.length <= 0) return s;
	while(s.length < l) s = c + s;
	return s;
};
StringTools.rpad = function(s,c,l) {
	if(c.length <= 0) return s;
	while(s.length < l) s = s + c;
	return s;
};
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
};
StringTools.hex = function(n,digits) {
	var s = "";
	var hexChars = "0123456789ABCDEF";
	do {
		s = hexChars.charAt(n & 15) + s;
		n >>>= 4;
	} while(n > 0);
	if(digits != null) while(s.length < digits) s = "0" + s;
	return s;
};
StringTools.fastCodeAt = function(s,index) {
	return s.charCodeAt(index);
};
var ValueType = $hxClasses["ValueType"] = { __ename__ : ["ValueType"], __constructs__ : ["TNull","TInt","TFloat","TBool","TObject","TFunction","TClass","TEnum","TUnknown"] };
ValueType.TNull = ["TNull",0];
ValueType.TNull.toString = $estr;
ValueType.TNull.__enum__ = ValueType;
ValueType.TInt = ["TInt",1];
ValueType.TInt.toString = $estr;
ValueType.TInt.__enum__ = ValueType;
ValueType.TFloat = ["TFloat",2];
ValueType.TFloat.toString = $estr;
ValueType.TFloat.__enum__ = ValueType;
ValueType.TBool = ["TBool",3];
ValueType.TBool.toString = $estr;
ValueType.TBool.__enum__ = ValueType;
ValueType.TObject = ["TObject",4];
ValueType.TObject.toString = $estr;
ValueType.TObject.__enum__ = ValueType;
ValueType.TFunction = ["TFunction",5];
ValueType.TFunction.toString = $estr;
ValueType.TFunction.__enum__ = ValueType;
ValueType.TClass = function(c) { var $x = ["TClass",6,c]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TEnum = function(e) { var $x = ["TEnum",7,e]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TUnknown = ["TUnknown",8];
ValueType.TUnknown.toString = $estr;
ValueType.TUnknown.__enum__ = ValueType;
ValueType.__empty_constructs__ = [ValueType.TNull,ValueType.TInt,ValueType.TFloat,ValueType.TBool,ValueType.TObject,ValueType.TFunction,ValueType.TUnknown];
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = ["Type"];
Type.getClass = function(o) {
	if(o == null) return null; else return js_Boot.getClass(o);
};
Type.getClassName = function(c) {
	var a = c.__name__;
	if(a == null) return null;
	return a.join(".");
};
Type.getEnumName = function(e) {
	var a = e.__ename__;
	return a.join(".");
};
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.resolveEnum = function(name) {
	var e = $hxClasses[name];
	if(e == null || !e.__ename__) return null;
	return e;
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw new js__$Boot_HaxeError("Too many arguments");
	}
	return null;
};
Type.createEmptyInstance = function(cl) {
	function empty() {}; empty.prototype = cl.prototype;
	return new empty();
};
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw new js__$Boot_HaxeError("No such constructor " + constr);
	if(Reflect.isFunction(f)) {
		if(params == null) throw new js__$Boot_HaxeError("Constructor " + constr + " need parameters");
		return Reflect.callMethod(e,f,params);
	}
	if(params != null && params.length != 0) throw new js__$Boot_HaxeError("Constructor " + constr + " does not need parameters");
	return f;
};
Type.getEnumConstructs = function(e) {
	var a = e.__constructs__;
	return a.slice();
};
Type["typeof"] = function(v) {
	var _g = typeof(v);
	switch(_g) {
	case "boolean":
		return ValueType.TBool;
	case "string":
		return ValueType.TClass(String);
	case "number":
		if(Math.ceil(v) == v % 2147483648.0) return ValueType.TInt;
		return ValueType.TFloat;
	case "object":
		if(v == null) return ValueType.TNull;
		var e = v.__enum__;
		if(e != null) return ValueType.TEnum(e);
		var c = js_Boot.getClass(v);
		if(c != null) return ValueType.TClass(c);
		return ValueType.TObject;
	case "function":
		if(v.__name__ || v.__ename__) return ValueType.TObject;
		return ValueType.TFunction;
	case "undefined":
		return ValueType.TNull;
	default:
		return ValueType.TUnknown;
	}
};
Type.allEnums = function(e) {
	return e.__empty_constructs__;
};
var Xml = function(nodeType) {
	this.nodeType = nodeType;
	this.children = [];
	this.attributeMap = new haxe_ds_StringMap();
};
$hxClasses["Xml"] = Xml;
Xml.__name__ = ["Xml"];
Xml.parse = function(str) {
	return haxe_xml_Parser.parse(str);
};
Xml.createElement = function(name) {
	var xml = new Xml(Xml.Element);
	if(xml.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + xml.nodeType);
	xml.nodeName = name;
	return xml;
};
Xml.createPCData = function(data) {
	var xml = new Xml(Xml.PCData);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createCData = function(data) {
	var xml = new Xml(Xml.CData);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createComment = function(data) {
	var xml = new Xml(Xml.Comment);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createDocType = function(data) {
	var xml = new Xml(Xml.DocType);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createProcessingInstruction = function(data) {
	var xml = new Xml(Xml.ProcessingInstruction);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createDocument = function() {
	return new Xml(Xml.Document);
};
Xml.prototype = {
	get_nodeName: function() {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.nodeName;
	}
	,get: function(att) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.get(att);
	}
	,set: function(att,value) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		this.attributeMap.set(att,value);
	}
	,exists: function(att) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.exists(att);
	}
	,attributes: function() {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.keys();
	}
	,iterator: function() {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		return HxOverrides.iter(this.children);
	}
	,elements: function() {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var ret;
		var _g = [];
		var _g1 = 0;
		var _g2 = this.children;
		while(_g1 < _g2.length) {
			var child = _g2[_g1];
			++_g1;
			if(child.nodeType == Xml.Element) _g.push(child);
		}
		ret = _g;
		return HxOverrides.iter(ret);
	}
	,elementsNamed: function(name) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var ret;
		var _g = [];
		var _g1 = 0;
		var _g2 = this.children;
		while(_g1 < _g2.length) {
			var child = _g2[_g1];
			++_g1;
			if(child.nodeType == Xml.Element && (function($this) {
				var $r;
				if(child.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + child.nodeType);
				$r = child.nodeName;
				return $r;
			}(this)) == name) _g.push(child);
		}
		ret = _g;
		return HxOverrides.iter(ret);
	}
	,firstElement: function() {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(child.nodeType == Xml.Element) return child;
		}
		return null;
	}
	,addChild: function(x) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		if(x.parent != null) x.parent.removeChild(x);
		this.children.push(x);
		x.parent = this;
	}
	,removeChild: function(x) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		if(HxOverrides.remove(this.children,x)) {
			x.parent = null;
			return true;
		}
		return false;
	}
	,__class__: Xml
};
var com_oddcast_app_FrameUpdate = function() {
	this.lastStartTime = Infinity;
	this.actionScriptDuration = this.imageDisplayedIntervalMilliSecs = this.sinceTalkingMillis = 0.0;
	this.fakeTimeMillis = 0.0;
	this.bClearCaches = true;
	this.deltamouseX = this.deltamouseY = this.mouseX = this.mouseY = 0.0;
	this.resetMouseMoveTime();
	this.iMouseHasStartedState = 5;
};
$hxClasses["com.oddcast.app.FrameUpdate"] = com_oddcast_app_FrameUpdate;
com_oddcast_app_FrameUpdate.__name__ = ["com","oddcast","app","FrameUpdate"];
com_oddcast_app_FrameUpdate.prototype = {
	shallUpdate: function(time,fps) {
		var diff = time - this.lastStartTime;
		if(diff < 0 || fps <= 0.0) return true;
		return diff > 1000 / fps;
	}
	,setMouse: function(mouseX,mouseY) {
		var _g = this.iMouseHasStartedState;
		switch(_g) {
		case 5:
			this.iMouseHasStartedState -= 1;
			this.lastMouseX = mouseX;
			this.lastMouseY = mouseY;
			break;
		case 0:
			this.deltamouseX = this.mouseX - mouseX;
			this.mouseX = mouseX;
			this.deltamouseY = this.mouseY - mouseY;
			this.mouseY = mouseY;
			if(this.mouseHasMoved()) this.resetMouseMoveTime();
			break;
		default:
			if(this.lastMouseX != mouseX || this.lastMouseY != mouseY) {
				this.iMouseHasStartedState -= 1;
				this.lastMouseX = mouseX;
				this.lastMouseY = mouseY;
			}
		}
	}
	,calcAdjustTimeMillis: function() {
		return this.actionScriptDuration + this.imageDisplayedIntervalMilliSecs * 0.5 + this.fakeTimeMillis;
	}
	,calcAverageAdvanceToScreen: function(as3Time) {
		return this.imageDisplayedIntervalMilliSecs * 0.0 - as3Time + this.fakeTimeMillis;
	}
	,mouseHasMoved: function() {
		return this.deltamouseX * this.deltamouseX + this.deltamouseY + this.deltamouseY > 1.0;
	}
	,beforeRender: function(time) {
		this.imageDisplayedIntervalMilliSecs = Math.max(0,time - this.lastStartTime);
		this.lastStartTime = time;
		this.timeSinceMouseMove += this.imageDisplayedIntervalMilliSecs;
	}
	,afterRender: function(time) {
		this.actionScriptDuration = time;
	}
	,fakeInterval: function(setMillis) {
		this.imageDisplayedIntervalMilliSecs = setMillis;
	}
	,getInterval: function() {
		return this.imageDisplayedIntervalMilliSecs;
	}
	,resetMouseMoveTime: function() {
		this.timeSinceMouseMove = 0;
	}
	,ArrangeHostAsIfAtTimeMillis: function(millis) {
		this.fakeTimeMillis = millis;
	}
	,isRealAudio: function() {
		return this.fakeTimeMillis == 0;
	}
	,setClearCache: function(v) {
		this.bClearCaches = v;
	}
	,__class__: com_oddcast_app_FrameUpdate
};
var com_oddcast_app_FrameUpdateTools = function() { };
$hxClasses["com.oddcast.app.FrameUpdateTools"] = com_oddcast_app_FrameUpdateTools;
com_oddcast_app_FrameUpdateTools.__name__ = ["com","oddcast","app","FrameUpdateTools"];
com_oddcast_app_FrameUpdateTools.getFakeTimeMillis = function(frameUpdate) {
	return frameUpdate.fakeTimeMillis;
};
var com_oddcast_app_flash_TimerAccelerated = function(acc) {
	if(acc == null) acc = 1.0;
	this.lastReportedTime = 0.0;
	this.setAcceleration(acc);
};
$hxClasses["com.oddcast.app.flash.TimerAccelerated"] = com_oddcast_app_flash_TimerAccelerated;
com_oddcast_app_flash_TimerAccelerated.__name__ = ["com","oddcast","app","flash","TimerAccelerated"];
com_oddcast_app_flash_TimerAccelerated.prototype = {
	setAcceleration: function(acc) {
		this.acc = acc;
	}
	,getTime: function() {
		var actualTime = com_oddcast_util_UtilsLite.getTime();
		if(isNaN(this.lastActualTime)) this.lastActualTime = actualTime;
		var delta = (actualTime - this.lastActualTime) * this.acc;
		this.lastReportedTime += delta;
		this.lastActualTime = actualTime;
		return this.lastReportedTime;
	}
	,__class__: com_oddcast_app_flash_TimerAccelerated
};
var com_oddcast_util_IDisposable = function() { };
$hxClasses["com.oddcast.util.IDisposable"] = com_oddcast_util_IDisposable;
com_oddcast_util_IDisposable.__name__ = ["com","oddcast","util","IDisposable"];
com_oddcast_util_IDisposable.prototype = {
	__class__: com_oddcast_util_IDisposable
};
var com_oddcast_host_morph_ISpeechManager = function() { };
$hxClasses["com.oddcast.host.morph.ISpeechManager"] = com_oddcast_host_morph_ISpeechManager;
com_oddcast_host_morph_ISpeechManager.__name__ = ["com","oddcast","host","morph","ISpeechManager"];
com_oddcast_host_morph_ISpeechManager.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_host_morph_ISpeechManager.prototype = {
	__class__: com_oddcast_host_morph_ISpeechManager
};
var com_oddcast_app_js_sitepalFullbodyLite_AudioManager = function() {
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "AudioManager.hx", lineNumber : 25, className : "com.oddcast.app.js.sitepalFullbodyLite.AudioManager", methodName : "new"});
	this.hostVolume = 1.0;
	this.debugBusy = false;
};
$hxClasses["com.oddcast.app.js.sitepalFullbodyLite.AudioManager"] = com_oddcast_app_js_sitepalFullbodyLite_AudioManager;
com_oddcast_app_js_sitepalFullbodyLite_AudioManager.__name__ = ["com","oddcast","app","js","sitepalFullbodyLite","AudioManager"];
com_oddcast_app_js_sitepalFullbodyLite_AudioManager.__interfaces__ = [com_oddcast_host_morph_ISpeechManager,com_oddcast_util_IDisposable];
com_oddcast_app_js_sitepalFullbodyLite_AudioManager.prototype = {
	sayUrl3: function(url,offsetSeconds,autoStart,id3String,playingAudio,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback) {
		var _g = this;
		this.debugBusy = true;
		this.audioStartedCallback = audioStartedCallback;
		this.audioFinishedCallback = audioFinishedCallback;
		if(this.sayUrl2(url,offsetSeconds,autoStart,id3String,playingAudio,function(URL,errorCode) {
			_g.stopPlayingAudio();
			if(fileErrorCallback != null) fileErrorCallback(URL,errorCode);
		},audioLoadedCallback) == false) this.stopPlayingAudio();
	}
	,sayUrl2: function(url,offsetSeconds,autoStart,id3String,playingAudio,fileErrorCallback,audioLoadedCallback) {
		var _g = this;
		var vAudio = new com_oddcast_util_js_JSAudio();
		if(url == null) return false;
		vAudio.playAudio(url,offsetSeconds * 1000.0,this.hostVolume,autoStart,id3String,fileErrorCallback,function(fullyQualifiedUrl) {
			if(audioLoadedCallback != null) audioLoadedCallback(url);
			_g.lastURL = new String(url);
		},function(id3Str,fullyQualifiedUrl1,timeSeconds) {
			_g.lastID3 = new String(id3String);
			if(id3Str == null) {
				if(fileErrorCallback != null) {
					fileErrorCallback(url,"no id3 data");
					haxe_Log.trace("NO id3 data",{ fileName : "AudioManager.hx", lineNumber : 153, className : "com.oddcast.app.js.sitepalFullbodyLite.AudioManager", methodName : "sayUrl2"});
				}
			}
			_g.currPlayingAudio = _g.instantiatePlayingVAudio(vAudio);
			_g.currPlayingAudio.setVolume(_g.hostVolume);
			var id3Duration = _g.currPlayingAudio.id3(id3Str);
			if(playingAudio != null) playingAudio(_g.currPlayingAudio);
			if(_g.audioStartedCallback != null) _g.audioStartedCallback(url,timeSeconds);
			return id3Duration;
		},function(fullyQualifiedUrl2,timeSeconds1) {
			_g.stopPlayingAudioWithCallback(url,timeSeconds1,true);
		});
		return true;
	}
	,stopPlayingAudioWithCallback: function(fullyQualifiedUrl,timeSeconds,bEffectCallback) {
		this.debugBusy = false;
		var tempCallback;
		if(bEffectCallback) tempCallback = this.audioFinishedCallback; else tempCallback = null;
		var retval = this.stopPlayingAudio();
		if(tempCallback != null) {
			haxe_Log.trace("onAudioEnded: " + fullyQualifiedUrl + " t:" + timeSeconds,{ fileName : "AudioManager.hx", lineNumber : 185, className : "com.oddcast.app.js.sitepalFullbodyLite.AudioManager", methodName : "stopPlayingAudioWithCallback"});
			tempCallback(fullyQualifiedUrl,timeSeconds);
		}
		return retval;
	}
	,stopPlayingAudio: function() {
		var retval = this.isPlaying();
		this.currPlayingAudio = com_oddcast_util_Disposable.disposeIfValid(this.currPlayingAudio);
		return retval;
	}
	,pauseAudio: function() {
		if(this.noAudio()) return false;
		this.currPlayingAudio.pauseResume(true);
		return true;
	}
	,resumeAudio: function() {
		if(this.noAudio()) return false;
		this.currPlayingAudio.pauseResume(false);
		return true;
	}
	,setToTime: function(millis) {
		if(this.noAudio()) return;
		this.currPlayingAudio.setToTime(millis);
	}
	,replayAudio: function(offsetSeconds) {
		if(this.lastURL != null) {
			this.sayUrl3(this.lastURL,offsetSeconds,true,this.lastID3,null,null,null,this.audioStartedCallback,this.audioFinishedCallback);
			return this.currPlayingAudio;
		}
		return null;
	}
	,setHostVolume: function(v) {
		this.hostVolume = v / 10.0;
		if(this.currPlayingAudio != null) this.currPlayingAudio.setVolume(this.hostVolume);
	}
	,isSayingSilent: function() {
		if(this.currPlayingAudio != null) return this.currPlayingAudio.isSayingSilent();
		return false;
	}
	,noAudio: function() {
		return this.currPlayingAudio == null;
	}
	,isPlaying: function() {
		return this.currPlayingAudio != null && this.currPlayingAudio.isPlaying();
	}
	,getCurrentAudioProgress: function() {
		if(this.noAudio()) return -1.0;
		return this.currPlayingAudio.playingProgress();
	}
	,saySilent2: function(playForSecs,playingAudio,audioStartedCallback,audioFinishedCallback) {
		var _g = this;
		this.currPlayingAudio = this.instantiatePlayingVAudioSaySilent(16.849,function(url,time) {
			_g.stopPlayingAudioWithCallback(url,time,true);
		},playForSecs,playingAudio);
		this.currPlayingAudio.id3("audio_duration = \"16.849\";\ndate = \"20100325_14:06:37.240\";\nhost = \"ODDAPS002\";\nkbps = \"48\";\nkhz = \"22050\";\nlip_string = \"f0=0&f1=0&f2=6&f3=5&f4=8&f5=8&f6=8&f7=1&f8=1&f9=12&f10=2&f11=5&f12=5&f13=8&f14=8&f15=5&f16=8&f17=8&f18=3&f19=3&f20=3&f21=5&f22=5&f23=7&f24=3&f25=13&f26=13&f27=3&f28=9&f29=9&f30=6&f31=10&f32=13&f33=1&f34=1&f35=15&f36=5&f37=5&f38=5&f39=0&f40=10&f41=10&f42=10&f43=13&f44=1&f45=3&f46=3&f47=5&f48=15&f49=14&f50=14&f51=6&f52=13&f53=13&f54=5&f55=5&f56=0&f57=0&f58=0&f59=0&f60=1&f61=1&f62=1&f63=15&f64=1&f65=14&f66=14&f67=3&f68=10&f69=12&f70=3&f71=3&f72=13&f73=13&f74=10&f75=1&f76=10&f77=5&f78=2&f79=2&f80=2&f81=12&f82=12&f83=3&f84=3&f85=5&f86=5&f87=15&f88=9&f89=5&f90=3&f91=9&f92=9&f93=9&f94=5&f95=3&f96=3&f97=5&f98=8&f99=8&f100=12&f101=9&f102=5&f103=5&f104=5&f105=5&f106=5&f107=6&f108=15&f109=7&f110=5&f111=7&f112=9&f113=9&f114=3&f115=1&f116=1&f117=1&f118=3&f119=4&f120=5&f121=1&f122=7&f123=9&f124=9&f125=9&f126=15&f127=7&f128=6&f129=10&f130=1&f131=11&f132=11&f133=7&f134=5&f135=5&f136=5&f137=3&f138=14&f139=14&f140=3&f141=3&f142=10&f143=10&f144=5&f145=7&f146=7&f147=7&f148=10&f149=1&f150=1&f151=1&f152=5&f153=5&f154=13&f155=13&f156=1&f157=3&f158=3&f159=12&f160=14&f161=14&f162=12&f163=3&f164=3&f165=8&f166=8&f167=8&f168=1&f169=1&f170=15&f171=5&f172=5&f173=5&f174=5&f175=5&f176=5&f177=0&f178=3&f179=3&f180=5&f181=6&f182=9&f183=9&f184=9&f185=0&f186=0&f187=13&f188=13&f189=0&f190=0&f191=0&f192=14&f193=13&f194=13&f195=3&f196=3&f197=5&f198=5&f199=0&f200=0&f201=0&f202=0&nofudge=1&lipversion=2&ok=1\";\ntimed_phonemes = \"P,0,36,3,x\tP,36,136,3,x\tP,136,226,65,t\tP,226,276,92,H\tP,276,536,91,u\tP,536,676,88,O\tP,676,736,83,l\tP,736,846,85,v\tP,846,1026,91,a\tP,1026,1186,79,W\tP,1186,1326,70,H\tP,1326,1476,76,W\tP,1476,1526,61,v\tP,1526,1666,76,m\tP,1666,1856,96,I\tP,1856,1906,85,N\tP,1906,2016,82,m\tP,2016,2186,87,E\tP,2186,2286,77,b\tP,2286,2326,84,E\tP,2326,2416,82,z\tP,2416,2466,84,t\tP,2466,2636,80,S\tP,2636,2696,84,y\tP,2696,2886,70,w\tP,2886,2956,58,e\tP,2956,3046,57,H\tP,3046,3176,70,a\tP,3176,3306,62,x\tP,3306,3506,77,S\tP,3506,3576,82,y\tP,3576,3716,78,w\tP,3716,3856,81,m\tP,3856,3996,91,a\tP,3996,4036,85,n\tP,4036,4096,86,i\tP,4096,4186,71,G\tP,4186,4246,88,t\tP,4246,4476,73,E\tP,4476,4636,61,H\tP,4636,4966,65,x\tP,4966,5186,73,w\tP,5186,5246,96,e\tP,5246,5406,87,O\tP,5406,5506,80,G\tP,5506,5596,75,m\tP,5596,5636,67,b\tP,5636,5706,83,J\tP,5706,5746,82,!\tP,5746,5806,79,D\tP,5806,5846,82,!\tP,5846,5986,64,b\tP,5986,6126,85,E\tP,6126,6166,68,d\tP,6166,6236,86,J\tP,6236,6306,89,U\tP,6306,6396,89,Z\tP,6396,6456,83,!\tP,6456,6676,84,F\tP,6676,6856,83,l\tP,6856,6906,51,n\tP,6906,7006,78,m\tP,7006,7166,93,I\tP,7166,7286,91,e\tP,7286,7376,85,z\tP,7376,7456,90,^\tP,7456,7526,71,b\tP,7526,7766,86,s\tP,7766,7806,91,R\tP,7806,7886,91,a\tP,7886,7926,80,n\tP,7926,7966,86,e\tP,7966,8066,67,b\tP,8066,8146,88,a\tP,8146,8286,80,W\tP,8286,8376,62,l\tP,8376,8436,53,z\tP,8436,8856,69,a\tP,8856,8936,65,t\tP,8936,8986,91,e\tP,8986,9046,82,n\tP,9046,9116,88,^\tP,9116,9216,79,n\tP,9216,9296,87,z\tP,9296,9386,89,s\tP,9386,9466,69,p\tP,9466,9516,86,w\tP,9516,9686,92,o\tP,9686,9786,88,O\tP,9786,9836,79,b\tP,9836,9906,94,c\tP,9906,10036,96,I\tP,10036,10156,86,U\tP,10156,10236,75,n\tP,10236,10276,79,d\tP,10276,10446,88,s\tP,10446,10536,94,e\tP,10536,10596,77,n\tP,10596,10646,80,t\tP,10646,10746,91,S\tP,10746,10846,84,w\tP,10846,11006,91,r\tP,11006,11046,92,A\tP,11046,11116,76,n\tP,11116,11296,80,H\tP,11296,11356,71,a\tP,11356,11446,78,m\tP,11446,11486,90,!\tP,11486,11616,76,k\tP,11616,11656,84,E\tP,11656,11796,63,b\tP,11796,11966,87,J\tP,11966,12036,89,A\tP,12036,12256,80,N\tP,12256,12326,80,S\tP,12326,12366,71,t\tP,12366,12406,79,D\tP,12406,12626,72,w\tP,12626,12786,87,A\tP,12786,12956,77,y\tP,12956,13046,75,w\tP,13046,13096,74,b\tP,13096,13216,80,m\tP,13216,13286,78,l\tP,13286,13356,80,G\tP,13356,13396,81,!\tP,13396,13496,69,G\tP,13496,13556,76,l\tP,13556,13746,62,b\tP,13746,13916,92,u\tP,13916,14166,77,O\tP,14166,14216,65,d\tP,14216,14276,70,e\tP,14276,14306,56,v\tP,14306,14686,73,a\tP,14686,14806,62,x\tP,14806,14856,66,D\tP,14856,14956,80,m\tP,14956,15056,92,a\tP,15056,15136,68,d\tP,15136,15306,86,s\tP,15306,15406,84,z\tP,15406,15556,61,x\tP,15556,15596,71,G\tP,15596,15726,73,E\tP,15726,15916,50,x\tP,15916,15996,78,k\tP,15996,16176,84,E\tP,16176,16356,65,m\tP,16356,16396,68,^\tP,16396,16506,71,a\tP,16506,16596,60,x\tP,16596,16656,28,x\tP,16656,16796,3,x\tP,16796,16826,3,x\";\n\n\n");
		this.currPlayingAudio.crop(playForSecs);
		playingAudio(this.currPlayingAudio);
		if(audioStartedCallback != null) audioStartedCallback("saySilent",0.0);
	}
	,getPrincipalAudioMP3Sync: function() {
		this.notYetImplemented();
		return null;
	}
	,updateSpeechManager: function(frameUpdate) {
	}
	,pause: function(p) {
		if(p) this.pauseAudio(); else this.resumeAudio();
	}
	,isPaused: function() {
		return !this.noAudio() && !this.isPlaying();
	}
	,isPlayingAudio: function() {
		this.notYetImplemented();
		return true;
	}
	,saySilent: function(durationMillis) {
		this.notYetImplemented();
	}
	,playingAudioTime: function() {
		if(!this.noAudio()) return this.currPlayingAudio.position();
		return 0.0;
	}
	,sayUrl: function(url,offset,autoStart) {
		if(autoStart == null) autoStart = true;
		if(offset == null) offset = 0.0;
		this.notYetImplemented();
	}
	,loadingProgress: function(bIsPlaying) {
		this.notYetImplemented();
	}
	,mp3errorloading: function(mp3Sync) {
		this.notYetImplemented();
	}
	,getVisemeAdvance: function() {
		this.notYetImplemented();
		return 0.0;
	}
	,notYetImplemented: function() {
		throw new js__$Boot_HaxeError("Not Yet Implemented");
	}
	,dispose: function() {
		this.stopPlayingAudio();
		this.audioStartedCallback = null;
		this.audioFinishedCallback = null;
	}
	,instantiatePlayingVAudio: function(vAudio) {
		throw new js__$Boot_HaxeError("abstract");
		return null;
	}
	,instantiatePlayingVAudioSaySilent: function(visemeDurationSecs,audioFinishedCallback,playForSecs,playingAudio) {
		throw new js__$Boot_HaxeError("abstract");
		return null;
	}
	,isAudioLoaded: function() {
		if(!this.noAudio()) {
			if(!this.isSayingSilent()) return true;
		}
		return false;
	}
	,getAudioPositionSeconds: function() {
		if(this.isAudioLoaded()) return -1.0;
		return this.playingAudioTime() / 1000.0;
	}
	,__class__: com_oddcast_app_js_sitepalFullbodyLite_AudioManager
};
var com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes = function() {
	com_oddcast_app_js_sitepalFullbodyLite_AudioManager.call(this);
};
$hxClasses["com.oddcast.app.js.sitepalFullbodyLite.AudioManagerSimpleVisemes"] = com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes;
com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes.__name__ = ["com","oddcast","app","js","sitepalFullbodyLite","AudioManagerSimpleVisemes"];
com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes.__super__ = com_oddcast_app_js_sitepalFullbodyLite_AudioManager;
com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes.prototype = $extend(com_oddcast_app_js_sitepalFullbodyLite_AudioManager.prototype,{
	getCurrentSimpleViseme_b0: function(frameUpdate,mouthVersion) {
		if(!this.isPlaying()) return 1;
		return this.getCurrentPlayingVAudioSimpleVisemes().currentMouthFrame_b0(frameUpdate,mouthVersion,0.0);
	}
	,getCurrentCurrentEnergy: function(frameUpdate) {
		if(!this.isPlaying()) return 0.0;
		return this.getCurrentPlayingVAudioSimpleVisemes().currentEnergy(frameUpdate,0.0);
	}
	,getCurrentPlayingVAudioSimpleVisemes: function() {
		if(this.noAudio()) return null;
		return this.currPlayingAudio;
	}
	,instantiatePlayingVAudio: function(vAudio) {
		return new com_oddcast_audio_PlayingVAudioSimpleVisemes(vAudio);
	}
	,instantiatePlayingVAudioSaySilent: function(visemeDurationSecs,audioFinishedCallback,playForSecs,playingAudio) {
		return new com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes(visemeDurationSecs,audioFinishedCallback,playForSecs,playingAudio);
	}
	,__class__: com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes
});
var com_oddcast_haxeSwf_IHaxeAPI = function() { };
$hxClasses["com.oddcast.haxeSwf.IHaxeAPI"] = com_oddcast_haxeSwf_IHaxeAPI;
com_oddcast_haxeSwf_IHaxeAPI.__name__ = ["com","oddcast","haxeSwf","IHaxeAPI"];
var com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine = function() { };
$hxClasses["com.oddcast.app.js.sitepalFullbodyLite.ISitePalFullBodyEngine"] = com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine;
com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine.__name__ = ["com","oddcast","app","js","sitepalFullbodyLite","ISitePalFullBodyEngine"];
com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine.__interfaces__ = [com_oddcast_haxeSwf_IHaxeAPI];
com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine.prototype = {
	__class__: com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine
};
var com_oddcast_app_vhss_$extraction_AccType = $hxClasses["com.oddcast.app.vhss_extraction.AccType"] = { __ename__ : ["com","oddcast","app","vhss_extraction","AccType"], __constructs__ : ["Hair","Glasses","Costume","Necklace","Hat","fhair","mouth","Bottom","Shoes","Props","Headphones","Unknown"] };
com_oddcast_app_vhss_$extraction_AccType.Hair = ["Hair",0];
com_oddcast_app_vhss_$extraction_AccType.Hair.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Hair.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Glasses = ["Glasses",1];
com_oddcast_app_vhss_$extraction_AccType.Glasses.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Glasses.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Costume = ["Costume",2];
com_oddcast_app_vhss_$extraction_AccType.Costume.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Costume.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Necklace = ["Necklace",3];
com_oddcast_app_vhss_$extraction_AccType.Necklace.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Necklace.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Hat = ["Hat",4];
com_oddcast_app_vhss_$extraction_AccType.Hat.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Hat.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.fhair = ["fhair",5];
com_oddcast_app_vhss_$extraction_AccType.fhair.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.fhair.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.mouth = ["mouth",6];
com_oddcast_app_vhss_$extraction_AccType.mouth.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.mouth.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Bottom = ["Bottom",7];
com_oddcast_app_vhss_$extraction_AccType.Bottom.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Bottom.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Shoes = ["Shoes",8];
com_oddcast_app_vhss_$extraction_AccType.Shoes.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Shoes.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Props = ["Props",9];
com_oddcast_app_vhss_$extraction_AccType.Props.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Props.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Headphones = ["Headphones",10];
com_oddcast_app_vhss_$extraction_AccType.Headphones.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Headphones.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.Unknown = ["Unknown",11];
com_oddcast_app_vhss_$extraction_AccType.Unknown.toString = $estr;
com_oddcast_app_vhss_$extraction_AccType.Unknown.__enum__ = com_oddcast_app_vhss_$extraction_AccType;
com_oddcast_app_vhss_$extraction_AccType.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_AccType.Hair,com_oddcast_app_vhss_$extraction_AccType.Glasses,com_oddcast_app_vhss_$extraction_AccType.Costume,com_oddcast_app_vhss_$extraction_AccType.Necklace,com_oddcast_app_vhss_$extraction_AccType.Hat,com_oddcast_app_vhss_$extraction_AccType.fhair,com_oddcast_app_vhss_$extraction_AccType.mouth,com_oddcast_app_vhss_$extraction_AccType.Bottom,com_oddcast_app_vhss_$extraction_AccType.Shoes,com_oddcast_app_vhss_$extraction_AccType.Props,com_oddcast_app_vhss_$extraction_AccType.Headphones,com_oddcast_app_vhss_$extraction_AccType.Unknown];
var com_oddcast_app_vhss_$extraction_AccTypeTools = function() { };
$hxClasses["com.oddcast.app.vhss_extraction.AccTypeTools"] = com_oddcast_app_vhss_$extraction_AccTypeTools;
com_oddcast_app_vhss_$extraction_AccTypeTools.__name__ = ["com","oddcast","app","vhss_extraction","AccTypeTools"];
com_oddcast_app_vhss_$extraction_AccTypeTools.fromString = function(s) {
	switch(s) {
	case "Hair":
		return com_oddcast_app_vhss_$extraction_AccType.Hair;
	case "Glasses":
		return com_oddcast_app_vhss_$extraction_AccType.Glasses;
	case "Costume":
		return com_oddcast_app_vhss_$extraction_AccType.Costume;
	case "Necklace":
		return com_oddcast_app_vhss_$extraction_AccType.Necklace;
	case "Hat":
		return com_oddcast_app_vhss_$extraction_AccType.Hat;
	case "Facial Hair":case "fhair":
		return com_oddcast_app_vhss_$extraction_AccType.fhair;
	case "Mouth":case "mouth":
		return com_oddcast_app_vhss_$extraction_AccType.mouth;
	case "Bottom":
		return com_oddcast_app_vhss_$extraction_AccType.Bottom;
	case "Shoes":
		return com_oddcast_app_vhss_$extraction_AccType.Shoes;
	case "Props":
		return com_oddcast_app_vhss_$extraction_AccType.Props;
	case "Headphones":
		return com_oddcast_app_vhss_$extraction_AccType.Headphones;
	case "Unknown":
		return com_oddcast_app_vhss_$extraction_AccType.Unknown;
	default:
		throw new js__$Boot_HaxeError(" accType " + s + " is not recognized");
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.fromStringCaseInsensitive = function(s) {
	var _g = s.toLowerCase();
	switch(_g) {
	case "hair":
		return com_oddcast_app_vhss_$extraction_AccType.Hair;
	case "glasses":
		return com_oddcast_app_vhss_$extraction_AccType.Glasses;
	case "costume":
		return com_oddcast_app_vhss_$extraction_AccType.Costume;
	case "necklace":
		return com_oddcast_app_vhss_$extraction_AccType.Necklace;
	case "hat":
		return com_oddcast_app_vhss_$extraction_AccType.Hat;
	case "facial hair":case "fhair":
		return com_oddcast_app_vhss_$extraction_AccType.fhair;
	case "mouth":
		return com_oddcast_app_vhss_$extraction_AccType.mouth;
	case "bottom":
		return com_oddcast_app_vhss_$extraction_AccType.Bottom;
	case "shoes":
		return com_oddcast_app_vhss_$extraction_AccType.Shoes;
	case "props":
		return com_oddcast_app_vhss_$extraction_AccType.Props;
	case "headphones":
		return com_oddcast_app_vhss_$extraction_AccType.Headphones;
	case "unknown":
		return com_oddcast_app_vhss_$extraction_AccType.Unknown;
	default:
		throw new js__$Boot_HaxeError(" accType " + s + " is not recognized");
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getStringFromIndex = function(index) {
	switch(index) {
	case 1:
		return "Costume";
	case 2:
		return "mouth";
	case 3:
		return "Hair";
	case 4:
		return "fhair";
	case 5:
		return "Hat";
	case 6:
		return "Necklace";
	case 7:
		return "Glasses";
	case 8:
		return "Bottom";
	case 9:
		return "Shoes";
	case 10:
		return "Props";
	default:
		return "Unknown";
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getIndexFromAccType = function(acctype) {
	switch(acctype[1]) {
	case 2:
		return 1;
	case 6:
		return 2;
	case 0:
		return 3;
	case 5:
		return 4;
	case 4:
		return 5;
	case 3:
		return 6;
	case 1:
		return 7;
	case 7:
		return 8;
	case 8:
		return 9;
	case 9:
		return 10;
	case 10:
		return -1;
	case 11:
		return -1;
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getAccTypeFromIndex = function(index) {
	switch(index) {
	case 1:
		return com_oddcast_app_vhss_$extraction_AccType.Costume;
	case 2:
		return com_oddcast_app_vhss_$extraction_AccType.mouth;
	case 3:
		return com_oddcast_app_vhss_$extraction_AccType.Hair;
	case 4:
		return com_oddcast_app_vhss_$extraction_AccType.fhair;
	case 5:
		return com_oddcast_app_vhss_$extraction_AccType.Hat;
	case 6:
		return com_oddcast_app_vhss_$extraction_AccType.Necklace;
	case 7:
		return com_oddcast_app_vhss_$extraction_AccType.Glasses;
	case 8:
		return com_oddcast_app_vhss_$extraction_AccType.Bottom;
	case 9:
		return com_oddcast_app_vhss_$extraction_AccType.Shoes;
	case 10:
		return com_oddcast_app_vhss_$extraction_AccType.Props;
	default:
		return com_oddcast_app_vhss_$extraction_AccType.Unknown;
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getCompatId = function(accType) {
	switch(accType[1]) {
	case 0:
		return 3;
	case 1:
		return 4;
	case 2:
		return 6;
	case 3:
		return 8;
	case 4:
		return 9;
	case 5:
		return 10;
	case 6:
		return 12;
	case 7:
		return 13;
	case 8:
		return 14;
	case 9:
		return 15;
	case 10:
		return 18;
	case 11:
		return -1;
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getCompatIdFromIndex = function(index) {
	return com_oddcast_app_vhss_$extraction_AccTypeTools.getCompatId(com_oddcast_app_vhss_$extraction_AccTypeTools.getAccTypeFromIndex(index));
};
com_oddcast_app_vhss_$extraction_AccTypeTools.getTypeFromCompatId = function(compatID) {
	switch(compatID) {
	case 3:
		return com_oddcast_app_vhss_$extraction_AccType.Hair;
	case 4:
		return com_oddcast_app_vhss_$extraction_AccType.Glasses;
	case 6:
		return com_oddcast_app_vhss_$extraction_AccType.Costume;
	case 8:
		return com_oddcast_app_vhss_$extraction_AccType.Necklace;
	case 9:
		return com_oddcast_app_vhss_$extraction_AccType.Hat;
	case 10:
		return com_oddcast_app_vhss_$extraction_AccType.fhair;
	case 12:
		return com_oddcast_app_vhss_$extraction_AccType.mouth;
	case 13:
		return com_oddcast_app_vhss_$extraction_AccType.Bottom;
	case 14:
		return com_oddcast_app_vhss_$extraction_AccType.Shoes;
	case 15:
		return com_oddcast_app_vhss_$extraction_AccType.Props;
	case 18:
		return com_oddcast_app_vhss_$extraction_AccType.Headphones;
	default:
		return com_oddcast_app_vhss_$extraction_AccType.Unknown;
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.isHeadAcc = function(accType) {
	switch(accType[1]) {
	case 0:case 1:case 4:case 5:case 6:case 10:
		return true;
	case 2:case 3:case 7:case 8:case 9:case 11:
		return false;
	}
};
com_oddcast_app_vhss_$extraction_AccTypeTools.enumerator = function() {
	return Type.allEnums(com_oddcast_app_vhss_$extraction_AccType);
};
var com_oddcast_app_vhss_$extraction_AccFragType = $hxClasses["com.oddcast.app.vhss_extraction.AccFragType"] = { __ename__ : ["com","oddcast","app","vhss_extraction","AccFragType"], __constructs__ : ["Front","Back","Left","Right","Mirror","Unknown"] };
com_oddcast_app_vhss_$extraction_AccFragType.Front = ["Front",0];
com_oddcast_app_vhss_$extraction_AccFragType.Front.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Front.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.Back = ["Back",1];
com_oddcast_app_vhss_$extraction_AccFragType.Back.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Back.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.Left = ["Left",2];
com_oddcast_app_vhss_$extraction_AccFragType.Left.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Left.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.Right = ["Right",3];
com_oddcast_app_vhss_$extraction_AccFragType.Right.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Right.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.Mirror = ["Mirror",4];
com_oddcast_app_vhss_$extraction_AccFragType.Mirror.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Mirror.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.Unknown = ["Unknown",5];
com_oddcast_app_vhss_$extraction_AccFragType.Unknown.toString = $estr;
com_oddcast_app_vhss_$extraction_AccFragType.Unknown.__enum__ = com_oddcast_app_vhss_$extraction_AccFragType;
com_oddcast_app_vhss_$extraction_AccFragType.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_AccFragType.Front,com_oddcast_app_vhss_$extraction_AccFragType.Back,com_oddcast_app_vhss_$extraction_AccFragType.Left,com_oddcast_app_vhss_$extraction_AccFragType.Right,com_oddcast_app_vhss_$extraction_AccFragType.Mirror,com_oddcast_app_vhss_$extraction_AccFragType.Unknown];
var com_oddcast_app_vhss_$extraction_EngineV5Constants = function() { };
$hxClasses["com.oddcast.app.vhss_extraction.EngineV5Constants"] = com_oddcast_app_vhss_$extraction_EngineV5Constants;
com_oddcast_app_vhss_$extraction_EngineV5Constants.__name__ = ["com","oddcast","app","vhss_extraction","EngineV5Constants"];
var com_oddcast_app_vhss_$extraction_IVHSSEngine = function() { };
$hxClasses["com.oddcast.app.vhss_extraction.IVHSSEngine"] = com_oddcast_app_vhss_$extraction_IVHSSEngine;
com_oddcast_app_vhss_$extraction_IVHSSEngine.__name__ = ["com","oddcast","app","vhss_extraction","IVHSSEngine"];
com_oddcast_app_vhss_$extraction_IVHSSEngine.__interfaces__ = [com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine];
com_oddcast_app_vhss_$extraction_IVHSSEngine.prototype = {
	__class__: com_oddcast_app_vhss_$extraction_IVHSSEngine
};
var com_oddcast_app_vhss_$extraction_VHSSAccessories = function() {
	this.accInUse = com_oddcast_util_ArrayTools.populate([],11,function() {
		return 0;
	});
	this.incompatMap = new haxe_ds_EnumValueMap();
	this.rasterDataSetStore = new haxe_ds_EnumValueMap();
	this.currLoadingAccArchiveSets = new haxe_ds_EnumValueMap();
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSAccessories"] = com_oddcast_app_vhss_$extraction_VHSSAccessories;
com_oddcast_app_vhss_$extraction_VHSSAccessories.__name__ = ["com","oddcast","app","vhss_extraction","VHSSAccessories"];
com_oddcast_app_vhss_$extraction_VHSSAccessories.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_app_vhss_$extraction_VHSSAccessories.splitOHstringToAccId = function(url) {
	if(url != null) {
		var split = url.split("/");
		if(split.length == 1) split = url.split("\\");
		if(split.length < 11) haxe_Log.trace("should be " + 11 + " ids, have only " + split.length,{ fileName : "VHSSAccessories.hx", lineNumber : 170, className : "com.oddcast.app.vhss_extraction.VHSSAccessories", methodName : "splitOHstringToAccId"}); else {
			var accIDs = [];
			var _g = 0;
			while(_g < 11) {
				var i = _g++;
				accIDs.push(Std.parseInt(split[i]));
			}
			return accIDs;
		}
	}
	return null;
};
com_oddcast_app_vhss_$extraction_VHSSAccessories.getAttachments = function(acctype) {
	switch(acctype[1]) {
	case 0:
		return [null,["backhair_art","hair_art_back"],["hair_art_l"],["hair_art_r"]];
	case 1:
		return [["glassesl","glassesr"]];
	case 2:
		return [["costume"]];
	case 3:
		return [["necklace"]];
	case 4:
		return [null,["hatB"],["hatL"],["hatR"]];
	case 5:
		return [null,null,["facialhairl"],["facialhairr"]];
	case 6:
		return [["mouth_attached"]];
	case 7:
		return [["bottom"]];
	case 8:
		return null;
	case 9:
		return [["prop_front"],["prop_back"]];
	case 10:
		return null;
	case 11:
		return null;
	}
};
com_oddcast_app_vhss_$extraction_VHSSAccessories.isAttachmentPoint = function(objName) {
	if(com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentMap == null) {
		com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentMap = new haxe_ds_StringMap();
		var _g = 0;
		var _g1 = com_oddcast_app_vhss_$extraction_AccTypeTools.enumerator();
		while(_g < _g1.length) {
			var accType = _g1[_g];
			++_g;
			var attachments = com_oddcast_app_vhss_$extraction_VHSSAccessories.getAttachments(accType);
			if(attachments != null) {
				var _g2 = 0;
				while(_g2 < attachments.length) {
					var a = attachments[_g2];
					++_g2;
					if(a != null) {
						var _g3 = 0;
						while(_g3 < a.length) {
							var b = a[_g3];
							++_g3;
							com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentMap.set(b,accType);
						}
					}
				}
			}
		}
	}
	return com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentMap.get(objName);
};
com_oddcast_app_vhss_$extraction_VHSSAccessories.prototype = {
	loadAccessory: function(frags,fileExtension,root,completeCallback) {
		var _g2 = this;
		var currLoadingAccArchives = this.currLoadingAccArchiveSets.get(frags.accType);
		if(currLoadingAccArchives != null) {
			var _g = 0;
			while(_g < currLoadingAccArchives.length) {
				var currLoadingAccArchive = currLoadingAccArchives[_g];
				++_g;
				currLoadingAccArchive.abort({ fileName : "VHSSAccessories.hx", lineNumber : 54, className : "com.oddcast.app.vhss_extraction.VHSSAccessories", methodName : "loadAccessory"});
			}
		}
		this.currLoadingAccArchiveSets.set(frags.accType,null);
		if(frags.accFrags == null) {
			this.setAccessory(frags.accType,null,root,null);
			if(completeCallback != null) completeCallback(false);
			return;
		}
		var _g1 = 0;
		var _g11 = frags.accFrags;
		while(_g1 < _g11.length) {
			var frag = _g11[_g1];
			++_g1;
			if(!frag.valid) {
				if(completeCallback != null) completeCallback(false);
				return;
			}
		}
		var accType = frags.accType;
		var compatId = frags.compatId;
		var toEnable = this.incompatMap.get(accType);
		if(toEnable != null) {
			this.enableAccType(toEnable,true,root);
			this.incompatMap.remove(accType);
		}
		if(frags.compatId != 0) {
			var incompatAccType = com_oddcast_app_vhss_$extraction_AccTypeTools.getTypeFromCompatId(compatId);
			this.enableAccType(incompatAccType,false,root);
			this.incompatMap.set(accType,incompatAccType);
		}
		var loaded = new haxe_ds_EnumValueMap();
		var isNonNullAcc = false;
		var currLoadingArchives = [];
		this.currLoadingAccArchiveSets.set(frags.accType,currLoadingArchives);
		var _g3 = 0;
		var _g12 = frags.accFrags;
		while(_g3 < _g12.length) {
			var frag1 = [_g12[_g3]];
			++_g3;
			try {
				var url = frag1[0].url;
				var dir = com_oddcast_util_UtilsLite.getFileDirectory(url) + "/";
				var imagename = com_oddcast_util_UtilsLite.stripFileDirectory(url);
				var archive = com_oddcast_app_vhss_$extraction_VHSSHostApp.createArchive(dir,com_oddcast_util_js_OStype.Debug,null);
				currLoadingArchives.push(archive);
				com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.readFromSpriteSheetImage(archive,imagename,(function() {
					return function() {
						return new com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet();
					};
				})(),(function(frag1) {
					return function(rasterDataSet) {
						var accFragDataSet = rasterDataSet;
						isNonNullAcc = isNonNullAcc || !accFragDataSet.isNull();
						if(!isNonNullAcc) {
							var junk = 1;
						}
						accFragDataSet.initDisplayTree();
						loaded.set(frag1[0].accFragType,accFragDataSet);
						if(Lambda.count(loaded) == frags.accFrags.length) {
							var baseRGBTotals = [];
							_g2.setAccessory(frags.accType,loaded,root,baseRGBTotals);
							var i = 0;
							var _g31 = 0;
							while(_g31 < baseRGBTotals.length) {
								var baseTotal = baseRGBTotals[_g31];
								++_g31;
								if(baseTotal != null) baseTotal.setAverageColoring("accColorIndex=" + i);
								i++;
							}
							_g2.setAccInUse(_g2.convertAccTypeToAccIndex(frags.accType),isNonNullAcc?frags.id:0);
							_g2.currLoadingAccArchiveSets.set(frags.accType,null);
							if(completeCallback != null) completeCallback(true);
						}
					};
				})(frag1),(function() {
					return function(url1,error) {
						throw new js__$Boot_HaxeError(url1 + " " + error);
					};
				})());
			} catch( e ) {
				haxe_CallStack.lastException = e;
				if (e instanceof js__$Boot_HaxeError) e = e.val;
				if(completeCallback != null) completeCallback(false);
				break;
			}
		}
	}
	,initAccInUseFromOptimizedHost: function(url) {
		if(url != null && url.length > 0) {
			var accIDs = com_oddcast_app_vhss_$extraction_VHSSAccessories.splitOHstringToAccId(url);
			if(accIDs != null) {
				var _g = 0;
				while(_g < 11) {
					var i = _g++;
					this.setAccInUse(i,accIDs[i]);
				}
			}
		}
	}
	,setHostID: function(id) {
		this.setAccInUse(0,id);
	}
	,setMouthID: function(id) {
		haxe_Log.trace("setMouthID(" + id + ")",{ fileName : "VHSSAccessories.hx", lineNumber : 200, className : "com.oddcast.app.vhss_extraction.VHSSAccessories", methodName : "setMouthID"});
		this.setAccInUse(2,id);
	}
	,createOHAccString: function() {
		var sb_b = "";
		var _g1 = 0;
		var _g = this.accInUse.length;
		while(_g1 < _g) {
			var i = _g1++;
			var v;
			if(this.isVisible(i)) v = this.accInUse[i]; else v = 0;
			sb_b += Std.string(v + "/");
		}
		return sb_b;
	}
	,dispose: function() {
		if(this.rasterDataSetStore != null) {
			var $it0 = this.rasterDataSetStore.iterator();
			while( $it0.hasNext() ) {
				var i = $it0.next();
				com_oddcast_util_Disposable.disposeMapIfValid(i);
			}
			this.rasterDataSetStore = null;
		}
	}
	,getAccessoryID: function(typeString) {
		var accType = com_oddcast_app_vhss_$extraction_AccTypeTools.fromStringCaseInsensitive(typeString);
		var index = com_oddcast_app_vhss_$extraction_AccTypeTools.getIndexFromAccType(accType);
		return this.getAccInUse(index);
	}
	,isVisible: function(accIndex) {
		var $it0 = this.incompatMap.iterator();
		while( $it0.hasNext() ) {
			var accType = $it0.next();
			if(this.convertAccTypeToAccIndex(accType) == accIndex) return false;
		}
		return true;
	}
	,setAccessory: function(acctype,loaded,root,rgbTotals) {
		var alreadyLoadedFrags = this.rasterDataSetStore.get(acctype);
		if(alreadyLoadedFrags != null) com_oddcast_util_Disposable.disposeMapIfValid(alreadyLoadedFrags);
		this.rasterDataSetStore.set(acctype,loaded);
		switch(acctype[1]) {
		case 6:
			this.setMouthAccessory(loaded,root);
			root.sumColoring(com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH,rgbTotals);
			break;
		case 1:
			this.setGlassesAccessory(loaded,root);
			break;
		case 0:case 2:case 3:case 4:case 5:case 7:case 8:case 9:case 10:
			this.setMultiFragAccessory(acctype,loaded,root);
			root.sumColoring(com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR,rgbTotals);
			break;
		case 11:
			break;
		}
	}
	,setMouthAccessory: function(loaded,root) {
		var attachTo = root.findDisplayAssetNamed("mouth_attached");
		attachTo.removeChildren();
		var _g = 0;
		var _g1 = ["lips","tt"];
		while(_g < _g1.length) {
			var attachString = _g1[_g];
			++_g;
			var accFragDataSet;
			if(loaded != null) accFragDataSet = loaded.get(com_oddcast_app_vhss_$extraction_AccFragType.Front); else accFragDataSet = null;
			var accDisplayAsset;
			if(accFragDataSet != null) accDisplayAsset = accFragDataSet.root.findDisplayAssetNamed(attachString); else accDisplayAsset = null;
			this.replaceAcc(attachTo,accDisplayAsset,false);
		}
	}
	,setGlassesAccessory: function(loaded,root) {
		var _g = 0;
		var _g1 = ["glassesl","glassesr"];
		while(_g < _g1.length) {
			var attachString = _g1[_g];
			++_g;
			var attachTo = root.findDisplayAssetNamed(attachString);
			if(attachTo == null) {
				if(loaded != null) haxe_Log.trace("[ERROR] cannot find attachpoint:" + attachString,{ fileName : "VHSSAccessories.hx", lineNumber : 304, className : "com.oddcast.app.vhss_extraction.VHSSAccessories", methodName : "setGlassesAccessory"});
				return;
			}
			var accFragDataSet;
			if(loaded != null) accFragDataSet = loaded.get(com_oddcast_app_vhss_$extraction_AccFragType.Mirror); else accFragDataSet = null;
			this.replaceAcc(attachTo,accFragDataSet != null?accFragDataSet.root:null);
		}
	}
	,setMultiFragAccessory: function(acctype,loaded,root) {
		var attachToStrs = com_oddcast_app_vhss_$extraction_VHSSAccessories.getAttachments(acctype);
		if(attachToStrs != null) {
			var _g1 = 0;
			var _g = attachToStrs.length;
			while(_g1 < _g) {
				var i = _g1++;
				var attachStrings = attachToStrs[i];
				if(attachStrings != null) {
					var bFound = false;
					var _g2 = 0;
					while(_g2 < attachStrings.length) {
						var attachString = attachStrings[_g2];
						++_g2;
						var attachTo = root.findDisplayAssetNamed(attachString);
						if(attachTo != null) {
							var accFragDataSet;
							if(loaded != null) accFragDataSet = loaded.get(com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentIndex[i]); else accFragDataSet = null;
							this.replaceAcc(attachTo,accFragDataSet != null?accFragDataSet.root:null);
							bFound = true;
							break;
						}
					}
					if(!bFound && loaded != null) haxe_Log.trace("[ERROR] cannot find attachpoint:" + Std.string(attachStrings),{ fileName : "VHSSAccessories.hx", lineNumber : 334, className : "com.oddcast.app.vhss_extraction.VHSSAccessories", methodName : "setMultiFragAccessory"});
				}
			}
		}
	}
	,replaceAcc: function(attachTo,accDisplayAsset,bRemoveChildren) {
		if(bRemoveChildren == null) bRemoveChildren = true;
		if(bRemoveChildren) {
			var _g = 0;
			var _g1 = attachTo.children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.dispose();
			}
			attachTo.removeChildren();
		}
		var attachImageAssets = null;
		if(accDisplayAsset != null) attachTo.addChild(accDisplayAsset);
		attachTo.replaceImageAssets(attachImageAssets);
	}
	,enableAccType: function(accType,bEnable,root) {
		var attachToStrs = com_oddcast_app_vhss_$extraction_VHSSAccessories.getAttachments(accType);
		if(attachToStrs != null) {
			var _g1 = 0;
			var _g = attachToStrs.length;
			while(_g1 < _g) {
				var i = _g1++;
				var attachStrings = attachToStrs[i];
				if(attachStrings != null) {
					var bFound = false;
					var _g2 = 0;
					while(_g2 < attachStrings.length) {
						var attachString = attachStrings[_g2];
						++_g2;
						var attachTo = root.findDisplayAssetNamed(attachString);
						if(attachTo != null) attachTo.noRender = !bEnable;
					}
				}
			}
		}
	}
	,setAccInUse: function(index,value) {
		if(index >= 0 && index < 11) this.accInUse[index] = value;
	}
	,getAccInUse: function(index) {
		if(index >= 0 && index < 11) return this.accInUse[index];
		return -1;
	}
	,convertAccTypeToAccIndex: function(accType) {
		switch(accType[1]) {
		case 0:
			return 3;
		case 1:
			return 7;
		case 2:
			return 1;
		case 3:
			return 6;
		case 4:
			return 5;
		case 5:
			return 4;
		case 6:
			return 2;
		case 7:
			return 8;
		case 8:
			return 9;
		case 9:
			return 10;
		case 10:case 11:
			return -1;
		}
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSAccessories
};
var com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon = function() {
	var compile_date = "Engine compiled: " + "2016-10-03 11:58:07";
};
$hxClasses["com.oddcast.app.vhss_extraction.SitePalFullBodyEngineCommon"] = com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon;
com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.__name__ = ["com","oddcast","app","vhss_extraction","SitePalFullBodyEngineCommon"];
com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.prototype = {
	isPointOverHost: function(x,y) {
		var hc = this.getActualHostCanvas();
		if(hc != null && !hc.canvasEquelTo(null)) {
			x = com_oddcast_util_EnsureType.isFloat(x,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 74, className : "com.oddcast.app.vhss_extraction.SitePalFullBodyEngineCommon", methodName : "isPointOverHost"});
			y = com_oddcast_util_EnsureType.isFloat(y,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 75, className : "com.oddcast.app.vhss_extraction.SitePalFullBodyEngineCommon", methodName : "isPointOverHost"});
			var pix = hc.getPixel(Math.floor(x),Math.floor(y),-16777216);
			if((pix & -16777216) != 0) return "host";
		}
		return null;
	}
	,getActualHostCanvas: function() {
		return this.hostCanvas;
	}
	,listCustomAnimations: function() {
		return null;
	}
	,playCustomAnimation: function(label,speed,startedToPlayCallback) {
		return null;
	}
	,dispose: function() {
		if(this.hostCanvas != null) this.hostCanvas.clear();
		this.hostCanvas = com_oddcast_util_Disposable.disposeIfValid(this.hostCanvas);
	}
	,__class__: com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon
};
var com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine = function() {
	this.defaultHostScale = 1.8;
	this.centerHostY = 0.0;
	this.centerHostX = 0.0;
	com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.call(this);
	this.audioManager = new com_oddcast_app_js_sitepalFullbodyLite_AudioManagerSimpleVisemes();
	this.canvasScale = 1.0;
	this.colorEditable = false;
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine"] = com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine;
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.__name__ = ["com","oddcast","app","vhss_extraction","VHSSSitePalFullBodyEngine"];
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_app_js_sitepalFullbodyLite_ISitePalFullBodyEngine];
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.__super__ = com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon;
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.prototype = $extend(com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.prototype,{
	init: function(createStageCanvasCallback,disposeStageCanvasCallback) {
		this.setCanvas(createStageCanvasCallback(0,0));
	}
	,setCanvas: function(canvas) {
		this.hostCanvas = com_oddcast_util_Disposable.disposeIfValid(this.hostCanvas);
		this.hostCanvas = canvas;
		this.setCanvas2(this.hostCanvas);
	}
	,setCanvas2: function(canvas) {
		if(canvas != null) {
			this.canvasScale = canvas.get_height() / com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.CANVAS_SIZE_UNITY;
			this.centerHostX = canvas.get_width() / 2;
			this.centerHostY = canvas.get_height() / 2;
			haxe_Log.trace("canvasScale:" + this.canvasScale,{ fileName : "VHSSHostApp.hx", lineNumber : 142, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "setCanvas2"});
		}
	}
	,setStageDimensions: function(width,height) {
		return 1.0;
	}
	,loadPersona: function(dir,fileErrorCallback_urlString_error_String,idleLoadedCallback_urlString,coreLoadedCallback_urlString) {
		if(this.hostCanvas == null) throw new js__$Boot_HaxeError("Cannot loadPersona prior to setJSCanvas(your canvas) call");
		haxe_Log.trace("loadPersona(" + dir + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 155, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "loadPersona"});
		if(dir == null) throw new js__$Boot_HaxeError("Do not call loadPersona(null, .....) , call unloadPersona() to clear prev character");
		var split = dir.split("?");
		var dirUrl = split[0];
		var cs;
		if(split.length > 1) cs = split[1]; else cs = null;
		this.loadHost(dirUrl,cs,this.colorEditable,function() {
			idleLoadedCallback_urlString(dir);
			coreLoadedCallback_urlString(dir);
		},fileErrorCallback_urlString_error_String);
	}
	,unloadPersona: function() {
		this.unloadHost();
	}
	,sayUrl: function(url,offsetSeconds,autoStart,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback) {
		this.sayUrlWithID3(url,offsetSeconds,autoStart,null,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback);
	}
	,sayUrlWithID3: function(url,offsetSeconds,autoStart,id3String,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback) {
		haxe_Log.trace("sayUrl(" + url + ", " + offsetSeconds + ", " + (autoStart == null?"null":"" + autoStart) + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 214, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "sayUrlWithID3"});
		if(url == null || url.length > 0) {
			this.bPause = false;
			this.bFrozen = false;
			this.stopPlayingAudio2(false);
			if(this.audioManager != null) this.audioManager.sayUrl3(url,com_oddcast_util_EnsureType.isFloat(offsetSeconds,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 222, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "sayUrlWithID3"}),com_oddcast_util_EnsureType.isBool(autoStart,true,null,{ fileName : "VHSSHostApp.hx", lineNumber : 222, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "sayUrlWithID3"}),id3String,$bind(this,this.audioStarting),fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback);
		}
	}
	,audioStarting: function(audio) {
		this.bPause = false;
	}
	,saySilent: function(sec,audioStartedCallback,audioFinishedCallback) {
		var _g = this;
		if(this.audioManager != null) this.audioManager.saySilent2(com_oddcast_util_EnsureType.isFloat(sec,10.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 241, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "saySilent"}),function(audio) {
			_g.bPause = false;
		},audioStartedCallback,function(s,f) {
			audioFinishedCallback(s,f);
		});
		return;
	}
	,setAnotherSpeechComing: function(anotherSpeechComing) {
		haxe_Log.trace("setAnotherSpeechComing(" + (anotherSpeechComing == null?"null":"" + anotherSpeechComing) + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 265, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "setAnotherSpeechComing"});
	}
	,stopPlayingAudio: function() {
		this.stopPlayingAudio2(true);
	}
	,stopPlayingAudio2: function(bEffectCallback) {
		if(this.audioManager != null) {
			if(this.audioManager.stopPlayingAudioWithCallback(null,0.0,com_oddcast_util_EnsureType.isBool(bEffectCallback,true,null,{ fileName : "VHSSHostApp.hx", lineNumber : 274, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "stopPlayingAudio2"}))) {
			}
		}
	}
	,setHostVolume: function(v) {
		v = com_oddcast_util_EnsureType.isInt(v,10,null,{ fileName : "VHSSHostApp.hx", lineNumber : 281, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "setHostVolume"});
		if(v < 0) v = 0;
		if(v > 10) v = 10;
		if(this.audioManager != null) this.audioManager.setHostVolume(v);
	}
	,getCurrentAudioProgress: function() {
		if(this.audioManager != null) return this.audioManager.getCurrentAudioProgress(); else return -1.0;
	}
	,isAudioLoaded: function() {
		if(this.audioManager != null) return this.audioManager.isAudioLoaded();
		return false;
	}
	,pauseAudio: function() {
		if(this.bPause == false) this.bFrozenLastFrame = com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.B_FROZEN_LAST_FRAME;
		this.bPause = true;
		if(this.audioManager != null) return this.audioManager.pauseAudio(); else return false;
	}
	,resumeAudio: function() {
		this.bPause = false;
		if(this.audioManager != null) return this.audioManager.resumeAudio(); else return false;
	}
	,getAudioPositionSeconds: function() {
		if(this.audioManager == null) return this.audioManager.getAudioPositionSeconds();
		return -1.0;
	}
	,replayAudio: function(offsetSeconds) {
		this.bPause = false;
		this.bFrozen = false;
		if(this.audioManager != null) {
			this.audioManager.replayAudio(com_oddcast_util_EnsureType.isFloat(offsetSeconds,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 322, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "replayAudio"}));
			return true;
		}
		return false;
	}
	,freezeToggle: function() {
		if(!this.bFrozen) this.freeze(true); else this.resume();
	}
	,freeze: function(eyesOpen) {
		if(eyesOpen == null) eyesOpen = false;
		this.bFrozen = true;
		this.pauseAudio();
		this.bForceEyesOpenDuringFreeze = com_oddcast_util_EnsureType.isBool(eyesOpen,false,null,{ fileName : "VHSSHostApp.hx", lineNumber : 338, className : "com.oddcast.app.vhss_extraction.VHSSSitePalFullBodyEngine", methodName : "freeze"});
	}
	,resume: function() {
		this.bFrozen = false;
		this.resumeAudio();
	}
	,dispose: function() {
		com_oddcast_app_vhss_$extraction_SitePalFullBodyEngineCommon.prototype.dispose.call(this);
		this.audioManager = com_oddcast_util_Disposable.disposeIfValid(this.audioManager);
		if(this.disposeStageCanvasCallback != null) this.disposeStageCanvasCallback(this.hostCanvas);
		this.disposeStageCanvasCallback = null;
	}
	,loadHost: function(url,colorString,colorEditable,completeCallback,errorCB) {
	}
	,unloadHost: function() {
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine
});
var com_oddcast_io_archive_iArchive_IArchiveReadOnly = function() { };
$hxClasses["com.oddcast.io.archive.iArchive.IArchiveReadOnly"] = com_oddcast_io_archive_iArchive_IArchiveReadOnly;
com_oddcast_io_archive_iArchive_IArchiveReadOnly.__name__ = ["com","oddcast","io","archive","iArchive","IArchiveReadOnly"];
com_oddcast_io_archive_iArchive_IArchiveReadOnly.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_io_archive_iArchive_IArchiveReadOnly.prototype = {
	__class__: com_oddcast_io_archive_iArchive_IArchiveReadOnly
};
var com_oddcast_io_archive_iArchive_IArchive = function() { };
$hxClasses["com.oddcast.io.archive.iArchive.IArchive"] = com_oddcast_io_archive_iArchive_IArchive;
com_oddcast_io_archive_iArchive_IArchive.__name__ = ["com","oddcast","io","archive","iArchive","IArchive"];
com_oddcast_io_archive_iArchive_IArchive.__interfaces__ = [com_oddcast_io_archive_iArchive_IArchiveReadOnly];
com_oddcast_io_archive_iArchive_IArchive.prototype = {
	__class__: com_oddcast_io_archive_iArchive_IArchive
};
var com_oddcast_io_archive_iArchive_ArchiveLiteAbstract = function(url,osType,archiveLoadStats) {
	this.errorCallback = null;
	this.base = url;
	this.archiveLoadStats = archiveLoadStats;
	this.osType = osType;
	this.transportTimeMillis = 0.0;
};
$hxClasses["com.oddcast.io.archive.iArchive.ArchiveLiteAbstract"] = com_oddcast_io_archive_iArchive_ArchiveLiteAbstract;
com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.__name__ = ["com","oddcast","io","archive","iArchive","ArchiveLiteAbstract"];
com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.__interfaces__ = [com_oddcast_io_archive_iArchive_IArchive];
com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.prototype = {
	load: function(archiveDataContent,errorCallback) {
		this.notYetImplemented();
	}
	,loadFinished: function() {
		if(this.archiveLoadStats != null) this.archiveLoadStats.loadFinished();
	}
	,fileFinished: function(fileLoadStats) {
		if(this.archiveLoadStats != null) this.archiveLoadStats.fileFinished(fileLoadStats);
	}
	,readTextFile: function(filename,callbackFunc,errorCallback) {
		this.notYetImplemented();
	}
	,readBinaryFile: function(url,txtbackupfilename,complete,errorCallback,littleEndian) {
		if(littleEndian == null) littleEndian = true;
		this.notYetImplemented();
	}
	,loadImage: function(colorURL,alphaURL,callbackFunc,errorCallback) {
		this.notYetImplemented();
	}
	,getFullpath: function(filename) {
		this.notYetImplemented();
		return null;
	}
	,getArchiveLoadStats: function() {
		return this.archiveLoadStats;
	}
	,dispose: function() {
		this.notYetImplemented();
	}
	,getID: function() {
		return null;
	}
	,writeToDeviceNow: function(zipname) {
		this.notYetImplemented();
	}
	,getNumberOfFilesStored: function() {
		this.notYetImplemented();
		return 0;
	}
	,saveStringAsFile: function(filename,content,index,bCompressed) {
		if(bCompressed == null) bCompressed = true;
		this.notYetImplemented();
	}
	,texturesShouldBeBase64: function() {
		return false;
	}
	,writeTo: function(filename,content,bCompressed) {
		this.notYetImplemented();
	}
	,notYetImplemented: function() {
		throw new js__$Boot_HaxeError("not yet implemented");
	}
	,getBaseString: function() {
		return new String(this.base);
	}
	,abort: function(posInfos) {
		this._abort = true;
	}
	,isAborted: function() {
		return this._abort;
	}
	,__class__: com_oddcast_io_archive_iArchive_ArchiveLiteAbstract
};
var com_oddcast_io_archive_iArchive_ArchiveLiteDirectory = function(url,osType,archiveLoadStats) {
	com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.call(this,url,osType,archiveLoadStats);
};
$hxClasses["com.oddcast.io.archive.iArchive.ArchiveLiteDirectory"] = com_oddcast_io_archive_iArchive_ArchiveLiteDirectory;
com_oddcast_io_archive_iArchive_ArchiveLiteDirectory.__name__ = ["com","oddcast","io","archive","iArchive","ArchiveLiteDirectory"];
com_oddcast_io_archive_iArchive_ArchiveLiteDirectory.__super__ = com_oddcast_io_archive_iArchive_ArchiveLiteAbstract;
com_oddcast_io_archive_iArchive_ArchiveLiteDirectory.prototype = $extend(com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.prototype,{
	load: function(archiveDataContent,errorCallback) {
		this.errorCallback = errorCallback;
		com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "ArchiveLite.hx", lineNumber : 49, className : "com.oddcast.io.archive.iArchive.ArchiveLiteDirectory", methodName : "load"});
	}
	,readTextFile: function(filename,callbackFunc,lerrorCallback) {
		var _g = this;
		var fileLoadStats = new com_oddcast_io_archive_iArchive_FileLoadStats(filename);
		var r = new haxe_Http(this.getFullpath(filename));
		r.onError = function(msg) {
			if(lerrorCallback != null) lerrorCallback(filename,msg); else if(_g.errorCallback != null) _g.errorCallback(filename,msg);
		};
		r.onData = function(str) {
			_g.fileFinished(fileLoadStats);
			callbackFunc(str);
		};
		r.request(false);
	}
	,loadImage: function(colorURL,maskURL,callbackFunc,lerrorCallback) {
		var _g = this;
		var fileLoadStats = new com_oddcast_io_archive_iArchive_FileLoadStats(colorURL + " " + maskURL);
		var useErrorCallback;
		if(lerrorCallback != null) useErrorCallback = lerrorCallback; else useErrorCallback = this.errorCallback;
		com_oddcast_util_js_JSImage.loadImage(this.getFullpath(colorURL),this.getFullpath(maskURL),false,function(vImage) {
			if(_g.checkAbort(useErrorCallback,colorURL,{ fileName : "ArchiveLite.hx", lineNumber : 97, className : "com.oddcast.io.archive.iArchive.ArchiveLiteDirectory", methodName : "loadImage"})) return;
			_g.fileFinished(fileLoadStats);
			callbackFunc(vImage);
		},useErrorCallback);
	}
	,readBinaryFile: function(filename,txtbackupfilename,complete,errorCallback,littleEndian) {
		if(littleEndian == null) littleEndian = true;
		var _g = this;
		try {
			var req = new XMLHttpRequest();
			req.onload = function(event) {
				if(_g.checkAbort(errorCallback,filename,{ fileName : "ArchiveLite.hx", lineNumber : 142, className : "com.oddcast.io.archive.iArchive.ArchiveLiteDirectory", methodName : "readBinaryFile"})) return;
				var arrayBuffer = req.response;
				var byteArrayJS = new com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS(arrayBuffer,0,littleEndian);
				complete(byteArrayJS);
			};
			req.onreadystatechange = function(event1) {
			};
			req.onerror = function(event2) {
				_g.errorFunc(txtbackupfilename,event2.toString(),errorCallback,filename);
			};
			req.open("GET",this.getFullpath(filename),true);
			req.responseType = "arraybuffer";
			req.send();
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			if(txtbackupfilename != null) this.readTextFile(txtbackupfilename,function(s) {
				complete(new com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS(s));
			},errorCallback); else haxe_Log.trace(filename + " " + Std.string(e.toString()),{ fileName : "ArchiveLite.hx", lineNumber : 214, className : "com.oddcast.io.archive.iArchive.ArchiveLiteDirectory", methodName : "readBinaryFile"});
			errorCallback(this.getFullpath(filename),e.toString());
		}
	}
	,errorFunc: function(txtbackupfilename,errorString,errorCallback,filename) {
		if(txtbackupfilename != null) throw new js__$Boot_HaxeError("Error " + errorString + " " + this.getFullpath(filename)); else if(errorCallback != null) errorCallback(this.getFullpath(filename),errorString);
	}
	,checkAbort: function(lErrorCallback,filename,posInfo) {
		if(this._abort) {
			if(lErrorCallback != null) lErrorCallback(this.getFullpath(filename),"UserAborted");
			return true;
		}
		return false;
	}
	,dispose: function() {
	}
	,getFullpath: function(filename) {
		if(filename != null) return this.base + filename; else return null;
	}
	,__class__: com_oddcast_io_archive_iArchive_ArchiveLiteDirectory
});
var com_oddcast_app_vhss_$extraction_VHSSHostApp = function() {
	this.timerAcc = new com_oddcast_app_flash_TimerAccelerated();
	this.frameUpdate = new com_oddcast_app_FrameUpdate();
	this.frameRenderedCallback = null;
	this.bOnScreenDebugging = 0;
	this.defaultNormalizedFaceHeight = 300;
	com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.call(this);
	this.vhssAnimation = new com_oddcast_app_vhss_$extraction_animation_VHSSanimation();
	this.vhssAppState = new com_oddcast_app_vhss_$extraction_display_VHSSappState();
	this.frameUpdate = new com_oddcast_app_FrameUpdate();
	this.timerAcc = new com_oddcast_app_flash_TimerAccelerated();
	this.gaze = new com_oddcast_app_vhss_$extraction_animation_Gaze();
	this.userHostScale = 1.0;
	this.userHostX = 0.0;
	this.userHostY = 0.0;
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSHostApp"] = com_oddcast_app_vhss_$extraction_VHSSHostApp;
com_oddcast_app_vhss_$extraction_VHSSHostApp.__name__ = ["com","oddcast","app","vhss_extraction","VHSSHostApp"];
com_oddcast_app_vhss_$extraction_VHSSHostApp.__interfaces__ = [com_oddcast_app_vhss_$extraction_IVHSSEngine];
com_oddcast_app_vhss_$extraction_VHSSHostApp.main = function() {
	new com_oddcast_app_vhss_$extraction_VHSSHostApp();
};
com_oddcast_app_vhss_$extraction_VHSSHostApp.__super__ = com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine;
com_oddcast_app_vhss_$extraction_VHSSHostApp.prototype = $extend(com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.prototype,{
	setJSCanvas: function(canvasElement) {
		if(this.hostCanvas == null || !this.hostCanvas.canvasEquelTo(canvasElement)) {
			var stageCanvas = null;
			if(canvasElement != null) {
				stageCanvas = new com_oddcast_util_js_JSCanvas("hostCanvas",0,0,true,canvasElement,true);
				haxe_Log.trace("setJSCanvas(" + canvasElement.width + "*" + canvasElement.height + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 436, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setJSCanvas"});
				stageCanvas.clear();
			} else haxe_Log.trace(" setJSCanvas(null) - called before page has loaded?",{ fileName : "VHSSHostApp.hx", lineNumber : 439, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setJSCanvas"});
			this.setCanvas(stageCanvas);
		} else this.setCanvas2(this.hostCanvas);
	}
	,resetForNewHost: function() {
		this.defaultMouthFrame_b0 = 0;
		this.debug_defaultBrowFrame_1b = 1;
		this.vhssAppState.bInitialized = false;
		this.gaze.resetHost();
		this.smilerMillis = -1;
	}
	,getOHUrl: function(incompatTypeArr) {
		return "oh/" + this.vhssHostInstance.createOHAccString() + "ohv2.png?" + this.vhssHostInstance.getColorURLstring();
	}
	,initAccInUseFromOptimizedHost: function(accStr) {
		this.vhssHostInstance.initAccInUseFromOptimizedHost(accStr);
	}
	,createFaceHeightString: function(normalizedFaceHeight) {
		return "";
	}
	,calcNormalizedFaceHeight: function() {
		return this.defaultNormalizedFaceHeight;
	}
	,loadHost: function(url,colorString,colorEditable,completeCallback,errorCB) {
		var _g = this;
		if(this.currLoadingHostArchive != null) this.currLoadingHostArchive.abort({ fileName : "VHSSHostApp.hx", lineNumber : 500, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "loadHost"});
		this.currLoadingHostArchive = null;
		this.unloadHost();
		this.resetForNewHost();
		var dir = com_oddcast_util_UtilsLite.getFileDirectory(url) + "/";
		var imagename = com_oddcast_util_UtilsLite.stripFileDirectory(url);
		var archive = com_oddcast_app_vhss_$extraction_VHSSHostApp.createArchive(dir,com_oddcast_util_js_OStype.Debug,null);
		this.currLoadingHostArchive = archive;
		com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.readFromSpriteSheetImage(archive,imagename,function() {
			return new com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet();
		},function(rasterDataSet) {
			var hostDataSet = rasterDataSet;
			_g.vhssHostInstance = new com_oddcast_app_vhss_$extraction_VHSSHostInstance(hostDataSet,colorString);
			var split = url.split("/oh/");
			if(split.length > 1) _g.vhssHostInstance.initAccInUseFromOptimizedHost(split[1]);
			_g.vhssHostInstance.setHostID(hostDataSet.hostID_);
			_g.vhssHostInstance.setMouthID(hostDataSet.mouthID_);
			_g.vhssAnimation.modelReady(hostDataSet);
			_g.gaze.setEV(hostDataSet.ev_);
			_g.currLoadingHostArchive = null;
			if(completeCallback != null) completeCallback();
		},errorCB);
	}
	,unloadHost: function() {
		this.vhssHostInstance = com_oddcast_util_Disposable.disposeIfValid(this.vhssHostInstance);
	}
	,loadAccessory: function(accfrags,completeCallback) {
		this.vhssHostInstance.loadAccessory(accfrags,"",completeCallback);
	}
	,loadAccessory2: function(accessoryFragments,accType,id,compatId,name,completeCallback) {
		var accFrags = [];
		var _g = 0;
		while(_g < accessoryFragments.length) {
			var accessoryFragment = accessoryFragments[_g];
			++_g;
			var accFrag = { accFragType : com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_AccFragType,accessoryFragment[0]), url : new String(accessoryFragment[1]), valid : true};
			accFrags.push(accFrag);
		}
		var frags = { accFrags : accFrags, accType : com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_AccType,accType), id : id, compatId : compatId, name : name};
		this.loadAccessory(frags,completeCallback);
	}
	,loadAccessoryFromXML: function(baseURL,xmlPath,completeCallback,errorCallback) {
		var _g = this;
		var url = baseURL + xmlPath;
		var r = new haxe_Http(url);
		r.onError = function(msg) {
			if(errorCallback != null) errorCallback(url,msg); else throw new js__$Boot_HaxeError(url + " " + msg);
		};
		r.onData = function(str) {
			haxe_Log.trace("xml loaded",{ fileName : "VHSSHostApp.hx", lineNumber : 593, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "loadAccessoryFromXML"});
			var doc = new haxe_xml_Fast(Xml.parse(str));
			var acc = com_oddcast_util_XmlFastTools.getElement(doc,"Acc");
			var accFragmentArray = [];
			var _g1 = 0;
			var _g11 = com_oddcast_util_XmlFastTools.getElements(acc,"Frag");
			while(_g1 < _g11.length) {
				var frag = _g11[_g1];
				++_g1;
				var accFragTypeStr = com_oddcast_util_XmlFastTools.getAttributeString(frag,"fragType");
				var filename = com_oddcast_util_XmlFastTools.getAttributeString(frag,"file");
				var accFrag = { accFragType : com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_AccFragType,accFragTypeStr), url : baseURL + filename, valid : true};
				accFragmentArray.push(accFrag);
			}
			var accFrags = { accFrags : accFragmentArray, accType : com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_AccType,com_oddcast_util_XmlFastTools.getAttributeString(acc,"accType")), id : com_oddcast_util_XmlFastTools.getAttributeInt(acc,"id"), compatId : com_oddcast_util_XmlFastTools.getAttributeInt(acc,"compatId",0), name : com_oddcast_util_XmlFastTools.getAttributeString(acc,"name","")};
			_g.loadAccessory(accFrags,completeCallback);
		};
		r.request(false);
	}
	,setGaze: function(angle,sec,rad,pageOrigin) {
		if(pageOrigin == null) pageOrigin = false;
		angle = com_oddcast_util_EnsureType.isInt(angle,0,"angle degrees",{ fileName : "VHSSHostApp.hx", lineNumber : 625, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGaze"});
		sec = com_oddcast_util_EnsureType.isFloat(sec,3.0,"duration seconds",{ fileName : "VHSSHostApp.hx", lineNumber : 626, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGaze"});
		rad = com_oddcast_util_EnsureType.isFloat(rad,100.0,"radius",{ fileName : "VHSSHostApp.hx", lineNumber : 627, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGaze"});
		this.gaze.setGaze(angle,sec,rad,pageOrigin);
	}
	,setGazeAtPoint: function(x,y,durationSeconds) {
		x = com_oddcast_util_EnsureType.isFloat(x,0.0,"x",{ fileName : "VHSSHostApp.hx", lineNumber : 632, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGazeAtPoint"});
		y = com_oddcast_util_EnsureType.isFloat(y,0.0,"y",{ fileName : "VHSSHostApp.hx", lineNumber : 633, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGazeAtPoint"});
		var hostCanvasX = this.hostCoordXtoPixel(this.getHostPosX());
		var hostCanvasY = this.hostCoordYtoPixel(this.getHostPosY());
		var relX = x - hostCanvasX;
		var relY = y - hostCanvasY;
		var angRad = com_oddcast_app_vhss_$extraction_animation_GazePos.xyToAngRad(relX,relY);
		haxe_Log.trace("x: " + Math.round(relX) + "  y:" + Math.round(relY) + " angle:" + Math.round(angRad[0]) + " rad:" + Math.round(angRad[1]),{ fileName : "VHSSHostApp.hx", lineNumber : 639, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setGazeAtPoint"});
		this.setGaze(Math.round(angRad[0]),durationSeconds,angRad[1],false);
	}
	,setHeadMovementBoundX: function(x) {
		this.gaze.setHeadMovementBoundX(com_oddcast_util_EnsureType.isFloat(x,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 644, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHeadMovementBoundX"}));
	}
	,setHeadMovementBoundY: function(y) {
		this.gaze.setHeadMovementBoundY(com_oddcast_util_EnsureType.isFloat(y,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 645, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHeadMovementBoundY"}));
	}
	,getHeadMovementBoundX: function() {
		return this.gaze.getHeadMovementBoundX();
	}
	,getHeadMovementBoundY: function() {
		return this.gaze.getHeadMovementBoundY();
	}
	,setLookSpeed: function(speedIndex) {
		speedIndex = com_oddcast_util_EnsureType.isInt(speedIndex,1,"speedIndex",{ fileName : "VHSSHostApp.hx", lineNumber : 650, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setLookSpeed"});
		this.gaze.setLookSpeed(speedIndex);
	}
	,randomMovement: function(b) {
		this.gaze.setRandomMovement(com_oddcast_util_EnsureType.isBool(b,true,null,{ fileName : "VHSSHostApp.hx", lineNumber : 655, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "randomMovement"}));
	}
	,recenter: function() {
		this.gaze.recenter();
	}
	,followCursor: function(b) {
		this.gaze.followCursor(com_oddcast_util_EnsureType.isBool(b,true,null,{ fileName : "VHSSHostApp.hx", lineNumber : 659, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "followCursor"}));
	}
	,isFollowingCursor: function() {
		return this.gaze.isFollowingCursor();
	}
	,getConfigString: function() {
		return null;
	}
	,getMaxAgeFrames: function() {
		return 0;
	}
	,setMouthFrame: function(frame) {
		this.vhssAppState.mouthFrame_b0 = com_oddcast_util_EnsureType.isInt(frame,0,"frame",{ fileName : "VHSSHostApp.hx", lineNumber : 675, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setMouthFrame"});
	}
	,renderFrame: function() {
		if(this.vhssHostInstance != null) {
			var time = this.timerAcc.getTime();
			this.frameUpdate.beforeRender(time);
			var mousePoint = this.getMousePoint();
			this.vhssAppState.mouseMoving = this.vhssAppState.canvasMouse == null || this.vhssAppState.canvasMouse.x != mousePoint.x || this.vhssAppState.canvasMouse.y != mousePoint.y;
			this.vhssAppState.canvasMouse = mousePoint;
			if(!this.vhssAppState.bInitialized) {
				var hostMeasurements = this.vhssHostInstance.measureHost();
				if(hostMeasurements != null) {
					this.vhssAppState.interOccularDist = hostMeasurements.interOccularDist;
					this.vhssAppState.halfFaceWidth = hostMeasurements.halfFaceWidth;
					this.vhssAppState.bInitialized = true;
				}
			}
			if(this.vhssAppState.bInitialized) {
				var hitTest = this.vhssHostInstance.getHostMouse(this.vhssAppState.canvasMouse);
				if(hitTest != null) {
					this.vhssAppState.hostMouse = hitTest.localMousePoint;
					this.vhssAppState.bMouseWithinFace = hitTest.isInBounds;
					this.vhssAppState.eyeFrame_0b = this.vhssAnimation.eyeBlink_0b(this.vhssAppState.eyeFrame_0b,this.getFrameInterval());
					if(this.vhssAppState.hostMouse != null) this.gaze.update(this.vhssAppState,this.getFrameInterval(),this.audioManager.isPlaying(),this.bPause);
				}
			}
			if((!this.bPause && !this.bFrozen || this.bFrozenLastFrame) && this.audioManager != null) {
				if(this.bFrozenLastFrame && this.bForceEyesOpenDuringFreeze) this.vhssAppState.eyeFrame_0b = 0;
				this.bFrozenLastFrame = false;
				this.vhssAppState.breathScale = this.vhssAnimation._breathe(this.audioManager.isPlaying(),this.getFrameInterval());
				this.vhssAppState.frame_IntervalMillis = this.getFrameInterval();
				this.hostCanvas.resetMatrix();
				this.hostCanvas.clear();
				if(!this.bPause && !this.bFrozen) {
					if(this.audioManager.isPlaying()) {
						this.vhssAppState.mouthFrame_b0 = this.audioManager.getCurrentSimpleViseme_b0(this.frameUpdate,this.vhssHostInstance.getMouthVersion());
						this.vhssAppState.audioEnergy = this.audioManager.getCurrentCurrentEnergy(this.frameUpdate);
						this.smilerMillis = 750.0;
					} else {
						this.vhssAppState.audioEnergy = 0.0;
						if(this.smilerMillis > 0) {
							this.smilerMillis -= this.getFrameInterval();
							this.vhssAppState.mouthFrame_b0 = this.defaultMouthFrame_b0;
						} else this.vhssAppState.mouthFrame_b0 = this.defaultMouthFrame_b0;
						this.vhssAppState.browFrame_1b = this.debug_defaultBrowFrame_1b;
					}
				}
				var totalScale = this.defaultHostScale * this.userHostScale * this.canvasScale;
				this.hostCanvas.pushPosScaleRot(this.centerHostX + this.centerHostX * this.userHostX * 2.0,this.centerHostY + this.centerHostY * this.userHostY * 2.0,totalScale,totalScale,0.0);
				this.vhssHostInstance.frameUpdate(this.vhssAppState);
				this.vhssHostInstance.draw(this.hostCanvas);
				this.hostCanvas.popMatrix();
				this.flipBuffer();
				if(this.frameRenderedCallback != null) this.frameRenderedCallback(this.vhssAppState.frame_IntervalMillis);
				this.renderDebugInfo();
			}
			this.frameUpdate.afterRender(this.timerAcc.getTime() - time);
		} else {
		}
	}
	,flipBuffer: function() {
	}
	,enableOnscreenDebugging: function(fontSize) {
		this.bOnScreenDebugging = fontSize;
	}
	,renderDebugInfo: function() {
		if(this.hostCanvas != null && this.bOnScreenDebugging > 0) {
			var linespace = this.bOnScreenDebugging;
			this.hostCanvas.fontSize(linespace);
			var line = 0;
			var x = this.centerHostX / 4;
			var y = this.centerHostY / 2;
			this.hostCanvas.strokeText("Canvas Size " + this.hostCanvas.get_width() + "px, " + this.hostCanvas.get_height() + "px",x,y + linespace * line++);
			this.hostCanvas.strokeText("HostPos " + com_oddcast_util_UtilsLite.formatFloat(this.getHostPosX(),4) + ", " + com_oddcast_util_UtilsLite.formatFloat(this.getHostPosY(),4) + " (" + Math.round(this.hostCoordXtoPixel(this.getHostPosX())) + "px, " + Math.round(this.hostCoordYtoPixel(this.getHostPosY())) + "px)",x,y + linespace * line++);
			this.hostCanvas.strokeText("Scale " + com_oddcast_util_UtilsLite.formatFloat(this.getHostScale(),4),x,y + linespace * line++);
		}
	}
	,getMousePoint: function() {
		return null;
	}
	,setFPS: function(fps,isEvent) {
		this.vhssAnimation.setFPS(com_oddcast_util_EnsureType.isFloat(fps,15.0,"fps",{ fileName : "VHSSHostApp.hx", lineNumber : 804, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setFPS"}),com_oddcast_util_EnsureType.isBool(isEvent,false,null,{ fileName : "VHSSHostApp.hx", lineNumber : 804, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setFPS"}));
	}
	,configFromCS: function(cs) {
		this.vhssHostInstance.configFromCS(cs);
	}
	,setScale: function(scaleName,scale) {
		this.vhssHostInstance.setScale(scaleName,com_oddcast_util_EnsureType.isFloat(scale,1.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 814, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setScale"}));
	}
	,getScale: function(scaleName) {
		return this.vhssHostInstance.getScale(scaleName);
	}
	,setColor: function(part,integer) {
		this.vhssHostInstance.setColor(part,com_oddcast_util_EnsureType.isInt(integer,-3618616,null,{ fileName : "VHSSHostApp.hx", lineNumber : 821, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setColor"}));
	}
	,getColor: function(part) {
		return this.vhssHostInstance.getColor(part);
	}
	,isColorable: function(part) {
		return this.vhssHostInstance.isColorable(part);
	}
	,getAlpha: function(part) {
		return this.vhssHostInstance.getAlpha(part);
	}
	,setAlpha: function(part,value) {
		this.vhssHostInstance.setAlpha(part,com_oddcast_util_EnsureType.isFloat(value,1.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 827, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setAlpha"}));
	}
	,isAlphable: function(part) {
		return this.vhssHostInstance.isAlphable(part);
	}
	,setHostPos: function(x,y) {
		x = com_oddcast_util_EnsureType.isFloat(x,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 830, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostPos"});
		y = com_oddcast_util_EnsureType.isFloat(y,0.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 831, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostPos"});
		var PARAMTER_BOUND = 5.0;
		if(Math.abs(x) <= PARAMTER_BOUND && Math.abs(y) <= PARAMTER_BOUND) {
			this.userHostX = x;
			this.userHostY = y;
			haxe_Log.trace("setHostPos(" + x + ", " + y + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 838, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostPos"});
		} else com_oddcast_util_Utils.releaseError("setHostPos valid parameters +-" + PARAMTER_BOUND,{ fileName : "VHSSHostApp.hx", lineNumber : 841, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostPos"});
	}
	,setHostScale: function(scale) {
		scale = com_oddcast_util_EnsureType.isFloat(scale,1.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 845, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostScale"});
		if(scale >= 0.01 && scale <= 10.0) {
			this.userHostScale = scale;
			haxe_Log.trace("setHostScale(" + scale + ")",{ fileName : "VHSSHostApp.hx", lineNumber : 849, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostScale"});
		} else com_oddcast_util_Utils.releaseError("invalid value of " + scale + ". setHostScale valid parameters 0.1 to 10.0",{ fileName : "VHSSHostApp.hx", lineNumber : 852, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setHostScale"});
	}
	,getHostPosX: function() {
		return this.userHostX;
	}
	,getHostPosY: function() {
		return this.userHostY;
	}
	,getHostScale: function() {
		return this.userHostScale;
	}
	,pixelXtoHostCoord: function(pixelX) {
		return (pixelX - this.centerHostX) / this.hostCanvas.get_width();
	}
	,pixelYtoHostCoord: function(pixelY) {
		return (pixelY - this.centerHostY) / this.hostCanvas.get_height();
	}
	,hostCoordXtoPixel: function(hostX) {
		return hostX * this.hostCanvas.get_width() + this.centerHostX;
	}
	,hostCoordYtoPixel: function(hostY) {
		return hostY * this.hostCanvas.get_height() + this.centerHostY;
	}
	,getHostDimensions: function() {
		if(this.vhssHostInstance != null) {
			var rect = this.vhssHostInstance.getBounds(null);
			haxe_Log.trace("HostDimensions:" + Std.string(rect),{ fileName : "VHSSHostApp.hx", lineNumber : 874, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "getHostDimensions"});
			haxe_Log.trace(this.pixelXtoHostCoord(rect.get_left()) + ", " + this.pixelYtoHostCoord(rect.get_top()),{ fileName : "VHSSHostApp.hx", lineNumber : 875, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "getHostDimensions"});
			haxe_Log.trace(this.hostCoordXtoPixel(this.pixelXtoHostCoord(rect.get_left())) + ", " + this.hostCoordYtoPixel(this.pixelYtoHostCoord(rect.get_top())),{ fileName : "VHSSHostApp.hx", lineNumber : 876, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "getHostDimensions"});
			return [this.pixelXtoHostCoord(rect.get_left()),this.pixelXtoHostCoord(rect.get_top()),this.pixelXtoHostCoord(rect.get_right()),this.pixelXtoHostCoord(rect.get_bottom())];
		}
		return null;
	}
	,getAccessoryID: function(typeString) {
		if(this.vhssHostInstance != null) return this.vhssHostInstance.getAccessoryID(typeString);
		return -1;
	}
	,getMouseX: function() {
		return this.getMousePoint().x;
	}
	,getMouseY: function() {
		return this.getMousePoint().y;
	}
	,setBreathRate: function(rate) {
		this.vhssAnimation.setBreathSpeed(rate);
		haxe_Log.trace("breathRate " + rate,{ fileName : "VHSSHostApp.hx", lineNumber : 901, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setBreathRate"});
	}
	,getBreathRate: function() {
		return this.vhssAnimation.getBreathSpeed();
	}
	,setEngineConstant: function(name,value) {
		if(this.vhssHostInstance != null) this.vhssHostInstance.setEngineConstant(name,value);
	}
	,getEngineConstant: function(name) {
		if(this.vhssHostInstance != null) return this.vhssHostInstance.getEngineConstant(name);
		return NaN;
	}
	,registerForFrameRenderedCallback: function(callbackFunction) {
		this.frameRenderedCallback = callbackFunction;
	}
	,calcX: function(x,scale,bNoSkin) {
		var left;
		var right;
		if(bNoSkin) {
			left = this.interp(0.2,28.0,2.53,-677,scale);
			right = this.interp(0.2,251.0,2.53,-454.0,scale);
		} else {
			left = this.interp(0.2,20.0,2.53,-681,scale);
			right = this.interp(0.2,257.0,2.53,-448.0,scale);
		}
		haxe_Log.trace("left:" + left + " right:" + right,{ fileName : "VHSSHostApp.hx", lineNumber : 1050, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "calcX"});
		var ratio = 0.5;
		return this.interp(left,-ratio,right,ratio,x);
	}
	,calcY: function(y,scale,bNoSkin) {
		var top;
		var bottom;
		if(bNoSkin) {
			top = this.interp(0.2,-27.0,2.53,-363.0,scale);
			bottom = this.interp(0.2,270.0,2.53,-67,scale);
		} else {
			top = this.interp(0.2,-22.0,2.53,-355.0,scale);
			bottom = this.interp(0.2,269.0,2.53,-65,scale);
		}
		haxe_Log.trace("top:" + top + " bottom:" + bottom,{ fileName : "VHSSHostApp.hx", lineNumber : 1076, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "calcY"});
		return this.interp(top,-0.5,bottom,0.5,y);
	}
	,interp: function(scale0,x0,scale1,x1,scale) {
		var grad = (x1 - x0) / (scale1 - scale0);
		var con = x0 - scale0 * grad;
		return grad * scale + con;
	}
	,setBlinkInterval: function(seconds) {
		this.vhssAnimation.setBlinkInterval(com_oddcast_util_EnsureType.isInt(seconds,3,null,{ fileName : "VHSSHostApp.hx", lineNumber : 1097, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setBlinkInterval"}));
	}
	,is3D: function() {
		return false;
	}
	,setIdleMovement: function(frequency,amplitude) {
		this.gaze.setIdleMovement(com_oddcast_util_EnsureType.isFloat(frequency,1.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 1104, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setIdleMovement"}),Math.max(Math.min(com_oddcast_util_EnsureType.isFloat(amplitude,50.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 1104, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setIdleMovement"}),100),1));
	}
	,setSpeechMovement: function(amplitude) {
		this.gaze.setSpeechMovement(Math.max(Math.min(com_oddcast_util_EnsureType.isFloat(amplitude,50.0,null,{ fileName : "VHSSHostApp.hx", lineNumber : 1114, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "setSpeechMovement"}),100),1));
	}
	,getBounds: function(accTypeString) {
		var accType;
		if(accTypeString == null) accType = null; else accType = com_oddcast_app_vhss_$extraction_AccTypeTools.fromString(accTypeString);
		return this.vhssHostInstance.getBounds(accType);
	}
	,dispose: function() {
		com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.prototype.dispose.call(this);
		this.unloadPersona();
		this.vhssAnimation = null;
		this.vhssAppState = com_oddcast_util_Disposable.disposeIfValid(this.vhssAppState);
		this.gaze = com_oddcast_util_Disposable.disposeIfValid(this.gaze);
		this.frameUpdate = null;
		this.timerAcc = null;
		this.frameRenderedCallback = null;
	}
	,getFrameInterval: function() {
		return Math.min(100.0,this.frameUpdate.getInterval());
	}
	,setDebug_defaultMouthFrame_b0: function(i) {
		this.defaultMouthFrame_b0 = i;
	}
	,debug_incDefaultMouthFrame: function(inc) {
		this.defaultMouthFrame_b0 = (this.defaultMouthFrame_b0 + inc) % 12;
		haxe_Log.trace("MouthFrame_b0:" + this.defaultMouthFrame_b0,{ fileName : "VHSSHostApp.hx", lineNumber : 1167, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "debug_incDefaultMouthFrame"});
	}
	,debug_incDefaultBrowFrame: function(inc) {
		this.debug_defaultBrowFrame_1b = 1 + (this.debug_defaultBrowFrame_1b - 1 + inc);
		if(this.debug_defaultBrowFrame_1b < 1) this.debug_defaultBrowFrame_1b = 1;
		haxe_Log.trace("BrowFrame_1b:" + this.debug_defaultBrowFrame_1b,{ fileName : "VHSSHostApp.hx", lineNumber : 1176, className : "com.oddcast.app.vhss_extraction.VHSSHostApp", methodName : "debug_incDefaultBrowFrame"});
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSHostApp
});
var com_oddcast_app_vhss_$extraction_VHSSHostAppJS = function() {
	this.lastBrowserUpdateMillis = 0.0;
	this.minUpdateMillis = 16.666666666666668;
	this.lastUpdateTimeMillis = 0.0;
	com_oddcast_app_vhss_$extraction_VHSSHostApp.call(this);
	this.cursorX = 0;
	this.cursorY = 0;
	com_oddcast_util_js_RequestAnimationFramePolyfil.init();
	this.setFPS(30.0,false);
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSHostAppJS"] = com_oddcast_app_vhss_$extraction_VHSSHostAppJS;
com_oddcast_app_vhss_$extraction_VHSSHostAppJS.__name__ = ["com","oddcast","app","vhss_extraction","VHSSHostAppJS"];
com_oddcast_app_vhss_$extraction_VHSSHostAppJS.__super__ = com_oddcast_app_vhss_$extraction_VHSSHostApp;
com_oddcast_app_vhss_$extraction_VHSSHostAppJS.prototype = $extend(com_oddcast_app_vhss_$extraction_VHSSHostApp.prototype,{
	setCanvas: function(canvas) {
		var _g = this;
		com_oddcast_app_vhss_$extraction_VHSSHostApp.prototype.setCanvas.call(this,canvas);
		this.actualHostCanvas = com_oddcast_util_Disposable.disposeIfValid(this.actualHostCanvas);
		if(this.hostCanvas != null && (this.hostCanvas.get_width() < 257 || this.hostCanvas.get_height() < 257)) {
			this.actualHostCanvas = this.hostCanvas;
			this.hostCanvas = new com_oddcast_util_js_JSCanvas("hostBackCanvas",this.actualHostCanvas.get_width() >= 257?this.actualHostCanvas.get_width():257,this.actualHostCanvas.get_height() >= 257?this.actualHostCanvas.get_height():257,null,null,true);
			haxe_Log.trace("actualHostCanvas created: " + this.hostCanvas.get_width() + "*" + this.hostCanvas.get_height() + "  instead of" + this.actualHostCanvas.get_width() + "*" + this.actualHostCanvas.get_height(),{ fileName : "VHSSHostAppJS.hx", lineNumber : 46, className : "com.oddcast.app.vhss_extraction.VHSSHostAppJS", methodName : "setCanvas"});
		}
		window.document.onmousemove = function(e) {
			var useCanvas;
			if(_g.actualHostCanvas != null) useCanvas = _g.actualHostCanvas; else useCanvas = _g.hostCanvas;
			if(useCanvas != null && !useCanvas.canvasEquelTo(null)) {
				var clientRect = useCanvas.getBoundingClientRect();
				_g.cursorX = Math.round(e.pageX - clientRect.left);
				_g.cursorY = Math.round(e.pageY - clientRect.top);
			}
		};
	}
	,flipBuffer: function() {
		if(this.actualHostCanvas != null) {
			this.actualHostCanvas.clear();
			this.actualHostCanvas.drawCanvasIntXYWH_XY(this.hostCanvas,0,0,this.actualHostCanvas.get_width(),this.actualHostCanvas.get_height(),0,0);
		}
	}
	,setFPS: function(fps,isEvent) {
		com_oddcast_app_vhss_$extraction_VHSSHostApp.prototype.setFPS.call(this,fps,isEvent);
		this.minUpdateMillis = 1000 / fps;
		this.lastUpdateTimeMillis = 0;
		this.lastBrowserUpdateMillis = 0;
		haxe_Log.trace("setFPS(" + fps + ", " + (isEvent == null?"null":"" + isEvent) + ")",{ fileName : "VHSSHostAppJS.hx", lineNumber : 93, className : "com.oddcast.app.vhss_extraction.VHSSHostAppJS", methodName : "setFPS"});
		this.animationLoop(0);
	}
	,animationLoop: function(totalTimeMillis) {
		var browserUpdateInterval = totalTimeMillis - this.lastBrowserUpdateMillis;
		this.lastBrowserUpdateMillis = totalTimeMillis;
		window.requestAnimationFrame($bind(this,this.animationLoop));
		if(totalTimeMillis - this.lastUpdateTimeMillis + browserUpdateInterval * 0.5 > this.minUpdateMillis) {
			this.lastUpdateTimeMillis = totalTimeMillis;
			this.renderFrame();
		}
		return true;
	}
	,dispose: function() {
		com_oddcast_app_vhss_$extraction_VHSSHostApp.prototype.dispose.call(this);
		this.actualHostCanvas = com_oddcast_util_Disposable.disposeIfValid(this.actualHostCanvas);
	}
	,getMousePoint: function() {
		return new com_oddcast_neko_geom_Point(this.cursorX,this.cursorY);
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSHostAppJS
});
var com_oddcast_app_vhss_$extraction_VHSSHostInstance = function(hostDataSet,colorQuery) {
	this.hostDataSet = hostDataSet;
	this.configFromCS(colorQuery);
	hostDataSet.initDisplayTree();
	this.vhssAccessories = new com_oddcast_app_vhss_$extraction_VHSSAccessories();
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSHostInstance"] = com_oddcast_app_vhss_$extraction_VHSSHostInstance;
com_oddcast_app_vhss_$extraction_VHSSHostInstance.__name__ = ["com","oddcast","app","vhss_extraction","VHSSHostInstance"];
com_oddcast_app_vhss_$extraction_VHSSHostInstance.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_app_vhss_$extraction_VHSSHostInstance.prototype = {
	configFromCS: function(cs) {
		if(cs != null) this.hostDataSet.primeFromQueryString(cs);
		this.cs = new String(cs);
	}
	,setScale: function(scaleName,scale) {
		this.hostDataSet.setScale(scaleName,scale);
	}
	,getScale: function(scaleName) {
		return this.hostDataSet.getScale(scaleName);
	}
	,setColor: function(part,hex) {
		this.hostDataSet.setColor(part,hex);
	}
	,getColor: function(part) {
		return this.hostDataSet.getColor(part);
	}
	,isColorable: function(part) {
		return this.hostDataSet.isColorable(part);
	}
	,getAlpha: function(part) {
		return this.hostDataSet.getAlpha(part);
	}
	,setAlpha: function(part,value) {
		this.hostDataSet.setAlpha(part,value);
	}
	,isAlphable: function(part) {
		return this.hostDataSet.isAlphable(part);
	}
	,loadAccessory: function(accfrags,fileExtension,completeCallback) {
		var _g = this;
		this.vhssAccessories.loadAccessory(accfrags,fileExtension,this.hostDataSet.root,function(b) {
			_g.hostDataSet.accessoryLoaded();
			if(completeCallback != null) completeCallback(b);
		});
	}
	,frameUpdate: function(vhssAppState) {
		this.hostDataSet.frameUpdate(vhssAppState);
	}
	,draw: function(hostCanvas) {
		this.hostDataSet.draw(hostCanvas);
	}
	,getAccessoryID: function(typeString) {
		if(this.vhssAccessories != null) return this.vhssAccessories.getAccessoryID(typeString);
		return -1;
	}
	,dispose: function() {
		this.hostDataSet = com_oddcast_util_Disposable.disposeIfValid(this.hostDataSet);
		this.vhssAccessories = com_oddcast_util_Disposable.disposeIfValid(this.vhssAccessories);
	}
	,getMouthVersion: function() {
		return this.hostDataSet.mouthVersion_;
	}
	,getHostMouse: function(mouse) {
		return this.hostDataSet.getHostMouse(mouse);
	}
	,measureHost: function() {
		return this.hostDataSet.measureHost();
	}
	,createOHAccString: function() {
		return this.vhssAccessories.createOHAccString();
	}
	,getColorURLstring: function() {
		return this.hostDataSet.getColorURLstring();
	}
	,initAccInUseFromOptimizedHost: function(url) {
		this.vhssAccessories.initAccInUseFromOptimizedHost(url);
	}
	,setHostID: function(id) {
		this.vhssAccessories.setHostID(id);
	}
	,setMouthID: function(id) {
		this.vhssAccessories.setMouthID(id);
	}
	,getBounds: function(accType) {
		var displayAssetNames;
		if(accType == null) displayAssetNames = [["model"]]; else displayAssetNames = com_oddcast_app_vhss_$extraction_VHSSAccessories.getAttachments(accType);
		var totalRect = null;
		var _g = 0;
		while(_g < displayAssetNames.length) {
			var level = displayAssetNames[_g];
			++_g;
			if(level != null) {
				var _g1 = 0;
				while(_g1 < level.length) {
					var name = level[_g1];
					++_g1;
					var displayAsset = this.hostDataSet.root.findDisplayAssetNamed(name);
					if(displayAsset != null) {
						var rect = displayAsset.getBoundsInRoot(true);
						if(rect != null) {
							if(totalRect == null) totalRect = rect; else totalRect = totalRect.union(rect);
						}
					}
				}
			}
		}
		return totalRect;
	}
	,setEngineConstant: function(name,value) {
		if(this.hostDataSet != null) this.hostDataSet.setEngineConstant(name,value);
	}
	,getEngineConstant: function(name) {
		if(this.hostDataSet != null) return this.hostDataSet.getEngineConstant(name);
		return NaN;
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSHostInstance
};
var com_oddcast_haxeSwf_IHaxeSwf = function() { };
$hxClasses["com.oddcast.haxeSwf.IHaxeSwf"] = com_oddcast_haxeSwf_IHaxeSwf;
com_oddcast_haxeSwf_IHaxeSwf.__name__ = ["com","oddcast","haxeSwf","IHaxeSwf"];
com_oddcast_haxeSwf_IHaxeSwf.prototype = {
	__class__: com_oddcast_haxeSwf_IHaxeSwf
};
var com_oddcast_app_vhss_$extraction_VHSSHostJS = $hx_exports.com.oddcast.app.vhss_extraction.VHSSHostJS = function() {
};
$hxClasses["com.oddcast.app.vhss_extraction.VHSSHostJS"] = com_oddcast_app_vhss_$extraction_VHSSHostJS;
com_oddcast_app_vhss_$extraction_VHSSHostJS.__name__ = ["com","oddcast","app","vhss_extraction","VHSSHostJS"];
com_oddcast_app_vhss_$extraction_VHSSHostJS.__interfaces__ = [com_oddcast_haxeSwf_IHaxeSwf];
com_oddcast_app_vhss_$extraction_VHSSHostJS.main = function() {
};
com_oddcast_app_vhss_$extraction_VHSSHostJS.prototype = {
	initAPI: function() {
		this.vhssHostAppJS = new com_oddcast_app_vhss_$extraction_VHSSHostAppJS();
		this.vhssHostAppJS.setJSCanvas(window.document.getElementById("hostCanvas"));
		return this.vhssHostAppJS;
	}
	,getAPI: function() {
		return this.initAPI();
	}
	,destroy: function() {
		this.vhssHostAppJS.dispose();
		return null;
	}
	,__class__: com_oddcast_app_vhss_$extraction_VHSSHostJS
};
var com_oddcast_io_xmlIO_IXmlIO = function() { };
$hxClasses["com.oddcast.io.xmlIO.IXmlIO"] = com_oddcast_io_xmlIO_IXmlIO;
com_oddcast_io_xmlIO_IXmlIO.__name__ = ["com","oddcast","io","xmlIO","IXmlIO"];
com_oddcast_io_xmlIO_IXmlIO.prototype = {
	__class__: com_oddcast_io_xmlIO_IXmlIO
};
var com_oddcast_io_xmlIO_XmlIOAbstract = function() { };
$hxClasses["com.oddcast.io.xmlIO.XmlIOAbstract"] = com_oddcast_io_xmlIO_XmlIOAbstract;
com_oddcast_io_xmlIO_XmlIOAbstract.__name__ = ["com","oddcast","io","xmlIO","XmlIOAbstract"];
com_oddcast_io_xmlIO_XmlIOAbstract.__interfaces__ = [com_oddcast_io_xmlIO_IXmlIO];
com_oddcast_io_xmlIO_XmlIOAbstract.prototype = {
	saved: function(archive) {
	}
	,loaded: function(allAssetsLoaded,archive,xmlName) {
	}
	,allAssetsLoaded: function() {
	}
	,__class__: com_oddcast_io_xmlIO_XmlIOAbstract
};
var com_oddcast_io_xmlIO_XmlIOVersioned = function() { };
$hxClasses["com.oddcast.io.xmlIO.XmlIOVersioned"] = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_io_xmlIO_XmlIOVersioned.__name__ = ["com","oddcast","io","xmlIO","XmlIOVersioned"];
com_oddcast_io_xmlIO_XmlIOVersioned.__super__ = com_oddcast_io_xmlIO_XmlIOAbstract;
com_oddcast_io_xmlIO_XmlIOVersioned.prototype = $extend(com_oddcast_io_xmlIO_XmlIOAbstract.prototype,{
	setVersion: function(ver) {
		this.version_ = ver;
	}
	,loaded: function(allAssetsLoaded,archive,xmlName) {
		com_oddcast_io_xmlIO_XmlIOAbstract.prototype.loaded.call(this,allAssetsLoaded,archive,xmlName);
	}
	,parse: function(bd) {
		this.version_ = bd.uShort16(this.version_,"version_");
	}
	,__class__: com_oddcast_io_xmlIO_XmlIOVersioned
});
var com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct = function() { };
$hxClasses["com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct"] = com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct;
com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct.__name__ = ["com","oddcast","io","xmlIO","binaryData","IBinaryDataStruct"];
com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct.__interfaces__ = [com_oddcast_io_xmlIO_IXmlIO];
com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct.prototype = {
	__class__: com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct
};
var com_oddcast_app_vhss_$extraction_animation_EngineConstantValues = function() {
	this.defaultMap = new haxe_ds_StringMap();
	this.defaultMap.set("em",0.67);
	this.defaultMap.set("EyeMovementRatio",1);
	this.defaultMap.set("MouthMovementRatio",1);
	this.defaultMap.set("BrowMovementRatio",1);
	this.defaultMap.set("BackHairXDampen",5);
	this.defaultMap.set("BackHairRotationDampen",1.5);
	this.defaultMap.set("YOverallFactor",0.5);
	this.defaultMap.set("YScaleFactor",8);
	this.defaultMap.set("YFeatureMovementFactor",0.35);
	this.defaultMap.set("YEyeMovementRatio",2);
	this.defaultMap.set("YBrowMovementRatio",1);
	this.defaultMap.set("YMouthMovementRatio",2);
	this.defaultMap.set("YNoseMovementRatio",2);
	this.defaultMap.set("YNoseScaleFactor",0);
	this.defaultMap.set("yOffsetEyes",0);
	this.defaultMap.set("YeyeScale",1);
	this.defaultMap.set("YHeadMoveFactor",0);
	this.defaultMap.set("WhiteLineCompensation",0.8);
	this.defaultMap.set("RestrictTotalTurning",1);
	this.defaultMap.set("eyeMaxX",12);
	this.defaultMap.set("eyeMaxY",3);
	this.defaultMap.set("eyeFactorX",25);
	this.defaultMap.set("eyeFactorY",13);
	this.defaultMap.set("r_iris_y",0.0);
	this.defaultMap.set("r_iris_x",0.0);
	this.defaultMap.set("xBound",300);
	this.defaultMap.set("yBound",100);
};
$hxClasses["com.oddcast.app.vhss_extraction.animation.EngineConstantValues"] = com_oddcast_app_vhss_$extraction_animation_EngineConstantValues;
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.__name__ = ["com","oddcast","app","vhss_extraction","animation","EngineConstantValues"];
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.floatMap_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewMapString16Basic(this.floatMap_,$bind(bd,bd.float32),bd,"floatMap_");
	}
	,getFloat: function(param) {
		if(this.floatMap_.exists(param)) {
			var fm = this.floatMap_.get(param);
			if(!isNaN(fm)) return this.floatMap_.get(param);
			haxe_Log.trace(com_oddcast_util_SmartTrace.FUCHSIA + "NaN " + param + " using:" + this.defaultMap.get(param),{ fileName : "EngineConstantValues.hx", lineNumber : 132, className : "com.oddcast.app.vhss_extraction.animation.EngineConstantValues", methodName : "getFloat"});
			return this.defaultMap.get(param);
		}
		return this.defaultMap.get(param);
	}
	,setFloat: function(param,value) {
		if(!this.defaultMap.exists(param)) haxe_Log.trace("[ERROR] engine constant " + param + " does not exist " + value,{ fileName : "EngineConstantValues.hx", lineNumber : 140, className : "com.oddcast.app.vhss_extraction.animation.EngineConstantValues", methodName : "setFloat"});
		this.floatMap_.set(param,value);
	}
	,dispose: function() {
		this.floatMap_ = this.defaultMap = null;
	}
	,__class__: com_oddcast_app_vhss_$extraction_animation_EngineConstantValues
});
var com_oddcast_app_vhss_$extraction_animation_FollowMode = $hxClasses["com.oddcast.app.vhss_extraction.animation.FollowMode"] = { __ename__ : ["com","oddcast","app","vhss_extraction","animation","FollowMode"], __constructs__ : ["FOLLOW_NONE","FOLLOW_RANDOM","FOLLOW_MOUSE","FOLLOW_GAZE","FOLLOW_SPEAKING"] };
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE = ["FOLLOW_NONE",0];
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE.toString = $estr;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE.__enum__ = com_oddcast_app_vhss_$extraction_animation_FollowMode;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM = ["FOLLOW_RANDOM",1];
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM.toString = $estr;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM.__enum__ = com_oddcast_app_vhss_$extraction_animation_FollowMode;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE = ["FOLLOW_MOUSE",2];
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE.toString = $estr;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE.__enum__ = com_oddcast_app_vhss_$extraction_animation_FollowMode;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_GAZE = ["FOLLOW_GAZE",3];
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_GAZE.toString = $estr;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_GAZE.__enum__ = com_oddcast_app_vhss_$extraction_animation_FollowMode;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING = ["FOLLOW_SPEAKING",4];
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING.toString = $estr;
com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING.__enum__ = com_oddcast_app_vhss_$extraction_animation_FollowMode;
com_oddcast_app_vhss_$extraction_animation_FollowMode.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE,com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM,com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE,com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_GAZE,com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING];
var com_oddcast_app_vhss_$extraction_animation_Gaze = function() {
	this.mouseGazePos = new com_oddcast_app_vhss_$extraction_animation_GazePos("mouse");
	this.followGazePos = new com_oddcast_app_vhss_$extraction_animation_GazePos("follow");
	this.currentHeadGazePos = new com_oddcast_app_vhss_$extraction_animation_GazePos("head");
	this.currentEyeGazePos = new com_oddcast_app_vhss_$extraction_animation_GazePos("eye");
	this.lookSpeed = 8;
	this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE,{ fileName : "Gaze.hx", lineNumber : 56, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "new"});
	this.setRandomMovement(true,3000);
	this.followCursor(true);
	this.resetHost();
};
$hxClasses["com.oddcast.app.vhss_extraction.animation.Gaze"] = com_oddcast_app_vhss_$extraction_animation_Gaze;
com_oddcast_app_vhss_$extraction_animation_Gaze.__name__ = ["com","oddcast","app","vhss_extraction","animation","Gaze"];
com_oddcast_app_vhss_$extraction_animation_Gaze.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_app_vhss_$extraction_animation_Gaze.prototype = {
	resetHost: function() {
		this.setSpeechMovement(50.0);
		this.setIdleMovement(1.0,50.0);
		this.resetMouseMillis();
		this.xHeadBound = 1.0;
		this.yHeadBound = 1.0;
	}
	,setEV: function(ev) {
		this.ev = ev;
	}
	,setGaze: function(angle,sec,radius,pageOrigin) {
		if(pageOrigin == null) pageOrigin = false;
		this.setGazePosRadAng(this.followGazePos,radius,angle);
		this.followGazePos.setTimer(sec * 1000);
		this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_GAZE,{ fileName : "Gaze.hx", lineNumber : 92, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "setGaze"});
	}
	,setLookSpeed: function(speedIndex) {
		switch(speedIndex) {
		case 0:
			this.lookSpeed = 8;
			break;
		case 1:
			this.lookSpeed = 4;
			break;
		case 2:
			this.lookSpeed = 2;
			break;
		default:
			this.lookSpeed = 8;
		}
	}
	,followCursor: function(b) {
		this.bFollowCursor = b;
		if(b) {
		} else this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE,{ fileName : "Gaze.hx", lineNumber : 118, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "followCursor"});
	}
	,isFollowingCursor: function() {
		return this.bFollowCursor;
	}
	,setSpeechMovement: function(amplitude) {
		this.fSpeechRadius = (amplitude - 1) * com_oddcast_app_vhss_$extraction_animation_Gaze.DEFAULT_SPEECH_MOVEMENT / 50.0;
	}
	,setIdleMovement: function(frequency,amplitude) {
		var mult;
		if(Math.abs(frequency) > 0.01) mult = 1; else mult = 0;
		this.fIdleRadius = mult * (amplitude - 1) * com_oddcast_app_vhss_$extraction_animation_Gaze.DEFAULT_SPEECH_MOVEMENT / 50.0;
	}
	,setRandomMovement: function(b,delay) {
		if(delay == null) delay = 1.0;
		this.bRandomMovement = b;
		if(this.bRandomMovement) {
			if(this.followMode == com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE) this.followGazePos.setTimer(delay);
		} else {
			this.followGazePos.clearTimer();
			if(this.followMode == com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM) this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE,{ fileName : "Gaze.hx", lineNumber : 154, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "setRandomMovement"});
		}
	}
	,setHeadMovementBoundX: function(x) {
		this.xHeadBound = x;
	}
	,setHeadMovementBoundY: function(y) {
		this.yHeadBound = y;
	}
	,getHeadMovementBoundX: function() {
		return this.xHeadBound;
	}
	,getHeadMovementBoundY: function() {
		return this.yHeadBound;
	}
	,recenter: function() {
		this.followGazePos.reset();
	}
	,updateMouse: function(vhssAppState) {
		if(vhssAppState.hostMouse != null) this.setGazePosXY(this.mouseGazePos,vhssAppState.hostMouse.x * 2,vhssAppState.hostMouse.y * 2);
	}
	,setGazePosRadAng: function(gazePos,radius,angle) {
		this.setGazePosXY(gazePos,com_oddcast_app_vhss_$extraction_animation_GazePos.radAngToX(radius,angle) * 2,com_oddcast_app_vhss_$extraction_animation_GazePos.radAngToY(radius,angle) * 2);
	}
	,setGazePosXY: function(gazePos,x,y) {
		gazePos.setXY(x,y + this.ev.getFloat("yOffsetEyes"),this.ev.getFloat("xBound"),this.ev.getFloat("yBound"));
	}
	,update: function(vhssAppState,intervalMillis,soundEngineBusy,bPause) {
		if(bPause) {
			var _g = this.followMode;
			switch(_g[1]) {
			case 0:
				break;
			case 3:
				this.defaultMode({ fileName : "Gaze.hx", lineNumber : 202, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "update"});
				break;
			case 1:
				break;
			case 2:
				break;
			case 4:
				break;
			}
			return;
		}
		if(soundEngineBusy && !this.isSpeakingMode()) this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING,{ fileName : "Gaze.hx", lineNumber : 212, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "update"});
		this.updateMouse(vhssAppState);
		if(this.followGazePos.checkTimer(intervalMillis)) {
			var _g1 = this.followMode;
			switch(_g1[1]) {
			case 0:case 1:
				this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_RANDOM,{ fileName : "Gaze.hx", lineNumber : 225, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "update"});
				this.thrustRandom(this.fIdleRadius);
				break;
			case 2:case 3:
				this.defaultMode({ fileName : "Gaze.hx", lineNumber : 230, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "update"});
				break;
			case 4:
				this.thrustRandom(this.fSpeechRadius);
				break;
			}
		}
		var useGazePos = this.followGazePos;
		var _g2 = this.followMode;
		switch(_g2[1]) {
		case 0:
			this.followGazePos.reset();
			this.checkFollowMouseCursor(vhssAppState);
			this.checkForSuddenMouseMovement(vhssAppState);
			break;
		case 1:
			this.checkFollowMouseCursor(vhssAppState);
			this.checkForSuddenMouseMovement(vhssAppState);
			break;
		case 2:
			useGazePos = this.mouseGazePos;
			this.checkFollowMouseCursor(vhssAppState);
			break;
		case 3:
			break;
		case 4:
			if(!soundEngineBusy) this.defaultMode({ fileName : "Gaze.hx", lineNumber : 254, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "update"});
			break;
		}
		this.currentHeadGazePos.copy(useGazePos);
		this.currentEyeGazePos.copy(useGazePos);
		var intervalRatio = intervalMillis * 12 / 1000;
		this.calcTurn(vhssAppState,intervalRatio);
		vhssAppState.headTwistDeg_d = this.dampen(vhssAppState.headTwistDeg_d,this.currentHeadGazePos.getTwistDeg(vhssAppState.interOccularDist),4.0,intervalRatio);
		vhssAppState.backhairTwistDeg_d = vhssAppState.headTwistDeg_d / this.ev.getFloat("BackHairRotationDampen");
		this.calcEyeGlance(vhssAppState,intervalRatio,soundEngineBusy);
		this.calcNod(vhssAppState,intervalRatio);
	}
	,isSpeakingMode: function() {
		return this.followMode == com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_SPEAKING;
	}
	,defaultMode: function(posInfo) {
		this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE,posInfo);
	}
	,setFollowMode: function(fm,posInfo) {
		if(this.followMode != fm) {
			this.followMode = fm;
			if(this.followMode == com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_NONE) {
				this.followGazePos.clearTimer();
				if(this.bRandomMovement) this.followGazePos.setTimer(3000);
			}
			var _g = this.followMode;
			switch(_g[1]) {
			case 0:
				this.followModeSpeed = 2;
				break;
			case 1:
				this.followModeSpeed = 2;
				break;
			case 2:
				this.followModeSpeed = 1.0;
				break;
			case 3:
				this.followModeSpeed = 1.0;
				break;
			case 4:
				this.followModeSpeed = 1.0;
				break;
			}
			this.resetMouseMillis();
		}
	}
	,thrustRandom: function(radius) {
		var normalizedAmplitude = radius / com_oddcast_app_vhss_$extraction_animation_Gaze.DEFAULT_SPEECH_MOVEMENT;
		var bound = this.ev.getFloat("yBound") * normalizedAmplitude;
		this.setGazePosXY(this.followGazePos,(this.random(bound * 2) - bound) * 0.5 * 2,(this.random(bound * 2) - bound) * 0.5 * 2);
		this.followGazePos.setTimer(500 + this.followGazePos.distanceTo(this.currentHeadGazePos) * 100 / this.lookSpeed);
	}
	,random: function(mult) {
		return Math.random() * mult;
	}
	,checkFollowMouseCursor: function(vhssAppState) {
		if(this.bFollowCursor) {
			if(vhssAppState.bMouseWithinFace && vhssAppState.mouseMoving) {
				this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE,{ fileName : "Gaze.hx", lineNumber : 339, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "checkFollowMouseCursor"});
				this.followGazePos.clearTimer();
				this.followGazePos.setTimer(2500);
			}
		}
	}
	,checkForSuddenMouseMovement: function(vhssAppState) {
		if(this.bFollowCursor) {
			if(!vhssAppState.mouseMoving) this.millisSinceMouseMoved += vhssAppState.frame_IntervalMillis; else {
				if(this.millisSinceMouseMoved > 3000) {
					this.setFollowMode(com_oddcast_app_vhss_$extraction_animation_FollowMode.FOLLOW_MOUSE,{ fileName : "Gaze.hx", lineNumber : 356, className : "com.oddcast.app.vhss_extraction.animation.Gaze", methodName : "checkForSuddenMouseMovement"});
					this.followGazePos.clearTimer();
					this.followGazePos.setTimer(2500);
				}
				this.resetMouseMillis();
			}
		}
	}
	,resetMouseMillis: function() {
		this.millisSinceMouseMoved = 0.0;
	}
	,calcTurn: function(vhssAppState,intervalRatio) {
		var y_Divisor = 3.33;
		var x_XBound = this.ev.getFloat("xBound");
		if(x_XBound < 100) y_Divisor = 0.2630682 + 0.02157197 * x_XBound - 0.0002045455 * x_XBound * x_XBound + 7.575758E-7 * x_XBound * x_XBound * x_XBound;
		var turn = this.xHeadBound * this.currentHeadGazePos.getTurn() / (300 * y_Divisor);
		if(turn > 0.02) {
			var junk = 1;
		}
		if(turn > 0) {
			vhssAppState.facelScale_d = this.dampen(vhssAppState.facelScale_d,1.0 + turn,4.0,intervalRatio);
			vhssAppState.facerScale_d = 1.0 / vhssAppState.facelScale_d;
		} else {
			vhssAppState.facerScale_d = this.dampen(vhssAppState.facerScale_d,1.0 - turn,4.0,intervalRatio);
			vhssAppState.facelScale_d = 1.0 / vhssAppState.facerScale_d;
		}
		vhssAppState.faceX_d = this.dampen(vhssAppState.faceX_d,turn * 80.0,4.0,intervalRatio);
		vhssAppState.backhairX_d = this.dampen(vhssAppState.backhairX_d,-turn * 17.0,this.ev.getFloat("BackHairXDampen"),intervalRatio);
		var scale = 37.0;
		var em = turn * scale * this.ev.getFloat("em");
		vhssAppState.eyeX_d = this.dampen(vhssAppState.eyeX_d,-em * this.ev.getFloat("EyeMovementRatio"),4.0,intervalRatio);
		vhssAppState.browX_d = this.dampen(vhssAppState.browX_d,-em * 0.85 * this.ev.getFloat("BrowMovementRatio"),4.0,intervalRatio);
		vhssAppState.noseX_d = this.dampen(vhssAppState.noseX_d,-em * 0.50,4.0,intervalRatio);
		vhssAppState.glassesX_d = this.dampen(vhssAppState.glassesX_d,-em * 0.25,4.0,intervalRatio);
		vhssAppState.mouthX_d = this.dampen(vhssAppState.mouthX_d,-em * 0.75 * this.ev.getFloat("MouthMovementRatio"),4.0,intervalRatio);
	}
	,dampen: function(oldValue,newValue,dampFactor,intervalRatio) {
		return oldValue + (newValue - oldValue) * 8 * intervalRatio / (dampFactor * this.lookSpeed * this.followModeSpeed);
	}
	,calcNod: function(vhssAppState,intervalRatio) {
		var nod = this.yHeadBound * 0.5 * this.currentHeadGazePos.getNod(this.ev.getFloat("YOverallFactor") * this.ev.getFloat("YScaleFactor"));
		vhssAppState.faceScaleY_d = this.dampen(vhssAppState.faceScaleY_d,1.0 - nod / 8000.0,4.0,intervalRatio);
		var div = 400.0;
		var yMovement = this.ev.getFloat("YFeatureMovementFactor");
		var scale = (1.0 + yMovement) / div;
		vhssAppState.browY_d = this.dampen(vhssAppState.browY_d,-nod * this.ev.getFloat("YBrowMovementRatio") * 2.5 * 2 * scale,4.0,intervalRatio);
		vhssAppState.eyeY_d = this.dampen(vhssAppState.eyeY_d,-nod * this.ev.getFloat("YEyeMovementRatio") * scale,4.0,intervalRatio);
		vhssAppState.noseScaleY_d = this.dampen(vhssAppState.noseScaleY_d,1.0 - nod * 0.055 * scale,4.0,intervalRatio);
		vhssAppState.noseY_d = this.dampen(vhssAppState.noseY_d,-nod * this.ev.getFloat("YNoseMovementRatio") * scale,4.0,intervalRatio);
		vhssAppState.mouthY_d = this.dampen(vhssAppState.mouthY_d,-nod * this.ev.getFloat("YMouthMovementRatio") * 0.5 * scale,4.0,intervalRatio);
		vhssAppState.faceY_d = this.dampen(vhssAppState.faceY_d,-nod * 0.01 * (1 + this.ev.getFloat("YHeadMoveFactor")),4.0,intervalRatio);
		vhssAppState.backhairY_d = this.dampen(vhssAppState.backhairY_d,nod * 0.02,this.ev.getFloat("BackHairXDampen"),intervalRatio);
	}
	,calcEyeGlance: function(vhssAppState,intervalRatio,isTalking) {
		var eyeballX = this.currentEyeGazePos.x / this.ev.getFloat("eyeFactorX");
		eyeballX -= vhssAppState.faceX_d / 7.0;
		var maxX = this.ev.getFloat("eyeMaxX");
		if(eyeballX < -maxX) eyeballX = -maxX;
		if(eyeballX > maxX) eyeballX = maxX;
		var eyeballY = this.currentEyeGazePos.y / this.ev.getFloat("eyeFactorY");
		var maxY = this.ev.getFloat("eyeMaxY");
		if(eyeballY < -maxY) eyeballY = -maxY;
		if(eyeballY > maxY) eyeballY = maxY;
		if(isTalking) eyeballX = eyeballY = 0;
		var a2 = maxX * maxX;
		var b2 = maxY * maxY;
		var x2 = eyeballX * eyeballX;
		var y2 = eyeballY * eyeballY;
		var lengthSqr = x2 / a2 + y2 / b2;
		if(lengthSqr > 1) {
			var normalize = Math.sqrt(lengthSqr);
			eyeballX /= normalize;
			eyeballY /= normalize;
		}
		vhssAppState.eyeballX_d = this.dampen(vhssAppState.eyeballX_d,eyeballX + this.ev.getFloat("r_iris_x"),1.3333333333333333,intervalRatio);
		vhssAppState.eyeballY_d = this.dampen(vhssAppState.eyeballY_d,eyeballY + this.ev.getFloat("r_iris_y"),1.3333333333333333,intervalRatio);
	}
	,dispose: function() {
		this.currentHeadGazePos = null;
		this.currentEyeGazePos = null;
		this.followGazePos = null;
		this.ev = null;
	}
	,__class__: com_oddcast_app_vhss_$extraction_animation_Gaze
};
var com_oddcast_app_vhss_$extraction_animation_GazePos = function(name) {
	this.name = name;
	this.clearTimer();
	this.reset();
};
$hxClasses["com.oddcast.app.vhss_extraction.animation.GazePos"] = com_oddcast_app_vhss_$extraction_animation_GazePos;
com_oddcast_app_vhss_$extraction_animation_GazePos.__name__ = ["com","oddcast","app","vhss_extraction","animation","GazePos"];
com_oddcast_app_vhss_$extraction_animation_GazePos.xyToAngRad = function(x,y) {
	var radius = Math.sqrt(x * x + y * y);
	var angle = com_oddcast_cv_util_Radians.toDegrees(Math.atan2(y,x));
	angle += 90;
	if(angle < 0) angle = 360 + angle;
	return [angle,radius];
};
com_oddcast_app_vhss_$extraction_animation_GazePos.radAngToX = function(radius,angle) {
	return radius * Math.sin(com_oddcast_cv_util_Radians.toRadians(angle));
};
com_oddcast_app_vhss_$extraction_animation_GazePos.radAngToY = function(radius,angle) {
	return -radius * Math.cos(com_oddcast_cv_util_Radians.toRadians(angle));
};
com_oddcast_app_vhss_$extraction_animation_GazePos.prototype = {
	reset: function() {
		this.x = 0.0;
		this.y = 0.0;
	}
	,setTimer: function(millis) {
		this.countDownMillis = millis;
	}
	,clearTimer: function() {
		this.countDownMillis = -1;
	}
	,setXY: function(X,Y,nXBound,nYBound) {
		if(Math.abs(X) > nXBound) {
			if(X > 0) this.x = nXBound; else this.x = -nXBound;
		} else this.x = X;
		if(Math.abs(Y) > nYBound) {
			if(Y > 0) this.y = nYBound; else this.y = -nYBound;
		} else this.y = Y;
	}
	,distanceTo: function(other) {
		var deltaX = this.x - other.x;
		var deltaY = this.y - other.y;
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}
	,copy: function(other) {
		this.x = other.x;
		this.y = other.y;
	}
	,checkTimer: function(intervalMillis) {
		if(this.countDownMillis >= 0) {
			this.countDownMillis -= intervalMillis;
			return this.countDownMillis < 0;
		}
		return false;
	}
	,getNod: function(totalYScaleFactor) {
		return -this.y * totalYScaleFactor;
	}
	,getTurn: function() {
		return this.x;
	}
	,getTwistDeg: function(interOccular) {
		return this.x * this.y / 4000;
	}
	,toString: function() {
		return " x:" + Math.round(this.x) + " y:" + Math.round(this.y);
	}
	,isEquelTo: function(other,epsilon) {
		if(epsilon == null) epsilon = 0.01;
		return Math.abs(this.x - other.x) < epsilon && Math.abs(this.y - other.y) < epsilon;
	}
	,radius: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	,angle: function() {
		return com_oddcast_cv_util_Radians.toDegrees(Math.atan2(this.x,-this.y));
	}
	,__class__: com_oddcast_app_vhss_$extraction_animation_GazePos
};
var com_oddcast_app_vhss_$extraction_animation_VHSSanimation = function() {
	this._nAnimationFPS = 30;
	this._bIsFrozen = false;
	this._bStopRandom = false;
	this.myRandom = new com_oddcast_util_Random();
	this.blinkState = 0;
	this.blinkTimerMillis = 8000;
	this.resetBlinkTimer();
};
$hxClasses["com.oddcast.app.vhss_extraction.animation.VHSSanimation"] = com_oddcast_app_vhss_$extraction_animation_VHSSanimation;
com_oddcast_app_vhss_$extraction_animation_VHSSanimation.__name__ = ["com","oddcast","app","vhss_extraction","animation","VHSSanimation"];
com_oddcast_app_vhss_$extraction_animation_VHSSanimation.prototype = {
	modelReady: function(hostDataSet) {
		this.setBreathSpeed(hostDataSet.breathRate_);
		this._nBreathSign = 1;
		this._nBreath = 0.0;
	}
	,setBreathSpeed: function(speed) {
		this._nBreathspeed = speed;
		if(speed < 0.001) this._nBreath = 0.0;
	}
	,getBreathSpeed: function() {
		return this._nBreathspeed;
	}
	,setFPS: function(fps,isEvent) {
		this._nAnimationFPS = Math.round(fps);
	}
	,_breathe: function(soundEngineBusy,intervalMillis) {
		if(this._nBreath > 5) if(soundEngineBusy) this._nBreathSign = -1; else this._nBreathSign = -0.5; else if(this._nBreath < 0) if(soundEngineBusy) this._nBreathSign = 1; else this._nBreathSign = 0.5;
		this._nBreath += this._nBreathspeed * this._nBreathSign * intervalMillis * 12 / 1000;
		return 1.0 + this._nBreath / 100;
	}
	,setBlinkInterval: function(seconds) {
		this.blinkTimerMillis = 1000 * seconds;
		if(this.blinkState == 0) this.resetBlinkTimer();
	}
	,resetBlinkTimer: function() {
		if(this.blinkTimerMillis == 0) {
			this.blinkCountDownMillis = -1;
			this.blinkState = 0;
		} else {
			var maxBlinkTime = 2 * this.blinkTimerMillis;
			var bias = maxBlinkTime / 8;
			this.blinkCountDownMillis = Math.round(bias + this.random(maxBlinkTime - bias));
		}
	}
	,eyeBlink_0b: function(currentFrame_0b,intervalMillis) {
		if(!this._bIsFrozen && !this._bStopRandom) {
			if(this.blinkCountDownMillis >= 0) this.blinkCountDownMillis -= intervalMillis;
			var _g = this.blinkState;
			switch(_g) {
			case 0:
				if(this.blinkCountDownMillis < 0 && this.blinkTimerMillis > 0) {
					this.blinkState = 1;
					this.blinkCountDownMillis = 83.333333333333329;
				}
				break;
			case 1:
				if(this.blinkCountDownMillis < 0) {
					this.blinkState = 2;
					this.blinkCountDownMillis = 83.333333333333329;
				}
				break;
			case 2:
				if(this.blinkCountDownMillis < 0) {
					this.blinkState = 3;
					this.blinkCountDownMillis = 83.333333333333329;
				}
				break;
			case 3:
				if(this.blinkCountDownMillis < 0) {
					this.blinkState = 0;
					this.resetBlinkTimer();
				}
				break;
			}
		}
		var _g1 = this.blinkState;
		switch(_g1) {
		case 0:
			return 0;
		case 1:case 3:
			return 1;
		case 2:
			return 2;
		default:
			return -1;
		}
	}
	,random: function(i) {
		var ret = Math.floor(this.myRandom.randomFloat() * Math.floor(i));
		return ret;
	}
	,__class__: com_oddcast_app_vhss_$extraction_animation_VHSSanimation
};
var com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils = function() { };
$hxClasses["com.oddcast.app.vhss_extraction.color.ColorAnalyzerHX_Utils"] = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils;
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.__name__ = ["com","oddcast","app","vhss_extraction","color","ColorAnalyzerHX_Utils"];
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.decodeURLHex = function(hex) {
	var n = Std.parseInt(hex);
	if(n == null) haxe_Log.trace("[ERROR]" + n + ":" + hex,{ fileName : "ColorAnalyzerHX_Utils.hx", lineNumber : 24, className : "com.oddcast.app.vhss_extraction.color.ColorAnalyzerHX_Utils", methodName : "decodeURLHex"});
	var bin = n & 7;
	var to = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(n >> 4);
	if((bin & 1) != 0) to.r *= -1;
	if((bin & 2) != 0) to.g *= -1;
	if((bin & 4) != 0) to.b *= -1;
	return to;
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.encodeURLHex = function(r,g,b) {
	var bn = 0;
	if(r < 0) bn += 1;
	if(g < 0) bn += 2;
	if(b < 0) bn += 4;
	return com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.transformToHex(Math.abs(r),Math.abs(g),Math.abs(b)) << 4 | bn;
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.transformToHex = function(r,g,b,offset) {
	if(offset == null) offset = 0;
	if(isNaN(offset)) offset = 0.0;
	r += offset;
	g += offset;
	b += offset;
	var ir = Math.round(Math.max(0,Math.min(255,r)));
	var ig = Math.round(Math.max(0,Math.min(255,g)));
	var ib = Math.round(Math.max(0,Math.min(255,b)));
	return ir << 16 | ig << 8 | ib;
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.calcTransform = function(desiredColor,baseColor) {
	var desiredRGB = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(desiredColor);
	var baseRGB = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(baseColor);
	var t = [desiredRGB.r - baseRGB.r,desiredRGB.g - baseRGB.g,desiredRGB.b - baseRGB.b];
	com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.forceRange(t,-255,255);
	return new com_oddcast_util_js_geom_ColorTransform(1.0,1.0,1.0,1.0,t[0],t[1],t[2],0.0);
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB = function(hex) {
	var r = (hex & 16711680) >> 16;
	var g = (hex & 65280) >> 8;
	var b = hex & 255;
	return { r : r, g : g, b : b};
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.rgbToHex = function(rgb) {
	return (rgb.r << 16) + (rgb.g << 8) + rgb.b;
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.forceRange = function(rgb,nmin,nmax) {
	var _g1 = 0;
	var _g = rgb.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(rgb[i] < nmin) rgb[i] = nmin; else if(rgb[i] > nmax) rgb[i] = nmax;
	}
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.RGBtoArray = function(rgb) {
	return [rgb.r,rgb.g,rgb.b];
};
com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.ArrayToRGB = function(a) {
	return { r : a[0], g : a[1], b : a[2]};
};
var com_oddcast_app_vhss_$extraction_color_ColorGroup = $hxClasses["com.oddcast.app.vhss_extraction.color.ColorGroup"] = { __ename__ : ["com","oddcast","app","vhss_extraction","color","ColorGroup"], __constructs__ : ["ColorGroup_EYES","ColorGroup_HAIR","ColorGroup_MOUTH","ColorGroup_SKIN","ColorGroup_MAKEUP"] };
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_EYES = ["ColorGroup_EYES",0];
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_EYES.toString = $estr;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_EYES.__enum__ = com_oddcast_app_vhss_$extraction_color_ColorGroup;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR = ["ColorGroup_HAIR",1];
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR.toString = $estr;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR.__enum__ = com_oddcast_app_vhss_$extraction_color_ColorGroup;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH = ["ColorGroup_MOUTH",2];
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH.toString = $estr;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH.__enum__ = com_oddcast_app_vhss_$extraction_color_ColorGroup;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_SKIN = ["ColorGroup_SKIN",3];
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_SKIN.toString = $estr;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_SKIN.__enum__ = com_oddcast_app_vhss_$extraction_color_ColorGroup;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MAKEUP = ["ColorGroup_MAKEUP",4];
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MAKEUP.toString = $estr;
com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MAKEUP.__enum__ = com_oddcast_app_vhss_$extraction_color_ColorGroup;
com_oddcast_app_vhss_$extraction_color_ColorGroup.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_EYES,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_SKIN,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MAKEUP];
var com_oddcast_app_vhss_$extraction_color_AlphaGroup = $hxClasses["com.oddcast.app.vhss_extraction.color.AlphaGroup"] = { __ename__ : ["com","oddcast","app","vhss_extraction","color","AlphaGroup"], __constructs__ : ["AlphaGroup_BLUSH","AlphaGroup_MAKEUP"] };
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH = ["AlphaGroup_BLUSH",0];
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH.toString = $estr;
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH.__enum__ = com_oddcast_app_vhss_$extraction_color_AlphaGroup;
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP = ["AlphaGroup_MAKEUP",1];
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP.toString = $estr;
com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP.__enum__ = com_oddcast_app_vhss_$extraction_color_AlphaGroup;
com_oddcast_app_vhss_$extraction_color_AlphaGroup.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH,com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP];
var com_oddcast_app_vhss_$extraction_color_RGB_$Total = function(index) {
	this.r = this.g = this.b = this.total = 0.0;
	this.index = index;
	this.displayAssetsInAverage = [];
};
$hxClasses["com.oddcast.app.vhss_extraction.color.RGB_Total"] = com_oddcast_app_vhss_$extraction_color_RGB_$Total;
com_oddcast_app_vhss_$extraction_color_RGB_$Total.__name__ = ["com","oddcast","app","vhss_extraction","color","RGB_Total"];
com_oddcast_app_vhss_$extraction_color_RGB_$Total.prototype = {
	addDisplayAsset: function(displayAsset) {
		this.displayAssetsInAverage.push(displayAsset);
	}
	,setAverageColoring: function(debugName) {
		var averageColorBase = new com_oddcast_app_vhss_$extraction_color_ColorBase();
		averageColorBase.baseColor_ = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.rgbToHex(this.averageRGB());
		var _g = 0;
		var _g1 = this.displayAssetsInAverage;
		while(_g < _g1.length) {
			var displayAsset = _g1[_g];
			++_g;
			if(displayAsset.imageAssets_ != null && displayAsset.imageAssets_.colorBase_ != null) displayAsset.imageAssets_.colorBase_.averageColorBase = averageColorBase;
		}
	}
	,sumColoring: function(baseRGB,baseWeight) {
		this.r += baseRGB.r * baseWeight;
		this.g += baseRGB.g * baseWeight;
		this.b += baseRGB.b * baseWeight;
		this.total += baseWeight;
	}
	,add: function(from) {
		this.r += from.r;
		this.g += from.g;
		this.b += from.b;
		this.total += from.total;
	}
	,averageRGB: function() {
		var baser = this.r / this.total;
		var baseg = this.g / this.total;
		var baseb = this.g / this.total;
		return { r : Math.round(baser), g : Math.round(baseg), b : Math.round(baseb)};
	}
	,toString: function() {
		return " i:" + this.index + " r:" + com_oddcast_util_UtilsLite.formatFloat(this.r) + " g:" + com_oddcast_util_UtilsLite.formatFloat(this.g) + " b:" + com_oddcast_util_UtilsLite.formatFloat(this.b) + " total:" + com_oddcast_util_UtilsLite.formatFloat(this.total);
	}
	,__class__: com_oddcast_app_vhss_$extraction_color_RGB_$Total
};
var com_oddcast_app_vhss_$extraction_color_ColorBase = function() {
	this.averageColorBase = null;
};
$hxClasses["com.oddcast.app.vhss_extraction.color.ColorBase"] = com_oddcast_app_vhss_$extraction_color_ColorBase;
com_oddcast_app_vhss_$extraction_color_ColorBase.__name__ = ["com","oddcast","app","vhss_extraction","color","ColorBase"];
com_oddcast_app_vhss_$extraction_color_ColorBase.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_color_ColorBase.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_color_ColorBase.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.baseWeight_ = bd.float32(this.baseWeight_,"baseWeight_");
		this.baseColor_ = bd.int32(this.baseColor_,"baseColor_");
	}
	,sumColoring: function(rgbTotal,weight) {
		if(this.baseWeight_ > 0.0) {
			var baseRGB = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(this.baseColor_);
			rgbTotal.sumColoring(baseRGB,this.baseWeight_ * weight);
		}
	}
	,adjustColorTransform: function(colorTransform) {
		if(this.averageColorBase != null) return this.averageColorBase.adjustColorTransform(colorTransform); else {
			var baseRGB = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(this.baseColor_);
			return new com_oddcast_util_js_geom_ColorTransform(colorTransform.redMultiplier,colorTransform.greenMultiplier,colorTransform.blueMultiplier,colorTransform.alphaMultiplier,colorTransform.redOffset - baseRGB.r,colorTransform.greenOffset - baseRGB.g,colorTransform.blueOffset - baseRGB.b,colorTransform.alphaOffset);
		}
	}
	,toString: function() {
		var avString;
		if(this.averageColorBase != null) avString = " av:" + this.averageColorBase.toString(); else avString = "";
		return "bCol:0x" + StringTools.hex(this.baseColor_,8) + " w:" + Math.round(this.baseWeight_) + avString;
	}
	,__class__: com_oddcast_app_vhss_$extraction_color_ColorBase
});
var com_oddcast_app_vhss_$extraction_color_Coloring = function() {
};
$hxClasses["com.oddcast.app.vhss_extraction.color.Coloring"] = com_oddcast_app_vhss_$extraction_color_Coloring;
com_oddcast_app_vhss_$extraction_color_Coloring.__name__ = ["com","oddcast","app","vhss_extraction","color","Coloring"];
com_oddcast_app_vhss_$extraction_color_Coloring.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_color_Coloring.str2ColorGroup = function(colorGroupStr) {
	var colorGroupStr1 = StringTools.replace(colorGroupStr,"-","");
	return com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_color_ColorGroup,"ColorGroup_" + colorGroupStr1.toUpperCase());
};
com_oddcast_app_vhss_$extraction_color_Coloring.str2AlphaGroup = function(alphaGroupStr) {
	var alphaGroupStr1 = StringTools.replace(alphaGroupStr,"-","");
	return com_oddcast_util_EnumTools.fromString(com_oddcast_app_vhss_$extraction_color_AlphaGroup,"AlphaGroup_" + alphaGroupStr1.toUpperCase());
};
com_oddcast_app_vhss_$extraction_color_Coloring.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_color_Coloring.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	setGroupFromString: function(colorGroupStr) {
		this.colorGroup_ = com_oddcast_app_vhss_$extraction_color_Coloring.str2ColorGroup(colorGroupStr);
		this.colorGroupStr = colorGroupStr;
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.colorGroup_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.enumer(this.colorGroup_,com_oddcast_app_vhss_$extraction_color_ColorGroup,bd,"colorGroup_");
	}
	,toString: function() {
		return com_oddcast_util_EnumTools.toString(this.colorGroup_);
	}
	,__class__: com_oddcast_app_vhss_$extraction_color_Coloring
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset = function() {
	this.coloringPartIndex = 0;
	this.children = [];
	this.addMatrix("base");
	this.currFrame = 0;
	this.treeDepth = 0;
	this.noRender = false;
	this.blendMode_ = 0;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_display_DisplayAsset.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	addMatrix: function(name) {
		var mll = new com_oddcast_app_vhss_$extraction_display_MatrixLL(name);
		if(this.matrixLL == null) this.matrixLL = mll; else this.matrixLL.addToEnd(mll);
		return mll.getMat();
	}
	,setBaseMatrix: function(m) {
		this.matrixLL.setMat(m);
	}
	,getBaseMatrix: function() {
		return this.matrixLL.getMat();
	}
	,getTotalMatrix: function() {
		return this.matrixLL.concat(new com_oddcast_util_js_geom_Matrix());
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.name = bd.string16(this.name,"name");
		this.coloring_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(this.coloring_,function() {
			return new com_oddcast_app_vhss_$extraction_color_Coloring();
		},bd,"coloring_");
		this.alphaGroup_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.enumer(this.alphaGroup_,com_oddcast_app_vhss_$extraction_color_AlphaGroup,bd,"alphaGroup_");
		this.matrixLL.parse(bd);
		this.imageAssets_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(this.imageAssets_,function() {
			return new com_oddcast_app_vhss_$extraction_display_ImageAssets();
		},bd,"imageAssets_");
		this.children = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse(this.children,null,bd,"children");
		if(this.version_ >= 2) this.blendMode_ = bd.int32(this.blendMode_,"blendMode_");
		if(!bd.isWrite()) {
			var _g = 0;
			var _g1 = this.children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.parent = this;
			}
		}
	}
	,getTransformFrom: function(otherDA) {
		var d1 = this;
		var d2 = otherDA;
		if(d1.treeDepth != d2.treeDepth) {
			var lowestTreeDepth;
			if(d1.treeDepth < d2.treeDepth) lowestTreeDepth = d1.treeDepth; else lowestTreeDepth = d2.treeDepth;
			while(d1.treeDepth > lowestTreeDepth) d1 = d1.parent;
			while(d2.treeDepth > lowestTreeDepth) d2 = d2.parent;
		}
		while(d1 != d2) {
			d1 = d1.parent;
			d2 = d2.parent;
		}
		var ancestor = d1;
		d1 = this;
		var m1 = new com_oddcast_util_js_geom_Matrix();
		while(d1 != ancestor) {
			m1.concat(d1.getTotalMatrix());
			d1 = d1.parent;
		}
		m1.invert();
		d2 = otherDA;
		var m2 = new com_oddcast_util_js_geom_Matrix();
		while(d2 != ancestor) {
			m2.concat(d2.getTotalMatrix());
			d2 = d2.parent;
		}
		m1.concat(m2);
		return m1;
	}
	,afterRead: function(afterReadData) {
	}
	,setDepths: function() {
		this.spanDisplay(function(da2) {
			if(da2.parent != null) da2.treeDepth = da2.parent.treeDepth + 1;
			return false;
		});
	}
	,isSameColoringGroup: function(other) {
		if(other.coloring_ != null) return this.isOfColorGroup(other.coloring_.colorGroup_);
		return false;
	}
	,isOfColorGroup: function(cg) {
		if(this.coloring_ != null) return this.coloring_.colorGroup_ == cg;
		return false;
	}
	,sumColoring: function(cg,rgbTotals,forceTrue,usedColorIndex) {
		if(usedColorIndex == null) usedColorIndex = 0;
		if(forceTrue == null) forceTrue = false;
		if(forceTrue || this.isOfColorGroup(cg)) {
			forceTrue = true;
			if(cg == com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR && this.coloringPartIndex == 0) {
				var junk = 1;
			}
			if(this.coloringPartIndex > usedColorIndex) usedColorIndex = this.coloringPartIndex; else usedColorIndex = usedColorIndex;
			this.sumColoringGroup(rgbTotals,usedColorIndex);
		}
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.sumColoring(cg,rgbTotals,forceTrue,usedColorIndex);
		}
	}
	,sumColoringGroup: function(rgbTotals,usedColorIndex) {
		if(rgbTotals != null) {
			if(rgbTotals[usedColorIndex] == null) rgbTotals[usedColorIndex] = new com_oddcast_app_vhss_$extraction_color_RGB_$Total(usedColorIndex);
			var rgbTotal = rgbTotals[usedColorIndex];
			rgbTotal.addDisplayAsset(this);
			if(this.imageAssets_ != null) this.imageAssets_.sumColoring(rgbTotal,this.getColorAverageWeighting());
		}
	}
	,getColorAverageWeighting: function() {
		return 1.0;
	}
	,finalfixup: function(ev) {
	}
	,addChild: function(child,pos) {
		if(pos == null) pos = -1;
		if(pos == -1) this.children.push(child); else this.children.splice(pos,0,child);
		child.parent = this;
		child.treeDepth = this.treeDepth + 1;
	}
	,addChildAfter: function(child,after) {
		var index = 0;
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var sibling = _g1[_g];
			++_g;
			index++;
			if(sibling == after) {
				this.addChild(child,index);
				return true;
			}
		}
		return false;
	}
	,switchChild: function(old,replace) {
		var index = 0;
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var sibling = _g1[_g];
			++_g;
			index++;
			if(sibling == old) {
				this.addChild(replace,index);
				this.removeChild(old);
				return true;
			}
		}
		return false;
	}
	,removeChild: function(rChild) {
		if(HxOverrides.remove(this.children,rChild)) {
			rChild.parent = null;
			rChild.treeDepth = 0;
		}
	}
	,removeChildren: function() {
		var kids = this.children.slice();
		var _g = 0;
		while(_g < kids.length) {
			var kid = kids[_g];
			++_g;
			this.removeChild(kid);
		}
	}
	,getChildByName: function(childName) {
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(child.name == childName) return child;
		}
		return null;
	}
	,initDisplayTree: function() {
	}
	,frameUpdate: function(appState) {
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.frameUpdate(appState);
		}
	}
	,frameUpdate2: function(appState) {
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.frameUpdate2(appState);
		}
	}
	,pushMatrix: function(canvas) {
		return this.matrixLL.push(canvas);
	}
	,popMatrix: function(canvas) {
		this.matrixLL.pop(canvas);
	}
	,draw: function(canvas,bDEBUG_DrawFlags) {
		if(!this.noRender) {
			this.transformMatrix23 = this.pushMatrix(canvas);
			this.iTransformMatrix23 = null;
			var _g = 0;
			var _g1 = this.children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.draw(canvas,bDEBUG_DrawFlags);
			}
			if(this.name == "model") {
				var junk = 1;
			}
			this.drawImage(canvas,bDEBUG_DrawFlags);
			this.popMatrix(canvas);
		}
	}
	,getCurrImage: function() {
		if(this.hasImage()) return this.imageAssets_.assets[this.currFrame];
		return null;
	}
	,drawImage: function(canvas,bDEBUG_DrawFlags) {
		if(this.hasImage()) {
			if(bDEBUG_DrawFlags == null || bDEBUG_DrawFlags.flagIsClear(1)) {
				var imageAsset = this.getCurrImage();
				if(imageAsset != null) imageAsset.draw(canvas,this.blendMode_);
			}
		}
	}
	,getBoundsInParentSpace: function(bRecursive) {
		if(bRecursive == null) bRecursive = true;
		return this.getBounds(this.matrixLL.concat(new com_oddcast_util_js_geom_Matrix()),bRecursive);
	}
	,getBounds: function(mat,bRecursive) {
		if(bRecursive == null) bRecursive = true;
		if(mat == null) mat = new com_oddcast_util_js_geom_Matrix();
		var rect = null;
		var imageAsset = this.getCurrImage();
		if(imageAsset != null) rect = imageAsset.getBounds();
		if(rect != null) {
			if(mat != null) rect = com_oddcast_util_Matrix23Tools.transformRectangle(mat,rect);
		}
		if(bRecursive) {
			var _g = 0;
			var _g1 = this.children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				var childRect = child.getBounds(child.matrixLL.concat(mat.clone()),true);
				if(childRect != null) {
					if(rect == null) rect = childRect; else rect = rect.union(childRect);
				}
			}
		}
		return rect;
	}
	,getBoundsInRoot: function(bAndChildren) {
		if(bAndChildren == null) bAndChildren = true;
		if(this.transformMatrix23 != null) return this.getBounds(this.transformMatrix23,bAndChildren);
		return null;
	}
	,toString: function() {
		var nImages = 0;
		if(this.hasImage()) nImages = this.imageAssets_.assets.length;
		var g;
		if(this.coloring_ != null) g = " g:" + this.coloring_.toString(); else g = "";
		var a;
		if(this.alphaGroup_ != null) a = " a:" + com_oddcast_util_EnumTools.toString(this.alphaGroup_); else a = "";
		return "" + this.name + " i:" + nImages + g + a + " d:" + this.treeDepth + " c:" + this.coloringPartIndex + " " + com_oddcast_util_UtilsLite.getShortClassName(this) + " tx:" + Math.round(this.getBaseMatrix().tx) + " ty:" + Math.round(this.getBaseMatrix().ty) + " a:" + com_oddcast_util_UtilsLite.formatFloat(this.getBaseMatrix().a,2) + " d:" + com_oddcast_util_UtilsLite.formatFloat(this.getBaseMatrix().d,2);
	}
	,hasImage: function() {
		return this.imageAssets_ != null;
	}
	,loadFromSpriteSheet: function(spriteImages) {
		if(this.hasImage()) {
			var frame = 0;
			var _g = 0;
			var _g1 = this.imageAssets_.assets;
			while(_g < _g1.length) {
				var imageAsset = _g1[_g];
				++_g;
				if(imageAsset != null) imageAsset.loadFromSpriteSheet(spriteImages);
			}
		}
		var _g2 = 0;
		var _g11 = this.children;
		while(_g2 < _g11.length) {
			var child = _g11[_g2];
			++_g2;
			child.loadFromSpriteSheet(spriteImages);
		}
	}
	,effectGroupColors: function(groupColorDelta,groupAlpha,parentColorTransform,bUseColorBase,bOn,editable,applyImmediatly,pad) {
		if(pad == null) pad = " ";
		if(this.coloring_ != null && groupColorDelta.exists(this.coloring_.colorGroup_) || this.alphaGroup_ != null && groupAlpha.exists(this.alphaGroup_)) {
			var alpha;
			if(this.alphaGroup_ != null && groupAlpha.exists(this.alphaGroup_)) alpha = groupAlpha.get(this.alphaGroup_); else alpha = 1.0;
			if(alpha == null || isNaN(alpha)) alpha = 1.0;
			var deltaRGB;
			if(this.coloring_ != null && groupColorDelta.exists(this.coloring_.colorGroup_)) deltaRGB = groupColorDelta.get(this.coloring_.colorGroup_); else deltaRGB = { r : 0, g : 0, b : 0};
			this.colorTransform = new com_oddcast_util_js_geom_ColorTransform(1.0,1.0,1.0,alpha,deltaRGB.r,deltaRGB.g,deltaRGB.b,0.0);
			bOn = true;
		} else this.colorTransform = new com_oddcast_util_js_geom_ColorTransform();
		this.colorTransform.concat(parentColorTransform);
		if(bOn) this.applyColorTransform(bUseColorBase,editable,applyImmediatly);
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.effectGroupColors(groupColorDelta,groupAlpha,this.colorTransform,bUseColorBase,bOn,editable,applyImmediatly,pad + " ");
		}
	}
	,isColorableOrAlphable: function(colorableGroup,alphableGroup,bOn) {
		if(bOn == null) bOn = false;
		if(StringTools.startsWith(this.name,"blush") && alphableGroup == com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH) {
			var junk = 1;
		}
		if(this.coloring_ != null && this.coloring_.colorGroup_ == colorableGroup || this.alphaGroup_ != null && this.alphaGroup_ == alphableGroup) bOn = true;
		if(bOn) {
			if(this.hasImage() && this.colorTransform != null) return true;
		}
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			var ret = child.isColorableOrAlphable(colorableGroup,alphableGroup,bOn);
			if(ret) return true;
		}
		return false;
	}
	,applyColorTransform: function(bUseColorBase,editable,applyImmediatly) {
		if(this.hasImage() && this.colorTransform != null) this.imageAssets_.effectColorTransform(this.colorTransform,editable,bUseColorBase,this.name,applyImmediatly);
	}
	,spanDisplay: function(cb) {
		var stop = cb(this);
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(!stop) stop = child.spanDisplay(cb);
		}
		return stop;
	}
	,spanDisplayChildrenFirst: function(cb) {
		var stop = false;
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(!stop) stop = child.spanDisplayChildrenFirst(cb);
		}
		if(!stop) stop = cb(this);
		return stop;
	}
	,spanDisplayArraySafe: function(cb) {
		var stop = cb(this);
		var children2 = this.children.slice();
		var _g = 0;
		while(_g < children2.length) {
			var child = children2[_g];
			++_g;
			if(!stop) stop = child.spanDisplayArraySafe(cb);
		}
		return stop;
	}
	,getRoot: function() {
		if(this.parent != null) return this.parent.getRoot();
		return this;
	}
	,findDisplayAssetNamed: function(findName) {
		var result = null;
		this.spanDisplay(function(da) {
			if(da.name == findName) {
				result = da;
				return true;
			}
			return false;
		});
		return result;
	}
	,getMouse: function(rootMouse) {
		if(this.transformMatrix23 == null) return null;
		if(this.iTransformMatrix23 == null) {
			this.iTransformMatrix23 = this.transformMatrix23.clone();
			this.iTransformMatrix23.invert();
		}
		return this.iTransformMatrix23.transformPoint(rootMouse);
	}
	,setScale: function(xScale,yScale) {
		this.matrixLL.setScale(xScale,yScale);
	}
	,isNearOne: function(a,error) {
		if(error == null) error = 0.001;
		return a < 1.0 + error && a > 1.0 - error;
	}
	,shiftOrigin: function(x,y) {
		var base = this.getBaseMatrix();
		var scaledX = x / base.a;
		var scaledY = y / base.d;
		if(this.hasImage()) {
			var _g = 0;
			var _g1 = this.imageAssets_.assets;
			while(_g < _g1.length) {
				var imageAsset = _g1[_g];
				++_g;
				if(imageAsset != null) imageAsset.shiftOrigin(-scaledX,-scaledY);
			}
		}
		var _g2 = 0;
		var _g11 = this.children;
		while(_g2 < _g11.length) {
			var child = _g11[_g2];
			++_g2;
			var childMat = child.getBaseMatrix();
			childMat.tx -= scaledX;
			childMat.ty -= scaledY;
		}
		base.tx += x;
		base.ty += y;
	}
	,seperateJaw: function(afterReadData) {
		var crackAvoid = 0.5;
		if(this.hasImage() && this.imageAssets_.assets.length == 1) {
			var mouth = this.getRoot().findDisplayAssetNamed("mouth");
			var origBounds = this.getBounds(null,true);
			if(origBounds != null) {
				var mouthTy = mouth.getBaseMatrix().ty;
				var ty = this.getBaseMatrix().ty;
				var splitY = this.getTransformFrom(mouth).ty - origBounds.y;
				var imageAsset = this.getCurrImage();
				if(imageAsset != null) {
					var jawImageAssets = imageAsset.splitY(splitY,"_jaw");
					if(jawImageAssets != null) {
						this.imageAssets_.assets = [jawImageAssets[0]];
						var newBounds = this.getBounds(null,true);
						var jaw = new com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw();
						jaw.imageAssets_ = new com_oddcast_app_vhss_$extraction_display_ImageAssets();
						jaw.name = this.name + "_jaw";
						this.addChild(jaw);
						jaw.imageAssets_.assets = [jawImageAssets[1]];
						jaw.treeDepth = this.treeDepth + 1;
						jaw.getBaseMatrix().ty = newBounds.height - crackAvoid;
						var jawBounds = jaw.getBounds();
						jaw.shiftOrigin(0,jawBounds.get_top());
						jaw.mouth = mouth;
						jaw.mouthBottom = mouth.getBounds(null,true).get_bottom();
						afterReadData.jawHeight = jaw.getBounds(null,true).height;
						jaw.jawHeight = afterReadData.jawHeight;
					}
				}
			}
		} else if(this.children.length > 0) this.children[0].seperateJaw(afterReadData);
	}
	,replaceImageAssets: function(inImageAsset) {
		this.imageAssets_ = inImageAsset;
	}
	,dispose: function() {
		this.children = com_oddcast_util_Disposable.disposeIterableIfValid(this.children);
		if(this.parent != null) this.parent.removeChild(this);
		this.parent = null;
		this.matrixLL = null;
		this.imageAssets_ = com_oddcast_util_Disposable.disposeIfValid(this.imageAssets_);
		this.colorTransform = null;
		this.treeDepth = -1;
	}
	,setBlendMode: function(bm) {
		this.blendMode_ = bm;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset
});
var com_oddcast_app_vhss_$extraction_display_AutoAniType = $hxClasses["com.oddcast.app.vhss_extraction.display.AutoAniType"] = { __ename__ : ["com","oddcast","app","vhss_extraction","display","AutoAniType"], __constructs__ : ["ANI_NONE","ANI_AUTO","ANI_VISEME"] };
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE = ["ANI_NONE",0];
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE.toString = $estr;
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE.__enum__ = com_oddcast_app_vhss_$extraction_display_AutoAniType;
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_AUTO = ["ANI_AUTO",1];
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_AUTO.toString = $estr;
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_AUTO.__enum__ = com_oddcast_app_vhss_$extraction_display_AutoAniType;
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_VISEME = ["ANI_VISEME",2];
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_VISEME.toString = $estr;
com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_VISEME.__enum__ = com_oddcast_app_vhss_$extraction_display_AutoAniType;
com_oddcast_app_vhss_$extraction_display_AutoAniType.__empty_constructs__ = [com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE,com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_AUTO,com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_VISEME];
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset.call(this);
	this.setAniType(com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_MC"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_MC"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset.prototype,{
	setAniType: function(autoAniType,bOnlyifNone) {
		if(bOnlyifNone == null) bOnlyifNone = false;
		if(!bOnlyifNone || this.autoAniType_ == com_oddcast_app_vhss_$extraction_display_AutoAniType.ANI_NONE) this.autoAniType_ = autoAniType;
	}
	,parse: function(bd) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset.prototype.parse.call(this,bd);
		this.autoAniType_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.enumer(this.autoAniType_,com_oddcast_app_vhss_$extraction_display_AutoAniType,bd,"autoAniType_");
	}
	,frameUpdate: function(appState) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset.prototype.frameUpdate.call(this,appState);
		var _g = this.autoAniType_;
		switch(_g[1]) {
		case 0:
			break;
		case 1:
			if(this.name == "fhairlart" || this.name == "fhairrart") {
				if(this.parent != null && (this.parent.name == "fhairR" || this.parent.name == "fhairL")) {
					if(this.parent.parent != null && (this.parent.parent.name == "facialhairr_attached" || this.parent.parent.name == "facialhairl_attached")) return;
				}
			}
			this.setToFrame_0b(this.currFrame + 1,true,false);
			break;
		case 2:
			if(this.name == "tt" && this.currFrame == 5 && appState.mouthFrame_b0 == 0) {
				var junk = 1;
			}
			this.setToFrame_0b(appState.mouthFrame_b0,false,true);
			break;
		}
	}
	,setToFrame_0b: function(f,loop,allowNulls) {
		if(allowNulls == null) allowNulls = false;
		if(loop == null) loop = false;
		if(this.imageAssets_ != null) {
			var totalFrame = this.imageAssets_.assets.length;
			while(f < 0) if(loop) f = f + totalFrame; else f = 0;
			if(this.hasImage()) while(f >= totalFrame) if(loop) f = f - totalFrame; else f = totalFrame - 1; else f = 0;
			if(allowNulls || this.imageAssets_.assets[f] != null) this.currFrame = f;
		}
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.coloringPartIndex = 4;
	this.currEnergy = 0.0;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_eyebrowArt"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_eyebrowArt"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate: function(appState) {
		var DAMPEN = 3;
		this.currEnergy = this.currEnergy + (appState.audioEnergy - this.currEnergy) * appState.frame_IntervalMillis * 12 / (DAMPEN * 1000);
		var MIN = 0.7;
		this.setToFrame_0b(Math.round(Math.max(this.currEnergy - MIN,0.0) * 4 / (1 - MIN)));
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.translateMatrix = this.addMatrix("translate");
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_Trans"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	shiftX: function(newValue) {
		this.translateMatrix.tx = newValue;
	}
	,shiftY: function(newValue) {
		this.translateMatrix.ty = newValue;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_Left_Trans"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype,{
	parse: function(bd) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.parse.call(this,bd);
		this.isLeft_ = bd.boolean8(this.isLeft_,"isLeft_");
	}
	,shiftX: function(newValue) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.shiftX.call(this,newValue * this.getMultiplicand());
	}
	,getMultiplicand: function() {
		if(this.isLeft_) return -1.0; else return 1.0;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_eye"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_eye"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.shiftX(appState.eyeX_d);
		this.shiftY(appState.eyeY_d);
		this.translateMatrix.d = appState.eyeScaleY_d;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.call(this);
	this.invBase = new com_oddcast_util_js_geom_Matrix();
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_eyeball"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_eyeball"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.invBase.identity();
		this.invBase.concat(this.getBaseMatrix());
		this.invBase.invert();
		this.shiftX(appState.eyeballX_d * this.invBase.a);
		this.shiftY(appState.eyeballY_d * this.invBase.d);
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_brow"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_brow"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.shiftX(appState.browX_d);
		this.shiftY(appState.browY_d);
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_mouth"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_mouth"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.setScale(appState.param_mScale,appState.param_mScale);
		this.shiftX(appState.mouthX_d);
		this.shiftY(appState.mouthY_d);
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_glasses"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_glasses"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.translateMatrix.identity();
		this.shiftX(appState.glassesX_d - appState.browX_d);
		this.shiftY(appState.browY_d / 5.0);
		var scale = 0.35;
		var faceScale;
		if(this.isLeft_) faceScale = appState.facelScale_d; else faceScale = appState.facerScale_d;
		var turner_d;
		if(faceScale > 1.0) turner_d = 1.0 + (faceScale - 1.0) * scale; else turner_d = 1.0 - (1.0 - faceScale) * scale;
		this.translateMatrix.a = turner_d;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.call(this);
	this.DEBUG_RegColor = -256;
	this.isLeft = 0;
	this.scaleable = true;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_nose"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_nose"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype,{
	afterRead: function(afterReadData) {
		var crackAvoid = 0.75;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.afterRead.call(this,afterReadData);
	}
	,makeNoseTip: function(div,crackAvoid) {
		var bounds = this.getBounds(null,true);
		var splitY = bounds.width / div;
		var imageAsset = this.getCurrImage();
		var newImageAssets = imageAsset.splitY(bounds.height - splitY,"_tip");
		if(newImageAssets != null) {
			this.imageAssets_.assets = [newImageAssets[0]];
			var newBounds = this.getBounds(null,true);
			var noseTip = new com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip();
			noseTip.imageAssets_ = new com_oddcast_app_vhss_$extraction_display_ImageAssets();
			noseTip.name = "noseTip";
			this.addChild(noseTip);
			noseTip.imageAssets_.assets = [newImageAssets[1]];
			noseTip.treeDepth = this.treeDepth + 1;
			noseTip.getBaseMatrix().ty = newBounds.height - crackAvoid;
			var tipBounds = noseTip.getBounds();
			noseTip.shiftOrigin(0,tipBounds.get_top());
		}
	}
	,frameUpdate: function(appState) {
		if(this.scaleable) this.setScale(appState.param_nScale,1.0);
		this.translateMatrix.identity();
		this.shiftX(appState.noseX_d);
		this.shiftY(appState.noseY_d);
		this.translateMatrix.d = appState.noseScaleY_d;
		var scale = 0.75;
		if(this.isLeft != 0) {
			var faceScale;
			if(this.isLeft == 1) faceScale = appState.facelScale_d; else faceScale = appState.facerScale_d;
			var turner_d;
			if(faceScale > 1.0) turner_d = 1.0 + (faceScale - 1.0) * scale; else turner_d = 1.0 - (1.0 - faceScale) * scale;
			this.translateMatrix.a = turner_d;
			this.translateMatrix.d = appState.noseScaleY_d;
			this.translateMatrix.c = (appState.facelScale_d - 1) * scale / 2;
		}
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.call(this);
	this.DEBUG_RegColor = -8355712;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_noseTip"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_noseTip"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype,{
	frameUpdate: function(appState) {
		this.translateMatrix.d = 1 / (appState.noseScaleY_d * appState.noseScaleY_d * appState.noseScaleY_d * appState.noseScaleY_d * appState.noseScaleY_d);
		var scale = 1.75;
		this.translateMatrix.c = -(appState.facelScale_d - 1) * scale / 2;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_sock"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_sock"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate: function(appState) {
		this.setToFrame_0b(appState.eyeFrame_0b,false,true);
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_mouth_attached"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_mouth_attached"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.breathingMatrix = this.addMatrix("breathing");
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_body"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_body"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate: function(appState) {
		this.setScale(appState.param_bscale,1.0);
		this.breathingMatrix.d = appState.breathScale;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_accfrag"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_accfrag"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_accmouth"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_accmouth"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth
});
var com_oddcast_app_vhss_$extraction_display_MatrixLL = function(name) {
	this.mat = new com_oddcast_util_js_geom_Matrix();
	this.name = name;
	this.next = null;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.MatrixLL"] = com_oddcast_app_vhss_$extraction_display_MatrixLL;
com_oddcast_app_vhss_$extraction_display_MatrixLL.__name__ = ["com","oddcast","app","vhss_extraction","display","MatrixLL"];
com_oddcast_app_vhss_$extraction_display_MatrixLL.prototype = {
	addToEnd: function(mll) {
		if(this.next == null) this.next = mll; else this.next.addToEnd(mll);
	}
	,push: function(canvas) {
		this.checkNaN();
		var matrix = canvas.pushMatrix(this.mat);
		if(this.next != null) matrix = this.next.push(canvas);
		return matrix;
	}
	,concat: function($with) {
		this.checkNaN();
		var v = this.mat.clone();
		v.concat($with);
		if(this.next != null) v = this.next.concat(v);
		return v;
	}
	,pop: function(canvas) {
		if(this.next != null) this.next.pop(canvas);
		canvas.popMatrix();
		this.checkNaN();
	}
	,getMat: function() {
		return this.mat;
	}
	,setMat: function(m) {
		this.mat = m;
		this.checkNaN();
	}
	,setScale: function(xScale,yScale) {
		this.mat.a = xScale;
		this.mat.d = yScale;
		this.checkNaN();
	}
	,parse: function(bd) {
		this.mat = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.Matrix23parse(this.mat,bd,"matrix");
		this.checkNaN();
	}
	,checkNaN: function() {
		if(isNaN(this.mat.a) || isNaN(this.mat.b) || isNaN(this.mat.c) || isNaN(this.mat.d) || isNaN(this.mat.tx) || isNaN(this.mat.ty)) {
			var junk = 1;
			return true;
		}
		return false;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_MatrixLL
};
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.translateMatrix = this.addMatrix("translate");
	this.DEBUG_RegColor = -1;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_host"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_host"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	finalfixup: function(ev) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.finalfixup.call(this,ev);
		this.centerArt(this.findDisplayAssetNamed("facel"),this.findDisplayAssetNamed("facer"),ev,"face");
		this.centerArt(this.findDisplayAssetNamed("hairl"),this.findDisplayAssetNamed("hair_r"),ev,"hair");
	}
	,centerArt: function(artl,artr,ev,name) {
		if(artr != null && artl != null) {
			var rBounds = artr.getBoundsInParentSpace(true);
			if(rBounds != null) {
				var rBoundsLeft = rBounds.get_left();
				var lBounds = artl.getBoundsInParentSpace(true);
				if(lBounds != null) {
					var lBounds1 = artl.getBoundsInParentSpace(true);
					var lBoundsRight = lBounds1.get_right();
					if(rBoundsLeft > 0 || lBoundsRight < 0) {
						var middleX = (rBoundsLeft + lBoundsRight) / 2;
						artr.shiftOrigin(middleX,0);
						artl.shiftOrigin(middleX,0);
						haxe_Log.trace(com_oddcast_util_SmartTrace.FUCHSIA + name + " Split:" + com_oddcast_util_UtilsLite.formatFloat(middleX),{ fileName : "DisplayAsset_host.hx", lineNumber : 71, className : "com.oddcast.app.vhss_extraction.display.DisplayAsset_host", methodName : "centerArt"});
					}
				}
			}
		}
	}
	,initDisplayTree: function() {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.initDisplayTree.call(this);
		var facer = this.findDisplayAssetNamed("facer");
		var facel = this.findDisplayAssetNamed("facel");
		var hairr = this.findDisplayAssetNamed("hair_r");
		var hairl = this.findDisplayAssetNamed("hairl");
		var facialhairl = this.findDisplayAssetNamed("facialhairl");
		var facialhairr = this.findDisplayAssetNamed("facialhairr");
		var hair_art_l = this.findDisplayAssetNamed("hair_art_l");
		var hair_art_r = this.findDisplayAssetNamed("hair_art_r");
		var hatL = this.findDisplayAssetNamed("hatL");
		var hatR = this.findDisplayAssetNamed("hatR");
	}
	,frameUpdate: function(appState) {
		this.setScale(appState.param_hxscale,appState.param_hyscale);
		var tx = this.translateMatrix.tx;
		this.translateMatrix.identity();
		com_oddcast_util_Matrix23Tools.rotateDegrees(this.translateMatrix,appState.headTwistDeg_d);
		this.translateMatrix.tx = appState.faceX_d;
		this.translateMatrix.ty = appState.faceY_d;
		this.translateMatrix.d = appState.faceScaleY_d;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate.call(this,appState);
	}
	,dispose: function() {
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.turnMatrix = this.addMatrix("turn");
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_turn"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_turn"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.call(this);
	this.isLeft_ = false;
	this.scaleFactor = 1.0;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_SimpleTurn"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_SimpleTurn"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.prototype,{
	parse: function(bd) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.prototype.parse.call(this,bd);
		this.isLeft_ = bd.boolean8(this.isLeft_,"isLeft_");
	}
	,frameUpdate: function(appState) {
		var faceScale;
		if(this.isLeft_) faceScale = appState.facelScale_d; else faceScale = appState.facerScale_d;
		var turner_d;
		if(faceScale > 1.0) turner_d = 1.0 + (faceScale - 1.0) * this.scaleFactor; else turner_d = 1.0 - (1.0 - faceScale) * this.scaleFactor;
		this.turnMatrix.a = turner_d;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.prototype.frameUpdate.call(this,appState);
	}
	,finalfixup: function(ev) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.prototype.finalfixup.call(this,ev);
		var base = this.getBaseMatrix();
		var yShift = 3.0;
		var xShift = 0.0;
		this.shiftOrigin(-base.tx,0.0);
		base.tx += (this.isLeft_?1.0:-1.0) * ev.getFloat("WhiteLineCompensation");
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_face"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_face"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.prototype,{
	afterRead: function(afterReadData) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.prototype.afterRead.call(this,afterReadData);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_jaw"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_jaw"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate2: function(appState) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate2.call(this,appState);
		var mouthBounds = this.mouth.getBounds();
		var extraPixs = mouthBounds.get_bottom() - this.mouthBottom;
		appState.jawScale = 1.0 + 0.5 * extraPixs / this.jawHeight;
		this.getBaseMatrix().d = appState.jawScale;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.coloringPartIndex = 1;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_hairArt"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_hairArt"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.coloringPartIndex = 3;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_facialHair"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_facialHair"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	replaceImageAssets: function(inImageAsset) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.replaceImageAssets.call(this,inImageAsset);
		this.afterRead(this.afterReadData);
	}
	,afterRead: function(afterReadData) {
		this.jaw = com_oddcast_util_Disposable.disposeIfValid(this.jaw);
		this.afterReadData = afterReadData;
		var crackAvoid = 0.75;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.afterRead.call(this,afterReadData);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_facialHairJaw"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_facialHairJaw"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate2: function(appState) {
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate2.call(this,appState);
		this.getBaseMatrix().d = appState.jawScale;
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.getBaseMatrix().d = 1 / appState.jawScale;
		}
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.coloringPartIndex = 1;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_backhairArt"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_backhairArt"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt
});
var com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair = function() {
	com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.call(this);
	this.rotateMatrix = this.addMatrix("rotate");
};
$hxClasses["com.oddcast.app.vhss_extraction.display.DisplayAsset_backhair"] = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair.__name__ = ["com","oddcast","app","vhss_extraction","display","DisplayAsset_backhair"];
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair.__super__ = com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair.prototype = $extend(com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype,{
	frameUpdate: function(appState) {
		this.setScale(appState.param_hxscale,appState.param_hyscale);
		this.rotateMatrix.identity();
		this.rotateMatrix.scale((appState.facelScale_d + appState.facerScale_d) / 2,1.0);
		com_oddcast_util_Matrix23Tools.rotateDegrees(this.rotateMatrix,appState.backhairTwistDeg_d);
		this.rotateMatrix.tx = appState.backhairX_d;
		this.rotateMatrix.ty = appState.backhairY_d;
		com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.prototype.frameUpdate.call(this,appState);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair
});
var com_oddcast_app_vhss_$extraction_display_ImageAssets = function() {
};
$hxClasses["com.oddcast.app.vhss_extraction.display.ImageAssets"] = com_oddcast_app_vhss_$extraction_display_ImageAssets;
com_oddcast_app_vhss_$extraction_display_ImageAssets.__name__ = ["com","oddcast","app","vhss_extraction","display","ImageAssets"];
com_oddcast_app_vhss_$extraction_display_ImageAssets.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_display_ImageAssets.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_display_ImageAssets.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	reset: function() {
		this.assets = [];
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.assets = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse(this.assets,null,bd,"assets");
		this.colorBase_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(this.colorBase_,function() {
			return new com_oddcast_app_vhss_$extraction_color_ColorBase();
		},bd,"colorBase_");
	}
	,sumColoring: function(rgbTotal,weight) {
		if(this.colorBase_ != null) this.colorBase_.sumColoring(rgbTotal,weight);
	}
	,effectColorTransform: function(colorTransform,editable,bUseColorBase,name,applyImmediatly) {
		var ct;
		if(this.colorBase_ != null && bUseColorBase) ct = this.colorBase_.adjustColorTransform(colorTransform); else ct = colorTransform;
		var bFirst = applyImmediatly;
		var _g = 0;
		var _g1 = this.assets;
		while(_g < _g1.length) {
			var imageAsset = _g1[_g];
			++_g;
			if(imageAsset != null) bFirst = imageAsset.effectColorTransform(ct,editable,bFirst);
		}
	}
	,clone: function() {
		var clone = new com_oddcast_app_vhss_$extraction_display_ImageAssets();
		var _g = 0;
		var _g1 = this.assets;
		while(_g < _g1.length) {
			var asset = _g1[_g];
			++_g;
			clone.assets.push(asset.clone());
		}
		return clone;
	}
	,dispose: function() {
		this.assets = com_oddcast_util_Disposable.disposeIterableIfValid(this.assets);
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_ImageAssets
});
var com_oddcast_app_vhss_$extraction_display_ImageAsset = function() {
	this.imageX_ = this.imageY_ = 0.0;
	this.spriteIndex_ = -1;
	this.scale_ = 1.0;
	this.checkScale();
};
$hxClasses["com.oddcast.app.vhss_extraction.display.ImageAsset"] = com_oddcast_app_vhss_$extraction_display_ImageAsset;
com_oddcast_app_vhss_$extraction_display_ImageAsset.__name__ = ["com","oddcast","app","vhss_extraction","display","ImageAsset"];
com_oddcast_app_vhss_$extraction_display_ImageAsset.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_display_ImageAsset.forEachImageAsset = function(imageAssets,cb) {
	var _g = 0;
	var _g1 = imageAssets.assets;
	while(_g < _g1.length) {
		var imageAsset = _g1[_g];
		++_g;
		if(cb(imageAsset)) return;
	}
};
com_oddcast_app_vhss_$extraction_display_ImageAsset.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_display_ImageAsset.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	clone: function(cloneNameSuffix) {
		var clone = new com_oddcast_app_vhss_$extraction_display_ImageAsset();
		clone.imageX_ = this.imageX_;
		clone.imageY_ = this.imageY_;
		clone.scale_ = this.scale_;
		clone.spriteIndex_ = this.spriteIndex_;
		clone.spriteImages = this.spriteImages;
		if(cloneNameSuffix == null) cloneNameSuffix = "_clone";
		clone.name_ = this.name_ + cloneNameSuffix;
		return clone;
	}
	,setOffsetAndScale: function(scaledImageX,scaledImageY,scale) {
		this.imageX_ = scaledImageX / scale;
		this.imageY_ = scaledImageY / scale;
		this.scale_ = scale;
		this.checkScale();
	}
	,shiftOrigin: function(x,y) {
		this.imageX_ += x;
		this.imageY_ += y;
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.imageX_ = bd.float32(this.imageX_,"imageX_");
		this.imageY_ = bd.float32(this.imageY_,"imageY_");
		this.scale_ = bd.float32(this.scale_,"scale_");
		this.name_ = bd.string16(this.name_,"name_");
		this.spriteIndex_ = bd.int32(this.spriteIndex_,"spriteIndex_");
		this.checkScale();
	}
	,checkScale: function() {
		if(this.scale_ == 0.0) throw new js__$Boot_HaxeError("zero scale");
	}
	,loadFromSpriteSheet: function(spriteImages) {
		this.spriteImages = spriteImages;
	}
	,effectColorTransform: function(colorTransform,editable,applyImmediatly) {
		if(this.spriteIndex_ >= 0) return this.spriteImages[this.spriteIndex_].effectColorTransform(colorTransform,editable,applyImmediatly);
		return applyImmediatly;
	}
	,draw: function(canvas,blendMode) {
		if(this.spriteIndex_ >= 0) this.getSpriteImage().draw(canvas,this.imageX_,this.imageY_,1 / this.scale_,blendMode);
	}
	,getSpriteImage: function() {
		return this.spriteImages[this.spriteIndex_];
	}
	,splitY: function(y,splitName) {
		if(y < 0.51) return null;
		var newSpriteImages = this.getSpriteImage().splitY(y * this.scale_,splitName);
		if(newSpriteImages == null) return null;
		var top = this.clone(splitName + "_top");
		top.spriteIndex_ = this.spriteImages.length;
		this.spriteImages.push(newSpriteImages[0]);
		var bot = this.clone(splitName + "_bot");
		bot.spriteIndex_ = this.spriteImages.length;
		this.spriteImages.push(newSpriteImages[1]);
		return [top,bot];
	}
	,splitX: function(x,splitName) {
		if(x < 0) return null;
		var newSpriteImages = this.getSpriteImage().splitX(x * this.scale_,splitName);
		var left = this.clone(splitName + "_left");
		left.spriteIndex_ = this.spriteImages.length;
		this.spriteImages.push(newSpriteImages[0]);
		var rite = this.clone(splitName + "_rite");
		rite.spriteIndex_ = this.spriteImages.length;
		this.spriteImages.push(newSpriteImages[1]);
		return [left,rite];
	}
	,getBounds: function() {
		var bounds = com_oddcast_util_RectangleTools.scale(this.getSpriteImage().getBounds(),1 / this.scale_);
		bounds.x += this.imageX_;
		bounds.y += this.imageY_;
		return bounds;
	}
	,dispose: function() {
		this.spriteImages = null;
		this.name_ = null;
		this.spriteIndex_ = -1;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_ImageAsset
});
var com_oddcast_app_vhss_$extraction_display_SpriteImage = function() {
};
$hxClasses["com.oddcast.app.vhss_extraction.display.SpriteImage"] = com_oddcast_app_vhss_$extraction_display_SpriteImage;
com_oddcast_app_vhss_$extraction_display_SpriteImage.__name__ = ["com","oddcast","app","vhss_extraction","display","SpriteImage"];
com_oddcast_app_vhss_$extraction_display_SpriteImage.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_display_SpriteImage.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_app_vhss_$extraction_display_SpriteImage.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.inSpriteSheetRect_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.rectangleParse(this.inSpriteSheetRect_,bd,"inSpriteSheetRect_");
		this.name_ = bd.string16(this.name_,"name_");
	}
	,loadFromSpriteSheet: function(spriteSheetImage) {
		this.assetWholeImage = spriteSheetImage;
	}
	,effectColorTransform: function(colorTransform,editable,applyImmediatly) {
		this.waitingColorTransform = colorTransform;
		if(applyImmediatly) this._applyColorTransform(this.waitingColorTransform,com_oddcast_app_vhss_$extraction_display_SpriteImage.dontCare);
		return false;
	}
	,_applyColorTransform: function(ct,editable) {
		if(this.clonedFrom != null) this.clonedFrom._applyColorTransform(ct,editable); else if(!com_oddcast_util_ColorTransformTools.isEqualTo(ct,this.currColorTransform)) {
			var x = Math.floor(this.inSpriteSheetRect_.x);
			var y = Math.floor(this.inSpriteSheetRect_.y);
			var w = Math.ceil(this.inSpriteSheetRect_.x + this.inSpriteSheetRect_.width - x);
			var h = Math.ceil(this.inSpriteSheetRect_.y + this.inSpriteSheetRect_.height - y);
			if(this.colorTransformedCanvas == null) this.colorTransformedCanvas = com_oddcast_util_js_JSCanvas.newFromRect(this.assetWholeImage,this.name_ + "_ct",x,y,w,h); else {
				this.colorTransformedCanvas.clear();
				this.colorTransformedCanvas.drawFloatsXsYsWsHdXdY(this.assetWholeImage,x,y,w,h,0,0,w,h);
			}
			this.colorTransformedCanvas.setColorTransform(ct);
			this.currColorTransform = ct;
		}
		this.waitingColorTransform = null;
	}
	,draw: function(canvas,imageX,imageY,invScale,blendMode) {
		if(this.waitingColorTransform != null) this._applyColorTransform(this.waitingColorTransform,com_oddcast_app_vhss_$extraction_display_SpriteImage.dontCare);
		switch(blendMode) {
		case 0:
			break;
		case 1:
			canvas.getContext2D().globalCompositeOperation = "screen";
			break;
		}
		if(this.colorTransformedCanvas != null) canvas.drawFloatXY_Rectangle(this.colorTransformedCanvas,imageX,imageY,null,invScale); else canvas.drawFloatXY_Rectangle(this.assetWholeImage,imageX,imageY,this.inSpriteSheetRect_,invScale);
		if(blendMode != 0) canvas.getContext2D().globalCompositeOperation = "source-over";
	}
	,getBounds: function() {
		return new com_oddcast_neko_geom_Rectangle(0,0,this.inSpriteSheetRect_.width,this.inSpriteSheetRect_.height);
	}
	,clone: function(cloneNameSuffix) {
		var ret = new com_oddcast_app_vhss_$extraction_display_SpriteImage();
		if(cloneNameSuffix == null) cloneNameSuffix = "_clone";
		ret.name_ = this.name_ + cloneNameSuffix;
		ret.inSpriteSheetRect_ = new com_oddcast_neko_geom_Rectangle(this.inSpriteSheetRect_.x,this.inSpriteSheetRect_.y,this.inSpriteSheetRect_.width,this.inSpriteSheetRect_.height);
		ret.assetWholeImage = this.assetWholeImage;
		ret.currColorTransform = this.currColorTransform;
		ret.clonedFrom = this;
		return ret;
	}
	,splitY: function(y,splitName) {
		if(y > this.inSpriteSheetRect_.height) return null;
		var top = this.clone(splitName + "_top");
		var bot = this.clone(splitName + "_bot");
		var split = this.inSpriteSheetRect_.get_top() + y;
		top.inSpriteSheetRect_.set_bottom(split);
		bot.inSpriteSheetRect_.set_top(split);
		return [top,bot];
	}
	,splitX: function(x,splitName) {
		var left = this.clone(splitName + "_left");
		var rite = this.clone(splitName + "_rite");
		var split = this.inSpriteSheetRect_.get_left() + x;
		left.inSpriteSheetRect_.set_right(split);
		rite.inSpriteSheetRect_.set_left(split);
		return [left,rite];
	}
	,dispose: function() {
		this.inSpriteSheetRect_ = null;
		this.name_ = null;
		this.assetWholeImage = null;
		this.currColorTransform = null;
		this.colorTransformedCanvas = com_oddcast_util_Disposable.disposeIfValid(this.colorTransformedCanvas);
		this.clonedFrom = null;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_SpriteImage
});
var com_oddcast_app_vhss_$extraction_display_VHSSappState = function() {
	this.bInitialized = false;
	this.hostMouse = new com_oddcast_neko_geom_Point(0,0);
	this.canvasMouse = new com_oddcast_neko_geom_Point(0,0);
	this.mouseMoving = false;
	this.bMouseWithinFace = false;
	this.eyeFrame_0b = 0;
	this.breathScale = 1.0;
	this.mouthFrame_b0 = 0;
	this.browFrame_1b = 1;
	this.audioEnergy = 0.0;
	this.facelScale_d = 1.0;
	this.facerScale_d = 1.0;
	this.faceScaleY_d = 1.0;
	this.faceX_d = 0.0;
	this.faceY_d = 0.0;
	this.headTwistDeg_d = 0.0;
	this.backhairTwistDeg_d = 0.0;
	this.interOccularDist = 70.0;
	this.halfFaceWidth = 70.0;
	this.eyeballX_d = this.eyeballY_d = 0.0;
	this.eyeY_d = 0.0;
	this.browY_d = 0.0;
	this.noseY_d = 0.0;
	this.mouthY_d = 0.0;
	this.eyeX_d = 0.0;
	this.browX_d = 0.0;
	this.noseX_d = 0.0;
	this.glassesX_d = 0.0;
	this.mouthX_d = 0.0;
	this.backhairX_d = 0.0;
	this.backhairY_d = 0.0;
	this.eyeScaleY_d = 1.0;
	this.noseScaleY_d = 1.0;
	this.jawScale = 1.0;
	this.param_mScale = 1.0;
	this.param_nScale = 1.0;
	this.param_hyscale = 1.0;
	this.param_hxscale = 1.0;
	this.param_bscale = 1.0;
	this.frame_IntervalMillis = 0;
};
$hxClasses["com.oddcast.app.vhss_extraction.display.VHSSappState"] = com_oddcast_app_vhss_$extraction_display_VHSSappState;
com_oddcast_app_vhss_$extraction_display_VHSSappState.__name__ = ["com","oddcast","app","vhss_extraction","display","VHSSappState"];
com_oddcast_app_vhss_$extraction_display_VHSSappState.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_app_vhss_$extraction_display_VHSSappState.prototype = {
	dispose: function() {
		this.hostMouse = this.canvasMouse = null;
	}
	,__class__: com_oddcast_app_vhss_$extraction_display_VHSSappState
};
var com_oddcast_app_vhss_$extraction_dummyData_DummyAccessory = function() {
	this.accList = new haxe_ds_EnumValueMap();
	this.typeIndex = 0;
};
$hxClasses["com.oddcast.app.vhss_extraction.dummyData.DummyAccessory"] = com_oddcast_app_vhss_$extraction_dummyData_DummyAccessory;
com_oddcast_app_vhss_$extraction_dummyData_DummyAccessory.__name__ = ["com","oddcast","app","vhss_extraction","dummyData","DummyAccessory"];
com_oddcast_app_vhss_$extraction_dummyData_DummyAccessory.prototype = {
	loadfromURL: function(url,completeCB,errorCB) {
		var _g = this;
		var r = new haxe_Http(url);
		r.onError = function(msg) {
			errorCB(msg);
		};
		r.onData = function(str) {
			_g.unserialize(str);
			completeCB();
		};
		r.request(false);
	}
	,addFrags: function(frags) {
		if(!this.accList.exists(frags.accType)) {
			var value = new com_oddcast_util_IndexedArray();
			this.accList.set(frags.accType,value);
		}
		this.accList.get(frags.accType).push(frags);
	}
	,getNextOfType: function(inc,accType) {
		var fragIndexArray = this.accList.get(accType);
		if(fragIndexArray != null) return fragIndexArray.increment(inc);
		return null;
	}
	,getNextType: function(inc) {
		this.typeIndex = (this.typeIndex + 1) % Lambda.count(this.accList);
		var j = 0;
		var $it0 = this.accList.keys();
		while( $it0.hasNext() ) {
			var k = $it0.next();
			if(j == this.typeIndex) return k;
			j++;
		}
		return null;
	}
	,getRandomFrags: function() {
		var i = com_oddcast_util_Random.getRandomInt(Lambda.count(this.accList));
		var j = 0;
		var $it0 = this.accList.keys();
		while( $it0.hasNext() ) {
			var k = $it0.next();
			if(j == i) return this.getRandomFragsOfType(k);
			j++;
		}
		return null;
	}
	,getRandomFragsOfType: function(accType) {
		if(this.accList.exists(accType)) return this.accList.get(accType).randomElement();
		return null;
	}
	,serialize: function() {
		return haxe_Serializer.run(this.accList);
	}
	,unserialize: function(s) {
		this.accList = haxe_Unserializer.run(s);
	}
	,__class__: com_oddcast_app_vhss_$extraction_dummyData_DummyAccessory
};
var com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp = $hx_exports.com.oddcast.app.vhss_extraction.mobile_IFrame.Mobile_IFrameApp = function() {
	com_oddcast_app_vhss_$extraction_VHSSHostJS.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.mobile_IFrame.Mobile_IFrameApp"] = com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp;
com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp.__name__ = ["com","oddcast","app","vhss_extraction","mobile_IFrame","Mobile_IFrameApp"];
com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp.main = function() {
};
com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp.__super__ = com_oddcast_app_vhss_$extraction_VHSSHostJS;
com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp.prototype = $extend(com_oddcast_app_vhss_$extraction_VHSSHostJS.prototype,{
	initAPI: function() {
		var retval = com_oddcast_app_vhss_$extraction_VHSSHostJS.prototype.initAPI.call(this);
		return retval;
	}
	,__class__: com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp
});
var com_oddcast_io_xmlIO_XmlIODated = function() { };
$hxClasses["com.oddcast.io.xmlIO.XmlIODated"] = com_oddcast_io_xmlIO_XmlIODated;
com_oddcast_io_xmlIO_XmlIODated.__name__ = ["com","oddcast","io","xmlIO","XmlIODated"];
com_oddcast_io_xmlIO_XmlIODated.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_io_xmlIO_XmlIODated.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	setCreationDate: function() {
		var _this = new Date();
		this.createdDate_ = HxOverrides.dateStr(_this);
	}
	,setCompiledDate: function() {
		this.compiledDate_ = "2016-10-03 11:58:07";
	}
	,parseDates: function(bd) {
		this.createdDate_ = bd.string16(this.createdDate_,"createdDate_");
		this.compiledDate_ = bd.string16(this.compiledDate_,"compiledDate_");
	}
	,__class__: com_oddcast_io_xmlIO_XmlIODated
});
var com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet = function() {
	this.spriteImages_ = [];
	this.scale_ = 1.0;
	this.dataSize = 0;
	this.mamName_ = "";
	this.defaultFileExtension = "ohv2.png";
};
$hxClasses["com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet"] = com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet;
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.__name__ = ["com","oddcast","app","vhss_extraction","rasterdataset","RasterDataSet"];
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.readFromSpriteSheetImage = function(archive,imageName,construct,completeCB,errorCB) {
	archive.loadImage(imageName,null,function(spriteSheetImage) {
		com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.spriteStartTime = com_oddcast_util_UtilsLite.getTime();
		if(archive.getID() == "OpenFL_Assets") {
			var spriteLayoutTxt = null;
			var extension = com_oddcast_util_UtilsLite.getFileExtension(imageName);
			archive.readTextFile(StringTools.replace(imageName,extension,"xml"),function(t) {
				spriteLayoutTxt = t;
			});
			completeCB(com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processSpriteSheetLayoutData(spriteLayoutTxt,construct,0,spriteSheetImage));
		} else {
			haxe_Log.trace("readBinaryFile(" + imageName + ")",{ fileName : "RasterDataSet.hx", lineNumber : 124, className : "com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet", methodName : "readFromSpriteSheetImage"});
			archive.readBinaryFile(imageName,null,function(bd) {
				new com_oddcast_io_pngEncoder_ParsePNGMetadata().loadFromBinaryData(bd,function(metaData) {
					completeCB(com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processMetaData(metaData,bd.totalSize(),spriteSheetImage,construct));
				},null);
			},function(url,errorCode) {
				haxe_Log.trace("readBinaryFile Error:" + errorCode + " " + url,{ fileName : "RasterDataSet.hx", lineNumber : 147, className : "com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet", methodName : "readFromSpriteSheetImage"});
				if(errorCode == "UserAborted") {
					errorCB(url,"UserAborted");
					return;
				}
				haxe_Log.trace("trying php fallback:" + errorCode + " " + url,{ fileName : "RasterDataSet.hx", lineNumber : 152, className : "com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet", methodName : "readFromSpriteSheetImage"});
				com_oddcast_io_pngEncoder_ParsePNGMetadata.getMetaDataFromPHP(url,function(metaData1) {
					if(archive.isAborted()) {
						errorCB("php " + url,"UserAborted");
						return;
					}
					haxe_Log.trace("php callback complete:" + url,{ fileName : "RasterDataSet.hx", lineNumber : 159, className : "com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet", methodName : "readFromSpriteSheetImage"});
					completeCB(com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processMetaData(metaData1,0,spriteSheetImage,construct));
				},errorCB);
			},false);
		}
	},errorCB);
};
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processMetaData = function(metaData,totalSize,spriteSheetImage,construct) {
	var _g = 0;
	while(_g < metaData.length) {
		var metadatum = metaData[_g];
		++_g;
		if(metadatum.keyword == com_oddcast_io_pngEncoder_PNGKeywords.COMMENT) return com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processSpriteSheetLayoutData(metadatum.value,construct,totalSize,spriteSheetImage);
	}
	return null;
};
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.processSpriteSheetLayoutData = function(dataString,construct,totalSize,spriteSheetImage) {
	var binaryData;
	if(StringTools.startsWith(dataString,"<?xml")) binaryData = new com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml(dataString); else binaryData = new com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS(dataString);
	var rasterDataSet = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(null,construct,binaryData,com_oddcast_util_UtilsLite.getShortClassName(construct()));
	rasterDataSet.dataSize = totalSize;
	rasterDataSet.setSpriteSheet(spriteSheetImage);
	rasterDataSet.afterRead();
	return rasterDataSet;
};
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.__super__ = com_oddcast_io_xmlIO_XmlIODated;
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype = $extend(com_oddcast_io_xmlIO_XmlIODated.prototype,{
	afterRead: function() {
		this.root.setDepths();
		var afterRead = { jawHeight : NaN};
		this.root.spanDisplay(function(da2) {
			da2.afterRead(afterRead);
			return false;
		});
	}
	,cleanBeforeSaving: function() {
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIODated.prototype.parse.call(this,bd);
		this.parseDates(bd);
		this.scale_ = bd.float32(this.scale_,"scale_");
		this.imageStr_ = bd.string16(this.imageStr_,"imageStr_");
		this.mamName_ = bd.string16(this.mamName_,"mamName_");
		this.spriteImages_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse(this.spriteImages_,function() {
			return new com_oddcast_app_vhss_$extraction_display_SpriteImage();
		},bd,"spriteImages_");
		this.root = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(this.root,null,bd,"root");
	}
	,setSpriteSheet: function(spriteSheetImage) {
		var spriteSheetType = spriteSheetImage;
		var _g = 0;
		var _g1 = this.spriteImages_;
		while(_g < _g1.length) {
			var spriteImage = _g1[_g];
			++_g;
			spriteImage.loadFromSpriteSheet(spriteSheetType);
		}
		this.root.loadFromSpriteSheet(this.spriteImages_);
		this.lspriteSheet = spriteSheetType;
	}
	,initDisplayTree: function() {
		this.root.spanDisplay(function(da) {
			da.initDisplayTree();
			return false;
		});
	}
	,dispose: function() {
		this.spriteImages_ = com_oddcast_util_Disposable.disposeIterableIfValid(this.spriteImages_);
		this.root = com_oddcast_util_Disposable.disposeIfValid(this.root);
		this.imageStr_ = null;
		this.lspriteSheet = com_oddcast_util_Disposable.disposeIfValid(this.lspriteSheet);
	}
	,__class__: com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet
});
var com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet = function() {
	com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.call(this);
};
$hxClasses["com.oddcast.app.vhss_extraction.rasterdataset.AccFragDataSet"] = com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet;
com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.__name__ = ["com","oddcast","app","vhss_extraction","rasterdataset","AccFragDataSet"];
com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.__super__ = com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet;
com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.prototype = $extend(com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype,{
	parse: function(bd) {
		com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.totalCount++;
		com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype.parse.call(this,bd);
		if(!bd.isWrite()) {
		}
		haxe_Log.trace("AccTotal Parse:" + com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.totalCount + " " + this.mamName_,{ fileName : "AccFragDataSet.hx", lineNumber : 54, className : "com.oddcast.app.vhss_extraction.rasterdataset.AccFragDataSet", methodName : "parse"});
	}
	,isNull: function() {
		return this.spriteImages_.length == 0;
	}
	,dispose: function() {
		haxe_Log.trace("AccTotal__________ Dispose:" + --com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.totalCount + " " + this.mamName_,{ fileName : "AccFragDataSet.hx", lineNumber : 65, className : "com.oddcast.app.vhss_extraction.rasterdataset.AccFragDataSet", methodName : "dispose"});
		com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype.dispose.call(this);
		var junk = 1;
	}
	,__class__: com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet
});
var com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet = function() {
	this.bodyScale = 1.0;
	this.noseScale = 1.0;
	this.mouthScale = 1.0;
	this.fHxscale = 1.0;
	this.fHyscale = 1.0;
	com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.call(this);
	this.hostXoffset_ = -300;
	this.hostYoffset_ = -139.5;
	this.colorEditable = true;
	this.normalRenderFlags = new com_oddcast_util_Flags();
	this.defaultFileExtension = "ohv2.png";
	this.mouthID_ = -1;
};
$hxClasses["com.oddcast.app.vhss_extraction.rasterdataset.HostDataSet"] = com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.__name__ = ["com","oddcast","app","vhss_extraction","rasterdataset","HostDataSet"];
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.__super__ = com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.prototype = $extend(com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype,{
	cleanBeforeSaving: function() {
		this.clearScales();
	}
	,parse: function(bd) {
		com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.prototype.parse.call(this,bd);
		this.mouthVersion_ = bd.int32(this.mouthVersion_,"mouthVersion_");
		this.breathRate_ = bd.float32(this.breathRate_,"breathRate_");
		this.ev_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(this.ev_,function() {
			return new com_oddcast_app_vhss_$extraction_animation_EngineConstantValues();
		},bd,"ev_");
		this.hostID_ = bd.int32(this.hostID_,"hostID_");
		if(this.version_ >= 2) {
			this.hostXoffset_ = bd.float32(this.hostXoffset_,"hostXoffset_");
			this.hostYoffset_ = bd.float32(this.hostYoffset_,"hostYoffset_");
		}
		if(this.version_ >= 3) this.mouthID_ = bd.int32(this.mouthID_,"mouthID_");
		if(!bd.isWrite()) {
		}
	}
	,setScale: function(scaleName,scale) {
		switch(scaleName) {
		case "hyscale":
			this.fHyscale = scale;
			break;
		case "hxscale":
			this.fHxscale = scale;
			break;
		case "mscale":
			this.mouthScale = scale;
			break;
		case "nscale":
			this.noseScale = scale;
			break;
		case "bscale":
			this.bodyScale = scale;
			break;
		}
	}
	,getScale: function(scaleName) {
		switch(scaleName) {
		case "hyscale":
			return this.fHyscale;
		case "hxscale":
			return this.fHxscale;
		case "mscale":
			return this.mouthScale;
		case "nscale":
			return this.noseScale;
		case "bscale":
			return this.bodyScale;
		default:
			return 1.0;
		}
	}
	,isAlphable: function(part) {
		return this.root.isColorableOrAlphable(null,com_oddcast_app_vhss_$extraction_color_Coloring.str2AlphaGroup(part));
	}
	,isColorable: function(part) {
		return this.root.isColorableOrAlphable(com_oddcast_app_vhss_$extraction_color_Coloring.str2ColorGroup(part),null);
	}
	,setColor: function(part,hex) {
		var colorGroup = com_oddcast_app_vhss_$extraction_color_Coloring.str2ColorGroup(part);
		if(typeof(hex) == "string") throw new js__$Boot_HaxeError("setColor color " + hex + " must be an integer, not a representation of a integer");
		var rgb = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.hexToRGB(hex);
		this.groupColorDelta.set(colorGroup,rgb);
		this.groupColorFromQueryString.set(colorGroup,null);
		this.effectGroupColors(this.groupColorDelta,this.groupAlpha,true,this.colorEditable,false);
	}
	,getColor: function(part) {
		if(this.groupColorDelta != null) {
			var colorGroup = com_oddcast_app_vhss_$extraction_color_Coloring.str2ColorGroup(part);
			var rgb2 = this.groupColorDelta.get(colorGroup);
			haxe_Log.trace(part + " rgb2 " + Std.string(rgb2),{ fileName : "HostDataSet.hx", lineNumber : 232, className : "com.oddcast.app.vhss_extraction.rasterdataset.HostDataSet", methodName : "getColor"});
			var a = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.RGBtoArray(rgb2);
			com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.forceRange(a,0,255);
			var rgb3 = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.ArrayToRGB(a);
			return com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.rgbToHex(rgb3);
		}
		throw new js__$Boot_HaxeError("no colorGroup of " + part);
	}
	,colorGroup2Str: function(colorGroup) {
		switch(colorGroup[1]) {
		case 0:
			return "eyes";
		case 1:
			return "hair";
		case 2:
			return "mouth";
		case 3:
			return "skin";
		case 4:
			return "make-up";
		}
	}
	,sumColoring: function(colorGroup) {
		var baseTotals = [];
		if(com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR == colorGroup) {
			var junk = 1;
		}
		this.root.sumColoring(colorGroup,baseTotals);
		var rgbTotal = new com_oddcast_app_vhss_$extraction_color_RGB_$Total(-1);
		var i = 0;
		var _g = 0;
		while(_g < baseTotals.length) {
			var baseTotal = baseTotals[_g];
			++_g;
			if(baseTotal != null) {
				rgbTotal.add(baseTotal);
				baseTotal.setAverageColoring(Std.string(colorGroup) + " colorIndex:" + i);
			}
			i++;
		}
		return rgbTotal.averageRGB();
	}
	,getAlpha: function(part) {
		if(this.groupAlpha != null) {
			var key = com_oddcast_app_vhss_$extraction_color_Coloring.str2AlphaGroup(part);
			return this.groupAlpha.get(key);
		}
		return -1.0;
	}
	,setAlpha: function(part,value) {
		if(this.groupAlpha != null) {
			var key = com_oddcast_app_vhss_$extraction_color_Coloring.str2AlphaGroup(part);
			this.groupAlpha.set(key,value);
			this.effectGroupColors(this.groupColorDelta,this.groupAlpha,true,this.colorEditable,false);
		}
	}
	,calcGroupColorDelta: function(colorGroup) {
		var rgb = this.groupColorFromQueryString.get(colorGroup);
		if(rgb != null) {
			var lrgb = com_oddcast_cv_util_COLOR.RGBclone(rgb);
			var baseRGB = this.sumColoring(colorGroup);
			lrgb.r += baseRGB.r;
			lrgb.g += baseRGB.g;
			lrgb.b += baseRGB.b;
			this.groupColorDelta.set(colorGroup,lrgb);
		}
	}
	,primeFromQueryString: function(queryString) {
		if(queryString != null) {
			var split = queryString.split("=");
			var cs = split[1];
			var values = cs.split(":");
			this.groupColorDelta = new haxe_ds_EnumValueMap();
			this.groupColorFromQueryString = new haxe_ds_EnumValueMap();
			var i = 0;
			while(i < com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.COLOR_GROUPS.length) {
				var value = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.decodeURLHex("0x" + values[i]);
				this.groupColorFromQueryString.set(com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.COLOR_GROUPS[i],value);
				this.calcGroupColorDelta(com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.COLOR_GROUPS[i]);
				i++;
			}
			this.fHyscale = parseFloat(values[5]) / 100.0;
			this.fHxscale = parseFloat(values[6]) / 100.0;
			this.mouthScale = parseFloat(values[7]) / 100.0;
			this.noseScale = parseFloat(values[8]) / 100.0;
			this.bodyScale = parseFloat(values[9]) / 100.0;
			this.ageStr = values[10];
			this.groupAlpha = new haxe_ds_EnumValueMap();
			var value1 = parseFloat(values[11]) / 100;
			this.groupAlpha.set(com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH,value1);
			var value2 = parseFloat(values[12]) / 100;
			this.groupAlpha.set(com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP,value2);
			this.effectGroupColors(this.groupColorDelta,this.groupAlpha,true,this.colorEditable,true);
		}
	}
	,getColorURLstring: function() {
		var sb = new StringBuf();
		sb.b += "cs=";
		var _g = 0;
		var _g1 = com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.COLOR_GROUPS;
		while(_g < _g1.length) {
			var colorGroup = _g1[_g];
			++_g;
			var baseRGB = this.sumColoring(colorGroup);
			var rgb = this.groupColorDelta.get(colorGroup);
			var encoded = com_oddcast_app_vhss_$extraction_color_ColorAnalyzerHX_$Utils.encodeURLHex(rgb.r - baseRGB.r,rgb.g - baseRGB.g,rgb.b - baseRGB.b);
			sb.add(StringTools.hex(encoded).toLowerCase() + ":");
		}
		var _g2 = 0;
		var _g11 = com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_GROUPS;
		while(_g2 < _g11.length) {
			var scaleGroup = _g11[_g2];
			++_g2;
			sb.add(Math.round(this.getScale(scaleGroup) * 100) + ":");
		}
		sb.b += Std.string(this.ageStr + ":");
		var fBlush = this.groupAlpha.get(com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_BLUSH) * 100;
		if(!isNaN(fBlush)) if(fBlush == null) sb.b += "null"; else sb.b += "" + fBlush;
		sb.b += ":";
		var fMakeUp = this.groupAlpha.get(com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP) * 100;
		if(!isNaN(fMakeUp)) sb.add(this.groupAlpha.get(com_oddcast_app_vhss_$extraction_color_AlphaGroup.AlphaGroup_MAKEUP) * 100);
		return sb.b;
	}
	,accessoryLoaded: function() {
		this.calcGroupColorDelta(com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR);
		this.calcGroupColorDelta(com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH);
		this.effectGroupColors(this.groupColorDelta,this.groupAlpha,true,this.colorEditable,true);
	}
	,clearScales: function() {
		this.fHyscale = 1.0;
		this.fHxscale = 1.0;
		this.mouthScale = 1.0;
		this.noseScale = 1.0;
		this.bodyScale = 1.0;
	}
	,frameUpdate: function(appState) {
		appState.param_mScale = this.mouthScale;
		appState.param_nScale = this.noseScale;
		appState.param_bscale = this.bodyScale;
		appState.param_hxscale = this.fHxscale;
		appState.param_hyscale = this.fHyscale;
		this.root.frameUpdate(appState);
		this.root.frameUpdate2(appState);
	}
	,draw: function(canvas) {
		canvas.pushPosScaleRot(this.hostXoffset_,this.hostYoffset_ + -3.0,1.0,1.0,0.0);
		this.root.draw(canvas,this.normalRenderFlags);
		canvas.popMatrix();
	}
	,getHostMouse: function(mouse) {
		var host = this.root.getChildByName("host");
		var eyerMouse = host.getChildByName("eyer").getMouse(mouse);
		if(eyerMouse != null) {
			var bounds = host.getBounds();
			var hostMouse = host.getMouse(mouse);
			var localMousePoint = new com_oddcast_neko_geom_Point(hostMouse.x,eyerMouse.y);
			return { localMousePoint : localMousePoint, isInBounds : bounds.contains(hostMouse.x,hostMouse.y)};
		}
		return null;
	}
	,measureHost: function() {
		var host = this.root.getChildByName("host");
		var eyer = host.getChildByName("eyer");
		var eyel = host.getChildByName("eyel");
		var interOccular = eyer.getTotalMatrix().tx - eyel.getTotalMatrix().tx;
		var halfFaceWidth = 0;
		return { interOccularDist : interOccular, halfFaceWidth : halfFaceWidth};
	}
	,effectGroupColors: function(groupColorDelta,groupAlpha,bUseColorBase,editable,applyImmediatly) {
		var startTime = com_oddcast_util_UtilsLite.getTime();
		this.root.effectGroupColors(groupColorDelta,groupAlpha,new com_oddcast_util_js_geom_ColorTransform(),bUseColorBase,false,editable,applyImmediatly);
	}
	,finalfixup: function() {
		var _g = this;
		this.root.spanDisplayChildrenFirst(function(da2) {
			da2.finalfixup(_g.ev_);
			return false;
		});
	}
	,setHostOffsets: function(hostXoffset,hostYoffset) {
		var mat = this.root.getBaseMatrix();
		mat.tx += hostXoffset;
		mat.ty += hostYoffset;
		this.hostXoffset_ = this.hostYoffset_ = 0;
	}
	,setEngineConstant: function(name,value) {
		if(this.ev_ != null) this.ev_.setFloat(name,value);
	}
	,getEngineConstant: function(name) {
		if(this.ev_ != null) return this.ev_.getFloat(name);
		return NaN;
	}
	,__class__: com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet
});
var com_oddcast_util_js_IAudioLite = function() { };
$hxClasses["com.oddcast.util.js.IAudioLite"] = com_oddcast_util_js_IAudioLite;
com_oddcast_util_js_IAudioLite.__name__ = ["com","oddcast","util","js","IAudioLite"];
com_oddcast_util_js_IAudioLite.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_util_js_IAudioLite.prototype = {
	__class__: com_oddcast_util_js_IAudioLite
};
var com_oddcast_audio_AudioSaySilent = function(durationSecs) {
	this.lastTime = com_oddcast_util_UtilsLite.getTime();
	this.bPaused = false;
	this.durationMs = durationSecs * 1000.0;
	this.position = 0.0;
};
$hxClasses["com.oddcast.audio.AudioSaySilent"] = com_oddcast_audio_AudioSaySilent;
com_oddcast_audio_AudioSaySilent.__name__ = ["com","oddcast","audio","AudioSaySilent"];
com_oddcast_audio_AudioSaySilent.__interfaces__ = [com_oddcast_util_js_IAudioLite];
com_oddcast_audio_AudioSaySilent.prototype = {
	playAudio: function(url,offsetMillis,volume,autoStart,id3String,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback) {
		throw new js__$Boot_HaxeError("ToDo");
		return null;
	}
	,pauseResume: function(pause) {
		this.bPaused = pause;
	}
	,replay: function(offsetMillis) {
		if(offsetMillis == null) offsetMillis = 0.0;
	}
	,isPlaying: function() {
		return !this.bPaused;
	}
	,setToTime: function(millis) {
		this.position = millis;
	}
	,positionMillis: function() {
		var thisTime = com_oddcast_util_UtilsLite.getTime();
		if(this.isPlaying()) this.position += thisTime - this.lastTime;
		this.lastTime = thisTime;
		haxe_Log.trace("position:" + this.position + " paused:" + Std.string(this.bPaused),{ fileName : "AudioSaySilent.hx", lineNumber : 53, className : "com.oddcast.audio.AudioSaySilent", methodName : "positionMillis"});
		return this.position;
	}
	,durationMillis: function() {
		return this.durationMs;
	}
	,playingProgress: function() {
		return this.positionMillis() / this.durationMillis();
	}
	,setVolume: function(v) {
	}
	,dispose: function() {
	}
	,__class__: com_oddcast_audio_AudioSaySilent
};
var com_oddcast_audio_ID3comment = function(id3Comment) {
	if(id3Comment != null) this.id3Comment = StringTools.replace(id3Comment,com_oddcast_audio_ID3comment.FALSE_ITEM_DELIMITER,com_oddcast_audio_ID3comment.ITEM_DELIMITER);
};
$hxClasses["com.oddcast.audio.ID3comment"] = com_oddcast_audio_ID3comment;
com_oddcast_audio_ID3comment.__name__ = ["com","oddcast","audio","ID3comment"];
com_oddcast_audio_ID3comment.prototype = {
	extract: function(sectionName) {
		try {
			var chunks = this.id3Comment.split(com_oddcast_audio_ID3comment.ITEM_DELIMITER);
			haxe_Log.trace("-" + sectionName + "-",{ fileName : "ID3comment.hx", lineNumber : 44, className : "com.oddcast.audio.ID3comment", methodName : "extract"});
			var _g = 0;
			while(_g < chunks.length) {
				var ch = chunks[_g];
				++_g;
				var firstEquels = ch.indexOf("=");
				if(firstEquels > 0) {
					var id3section = StringTools.trim(HxOverrides.substr(ch,0,firstEquels - 1));
					id3section = StringTools.replace(id3section,String.fromCharCode(0),"");
					var str = StringTools.trim(HxOverrides.substr(ch,firstEquels + 1,null));
					var data = HxOverrides.substr(str,1,str.length - 2);
					haxe_Log.trace("id3section:" + id3section,{ fileName : "ID3comment.hx", lineNumber : 65, className : "com.oddcast.audio.ID3comment", methodName : "extract"});
					if(com_oddcast_util_StringTools2.equals(id3section,sectionName)) return data;
				}
			}
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			haxe_Log.trace(" reading id3 section:" + Std.string(e.toString()) + " " + sectionName + " id3:" + this.id3Comment,{ fileName : "ID3comment.hx", lineNumber : 149, className : "com.oddcast.audio.ID3comment", methodName : "extract"});
		}
		return null;
	}
	,__class__: com_oddcast_audio_ID3comment
};
var com_oddcast_audio_PlayingVAudio = function(vAudio) {
	this.vAudio = vAudio;
};
$hxClasses["com.oddcast.audio.PlayingVAudio"] = com_oddcast_audio_PlayingVAudio;
com_oddcast_audio_PlayingVAudio.__name__ = ["com","oddcast","audio","PlayingVAudio"];
com_oddcast_audio_PlayingVAudio.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_audio_PlayingVAudio.prototype = {
	id3: function(id3Str) {
		this.id3Comment = new com_oddcast_audio_ID3comment(id3Str);
		var id3Duration = this.id3Comment.extract("audio_duration");
		if(id3Duration == null) {
			var z = "";
			id3Duration = this.id3Comment.extract("XXX" + z + "audio_duration");
			if(id3Duration == null) id3Duration = this.id3Comment.extract("XX" + z + "audio_duration");
			if(id3Duration == null) return -1.0;
		}
		var durationSeconds = parseFloat(id3Duration);
		this.extractVisemeData(this.id3Comment,durationSeconds);
		return durationSeconds;
	}
	,extractVisemeData: function(id3Comment,durationSeconds) {
	}
	,setToTime: function(millis) {
		this.vAudio.setToTime(millis);
	}
	,exists: function() {
		return this.vAudio != null;
	}
	,position: function() {
		if(this.vAudio != null) return this.vAudio.positionMillis(); else return 0;
	}
	,duration: function() {
		return this.vAudio.durationMillis();
	}
	,playingProgress: function() {
		return this.vAudio.playingProgress();
	}
	,isPlaying: function() {
		if(this.vAudio != null) return this.vAudio.isPlaying(); else return false;
	}
	,restartVisemes: function() {
	}
	,getVisemeTime: function(frameUpdate) {
		return this.positionWithFrameUpdate(frameUpdate);
	}
	,positionWithFrameUpdate: function(frameUpdate) {
		return this.position() + com_oddcast_app_FrameUpdateTools.getFakeTimeMillis(frameUpdate);
	}
	,pauseResume: function(pause) {
		this.vAudio.pauseResume(pause);
	}
	,replay: function(offsetMillis) {
		if(offsetMillis == null) offsetMillis = 0.0;
		this.restartVisemes();
		if(this.vAudio != null) this.vAudio.replay(offsetMillis);
	}
	,debug_getFrameNo: function() {
		return this.debugFrameNo++;
	}
	,setVolume: function(v) {
		this.vAudio.setVolume(v);
	}
	,isSayingSilent: function() {
		return false;
	}
	,crop: function(playForSecs) {
	}
	,play: function(startTime) {
		this.replay(startTime);
	}
	,length: function() {
		return this.duration();
	}
	,shortName: function() {
		return "not yet implemented";
	}
	,dispose: function() {
		this.vAudio = com_oddcast_util_Disposable.disposeIfValid(this.vAudio);
		this.id3Comment = null;
	}
	,__class__: com_oddcast_audio_PlayingVAudio
};
var com_oddcast_audio_PlayingVAudioSimpleVisemes = function(vAudio) {
	com_oddcast_audio_PlayingVAudio.call(this,vAudio);
};
$hxClasses["com.oddcast.audio.PlayingVAudioSimpleVisemes"] = com_oddcast_audio_PlayingVAudioSimpleVisemes;
com_oddcast_audio_PlayingVAudioSimpleVisemes.__name__ = ["com","oddcast","audio","PlayingVAudioSimpleVisemes"];
com_oddcast_audio_PlayingVAudioSimpleVisemes.__super__ = com_oddcast_audio_PlayingVAudio;
com_oddcast_audio_PlayingVAudioSimpleVisemes.prototype = $extend(com_oddcast_audio_PlayingVAudio.prototype,{
	extractVisemeData: function(id3Comment,durationSeconds) {
		var timedVisemes = new com_oddcast_host_morph_lipsync_TimedVisemes(0);
		timedVisemes.extract(id3Comment);
		var timedHeadMovements = new com_oddcast_host_morph_lipsync_TimedHeadMovements("",0);
		timedHeadMovements.calculateHeadMovements(timedVisemes.syncData);
		var audioAccents = com_oddcast_host_morph_lipsync_Emphasis.calcAccents(timedVisemes,timedHeadMovements.syncData,2.5);
		haxe_Log.trace(audioAccents,{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 59, className : "com.oddcast.audio.PlayingVAudioSimpleVisemes", methodName : "extractVisemeData"});
		var origTxt = id3Comment.extract(com_oddcast_audio_ID3comment.TEXT);
		this.energyArray = new com_oddcast_util_IndexedArray();
		this.wordEnergyArray = new com_oddcast_util_IndexedArray();
		var phonemeString = id3Comment.extract("timed_phonemes");
		var splitPhonemes = phonemeString.split("\t");
		var minEnergy = 100.0;
		var maxEnergy = 0.0;
		var wordSyncData = [];
		var _g = 0;
		while(_g < splitPhonemes.length) {
			var phoneme = splitPhonemes[_g];
			++_g;
			var dataSplit = phoneme.split(",");
			var begin = Std.parseInt(dataSplit[1]);
			var end = Std.parseInt(dataSplit[2]);
			var energy = parseFloat(dataSplit[3]);
			if(energy > 0 && energy < minEnergy) minEnergy = energy;
			if(energy > maxEnergy) maxEnergy = energy;
			if(dataSplit[0] == "P") this.energyArray.push({ begin : begin, end : end, energy : energy, filterE : 0, dedt : 0, dedt2 : 0});
			if(dataSplit[0] == "W") this.wordEnergyArray.push({ begin : begin, end : end, energy : energy, filterE : 0, dedt : 0, dedt2 : 0});
		}
		haxe_Log.trace("minEnergy:" + minEnergy + " maxEnergy:" + maxEnergy,{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 100, className : "com.oddcast.audio.PlayingVAudioSimpleVisemes", methodName : "extractVisemeData"});
		if(minEnergy < maxEnergy) {
			var normalize = 1 / (maxEnergy - minEnergy);
			var $it0 = this.energyArray.iterator();
			while( $it0.hasNext() ) {
				var energyDatum = $it0.next();
				energyDatum.energy = Math.max(0.0,energyDatum.energy - minEnergy) * normalize;
			}
		}
		var _g1 = 0;
		while(_g1 < 2) {
			var t = _g1++;
			var filterSize = 5;
			var halfFilter = filterSize >> 1;
			var _g2 = 0;
			var _g11 = this.energyArray.length();
			while(_g2 < _g11) {
				var i = _g2++;
				var e = this.energyArray.get(i);
				var total = 0.0;
				var nSample = 0;
				var _g3 = 0;
				while(_g3 < filterSize) {
					var j = _g3++;
					var c = i + j - halfFilter;
					if(c >= 0 && c < this.energyArray.length()) {
						total += this.energyArray.get(c).energy;
						nSample++;
					}
				}
				e.filterE = total / nSample;
			}
			var $it1 = this.energyArray.iterator();
			while( $it1.hasNext() ) {
				var e1 = $it1.next();
				e1.energy = e1.filterE;
			}
		}
		var last = -1.0;
		var $it2 = this.energyArray.iterator();
		while( $it2.hasNext() ) {
			var sample = $it2.next();
			if(last >= 0) sample.dedt = sample.filterE - last;
			last = sample.filterE;
		}
		var changes = [];
		var firstIsMaxima = false;
		var last1 = -1.0;
		var $it3 = this.energyArray.iterator();
		while( $it3.hasNext() ) {
			var sample1 = $it3.next();
			if(last1 >= 0) {
				sample1.dedt2 = sample1.dedt - last1;
				if(last1 * sample1.dedt < 0) {
					if(changes.length == 0) firstIsMaxima = last1 > 0;
					changes.push(sample1);
				}
			}
			last1 = sample1.dedt;
		}
		var isMax = firstIsMaxima;
		var _g4 = 0;
		while(_g4 < changes.length) {
			var change = changes[_g4];
			++_g4;
			isMax = !isMax;
		}
		var lipString = id3Comment.extract(com_oddcast_audio_ID3comment.LIP_STRING);
		var lipFrames = lipString.split("&");
		this.lipFrameArray = [];
		var prevFrameNo = -1;
		var frameRange = new com_oddcast_util_MaxMinFloat();
		haxe_Log.trace("lipFrames" + Std.string(lipFrames),{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 256, className : "com.oddcast.audio.PlayingVAudioSimpleVisemes", methodName : "extractVisemeData"});
		var _g5 = 0;
		while(_g5 < lipFrames.length) {
			var frame = lipFrames[_g5];
			++_g5;
			var data = frame.split("=");
			var desc = data[0];
			var value = Std.parseInt(data[1]);
			if(StringTools.startsWith(desc,"f")) {
				var frameNo = Std.parseInt(HxOverrides.substr(desc,1,null));
				if(frameNo != prevFrameNo + 1) throw new js__$Boot_HaxeError("missing Frame " + prevFrameNo + " " + frameNo);
				prevFrameNo = frameNo;
				this.lipFrameArray.push(value);
				frameRange.set(value);
			} else switch(desc) {
			case "nofudge":
				this.nofudge = value;
				break;
			case "lipversion":
				this.lipversion = value;
				break;
			case "ok":
				this.ok = value;
				break;
			default:
				throw new js__$Boot_HaxeError("unknown string in id3 " + desc);
			}
		}
		this.fps = 12.0;
		haxe_Log.trace("lipversion:" + this.lipversion + " frameRange:" + frameRange.toString(),{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 280, className : "com.oddcast.audio.PlayingVAudioSimpleVisemes", methodName : "extractVisemeData"});
		haxe_Log.trace("lipFrameArray" + Std.string(this.lipFrameArray),{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 281, className : "com.oddcast.audio.PlayingVAudioSimpleVisemes", methodName : "extractVisemeData"});
	}
	,currentMouthFrame_b0: function(frameUpdate,mouthVer,visemeAdvanceMillis) {
		if(visemeAdvanceMillis == null) visemeAdvanceMillis = 0.0;
		var time = (this.getVisemeTime(frameUpdate) + visemeAdvanceMillis) / 1000;
		var frameNo = Math.round(time * this.fps);
		if(frameNo < 0) frameNo = 0;
		if(this.lipFrameArray != null) {
			if(frameNo >= this.lipFrameArray.length) frameNo = this.lipFrameArray.length - 1;
			var lipFrame = this.lipFrameArray[frameNo];
			var mouthFrame = this.getMouthFrame(lipFrame,mouthVer);
			return mouthFrame;
		}
		return 0;
	}
	,currentEnergy: function(frameUpdate,energyAdvanceMillis) {
		if(energyAdvanceMillis == null) energyAdvanceMillis = 0.0;
		var time = (this.getVisemeTime(frameUpdate) + energyAdvanceMillis) / 1000;
		while(this.energyArray != null) {
			var energyDatum = this.energyArray.increment(1);
			if(energyDatum.end >= time) return energyDatum.energy;
			if(this.energyArray.isFinished()) break;
		}
		return 0.0;
	}
	,calcAccents: function(origTxt,realLength,words) {
		return null;
	}
	,setToTime: function(millis) {
		com_oddcast_audio_PlayingVAudio.prototype.setToTime.call(this,millis);
		this.energyArray.resetIndex();
	}
	,getMouthFrame: function(n,mouthVer) {
		if(this.lipversion >= 2 && mouthVer != 2) return com_oddcast_audio_PlayingVAudioSimpleVisemes.lip2to1Map[n]; else return n;
	}
	,__class__: com_oddcast_audio_PlayingVAudioSimpleVisemes
});
var com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes = function(visemeDurationSecs,audioFinishedCallback,playForSecs,playingAudio) {
	this.visemeDurationMillis = visemeDurationSecs * 1000.0;
	this.looped = 0;
	this.audioFinishedCallback = audioFinishedCallback;
	this.playForMillis = playForSecs * 1000.0;
	this.playingAudio = playingAudio;
	this.bLoopReseting = false;
	com_oddcast_audio_PlayingVAudioSimpleVisemes.call(this,new com_oddcast_audio_AudioSaySilent(playForSecs));
};
$hxClasses["com.oddcast.audio.PlayingVAudioSaySilentSimpleVisemes"] = com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes;
com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes.__name__ = ["com","oddcast","audio","PlayingVAudioSaySilentSimpleVisemes"];
com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes.__super__ = com_oddcast_audio_PlayingVAudioSimpleVisemes;
com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes.prototype = $extend(com_oddcast_audio_PlayingVAudioSimpleVisemes.prototype,{
	crop: function(playForSecs) {
	}
	,position: function() {
		var audioPos = this.vAudio.positionMillis();
		var inLoopPos = audioPos - this.looped * this.visemeDurationMillis;
		haxe_Log.trace("audioPos:" + audioPos + " inloop:" + inLoopPos + " loop:" + this.looped + " dur:" + this.visemeDurationMillis,{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 497, className : "com.oddcast.audio.PlayingVAudioSaySilentSimpleVisemes", methodName : "position"});
		if(inLoopPos > this.visemeDurationMillis) {
			inLoopPos -= this.visemeDurationMillis;
			this.looped++;
			this.playingAudio(this);
			haxe_Log.trace("loop:" + this.looped,{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 507, className : "com.oddcast.audio.PlayingVAudioSaySilentSimpleVisemes", methodName : "position"});
		}
		if(audioPos >= this.playForMillis) {
			this.audioFinishedCallback("saySilent",this.playForMillis / 1000);
			haxe_Log.trace("done",{ fileName : "PlayingVAudioSimpleVisemes.hx", lineNumber : 511, className : "com.oddcast.audio.PlayingVAudioSaySilentSimpleVisemes", methodName : "position"});
		}
		return inLoopPos;
	}
	,isSayingSilent: function() {
		return true;
	}
	,dispose: function() {
		this.audioFinishedCallback = null;
		this.playingAudio = null;
		com_oddcast_audio_PlayingVAudioSimpleVisemes.prototype.dispose.call(this);
	}
	,__class__: com_oddcast_audio_PlayingVAudioSaySilentSimpleVisemes
});
var com_oddcast_cv_util_COLOR = function() { };
$hxClasses["com.oddcast.cv.util.COLOR"] = com_oddcast_cv_util_COLOR;
com_oddcast_cv_util_COLOR.__name__ = ["com","oddcast","cv","util","COLOR"];
com_oddcast_cv_util_COLOR.RGBtoString = function(rgb,mult) {
	if(mult == null) mult = 1.0;
	return Math.round(rgb.r * mult) + ":" + Math.round(rgb.g * mult) + ":" + Math.round(rgb.b * mult);
};
com_oddcast_cv_util_COLOR.RGBclone = function(rgb) {
	return { r : rgb.r, g : rgb.g, b : rgb.b};
};
var com_oddcast_cv_util_Radians = function() { };
$hxClasses["com.oddcast.cv.util.Radians"] = com_oddcast_cv_util_Radians;
com_oddcast_cv_util_Radians.__name__ = ["com","oddcast","cv","util","Radians"];
com_oddcast_cv_util_Radians.toDegrees = function(rad) {
	return rad * 360 / (2 * Math.PI);
};
com_oddcast_cv_util_Radians.toRadians = function(deg) {
	return deg * 2 * Math.PI / 360;
};
var com_oddcast_host_IEventHandler = function() { };
$hxClasses["com.oddcast.host.IEventHandler"] = com_oddcast_host_IEventHandler;
com_oddcast_host_IEventHandler.__name__ = ["com","oddcast","host","IEventHandler"];
com_oddcast_host_IEventHandler.prototype = {
	__class__: com_oddcast_host_IEventHandler
};
var com_oddcast_host_EventHandlerImplementAnon = function(hxEventHandlerCallback,shortNameCallback) {
	this.hxEventHandlerCallback = hxEventHandlerCallback;
	this.shortNameCallback = shortNameCallback;
};
$hxClasses["com.oddcast.host.EventHandlerImplementAnon"] = com_oddcast_host_EventHandlerImplementAnon;
com_oddcast_host_EventHandlerImplementAnon.__name__ = ["com","oddcast","host","EventHandlerImplementAnon"];
com_oddcast_host_EventHandlerImplementAnon.__interfaces__ = [com_oddcast_host_IEventHandler];
com_oddcast_host_EventHandlerImplementAnon.prototype = {
	HXEventHandler: function(file,eventType) {
		this.hxEventHandlerCallback(file,eventType);
	}
	,shortName: function() {
		return this.shortNameCallback();
	}
	,__class__: com_oddcast_host_EventHandlerImplementAnon
};
var com_oddcast_util_Flags = function(inflags) {
	this.flags = 0;
	this.setFlag(com_oddcast_util_Utils.defaultParamInt(inflags,0));
};
$hxClasses["com.oddcast.util.Flags"] = com_oddcast_util_Flags;
com_oddcast_util_Flags.__name__ = ["com","oddcast","util","Flags"];
com_oddcast_util_Flags.prototype = {
	setFlag: function(f) {
		this.chooseFlag(f,true);
	}
	,clearFlag: function(f) {
		this.chooseFlag(f,false);
	}
	,chooseFlag: function(f,set) {
		if(set) this.flags |= f; else this.flags &= f ^ -1;
	}
	,flagIsSet: function(f) {
		return (this.flags & f) != 0;
	}
	,flagIsSetThenClear: function(f) {
		var ret = this.flagIsSet(f);
		if(ret) this.clearFlag(f);
		return ret;
	}
	,flagIsClear: function(f) {
		return !this.flagIsSet(f);
	}
	,toString: function() {
		return " flags:" + com_oddcast_util_Utils.hex0x(this.flags);
	}
	,__class__: com_oddcast_util_Flags
};
var com_oddcast_host_IUnloadable = function() { };
$hxClasses["com.oddcast.host.IUnloadable"] = com_oddcast_host_IUnloadable;
com_oddcast_host_IUnloadable.__name__ = ["com","oddcast","host","IUnloadable"];
com_oddcast_host_IUnloadable.prototype = {
	__class__: com_oddcast_host_IUnloadable
};
var com_oddcast_host_HXEvent = function(name,inflags) {
	this.name = name;
	com_oddcast_util_Flags.call(this,inflags);
};
$hxClasses["com.oddcast.host.HXEvent"] = com_oddcast_host_HXEvent;
com_oddcast_host_HXEvent.__name__ = ["com","oddcast","host","HXEvent"];
com_oddcast_host_HXEvent.__interfaces__ = [com_oddcast_host_IUnloadable,com_oddcast_host_IEventHandler];
com_oddcast_host_HXEvent.fireAllEvents = function() {
	if(!com_oddcast_host_HXEvent.bFireEventsBusy) {
		com_oddcast_host_HXEvent.bFireEventsBusy = true;
		var e;
		while((e = com_oddcast_host_HXEvent.fireList.pop()) != null) e.event.fireEventFire(e.type);
		com_oddcast_host_HXEvent.bFireEventsBusy = false;
	}
};
com_oddcast_host_HXEvent.charCode = function(s) {
	return HxOverrides.cca(s,0);
};
com_oddcast_host_HXEvent.__super__ = com_oddcast_util_Flags;
com_oddcast_host_HXEvent.prototype = $extend(com_oddcast_util_Flags.prototype,{
	shortName: function() {
		return com_oddcast_util_Utils.stripFileDirectory(this.name);
	}
	,HXEventHandler: function(file,eventType) {
		if(this.filesToLoadBeforeFireing != null) {
			var itFiles = HxOverrides.iter(this.filesToLoadBeforeFireing);
			var found = null;
			while( itFiles.hasNext() ) {
				var f = itFiles.next();
				found = f;
			}
			if(found != null) HxOverrides.remove(this.filesToLoadBeforeFireing,found);
			if(this.filesToLoadBeforeFireing.length == 0) {
				haxe_Log.trace("AllFilesLoaded:" + this.name,{ fileName : "HXEvent.hx", lineNumber : 126, className : "com.oddcast.host.HXEvent", methodName : "HXEventHandler"});
				this.filesToLoadBeforeFireing = null;
				this.fireEvent(0);
			}
		}
	}
	,ensureFileIsLoadedBeforeFireing: function(file) {
		file.addListener(this,0);
		if(this.filesToLoadBeforeFireing == null) this.filesToLoadBeforeFireing = [];
		this.filesToLoadBeforeFireing.push(file);
	}
	,listenForFileComplete: function(to) {
		if(to != null) to.addListener(this,0);
	}
	,listenForTexComplete: function(to) {
		if(to != null) to.addListener(this,1);
	}
	,addListener: function(listener,type) {
		if(listener != null) {
			if(this.listeners == null) this.listeners = new haxe_ds_IntMap();
			if(!this.listeners.h.hasOwnProperty(type)) {
				var value = [];
				this.listeners.h[type] = value;
			}
			var listenerType = this.listeners.h[type];
			HxOverrides.remove(listenerType,listener);
			listenerType.push(listener);
		}
		if(this.isReady()) this.fireEvent(type);
	}
	,removeListener: function(listener,type) {
		if(listener != null) {
			if(this.listeners != null) {
				if(this.listeners.h.hasOwnProperty(type)) {
					var listenerType = this.listeners.h[type];
					HxOverrides.remove(listenerType,listener);
				}
			}
		}
	}
	,fireEvent: function(iType) {
		var e = { event : this, type : iType};
		com_oddcast_host_HXEvent.fireList.add(e);
		com_oddcast_host_HXEvent.fireAllEvents();
	}
	,fireEventFire: function(type) {
		if(this.listeners != null && this.listeners.h.hasOwnProperty(type)) {
			var listenerType = this.listeners.h[type];
			var listenerIterator = HxOverrides.iter(listenerType);
			var removeList = [];
			while( listenerIterator.hasNext() ) {
				var listener = listenerIterator.next();
				listener.HXEventHandler(this,type);
				removeList.push(listener);
			}
			var _g = 0;
			while(_g < removeList.length) {
				var rem = removeList[_g];
				++_g;
				HxOverrides.remove(listenerType,rem);
			}
		}
	}
	,isValidAndReady: function(test) {
		return test != null && test.isReady();
	}
	,isInvalidOrReady: function(test) {
		return test == null || test.isReady();
	}
	,areInvalidOrReady: function(array) {
		if(array != null) {
			var $it0 = $iterator(array)();
			while( $it0.hasNext() ) {
				var a = $it0.next();
				if(!this.isInvalidOrReady(a)) return false;
			}
		}
		return true;
	}
	,imin: function(a,b) {
		if(a < b) return a; else return b;
	}
	,imax: function(a,b) {
		if(a > b) return a; else return b;
	}
	,chooseReady: function(r) {
		if(r) this.setReady(); else this.setNotReady();
		return r;
	}
	,setReady: function() {
		this.setFlag(1);
	}
	,setNotReady: function() {
		this.clearFlag(1);
	}
	,isReady: function() {
		return this.flagIsSet(1);
	}
	,isDirty: function() {
		return this.flagIsSet(2);
	}
	,isClean: function() {
		return !this.isDirty();
	}
	,setDirty: function() {
		this.setFlag(2);
		return;
	}
	,setClean: function() {
		this.clearFlag(2);
		return;
	}
	,floatToInt: function(f) {
		return Math.round(f);
	}
	,addInterp: function(interp) {
		if(this.interpList == null) this.interpList = [];
		if(interp != null) this.interpList.push(interp);
		return interp;
	}
	,removeInterp: function(interp) {
		if(this.interpList != null) {
			HxOverrides.remove(this.interpList,interp);
			if(Lambda.empty(this.interpList)) this.interpList = null;
		}
		return null;
	}
	,update: function(frameUpdate) {
		if(this.interpList != null) {
			var interval = frameUpdate.getInterval();
			var _g = 0;
			var _g1 = this.interpList;
			while(_g < _g1.length) {
				var interp = _g1[_g];
				++_g;
				interp.update(interval);
			}
		}
	}
	,registerEditControl: function(plabel,index) {
	}
	,unload: function() {
		this.interpList = null;
	}
	,unloadOrNull: function(child) {
		if(child != null) {
			com_oddcast_host_HXEvent.debugSpacing += " ";
			child.unload();
			com_oddcast_host_HXEvent.debugSpacing = HxOverrides.substr(com_oddcast_host_HXEvent.debugSpacing,1,null);
		}
		return null;
	}
	,unloadOrNullIterable: function(array) {
		if(array != null) {
			var $it0 = $iterator(array)();
			while( $it0.hasNext() ) {
				var a = $it0.next();
				this.unloadOrNull(a);
			}
		}
		return null;
	}
	,toString: function() {
		return " shortName:" + this.shortName() + com_oddcast_util_Flags.prototype.toString.call(this);
	}
	,ifInvalidOrReadyDebug: function(base) {
		if(base == null) return true;
		return base.debugReady();
	}
	,isValidAndReadyDebug: function(test) {
		return test != null && test.debugReady();
	}
	,debugReady: function() {
		if(this.isReady()) return true;
		haxe_Log.trace("debugReady " + this.name + " is not ready",{ fileName : "HXEvent.hx", lineNumber : 577, className : "com.oddcast.host.HXEvent", methodName : "debugReady"});
		return false;
	}
	,__class__: com_oddcast_host_HXEvent
});
var com_oddcast_host_HXEventLite = function(name) {
};
$hxClasses["com.oddcast.host.HXEventLite"] = com_oddcast_host_HXEventLite;
com_oddcast_host_HXEventLite.__name__ = ["com","oddcast","host","HXEventLite"];
com_oddcast_host_HXEventLite.__interfaces__ = [com_oddcast_host_IUnloadable];
com_oddcast_host_HXEventLite.prototype = {
	unload: function() {
	}
	,unloadOrNull: function(child) {
		if(child != null) child.unload();
		return null;
	}
	,isReady: function() {
		return true;
	}
	,__class__: com_oddcast_host_HXEventLite
};
var com_oddcast_host_engine3d_math_Float2D = function(u,v) {
	this.u = u;
	this.v = v;
};
$hxClasses["com.oddcast.host.engine3d.math.Float2D"] = com_oddcast_host_engine3d_math_Float2D;
com_oddcast_host_engine3d_math_Float2D.__name__ = ["com","oddcast","host","engine3d","math","Float2D"];
com_oddcast_host_engine3d_math_Float2D.ZERO = function() {
	return new com_oddcast_host_engine3d_math_Float2D(0,0);
};
com_oddcast_host_engine3d_math_Float2D.uvListTransform = function(uvList,transMatrix) {
	var _g = 0;
	while(_g < uvList.length) {
		var i = uvList[_g];
		++_g;
		i.transformUVby(transMatrix);
	}
};
com_oddcast_host_engine3d_math_Float2D.prototype = {
	clone: function() {
		return new com_oddcast_host_engine3d_math_Float2D(this.u,this.v);
	}
	,toString: function() {
		return "u:" + com_oddcast_util_UtilsLite.formatFloat(this.u,6) + " v:" + com_oddcast_util_UtilsLite.formatFloat(this.v,6);
	}
	,makeHashKey: function() {
		return Std.string(this.u) + "," + Std.string(this.v);
	}
	,min: function(u1,v1) {
		this.u = Math.min(this.u,u1);
		this.v = Math.min(this.v,v1);
	}
	,max: function(u1,v1) {
		this.u = Math.max(this.u,u1);
		this.v = Math.max(this.v,v1);
	}
	,distSqr: function(other) {
		return (other.u - this.u) * (other.u - this.u) + (other.v - this.v) * (other.v - this.v);
	}
	,UandVareWithin: function(other,precision) {
		return this.distSqr(other) < precision * precision;
	}
	,transformUVby: function(m) {
		var origU = this.u;
		this.u = origU * m.n11 + this.v * m.n12 + m.n14;
		this.v = origU * m.n21 + this.v * m.n22 + m.n24;
	}
	,__class__: com_oddcast_host_engine3d_math_Float2D
};
var com_oddcast_host_engine3d_math_Float3D = function(x,y,z) {
	if(z == null) z = 0.0;
	if(y == null) y = 0.0;
	if(x == null) x = 0.0;
	this.assign(x,y,z);
};
$hxClasses["com.oddcast.host.engine3d.math.Float3D"] = com_oddcast_host_engine3d_math_Float3D;
com_oddcast_host_engine3d_math_Float3D.__name__ = ["com","oddcast","host","engine3d","math","Float3D"];
com_oddcast_host_engine3d_math_Float3D.parse = function(str,delimiter) {
	var val = str.split(delimiter);
	return new com_oddcast_host_engine3d_math_Float3D(parseFloat(val[0]),parseFloat(val[1]),parseFloat(val[2]));
};
com_oddcast_host_engine3d_math_Float3D.add = function(v,w) {
	return new com_oddcast_host_engine3d_math_Float3D(v.x + w.x,v.y + w.y,v.z + w.z);
};
com_oddcast_host_engine3d_math_Float3D.sub = function(v,w) {
	return new com_oddcast_host_engine3d_math_Float3D(v.x - w.x,v.y - w.y,v.z - w.z);
};
com_oddcast_host_engine3d_math_Float3D.staticDot = function(v,w) {
	return v.x * w.x + v.y * w.y + w.z * v.z;
};
com_oddcast_host_engine3d_math_Float3D.cross = function(v,w) {
	return new com_oddcast_host_engine3d_math_Float3D(w.y * v.z - w.z * v.y,w.z * v.x - w.x * v.z,w.x * v.y - w.y * v.x);
};
com_oddcast_host_engine3d_math_Float3D.ZERO = function() {
	return new com_oddcast_host_engine3d_math_Float3D(0,0,0);
};
com_oddcast_host_engine3d_math_Float3D.prototype = {
	assign: function(zx,zy,zz) {
		this.x = zx;
		this.y = zy;
		this.z = zz;
	}
	,serialize: function(delimiter,nDec) {
		if(nDec == null) nDec = 3;
		return com_oddcast_util_UtilsLite.formatFloat(this.x,nDec) + delimiter + com_oddcast_util_UtilsLite.formatFloat(this.y,nDec) + delimiter + com_oddcast_util_UtilsLite.formatFloat(this.z,nDec);
	}
	,clone: function() {
		return new com_oddcast_host_engine3d_math_Float3D(this.x,this.y,this.z);
	}
	,length: function() {
		return Math.sqrt(this.lengthSqrd());
	}
	,lengthSqrd: function() {
		return this.x * this.x + this.y * this.y + this.z * this.z;
	}
	,addThese: function(v,w) {
		this.x = v.x + w.x;
		this.y = v.y + w.y;
		this.z = v.z + w.z;
	}
	,addFloat3D: function(w) {
		this.x += w.x;
		this.y += w.y;
		this.z += w.z;
	}
	,addFloat3DAbs: function(w) {
		this.x += Math.abs(w.x);
		this.y += Math.abs(w.y);
		this.z += Math.abs(w.z);
	}
	,addConstant: function(f) {
		this.x += f;
		this.y += f;
		this.z += f;
	}
	,sub2: function(v,w) {
		this.x = v.x - w.x;
		this.y = v.y - w.y;
		this.z = v.z - w.z;
	}
	,subFloat3D: function(w) {
		this.x -= w.x;
		this.y -= w.y;
		this.z -= w.z;
	}
	,dot: function(w) {
		return this.x * w.x + this.y * w.y + this.z * w.z;
	}
	,copyFrom: function(v) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
		return this;
	}
	,scale: function(v,s) {
		this.x = v.x * s;
		this.y = v.y * s;
		this.z = v.z * s;
	}
	,scaleConstant: function(s) {
		this.x *= s;
		this.y *= s;
		this.z *= s;
	}
	,addScaled: function(v,s) {
		this.x += v.x * s;
		this.y += v.y * s;
		this.z += v.z * s;
	}
	,interpolate: function(v1,v2,s) {
		var t = 1 - s;
		this.x = v1.x * s + v2.x * t;
		this.y = v1.y * s + v2.y * t;
		this.z = v1.z * s + v2.z * t;
	}
	,multiply: function(v) {
		this.x *= v.x;
		this.y *= v.y;
		this.z *= v.z;
	}
	,mult: function(scale) {
		return new com_oddcast_host_engine3d_math_Float3D(this.x * scale,this.y * scale,this.z * scale);
	}
	,tangent: function(to) {
		var orth = com_oddcast_host_engine3d_math_Float3D.cross(this,to);
		return com_oddcast_host_engine3d_math_Float3D.cross(this,orth);
	}
	,normalize: function() {
		var mod = this.length();
		if(mod != 0 && mod != 1) this.scaleConstant(1.0 / mod);
		return this;
	}
	,clamp: function(lower,upper) {
		this.x = com_oddcast_util_UtilsLite.maxMin(this.x,upper,lower);
		this.y = com_oddcast_util_UtilsLite.maxMin(this.y,upper,lower);
		this.z = com_oddcast_util_UtilsLite.maxMin(this.z,upper,lower);
	}
	,clamp3D: function(lower,upper) {
		this.x = com_oddcast_util_UtilsLite.maxMin(this.x,upper.x,lower.x);
		this.y = com_oddcast_util_UtilsLite.maxMin(this.y,upper.y,lower.y);
		this.z = com_oddcast_util_UtilsLite.maxMin(this.z,upper.z,lower.z);
	}
	,distSqr: function(a) {
		return (a.x - this.x) * (a.x - this.x) + (a.y - this.y) * (a.y - this.y) + (a.z - this.z) * (a.z - this.z);
	}
	,dist: function(a) {
		return Math.sqrt(this.distSqr(a));
	}
	,toString: function(nDec) {
		if(nDec == null) nDec = 3;
		return " x:" + com_oddcast_util_UtilsLite.formatFloat(this.x,nDec) + " y:" + com_oddcast_util_UtilsLite.formatFloat(this.y,nDec) + " z:" + com_oddcast_util_UtilsLite.formatFloat(this.z,nDec);
	}
	,mathMax: function(a) {
		this.x = Math.max(this.x,a.x);
		this.y = Math.max(this.y,a.y);
		this.z = Math.max(this.z,a.z);
	}
	,mathMin: function(a) {
		this.x = Math.min(this.x,a.x);
		this.y = Math.min(this.y,a.y);
		this.z = Math.min(this.z,a.z);
	}
	,toXml: function(id,nDec) {
		if(nDec == null) nDec = 5;
		var el = com_oddcast_util_XmlTools.createElementWithID(id);
		com_oddcast_util_XmlTools.setAttributeFloat(el,"x",this.x,nDec);
		com_oddcast_util_XmlTools.setAttributeFloat(el,"y",this.y,nDec);
		com_oddcast_util_XmlTools.setAttributeFloat(el,"z",this.z,nDec);
		return el;
	}
	,parseXml: function(xml) {
		this.x = com_oddcast_util_XmlTools.getAttributeFloat(xml,"x",this.x);
		this.y = com_oddcast_util_XmlTools.getAttributeFloat(xml,"y",this.y);
		this.z = com_oddcast_util_XmlTools.getAttributeFloat(xml,"z",this.z);
		return this;
	}
	,getValueOnAxis: function(axis) {
		switch(axis) {
		case 0:
			return this.x;
		case 1:
			return this.y;
		case 2:
			return this.z;
		default:
			return 0.0;
		}
	}
	,setValueOnAxis: function(axis,value) {
		switch(axis) {
		case 0:
			this.x = value;
			break;
		case 1:
			this.y = value;
			break;
		case 2:
			this.z = value;
			break;
		}
	}
	,total: function() {
		return this.x + this.y + this.z;
	}
	,__class__: com_oddcast_host_engine3d_math_Float3D
};
var com_oddcast_host_engine3d_math_Matrix34 = function(args) {
	this.setFromFloatArray(args);
};
$hxClasses["com.oddcast.host.engine3d.math.Matrix34"] = com_oddcast_host_engine3d_math_Matrix34;
com_oddcast_host_engine3d_math_Matrix34.__name__ = ["com","oddcast","host","engine3d","math","Matrix34"];
com_oddcast_host_engine3d_math_Matrix34.getIdentityTemp = function() {
	com_oddcast_host_engine3d_math_Matrix34.TEMP.makeIdentity();
	return com_oddcast_host_engine3d_math_Matrix34.TEMP;
};
com_oddcast_host_engine3d_math_Matrix34.IDENTITY = function() {
	return new com_oddcast_host_engine3d_math_Matrix34(null);
};
com_oddcast_host_engine3d_math_Matrix34.clone = function(m) {
	return new com_oddcast_host_engine3d_math_Matrix34([m.n11,m.n12,m.n13,m.n14,m.n21,m.n22,m.n23,m.n24,m.n31,m.n32,m.n33,m.n34]);
};
com_oddcast_host_engine3d_math_Matrix34.matrix2euler = function(mat) {
	var angle = new com_oddcast_host_engine3d_math_Float3D(0,0,0);
	var d = -Math.asin(Math.max(-1,Math.min(1,mat.n13)));
	var c = Math.cos(d);
	angle.y = d * com_oddcast_host_engine3d_math_Matrix34.toDEGREES;
	var trX;
	var trY;
	if(Math.abs(c) > 0.005) {
		trX = mat.n33 / c;
		trY = -mat.n23 / c;
		angle.x = Math.atan2(trY,trX) * com_oddcast_host_engine3d_math_Matrix34.toDEGREES;
		trX = mat.n11 / c;
		trY = -mat.n12 / c;
		angle.z = Math.atan2(trY,trX) * com_oddcast_host_engine3d_math_Matrix34.toDEGREES;
	} else {
		angle.x = 0;
		trX = mat.n22;
		trY = mat.n21;
		angle.z = Math.atan2(trY,trX) * com_oddcast_host_engine3d_math_Matrix34.toDEGREES;
	}
	return angle;
};
com_oddcast_host_engine3d_math_Matrix34.euler2matrix = function(angle) {
	var m = com_oddcast_host_engine3d_math_Matrix34.IDENTITY();
	var ax = angle.x * com_oddcast_host_engine3d_math_Matrix34.toRADIANS;
	var ay = angle.y * com_oddcast_host_engine3d_math_Matrix34.toRADIANS;
	var az = angle.z * com_oddcast_host_engine3d_math_Matrix34.toRADIANS;
	var a = Math.cos(ax);
	var b = Math.sin(ax);
	var c = Math.cos(ay);
	var d = Math.sin(ay);
	var e = Math.cos(az);
	var f = Math.sin(az);
	var ad = a * d;
	var bd = b * d;
	m.n11 = c * e;
	m.n12 = -c * f;
	m.n13 = d;
	m.n21 = bd * e + a * f;
	m.n22 = -bd * f + a * e;
	m.n23 = -b * c;
	m.n31 = -ad * e + b * f;
	m.n32 = ad * f + b * e;
	m.n33 = a * c;
	return m;
};
com_oddcast_host_engine3d_math_Matrix34.rotationX = function(angleRad) {
	var m = com_oddcast_host_engine3d_math_Matrix34.IDENTITY();
	var c = Math.cos(angleRad);
	var s = Math.sin(angleRad);
	m.n22 = m.n33 = c;
	m.n23 = -s;
	m.n32 = s;
	return m;
};
com_oddcast_host_engine3d_math_Matrix34.rotationY = function(angleRad) {
	var m = com_oddcast_host_engine3d_math_Matrix34.IDENTITY();
	var c = Math.cos(angleRad);
	var s = Math.sin(angleRad);
	m.n11 = m.n33 = c;
	m.n13 = -s;
	m.n31 = s;
	return m;
};
com_oddcast_host_engine3d_math_Matrix34.rotationZ = function(angleRad) {
	var m = com_oddcast_host_engine3d_math_Matrix34.IDENTITY();
	var c = Math.cos(angleRad);
	var s = Math.sin(angleRad);
	m.n11 = m.n22 = c;
	m.n12 = -s;
	m.n21 = s;
	return m;
};
com_oddcast_host_engine3d_math_Matrix34.rotationMatrix = function(u,v,w,angle) {
	var m = com_oddcast_host_engine3d_math_Matrix34.IDENTITY();
	var nCos = Math.cos(angle);
	var nSin = Math.sin(angle);
	var scos = 1 - nCos;
	var suv = u * v * scos;
	var svw = v * w * scos;
	var suw = u * w * scos;
	var sw = nSin * w;
	var sv = nSin * v;
	var su = nSin * u;
	m.n11 = nCos + u * u * scos;
	m.n12 = -sw + suv;
	m.n13 = sv + suw;
	m.n21 = sw + suv;
	m.n22 = nCos + v * v * scos;
	m.n23 = -su + svw;
	m.n31 = -sv + suw;
	m.n32 = su + svw;
	m.n33 = nCos + w * w * scos;
	return m;
};
com_oddcast_host_engine3d_math_Matrix34.ReflectVAroundHalf = function() {
	return new com_oddcast_host_engine3d_math_Matrix34([-1.0,0,0,1,0,1,0,0,0,0,0,0]);
};
com_oddcast_host_engine3d_math_Matrix34.prototype = {
	makeIdentity: function() {
		this.n11 = this.n22 = this.n33 = 1;
		this.n12 = this.n13 = this.n14 = this.n21 = this.n23 = this.n24 = this.n31 = this.n32 = this.n34 = 0;
	}
	,setFromFloatArray: function(args) {
		if(args != null && args.length >= 12) {
			this.n11 = args[0];
			this.n12 = args[1];
			this.n13 = args[2];
			this.n14 = args[3];
			this.n21 = args[4];
			this.n22 = args[5];
			this.n23 = args[6];
			this.n24 = args[7];
			this.n31 = args[8];
			this.n32 = args[9];
			this.n33 = args[10];
			this.n34 = args[11];
		} else this.makeIdentity();
		if(args != null && args.length >= 16) {
			this.n41 = args[12];
			this.n42 = args[13];
			this.n43 = args[14];
			this.n44 = args[15];
		} else {
			this.n41 = this.n42 = this.n43 = 0;
			this.n44 = 1;
		}
	}
	,getAsFloatArray: function() {
		var a = [this.n11,this.n12,this.n13,this.n14,this.n21,this.n22,this.n23,this.n24,this.n31,this.n32,this.n33,this.n34,this.n41,this.n42,this.n43,this.n44];
		return a;
	}
	,directScale: function(x,y,z) {
		this.n11 *= x;
		this.n22 *= y;
		this.n33 *= z;
	}
	,getPos: function() {
		return new com_oddcast_host_engine3d_math_Float3D(this.n14,this.n24,this.n34);
	}
	,setPos: function(pos) {
		this.n14 = pos.x;
		this.n24 = pos.y;
		this.n34 = pos.z;
	}
	,getScale: function() {
		return new com_oddcast_host_engine3d_math_Float3D(this.n11,this.n22,this.n33);
	}
	,setScale: function(scale) {
		this.n11 = scale.x;
		this.n22 = scale.y;
		this.n33 = scale.z;
		haxe_Log.trace("[ALWAYS] Scale" + scale.toString(),{ fileName : "Matrix34.hx", lineNumber : 131, className : "com.oddcast.host.engine3d.math.Matrix34", methodName : "setScale"});
	}
	,setScale1D: function(scale) {
		this.n11 *= scale;
		this.n22 *= scale;
		this.n33 *= scale;
		return this;
	}
	,rotateAxis: function(v) {
		var vx;
		var vy;
		var vz;
		v.x = (vx = v.x) * this.n11 + (vy = v.y) * this.n12 + (vz = v.z) * this.n13;
		v.y = vx * this.n21 + vy * this.n22 + vz * this.n23;
		v.z = vx * this.n31 + vy * this.n32 + vz * this.n33;
		v.normalize();
	}
	,multiply: function(m1,m2) {
		var m111;
		var m211;
		var m121;
		var m221;
		var m131;
		var m231;
		var m112;
		var m212;
		var m122;
		var m222;
		var m132;
		var m232;
		var m113;
		var m213;
		var m123;
		var m223;
		var m133;
		var m233;
		var m114;
		var m214;
		var m124;
		var m224;
		var m134;
		var m234;
		this.n11 = (m111 = m1.n11) * (m211 = m2.n11) + (m112 = m1.n12) * (m221 = m2.n21) + (m113 = m1.n13) * (m231 = m2.n31);
		this.n12 = m111 * (m212 = m2.n12) + m112 * (m222 = m2.n22) + m113 * (m232 = m2.n32);
		this.n13 = m111 * (m213 = m2.n13) + m112 * (m223 = m2.n23) + m113 * (m233 = m2.n33);
		this.n14 = m111 * (m214 = m2.n14) + m112 * (m224 = m2.n24) + m113 * (m234 = m2.n34) + (m114 = m1.n14);
		this.n21 = (m121 = m1.n21) * m211 + (m122 = m1.n22) * m221 + (m123 = m1.n23) * m231;
		this.n22 = m121 * m212 + m122 * m222 + m123 * m232;
		this.n23 = m121 * m213 + m122 * m223 + m123 * m233;
		this.n24 = m121 * m214 + m122 * m224 + m123 * m234 + (m124 = m1.n24);
		this.n31 = (m131 = m1.n31) * m211 + (m132 = m1.n32) * m221 + (m133 = m1.n33) * m231;
		this.n32 = m131 * m212 + m132 * m222 + m133 * m232;
		this.n33 = m131 * m213 + m132 * m223 + m133 * m233;
		this.n34 = m131 * m214 + m132 * m224 + m133 * m234 + (m134 = m1.n34);
	}
	,multiply4x4: function(m1,m2) {
		var m111;
		var m211;
		var m121;
		var m221;
		var m131;
		var m231;
		var m141;
		var m241;
		var m112;
		var m212;
		var m122;
		var m222;
		var m132;
		var m232;
		var m142;
		var m242;
		var m113;
		var m213;
		var m123;
		var m223;
		var m133;
		var m233;
		var m143;
		var m243;
		var m114;
		var m214;
		var m124;
		var m224;
		var m134;
		var m234;
		var m144;
		var m244;
		this.n11 = (m111 = m1.n11) * (m211 = m2.n11) + (m112 = m1.n12) * (m221 = m2.n21) + (m113 = m1.n13) * (m231 = m2.n31) + (m114 = m1.n14) * (m241 = m2.n41);
		this.n12 = m111 * (m212 = m2.n12) + m112 * (m222 = m2.n22) + m113 * (m232 = m2.n32) + m114 * (m242 = m2.n42);
		this.n13 = m111 * (m213 = m2.n13) + m112 * (m223 = m2.n23) + m113 * (m233 = m2.n33) + m114 * (m243 = m2.n43);
		this.n14 = m111 * (m214 = m2.n14) + m112 * (m224 = m2.n24) + m113 * (m234 = m2.n34) + m114 * (m244 = m2.n44);
		this.n21 = (m121 = m1.n21) * m211 + (m122 = m1.n22) * m221 + (m123 = m1.n23) * m231 + (m124 = m1.n24) * m241;
		this.n22 = m121 * m212 + m122 * m222 + m123 * m232 + m124 * m242;
		this.n23 = m121 * m213 + m122 * m223 + m123 * m233 + m124 * m243;
		this.n24 = m121 * m214 + m122 * m224 + m123 * m234 + m124 * m244;
		this.n31 = (m131 = m1.n31) * m211 + (m132 = m1.n32) * m221 + (m133 = m1.n33) * m231 + (m134 = m1.n34) * m241;
		this.n32 = m131 * m212 + m132 * m222 + m133 * m232 + m134 * m242;
		this.n33 = m131 * m213 + m132 * m223 + m133 * m233 + m134 * m243;
		this.n34 = m131 * m214 + m132 * m224 + m133 * m234 + m134 * m244;
		this.n41 = (m141 = m1.n41) * m211 + (m142 = m1.n42) * m221 + (m143 = m1.n43) * m231 + (m144 = m1.n44) * m241;
		this.n42 = m141 * m212 + m142 * m222 + m143 * m232 + m144 * m242;
		this.n43 = m141 * m213 + m142 * m223 + m143 * m233 + m144 * m243;
		this.n44 = m141 * m214 + m142 * m224 + m143 * m234 + m144 * m244;
	}
	,copy: function(m) {
		this.n11 = m.n11;
		this.n12 = m.n12;
		this.n13 = m.n13;
		this.n14 = m.n14;
		this.n21 = m.n21;
		this.n22 = m.n22;
		this.n23 = m.n23;
		this.n24 = m.n24;
		this.n31 = m.n31;
		this.n32 = m.n32;
		this.n33 = m.n33;
		this.n34 = m.n34;
		return this;
	}
	,copy3x3: function(m) {
		this.n11 = m.n11;
		this.n12 = m.n12;
		this.n13 = m.n13;
		this.n21 = m.n21;
		this.n22 = m.n22;
		this.n23 = m.n23;
		this.n31 = m.n31;
		this.n32 = m.n32;
		this.n33 = m.n33;
		return this;
	}
	,multiplyTranspose3x3Vector: function(v) {
		var vx;
		var vy;
		var vz;
		v.x = (vx = v.x) * this.n11 + (vy = v.y) * this.n21 + (vz = v.z) * this.n31;
		v.y = vx * this.n12 + vy * this.n22 + vz * this.n32;
		v.z = vx * this.n13 + vy * this.n23 + vz * this.n33;
	}
	,multiplyVector: function(v) {
		var vx;
		var vy;
		var vz;
		v.x = (vx = v.x) * this.n11 + (vy = v.y) * this.n12 + (vz = v.z) * this.n13 + this.n14;
		v.y = vx * this.n21 + vy * this.n22 + vz * this.n23 + this.n24;
		v.z = vx * this.n31 + vy * this.n32 + vz * this.n33 + this.n34;
	}
	,multiplyVector3x3: function(v) {
		var vx;
		var vy;
		var vz;
		v.x = (vx = v.x) * this.n11 + (vy = v.y) * this.n12 + (vz = v.z) * this.n13;
		v.y = vx * this.n21 + vy * this.n22 + vz * this.n23;
		v.z = vx * this.n31 + vy * this.n32 + vz * this.n33;
	}
	,multiplyVector4x4: function(v) {
		var vx;
		var vy;
		var vz;
		v.x = (vx = v.x) * this.n11 + (vy = v.y) * this.n12 + (vz = v.z) * this.n13 + this.n14;
		v.y = vx * this.n21 + vy * this.n22 + vz * this.n23 + this.n24;
		v.z = vx * this.n31 + vy * this.n32 + vz * this.n33 + this.n34;
		var w = vx * this.n41 + vy * this.n42 + vz * this.n43 + this.n44;
		if(w != 0) v.scaleConstant(1 / w);
	}
	,det: function() {
		return (this.n11 * this.n22 - this.n21 * this.n12) * this.n33 - (this.n11 * this.n32 - this.n31 * this.n12) * this.n23 + (this.n21 * this.n32 - this.n31 * this.n22) * this.n13;
	}
	,toString: function(precision) {
		if(precision == null) precision = 5;
		return "";
	}
	,inverse: function(result) {
		if(result == null) result = new com_oddcast_host_engine3d_math_Matrix34(null);
		var d = this.det();
		if(Math.abs(d) < 0.001) {
			result.makeIdentity();
			return result;
		}
		d = 1 / d;
		result.n11 = d * (this.n22 * this.n33 - this.n32 * this.n23);
		result.n12 = -d * (this.n12 * this.n33 - this.n32 * this.n13);
		result.n13 = d * (this.n12 * this.n23 - this.n22 * this.n13);
		result.n14 = -d * (this.n12 * (this.n23 * this.n34 - this.n33 * this.n24) - this.n22 * (this.n13 * this.n34 - this.n33 * this.n14) + this.n32 * (this.n13 * this.n24 - this.n23 * this.n14));
		result.n21 = -d * (this.n21 * this.n33 - this.n31 * this.n23);
		result.n22 = d * (this.n11 * this.n33 - this.n31 * this.n13);
		result.n23 = -d * (this.n11 * this.n23 - this.n21 * this.n13);
		result.n24 = d * (this.n11 * (this.n23 * this.n34 - this.n33 * this.n24) - this.n21 * (this.n13 * this.n34 - this.n33 * this.n14) + this.n31 * (this.n13 * this.n24 - this.n23 * this.n14));
		result.n31 = d * (this.n21 * this.n32 - this.n31 * this.n22);
		result.n32 = -d * (this.n11 * this.n32 - this.n31 * this.n12);
		result.n33 = d * (this.n11 * this.n22 - this.n21 * this.n12);
		result.n34 = -d * (this.n11 * (this.n22 * this.n34 - this.n32 * this.n24) - this.n21 * (this.n12 * this.n34 - this.n32 * this.n14) + this.n31 * (this.n12 * this.n24 - this.n22 * this.n14));
		return result;
	}
	,quaternion2matrix: function(q) {
		var xx = q.x * q.x;
		var xy = q.x * q.y;
		var xz = q.x * q.z;
		var xw = q.x * q.w;
		var yy = q.y * q.y;
		var yz = q.y * q.z;
		var yw = q.y * q.w;
		var zz = q.z * q.z;
		var zw = q.z * q.w;
		this.n11 = 1 - 2 * (yy + zz);
		this.n12 = 2 * (xy - zw);
		this.n13 = 2 * (xz + yw);
		this.n21 = 2 * (xy + zw);
		this.n22 = 1 - 2 * (xx + zz);
		this.n23 = 2 * (yz - xw);
		this.n31 = 2 * (xz - yw);
		this.n32 = 2 * (yz + xw);
		this.n33 = 1 - 2 * (xx + yy);
	}
	,toQuat: function(quat) {
		quat.matrixArrayToQuat([[this.n11,this.n21,this.n31],[this.n12,this.n22,this.n32],[this.n13,this.n23,this.n33]]);
	}
	,__class__: com_oddcast_host_engine3d_math_Matrix34
};
var com_oddcast_host_engine3d_math_Quaternion = function(x,y,z,w) {
	if(w == null) w = 1.0;
	if(z == null) z = 0.0;
	if(y == null) y = 0.0;
	if(x == null) x = 0.0;
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
};
$hxClasses["com.oddcast.host.engine3d.math.Quaternion"] = com_oddcast_host_engine3d_math_Quaternion;
com_oddcast_host_engine3d_math_Quaternion.__name__ = ["com","oddcast","host","engine3d","math","Quaternion"];
com_oddcast_host_engine3d_math_Quaternion.prototype = {
	normalizeQuaternion: function() {
		var mag = this.magnitudeQuaternion();
		this.x /= mag;
		this.y /= mag;
		this.z /= mag;
		this.w /= mag;
		return this;
	}
	,magnitudeQuaternion: function() {
		return Math.sqrt(this.w * this.w + this.x * this.x + this.y * this.y + this.z * this.z);
	}
	,multiplyQuaternion: function(qa,qb) {
		var aw = qa.w;
		var ax = qa.x;
		var ay = qa.y;
		var az = qa.z;
		var bw = qb.w;
		var bx = qb.x;
		var by = qb.y;
		var bz = qb.z;
		this.w = aw * bw - ax * bx - ay * by - az * bz;
		this.x = aw * bx + ax * bw + ay * bz - az * by;
		this.y = aw * by - ax * bz + ay * bw + az * bx;
		this.z = aw * bz + ax * by - ay * bx + az * bw;
		return this;
	}
	,euler2quaternion: function(ax,ay,az) {
		var fSinPitch = Math.sin(ax * 0.5);
		var fCosPitch = Math.cos(ax * 0.5);
		var fSinYaw = Math.sin(ay * 0.5);
		var fCosYaw = Math.cos(ay * 0.5);
		var fSinRoll = Math.sin(az * 0.5);
		var fCosRoll = Math.cos(az * 0.5);
		var fCosPitchCosYaw = fCosPitch * fCosYaw;
		var fSinPitchSinYaw = fSinPitch * fSinYaw;
		this.x = fSinRoll * fCosPitchCosYaw - fCosRoll * fSinPitchSinYaw;
		this.y = fCosRoll * fSinPitch * fCosYaw + fSinRoll * fCosPitch * fSinYaw;
		this.z = fCosRoll * fCosPitch * fSinYaw - fSinRoll * fSinPitch * fCosYaw;
		this.w = fCosRoll * fCosPitchCosYaw + fSinRoll * fSinPitchSinYaw;
		return this;
	}
	,toEuler: function(result) {
		if(result == null) result = new com_oddcast_host_engine3d_math_Float3D();
		var sqx = this.x * this.x;
		var sqy = this.y * this.y;
		var sqz = this.z * this.z;
		var singularityTest = this.x * this.y + this.z * this.w;
		if(singularityTest > 0.449) {
			result.x = 2 * Math.atan2(this.x,this.w);
			result.y = Math.PI / 2;
			result.z = 0;
			return result;
		}
		if(singularityTest < -0.449) {
			result.x = -2 * Math.atan2(this.x,this.w);
			result.y = Math.PI / 2;
			result.z = 0;
			return result;
		}
		result.x = Math.atan2(2 * this.y * this.w - 2 * this.x * this.z,1 - 2 * sqy - 2 * sqz);
		result.y = Math.asin(2 * this.x * this.y + 2 * this.z * this.w);
		result.z = Math.atan2(2 * this.x * this.w - 2 * this.y * this.z,1 - 2 * sqx - 2 * sqz);
		return result;
	}
	,axisAngle2quaternion: function(axis,radiens) {
		var halfRad = radiens * 0.5;
		this.w = Math.cos(halfRad);
		var sinHalfRad = Math.sin(halfRad);
		this.x = axis.x * sinHalfRad;
		this.y = axis.y * sinHalfRad;
		this.z = axis.z * sinHalfRad;
		return this.normalizeQuaternion();
	}
	,matrixArrayToQuat: function(m) {
		var s = 1.0;
		var q = [0.0,0.0,0.0,1.0];
		var i = 0;
		var j = 0;
		var k = 0;
		var nxt = [1,2,0];
		var tr = m[0][0] + m[1][1] + m[2][2];
		if(tr > 0.0) {
			s = Math.sqrt(tr + 1.0);
			this.w = s / 2.0;
			s = 0.5 / s;
			this.x = (m[1][2] - m[2][1]) * s;
			this.y = (m[2][0] - m[0][2]) * s;
			this.z = (m[0][1] - m[1][0]) * s;
		} else {
			i = 0;
			if(m[1][1] > m[0][0]) i = 1;
			if(m[2][2] > m[i][i]) i = 2;
			j = nxt[i];
			k = nxt[j];
			s = Math.sqrt(m[i][i] - (m[j][j] + m[k][k]) + 1.0);
			q[i] = s * 0.5;
			if(s != 0.0) s = 0.5 / s;
			q[3] = (m[j][k] - m[k][j]) * s;
			q[j] = (m[i][j] + m[j][i]) * s;
			q[k] = (m[i][k] + m[k][i]) * s;
			this.x = q[0];
			this.y = q[1];
			this.z = q[2];
			this.w = q[3];
		}
	}
	,toString: function(nSigFig) {
		if(nSigFig == null) nSigFig = 3;
		return " quat: x:" + com_oddcast_util_Utils.formatFloat(this.x,nSigFig) + " y:" + com_oddcast_util_Utils.formatFloat(this.y,nSigFig) + " z:" + com_oddcast_util_Utils.formatFloat(this.z,nSigFig) + " w:" + com_oddcast_util_Utils.formatFloat(this.w,nSigFig);
	}
	,slerp: function(qa,qb,t) {
		var aw = qa.w;
		var ax = qa.x;
		var ay = qa.y;
		var az = qa.z;
		var bw = qb.w;
		var bx = qb.x;
		var by = qb.y;
		var bz = qb.z;
		var cosom = ax * bx + ay * by + az * bz + aw * bw;
		if(cosom < 0.0) {
			cosom = -cosom;
			bx = -bx;
			by = -by;
			bz = -bz;
			bw = -bw;
		}
		var scale0 = 1.0 - t;
		var scale1 = t;
		if(1.0 - cosom > 0.0001) {
			var omega = Math.acos(cosom);
			var sinom = 1 / Math.sin(omega);
			scale0 = Math.sin(scale0 * omega) * sinom;
			scale1 = Math.sin(scale1 * omega) * sinom;
		}
		this.x = scale0 * ax + scale1 * bx;
		this.y = scale0 * ay + scale1 * by;
		this.z = scale0 * az + scale1 * bz;
		this.w = scale0 * aw + scale1 * bw;
	}
	,toFloatArray: function() {
		return [this.x,this.y,this.z,this.w];
	}
	,__class__: com_oddcast_host_engine3d_math_Quaternion
};
var com_oddcast_host_engine3d_math_Rectangle = function() {
	this.left = this.right = this.top = this.bottom = 0;
};
$hxClasses["com.oddcast.host.engine3d.math.Rectangle"] = com_oddcast_host_engine3d_math_Rectangle;
com_oddcast_host_engine3d_math_Rectangle.__name__ = ["com","oddcast","host","engine3d","math","Rectangle"];
com_oddcast_host_engine3d_math_Rectangle.prototype = {
	set: function(x,y,w,h) {
		if(h == null) h = 0;
		if(w == null) w = 0;
		this.left = x;
		this.top = y;
		this.right = x + w;
		this.bottom = y + h;
		return this;
	}
	,makeEmpty: function() {
		this.left = this.top = Infinity;
		this.right = this.bottom = -Infinity;
	}
	,includeFloat2D: function(f2D) {
		this.includePoint(f2D.u,f2D.v);
	}
	,includePoint: function(x,y) {
		this.left = Math.min(this.left,x);
		this.top = Math.min(this.top,y);
		this.right = Math.max(this.right,x);
		this.bottom = Math.max(this.bottom,y);
	}
	,isInside: function(x,y,tolerance) {
		tolerance = com_oddcast_util_Utils.defaultParamFloat(tolerance,0.0);
		return this.left - tolerance <= x && x <= this.right + tolerance && this.top - tolerance <= y && y <= this.bottom + tolerance;
	}
	,toString: function() {
		return new String(this.left + "," + this.top + ", " + this.right + ", " + this.bottom + " : " + this.width() + "," + this.height());
	}
	,width: function() {
		return this.right - this.left;
	}
	,height: function() {
		return this.bottom - this.top;
	}
	,__class__: com_oddcast_host_engine3d_math_Rectangle
};
var com_oddcast_host_morph_TargetList = function(smoothingWeight,sync,child) {
	this.smoothingWeight = smoothingWeight;
	this.child = child;
	this.sync = sync;
};
$hxClasses["com.oddcast.host.morph.TargetList"] = com_oddcast_host_morph_TargetList;
com_oddcast_host_morph_TargetList.__name__ = ["com","oddcast","host","morph","TargetList"];
com_oddcast_host_morph_TargetList.prototype = {
	promoteGrandChildToChild: function() {
		this.child = this.child.child;
		return this;
	}
	,setAsSoleWeighting: function() {
		this.child = null;
		this.smoothingWeight = 1.0;
	}
	,setPartialWeighting: function(midTime) {
		if(midTime < this.sync.peakTime) this.smoothingWeight = (midTime - this.sync.startTime) / (this.sync.peakTime - this.sync.startTime); else this.smoothingWeight = (this.sync.endTime - midTime) / (this.sync.endTime - this.sync.peakTime);
		this.smoothingWeight = this.clampZeroOne(this.smoothingWeight);
		if(this.child != null) this.child.setPartialWeighting(midTime);
	}
	,clampZeroOne: function(input) {
		if(input > 1.0) return 1.0; else if(input < 0.0) return 0.0; else return input;
	}
	,setSmoothingWeighting: function(time) {
		var prevTime;
		if(this.child != null) prevTime = this.child.sync.peakTime; else prevTime = this.sync.startTime - com_oddcast_host_morph_TargetList.MAX_DECAY;
		if(prevTime < this.sync.startTime - com_oddcast_host_morph_TargetList.MAX_DECAY) prevTime = this.sync.startTime - com_oddcast_host_morph_TargetList.MAX_DECAY;
		var thisTime = Math.min(this.sync.startTime + com_oddcast_host_morph_TargetList.MAX_ATTACK,this.sync.peakTime);
		var interval = thisTime - prevTime;
		var weightNext;
		if(interval > 0) weightNext = (time - prevTime) / interval; else weightNext = 0;
		this.smoothingWeight = this.clampAndSmooth(weightNext);
		if(this.smoothingWeight > 0.99) this.child = null;
		if(this.child != null) {
			this.child.smoothingWeight = 1 - this.smoothingWeight;
			this.child.child = null;
		}
		return this.smoothingWeight;
	}
	,setSmoothingWeightingMoods: function(time) {
		this.smoothingWeight = this.clampAndSmooth(this.sync.calcPositionWithinAttackDecay(time));
		haxe_Log.trace("FACEMORPH: " + com_oddcast_util_UtilsLite.formatFloat(this.smoothingWeight) + " " + time,{ fileName : "TargetList.hx", lineNumber : 91, className : "com.oddcast.host.morph.TargetList", methodName : "setSmoothingWeightingMoods"});
		if(this.child != null) this.child.setSmoothingWeightingMoods(time);
	}
	,setToDecay: function(lastTime) {
		this.sync.setToDecay(lastTime);
		if(this.child != null) this.child.setToDecay(lastTime);
	}
	,isEmpty: function(minimumWeight) {
		return this.smoothingWeight <= minimumWeight;
	}
	,cullEmpty: function(minimumWeight) {
		while(this.child != null) if(this.child.isEmpty(minimumWeight)) this.promoteGrandChildToChild(); else break;
		if(this.child != null) this.child.cullEmpty(minimumWeight);
		if(this.isEmpty(minimumWeight)) return this.child; else return this;
	}
	,clampAndSmooth: function(weightNext) {
		if(weightNext > 1) weightNext = 1;
		if(weightNext < 0) weightNext = 0;
		var w2 = weightNext * weightNext;
		weightNext = 3 * w2 - 2 * w2 * weightNext;
		return weightNext;
	}
	,getDominant: function(dom) {
		if(dom == null) dom = 0.0;
		var retval = null;
		var ourDom;
		if(this.peakIsInTime) ourDom = this.sync.getDominance(); else ourDom = 0.0;
		if(ourDom > dom) {
			retval = this;
			dom = ourDom;
		}
		var childDom;
		if(this.child != null) childDom = this.child.getDominant(dom); else childDom = null;
		if(childDom != null) return childDom;
		return retval;
	}
	,getLongest: function(start,end,all,longest) {
		if(longest == null) longest = 0.0;
		if(all == null) all = true;
		var retval = null;
		var ourLength;
		if(all || this.peakIsInTime) ourLength = Math.min(this.sync.endTime,end) - Math.max(this.sync.startTime,start); else ourLength = -1.0;
		if(ourLength >= longest) {
			retval = this;
			longest = ourLength;
		}
		var longerChild;
		if(this.child != null) longerChild = this.child.getLongest(start,end,all,longest); else longerChild = null;
		if(longerChild != null) return longerChild;
		return retval;
	}
	,setPeakIsInTime: function(start,end) {
		this.peakIsInTime = start < this.sync.peakTime && this.sync.peakTime <= end;
		if(this.child != null) this.child.setPeakIsInTime(start,end);
	}
	,purge: function(time) {
		if(this.child != null) this.child = this.child.purge(time);
		if(time < this.sync.endTime) return this; else return null;
	}
	,getListLength: function(all,i) {
		if(i == null) i = 0;
		if(this.child != null) i = this.child.getListLength(all,i);
		if(all || this.peakIsInTime) i++;
		return i;
	}
	,getLastSync: function() {
		if(this.child != null) return this.child.getLastSync();
		return this.sync;
	}
	,toString: function() {
		return " " + com_oddcast_util_UtilsLite.formatFloat(this.smoothingWeight,2) + " " + this.sync.eventString + " " + Std.string(this.child != null?"; ":null);
	}
	,__class__: com_oddcast_host_morph_TargetList
};
var com_oddcast_host_morph_lipsync_Emphasis = function() {
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "Emphasis.hx", lineNumber : 34, className : "com.oddcast.host.morph.lipsync.Emphasis", methodName : "new"});
};
$hxClasses["com.oddcast.host.morph.lipsync.Emphasis"] = com_oddcast_host_morph_lipsync_Emphasis;
com_oddcast_host_morph_lipsync_Emphasis.__name__ = ["com","oddcast","host","morph","lipsync","Emphasis"];
com_oddcast_host_morph_lipsync_Emphasis.calcAccents = function(timedVisemes,syncData,secondsPerAccent) {
	var realLength = timedVisemes.getRealLength();
	var accentStrengths = [];
	var maxAccents = 1000;
	if(accentStrengths.length == 0) {
		var _g1 = 0;
		var _g = syncData.length;
		while(_g1 < _g) {
			var iSync = _g1++;
			var accentStrength = com_oddcast_host_morph_lipsync_Emphasis.calcAccentPotential(syncData,iSync);
			if(accentStrength != null) accentStrengths.push(accentStrength);
		}
		var MINIMAL_DISTANCE = 500.0;
		var _g11 = 0;
		var _g2 = accentStrengths.length;
		while(_g11 < _g2) {
			var iAccentStrength = _g11++;
			accentStrengths[iAccentStrength].removeIfLesserWithin(MINIMAL_DISTANCE,iAccentStrength,accentStrengths);
		}
		accentStrengths.sort(function(a,b) {
			if(a.accentStrength < b.accentStrength) return 1;
			return -1;
		});
		maxAccents = Math.round(realLength / (1000 * secondsPerAccent));
	}
	var audioAccents = [];
	var _g3 = 0;
	while(_g3 < accentStrengths.length) {
		var accentStrength1 = accentStrengths[_g3];
		++_g3;
		haxe_Log.trace("accentStrength" + Std.string(accentStrength1),{ fileName : "Emphasis.hx", lineNumber : 102, className : "com.oddcast.host.morph.lipsync.Emphasis", methodName : "calcAccents"});
		audioAccents.push(accentStrength1.makeAudioAccent(accentStrength1.accentStrength));
		if(audioAccents.length >= maxAccents) break;
	}
	return audioAccents;
};
com_oddcast_host_morph_lipsync_Emphasis.calcAccentPotential = function(syncData,iSync) {
	var syncDatum = syncData[iSync];
	if(!syncDatum.isPhoneme() || syncDatum.isSilent()) return null;
	if(syncDatum.eventString == "z") {
		var junk = 1;
	}
	var height = 0.0;
	if(com_oddcast_util_VectorTools.boundsCheck(syncData,iSync + 1)) {
		var syncDataNext = syncData[iSync + 1];
		if(syncDataNext.normalizedEnergy < syncDatum.normalizedEnergy) {
			if(iSync - 1 >= 0) {
				var prevValue = syncData[iSync - 1];
				if(!prevValue.isPhoneme() || prevValue.isSilent()) return null;
				height = syncDatum.normalizedEnergy - prevValue.normalizedEnergy;
				if(height >= 0) {
					if(iSync - 2 >= 0) {
						var prevprevValue = syncData[iSync - 2];
						if(prevprevValue.isPhoneme() && !prevprevValue.isSilent()) {
							var height2 = prevValue.normalizedEnergy - prevprevValue.normalizedEnergy;
							if(height2 >= 0) height += height2;
						}
					}
					return new com_oddcast_host_morph_lipsync_SyncDatumAccentStrength(height,syncDatum);
				} else height = 0;
			}
		}
	}
	return null;
};
com_oddcast_host_morph_lipsync_Emphasis.prototype = {
	extractWordAccents: function(wordData,origTxt) {
		this.DEBUG_traceWordsFromWordData(wordData);
		var wordAccents = this.getWordAccents(origTxt);
		this.DEBUG_traceWordAccentList(wordAccents);
		var accentStrengths = this.matchAccentStrengths(wordAccents,wordData);
		return accentStrengths;
	}
	,getWordAccents: function(origTxt) {
		var wordAccents = [];
		if(origTxt != null && origTxt.length > 1) {
			var startTagSplit = origTxt.split(com_oddcast_host_morph_lipsync_Emphasis.OPEN_TAG_STRONG);
			if(startTagSplit.length == 1) startTagSplit = origTxt.split(com_oddcast_host_morph_lipsync_Emphasis.OPEN_TAG);
			var bFirstSegIsAccent = startTagSplit[0].length == 0;
			var _g = 0;
			while(_g < startTagSplit.length) {
				var seg = startTagSplit[_g];
				++_g;
				var punc = [com_oddcast_host_morph_lipsync_Emphasis.CLOSE_TAG];
				var _g1 = 0;
				while(_g1 < punc.length) {
					var p = punc[_g1];
					++_g1;
					seg = StringTools.replace(seg,p," ");
				}
				var cleanSeg = "";
				var _g2 = 0;
				var _g11 = seg.length;
				while(_g2 < _g11) {
					var iC = _g2++;
					var c = seg.charAt(iC);
					var pos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ".indexOf(c);
					if(pos >= 0) cleanSeg += c; else cleanSeg += " ";
				}
				var words = cleanSeg.split(" ");
				var bFirst = bFirstSegIsAccent;
				var _g12 = 0;
				while(_g12 < words.length) {
					var w = words[_g12];
					++_g12;
					if(w.length > 0) {
						var accentStrength;
						if(bFirst == true) accentStrength = com_oddcast_host_morph_lipsync_Emphasis.DEFAULT_ACCENT_STRENGTH; else accentStrength = 0;
						var wordAccent = { word : w, accentStrength : accentStrength};
						bFirst = false;
						wordAccents.push(wordAccent);
					}
				}
				bFirstSegIsAccent = true;
			}
		}
		return wordAccents;
	}
	,traceStringCharacters: function(str) {
		var _g1 = 0;
		var _g = str.length;
		while(_g1 < _g) {
			var i = _g1++;
			haxe_Log.trace(i + " " + str.charAt(i) + "   0x" + StringTools.hex(HxOverrides.cca(str,i)),{ fileName : "Emphasis.hx", lineNumber : 225, className : "com.oddcast.host.morph.lipsync.Emphasis", methodName : "traceStringCharacters"});
		}
	}
	,matchAccentStrengths: function(wordAccents,wordData) {
		var accentStrengths = [];
		var wordDataIndex = 0;
		var _g = 0;
		while(_g < wordAccents.length) {
			var txtword = wordAccents[_g];
			++_g;
			var w = txtword.word;
			var attempted = "";
			var _g1 = 0;
			while(_g1 < 6) {
				var checkWord = _g1++;
				if(wordDataIndex + checkWord >= wordData.length) break;
				var matchWordDatum = wordData[wordDataIndex + checkWord];
				attempted += matchWordDatum.eventString + " ";
				if(txtword.word == matchWordDatum.eventString) {
					if(txtword.accentStrength > com_oddcast_host_morph_lipsync_Emphasis.MINIMUM_ACCENT_STRENGTH) accentStrengths.push(new com_oddcast_host_morph_lipsync_WordAccentStrength(txtword.accentStrength,matchWordDatum));
					wordDataIndex += checkWord;
					attempted = null;
					break;
				}
			}
			if(attempted != null) {
				var junk = 1;
			}
		}
		return accentStrengths;
	}
	,DEBUG_traceWordAccentList: function(wordAccents) {
		var allwords = "";
		var _g = 0;
		while(_g < wordAccents.length) {
			var wordAccent = wordAccents[_g];
			++_g;
			var word = wordAccent.word;
			if(wordAccent.accentStrength > com_oddcast_host_morph_lipsync_Emphasis.MINIMUM_ACCENT_STRENGTH) word = word.toUpperCase();
			allwords += word + " ";
		}
	}
	,DEBUG_traceWordsFromWordData: function(wordData) {
		var allwords = "";
		var _g = 0;
		while(_g < wordData.length) {
			var word = wordData[_g];
			++_g;
			allwords += word.eventString + " ";
		}
	}
	,__class__: com_oddcast_host_morph_lipsync_Emphasis
};
var com_oddcast_host_morph_lipsync_AudioAccent = function(timestamp,strength) {
	this.timestamp = timestamp;
	this.strength = strength;
};
$hxClasses["com.oddcast.host.morph.lipsync.AudioAccent"] = com_oddcast_host_morph_lipsync_AudioAccent;
com_oddcast_host_morph_lipsync_AudioAccent.__name__ = ["com","oddcast","host","morph","lipsync","AudioAccent"];
com_oddcast_host_morph_lipsync_AudioAccent.prototype = {
	toString: function() {
		return " t:" + com_oddcast_util_UtilsLite.formatFloat(this.timestamp) + " s:" + com_oddcast_util_UtilsLite.formatFloat(this.strength);
	}
	,__class__: com_oddcast_host_morph_lipsync_AudioAccent
};
var com_oddcast_host_morph_lipsync_SyncDatumAccentStrength = function(accentStrength,syncDatum) {
	this.accentStrength = accentStrength;
	this.syncDatum = syncDatum;
};
$hxClasses["com.oddcast.host.morph.lipsync.SyncDatumAccentStrength"] = com_oddcast_host_morph_lipsync_SyncDatumAccentStrength;
com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.__name__ = ["com","oddcast","host","morph","lipsync","SyncDatumAccentStrength"];
com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.prototype = {
	makeAudioAccent: function(strength) {
		return new com_oddcast_host_morph_lipsync_AudioAccent(this.syncDatum.startTime - com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.VISUAL_AHEAD_OF_AUDIO,strength);
	}
	,removeIfLesserWithin: function(minDistance,iAccentStrength,accentStrengths) {
		var _g1 = iAccentStrength + 1;
		var _g = accentStrengths.length;
		while(_g1 < _g) {
			var i1 = _g1++;
			if(this.removeIfLesserThan(accentStrengths[i1],minDistance)) break;
		}
		var i = iAccentStrength - 1;
		while(i >= 0) {
			if(this.removeIfLesserThan(accentStrengths[i],minDistance)) break;
			i--;
		}
	}
	,removeIfLesserThan: function(other,minDistance) {
		if(Math.abs(this.syncDatum.peakTime - other.syncDatum.peakTime) > minDistance) return true;
		if(this.accentStrength < other.accentStrength) this.accentStrength = 0;
		return false;
	}
	,__class__: com_oddcast_host_morph_lipsync_SyncDatumAccentStrength
};
var com_oddcast_host_morph_lipsync_WordAccentStrength = function(accentStrength,syncDatum) {
	com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.call(this,accentStrength,syncDatum);
};
$hxClasses["com.oddcast.host.morph.lipsync.WordAccentStrength"] = com_oddcast_host_morph_lipsync_WordAccentStrength;
com_oddcast_host_morph_lipsync_WordAccentStrength.__name__ = ["com","oddcast","host","morph","lipsync","WordAccentStrength"];
com_oddcast_host_morph_lipsync_WordAccentStrength.__super__ = com_oddcast_host_morph_lipsync_SyncDatumAccentStrength;
com_oddcast_host_morph_lipsync_WordAccentStrength.prototype = $extend(com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.prototype,{
	makeAudioAccent: function(strength) {
		return new com_oddcast_host_morph_lipsync_AudioAccent((this.syncDatum.startTime + this.syncDatum.endTime) / 2.0 - com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.VISUAL_AHEAD_OF_AUDIO,strength);
	}
	,__class__: com_oddcast_host_morph_lipsync_WordAccentStrength
});
var com_oddcast_host_morph_sync_TimedFaceBase = function(name,fvisemeAdvance) {
	if(fvisemeAdvance == null) fvisemeAdvance = 0.0;
	com_oddcast_host_HXEventLite.call(this,name);
	this.visemeAdvance = fvisemeAdvance;
	this.reset();
};
$hxClasses["com.oddcast.host.morph.sync.TimedFaceBase"] = com_oddcast_host_morph_sync_TimedFaceBase;
com_oddcast_host_morph_sync_TimedFaceBase.__name__ = ["com","oddcast","host","morph","sync","TimedFaceBase"];
com_oddcast_host_morph_sync_TimedFaceBase.__super__ = com_oddcast_host_HXEventLite;
com_oddcast_host_morph_sync_TimedFaceBase.prototype = $extend(com_oddcast_host_HXEventLite.prototype,{
	isReady: function() {
		return true;
	}
	,reset: function() {
		this.syncData = [];
		this.syncIndex = 0;
	}
	,adjustTime: function(time,frameUpdate) {
		return time + this.visemeAdvance;
	}
	,play: function(startTime) {
		this.syncIndex = 0;
	}
	,__class__: com_oddcast_host_morph_sync_TimedFaceBase
});
var com_oddcast_host_morph_lipsync_TimedVisemes = function(fVisemeAdvance) {
	com_oddcast_host_morph_sync_TimedFaceBase.call(this,"viseme",fVisemeAdvance);
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(false,{ fileName : "TimedVisemes.hx", lineNumber : 38, className : "com.oddcast.host.morph.lipsync.TimedVisemes", methodName : "new"});
	this.bHasTranscript = false;
	this.restart();
};
$hxClasses["com.oddcast.host.morph.lipsync.TimedVisemes"] = com_oddcast_host_morph_lipsync_TimedVisemes;
com_oddcast_host_morph_lipsync_TimedVisemes.__name__ = ["com","oddcast","host","morph","lipsync","TimedVisemes"];
com_oddcast_host_morph_lipsync_TimedVisemes.__super__ = com_oddcast_host_morph_sync_TimedFaceBase;
com_oddcast_host_morph_lipsync_TimedVisemes.prototype = $extend(com_oddcast_host_morph_sync_TimedFaceBase.prototype,{
	isTTS: function() {
		return this.bHasTranscript;
	}
	,extract: function(id3Comment,power,maxEnergy,minEnergy) {
		if(minEnergy == null) minEnergy = 0.60;
		if(maxEnergy == null) maxEnergy = 2.00;
		if(power == null) power = 3.0;
		var timedPhonemesID3 = id3Comment.extract("timed_phonemes");
		this.extractTimedPhonemes(timedPhonemesID3,minEnergy,maxEnergy,power);
		this.origTxt = id3Comment.extract(com_oddcast_audio_ID3comment.TEXT);
		if(this.origTxt != null) {
			haxe_Log.trace(this.origTxt,{ fileName : "TimedVisemes.hx", lineNumber : 86, className : "com.oddcast.host.morph.lipsync.TimedVisemes", methodName : "extract"});
			var junk = 1;
		}
	}
	,extractTimedPhonemes: function(timedPhonemesID3,minEnergy,maxEnergy,power) {
		if(timedPhonemesID3 != null) {
			var lines = timedPhonemesID3.split("\t");
			var _g = 0;
			while(_g < lines.length) {
				var line = lines[_g];
				++_g;
				this.addToSyncData(line,minEnergy,maxEnergy,power);
			}
			if(this.bHasTranscript) {
				this.syncData = this.fixupPhonemes(this.syncData);
				if(this.isTTS()) this.TTSVoiceNormalize(this.syncData,0.8);
			}
		}
	}
	,addToSyncData: function(line,minEnergy,maxEnergy,power) {
		var syncDatum = new com_oddcast_host_morph_sync_SyncDatum();
		if(syncDatum.extractFromCompressedString(line,minEnergy,maxEnergy,power) != com_oddcast_host_morph_sync_SyncDatum.NO_EVENT) this.syncData.push(syncDatum);
		this.bHasTranscript = this.bHasTranscript || syncDatum.isWord();
	}
	,cropSyncData: function(startMillis,endMillis) {
		this.syncData = this.syncData.filter(function(syncDatum) {
			return syncDatum.startTime >= startMillis && syncDatum.endTime <= endMillis;
		});
	}
	,getRealLength: function() {
		var realLength = -1.0;
		var _g = 0;
		var _g1 = this.syncData;
		while(_g < _g1.length) {
			var sync = _g1[_g];
			++_g;
			if(sync.isPhoneme() && !sync.isSilent()) realLength = sync.endTime;
		}
		return realLength;
	}
	,extractSentences: function() {
		return this.extractDataType(com_oddcast_host_morph_sync_SyncDatum.SENTENCE);
	}
	,extractWords: function() {
		return this.extractDataType(com_oddcast_host_morph_sync_SyncDatum.WORD);
	}
	,extractDataType: function(typeStr) {
		var results = [];
		var _g = 0;
		var _g1 = this.syncData;
		while(_g < _g1.length) {
			var syncDatum = _g1[_g];
			++_g;
			if(syncDatum.typeStr == typeStr) results.push(syncDatum);
		}
		return results;
	}
	,extractSilence: function(minPause) {
		var results = [];
		var _g = 0;
		var _g1 = this.syncData;
		while(_g < _g1.length) {
			var syncDatum = _g1[_g];
			++_g;
			if(syncDatum.isSilent()) {
				if(syncDatum.duration() >= minPause) results.push(syncDatum);
			}
		}
		return results;
	}
	,extractBetweenPauses: function(minPause) {
		var results = [];
		var currSentenceDatum = null;
		var _g = 0;
		var _g1 = this.syncData;
		while(_g < _g1.length) {
			var syncDatum = _g1[_g];
			++_g;
			if(syncDatum.isSilent()) {
				if(currSentenceDatum != null) {
					currSentenceDatum.endTime = syncDatum.startTime;
					if(syncDatum.duration() >= minPause) {
						results.push(currSentenceDatum);
						currSentenceDatum = null;
					}
				}
				if(currSentenceDatum == null) currSentenceDatum = this.createBetweenPauseSyncDatum(syncDatum.endTime);
			} else if(syncDatum.typeStr == com_oddcast_host_morph_sync_SyncDatum.PHONE) {
				if(currSentenceDatum == null) {
					if(results.length == 0) currSentenceDatum = this.createBetweenPauseSyncDatum(syncDatum.startTime);
				}
			}
		}
		if(currSentenceDatum != null) {
			currSentenceDatum.endTime = com_oddcast_util_VectorTools.lastElement(this.syncData).endTime;
			if(currSentenceDatum.duration() >= minPause) {
				results.push(currSentenceDatum);
				currSentenceDatum = null;
			}
		}
		return results;
	}
	,createBetweenPauseSyncDatum: function(startTime) {
		var sentenceDatum = new com_oddcast_host_morph_sync_SyncDatum();
		sentenceDatum.typeStr = com_oddcast_host_morph_sync_SyncDatum.BETWEEN_PAUSE;
		sentenceDatum.startTime = startTime;
		return sentenceDatum;
	}
	,TTSVoiceNormalize: function(syncData,normalizeTo) {
		if(normalizeTo == null) normalizeTo = 1.00;
		var totalEnergy = 0.0;
		var totalVisemes = 0;
		var _g = 0;
		while(_g < syncData.length) {
			var sync = syncData[_g];
			++_g;
			if(sync.isPhoneme() && !sync.isSilent()) {
				totalEnergy += sync.energy;
				totalVisemes++;
			}
		}
		var oneOverNorm = totalVisemes / totalEnergy;
		haxe_Log.trace("Average phoneme energy before:" + 1 / oneOverNorm,{ fileName : "TimedVisemes.hx", lineNumber : 278, className : "com.oddcast.host.morph.lipsync.TimedVisemes", methodName : "TTSVoiceNormalize"});
		var _g1 = 0;
		while(_g1 < syncData.length) {
			var sync1 = syncData[_g1];
			++_g1;
			if(sync1.isPhoneme() && !sync1.isSilent()) sync1.energy *= normalizeTo * oneOverNorm;
		}
	}
	,fixupPhonemes: function(syncData) {
		var length = syncData.length;
		var ph = 0;
		while(ph < length) {
			var datum = syncData[ph];
			if(datum.typeStr == com_oddcast_host_morph_sync_SyncDatum.PHONE) {
				if(datum.eventString == "p") {
					var nextPh = syncData[ph + 1];
					if(nextPh == null || !nextPh.isPhoneme() || nextPh.isSilent() || nextPh.isCloseMouthed()) {
						var insertDatum = datum.clone();
						datum.endTime = insertDatum.startTime = datum.startTime + (datum.endTime - datum.startTime) * 0.7;
						datum.calcValues();
						insertDatum.eventString = "^";
						insertDatum.setPlabel();
						insertDatum.energy * 0.6;
						insertDatum.calcValues();
						syncData.splice(ph + 1,0,insertDatum);
					}
				}
				if(datum.isCloseMouthed()) {
					var nextPh1 = syncData[ph + 1];
					if(nextPh1 != null && nextPh1.isPhoneme() && !nextPh1.isSilent() && !nextPh1.isCloseMouthed()) nextPh1.setPeakTime(0.95);
				}
			}
			ph++;
		}
		return syncData;
	}
	,play: function(startTime) {
		com_oddcast_host_morph_sync_TimedFaceBase.prototype.play.call(this,startTime);
		this.restart();
	}
	,restart: function() {
		this.lastsoundPosition = 0;
		this.lastTime = -1;
		this.keyFrame = 0;
		this.phonTargetList = null;
		this.currentIntervalIndex = 0;
		this.soundPositionIntervals = [];
		var _g1 = 0;
		var _g = com_oddcast_host_morph_lipsync_TimedVisemes.TOTAL_INTERVALS;
		while(_g1 < _g) {
			var i = _g1++;
			this.soundPositionIntervals[i] = 0.0;
		}
	}
	,adjustTime: function(soundPosition,frameUpdate) {
		var interval = soundPosition - this.lastsoundPosition;
		this.soundPositionIntervals[this.currentIntervalIndex++] = interval;
		if(this.currentIntervalIndex == com_oddcast_host_morph_lipsync_TimedVisemes.TOTAL_INTERVALS) this.currentIntervalIndex = 0;
		this.lastsoundPosition = soundPosition;
		var screenAdvanceTime = 0.0;
		var _g = 0;
		var _g1 = this.soundPositionIntervals;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			screenAdvanceTime += i;
		}
		screenAdvanceTime *= 1.5 / com_oddcast_host_morph_lipsync_TimedVisemes.TOTAL_INTERVALS;
		return com_oddcast_host_morph_sync_TimedFaceBase.prototype.adjustTime.call(this,soundPosition + screenAdvanceTime,frameUpdate);
	}
	,findCurrentTarget: function(time,frameUpdate) {
		var interval = Math.max(0.0,time - this.lastTime);
		this.lastTime = time;
		if(time < 500) {
			var junk = 1;
		}
		var targetList = this.phonTargetList;
		if(targetList != null) targetList.setPeakIsInTime(time - interval,time);
		var sync;
		if(this.keyFrame < this.syncData.length) sync = this.syncData[this.keyFrame]; else sync = null;
		while(sync != null && (!sync.isPhoneme() || time > sync.startTime)) {
			var _g = sync.typeStr;
			switch(_g) {
			case "P":
				if(targetList != null) targetList = targetList.purge(time - interval);
				targetList = new com_oddcast_host_morph_TargetList(1.0,sync,targetList);
				targetList.setPeakIsInTime(time - interval,time);
				if(frameUpdate != null) {
					if(sync.plabel == HxOverrides.cca("x",0)) frameUpdate.sinceTalkingMillis = time - sync.peakTime; else frameUpdate.sinceTalkingMillis = 0.0;
				}
				break;
			case "W":
				this.wordDatum = sync;
				haxe_Log.trace("MouthBlendShape  word: " + this.wordDatum.eventString,{ fileName : "TimedVisemes.hx", lineNumber : 546, className : "com.oddcast.host.morph.lipsync.TimedVisemes", methodName : "findCurrentTarget"});
				break;
			case "S":
				this.sentenceDatum = sync;
				break;
			}
			this.keyFrame++;
			if(this.keyFrame < this.syncData.length) sync = this.syncData[this.keyFrame]; else sync = null;
		}
		this.phonTargetList = targetList;
		var retTargetList = targetList;
		if(targetList != null) {
			var allTargets = targetList.getListLength(true);
			var peakTargets = targetList.getListLength(false);
			var midTime = time - interval / 2;
			var peakDominant = targetList.getDominant();
			if(peakDominant != null) {
				retTargetList = peakDominant;
				retTargetList.setAsSoleWeighting();
			} else switch(allTargets) {
			case 1:
				switch(peakTargets) {
				case 0:
					targetList.setSmoothingWeighting(midTime);
					break;
				case 1:
					targetList.setAsSoleWeighting();
					break;
				}
				break;
			case 2:
				targetList.setSmoothingWeighting(midTime);
				break;
			default:
				retTargetList = targetList.getLongest(time - interval,time,false);
				if(retTargetList == null) retTargetList = targetList.getLongest(time - interval,time,true);
				if(retTargetList != null) retTargetList.setAsSoleWeighting();
			}
			if(retTargetList != null) {
			}
		} else {
		}
		return retTargetList;
	}
	,getRecommendedMinimumVisemeDuration: function() {
		if(this.bHasTranscript) return com_oddcast_host_morph_lipsync_TimedVisemes.TRANSCRIPT_MINIMUM_VISEME_DURATION; else return com_oddcast_host_morph_lipsync_TimedVisemes.MINIMUM_VISEME_DURATION;
	}
	,__class__: com_oddcast_host_morph_lipsync_TimedVisemes
});
var com_oddcast_host_morph_lipsync_TimedHeadMovements = function(name,fvisemeAdvance) {
	com_oddcast_host_morph_sync_TimedFaceBase.call(this,name,fvisemeAdvance);
};
$hxClasses["com.oddcast.host.morph.lipsync.TimedHeadMovements"] = com_oddcast_host_morph_lipsync_TimedHeadMovements;
com_oddcast_host_morph_lipsync_TimedHeadMovements.__name__ = ["com","oddcast","host","morph","lipsync","TimedHeadMovements"];
com_oddcast_host_morph_lipsync_TimedHeadMovements.__super__ = com_oddcast_host_morph_sync_TimedFaceBase;
com_oddcast_host_morph_lipsync_TimedHeadMovements.prototype = $extend(com_oddcast_host_morph_sync_TimedFaceBase.prototype,{
	calculateHeadMovements: function(allSyncData) {
		var diminish = 0.5;
		var justPhenomes = this.justPhonemes(allSyncData);
		this.normalize(justPhenomes);
		var max = this.differentiate(justPhenomes,1.0,0);
		max = this.differentiate(max,1.0,diminish);
		var min = this.differentiate(justPhenomes,-1.0,0);
		min = this.differentiate(min,-1.0,diminish);
		this.syncData = min.concat(max);
		var f = function(a,b) {
			if(a.peakTime > b.peakTime) return 1; else return -1;
		};
		this.syncData.sort(f);
		var average = 0.0;
		var _g = 0;
		var _g1 = this.syncData;
		while(_g < _g1.length) {
			var data = _g1[_g];
			++_g;
			average += data.normalizedEnergy;
		}
		average /= this.syncData.length;
		haxe_Log.trace("[ALWAYS] average NormalizedEnergy:" + average,{ fileName : "TimedVisemes.hx", lineNumber : 777, className : "com.oddcast.host.morph.lipsync.TimedHeadMovements", methodName : "calculateHeadMovements"});
		var _g2 = 0;
		var _g11 = this.syncData;
		while(_g2 < _g11.length) {
			var data1 = _g11[_g2];
			++_g2;
			data1.normalizedEnergy += 0.5 - average;
		}
	}
	,justPhonemes: function(input) {
		var output = [];
		var _g = 0;
		while(_g < input.length) {
			var sync = input[_g];
			++_g;
			if(sync.isPhoneme() && !sync.isSilent()) output[output.length] = sync;
		}
		return output;
	}
	,normalize: function(input) {
		var min = 1.0;
		var max = 0.0;
		max = 1.0;
		min = 0.0;
		var _g = 0;
		while(_g < input.length) {
			var sync = input[_g];
			++_g;
			min = Math.min(min,sync.energy);
			max = Math.max(max,sync.energy);
		}
		var _g1 = 0;
		while(_g1 < input.length) {
			var sync1 = input[_g1];
			++_g1;
			sync1.normalizeEnergy(min,max);
		}
	}
	,differentiate: function(nEventData,maxOrMin,diminish) {
		var mm = [];
		var direction = 1;
		var currNenergy = 0.0;
		var totalnEnergy = 0.0;
		var minEnergy = 1.0;
		var _g = 0;
		while(_g < nEventData.length) {
			var sync = nEventData[_g];
			++_g;
			if((sync.normalizedEnergy - currNenergy) * direction < 0) {
				if(Math.abs(direction - maxOrMin) < 0.01) {
					mm[mm.length] = sync;
					minEnergy = Math.min(minEnergy,sync.normalizedEnergy);
					totalnEnergy += sync.normalizedEnergy;
				}
				direction *= -1;
			}
			currNenergy = sync.normalizedEnergy;
		}
		var meanEnergy = totalnEnergy / mm.length;
		var _g1 = 0;
		while(_g1 < nEventData.length) {
			var sync1 = nEventData[_g1];
			++_g1;
			sync1.normalizedEnergy -= diminish * meanEnergy;
		}
		return mm;
	}
	,startHeadMove: function() {
		this.syncIndex = 0;
	}
	,getCurrentHeadEvent: function(time) {
		if(this.syncIndex >= this.syncData.length - 1) return null;
		while(time > this.syncData[this.syncIndex].peakTime) {
			if(this.syncIndex == this.syncData.length - 1) return null;
			this.syncIndex++;
		}
		return this.syncData[this.syncIndex];
	}
	,__class__: com_oddcast_host_morph_lipsync_TimedHeadMovements
});
var com_oddcast_host_morph_sync_SyncDatum = function() {
	this.normalizedEnergy = 0.0;
	this.timeOffset = 0.0;
	this.attackDuration = 20.0;
	this.decayDuration = 20.0;
};
$hxClasses["com.oddcast.host.morph.sync.SyncDatum"] = com_oddcast_host_morph_sync_SyncDatum;
com_oddcast_host_morph_sync_SyncDatum.__name__ = ["com","oddcast","host","morph","sync","SyncDatum"];
com_oddcast_host_morph_sync_SyncDatum.prototype = {
	getLabelStr: function() {
		return String.fromCharCode(this.plabel);
	}
	,duration: function() {
		return this.endTime - this.startTime;
	}
	,toString: function() {
		return new String(" plabel:" + this.plabel + ", eventString:" + this.eventString + ", normalizedEnergy:" + com_oddcast_util_UtilsLite.formatFloat(this.normalizedEnergy) + ", peak:" + this.peakTime + ", typeStr:" + this.typeStr + ", energy:" + this.energy + ", startTime:" + this.startTime + ", timeOffset:" + this.timeOffset + ", endTime:" + this.endTime + ", duration:" + (this.endTime - this.startTime));
	}
	,extractFromCompressedString: function(input,minEnergy,maxEnergy,power) {
		var seg = input.split(com_oddcast_host_morph_sync_SyncDatum.DELIMITER);
		this.typeStr = seg[0];
		this.eventString = seg[4];
		if(this.typeStr == com_oddcast_host_morph_sync_SyncDatum.PHONE || this.typeStr == com_oddcast_host_morph_sync_SyncDatum.SENTENCE || this.typeStr == com_oddcast_host_morph_sync_SyncDatum.GROUP) this.setPlabel();
		this.startTime = parseFloat(seg[1]);
		this.endTime = parseFloat(seg[2]);
		this.energy = parseFloat(seg[3]) * 0.01;
		this.energy = minEnergy + Math.pow(this.energy,power) * (maxEnergy - minEnergy);
		this.calcValues();
		return this.typeStr;
	}
	,setPlabel: function() {
		this.plabel = HxOverrides.cca(this.eventString,0);
	}
	,clone: function() {
		var clone = new com_oddcast_host_morph_sync_SyncDatum();
		clone.copyDataFrom(this);
		return clone;
	}
	,copyDataFrom: function(from) {
		this.startTime = from.startTime;
		this.endTime = from.endTime;
		this.peakTime = from.peakTime;
		this.energy = from.energy;
		this.plabel = from.plabel;
		this.eventString = new String(from.eventString);
		this.typeStr = new String(from.typeStr);
	}
	,setTimedMood: function(name,amount,startTime,endTime,timeOffset,attackDuration,decayDuration) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.energy = amount;
		this.eventString = name;
		this.typeStr = com_oddcast_host_morph_sync_SyncDatum.MOOD;
		this.timeOffset = timeOffset;
		this.attackDuration = attackDuration;
		this.decayDuration = decayDuration;
		this.calcValues();
		return this;
	}
	,writeToOH: function() {
		return this.typeStr + "," + this.startTime + "," + this.endTime + "," + Std.string(Math.round(this.energy * 100)) + "," + this.eventString;
	}
	,isMood: function() {
		return this.typeStr == com_oddcast_host_morph_sync_SyncDatum.MOOD;
	}
	,isPhoneme: function() {
		return this.typeStr == com_oddcast_host_morph_sync_SyncDatum.PHONE;
	}
	,isWord: function() {
		return this.typeStr == com_oddcast_host_morph_sync_SyncDatum.WORD;
	}
	,isSilent: function() {
		return this.isPhoneme() && (this.eventString == "x" || this.eventString == "X");
	}
	,isCloseMouthed: function() {
		return this.isPhoneme() && (this.eventString == "b" || this.eventString == "m" || this.eventString == "p");
	}
	,normalizeEnergy: function(min,max) {
		this.normalizedEnergy = (this.energy - min) / (max - min);
	}
	,calcValues: function() {
		var startWeight = 1.0;
		if(this.typeStr == com_oddcast_host_morph_sync_SyncDatum.PHONE) {
			var vowel = 0.35;
			var sharpConsonant = 0.65;
			var _g = this.plabel;
			switch(_g) {
			case 99:
				startWeight = vowel;
				break;
			case 97:
				startWeight = vowel;
				break;
			case 94:
				startWeight = vowel;
				break;
			case 67:
				startWeight = vowel;
				break;
			case 87:
				startWeight = vowel;
				break;
			case 73:
				startWeight = vowel;
				break;
			case 69:
				startWeight = vowel;
				break;
			case 85:
				startWeight = vowel;
				break;
			case 79:
				startWeight = vowel;
				break;
			case 65:
				startWeight = vowel;
				break;
			case 98:
				startWeight = sharpConsonant;
				break;
			case 109:
				startWeight = sharpConsonant;
				break;
			case 112:
				startWeight = sharpConsonant;
				break;
			case 68:
				startWeight = sharpConsonant;
				break;
			case 70:
				startWeight = sharpConsonant;
				break;
			default:
				startWeight = 0.5;
			}
		}
		if(this.typeStr == com_oddcast_host_morph_sync_SyncDatum.MOOD) startWeight = 0.5;
		this.setPeakTime(startWeight);
	}
	,setPeakTime: function(startWeight) {
		this.peakTime = this.startTime * startWeight + this.endTime * (1 - startWeight);
	}
	,calcPositionWithinAttackDecay: function(time) {
		var start = this.startTime + this.timeOffset;
		var rampup = start + this.attackDuration;
		var end = this.endTime + this.timeOffset;
		var rampdown = end - this.decayDuration;
		if(time > end || time < start) return 0.0;
		if(time > rampdown) return 1 - (time - rampdown) / this.decayDuration;
		if(time > rampup) return 1.0;
		return (time - start) / this.attackDuration;
	}
	,setToDecay: function(lastTime) {
		var curr = this.calcPositionWithinAttackDecay(lastTime);
		this.endTime = lastTime - this.timeOffset + curr * this.decayDuration;
	}
	,getDominance: function() {
		var _g = this.plabel;
		switch(_g) {
		case 119:
			return 1.0;
		case 111:
			return 1.0;
		case 79:
			return 1.0;
		case 85:
			return 1.0;
		case 98:
			return 0.9;
		case 109:
			return 0.9;
		case 112:
			return 0.9;
		case 99:
			return 0.8;
		case 67:
			return 0.8;
		case 120:
			return 0.7;
		case 88:
			return 0.7;
		default:
			return -1.0;
		}
	}
	,__class__: com_oddcast_host_morph_sync_SyncDatum
};
var haxe_io_Input = function() { };
$hxClasses["haxe.io.Input"] = haxe_io_Input;
haxe_io_Input.__name__ = ["haxe","io","Input"];
haxe_io_Input.prototype = {
	readByte: function() {
		throw new js__$Boot_HaxeError("Not implemented");
	}
	,readBytes: function(s,pos,len) {
		var k = len;
		var b = s.b;
		if(pos < 0 || len < 0 || pos + len > s.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
		while(k > 0) {
			b[pos] = this.readByte();
			pos++;
			k--;
		}
		return len;
	}
	,set_bigEndian: function(b) {
		this.bigEndian = b;
		return b;
	}
	,read: function(nbytes) {
		var s = haxe_io_Bytes.alloc(nbytes);
		var p = 0;
		while(nbytes > 0) {
			var k = this.readBytes(s,p,nbytes);
			if(k == 0) throw new js__$Boot_HaxeError(haxe_io_Error.Blocked);
			p += k;
			nbytes -= k;
		}
		return s;
	}
	,readUntil: function(end) {
		var buf_b = "";
		var last;
		while((last = this.readByte()) != end) buf_b += String.fromCharCode(last);
		return buf_b;
	}
	,readUInt16: function() {
		var ch1 = this.readByte();
		var ch2 = this.readByte();
		if(this.bigEndian) return ch2 | ch1 << 8; else return ch1 | ch2 << 8;
	}
	,__class__: haxe_io_Input
};
var com_oddcast_io_BinaryAsTextInput = function(txt,bigEndian) {
	this.txt = txt;
	this.set_bigEndian(bigEndian);
	this.position = 0;
};
$hxClasses["com.oddcast.io.BinaryAsTextInput"] = com_oddcast_io_BinaryAsTextInput;
com_oddcast_io_BinaryAsTextInput.__name__ = ["com","oddcast","io","BinaryAsTextInput"];
com_oddcast_io_BinaryAsTextInput.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_io_BinaryAsTextInput.__super__ = haxe_io_Input;
com_oddcast_io_BinaryAsTextInput.prototype = $extend(haxe_io_Input.prototype,{
	readString: function(len) {
		var pos = this.incPos(len);
		return HxOverrides.substr(this.txt,pos,len);
	}
	,readNullTerminatedString: function() {
		return this.readUntil(0);
	}
	,readByte: function() {
		return this.readUInt8();
	}
	,readUInt8: function() {
		var pos = this.incPos(1);
		var code = HxOverrides.cca(this.txt,pos);
		if(pos < 0) throw new js__$Boot_HaxeError("Negative Pos");
		haxe_Log.trace("pos:" + pos + " code:" + com_oddcast_util_Utils.hex0x(code),{ fileName : "BinaryAsTextInput.hx", lineNumber : 46, className : "com.oddcast.io.BinaryAsTextInput", methodName : "readUInt8"});
		return 255 & code;
	}
	,readUInt16: function() {
		var b0 = this.readUInt8();
		var b1 = this.readUInt8();
		if(!this.bigEndian) {
			var t = b0;
			b0 = b1;
			b1 = t;
		}
		return b0 << 8 | b1;
	}
	,readUInt24: function() {
		var b0 = this.readUInt16();
		var b1 = this.readUInt8();
		var shift = 16;
		if(!this.bigEndian) {
			var t = b0;
			b0 = b1;
			b1 = t;
			shift = 8;
		}
		return b0 << shift | b1;
	}
	,readUInt32: function() {
		var b0 = this.readUInt16();
		var b1 = this.readUInt16();
		if(!this.bigEndian) {
			var t = b0;
			b0 = b1;
			b1 = t;
		}
		return b0 << 16 | b1;
	}
	,readSynchsafeInteger32: function() {
		var size = 0;
		var _g = 0;
		while(_g < 4) {
			var i = _g++;
			var u;
			var a = this.readUInt8();
			u = a & 127;
			size = size + (u << 21 - 7 * i);
		}
		return size;
	}
	,incPos: function(inc) {
		var ret = this.position;
		this.position += inc;
		return ret;
	}
	,dispose: function() {
		this.txt = null;
		this.position = -1;
	}
	,__class__: com_oddcast_io_BinaryAsTextInput
});
var com_oddcast_io_archive_iArchive_IArchiveScaled = function() { };
$hxClasses["com.oddcast.io.archive.iArchive.IArchiveScaled"] = com_oddcast_io_archive_iArchive_IArchiveScaled;
com_oddcast_io_archive_iArchive_IArchiveScaled.__name__ = ["com","oddcast","io","archive","iArchive","IArchiveScaled"];
com_oddcast_io_archive_iArchive_IArchiveScaled.__interfaces__ = [com_oddcast_io_archive_iArchive_IArchive];
com_oddcast_io_archive_iArchive_IArchiveScaled.prototype = {
	__class__: com_oddcast_io_archive_iArchive_IArchiveScaled
};
var com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled = function(url,osType,scale,archiveLoadStats) {
	com_oddcast_io_archive_iArchive_ArchiveLiteDirectory.call(this,url,osType,archiveLoadStats);
	this.scale = scale;
};
$hxClasses["com.oddcast.io.archive.iArchive.ArchiveLiteDirectoryScaled"] = com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled;
com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled.__name__ = ["com","oddcast","io","archive","iArchive","ArchiveLiteDirectoryScaled"];
com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled.__interfaces__ = [com_oddcast_io_archive_iArchive_IArchiveScaled];
com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled.__super__ = com_oddcast_io_archive_iArchive_ArchiveLiteDirectory;
com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled.prototype = $extend(com_oddcast_io_archive_iArchive_ArchiveLiteDirectory.prototype,{
	__class__: com_oddcast_io_archive_iArchive_ArchiveLiteDirectoryScaled
});
var com_oddcast_io_archive_iArchive_ArchiveLiteFile = function(url,osType,archiveLoadStats) {
	com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.call(this,url,osType,archiveLoadStats);
};
$hxClasses["com.oddcast.io.archive.iArchive.ArchiveLiteFile"] = com_oddcast_io_archive_iArchive_ArchiveLiteFile;
com_oddcast_io_archive_iArchive_ArchiveLiteFile.__name__ = ["com","oddcast","io","archive","iArchive","ArchiveLiteFile"];
com_oddcast_io_archive_iArchive_ArchiveLiteFile.__super__ = com_oddcast_io_archive_iArchive_ArchiveLiteAbstract;
com_oddcast_io_archive_iArchive_ArchiveLiteFile.prototype = $extend(com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.prototype,{
	__class__: com_oddcast_io_archive_iArchive_ArchiveLiteFile
});
var com_oddcast_io_archive_iArchive_FileLoadStats = function(name) {
	this.name = name;
	this.loadTime = -com_oddcast_util_UtilsLite.getTime();
};
$hxClasses["com.oddcast.io.archive.iArchive.FileLoadStats"] = com_oddcast_io_archive_iArchive_FileLoadStats;
com_oddcast_io_archive_iArchive_FileLoadStats.__name__ = ["com","oddcast","io","archive","iArchive","FileLoadStats"];
com_oddcast_io_archive_iArchive_FileLoadStats.prototype = {
	loadFinished: function() {
		this.loadTime = com_oddcast_util_UtilsLite.getTime() + this.loadTime;
	}
	,getLoadTime: function() {
		if(this.loadTime < 0) return com_oddcast_io_archive_iArchive_FileLoadStats.INVALID_TIME;
		return this.loadTime;
	}
	,traceString: function() {
	}
	,__class__: com_oddcast_io_archive_iArchive_FileLoadStats
};
var com_oddcast_io_archive_iArchive_ArchiveLoadStats = function(name) {
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "ArchiveLoadStats.hx", lineNumber : 46, className : "com.oddcast.io.archive.iArchive.ArchiveLoadStats", methodName : "new"});
	this.name = name;
	this.totalLoadTime = -com_oddcast_util_UtilsLite.getTime();
	this.getIntervalMillis();
	this.fileLoadStatsArray = [];
};
$hxClasses["com.oddcast.io.archive.iArchive.ArchiveLoadStats"] = com_oddcast_io_archive_iArchive_ArchiveLoadStats;
com_oddcast_io_archive_iArchive_ArchiveLoadStats.__name__ = ["com","oddcast","io","archive","iArchive","ArchiveLoadStats"];
com_oddcast_io_archive_iArchive_ArchiveLoadStats.prototype = {
	fileFinished: function(fileLoadStats) {
		fileLoadStats.loadFinished();
		this.fileLoadStatsArray.push(fileLoadStats);
		this.getIntervalMillis();
	}
	,xmlParsed: function() {
		this.xmlParseMillis = this.getIntervalMillis();
	}
	,xmlRead: function() {
		this.xmlReadMillis = this.getIntervalMillis();
	}
	,loadFinished: function() {
		this.totalLoadTime = com_oddcast_util_UtilsLite.getTime() + this.totalLoadTime;
		this.traceString();
	}
	,getTotalLoadTime: function() {
		if(this.totalLoadTime < 0) return com_oddcast_io_archive_iArchive_FileLoadStats.INVALID_TIME;
		return this.totalLoadTime;
	}
	,traceString: function() {
	}
	,getIntervalMillis: function() {
		var time = com_oddcast_util_UtilsLite.getTime();
		var interval = time - this.lastTime;
		this.lastTime = time;
		return interval;
	}
	,__class__: com_oddcast_io_archive_iArchive_ArchiveLoadStats
};
var com_oddcast_io_pngEncoder_PNGKeywords = function() { };
$hxClasses["com.oddcast.io.pngEncoder.PNGKeywords"] = com_oddcast_io_pngEncoder_PNGKeywords;
com_oddcast_io_pngEncoder_PNGKeywords.__name__ = ["com","oddcast","io","pngEncoder","PNGKeywords"];
com_oddcast_io_pngEncoder_PNGKeywords.parseJSON = function(str) {
	var ret = [];
	var json = JSON.parse(str);
	if(json.Comment != null) ret.push({ keyword : com_oddcast_io_pngEncoder_PNGKeywords.COMMENT, value : json.Comment[0], compress : false});
	if(json.Copyright != null) ret.push({ keyword : com_oddcast_io_pngEncoder_PNGKeywords.COPYRIGHT, value : json.Copyright[0], compress : false});
	return ret;
};
var com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG = function() { };
$hxClasses["com.oddcast.io.pngEncoder.PNG_CHUNK_SIG"] = com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.__name__ = ["com","oddcast","io","pngEncoder","PNG_CHUNK_SIG"];
var com_oddcast_io_pngEncoder_ParsePNGMetadata = function() {
};
$hxClasses["com.oddcast.io.pngEncoder.ParsePNGMetadata"] = com_oddcast_io_pngEncoder_ParsePNGMetadata;
com_oddcast_io_pngEncoder_ParsePNGMetadata.__name__ = ["com","oddcast","io","pngEncoder","ParsePNGMetadata"];
com_oddcast_io_pngEncoder_ParsePNGMetadata.getMetaDataFromPHP = function(url,complete,errorCallback) {
	var windowURL = window.document.URL;
	var secure;
	if(StringTools.startsWith(windowURL,"http:")) secure = "http:"; else secure = "https:";
	var PHP_SCRIPT = secure + "//host-d.oddcast.com/api/metadata.php?fileUrl=";
	var filename = PHP_SCRIPT + url;
	var r = new haxe_Http(filename);
	r.onError = function(msg) {
		if(errorCallback != null) errorCallback(filename,msg);
	};
	r.onData = function(str) {
		if(str.indexOf("Unable to open file") >= 0) errorCallback(filename,str); else complete(com_oddcast_io_pngEncoder_PNGKeywords.parseJSON(str));
	};
	r.request(false);
};
com_oddcast_io_pngEncoder_ParsePNGMetadata.prototype = {
	loadFromBinaryData: function(bd,cb,errorCallback) {
		var png0 = bd.int32(-1991225785,"png0");
		var png1 = bd.int32(218765834,"png1");
		var metaData = [];
		var _g = 0;
		while(_g < 10) {
			var c = _g++;
			if(this.parseChunkBD(bd,metaData)) break;
		}
		cb(metaData);
	}
	,parseChunkBD: function(data,metaData) {
		var chunk_length = data.uint32(0,null);
		var chunk_sig = data.uint32(0,null);
		switch(chunk_sig) {
		case 1229472850:
			var width = data.int32(0,null);
			var height = data.int32(0,null);
			var rgba_32 = data.uint32(0,null);
			var zero = data.byte8(0,null);
			break;
		case 1950701684:
			var keyword = data.stringNullTerminated("",null);
			var value = data.stringLength("",chunk_length - keyword.length - 1,null);
			metaData.push({ keyword : keyword, value : value, compress : false});
			break;
		case 1767135348:
			throw new js__$Boot_HaxeError("iTXt");
			break;
		case 2052348020:
			var keyword1 = data.stringNullTerminated("",null);
			var compressionType = data.byte8(1,"complib");
			var value1 = null;
			var uncompressedBytes = data.uncompress(chunk_length - keyword1.length - 2);
			value1 = uncompressedBytes.toString();
			metaData.push({ keyword : keyword1, value : value1, compress : false});
			break;
		case 1229209940:case 1229278788:
			return true;
		default:
			data.skip(chunk_length);
		}
		var chunk_crc = data.uint32(0,null);
		return false;
	}
	,dispose: function() {
	}
	,__class__: com_oddcast_io_pngEncoder_ParsePNGMetadata
};
var com_oddcast_io_xmlIO_AllAssetsLoaded = function(emptyCallback) {
	this.waitingForCallbackList = new List();
	this.retVal = null;
	this.emptyCallback = emptyCallback;
};
$hxClasses["com.oddcast.io.xmlIO.AllAssetsLoaded"] = com_oddcast_io_xmlIO_AllAssetsLoaded;
com_oddcast_io_xmlIO_AllAssetsLoaded.__name__ = ["com","oddcast","io","xmlIO","AllAssetsLoaded"];
com_oddcast_io_xmlIO_AllAssetsLoaded.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_io_xmlIO_AllAssetsLoaded.prototype = {
	allowExit: function(retVal) {
		this.retVal = retVal;
		this.fileCompleteCallback(null);
	}
	,fileWaitingCallback: function(obj) {
		if(obj != null) this.waitingForCallbackList.push(obj);
	}
	,fileCompleteCallback: function(obj) {
		if(obj != null) {
			if(!this.waitingForCallbackList.remove(obj)) throw new js__$Boot_HaxeError("cannot find obj:" + Std.string(obj));
		}
		if(this.retVal != null && this.waitingForCallbackList.isEmpty()) {
			this.waitingForCallbackList = null;
			if(this.emptyCallback != null) {
				this.emptyCallback(this.retVal);
				this.emptyCallback = null;
			} else throw new js__$Boot_HaxeError("callback already made");
		}
	}
	,dispose: function() {
		this.waitingForCallbackList = null;
		this.retVal = null;
		this.emptyCallback = null;
	}
	,__class__: com_oddcast_io_xmlIO_AllAssetsLoaded
};
var com_oddcast_io_xmlIO_AssetURL = function() { };
$hxClasses["com.oddcast.io.xmlIO.AssetURL"] = com_oddcast_io_xmlIO_AssetURL;
com_oddcast_io_xmlIO_AssetURL.__name__ = ["com","oddcast","io","xmlIO","AssetURL"];
com_oddcast_io_xmlIO_AssetURL.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_io_xmlIO_AssetURL.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_io_xmlIO_AssetURL.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	dispose: function() {
	}
	,__class__: com_oddcast_io_xmlIO_AssetURL
});
var com_oddcast_io_xmlIO_TextureAssetURL = function() {
};
$hxClasses["com.oddcast.io.xmlIO.TextureAssetURL"] = com_oddcast_io_xmlIO_TextureAssetURL;
com_oddcast_io_xmlIO_TextureAssetURL.__name__ = ["com","oddcast","io","xmlIO","TextureAssetURL"];
com_oddcast_io_xmlIO_TextureAssetURL.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_io_xmlIO_TextureAssetURL.__super__ = com_oddcast_io_xmlIO_AssetURL;
com_oddcast_io_xmlIO_TextureAssetURL.prototype = $extend(com_oddcast_io_xmlIO_AssetURL.prototype,{
	parse: function(bd) {
		com_oddcast_io_xmlIO_AssetURL.prototype.parse.call(this,bd);
		this.imagePairs_ = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewMapString16Parse(this.imagePairs_,function() {
			return new com_oddcast_io_xmlIO_ImagePair();
		},bd,"imagePairs_");
	}
	,setTexture: function(tex) {
		this.textureAsset = tex;
	}
	,getTexture: function() {
		return this.textureAsset;
	}
	,dispose: function() {
		com_oddcast_io_xmlIO_AssetURL.prototype.dispose.call(this);
		if(this.textureAsset != null) this.textureAsset = null;
	}
	,insertScaleDirectory: function(archive,url) {
		return url;
	}
	,loaded: function(allAssetsLoaded,archive,xmlName) {
		com_oddcast_io_xmlIO_AssetURL.prototype.loaded.call(this,allAssetsLoaded,archive,xmlName);
		var imagePair = this.imagePairs_.get(Std.string(archive.osType));
		if(imagePair != null) {
			allAssetsLoaded.fileWaitingCallback(this);
			var me = this;
			archive.loadImage(this.insertScaleDirectory(archive,imagePair.colorURL_),this.insertScaleDirectory(archive,imagePair.maskURL_),function(color) {
				me.textureAsset = com_oddcast_util_js_JSImage.getTextureAsset(color,true);
				com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "IXmlIO.hx", lineNumber : 273, className : "com.oddcast.io.xmlIO.TextureAssetURL", methodName : "loaded"});
				haxe_Log.trace(me.textureAsset.DEBUG_String(),{ fileName : "IXmlIO.hx", lineNumber : 274, className : "com.oddcast.io.xmlIO.TextureAssetURL", methodName : "loaded"});
				allAssetsLoaded.fileCompleteCallback(me);
			});
		}
	}
	,getAssetSize: function() {
		if(this.textureAsset != null) return this.textureAsset.getAssetSize();
		return 0;
	}
	,getDownloadSize: function() {
		if(this.textureAsset != null) return this.textureAsset.getDownloadSize();
		return 0;
	}
	,__class__: com_oddcast_io_xmlIO_TextureAssetURL
});
var com_oddcast_io_xmlIO_TextureAssetURLScaled = function() {
	com_oddcast_io_xmlIO_TextureAssetURL.call(this);
};
$hxClasses["com.oddcast.io.xmlIO.TextureAssetURLScaled"] = com_oddcast_io_xmlIO_TextureAssetURLScaled;
com_oddcast_io_xmlIO_TextureAssetURLScaled.__name__ = ["com","oddcast","io","xmlIO","TextureAssetURLScaled"];
com_oddcast_io_xmlIO_TextureAssetURLScaled.__super__ = com_oddcast_io_xmlIO_TextureAssetURL;
com_oddcast_io_xmlIO_TextureAssetURLScaled.prototype = $extend(com_oddcast_io_xmlIO_TextureAssetURL.prototype,{
	parse: function(bd) {
		com_oddcast_io_xmlIO_TextureAssetURL.prototype.parse.call(this,bd);
		this.scaleSigFig_ = bd.uShort16(this.scaleSigFig_,"scaleSigFig_");
		this.scaleArray_ = bd.float32TrueArray16(this.scaleArray_,"scaleArray_");
	}
	,findNearestScale: function(inscale) {
		var minScaleRatio = Infinity;
		this.imageScale = 1.0;
		var _g = 0;
		var _g1 = this.scaleArray_;
		while(_g < _g1.length) {
			var scale = _g1[_g];
			++_g;
			var scaleRatio = scale / inscale;
			if(scaleRatio < 1) scaleRatio = 1 / scaleRatio;
			if(scaleRatio < minScaleRatio) {
				minScaleRatio = scaleRatio;
				this.imageScale = scale;
			}
		}
		return this.imageScale;
	}
	,insertScaleDirectory: function(archive,url) {
		if(url == null) return url;
		var scaledArchive = archive;
		return com_oddcast_util_UtilsLite.formatFloat(this.findNearestScale(scaledArchive.scale),this.scaleSigFig_,true) + "/" + url;
	}
	,__class__: com_oddcast_io_xmlIO_TextureAssetURLScaled
});
var com_oddcast_io_xmlIO_ImagePair = function() {
};
$hxClasses["com.oddcast.io.xmlIO.ImagePair"] = com_oddcast_io_xmlIO_ImagePair;
com_oddcast_io_xmlIO_ImagePair.__name__ = ["com","oddcast","io","xmlIO","ImagePair"];
com_oddcast_io_xmlIO_ImagePair.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct,com_oddcast_util_IDisposable];
com_oddcast_io_xmlIO_ImagePair.__super__ = com_oddcast_io_xmlIO_XmlIOVersioned;
com_oddcast_io_xmlIO_ImagePair.prototype = $extend(com_oddcast_io_xmlIO_XmlIOVersioned.prototype,{
	dispose: function() {
	}
	,parse: function(bd) {
		com_oddcast_io_xmlIO_XmlIOVersioned.prototype.parse.call(this,bd);
		this.colorURL_ = bd.string16(this.colorURL_,"colorURL");
		this.maskURL_ = bd.string16(this.maskURL_,"maskURL");
	}
	,__class__: com_oddcast_io_xmlIO_ImagePair
});
var com_oddcast_io_xmlIO_binaryData_BinaryDataIObase = function() { };
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataIObase"] = com_oddcast_io_xmlIO_binaryData_BinaryDataIObase;
com_oddcast_io_xmlIO_binaryData_BinaryDataIObase.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataIObase"];
com_oddcast_io_xmlIO_binaryData_BinaryDataIObase.prototype = {
	isValid: function(valid) {
		if(!valid) {
			var junk = 1;
		}
		var ret = this.boolean8(valid,null);
		return ret;
	}
	,boolean8: function(b,desc) {
		return true;
	}
	,usageString: function() {
		return "";
	}
	,createWriteXml: function() {
		var xmlDoc = Xml.createDocument();
		xmlDoc.addChild(Xml.createProcessingInstruction("xml version=\"1.0\""));
		this.xml = xmlDoc;
	}
	,xmlPush: function(desc) {
		if(this.xml != null) {
			var xmlElement = Xml.createElement(desc);
			this.xml.addChild(xmlElement);
			this.xml = xmlElement;
		}
		return true;
	}
	,xmlPop: function() {
		if(this.xml != null) this.xml = this.xml.parent;
	}
	,uncompress: function(nbyte) {
		if(nbyte == null) nbyte = 0;
		return null;
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_BinaryDataIObase
};
var com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase = function() {
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataReadBase"] = com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataReadBase"];
com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.__super__ = com_oddcast_io_xmlIO_binaryData_BinaryDataIObase;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype = $extend(com_oddcast_io_xmlIO_binaryData_BinaryDataIObase.prototype,{
	setReadData: function(allAssetsLoaded,read_archive,name) {
		this.allAssetsLoaded = allAssetsLoaded;
		this.read_archive = read_archive;
		this.name = name;
	}
	,boolean8: function(b,desc) {
		var b8 = this.getChar();
		if(b8 == 0) return false;
		if(b8 == 1) return true;
		throw new js__$Boot_HaxeError("not boolean");
	}
	,string16: function(string,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var stringBuf = new StringBuf();
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			stringBuf.addChar(this.getChar());
		}
		return stringBuf.b;
	}
	,stringNullTerminated: function(string,desc) {
		var stringBuf_b = "";
		while(true) {
			var $char = this.getChar();
			if($char == 0) break;
			stringBuf_b += String.fromCharCode($char);
		}
		return stringBuf_b;
	}
	,stringLength: function(string,length,desc) {
		var stringBuf_b = "";
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			var $char = this.getChar();
			if($char != 0) stringBuf_b += String.fromCharCode($char);
		}
		return stringBuf_b;
	}
	,dispose: function() {
		this.allAssetsLoaded = com_oddcast_util_Disposable.disposeIfValid(this.allAssetsLoaded);
		this.read_archive = null;
	}
	,getLength16: function() {
		throw new js__$Boot_HaxeError("abstract");
	}
	,getChar: function() {
		throw new js__$Boot_HaxeError("abstract");
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase
});
var com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback = function(s) {
	this.lines = s.split("\n");
	this.lineNo = 0;
	com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.call(this);
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataReadTextFallback"] = com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataReadTextFallback"];
com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback.__super__ = com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback.prototype = $extend(com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype,{
	isWrite: function() {
		return false;
	}
	,float32: function(f,desc) {
		return Std.parseFloat(this.getLine());
	}
	,int32: function(i,desc) {
		return Std.parseInt(this.getLine());
	}
	,uint32: function(i,desc) {
		return Std.parseInt(this.getLine());
	}
	,short16: function(s,desc) {
		return Std.parseInt(this.getLine());
	}
	,uShort16: function(s,desc) {
		return Std.parseInt(this.getLine());
	}
	,byte8: function(b,desc) {
		return Std.parseInt(this.getLine());
	}
	,uByte8: function(b,desc) {
		return Std.parseInt(this.getLine());
	}
	,string16: function(string,desc) {
		if(!this.isValid(true)) return null;
		return this.getLine();
	}
	,skip: function(nBytes) {
		var _g = 0;
		while(_g < nBytes) {
			var i = _g++;
			this.uByte8(0,null);
		}
	}
	,totalSize: function() {
		return 0;
	}
	,getLength16: function() {
		return this.uShort16(0,null);
	}
	,getLength32: function() {
		return this.int32(0,null);
	}
	,getChar: function() {
		return this.uByte8(0,null);
	}
	,dispose: function() {
		com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype.dispose.call(this);
		this.lines = null;
	}
	,getLine: function() {
		return this.lines[this.lineNo++];
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback
});
var com_oddcast_io_xmlIO_binaryData_IBinaryDataIO = function() { };
$hxClasses["com.oddcast.io.xmlIO.binaryData.IBinaryDataIO"] = com_oddcast_io_xmlIO_binaryData_IBinaryDataIO;
com_oddcast_io_xmlIO_binaryData_IBinaryDataIO.__name__ = ["com","oddcast","io","xmlIO","binaryData","IBinaryDataIO"];
com_oddcast_io_xmlIO_binaryData_IBinaryDataIO.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_io_xmlIO_binaryData_IBinaryDataIO.prototype = {
	__class__: com_oddcast_io_xmlIO_binaryData_IBinaryDataIO
};
var com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml = function(xmlStr) {
	com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.call(this);
	this.xml = Xml.parse(xmlStr);
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataReadXml"] = com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataReadXml"];
com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataIO];
com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml.__super__ = com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase;
com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml.prototype = $extend(com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype,{
	isWrite: function() {
		return false;
	}
	,isValid: function(valid) {
		return true;
	}
	,float32: function(f,desc) {
		var value = this.xml.get(desc);
		if(value == null) return f;
		return parseFloat(value);
	}
	,int32: function(i,desc) {
		var value = this.xml.get(desc);
		if(value == null) return i;
		return Std.parseInt(value);
	}
	,uint32: function(i,desc) {
		return this.int32(i,desc);
	}
	,short16: function(s,desc) {
		return this.int32(s,desc);
	}
	,uShort16: function(s,desc) {
		return this.int32(s,desc);
	}
	,byte8: function(b,desc) {
		return this.int32(b,desc);
	}
	,uByte8: function(b,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return 0;
	}
	,boolean8: function(b,desc) {
		var bStr = this.xml.get(desc);
		if(bStr == null) return b;
		if(bStr == "true") return true;
		if(bStr == "false") return false;
		throw new js__$Boot_HaxeError("unknown bool:" + bStr);
	}
	,string16: function(string,desc) {
		return this.xml.get(desc);
	}
	,stringNullTerminated: function(string,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return "";
	}
	,stringLength: function(string,length,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return "";
	}
	,skip: function(nBytes) {
		throw new js__$Boot_HaxeError("to be implemented");
	}
	,totalSize: function() {
		throw new js__$Boot_HaxeError("to be implemented");
		return 0;
	}
	,float32Array16: function(a,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,int32Array16: function(a,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,int16Array16: function(af,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,int16Array32: function(af,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,int32Array32: function(af,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,float32TrueArray16: function(a,desc) {
		throw new js__$Boot_HaxeError("to be implemented");
		return null;
	}
	,usageString: function() {
		throw new js__$Boot_HaxeError("to be implemented");
		return "";
	}
	,xmlPush: function(desc) {
		var elementIterator = this.xml.elementsNamed(desc);
		if(elementIterator.hasNext()) {
			this.xml = elementIterator.next();
			return true;
		}
		return false;
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_BinaryDataReadXml
});
var com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract = function() {
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataStructAbstract"] = com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract;
com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataStructAbstract"];
com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataStruct];
com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract.prototype = {
	setVersion: function(v) {
		this.version_ = v;
		return this;
	}
	,parse: function(bd) {
		this.version_ = bd.uShort16(this.version_,"version_");
	}
	,saved: function(archive) {
	}
	,loaded: function(allAssetsLoaded,archive,xmlName) {
	}
	,allAssetsLoaded: function() {
	}
	,toString: function() {
		return " version_:" + this.version_;
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_BinaryDataStructAbstract
};
var com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools = function() { };
$hxClasses["com.oddcast.io.xmlIO.binaryData.BinaryDataStructTools"] = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools;
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.__name__ = ["com","oddcast","io","xmlIO","binaryData","BinaryDataStructTools"];
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse = function(struct,construct,bd,desc) {
	if(bd.isValid(struct != null)) {
		if(bd.xmlPush(desc)) {
			var className = "";
			if(construct == null) {
				if(bd.isWrite()) className = com_oddcast_util_UtilsLite.getClassName(struct);
				className = bd.string16(className,"className");
			}
			if(struct == null) {
				if(construct != null) struct = construct(); else struct = Type.createInstance(Type.resolveClass(className),[]);
			}
			struct.parse(bd);
			if(desc == "HostDataSet") {
				var junk = 1;
			}
			if(bd.isWrite()) {
			} else struct.loaded(bd.allAssetsLoaded,bd.read_archive,bd.name);
			bd.xmlPop();
		}
	}
	return struct;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16 = function(bd,length,desc) {
	var ret = bd.uShort16(length,desc);
	return ret;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength32 = function(bd,length) {
	var ret = bd.int32(length,null);
	return ret;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse = function(structArray,construct,bd,desc) {
	if(bd.isValid(structArray != null)) {
		if(structArray != null) structArray = structArray; else structArray = [];
		if(bd.xmlPush(desc)) {
			var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,structArray.length,"n");
			var _g = 0;
			while(_g < n) {
				var i = _g++;
				structArray[i] = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(structArray[i],construct,bd,desc + "_" + i);
			}
			bd.xmlPop();
		}
	}
	return structArray;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewList16Parse = function(structList,construct,bd,desc) {
	if(bd.isValid(structList != null)) {
		if(structList != null) structList = structList; else structList = new List();
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,structList.length,null);
		if(bd.isWrite()) {
			var _g_head = structList.h;
			var _g_val = null;
			while(_g_head != null) {
				var s;
				s = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(s,construct,bd,desc);
			}
		} else {
			var dummy = structList.first();
			var _g = 0;
			while(_g < n) {
				var i = _g++;
				structList.push(com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(dummy,construct,bd,desc));
			}
		}
		bd.xmlPop();
	}
	return structList;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewMapInt16Parse = function(structMapInt,construct,bd,desc) {
	if(bd.isValid(structMapInt != null)) {
		if(structMapInt != null) structMapInt = structMapInt; else structMapInt = new haxe_ds_IntMap();
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,Lambda.count(structMapInt),null);
		if(bd.isWrite()) {
			var $it0 = structMapInt.keys();
			while( $it0.hasNext() ) {
				var ikey = $it0.next();
				bd.short16(ikey,"key");
				com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(structMapInt.h[ikey],construct,bd,"value");
			}
		} else {
			var _g = 0;
			while(_g < n) {
				var i = _g++;
				var key = bd.short16(0,"key");
				var struct = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(null,construct,bd,"value");
				structMapInt.h[key] = struct;
			}
		}
		bd.xmlPop();
	}
	return structMapInt;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewMapString16Parse = function(structMapString,construct,bd,desc) {
	if(bd.isValid(structMapString != null)) {
		if(structMapString != null) structMapString = structMapString; else structMapString = new haxe_ds_StringMap();
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,Lambda.count(structMapString),null);
		if(bd.isWrite()) {
			var $it0 = structMapString.keys();
			while( $it0.hasNext() ) {
				var ikey = $it0.next();
				bd.string16(ikey,"key");
				com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(__map_reserved[ikey] != null?structMapString.getReserved(ikey):structMapString.h[ikey],construct,bd,"value");
			}
		} else {
			var _g = 0;
			while(_g < n) {
				var i = _g++;
				var key = bd.string16("","key");
				var struct = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewParse(null,construct,bd,"value");
				if(__map_reserved[key] != null) structMapString.setReserved(key,struct); else structMapString.h[key] = struct;
			}
			bd.xmlPop();
		}
	}
	return structMapString;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArrayMapString16Parse = function(arrayMapString,construct,bd,desc) {
	if(bd.isValid(arrayMapString != null)) {
		if(arrayMapString != null) arrayMapString = arrayMapString; else arrayMapString = new haxe_ds_StringMap();
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,Lambda.count(arrayMapString),null);
		if(bd.isWrite()) {
			var $it0 = arrayMapString.keys();
			while( $it0.hasNext() ) {
				var ikey = $it0.next();
				bd.string16(ikey,"key");
				com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse(__map_reserved[ikey] != null?arrayMapString.getReserved(ikey):arrayMapString.h[ikey],construct,bd,"value");
			}
		} else {
			var _g = 0;
			while(_g < n) {
				var i = _g++;
				var key = bd.string16("","key");
				var struct = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16Parse(null,construct,bd,"value");
				if(__map_reserved[key] != null) arrayMapString.setReserved(key,struct); else arrayMapString.h[key] = struct;
			}
		}
		bd.xmlPop();
	}
	return arrayMapString;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewArray16List16Parse = function(arrayList,construct,bd,desc) {
	if(bd.isValid(arrayList != null)) {
		if(arrayList != null) arrayList = arrayList; else arrayList = [];
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,Lambda.count(arrayList),null);
		var _g = 0;
		while(_g < n) {
			var i = _g++;
			arrayList[i] = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewList16Parse(arrayList[i],construct,bd,desc);
		}
		bd.xmlPop();
	}
	return arrayList;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.enumer = function(e,type,bd,desc) {
	var str = null;
	if(bd.isWrite()) {
		str = Std.string(e);
		bd.string16(str,desc);
	} else str = bd.string16("",desc);
	if(str == null || str == "null") return null;
	return Type.createEnum(type,str);
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.thisOrNewMapString16Basic = function(structMapString,cellCallback,bd,desc) {
	if(bd.isValid(structMapString != null)) {
		if(structMapString != null) structMapString = structMapString; else structMapString = new haxe_ds_StringMap();
		bd.xmlPush(desc);
		var n = com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.arrayLength16(bd,Lambda.count(structMapString),"n");
		var i = 0;
		if(bd.isWrite()) {
			var $it0 = structMapString.keys();
			while( $it0.hasNext() ) {
				var ikey = $it0.next();
				bd.string16(ikey,"key_" + i);
				i++;
				cellCallback(__map_reserved[ikey] != null?structMapString.getReserved(ikey):structMapString.h[ikey],ikey);
			}
		} else {
			var _g = 0;
			while(_g < n) {
				var i1 = _g++;
				var key = bd.string16("","key_" + i1);
				var struct = cellCallback(null,key);
				if(__map_reserved[key] != null) structMapString.setReserved(key,struct); else structMapString.h[key] = struct;
			}
		}
		bd.xmlPop();
	}
	return structMapString;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.Matrix23parse = function(m,bd,desc) {
	if(m == null) m = new com_oddcast_util_js_geom_Matrix();
	m.a = bd.float32(m.a,"a");
	m.b = bd.float32(m.b,"b");
	m.c = bd.float32(m.c,"c");
	m.d = bd.float32(m.d,"d");
	m.tx = bd.float32(m.tx,"tx");
	m.ty = bd.float32(m.ty,"ty");
	return m;
};
com_oddcast_io_xmlIO_binaryData_BinaryDataStructTools.rectangleParse = function(r,bd,desc) {
	if(r == null) r = new com_oddcast_neko_geom_Rectangle();
	r.x = bd.float32(r.x,"x");
	r.y = bd.float32(r.y,"y");
	r.width = bd.float32(r.width,"width");
	r.height = bd.float32(r.height,"height");
	return r;
};
var com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS = function(arrayBuffer,byteOffset,littleEndian) {
	com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.call(this);
	this.dataView = new DataView(arrayBuffer,byteOffset);
	this.littleEndian = littleEndian;
	this.arrayBuffer = arrayBuffer;
	this.systemLittleEndian = this.detectLittleEndianess();
	this.position = 0;
	this.int16_16_total = this.int32_16_total = this.float32_16_total = this.int32_32_total = 0;
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.js.BinaryDataReadJS"] = com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS;
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS.__name__ = ["com","oddcast","io","xmlIO","binaryData","js","BinaryDataReadJS"];
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataIO];
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS.__super__ = com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase;
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS.prototype = $extend(com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype,{
	isWrite: function() {
		return false;
	}
	,float32: function(f,desc) {
		var ret = this.dataView.getFloat32(this.position,this.littleEndian);
		this.position += 4;
		return ret;
	}
	,int32: function(i,desc) {
		var ret = this.dataView.getInt32(this.position,this.littleEndian);
		this.position += 4;
		return ret;
	}
	,uint32: function(i,desc) {
		var ret = this.dataView.getUint32(this.position,this.littleEndian);
		this.position += 4;
		return ret;
	}
	,short16: function(i,desc) {
		var ret = this.dataView.getInt16(this.position,this.littleEndian);
		this.position += 2;
		return ret;
	}
	,uShort16: function(s,desc) {
		var u16 = this.dataView.getUint16(this.position,this.littleEndian);
		this.position += 2;
		return u16;
	}
	,skip: function(nBytes) {
		this.position += nBytes;
	}
	,totalSize: function() {
		return this.arrayBuffer.byteLength;
	}
	,byte8: function(b,desc) {
		return this.dataView.getInt8(this.position++);
	}
	,uByte8: function(b,desc) {
		return this.dataView.getUint8(this.position++);
	}
	,getLength16: function() {
		return this.uShort16(0,null);
	}
	,getLength32: function() {
		return this.int32(0,null);
	}
	,getChar: function() {
		return this.dataView.getUint8(this.position++);
	}
	,float32Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var byteLength = length * 4;
		var ab = this.arrayBuffer.slice(this.position,this.position + byteLength);
		if(this.littleEndian != this.systemLittleEndian) this.switchEndian(ab,4);
		var ret = new Float32Array(ab);
		this.position += byteLength;
		this.float32_16_total += length;
		return ret;
	}
	,float32TrueArray16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var ret = [];
		ret[length - 1] = 0;
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.float32(0,desc);
		}
		return ret;
	}
	,int32Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var byteLength = length * 4;
		var ab = this.arrayBuffer.slice(this.position,this.position + byteLength);
		if(this.littleEndian != this.systemLittleEndian) this.switchEndian(ab,4);
		var ret = new Int32Array(ab);
		this.position += byteLength;
		this.int32_16_total += length;
		return ret;
	}
	,int16Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var byteLength = length * 2;
		var ab = this.arrayBuffer.slice(this.position,this.position + byteLength);
		if(this.littleEndian != this.systemLittleEndian) this.switchEndian(ab,2);
		var ret = new Int16Array(ab);
		this.position += byteLength;
		this.int16_16_total += length;
		return ret;
	}
	,int16Array32: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength32();
		var byteLength = length * 2;
		var ab = this.arrayBuffer.slice(this.position,this.position + byteLength);
		if(this.littleEndian != this.systemLittleEndian) this.switchEndian(ab,2);
		var ret = new Int16Array(ab);
		this.position += byteLength;
		this.int16_16_total += length;
		return ret;
	}
	,int32Array32: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength32();
		var byteLength = length * 4;
		var ab = this.arrayBuffer.slice(this.position,this.position + byteLength);
		if(this.littleEndian != this.systemLittleEndian) this.switchEndian(ab,4);
		var ret = new Int32Array(ab);
		this.position += byteLength;
		this.int32_32_total += length;
		return ret;
	}
	,dispose: function() {
		com_oddcast_io_xmlIO_binaryData_BinaryDataReadBase.prototype.dispose.call(this);
		this.dataView = null;
		this.arrayBuffer = null;
	}
	,getHaxeIoBytes: function() {
		return haxe_io_Bytes.ofData(this.arrayBuffer);
	}
	,getPosition: function() {
		return this.position;
	}
	,uncompress: function(nbyte) {
		if(nbyte == null) nbyte = 0;
		var haxeIoBytes = this.getHaxeIoBytes();
		var bytesInput = new haxe_io_BytesInput(haxeIoBytes,this.getPosition(),nbyte);
		var bytes = format_tools_InflateImpl.run(bytesInput);
		this.skip(nbyte);
		return bytes;
	}
	,usageString: function() {
		return " int16_16_total:" + this.int16_16_total + " int32_16_total:" + this.int32_16_total + " float32_16_total:" + this.float32_16_total + " int32_32_total:" + this.int32_32_total;
	}
	,detectLittleEndianess: function() {
		var uint8Array = new Uint8Array(4);
		uint8Array[0] = 1;
		uint8Array[1] = 2;
		uint8Array[2] = 3;
		uint8Array[3] = 4;
		var arrayBuffer = uint8Array.buffer;
		var uint32Array = new Uint32Array(arrayBuffer,0,1);
		var read = uint32Array[0];
		if(read == 67305985) return true;
		if(read == 16909060) return false; else throw new js__$Boot_HaxeError(" unknown endian " + com_oddcast_util_UtilsLite.hex0x(read));
		return true;
	}
	,switchEndian: function(arrayBuffer,elementLength) {
		var uint8Array = new Uint8Array(arrayBuffer);
		var i = 0;
		var len = uint8Array.length;
		while(i < len) {
			var store = uint8Array[i];
			if(elementLength == 2) {
				uint8Array[i] = uint8Array[i + 1];
				uint8Array[i + 1] = store;
			} else {
				uint8Array[i] = uint8Array[i + 3];
				uint8Array[i + 3] = store;
				store = uint8Array[i + 1];
				uint8Array[i + 1] = uint8Array[i + 2];
				uint8Array[i + 2] = store;
			}
			i += elementLength;
		}
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadJS
});
var com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS = function(s) {
	com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback.call(this,s);
};
$hxClasses["com.oddcast.io.xmlIO.binaryData.js.BinaryDataReadTextFallbackJS"] = com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS;
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS.__name__ = ["com","oddcast","io","xmlIO","binaryData","js","BinaryDataReadTextFallbackJS"];
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS.__interfaces__ = [com_oddcast_io_xmlIO_binaryData_IBinaryDataIO];
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS.__super__ = com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback;
com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS.prototype = $extend(com_oddcast_io_xmlIO_binaryData_BinaryDataReadTextFallback.prototype,{
	float32Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var ret = new Float32Array(length);
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.float32(0,desc);
		}
		return ret;
	}
	,float32TrueArray16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var ret = [];
		ret[length - 1] = 0;
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.float32(0,desc);
		}
		return ret;
	}
	,int32Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var ret = new Int32Array(length);
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.int32(0,desc);
		}
		return ret;
	}
	,int16Array16: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength16();
		var ret = new Int16Array(length);
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.short16(0,desc);
		}
		return ret;
	}
	,int16Array32: function(a,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength32();
		var ret = new Int16Array(length);
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.short16(0,desc);
		}
		return ret;
	}
	,int32Array32: function(af,desc) {
		if(!this.isValid(true)) return null;
		var length = this.getLength32();
		var ret = new Int32Array(length);
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			ret[i] = this.int32(0,desc);
		}
		return ret;
	}
	,__class__: com_oddcast_io_xmlIO_binaryData_js_BinaryDataReadTextFallbackJS
});
var com_oddcast_neko_geom_Point = function(x,y) {
	if(y == null) y = 0.0;
	if(x == null) x = 0.0;
	this.x = x;
	this.y = y;
};
$hxClasses["com.oddcast.neko.geom.Point"] = com_oddcast_neko_geom_Point;
com_oddcast_neko_geom_Point.__name__ = ["com","oddcast","neko","geom","Point"];
com_oddcast_neko_geom_Point.polar = function(len,angle) {
	return new com_oddcast_neko_geom_Point(len * Math.cos(angle),len * Math.sin(angle));
};
com_oddcast_neko_geom_Point.prototype = {
	get_length: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	,__class__: com_oddcast_neko_geom_Point
};
var com_oddcast_neko_geom_Rectangle = function(x,y,width,height) {
	if(height == null) height = 0.0;
	if(width == null) width = 0.0;
	if(y == null) y = 0.0;
	if(x == null) x = 0.0;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
};
$hxClasses["com.oddcast.neko.geom.Rectangle"] = com_oddcast_neko_geom_Rectangle;
com_oddcast_neko_geom_Rectangle.__name__ = ["com","oddcast","neko","geom","Rectangle"];
com_oddcast_neko_geom_Rectangle.prototype = {
	get_g: function() {
		return this.my_x;
	}
	,set_g: function(v) {
		if(v >= 0) this.my_x = v;
		return this.my_x;
	}
	,get_topLeft: function() {
		return new com_oddcast_neko_geom_Point(this.x,this.y);
	}
	,get_bottomRight: function() {
		return new com_oddcast_neko_geom_Point(this.x + this.width,this.y + this.height);
	}
	,intersection: function($with) {
		var ileft = Math.max(this.x,$with.x);
		var itop = Math.max(this.y,$with.y);
		var iright = Math.min(this.get_right(),$with.get_right());
		var ibottom = Math.min(this.get_bottom(),$with.get_bottom());
		if(this.get_left() > this.get_right() || this.get_top() > this.get_bottom()) return new com_oddcast_neko_geom_Rectangle(0,0,0,0);
		return new com_oddcast_neko_geom_Rectangle(ileft,itop,iright - ileft,ibottom - itop);
	}
	,union: function($with) {
		var x0;
		if(this.x > $with.x) x0 = $with.x; else x0 = this.x;
		var x1;
		if(this.get_right() < $with.get_right()) x1 = $with.get_right(); else x1 = this.get_right();
		var y0;
		if(this.y > $with.y) y0 = $with.y; else y0 = this.y;
		var y1;
		if(this.get_bottom() < $with.get_bottom()) y1 = $with.get_bottom(); else y1 = this.get_bottom();
		return new com_oddcast_neko_geom_Rectangle(x0,y0,x1 - x0,y1 - y0);
	}
	,contains: function(pX,pY) {
		return pX >= this.get_left() && pX <= this.get_right() && pY >= this.get_top() && pY <= this.get_bottom();
	}
	,inflate: function(dx,dy) {
		this.x -= dx;
		this.width += dx + dx;
		this.y -= dy;
		this.height += dy + dy;
	}
	,clone: function() {
		return new com_oddcast_neko_geom_Rectangle(this.x,this.y,this.width,this.height);
	}
	,toString: function() {
		return " x:" + this.x + " y:" + this.y + " width:" + this.width + " height:" + this.height;
	}
	,get_left: function() {
		return this.x;
	}
	,set_left: function(le) {
		this.width += this.x - le;
		this.x = le;
		return this.x;
	}
	,get_right: function() {
		return this.x + this.width;
	}
	,set_right: function(r) {
		this.width = r - this.x;
		return this.x + this.width;
	}
	,get_top: function() {
		return this.y;
	}
	,set_top: function(t) {
		this.height += this.y - t;
		this.y = t;
		return this.y;
	}
	,get_bottom: function() {
		return this.y + this.height;
	}
	,set_bottom: function(b) {
		this.height = b - this.y;
		return this.y + this.height;
	}
	,__class__: com_oddcast_neko_geom_Rectangle
};
var com_oddcast_util_ArrayTools = function() { };
$hxClasses["com.oddcast.util.ArrayTools"] = com_oddcast_util_ArrayTools;
com_oddcast_util_ArrayTools.__name__ = ["com","oddcast","util","ArrayTools"];
com_oddcast_util_ArrayTools.firstElement = function(a) {
	var length = a.length;
	if(length > 0) return a[0]; else return null;
};
com_oddcast_util_ArrayTools.first = function(a) {
	return com_oddcast_util_ArrayTools.firstElement(a);
};
com_oddcast_util_ArrayTools.lastElement = function(a) {
	return a[a.length - 1];
};
com_oddcast_util_ArrayTools.randomElement = function(a,random) {
	var length = a.length;
	if(length > 0) return a[Math.floor(random.randomFloat() * length)]; else return null;
};
com_oddcast_util_ArrayTools.getOrMake = function(a,index,callbackFunc) {
	var ret = a[index];
	if(ret == null) {
		ret = callbackFunc();
		a[index] = ret;
	}
	return ret;
};
com_oddcast_util_ArrayTools.populate = function(a,length,callNew) {
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		a[i] = callNew();
	}
	return a;
};
com_oddcast_util_ArrayTools.join = function(a,delimeter) {
	var strBuf_b = "";
	var _g = 0;
	while(_g < a.length) {
		var i = a[_g];
		++_g;
		strBuf_b += Std.string(Std.string(i) + delimeter);
	}
	return strBuf_b;
};
com_oddcast_util_ArrayTools.joinToString = function(a,delimeter,toStringFunction) {
	var strBuf = new StringBuf();
	var _g1 = 0;
	var _g = a.length;
	while(_g1 < _g) {
		var j = _g1++;
		var i = a[j];
		strBuf.add(toStringFunction(i));
		if(j < a.length - 1) if(delimeter == null) strBuf.b += "null"; else strBuf.b += "" + delimeter;
	}
	return strBuf.b;
};
com_oddcast_util_ArrayTools.toXmlCData = function(a,seperator) {
	if(seperator == null) seperator = ",";
	var value = a.join(seperator);
	var cdata = Xml.createCData(value);
	return cdata;
};
com_oddcast_util_ArrayTools.fromXmlCData = function(a,cdata,StringToElementFunction,seperator) {
	if(seperator == null) seperator = ",";
	var values;
	if(cdata.nodeType == Xml.Document || cdata.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + cdata.nodeType);
	values = cdata.nodeValue;
	var valueStr = values;
	var split = valueStr.split(",");
	var _g = 0;
	while(_g < split.length) {
		var s = split[_g];
		++_g;
		a.push(StringToElementFunction(s));
	}
};
com_oddcast_util_ArrayTools.fromXmlCDataInt = function(a,cdata,seperator) {
	if(seperator == null) seperator = ",";
	com_oddcast_util_ArrayTools.fromXmlCData(a,cdata,function(s) {
		return Std.parseInt(s);
	},seperator);
};
com_oddcast_util_ArrayTools.indexOf = function(a,searchElement,fromIndex) {
	if(fromIndex == null) fromIndex = 0;
	if(fromIndex < 0) fromIndex = a.length + fromIndex;
	var _g1 = fromIndex;
	var _g = a.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(a[i] == searchElement) return i;
	}
	return -1;
};
com_oddcast_util_ArrayTools.isWithin = function(a,index) {
	if(index >= 0 && index < a.length) return a[index] != null;
	return false;
};
com_oddcast_util_ArrayTools.isShallowEqualTo = function(a,b) {
	if(a.length != b.length) return -2;
	var _g1 = 0;
	var _g = a.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(a[i] != b[i]) return i;
	}
	return -1;
};
com_oddcast_util_ArrayTools.isDeepEqualTo = function(a,b) {
	if(a.length != b.length) return [-2,-3];
	var _g1 = 0;
	var _g = a.length;
	while(_g1 < _g) {
		var i = _g1++;
		var v = com_oddcast_util_ArrayTools.isShallowEqualTo(a[i],b[i]);
		if(v != -1) return [i,v];
	}
	return [-1,-1];
};
com_oddcast_util_ArrayTools.toIntMap = function(array,map) {
	var i = 0;
	var _g = 0;
	while(_g < array.length) {
		var v = array[_g];
		++_g;
		var value = i++;
		map.set(v,value);
	}
	return map;
};
var com_oddcast_util_ColorTransformTools = function() { };
$hxClasses["com.oddcast.util.ColorTransformTools"] = com_oddcast_util_ColorTransformTools;
com_oddcast_util_ColorTransformTools.__name__ = ["com","oddcast","util","ColorTransformTools"];
com_oddcast_util_ColorTransformTools.isEqualTo = function(a,b,epsilon) {
	if(epsilon == null) epsilon = 0.001;
	if(a == null || b == null) return false;
	if(Math.abs(a.redMultiplier - b.redMultiplier) > epsilon) return false;
	if(Math.abs(a.greenMultiplier - b.greenMultiplier) > epsilon) return false;
	if(Math.abs(a.blueMultiplier - b.blueMultiplier) > epsilon) return false;
	if(Math.abs(a.alphaMultiplier - b.alphaMultiplier) > epsilon) return false;
	if(Math.abs(a.redOffset - b.redOffset) > epsilon) return false;
	if(Math.abs(a.greenOffset - b.greenOffset) > epsilon) return false;
	if(Math.abs(a.blueOffset - b.blueOffset) > epsilon) return false;
	if(Math.abs(a.alphaOffset - b.alphaOffset) > epsilon) return false;
	return true;
};
com_oddcast_util_ColorTransformTools.isIdentity = function(ct,delta) {
	if(delta == null) delta = 0.01;
	return Math.abs(ct.redOffset) < delta && Math.abs(ct.greenOffset) < delta && Math.abs(ct.blueOffset) < delta && Math.abs(ct.alphaOffset) < delta && Math.abs(ct.redMultiplier - 1.0) < delta && Math.abs(ct.greenMultiplier - 1.0) < delta && Math.abs(ct.blueMultiplier - 1.0) < delta && Math.abs(ct.alphaMultiplier - 1.0) < delta;
};
com_oddcast_util_ColorTransformTools.toString = function(a) {
	return " rm:" + com_oddcast_util_UtilsLite.formatFloat(a.redMultiplier) + " gm:" + com_oddcast_util_UtilsLite.formatFloat(a.greenMultiplier) + " bm:" + com_oddcast_util_UtilsLite.formatFloat(a.blueMultiplier) + " am:" + com_oddcast_util_UtilsLite.formatFloat(a.alphaMultiplier) + "    " + " ro:" + com_oddcast_util_UtilsLite.formatFloat(a.redOffset) + " go:" + com_oddcast_util_UtilsLite.formatFloat(a.greenOffset) + " bo:" + com_oddcast_util_UtilsLite.formatFloat(a.blueOffset) + " ao:" + com_oddcast_util_UtilsLite.formatFloat(a.alphaOffset);
};
var com_oddcast_util_EnsureType = function() { };
$hxClasses["com.oddcast.util.EnsureType"] = com_oddcast_util_EnsureType;
com_oddcast_util_EnsureType.__name__ = ["com","oddcast","util","EnsureType"];
com_oddcast_util_EnsureType.isString = function(i,defValue,desc,posInfos) {
	if(desc == null) desc = "";
	if(typeof(i) == "number" || ((i | 0) === i) || typeof(i) == "boolean") if(i == null) return "null"; else return "" + i;
	if(typeof(i) == "string") {
		var trim = StringTools.trim(i);
		return trim;
	}
	com_oddcast_util_EnsureType.reportError(i,desc,"String",defValue,posInfos);
	return defValue;
};
com_oddcast_util_EnsureType.isInt = function(i,defValue,desc,posInfos) {
	if(desc == null) desc = "";
	if(typeof(i) == "string") {
		var trim = StringTools.trim(i);
		if(StringTools.startsWith(trim,"#")) trim = StringTools.replace(trim,"#","0x");
		i = Std.parseInt(trim);
	}
	if(((i | 0) === i)) return i;
	com_oddcast_util_EnsureType.reportError(i,desc,defValue == null?"null":"" + defValue,"Int",posInfos);
	return defValue;
};
com_oddcast_util_EnsureType.isFloat = function(i,defValue,desc,posInfos) {
	if(desc == null) desc = "";
	if(typeof(i) == "string") {
		var trim = StringTools.trim(i);
		i = parseFloat(trim);
	}
	if(typeof(i) == "number" && !isNaN(i) && isFinite(i)) return i;
	com_oddcast_util_EnsureType.reportError(i,desc,"Float",defValue == null?"null":"" + defValue,posInfos);
	return defValue;
};
com_oddcast_util_EnsureType.isBool = function(i,defValue,desc,posInfos) {
	if(desc == null) desc = "";
	if(typeof(i) == "string") {
		var trim = StringTools.trim(i);
		if(trim.toLowerCase() == "true") return true;
		if(trim.toLowerCase() == "false") return false;
	}
	if(typeof(i) == "boolean") return i;
	com_oddcast_util_EnsureType.reportError(i,desc,"Bool",defValue == null?"null":"" + defValue,posInfos);
	return defValue;
};
com_oddcast_util_EnsureType.reportError = function(i,desc,type,usingString,posInfos) {
	if(usingString == null) usingString = "";
	haxe_Log.trace("[WARNING] expecting " + type + " " + desc + " " + Std.string(Type.getClass(i)) + " using:" + usingString + " in " + posInfos.methodName,posInfos);
};
var com_oddcast_util_EnumTools = function() { };
$hxClasses["com.oddcast.util.EnumTools"] = com_oddcast_util_EnumTools;
com_oddcast_util_EnumTools.__name__ = ["com","oddcast","util","EnumTools"];
com_oddcast_util_EnumTools.toString = function(e) {
	return Std.string(e);
};
com_oddcast_util_EnumTools.fromString = function(type,s) {
	return Type.createEnum(type,s);
};
var com_oddcast_util_FloatInterp = function(init,duration,smoothing,plabel,loops,loopStyle) {
	if(loopStyle == null) loopStyle = 0;
	if(loops == null) loops = 1;
	this.duration = com_oddcast_util_Utils.defaultParamFloat(duration,0.0);
	smoothing = com_oddcast_util_Utils.defaultParamFloat(smoothing,0.0);
	this.oscillateValue = this.origValue = this.finalValue = this.currentValue = init;
	this.smoothing = smoothing;
	this.loops = loops;
	this.loopStyle = loopStyle;
	this.countdown = 0;
	this.offset = 0.0;
	this.plabel = plabel;
};
$hxClasses["com.oddcast.util.FloatInterp"] = com_oddcast_util_FloatInterp;
com_oddcast_util_FloatInterp.__name__ = ["com","oddcast","util","FloatInterp"];
com_oddcast_util_FloatInterp.prototype = {
	setDuration: function(d) {
		this.duration = d;
	}
	,setOffset: function(o) {
		this.offset = o;
	}
	,update: function(interval) {
		if((this.offsetCountdown -= interval) <= 0) {
			if(this.countdown > 0) {
				if(this.duration <= 0.00001) this.countdown = 0; else this.countdown -= interval / this.duration;
				if(this.countdown <= 0) {
					this.currentValue = this.finalValue;
					if(this.loops == 10000000 || --this.loops > 0) {
						var _g = this.loopStyle;
						switch(_g) {
						case 0:
							this.currentValue = this.origValue;
							this.set_value(this.finalValue);
							break;
						case 1:
							this.set_value((this.oscillateValue - this.origValue) * this.increaseBy > 0?this.oscillateValue:this.origValue);
							break;
						}
					}
					return;
				}
				var time = this.countdown;
				var smoothloops = this.smoothing;
				while(smoothloops < -0.01) {
					time = Math.sin(time * Math.PI * 0.5);
					smoothloops += 1.0;
				}
				while(smoothloops > 0.01) {
					time = 3 * time * time - 2 * time * time * time;
					smoothloops -= 1.0;
				}
				this.currentValue = this.finalValue + this.increaseBy * time;
			}
		}
	}
	,isActive: function() {
		return this.countdown > 0;
	}
	,set_value: function(finalValue) {
		this.increaseBy = this.currentValue - finalValue;
		this.finalValue = finalValue;
		this.countdown = 1.0;
		this.offsetCountdown = this.offset;
		return this.currentValue;
	}
	,setOscillateValue: function(val) {
		this.oscillateValue = val;
		this.set_value(val);
	}
	,timeLeft: function() {
		var total = 0.0;
		if(this.isActive()) {
			if(this.offsetCountdown > 0) total += this.offsetCountdown;
			total += this.countdown * this.duration;
		}
		return total;
	}
	,get_value: function() {
		return this.currentValue;
	}
	,__class__: com_oddcast_util_FloatInterp
};
var com_oddcast_util_IntInterp = function(init,duration,smoothing) {
	com_oddcast_util_FloatInterp.call(this,init,duration,smoothing);
};
$hxClasses["com.oddcast.util.IntInterp"] = com_oddcast_util_IntInterp;
com_oddcast_util_IntInterp.__name__ = ["com","oddcast","util","IntInterp"];
com_oddcast_util_IntInterp.__super__ = com_oddcast_util_FloatInterp;
com_oddcast_util_IntInterp.prototype = $extend(com_oddcast_util_FloatInterp.prototype,{
	set_valueInt: function(finalValue) {
		this.set_value(finalValue);
		return this.get_valueInt();
	}
	,get_valueInt: function() {
		return this.currentValue | 0;
	}
	,__class__: com_oddcast_util_IntInterp
});
var com_oddcast_util_FloatInterp3 = function(duration,smoothing) {
	this.x = new com_oddcast_util_FloatInterp(0,duration,smoothing);
	this.y = new com_oddcast_util_FloatInterp(0,duration,smoothing);
	this.z = new com_oddcast_util_FloatInterp(0,duration,smoothing);
};
$hxClasses["com.oddcast.util.FloatInterp3"] = com_oddcast_util_FloatInterp3;
com_oddcast_util_FloatInterp3.__name__ = ["com","oddcast","util","FloatInterp3"];
com_oddcast_util_FloatInterp3.prototype = {
	setDuration: function(d) {
		this.x.setDuration(d);
		this.y.setDuration(d);
		this.z.setDuration(d);
	}
	,update: function(interval) {
		this.x.update(interval);
		this.y.update(interval);
		this.z.update(interval);
	}
	,getInterpArray: function() {
		return [this.x,this.y,this.z];
	}
	,__class__: com_oddcast_util_FloatInterp3
};
var com_oddcast_util_FloatSineOscillator = function(init,wavelength) {
	com_oddcast_util_FloatInterp.call(this,init,wavelength,0);
	this.set_value(init);
};
$hxClasses["com.oddcast.util.FloatSineOscillator"] = com_oddcast_util_FloatSineOscillator;
com_oddcast_util_FloatSineOscillator.__name__ = ["com","oddcast","util","FloatSineOscillator"];
com_oddcast_util_FloatSineOscillator.__super__ = com_oddcast_util_FloatInterp;
com_oddcast_util_FloatSineOscillator.prototype = $extend(com_oddcast_util_FloatInterp.prototype,{
	update: function(interval) {
		this.countdown += interval * 4 / this.duration;
		this.currentValue = Math.sin(this.countdown);
	}
	,set_value: function(init) {
		this.countdown = Math.asin(init);
		return this.get_value();
	}
	,__class__: com_oddcast_util_FloatSineOscillator
});
var com_oddcast_util_Id3Entry = function() {
};
$hxClasses["com.oddcast.util.Id3Entry"] = com_oddcast_util_Id3Entry;
com_oddcast_util_Id3Entry.__name__ = ["com","oddcast","util","Id3Entry"];
com_oddcast_util_Id3Entry.getHeaderSize = function(major) {
	switch(major) {
	case 2:
		return 6;
	case 3:
		return 10;
	case 4:
		return 10;
	default:
		return -1;
	}
};
com_oddcast_util_Id3Entry.prototype = {
	parse: function(major,data) {
		switch(major) {
		case 2:
			this.frameID = data.readString(3);
			this.size = data.readUInt24();
			this.flags = 0;
			break;
		case 3:
			this.frameID = data.readString(4);
			this.size = data.readUInt32();
			this.flags = data.readUInt16();
			break;
		case 4:
			this.frameID = data.readString(4);
			this.size = data.readSynchsafeInteger32();
			this.flags = data.readUInt16();
			break;
		}
		if(this.size == 0) return null;
		this.text = data.readString(this.size);
		return this;
	}
	,__class__: com_oddcast_util_Id3Entry
};
var haxe_Http = function(url) {
	this.url = url;
	this.headers = new List();
	this.params = new List();
	this.async = true;
};
$hxClasses["haxe.Http"] = haxe_Http;
haxe_Http.__name__ = ["haxe","Http"];
haxe_Http.prototype = {
	request: function(post) {
		var me = this;
		me.responseData = null;
		var r = this.req = js_Browser.createXMLHttpRequest();
		var onreadystatechange = function(_) {
			if(r.readyState != 4) return;
			var s;
			try {
				s = r.status;
			} catch( e ) {
				haxe_CallStack.lastException = e;
				if (e instanceof js__$Boot_HaxeError) e = e.val;
				s = null;
			}
			if(s != null) {
				var protocol = window.location.protocol.toLowerCase();
				var rlocalProtocol = new EReg("^(?:about|app|app-storage|.+-extension|file|res|widget):$","");
				var isLocal = rlocalProtocol.match(protocol);
				if(isLocal) if(r.responseText != null) s = 200; else s = 404;
			}
			if(s == undefined) s = null;
			if(s != null) me.onStatus(s);
			if(s != null && s >= 200 && s < 400) {
				me.req = null;
				me.onData(me.responseData = r.responseText);
			} else if(s == null) {
				me.req = null;
				me.onError("Failed to connect or resolve host");
			} else switch(s) {
			case 12029:
				me.req = null;
				me.onError("Failed to connect to host");
				break;
			case 12007:
				me.req = null;
				me.onError("Unknown host");
				break;
			default:
				me.req = null;
				me.responseData = r.responseText;
				me.onError("Http Error #" + r.status);
			}
		};
		if(this.async) r.onreadystatechange = onreadystatechange;
		var uri = this.postData;
		if(uri != null) post = true; else {
			var _g_head = this.params.h;
			var _g_val = null;
			while(_g_head != null) {
				var p;
				p = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				if(uri == null) uri = ""; else uri += "&";
				uri += encodeURIComponent(p.param) + "=" + encodeURIComponent(p.value);
			}
		}
		try {
			if(post) r.open("POST",this.url,this.async); else if(uri != null) {
				var question = this.url.split("?").length <= 1;
				r.open("GET",this.url + (question?"?":"&") + uri,this.async);
				uri = null;
			} else r.open("GET",this.url,this.async);
		} catch( e1 ) {
			haxe_CallStack.lastException = e1;
			if (e1 instanceof js__$Boot_HaxeError) e1 = e1.val;
			me.req = null;
			this.onError(e1.toString());
			return;
		}
		if(!Lambda.exists(this.headers,function(h) {
			return h.header == "Content-Type";
		}) && post && this.postData == null) r.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		var _g_head1 = this.headers.h;
		var _g_val1 = null;
		while(_g_head1 != null) {
			var h1;
			h1 = (function($this) {
				var $r;
				_g_val1 = _g_head1[0];
				_g_head1 = _g_head1[1];
				$r = _g_val1;
				return $r;
			}(this));
			r.setRequestHeader(h1.header,h1.value);
		}
		r.send(uri);
		if(!this.async) onreadystatechange(null);
	}
	,onData: function(data) {
	}
	,onError: function(msg) {
	}
	,onStatus: function(status) {
	}
	,__class__: haxe_Http
};
var com_oddcast_util_HttpFileRead = function(url) {
	haxe_Http.call(this,url);
};
$hxClasses["com.oddcast.util.HttpFileRead"] = com_oddcast_util_HttpFileRead;
com_oddcast_util_HttpFileRead.__name__ = ["com","oddcast","util","HttpFileRead"];
com_oddcast_util_HttpFileRead.__super__ = haxe_Http;
com_oddcast_util_HttpFileRead.prototype = $extend(haxe_Http.prototype,{
	request: function(post) {
		haxe_Http.prototype.request.call(this,post);
	}
	,__class__: com_oddcast_util_HttpFileRead
});
var com_oddcast_util_ID3 = function() {
};
$hxClasses["com.oddcast.util.ID3"] = com_oddcast_util_ID3;
com_oddcast_util_ID3.__name__ = ["com","oddcast","util","ID3"];
com_oddcast_util_ID3.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_util_ID3.prototype = {
	load: function(url,cb,errorCallback,id3String) {
		var _g = this;
		com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "ID3.hx", lineNumber : 71, className : "com.oddcast.util.ID3", methodName : "load"});
		if(id3String != null) {
			this.parse(id3String,errorCallback,url,cb);
			return;
		}
		if(StringTools.startsWith(url,"/android_asset")) {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var res = xmlhttp.responseText;
					console.log(res);
					console.log('@@');
					_g.parse(res,errorCallback,url,cb);
				}
			};
			xmlhttp.open("GET",url,true);
			xmlhttp.send();
		} else {
		console.log('Mp3 case ::');
			var mp3 = new haxe_Http(url);
			mp3.onError = function(msg) {
				if(errorCallback != null) errorCallback(_g,url,msg);
			};
			mp3.onData = function(id3String1) {
				_g.parse(id3String1,errorCallback,url,cb);
			};
			mp3.onStatus = function(status) {
			};
			mp3.request(false);
		}
	}
	,getTag: function(tag) {
		var id3Entry = this.id3Entries.get(tag);
		if(id3Entry == null) return null;
		return id3Entry.text;
	}
	,parse: function(str,errorCallback,url,cb) {
		haxe_Log.trace(str,{ fileName : "ID3.hx", lineNumber : 122, className : "com.oddcast.util.ID3", methodName : "parse"});
		var data = new com_oddcast_io_BinaryAsTextInput(str,true);
		var id3Sig = data.readString(3);
		if(id3Sig != "ID3") {
			errorCallback(this,url,"header " + id3Sig + " does != 'ID3' ");
			return;
		}
		this.major = data.readUInt8();
		if(this.major < 2 || this.major > 4) {
			errorCallback(this,url,"major version " + this.major + " is not yet handled");
			return;
		}
		this.minor = data.readUInt8();
		this.flags = data.readUInt8();
		this.size = data.readSynchsafeInteger32();
		if((this.flags & 64) != 0) {
			var xheadersize = data.readUInt32();
			data.incPos(xheadersize);
		}
		this.id3Entries = new haxe_ds_StringMap();
		var finalPos = this.size + 10;
		var frameHeaderSize = com_oddcast_util_Id3Entry.getHeaderSize(this.major);
		while(data.position < finalPos + frameHeaderSize) {
			var id3Entry = new com_oddcast_util_Id3Entry().parse(this.major,data);
			if(id3Entry != null) {
				this.id3Entries.set(id3Entry.frameID,id3Entry);
				if(id3Entry.frameID == "COMM") break;
			}
		}
		data.dispose();
		cb(this);
	}
	,dispose: function() {
		this.id3Entries = null;
	}
	,__class__: com_oddcast_util_ID3
};
var com_oddcast_util_Disposable = function() { };
$hxClasses["com.oddcast.util.Disposable"] = com_oddcast_util_Disposable;
com_oddcast_util_Disposable.__name__ = ["com","oddcast","util","Disposable"];
com_oddcast_util_Disposable.disposeIfValid = function(disposable) {
	if(disposable != null) disposable.dispose();
	return null;
};
com_oddcast_util_Disposable.disposeIterableIfValid = function(iterable) {
	if(iterable != null && Lambda.count(iterable) > 0) {
		var $it0 = $iterator(iterable)();
		while( $it0.hasNext() ) {
			var i = $it0.next();
			com_oddcast_util_Disposable.disposeIfValid(i);
		}
	}
	return null;
};
com_oddcast_util_Disposable.disposeIterableIterableIfValid = function(iterable) {
	if(iterable != null && Lambda.count(iterable) > 0) {
		var $it0 = $iterator(iterable)();
		while( $it0.hasNext() ) {
			var i = $it0.next();
			com_oddcast_util_Disposable.disposeIterableIfValid(i);
		}
	}
	return null;
};
com_oddcast_util_Disposable.disposeMapIfValid = function(iterable) {
	if(iterable != null) {
		var $it0 = iterable.iterator();
		while( $it0.hasNext() ) {
			var i = $it0.next();
			com_oddcast_util_Disposable.disposeIfValid(i);
		}
	}
	return null;
};
var com_oddcast_util_IndexedArray = function(data) {
	this.reset();
	if(data != null) {
		var _g = 0;
		while(_g < data.length) {
			var d = data[_g];
			++_g;
			this.push(d);
		}
	}
};
$hxClasses["com.oddcast.util.IndexedArray"] = com_oddcast_util_IndexedArray;
com_oddcast_util_IndexedArray.__name__ = ["com","oddcast","util","IndexedArray"];
com_oddcast_util_IndexedArray.prototype = {
	reset: function() {
		this.array = [];
		this.resetIndex();
	}
	,unshift: function(v) {
		this.array.unshift(v);
	}
	,push: function(v) {
		this.array.push(v);
	}
	,set: function(v,i) {
		this.array[i] = v;
	}
	,get: function(i) {
		return this.array[i];
	}
	,length: function() {
		return this.array.length;
	}
	,iterator: function() {
		return HxOverrides.iter(this.array);
	}
	,insert: function(v) {
		this.array.splice(this.index + 1,0,v);
	}
	,increment: function(inc) {
		if(inc == null) inc = 1;
		if(this.array.length > 0) {
			this.incCount++;
			this.index += inc;
			while(this.index < 0) this.index += this.array.length;
			while(this.index >= this.array.length) this.index -= this.array.length;
			return this.array[this.index];
		}
		return null;
	}
	,isFinished: function() {
		return this.incCount >= this.array.length;
	}
	,randomElement: function() {
		return this.array[com_oddcast_util_Random.getRandomInt(this.array.length)];
	}
	,setIndexToLastPushed: function() {
		this.index = this.array.length - 1;
	}
	,resetIndex: function() {
		this.index = -1;
		this.incCount = 0;
	}
	,toString: function() {
		return " index:" + this.index + " incCount:" + this.incCount + " length:" + this.array.length;
	}
	,__class__: com_oddcast_util_IndexedArray
};
var com_oddcast_util_Macros = function() { };
$hxClasses["com.oddcast.util.Macros"] = com_oddcast_util_Macros;
com_oddcast_util_Macros.__name__ = ["com","oddcast","util","Macros"];
var com_oddcast_util_MapTools = function() { };
$hxClasses["com.oddcast.util.MapTools"] = com_oddcast_util_MapTools;
com_oddcast_util_MapTools.__name__ = ["com","oddcast","util","MapTools"];
var com_oddcast_util_Matrix23Tools = function() { };
$hxClasses["com.oddcast.util.Matrix23Tools"] = com_oddcast_util_Matrix23Tools;
com_oddcast_util_Matrix23Tools.__name__ = ["com","oddcast","util","Matrix23Tools"];
com_oddcast_util_Matrix23Tools.transformRectangle = function(matrix23,rect) {
	var topLeft = matrix23.transformPoint(new com_oddcast_neko_geom_Point(rect.get_left(),rect.get_top()));
	var topRight = matrix23.transformPoint(new com_oddcast_neko_geom_Point(rect.get_right(),rect.get_top()));
	var bottomLeft = matrix23.transformPoint(new com_oddcast_neko_geom_Point(rect.get_left(),rect.get_bottom()));
	var bottomRight = matrix23.transformPoint(new com_oddcast_neko_geom_Point(rect.get_right(),rect.get_bottom()));
	var nLeft = Math.min(Math.min(topLeft.x,topRight.x),Math.min(bottomLeft.x,bottomRight.x));
	var nRight = Math.max(Math.max(topLeft.x,topRight.x),Math.max(bottomLeft.x,bottomRight.x));
	var nTop = Math.min(Math.min(topLeft.y,topRight.y),Math.min(bottomLeft.y,bottomRight.y));
	var nBottom = Math.max(Math.max(topLeft.y,topRight.y),Math.max(bottomLeft.y,bottomRight.y));
	return new com_oddcast_neko_geom_Rectangle(nLeft,nTop,nRight - nLeft,nBottom - nTop);
};
com_oddcast_util_Matrix23Tools.rotateDegrees = function(matrix23,deg) {
	matrix23.rotate(deg * Math.PI / 180);
};
com_oddcast_util_Matrix23Tools.isIdentity = function(m,epsilon) {
	if(epsilon == null) epsilon = 0.000001;
	return Math.abs(m.a - 1.0) < epsilon && Math.abs(m.b) < epsilon && Math.abs(m.c) < epsilon && Math.abs(m.d - 1.0) < epsilon && Math.abs(m.tx) < epsilon && Math.abs(m.ty) < epsilon;
};
var com_oddcast_util_MaxMinFloat = function() {
	this.resetMinMax();
};
$hxClasses["com.oddcast.util.MaxMinFloat"] = com_oddcast_util_MaxMinFloat;
com_oddcast_util_MaxMinFloat.__name__ = ["com","oddcast","util","MaxMinFloat"];
com_oddcast_util_MaxMinFloat.prototype = {
	set: function(v) {
		this.value = v;
		if(this.value > this.max) this.max = this.value;
		if(this.value < this.min) this.min = this.value;
		return this;
	}
	,resetMinMax: function() {
		this.min = Infinity;
		this.max = -Infinity;
	}
	,toString: function(nDec) {
		if(nDec == null) nDec = 3;
		return " value:" + com_oddcast_util_UtilsLite.formatFloat(this.value,nDec) + " min:" + com_oddcast_util_UtilsLite.formatFloat(this.min,nDec) + " max:" + com_oddcast_util_UtilsLite.formatFloat(this.max,nDec) + " var:" + com_oddcast_util_UtilsLite.formatFloat(this.max - this.min,nDec);
	}
	,__class__: com_oddcast_util_MaxMinFloat
};
var com_oddcast_util_PointTools = function() { };
$hxClasses["com.oddcast.util.PointTools"] = com_oddcast_util_PointTools;
com_oddcast_util_PointTools.__name__ = ["com","oddcast","util","PointTools"];
com_oddcast_util_PointTools.getCenterPoint = function(near,far) {
	return new com_oddcast_neko_geom_Point((near.x + far.x) * 0.5,(near.y + far.y) * 0.5);
};
com_oddcast_util_PointTools.distanceTo = function(near,far) {
	var dx = near.x - far.x;
	var dy = near.y - far.y;
	return Math.sqrt(dx * dx + dy * dy);
};
com_oddcast_util_PointTools.distanceSqrTo = function(near,far) {
	var dx = near.x - far.x;
	var dy = near.y - far.y;
	return dx * dx + dy * dy;
};
com_oddcast_util_PointTools.vectorTo = function(near,far) {
	return new com_oddcast_neko_geom_Point(near.x - far.x,near.y - far.y);
};
com_oddcast_util_PointTools.scale_inPlace = function(p,by) {
	p.x *= by;
	p.y *= by;
};
com_oddcast_util_PointTools.add_inPlace = function(p,other) {
	p.x += other.x;
	p.y += other.y;
};
com_oddcast_util_PointTools.lengthSqr = function(p) {
	return p.x * p.x + p.y * p.y;
};
com_oddcast_util_PointTools.toString = function(point,prec) {
	if(prec == null) prec = 3;
	return " x:" + com_oddcast_util_Utils.formatFloat(point.x,prec) + " y:" + com_oddcast_util_Utils.formatFloat(point.y,prec);
};
com_oddcast_util_PointTools.toXml = function(point,id,precision) {
	if(precision == null) precision = 3;
	if(point == null) return null;
	var pointEl = com_oddcast_util_XmlTools.createElementWithID(com_oddcast_util_PointTools.POINT_ELEMENT_NAME,id);
	com_oddcast_util_PointTools.attributes(point,pointEl,true);
	return pointEl;
};
com_oddcast_util_PointTools.fromXml = function(pointEl) {
	return com_oddcast_util_PointTools.attributes(new com_oddcast_neko_geom_Point(),pointEl,false);
};
com_oddcast_util_PointTools.tryFromXml = function(parent,def,id) {
	var e = com_oddcast_util_XmlTools.getElement(parent,com_oddcast_util_PointTools.POINT_ELEMENT_NAME,id);
	if(e == null) return def;
	return com_oddcast_util_PointTools.fromXml(e);
};
com_oddcast_util_PointTools.attributes = function(point,xml,bWrite) {
	point.x = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"x",point.x,3);
	point.y = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"y",point.y,3);
	return point;
};
var com_oddcast_util_Random = function(seed) {
	this.reseed(seed);
};
$hxClasses["com.oddcast.util.Random"] = com_oddcast_util_Random;
com_oddcast_util_Random.__name__ = ["com","oddcast","util","Random"];
com_oddcast_util_Random.getRandomInt = function(max) {
	if(com_oddcast_util_Random.r == null) com_oddcast_util_Random.r = new com_oddcast_util_Random();
	return com_oddcast_util_Random.r.randomInt() % max;
};
com_oddcast_util_Random.prototype = {
	reseed: function(seed) {
		if(seed == null) seed = 102030405;
		this.seed = seed;
	}
	,randomFloat: function(mult) {
		if(mult == null) mult = 1.0;
		return this.randomInt() * mult / 32768;
	}
	,randomInt: function() {
		this.seed = 2147483647 & this.seed * 1103515245 + 12345;
		var i = this.seed / 65536 | 0;
		if(i < 0) i = 1 - i;
		return i % 32768;
	}
	,__class__: com_oddcast_util_Random
};
var com_oddcast_util_RectangleTools = function() { };
$hxClasses["com.oddcast.util.RectangleTools"] = com_oddcast_util_RectangleTools;
com_oddcast_util_RectangleTools.__name__ = ["com","oddcast","util","RectangleTools"];
com_oddcast_util_RectangleTools.newFromCorners = function(left,top,right,bottom) {
	return new com_oddcast_neko_geom_Rectangle(left,top,right - left,bottom - top);
};
com_oddcast_util_RectangleTools.newFromCenter = function(xCenter,yCenter,width,height) {
	return new com_oddcast_neko_geom_Rectangle(xCenter - width * 0.5,yCenter - height * 0.5,width,height);
};
com_oddcast_util_RectangleTools.newFromCenterAsym = function(xCenter,yCenter,leftWidth,rightWidth,topHeight,bottomHeight) {
	return new com_oddcast_neko_geom_Rectangle(xCenter - leftWidth,yCenter - topHeight,leftWidth + rightWidth,topHeight + bottomHeight);
};
com_oddcast_util_RectangleTools.getCenterPoint = function(rect) {
	return new com_oddcast_neko_geom_Point(rect.x + rect.width * 0.5,rect.y + rect.height * 0.5);
};
com_oddcast_util_RectangleTools.getCenterPointProvided = function(rect,p) {
	p.x = rect.x + rect.width * 0.5;
	p.y = rect.y + rect.height * 0.5;
};
com_oddcast_util_RectangleTools.centerOnPoint = function(rect,center) {
	com_oddcast_util_RectangleTools.centerOn(rect,center.x,center.y);
};
com_oddcast_util_RectangleTools.centerOn = function(rect,cx,cy) {
	rect.x = cx - rect.width * 0.5;
	rect.y = cy - rect.height * 0.5;
};
com_oddcast_util_RectangleTools.scaleAroundCenter = function(rect,xScale,yScale) {
	if(yScale == null) yScale = xScale;
	var dx = rect.width * (xScale - 1.0) * 0.5;
	var dy = rect.height * (yScale - 1.0) * 0.5;
	rect.inflate(dx,dy);
	return rect;
};
com_oddcast_util_RectangleTools.integerAlign = function(rect) {
	var left = rect.x | 0;
	var right = Std["int"](rect.get_right());
	var top = rect.y | 0;
	var bottom = Std["int"](rect.get_bottom());
	rect.x = left;
	rect.set_right(right);
	rect.set_top(top);
	rect.set_bottom(bottom);
	return rect;
};
com_oddcast_util_RectangleTools.area = function(rect) {
	return rect.width * rect.height;
};
com_oddcast_util_RectangleTools.scaleDimensions = function(rect,xScale,yScale) {
	if(yScale == null) yScale = xScale;
	rect.width *= xScale;
	rect.height *= yScale;
	return rect;
};
com_oddcast_util_RectangleTools.inflateBy = function(rect,xScale,yScale) {
	if(yScale == null) yScale = xScale;
	rect.inflate((xScale - 1.0) * rect.width,(yScale - 1.0) * rect.height);
};
com_oddcast_util_RectangleTools.enlargeByPoint = function(rect,p) {
	if(p.x >= 0) rect.width += p.x; else {
		rect.x += p.x;
		rect.width -= p.x;
	}
	if(p.y >= 0) rect.height += p.y; else {
		rect.y += p.y;
		rect.height -= p.y;
	}
};
com_oddcast_util_RectangleTools.enlargeByPixels = function(rect,x,y) {
	var _g = rect;
	_g.set_left(_g.get_left() - x);
	var _g1 = rect;
	_g1.set_right(_g1.get_right() + x);
	var _g2 = rect;
	_g2.set_top(_g2.get_top() - y);
	var _g3 = rect;
	_g3.set_bottom(_g3.get_bottom() + y);
};
com_oddcast_util_RectangleTools.topRight = function(rect) {
	return new com_oddcast_neko_geom_Point(rect.get_right(),rect.get_top());
};
com_oddcast_util_RectangleTools.bottomLeft = function(rect) {
	return new com_oddcast_neko_geom_Point(rect.get_left(),rect.get_bottom());
};
com_oddcast_util_RectangleTools.bottomCenter = function(rect) {
	return new com_oddcast_neko_geom_Point((rect.get_left() + rect.get_right()) * 0.5,rect.get_bottom());
};
com_oddcast_util_RectangleTools.topCenter = function(rect) {
	return new com_oddcast_neko_geom_Point((rect.get_left() + rect.get_right()) * 0.5,rect.get_top());
};
com_oddcast_util_RectangleTools.leftCenter = function(rect) {
	return new com_oddcast_neko_geom_Point(rect.get_left(),(rect.get_bottom() + rect.get_top()) * 0.5);
};
com_oddcast_util_RectangleTools.rightCenter = function(rect) {
	return new com_oddcast_neko_geom_Point(rect.get_right(),(rect.get_bottom() + rect.get_top()) * 0.5);
};
com_oddcast_util_RectangleTools.getPointXY = function(rect,px,py) {
	return new com_oddcast_neko_geom_Point(rect.x + px * rect.width,rect.y + py * rect.height);
};
com_oddcast_util_RectangleTools.getPoint = function(rect,point) {
	return com_oddcast_util_RectangleTools.getPointXY(rect,point.x,point.y);
};
com_oddcast_util_RectangleTools.mirrorPoint = function(rect,p) {
	return new com_oddcast_neko_geom_Point(rect.x + rect.x + rect.width - p.x,p.y);
};
com_oddcast_util_RectangleTools.createLeftTopRightBottom = function(left,top,right,bottom) {
	return new com_oddcast_neko_geom_Rectangle(left,top,right - left,bottom - top);
};
com_oddcast_util_RectangleTools.createAround = function(center,width,height) {
	return new com_oddcast_neko_geom_Rectangle(center.x - width / 2,center.y - height / 2,width,height);
};
com_oddcast_util_RectangleTools.hypotenuse = function(rect) {
	return Math.sqrt(rect.x * rect.x + rect.y * rect.y);
};
com_oddcast_util_RectangleTools.aspect = function(rect) {
	return rect.width / rect.height;
};
com_oddcast_util_RectangleTools.expandPixelAlign = function(rect,align) {
	var remove = align - 1;
	var mask = remove ^ -1;
	var left = (rect.x | 0) & mask;
	var top = (rect.y | 0) & mask;
	var right = Std["int"](rect.get_right());
	if((right & remove) != 0) right = (right & mask) + align;
	var bottom = Std["int"](rect.get_bottom());
	if((bottom & remove) != 0) bottom = (bottom & mask) + align;
	rect.x = left;
	rect.set_right(right);
	rect.set_top(top);
	rect.set_bottom(bottom);
	return rect;
};
com_oddcast_util_RectangleTools.scale = function(rect,xScale,yScale) {
	if(yScale == null) yScale = xScale;
	return new com_oddcast_neko_geom_Rectangle(rect.x * xScale,rect.y * yScale,rect.width * xScale,rect.height * yScale);
};
com_oddcast_util_RectangleTools.degreeOfOverlap = function(rect,other) {
	return com_oddcast_util_RectangleTools.area(rect.intersection(other)) / com_oddcast_util_RectangleTools.area(rect.union(other));
};
com_oddcast_util_RectangleTools.toString = function(rect) {
	return " x:" + rect.x + " y:" + rect.y + " width:" + rect.width + " height:" + rect.height;
};
com_oddcast_util_RectangleTools.toXml = function(rect,id,precision) {
	if(precision == null) precision = 3;
	var rectEl = com_oddcast_util_XmlTools.createElementWithID(com_oddcast_util_RectangleTools.RECTANGLE_ELEMENT_NAME,id);
	com_oddcast_util_RectangleTools.attributes(rect,rectEl,true,precision);
	return rectEl;
};
com_oddcast_util_RectangleTools.fromXml = function(rectEl) {
	return com_oddcast_util_RectangleTools.attributes(new com_oddcast_neko_geom_Rectangle(),rectEl,false);
};
com_oddcast_util_RectangleTools.tryFromXml = function(parent,def,id) {
	var e = com_oddcast_util_XmlTools.getElement(parent,com_oddcast_util_RectangleTools.RECTANGLE_ELEMENT_NAME,id);
	if(e == null) return def;
	return com_oddcast_util_RectangleTools.fromXml(e);
};
com_oddcast_util_RectangleTools.attributes = function(rect,xml,bWrite,precision) {
	if(precision == null) precision = 3;
	rect.x = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"x",rect.x,precision,0.0);
	rect.y = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"y",rect.y,precision,0.0);
	rect.width = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"width",rect.width,precision,0.0);
	rect.height = com_oddcast_util_XmlTools.attributesIOFloat(xml,bWrite,"height",rect.height,precision,0.0);
	return rect;
};
var com_oddcast_util_SmartTrace = function() { };
$hxClasses["com.oddcast.util.SmartTrace"] = com_oddcast_util_SmartTrace;
com_oddcast_util_SmartTrace.__name__ = ["com","oddcast","util","SmartTrace"];
com_oddcast_util_SmartTrace.init = function() {
	com_oddcast_util_SmartTrace.origTrace = haxe_Log.trace;
	haxe_Log.trace = com_oddcast_util_SmartTrace.myTrace;
	com_oddcast_util_SmartTrace.lastMap = new haxe_ds_StringMap();
};
com_oddcast_util_SmartTrace.myTrace = function(v,inf) {
	var key = inf.fileName + ":" + inf.lineNumber;
	var value = "" + Std.string(v);
	if(StringTools.startsWith(value,com_oddcast_util_SmartTrace.TRACE_UPDATED)) {
		if(com_oddcast_util_SmartTrace.lastMap != null) {
			if(com_oddcast_util_SmartTrace.lastMap.exists(key)) {
				if(com_oddcast_util_SmartTrace.lastMap.get(key) == value) return;
			}
			com_oddcast_util_SmartTrace.lastMap.set(key,value);
		}
	}
	var colCode = "1";
	if(value.charAt(1) == ":") {
		var code = value.charAt(0);
		if(com_oddcast_util_SmartTrace.ALL_CODES.indexOf(code) >= 0) {
			colCode = code;
			value = HxOverrides.substr(value,2,null);
		}
	}
	if(StringTools.startsWith(value,"[ERROR]")) colCode = "3";
	if(StringTools.startsWith(value,"[WARNING]")) colCode = "2";
	var arg = value;
	var bgc = "white";
	var color;
	switch(colCode) {
	case "0":
		color = "gray";
		break;
	case "1":
		color = "black";
		break;
	case "2":
		color = "GoldenRod";
		break;
	case "3":
		color = "red";
		break;
	case "4":
		color = "magenta";
		break;
	default:
		color = "black";
	}
	console.log("%c" + arg,"color:" + color + ";font-weight:bold; background-color: " + bgc + ";");
};
var com_oddcast_util_StringTools2 = function() { };
$hxClasses["com.oddcast.util.StringTools2"] = com_oddcast_util_StringTools2;
com_oddcast_util_StringTools2.__name__ = ["com","oddcast","util","StringTools2"];
com_oddcast_util_StringTools2.loadAsString = function(url,errorCallback,successCallback) {
	var r = new haxe_Http(url);
	r.onError = function(msg) {
		if(errorCallback != null) errorCallback(url,msg);
	};
	r.onData = function(str) {
		successCallback(str);
	};
	r.request(false);
};
com_oddcast_util_StringTools2.intToString = function($int,zeroPad) {
	if(zeroPad == null) zeroPad = 0;
	var base = "" + $int;
	return StringTools.lpad(base,"0",zeroPad);
};
com_oddcast_util_StringTools2.clipEnds = function(input,offBeginning,offEnd) {
	if(offEnd == null) offEnd = 0;
	if(offBeginning == null) offBeginning = 0;
	var ret = HxOverrides.substr(input,offBeginning,input.length - offEnd - offBeginning);
	return ret;
};
com_oddcast_util_StringTools2.repairDoubleCR = function(input) {
	return StringTools.replace(input,"\r" + com_oddcast_util_StringTools2.CRLF,com_oddcast_util_StringTools2.CRLF);
};
com_oddcast_util_StringTools2.CRLFtoLF = function(input) {
	return StringTools.replace(input,com_oddcast_util_StringTools2.CRLF,com_oddcast_util_StringTools2.LF);
};
com_oddcast_util_StringTools2.toHexCodes = function(input,startIndex,length) {
	if(length == null) length = 5;
	var strBuf = new StringBuf();
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		strBuf.add(" " + com_oddcast_util_UtilsLite.hex0x(HxOverrides.cca(input,startIndex + i)));
	}
	return strBuf.b;
};
com_oddcast_util_StringTools2.toCharCodes = function(input) {
	var string = "";
	var _g1 = 0;
	var _g = input.length;
	while(_g1 < _g) {
		var i = _g1++;
		string += HxOverrides.cca(input,i) + "-" + input.charAt(i) + "  ";
	}
	return string;
};
com_oddcast_util_StringTools2.splitTab = function(input,repString) {
	var line = StringTools.replace(input,"\t",repString);
	return line.split(repString);
};
com_oddcast_util_StringTools2.splitInt = function(input,sep) {
	var split = input.split(sep);
	var retval = [];
	var _g = 0;
	while(_g < split.length) {
		var s = split[_g];
		++_g;
		retval.push(Std.parseInt(s));
	}
	return retval;
};
com_oddcast_util_StringTools2.splitFloat = function(input,sep) {
	var split = input.split(sep);
	var retval = [];
	var _g = 0;
	while(_g < split.length) {
		var s = split[_g];
		++_g;
		retval.push(parseFloat(s));
	}
	return retval;
};
com_oddcast_util_StringTools2.splitLineFeeds = function(input) {
	var split = input.split(com_oddcast_util_StringTools2.CRLF);
	if(split.length == 1) {
		split = input.split(com_oddcast_util_StringTools2.LF);
		if(split.length == 1) split = input.split(com_oddcast_util_StringTools2.CR);
	}
	return split;
};
com_oddcast_util_StringTools2.makeURLsafe = function(input) {
	var safename = StringTools.replace(input,",","_");
	safename = StringTools.replace(safename,"#","_");
	return safename;
};
com_oddcast_util_StringTools2.stripQuotes = function(input) {
	if(StringTools.startsWith(input,"\"") && StringTools.endsWith(input,"\"")) return com_oddcast_util_StringTools2.clipEnds(input,1,1);
	return input;
};
com_oddcast_util_StringTools2.copyUpToStartOf = function(input,term) {
	var len = input.indexOf(term);
	if(len < 0) return null;
	return HxOverrides.substr(input,0,len);
};
com_oddcast_util_StringTools2.copyUpTo = function(input,term) {
	var len = input.indexOf(term);
	if(len < 0) return null;
	return HxOverrides.substr(input,0,len + term.length);
};
com_oddcast_util_StringTools2.copyFromAfter = function(input,term) {
	var len = input.indexOf(term);
	if(len < 0) return null;
	return HxOverrides.substr(input,len + term.length,null);
};
com_oddcast_util_StringTools2.equals = function(a,b) {
	if(b != null) {
		if(a.length == b.length) {
			if(StringTools.startsWith(a,b)) return true;
		}
	}
	return false;
};
var com_oddcast_util_Utils = function() { };
$hxClasses["com.oddcast.util.Utils"] = com_oddcast_util_Utils;
com_oddcast_util_Utils.__name__ = ["com","oddcast","util","Utils"];
com_oddcast_util_Utils.getFileExtension = function(filename) {
	return com_oddcast_util_UtilsLite.getFileExtension(filename);
};
com_oddcast_util_Utils.stripFileExtension = function(filename) {
	return com_oddcast_util_UtilsLite.stripFileExtension(filename);
};
com_oddcast_util_Utils.stripFileDirectory = function(filename) {
	return com_oddcast_util_UtilsLite.stripFileDirectory(filename);
};
com_oddcast_util_Utils.getFileDirectory = function(filename) {
	return com_oddcast_util_UtilsLite.getFileDirectory(filename);
};
com_oddcast_util_Utils.ensureDirectoryEndsWithSlash = function(directoryName) {
	var lastChar = directoryName.charAt(directoryName.length - 1);
	if(lastChar == "/" || lastChar == "\\") return directoryName;
	return directoryName + "\\";
};
com_oddcast_util_Utils.imin = function(a,b) {
	if(a < b) return a; else return b;
};
com_oddcast_util_Utils.imax = function(a,b) {
	if(a > b) return a; else return b;
};
com_oddcast_util_Utils.format = function(s,subs) {
	var _g1 = 0;
	var _g = subs.length;
	while(_g1 < _g) {
		var i = _g1++;
		s = StringTools.replace(s,"{" + i + "}",subs[i]);
	}
	return s;
};
com_oddcast_util_Utils.padL = function(str,padLength,paddingChar) {
	while(str.length < padLength) str = paddingChar + str;
	return str;
};
com_oddcast_util_Utils.padR = function(str,padLength,paddingChar) {
	while(str.length < padLength) str += paddingChar;
	return str;
};
com_oddcast_util_Utils.getTextLineIterator = function(text) {
	if(text == null) return null;
	var split = text.split("\r" + "\n");
	if(split.length == 1) split = text.split("\n");
	return HxOverrides.iter(split);
};
com_oddcast_util_Utils.hashKeysAlphabetically = function(keys) {
	var ret = [];
	while( keys.hasNext() ) {
		var k = keys.next();
		ret.push(k);
	}
	com_oddcast_util_Utils.sortArrayAlphabetical(ret);
	return ret;
};
com_oddcast_util_Utils.sortArrayAlphabetical = function(array) {
	if(array != null) array.sort(com_oddcast_util_Utils.sortOnAlphabetical);
	return array;
};
com_oddcast_util_Utils.sortOnAlphabetical = function(a,b) {
	if(a == null || b == null) return 1;
	var aStr = a.toLowerCase();
	var bStr = b.toLowerCase();
	var minLength = Std["int"](Math.min(aStr.length,bStr.length));
	var _g = 0;
	while(_g < minLength) {
		var c = _g++;
		var diff = HxOverrides.cca(aStr,c) - HxOverrides.cca(bStr,c);
		if(diff < 0) return -1;
		if(diff > 0) return 1;
	}
	if(aStr.length < bStr.length) return 1; else return -1;
};
com_oddcast_util_Utils.defaultParamFloat = function(input,def) {
	if(input == null) return def; else return input;
};
com_oddcast_util_Utils.defaultParamBool = function(input,def) {
	if(input == null) return def; else return input;
};
com_oddcast_util_Utils.defaultParamInt = function(input,def) {
	if(input == null) return def; else return input;
};
com_oddcast_util_Utils.defaultParamString = function(input,def) {
	if(input == null) return def; else return input;
};
com_oddcast_util_Utils.stringChars = function(str) {
	var retval = "";
	var _g1 = 0;
	var _g = str.length;
	while(_g1 < _g) {
		var i = _g1++;
		retval += str.charAt(i) + ":0x" + StringTools.hex(HxOverrides.cca(str,i)) + ",  ";
	}
	return retval;
};
com_oddcast_util_Utils.splitWhiteSpace = function(str) {
	var ret = [];
	var searchPos = 0;
	var entry = "";
	while(searchPos < str.length) {
		var $char = str.charAt(searchPos);
		if($char == " " || $char == "\t") {
			if(entry.length > 0) {
				ret.push(entry);
				entry = "";
			}
		} else entry += $char;
		searchPos++;
	}
	if(entry.length > 0) ret.push(entry);
	return ret;
};
com_oddcast_util_Utils.findPowerOfTwo = function(num) {
	var _g = 0;
	while(_g < 32) {
		var i = _g++;
		if(1 << i == num) return i;
	}
	return 0;
};
com_oddcast_util_Utils.findNearestPowerOfTwo = function(num,maximum) {
	haxe_Log.trace("SoleUse",{ fileName : "Utils.hx", lineNumber : 202, className : "com.oddcast.util.Utils", methodName : "findNearestPowerOfTwo"});
	var above = com_oddcast_util_Utils.findPowerOfTwoAbove(num,maximum);
	var below = above >> 1;
	var result;
	if(num + num >= above + below) result = above; else result = below;
	haxe_Log.trace(num + "  " + maximum + "  " + result,{ fileName : "Utils.hx", lineNumber : 207, className : "com.oddcast.util.Utils", methodName : "findNearestPowerOfTwo"});
	return result;
};
com_oddcast_util_Utils.findPowerOfTwoAbove = function(num,maximum) {
	var i = 0;
	var result;
	do result = 1 << i++; while(result < num && result < maximum);
	return result;
};
com_oddcast_util_Utils.isInvalidFloat = function($float) {
	return isNaN($float) || !isFinite($float);
};
com_oddcast_util_Utils.hex0x = function(num,digits) {
	return "0x" + StringTools.hex(num,digits);
};
com_oddcast_util_Utils.formatColor = function(color,digits) {
	if(digits == null) digits = 8;
	var retval = "0x";
	if(digits > 6) retval += StringTools.hex(color >>> 24,2);
	retval += StringTools.hex(color & 16777215,6);
	return retval;
};
com_oddcast_util_Utils.formatInt = function(input,leftPad) {
	if(leftPad == null) leftPad = 0;
	var str;
	if(input == null) str = "null"; else str = "" + input;
	if(leftPad > 0) str = com_oddcast_util_Utils.padL(str,leftPad,"0");
	return str;
};
com_oddcast_util_Utils.formatFloat = function(input,nDec,bPad) {
	if(bPad == null) bPad = true;
	if(nDec == null) nDec = 3;
	return com_oddcast_util_UtilsLite.formatFloat(input,nDec,bPad);
};
com_oddcast_util_Utils.formatString = function(input,nPad) {
	if(nPad == null) nPad = 12;
	var space = "                                                                ";
	return input + HxOverrides.substr(space,0,nPad - input.length);
};
com_oddcast_util_Utils.formatDataSize = function(f) {
	return com_oddcast_util_UtilsLite.formatDataSize(f);
};
com_oddcast_util_Utils.maxMin = function(val,max,min) {
	return Math.max(Math.min(max,val),min);
};
com_oddcast_util_Utils.int2uint = function(input) {
	return input;
};
com_oddcast_util_Utils.ASSERT = function(a,str,posInfo) {
	if(!a) {
		com_oddcast_util_Utils.traceCallStack(null,posInfo);
		throw new js__$Boot_HaxeError(str);
	}
	return new String((a?com_oddcast_util_Utils.ASSERT_TRUE:com_oddcast_util_Utils.ASSERT_FALSE) + ": " + str);
};
com_oddcast_util_Utils.DebugASSERT = function(a,str) {
	if(!a) throw new js__$Boot_HaxeError(str);
	return com_oddcast_util_Utils.ASSERT_TRUE;
};
com_oddcast_util_Utils.ReleaseASSERT = function(a,str,posInfo) {
	if(!a) com_oddcast_util_Utils.releaseTrace(com_oddcast_util_Utils.ASSERT(a,str,{ fileName : "Utils.hx", lineNumber : 382, className : "com.oddcast.util.Utils", methodName : "ReleaseASSERT"}),posInfo);
	return a;
};
com_oddcast_util_Utils.releaseError = function(str,posInfo) {
	com_oddcast_util_Utils.logTrace("[ERROR] " + str,posInfo);
};
com_oddcast_util_Utils.releaseWarning = function(str,posInfo) {
	com_oddcast_util_Utils.logTrace("[WARNING]" + str,posInfo);
};
com_oddcast_util_Utils.releaseTrace = function(str,posInfo) {
	com_oddcast_util_Utils.logTrace("[ENGINE3D] " + str,posInfo);
};
com_oddcast_util_Utils.progressTrace = function(progress,str,posInfo) {
	if(str == null) str = "";
	com_oddcast_util_Utils.logTrace("[PROGRESS] " + com_oddcast_util_Utils.formatFloat(progress,2,true) + "  " + str,posInfo);
};
com_oddcast_util_Utils.buildTrace = function(str,posInfo) {
	com_oddcast_util_Utils.logTrace("[BUILD] " + str,posInfo);
};
com_oddcast_util_Utils.legalTrace = function(str) {
	com_oddcast_util_Utils.logTrace("[LEGAL] " + str,{ fileName : "Utils.hx", lineNumber : 407, className : "com.oddcast.util.Utils", methodName : "legalTrace"});
};
com_oddcast_util_Utils.serverTrace = function(str,posInfo) {
	com_oddcast_util_Utils.logTrace(str,posInfo);
};
com_oddcast_util_Utils.logTrace = function(str,posInfo) {
	if(haxe_Log != null) {
		if(haxe_Log.trace != null) haxe_Log.trace(str,posInfo);
	}
};
com_oddcast_util_Utils.traceMethod = function(string,posInfos) {
	if(string == null) string = "";
};
com_oddcast_util_Utils.AS3trace = function(str) {
	com_oddcast_util_Utils.logTrace(str,{ fileName : "Utils.hx", lineNumber : 452, className : "com.oddcast.util.Utils", methodName : "AS3trace"});
};
com_oddcast_util_Utils.traceLines = function(inStr,posInfo,pre) {
	if(pre == null) pre = "";
	haxe_Log.trace("" + posInfo.lineNumber,{ fileName : "Utils.hx", lineNumber : 457, className : "com.oddcast.util.Utils", methodName : "traceLines"});
	var lines = inStr.split("\n");
	var _g = 0;
	while(_g < lines.length) {
		var line = lines[_g];
		++_g;
		com_oddcast_util_Utils.logTrace(pre + line,posInfo);
	}
};
com_oddcast_util_Utils.traceCallStack = function(desc,posInfo) {
	if(desc == null) desc = "[ALWAYS]";
	var stack = haxe_CallStack.callStack();
	com_oddcast_util_Utils.traceStack(stack,desc,posInfo);
};
com_oddcast_util_Utils.traceExceptionStack = function(e,posInfo) {
	com_oddcast_util_Utils.logTrace(e.toString(),posInfo);
	var stack = haxe_CallStack.exceptionStack();
	com_oddcast_util_Utils.traceStack(stack," exception ",posInfo);
};
com_oddcast_util_Utils.traceStack = function(stack,desc,posInfo) {
	var stackStr;
	if(stack != null) stackStr = haxe_CallStack.toString(stack); else stackStr = "no Stack";
	var stackLines = stackStr.split("Called");
	var _g = 0;
	while(_g < stackLines.length) {
		var l = stackLines[_g];
		++_g;
		com_oddcast_util_Utils.logTrace(desc + " Called" + l,posInfo);
	}
};
com_oddcast_util_Utils.traceStringCharacters = function(str,maxChar,posInfo) {
	var _g1 = 0;
	var _g = com_oddcast_util_Utils.imin(str.length,maxChar);
	while(_g1 < _g) {
		var i = _g1++;
		com_oddcast_util_Utils.logTrace(i + " " + str.charAt(i) + "   0x" + StringTools.hex(HxOverrides.cca(str,i)),posInfo);
	}
};
com_oddcast_util_Utils.stripQuotes = function(str) {
	return HxOverrides.substr(str,1,str.length - 2);
};
var com_oddcast_util_UtilsLite = function() { };
$hxClasses["com.oddcast.util.UtilsLite"] = com_oddcast_util_UtilsLite;
com_oddcast_util_UtilsLite.__name__ = ["com","oddcast","util","UtilsLite"];
com_oddcast_util_UtilsLite.ASSERT = function(b,posInfos) {
	if(!b) throw new js__$Boot_HaxeError(posInfos.fileName + posInfos.lineNumber);
};
com_oddcast_util_UtilsLite.formatFloat = function(input,nDec,bPad) {
	if(bPad == null) bPad = true;
	if(nDec == null) nDec = 3;
	var math = Math;
	var ret = input + "";
	if(input == Infinity || input == -Infinity || isNaN(input)) return ret;
	if(ret.indexOf("e-") != -1) ret = "0.0";
	var point = ret.indexOf(".");
	if(point < ret.length - nDec && point > -1) return HxOverrides.substr(ret,0,point + nDec + 1);
	if(point == -1) {
		ret += ".";
		point = ret.length - 1;
	}
	if(bPad) while(point > ret.length - nDec - 1) ret += "0";
	if(StringTools.endsWith(ret,".")) ret = HxOverrides.substr(ret,0,-1);
	return ret;
};
com_oddcast_util_UtilsLite.lpad = function(s,min,max) {
	if(max == null) max = 0;
	var string = Std.string(s);
	var intString;
	if(max > 0) intString = HxOverrides.substr(string,0,max); else intString = string;
	var outStr = StringTools.lpad(intString," ",min);
	return outStr;
};
com_oddcast_util_UtilsLite.rpad = function(s,min,max) {
	if(max == null) max = 0;
	var string = Std.string(s);
	var intString;
	if(max > 0) intString = HxOverrides.substr(string,0,max); else intString = string;
	var outStr = StringTools.rpad(intString," ",min);
	return outStr;
};
com_oddcast_util_UtilsLite.getFileExtension = function(filename) {
	var lastDot = filename.lastIndexOf(".");
	if(lastDot < 0 || lastDot == filename.length - 1) return "";
	return HxOverrides.substr(filename,lastDot + 1,null);
};
com_oddcast_util_UtilsLite.stripFileExtension = function(filename) {
	var lastDot = filename.lastIndexOf(".");
	if(lastDot < 1 || lastDot < filename.length - 5) return filename;
	var retval = HxOverrides.substr(filename,0,lastDot);
	return retval;
};
com_oddcast_util_UtilsLite.stripFileDirectory = function(filename) {
	if(filename == null) return null;
	var lastSlash = com_oddcast_util_UtilsLite.imax(filename.lastIndexOf("/"),filename.lastIndexOf("\\"));
	if(lastSlash < 0) return filename;
	return HxOverrides.substr(filename,lastSlash + 1,null);
};
com_oddcast_util_UtilsLite.getFileDirectory = function(filename) {
	if(filename == null) return null;
	var lastSlash = com_oddcast_util_UtilsLite.imax(filename.lastIndexOf("/"),filename.lastIndexOf("\\"));
	if(lastSlash < 0) return "";
	return HxOverrides.substr(filename,0,lastSlash);
};
com_oddcast_util_UtilsLite.formatDataSize = function(f,nFig) {
	if(nFig == null) nFig = 3;
	if(f >= 1048576) return com_oddcast_util_UtilsLite.formatFloat(f / 1048576,nFig) + " MB";
	if(f >= 1024) return com_oddcast_util_UtilsLite.formatFloat(f / 1024,nFig) + " KB";
	return com_oddcast_util_UtilsLite.formatFloat(f,0) + " B";
};
com_oddcast_util_UtilsLite.imin = function(a,b) {
	if(a < b) return a; else return b;
};
com_oddcast_util_UtilsLite.imax = function(a,b) {
	if(a > b) return a; else return b;
};
com_oddcast_util_UtilsLite.maxMin = function(val,max,min) {
	return Math.max(Math.min(max,val),min);
};
com_oddcast_util_UtilsLite.hex0x = function(num,digits,pre) {
	if(pre == null) pre = "0x";
	return pre + StringTools.hex(num,digits);
};
com_oddcast_util_UtilsLite.getTime = function() {
	return haxe_Timer.stamp() * 1000.0;
};
com_oddcast_util_UtilsLite.getClassName = function(obj) {
	return Type.getClassName(Type.getClass(obj));
};
com_oddcast_util_UtilsLite.getShortClassName = function(obj) {
	var className = com_oddcast_util_UtilsLite.getClassName(obj);
	var pos = className.lastIndexOf(".") + 1;
	return HxOverrides.substr(className,pos,null);
};
com_oddcast_util_UtilsLite.loadTextFile = function(filename,callbackFunc,errorCallback) {
	var r = new haxe_Http(filename);
	r.onError = function(msg) {
		if(errorCallback != null) errorCallback(filename,msg); else if(errorCallback != null) errorCallback(filename,msg);
	};
	r.onData = function(str) {
		callbackFunc(str);
	};
	r.request(false);
};
var com_oddcast_util_VectorTools = function() { };
$hxClasses["com.oddcast.util.VectorTools"] = com_oddcast_util_VectorTools;
com_oddcast_util_VectorTools.__name__ = ["com","oddcast","util","VectorTools"];
com_oddcast_util_VectorTools.firstElement = function(v) {
	return v[0];
};
com_oddcast_util_VectorTools.lastElement = function(v) {
	return v[v.length - 1];
};
com_oddcast_util_VectorTools.boundsCheck = function(v,i) {
	return i >= 0 && i < v.length;
};
com_oddcast_util_VectorTools.hasElements = function(v) {
	return v != null && v.length > 0;
};
com_oddcast_util_VectorTools.filter = function(v,filterFunc,bRemove) {
	if(bRemove == null) bRemove = false;
	var result = [];
	var copy = v.slice();
	var _g = 0;
	while(_g < copy.length) {
		var t = copy[_g];
		++_g;
		if(filterFunc(t)) {
			result.push(t);
			if(bRemove) HxOverrides.remove(v,t);
		}
	}
	return result;
};
var com_oddcast_util_XmlFastTools = function() { };
$hxClasses["com.oddcast.util.XmlFastTools"] = com_oddcast_util_XmlFastTools;
com_oddcast_util_XmlFastTools.__name__ = ["com","oddcast","util","XmlFastTools"];
com_oddcast_util_XmlFastTools.parseURL = function(url,errorCallback,successCallback) {
	com_oddcast_util_StringTools2.loadAsString(url,errorCallback,function(str) {
		successCallback(new haxe_xml_Fast(Xml.parse(str)));
	});
};
com_oddcast_util_XmlFastTools.splitInnerDataToStringArray = function(node) {
	var innerData = node.get_innerData();
	var innerData1 = StringTools.rtrim(StringTools.ltrim(StringTools.replace(innerData,"\n"," ")));
	return innerData1.split(" ");
};
com_oddcast_util_XmlFastTools.splitInnerDataToFloatArray = function(node) {
	var floatArray = [];
	var stringArray = com_oddcast_util_XmlFastTools.splitInnerDataToStringArray(node);
	var _g = 0;
	while(_g < stringArray.length) {
		var string = stringArray[_g];
		++_g;
		floatArray.push(parseFloat(string));
	}
	return floatArray;
};
com_oddcast_util_XmlFastTools.getElement = function(fast,name,value,att) {
	var $it0 = fast.get_elements();
	while( $it0.hasNext() ) {
		var e = $it0.next();
		if(e.get_name() == name) {
			if(att == null || value == e.x.get(att)) return e;
		}
	}
	return null;
};
com_oddcast_util_XmlFastTools.getElements = function(fast,name,value,att) {
	var retArray = [];
	var $it0 = fast.get_elements();
	while( $it0.hasNext() ) {
		var e = $it0.next();
		if(e.get_name() == name) {
			if(att == null || value == e.x.get(att)) retArray.push(e);
		}
	}
	return retArray;
};
com_oddcast_util_XmlFastTools.getAttributeString = function(fast,attName,defaultValue) {
	var ret = fast.x.get(attName);
	if(ret != null) return ret;
	return defaultValue;
};
com_oddcast_util_XmlFastTools.getAttributeFloat = function(fast,attName,defaultValue) {
	var defaultString = null;
	if(defaultValue != null) if(defaultValue == null) defaultString = "null"; else defaultString = "" + defaultValue;
	var ret = com_oddcast_util_XmlFastTools.getAttributeString(fast,attName,defaultString);
	if(ret != null) return parseFloat(ret);
	return null;
};
com_oddcast_util_XmlFastTools.getAttributeInt = function(fast,attName,defaultValue) {
	var defaultString = null;
	if(defaultValue != null) if(defaultValue == null) defaultString = "null"; else defaultString = "" + defaultValue;
	var ret = com_oddcast_util_XmlFastTools.getAttributeString(fast,attName,defaultString);
	if(ret != null) return Std.parseInt(ret);
	return null;
};
com_oddcast_util_XmlFastTools.setAttribute = function(fast,attName,value) {
	fast.x.set(attName,value);
};
com_oddcast_util_XmlFastTools.setAttributeFloat = function(fast,attName,value,nDecimalPlaces) {
	if(nDecimalPlaces == null) nDecimalPlaces = 6;
	fast.x.set(attName,com_oddcast_util_UtilsLite.formatFloat(value,nDecimalPlaces));
};
com_oddcast_util_XmlFastTools.setAttributeInt = function(fast,attName,value) {
	fast.x.set(attName,value == null?"null":"" + value);
};
var com_oddcast_util_XmlTools = function() { };
$hxClasses["com.oddcast.util.XmlTools"] = com_oddcast_util_XmlTools;
com_oddcast_util_XmlTools.__name__ = ["com","oddcast","util","XmlTools"];
com_oddcast_util_XmlTools.isObfuscated = function(name) {
	if(StringTools.startsWith(name,"_")) {
		if(StringTools.endsWith(name,"_")) {
			throw new js__$Boot_HaxeError("cannot use reflection on obfuscated name:" + name);
			return true;
		}
	}
	return false;
};
com_oddcast_util_XmlTools.createElementWithID = function(elementName,id,value) {
	com_oddcast_util_XmlTools.isObfuscated(elementName);
	var retval = Xml.createElement(elementName);
	if(id != null) retval.set("id",id);
	if(value != null) retval.set("value",value);
	return retval;
};
com_oddcast_util_XmlTools.addCreatedElementWithID = function(parent,elementName,id,value) {
	var xml = com_oddcast_util_XmlTools.createElementWithID(elementName,id,value);
	parent.addChild(xml);
	return xml;
};
com_oddcast_util_XmlTools.attributesIO = function(xml,obj,bWrite,fieldName,defValue) {
	com_oddcast_util_XmlTools.isObfuscated(fieldName);
	if(bWrite) xml.set(fieldName,Reflect.field(obj,fieldName)); else Reflect.setField(obj,fieldName,com_oddcast_util_XmlTools.getString(xml,fieldName,defValue));
};
com_oddcast_util_XmlTools.attributesIOString = function(xml,bWrite,fieldName,value,defValue) {
	com_oddcast_util_XmlTools.isObfuscated(fieldName);
	if(bWrite) {
		xml.set(fieldName,value);
		return value;
	} else return com_oddcast_util_XmlTools.getString(xml,fieldName,defValue);
};
com_oddcast_util_XmlTools.attributesIOFloat = function(xml,bWrite,fieldName,value,precision,defValue) {
	if(defValue == null) defValue = 0.0;
	if(precision == null) precision = 3;
	com_oddcast_util_XmlTools.isObfuscated(fieldName);
	if(bWrite) {
		xml.set(fieldName,com_oddcast_util_UtilsLite.formatFloat(value,precision,false));
		return value;
	} else {
		var floatString = com_oddcast_util_XmlTools.getString(xml,fieldName,defValue == null?"null":"" + defValue);
		return parseFloat(floatString);
	}
};
com_oddcast_util_XmlTools.attributesIOInt = function(xml,bWrite,fieldName,value,defValue) {
	if(defValue == null) defValue = 0;
	com_oddcast_util_XmlTools.isObfuscated(fieldName);
	if(bWrite) {
		xml.set(fieldName,value == null?"null":"" + value);
		return value;
	} else return Std.parseInt(com_oddcast_util_XmlTools.getString(xml,fieldName,defValue == null?"null":"" + defValue));
};
com_oddcast_util_XmlTools.getString = function(xml,att,defvalue) {
	var v = xml.get(att);
	if(v == null) return defvalue; else return v;
};
com_oddcast_util_XmlTools.getElement = function(xml,elementName,value,att,bTrace) {
	if(bTrace == null) bTrace = true;
	if(att == null) att = "id";
	if(xml != null) {
		var iterator = xml.elements();
		if(iterator != null) {
			while( iterator.hasNext() ) {
				var e = iterator.next();
				if((function($this) {
					var $r;
					if(e.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + e.nodeType);
					$r = e.nodeName;
					return $r;
				}(this)) == elementName) {
					if(value == null || e.get(att) == value) return e;
				}
			}
		}
		if(bTrace) {
			var $it0 = xml.elements();
			while( $it0.hasNext() ) {
				var e1 = $it0.next();
			}
		}
	}
	return null;
};
com_oddcast_util_XmlTools.addChildIfNotNull = function(xml,child) {
	if(child != null) xml.addChild(child);
	return child;
};
com_oddcast_util_XmlTools.deepClone = function(xml) {
	return Xml.parse(haxe_xml_Printer.print(xml)).firstElement();
};
com_oddcast_util_XmlTools.makeXmlPretty = function(xml,extra) {
	if(extra == null) extra = "";
	return com_oddcast_util_XmlTools.makeXmlStringPretty(haxe_xml_Printer.print(xml),extra);
};
com_oddcast_util_XmlTools.makeXmlStringPretty = function(inStr,extra) {
	if(extra == null) extra = "";
	var lines = inStr.split("><");
	var tabCount = 0;
	var sb_b = "";
	var bFirstline = true;
	var _g1 = 0;
	var _g = lines.length;
	while(_g1 < _g) {
		var iline = _g1++;
		var line = lines[iline];
		if(StringTools.startsWith(line,"/")) tabCount--;
		if(line.indexOf(">") != -1 && line.indexOf("</") != -1) tabCount--;
		if(StringTools.startsWith(line,"!")) tabCount--; else {
			if(!bFirstline) sb_b += Std.string(com_oddcast_util_XmlTools.NL);
			if(extra == null) sb_b += "null"; else sb_b += "" + extra;
			var _g2 = 0;
			while(_g2 < tabCount) {
				var t = _g2++;
				sb_b += Std.string(com_oddcast_util_XmlTools.TAB);
			}
		}
		if(!StringTools.startsWith(line,"/")) tabCount++;
		if(!bFirstline) sb_b += "<";
		bFirstline = false;
		if(line == null) sb_b += "null"; else sb_b += "" + line;
		if(StringTools.endsWith(line,"/")) tabCount--;
		if(iline < lines.length - 1) sb_b += ">";
	}
	return sb_b;
};
com_oddcast_util_XmlTools.getAttributeString = function(xml,attName,defaultValue) {
	var ret = xml.get(attName);
	if(ret != null) return ret;
	return defaultValue;
};
com_oddcast_util_XmlTools.getAttributeFloat = function(xml,attName,defaultValue) {
	var defaultString = null;
	if(defaultValue != null) if(defaultValue == null) defaultString = "null"; else defaultString = "" + defaultValue;
	var ret = com_oddcast_util_XmlTools.getAttributeString(xml,attName,defaultString);
	if(ret != null) return parseFloat(ret);
	return null;
};
com_oddcast_util_XmlTools.getAttributeInt = function(xml,attName,defaultValue) {
	var defaultString = null;
	if(defaultValue != null) if(defaultValue == null) defaultString = "null"; else defaultString = "" + defaultValue;
	var ret = com_oddcast_util_XmlTools.getAttributeString(xml,attName,defaultString);
	if(ret != null) return Std.parseInt(ret);
	return null;
};
com_oddcast_util_XmlTools.getAttributeBool = function(xml,attName,defaultValue) {
	var ret = xml.get(attName);
	if(ret == null) return defaultValue;
	return ret.toLowerCase() == "true";
};
com_oddcast_util_XmlTools.setAttribute = function(xml,attName,value) {
	xml.set(attName,value);
};
com_oddcast_util_XmlTools.setAttributeFloat = function(xml,attName,value,nDec) {
	if(nDec == null) nDec = 15;
	xml.set(attName,com_oddcast_util_UtilsLite.formatFloat(value,nDec));
};
com_oddcast_util_XmlTools.setAttributeInt = function(xml,attName,value) {
	xml.set(attName,value == null?"null":"" + value);
};
var com_oddcast_util_js_ConsoleTracing = function() { };
$hxClasses["com.oddcast.util.js.ConsoleTracing"] = com_oddcast_util_js_ConsoleTracing;
com_oddcast_util_js_ConsoleTracing.__name__ = ["com","oddcast","util","js","ConsoleTracing"];
com_oddcast_util_js_ConsoleTracing.setRedirection = function() {
	haxe_Log.trace("setRedirection()",{ fileName : "ConsoleTracing.hx", lineNumber : 20, className : "com.oddcast.util.js.ConsoleTracing", methodName : "setRedirection"});
	com_oddcast_util_js_ConsoleTracing.origTrace = haxe_Log.trace;
	haxe_Log.trace = com_oddcast_util_js_ConsoleTracing.myTrace;
};
com_oddcast_util_js_ConsoleTracing.disableFilters = function() {
	com_oddcast_util_js_ConsoleTracing.bDisableFilters = true;
};
com_oddcast_util_js_ConsoleTracing.addClassToFilter = function(bWhitelist,inf) {
	if(bWhitelist == null) bWhitelist = true;
	if(bWhitelist) {
		if(com_oddcast_util_js_ConsoleTracing.whiteList == null) com_oddcast_util_js_ConsoleTracing.whiteList = new haxe_ds_StringMap();
		if(!com_oddcast_util_js_ConsoleTracing.whiteList.exists(inf.fileName)) com_oddcast_util_js_ConsoleTracing.whiteList.set(inf.fileName,true);
	} else {
		if(com_oddcast_util_js_ConsoleTracing.blackList == null) com_oddcast_util_js_ConsoleTracing.blackList = new haxe_ds_StringMap();
		if(!com_oddcast_util_js_ConsoleTracing.blackList.exists(inf.fileName)) com_oddcast_util_js_ConsoleTracing.blackList.set(inf.fileName,true);
	}
};
com_oddcast_util_js_ConsoleTracing.myTrace = function(v,inf) {
	if(com_oddcast_util_js_ConsoleTracing.filter(inf)) {
		var arg = v;
		console.log(arg);
	}
};
com_oddcast_util_js_ConsoleTracing.filter = function(inf) {
	if(com_oddcast_util_js_ConsoleTracing.bDisableFilters || com_oddcast_util_js_ConsoleTracing.blackList == null || !com_oddcast_util_js_ConsoleTracing.blackList.exists(inf.fileName)) {
		if(com_oddcast_util_js_ConsoleTracing.bDisableFilters || com_oddcast_util_js_ConsoleTracing.whiteList == null || com_oddcast_util_js_ConsoleTracing.whiteList.exists(inf.fileName)) return true;
	}
	return false;
};
var com_oddcast_util_js_AutoListener = function() {
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(false,{ fileName : "RegisteredListener.hx", lineNumber : 48, className : "com.oddcast.util.js.AutoListener", methodName : "new"});
	this.registeredListeners = new haxe_ds_StringMap();
};
$hxClasses["com.oddcast.util.js.AutoListener"] = com_oddcast_util_js_AutoListener;
com_oddcast_util_js_AutoListener.__name__ = ["com","oddcast","util","js","AutoListener"];
com_oddcast_util_js_AutoListener.prototype = {
	addListener: function(target,eventString,eventListener) {
		var keyString;
		var sub = 0;
		do {
			keyString = eventString + "_" + sub;
			sub++;
		} while(this.registeredListeners.get(keyString) != null);
		var value = new com_oddcast_util_js_RegisteredListener(target,eventString,eventListener);
		this.registeredListeners.set(keyString,value);
		return keyString;
	}
	,removeListener: function(keyString) {
		var value = com_oddcast_util_Disposable.disposeIfValid(this.registeredListeners.get(keyString));
		this.registeredListeners.set(keyString,value);
		return null;
	}
	,dispose: function() {
		this.registeredListeners = com_oddcast_util_Disposable.disposeIterableIfValid(this.registeredListeners);
	}
	,__class__: com_oddcast_util_js_AutoListener
};
var com_oddcast_util_js_JSAudio = function() {
	com_oddcast_util_js_AutoListener.call(this);
	com_oddcast_util_js_ConsoleTracing.addClassToFilter(true,{ fileName : "JSAudio.hx", lineNumber : 45, className : "com.oddcast.util.js.JSAudio", methodName : "new"});
	haxe_Log.trace("JSAudio.new()",{ fileName : "JSAudio.hx", lineNumber : 46, className : "com.oddcast.util.js.JSAudio", methodName : "new"});
	if(com_oddcast_util_js_JSAudio.audio == null) {
		haxe_Log.trace("JSAudio createElement",{ fileName : "JSAudio.hx", lineNumber : 50, className : "com.oddcast.util.js.JSAudio", methodName : "new"});
		com_oddcast_util_js_JSAudio.audio = window.document.createElement("audio");
		try {
			com_oddcast_util_js_JSAudio.audio.load();
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			com_oddcast_util_js_JSAudio.audio = null;
		}
	}
};
$hxClasses["com.oddcast.util.js.JSAudio"] = com_oddcast_util_js_JSAudio;
com_oddcast_util_js_JSAudio.__name__ = ["com","oddcast","util","js","JSAudio"];
com_oddcast_util_js_JSAudio.__interfaces__ = [com_oddcast_util_IDisposable,com_oddcast_util_js_IAudioLite];
com_oddcast_util_js_JSAudio.__super__ = com_oddcast_util_js_AutoListener;
com_oddcast_util_js_JSAudio.prototype = $extend(com_oddcast_util_js_AutoListener.prototype,{
	handleEvent: function(event) {
	}
	,playAudio: function(url,offsetMillis,volume,autoStart,id3String,fileErrorCallback,audioLoadedCallback,audioStartedCallback,audioFinishedCallback) {
		var _g = this;
		if(window.navigator.appName == "Microsoft Internet Explorer") {
			if(url.indexOf("tts/gen.php") >= 0) {
				haxe_Log.trace("adding Random to audio URL",{ fileName : "JSAudio.hx", lineNumber : 77, className : "com.oddcast.util.js.JSAudio", methodName : "playAudio"});
				url = url + "&Random=" + Math.round(Math.random() * 1000000);
			}
		}
		com_oddcast_util_js_JSAudio.audio.id = new String(com_oddcast_util_UtilsLite.stripFileDirectory(url));
		this.id = new String(com_oddcast_util_UtilsLite.stripFileDirectory(url));
		haxe_Log.trace(this.id + " JSAudio.playAudio",{ fileName : "JSAudio.hx", lineNumber : 85, className : "com.oddcast.util.js.JSAudio", methodName : "playAudio"});
		this.setVolume(volume);
		this.id3Comment = null;
		this.chromeHackID3durationMillis = -1.0;
		this.bUseChromeHack = false;
		this.origOffsetMillis = offsetMillis;
		this.audioStartedCallback = audioStartedCallback;
		this.audioFinishedCallback = audioFinishedCallback;
		this.lastProgressTime = -1;
		this.addListener(com_oddcast_util_js_JSAudio.audio,"progress",function(dyn) {
			if(com_oddcast_util_js_JSAudio.audio.currentTime < _g.lastProgressTime) _g.audioEnded(null);
			_g.lastProgressTime = com_oddcast_util_js_JSAudio.audio.currentTime;
		});
		com_oddcast_util_js_JSAudio.audio.onerror = function(event) {
			var errorString = "#" + event.type;
			var _g1 = event.type;
			switch(_g1) {
			case "error":
				var ct = event.currentTarget;
				var _g11 = ct.error.code;
				switch(_g11) {
				case 1:
					errorString = "MEDIA_ERR_ABORTED";
					break;
				case 2:
					errorString = "MEDIA_ERR_NETWORK";
					break;
				case 3:
					errorString = "MEDIA_ERR_DECODE";
					break;
				case 4:
					errorString = "MEDIA_ERR_SRC_NOT_SUPPORTED";
					break;
				default:
					errorString = errorString;
				}
				break;
			}
			haxe_Log.trace("JSAudio.event onerror " + errorString + " " + url,{ fileName : "JSAudio.hx", lineNumber : 167, className : "com.oddcast.util.js.JSAudio", methodName : "playAudio"});
			_g.dispose();
			fileErrorCallback(url,errorString);
		};
		this.addListener(com_oddcast_util_js_JSAudio.audio,"ended",$bind(this,this.audioEnded));
		this.loadID3(url,id3String,function() {
			haxe_Log.trace("id3Comment2:" + _g.id3Comment,{ fileName : "JSAudio.hx", lineNumber : 177, className : "com.oddcast.util.js.JSAudio", methodName : "playAudio"});
			haxe_Log.trace("audio.src = " + url,{ fileName : "JSAudio.hx", lineNumber : 178, className : "com.oddcast.util.js.JSAudio", methodName : "playAudio"});
			com_oddcast_util_js_JSAudio.audio.src = url;
			_g.readyToPlay(url,audioLoadedCallback,offsetMillis,autoStart);
		},fileErrorCallback);
		return this;
	}
	,handler: function(e) {
	}
	,audioEnded: function(anything) {
		haxe_Log.trace(this.id + "JSAudio.audioEnded " + Std.string(anything),{ fileName : "JSAudio.hx", lineNumber : 196, className : "com.oddcast.util.js.JSAudio", methodName : "audioEnded"});
		this.bIsPlaying = false;
		if(this.audioFinishedCallback != null) this.audioFinishedCallback(com_oddcast_util_js_JSAudio.audio.src,com_oddcast_util_js_JSAudio.audio.currentTime);
		this.audioFinishedCallback = null;
	}
	,loadID3: function(url,id3String,id3LoadFunc,fileErrorCallback) {
		var _g = this;
		haxe_Log.trace(this.id + "JSAudio.loadID3",{ fileName : "JSAudio.hx", lineNumber : 205, className : "com.oddcast.util.js.JSAudio", methodName : "loadID3"});
		new com_oddcast_util_ID3().load(url,function(id3) {
			_g.id3Comment = id3.getTag("COMM");
			id3.dispose();
			id3LoadFunc();
		},function(id31,url1,msg) {
			_g.dispose();
			if(fileErrorCallback != null) fileErrorCallback(url1,msg);
		},id3String);
	}
	,readyToPlay: function(url,audioLoadedCallback,offsetMillis,autoStart) {
		haxe_Log.trace(this.id + "JSAudio.readyToPlay " + offsetMillis + ", " + (autoStart == null?"null":"" + autoStart) + ")",{ fileName : "JSAudio.hx", lineNumber : 246, className : "com.oddcast.util.js.JSAudio", methodName : "readyToPlay"});
		this.checkChromeHack();
		if(audioLoadedCallback != null) {
			haxe_Log.trace("audioLoadedCallback " + url,{ fileName : "JSAudio.hx", lineNumber : 249, className : "com.oddcast.util.js.JSAudio", methodName : "readyToPlay"});
			audioLoadedCallback(url);
		}
		if(autoStart) this.playAt(offsetMillis);
	}
	,playAt: function(offsetMillis) {
		if(offsetMillis == null) offsetMillis = 0.0;
		this.chromeHackAudioCurrentTimeMillis = 0;
		this.play();
		var offsetSecs = offsetMillis * 0.001;
		haxe_Log.trace(this.id + "JSAudio.playAt(" + offsetMillis + ")",{ fileName : "JSAudio.hx", lineNumber : 263, className : "com.oddcast.util.js.JSAudio", methodName : "playAt"});
		try {
			this.lastProgressTime = offsetSecs;
			if(offsetSecs > 0.0) {
				this.chromeHackAudioCurrentTimeMillis = offsetMillis;
				com_oddcast_util_js_JSAudio.audio.currentTime = offsetSecs;
				haxe_Log.trace("sayURL(url, offset): offset not yet working on iOS",{ fileName : "JSAudio.hx", lineNumber : 269, className : "com.oddcast.util.js.JSAudio", methodName : "playAt"});
			}
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			haxe_Log.trace("threw on audio.currentTime(" + offsetSecs + ")",{ fileName : "JSAudio.hx", lineNumber : 272, className : "com.oddcast.util.js.JSAudio", methodName : "playAt"});
		}
	}
	,play: function() {
		haxe_Log.trace(this.id + "JSAudio.play()",{ fileName : "JSAudio.hx", lineNumber : 277, className : "com.oddcast.util.js.JSAudio", methodName : "play"});
		if(this.audioStartedCallback != null && this.id3Comment != null) {
			this.chromeHackID3durationMillis = 1000 * this.audioStartedCallback(this.id3Comment,com_oddcast_util_js_JSAudio.audio.src,com_oddcast_util_js_JSAudio.audio.currentTime);
			haxe_Log.trace("chromeHackID3durationMillis:" + this.chromeHackID3durationMillis,{ fileName : "JSAudio.hx", lineNumber : 280, className : "com.oddcast.util.js.JSAudio", methodName : "play"});
		}
		this.audioStartedCallback = null;
		com_oddcast_util_js_JSAudio.audio.play();
		this.chromeHackLastTimeMillis = com_oddcast_util_UtilsLite.getTime();
		this.bIsPlaying = true;
	}
	,setToTime: function(millis) {
		haxe_Log.trace(this.id + "JSAudio.setToTime(" + millis + ")",{ fileName : "JSAudio.hx", lineNumber : 291, className : "com.oddcast.util.js.JSAudio", methodName : "setToTime"});
		if(this.isPlaying()) this.playAt(millis); else {
		}
	}
	,positionMillis: function() {
		if(this.bUseChromeHack) {
			if(this.bIsPlaying) {
				var time = com_oddcast_util_UtilsLite.getTime();
				this.chromeHackAudioCurrentTimeMillis += time - this.chromeHackLastTimeMillis;
				this.chromeHackLastTimeMillis = time;
			}
			var overrunHack = 0.0;
			if(com_oddcast_util_js_JSAudio.audio.currentTime > 0) this.chromeHackAudioCurrentTimeMillis = com_oddcast_util_js_JSAudio.audio.currentTime * 1000.0; else overrunHack = com_oddcast_util_js_JSAudio.CHROME_HACK_OVERRUN;
			if(this.chromeHackID3durationMillis > 0 && this.chromeHackAudioCurrentTimeMillis > this.chromeHackID3durationMillis + overrunHack) this.audioEnded(null);
			return this.chromeHackAudioCurrentTimeMillis;
		}
		return com_oddcast_util_js_JSAudio.audio.currentTime * 1000.0;
	}
	,isPlaying: function() {
		return this.bIsPlaying;
	}
	,durationMillis: function() {
		if(com_oddcast_util_js_JSAudio.audio != null) {
			if(this.bUseChromeHack) {
				haxe_Log.trace("durationMillis()" + Std.string($bind(this,this.durationMillis)),{ fileName : "JSAudio.hx", lineNumber : 331, className : "com.oddcast.util.js.JSAudio", methodName : "durationMillis"});
				return this.chromeHackID3durationMillis;
			}
			return com_oddcast_util_js_JSAudio.audio.duration * 1000.0;
		}
		return 0.0;
	}
	,playingProgress: function() {
		var ret = Math.max(0,Math.min(1.0,(this.positionMillis() - this.origOffsetMillis) / (this.durationMillis() - this.origOffsetMillis)));
		if(isNaN(ret)) {
			var junk = 1.0;
		}
		return ret;
	}
	,setVolume: function(v) {
		if(com_oddcast_util_js_JSAudio.audio != null) {
			com_oddcast_util_js_JSAudio.audio.volume = v;
			haxe_Log.trace("Volume:" + v,{ fileName : "JSAudio.hx", lineNumber : 352, className : "com.oddcast.util.js.JSAudio", methodName : "setVolume"});
		}
	}
	,pauseResume: function(pause) {
		haxe_Log.trace(this.id + "JSAudio.pauseResume(pause:" + (pause == null?"null":"" + pause) + ")",{ fileName : "JSAudio.hx", lineNumber : 357, className : "com.oddcast.util.js.JSAudio", methodName : "pauseResume"});
		if(pause) {
			if(this.isPlaying()) {
				com_oddcast_util_js_JSAudio.audio.pause();
				this.bIsPlaying = false;
			}
			return;
		}
		if(!this.isPlaying()) this.play();
	}
	,replay: function(offsetMillis) {
		if(offsetMillis == null) offsetMillis = 0.0;
		haxe_Log.trace(this.id + "JSAudio.replay()",{ fileName : "JSAudio.hx", lineNumber : 374, className : "com.oddcast.util.js.JSAudio", methodName : "replay"});
		this.playAt(offsetMillis);
	}
	,dispose: function() {
		if(com_oddcast_util_js_JSAudio.audio != null) {
			haxe_Log.trace(this.id + "JSAudio.dispose",{ fileName : "JSAudio.hx", lineNumber : 380, className : "com.oddcast.util.js.JSAudio", methodName : "dispose"});
			com_oddcast_util_js_JSAudio.audio.pause();
			this.audioStartedCallback = null;
			this.audioFinishedCallback = null;
			this.id3Comment = null;
			com_oddcast_util_js_AutoListener.prototype.dispose.call(this);
		}
	}
	,checkChromeHack: function() {
		this.bUseChromeHack = false;
		if(com_oddcast_util_js_JSAudio.audio.buffered.length == 1) {
			if(com_oddcast_util_js_JSAudio.audio.buffered.start(0) == 0 && com_oddcast_util_js_JSAudio.audio.buffered.end(0) == 0) this.bUseChromeHack = true;
		}
		haxe_Log.trace("bUseChromeHack:" + Std.string(this.bUseChromeHack),{ fileName : "JSAudio.hx", lineNumber : 412, className : "com.oddcast.util.js.JSAudio", methodName : "checkChromeHack"});
	}
	,debug_TimeRangeToString: function(tr,desc) {
		var s = "TimeRange " + desc;
		var _g1 = 0;
		var _g = tr.length;
		while(_g1 < _g) {
			var t = _g1++;
			s += " t:" + t + "[" + tr.start(t) + "," + tr.end(t) + "]";
		}
		return s;
	}
	,__class__: com_oddcast_util_js_JSAudio
});
var com_oddcast_util_js_IJSdrawable = function() { };
$hxClasses["com.oddcast.util.js.IJSdrawable"] = com_oddcast_util_js_IJSdrawable;
com_oddcast_util_js_IJSdrawable.__name__ = ["com","oddcast","util","js","IJSdrawable"];
com_oddcast_util_js_IJSdrawable.prototype = {
	__class__: com_oddcast_util_js_IJSdrawable
};
var com_oddcast_util_js_JSCanvas = function(id,w,h,trans,inCanvas,useMatrixStack) {
	if(useMatrixStack == null) useMatrixStack = false;
	if(trans == null) trans = true;
	if(h == null) h = 0;
	if(w == null) w = 0;
	if(inCanvas != null) {
		this.canvas = inCanvas;
		this.bExternalSuppliedCanvas = true;
	} else {
		this.canvas = com_oddcast_util_js_elements_JSCanvasElement.create();
		this.canvas.id = new String(id);
		if(w != 0 && h != 0) {
			this.canvas.width = w;
			this.canvas.height = h;
		}
		this.bExternalSuppliedCanvas = false;
	}
	if(useMatrixStack) this.matrixStack = new com_oddcast_util_js_MatrixStack();
	this.DEBUGcanvasCount(1);
};
$hxClasses["com.oddcast.util.js.JSCanvas"] = com_oddcast_util_js_JSCanvas;
com_oddcast_util_js_JSCanvas.__name__ = ["com","oddcast","util","js","JSCanvas"];
com_oddcast_util_js_JSCanvas.__interfaces__ = [com_oddcast_util_js_IJSdrawable,com_oddcast_util_IDisposable];
com_oddcast_util_js_JSCanvas.newFromRect = function(drawable,id,x,y,width,height) {
	var useWidth = width;
	var useHeight = height;
	var jscanvas = new com_oddcast_util_js_JSCanvas(id,useWidth,useHeight);
	if(drawable != null) jscanvas.drawFloatsXsYsWsHdXdY(drawable,x,y,width,height,0,0,width,height);
	return jscanvas;
};
com_oddcast_util_js_JSCanvas.getTextureAsset = function(color,bDelete) {
	var canvas = color.newCanvasFromImage(color.id() + "_AsCanvas");
	if(bDelete) color.dispose();
	return canvas;
};
com_oddcast_util_js_JSCanvas.prototype = {
	canvasEquelTo: function(canvasElement) {
		return this.canvas == canvasElement;
	}
	,drawImageIntXY: function(image,x,y) {
		var context2D = this.getContext2D();
		context2D.drawImage(image.image,x,y);
		if(this.hasAlpha(image,context2D)) {
			context2D.drawImage(image.mask != null?image.mask.image:null,x,y);
			this.getContext2D().globalCompositeOperation = "source-over";
		}
	}
	,drawCanvasIntXY: function(canvas,x,y) {
		var context2D = this.getContext2D();
		context2D.drawImage(canvas.canvas,x,y);
	}
	,getContext2D: function() {
		return this.canvas.getContext("2d");
	}
	,renderDeferredImages: function() {
		if(this.deferredImages != null) {
			var context2D = this.getContext2D();
			var $it0 = this.deferredImages.iterator();
			while( $it0.hasNext() ) {
				var deferredImageArray = $it0.next();
				var _g = 0;
				while(_g < deferredImageArray.length) {
					var deferredImage = deferredImageArray[_g];
					++_g;
					context2D.drawImage(deferredImage.image.image,deferredImage.sx,deferredImage.sy,deferredImage.swidth,deferredImage.sheight,deferredImage.dx,deferredImage.dy,deferredImage.swidth,deferredImage.sheight);
				}
			}
			context2D.globalCompositeOperation = "xor";
			var $it1 = this.deferredImages.iterator();
			while( $it1.hasNext() ) {
				var deferredImageArray1 = $it1.next();
				var nativeMask = deferredImageArray1[0].image.getMaskNative();
				if(nativeMask != null) {
					var _g1 = 0;
					while(_g1 < deferredImageArray1.length) {
						var deferredImage1 = deferredImageArray1[_g1];
						++_g1;
						context2D.drawImage(nativeMask,deferredImage1.sx,deferredImage1.sy,deferredImage1.swidth,deferredImage1.sheight,deferredImage1.dx,deferredImage1.dy,deferredImage1.swidth,deferredImage1.sheight);
					}
				}
			}
			this.getContext2D().globalCompositeOperation = "source-over";
			this.deferredImages = null;
		}
	}
	,drawImageIntXYWH_XY_Deferred: function(image,sx,sy,swidth,sheight,dx,dy) {
		if(this.deferredImages == null) this.deferredImages = new haxe_ds_ObjectMap();
		var deferredImageArray = this.deferredImages.h[image.__id__];
		if(deferredImageArray == null) {
			deferredImageArray = [];
			this.deferredImages.set(image,deferredImageArray);
		}
		deferredImageArray.push({ image : image, sx : sx, sy : sy, swidth : swidth, sheight : sheight, dx : dx, dy : dy});
	}
	,drawImageIntXYWH_XY: function(image,sx,sy,swidth,sheight,dx,dy) {
		var context2D = this.getContext2D();
		context2D.drawImage(image.image,sx,sy,swidth,sheight,dx,dy,swidth,sheight);
		if(this.hasAlpha(image,context2D)) {
			context2D.drawImage(image.mask != null?image.mask.image:null,sx,sy,swidth,sheight,dx,dy,swidth,sheight);
			this.getContext2D().globalCompositeOperation = "source-over";
		}
	}
	,drawCanvasFloatXY_Rectangle: function(canvas2,x,y,sourceRect,scale) {
		if(sourceRect == null) this.drawCanvasFloatsXsYsWsHdXdY(canvas2,0,0,canvas2.get_width(),canvas2.get_height(),x,y,canvas2.get_width() * scale,canvas2.get_height() * scale); else this.drawCanvasFloatsXsYsWsHdXdY(canvas2,sourceRect.x,sourceRect.y,sourceRect.width,sourceRect.height,x,y,sourceRect.width * scale,sourceRect.height * scale);
	}
	,drawImageFloatXY_Rectangle: function(image2,x,y,sourceRect,scale) {
		if(sourceRect == null) this.drawImageFloatsXsYsWsHdXdY(image2,0,0,image2.get_width(),image2.get_height(),x,y,image2.get_width() * scale,image2.get_height() * scale); else this.drawImageFloatsXsYsWsHdXdY(image2,sourceRect.x,sourceRect.y,sourceRect.width,sourceRect.height,x,y,sourceRect.width * scale,sourceRect.height * scale);
	}
	,drawFloatXY_Rectangle: function(drawable,x,y,sourceRect,scale) {
		if(sourceRect == null) this.drawFloatsXsYsWsHdXdY(drawable,0,0,drawable.get_width(),drawable.get_height(),x,y,drawable.get_width() * scale,drawable.get_height() * scale); else this.drawFloatsXsYsWsHdXdY(drawable,sourceRect.x,sourceRect.y,sourceRect.width,sourceRect.height,x,y,sourceRect.width * scale,sourceRect.height * scale);
	}
	,drawFloatsXsYsWsHdXdY: function(drawable,sx,sy,swidth,sheight,dx,dy,dwidth,dheight) {
		var context2D = this.getContext2D();
		context2D.drawImage(drawable.getNative(),sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
		if(this.hasAlpha(drawable,context2D)) {
			context2D.drawImage(drawable.getMaskNative(),sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
			this.getContext2D().globalCompositeOperation = "source-over";
		}
	}
	,getBounds: function() {
		return new com_oddcast_neko_geom_Rectangle(0,0,this.get_width(),this.get_height());
	}
	,drawImageIntXYWH: function(image,x,y,width,height) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(image.image,x,y,width,height);
	}
	,drawCanvasIntXYWH: function(canvas2,x,y,width,height) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(canvas2.canvas,x,y,width,height);
	}
	,drawImageIntsXsYsWsHdXdYdWdH: function(image,sx,sy,swidth,sheight,dx,dy,dwidth,dheight) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(image.image,sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
	}
	,drawCanvasFloatsXsYsWsHdXdY: function(canvas2,sx,sy,swidth,sheight,dx,dy,dwidth,dheight) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(canvas2.canvas,sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
	}
	,drawImageFloatsXsYsWsHdXdY: function(image,sx,sy,swidth,sheight,dx,dy,dwidth,dheight) {
		var context2D = this.getContext2D();
		context2D.drawImage(image.image,sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
		if(this.hasAlpha(image,context2D)) {
			context2D.drawImage(image.mask != null?image.mask.image:null,sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
			this.getContext2D().globalCompositeOperation = "source-over";
		}
	}
	,drawCanvasFloat: function(canvas2,x,y) {
		this.drawCanvasFloatXY(canvas2,x,y);
	}
	,drawCanvasFloatXY: function(canvas2,x,y) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(canvas2.canvas,x,y);
	}
	,drawCanvasIntsXsYsWsHdXdYdWdH: function(canvas2,sx,sy,swidth,sheight,dx,dy,dwidth,dheight) {
		var context2D = this.canvas.getContext("2d");
		context2D.drawImage(canvas2.canvas,sx,sy,swidth,sheight,dx,dy,dwidth,dheight);
	}
	,drawCanvasIntXYWH_XY: function(canvas2,sx,sy,swidth,sheight,dx,dy) {
		this.canvas.getContext("2d").drawImage(canvas2.canvas,sx,sy,swidth,sheight,dx,dy,swidth,sheight);
	}
	,newCanvasFromCanvasRect: function(id,x,y,width,height) {
		var jscanvas = new com_oddcast_util_js_JSCanvas(id,width,height);
		jscanvas.drawCanvasIntsXsYsWsHdXdYdWdH(this,x,y,width,height,0,0,width,height);
		return jscanvas;
	}
	,clearRectInt: function(x,y,width,height) {
		this.canvas.getContext("2d").clearRect(x,y,width,height);
	}
	,pushPosScaleRot: function(x,y,xScale,yScale,rotRad) {
		if(rotRad == null) rotRad = 0.0;
		if(yScale == null) yScale = 1.0;
		if(xScale == null) xScale = 1.0;
		var c2d = this.canvas.getContext("2d");
		if(this.matrixStack != null) {
			var mat = new com_oddcast_util_js_geom_Matrix();
			mat.scale(xScale,yScale);
			mat.rotate(rotRad);
			mat.translate(x,y);
			var t = this.matrixStack.pushMatrix(mat);
			c2d.setTransform(t.a,t.b,t.c,t.d,t.tx,t.ty);
		} else {
			c2d.save();
			c2d.translate(x,y);
			c2d.scale(xScale,yScale);
			c2d.rotate(rotRad);
		}
	}
	,pushMatrix: function(matrix23) {
		var c2d = this.canvas.getContext("2d");
		if(this.matrixStack != null) {
			var t = this.matrixStack.pushMatrix(matrix23);
			c2d.setTransform(t.a,t.b,t.c,t.d,t.tx,t.ty);
			return t;
		} else {
			c2d.save();
			c2d.transform(matrix23.a,matrix23.b,matrix23.c,matrix23.d,matrix23.tx,matrix23.ty);
			return null;
		}
	}
	,debugRectLine: function(rect,color,lineWidth) {
		if(lineWidth == null) lineWidth = 1.0;
		if(color == null) color = -16777216;
		var ctx = this.getContext2D();
		ctx.beginPath();
		ctx.lineWidth = lineWidth;
		ctx.strokeStyle = com_oddcast_util_UtilsLite.hex0x(color,6,"#");
		ctx.rect(rect.x,rect.y,rect.width,rect.height);
		ctx.stroke();
	}
	,resetMatrix: function() {
		if(this.matrixStack != null) this.matrixStack.resetMatrix();
		this.canvas.getContext("2d").setTransform(1,0,0,1,0,0);
		this.deferredImages = null;
	}
	,popMatrix: function() {
		if(this.matrixStack != null) {
			this.matrixStack.popMatrix();
			var t = this.matrixStack.currMatrix;
			this.canvas.getContext("2d").setTransform(t.a,t.b,t.c,t.d,t.tx,t.ty);
		} else this.canvas.getContext("2d").restore();
	}
	,clear: function() {
		this.clearRectInt(0,0,this.canvas.width,this.canvas.height);
	}
	,debugRect: function(rect,color) {
		if(color == null) color = -16777216;
		var ctx = this.getContext2D();
		if(rect == null) rect = new com_oddcast_host_engine3d_math_Rectangle().set(0,0,this.get_width(),this.get_height());
		ctx.beginPath();
		ctx.rect(rect.left,rect.top,rect.width(),rect.height());
		ctx.fillStyle = "red";
		ctx.fill();
	}
	,_fillRect: function(x,y,width,height) {
		this.canvas.getContext("2d").fillRect(x,y,width,height);
	}
	,getPixel: function(x,y,defRet) {
		try {
			var id = this.getContext2D().getImageData(x,y,1,1);
			return (id.data[3] & 255) << 24 | (id.data[0] & 255) << 16 | (id.data[1] & 255) << 8 | id.data[2] & 255;
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return defRet;
		}
	}
	,setColorTransform: function(ct,rect) {
		var ctx = this.canvas.getContext("2d");
		if(rect == null) rect = new com_oddcast_neko_geom_Rectangle(0,0,this.get_width(),this.get_height());
		var x = Math.round(rect.x);
		var y = Math.round(rect.y);
		var w = Math.round(rect.width);
		var h = Math.round(rect.height);
		var redOffset = Math.round(ct.redOffset);
		var greenOffset = Math.round(ct.greenOffset);
		var blueOffset = Math.round(ct.blueOffset);
		var alphaMult = Math.round(ct.alphaMultiplier * 256);
		try {
			var imgData = ctx.getImageData(x,y,w,h);
			var data = imgData.data;
			if(Math.abs(1.0 - ct.redMultiplier) < 0.01 && Math.abs(1.0 - ct.blueMultiplier) < 0.01 && Math.abs(1.0 - ct.greenMultiplier) < 0.01) {
				var _g1 = 0;
				var _g = imgData.width * imgData.height;
				while(_g1 < _g) {
					var p = _g1++;
					var i = p << 2;
					data[i] += redOffset;
					data[i + 1] += greenOffset;
					data[i + 2] += blueOffset;
					if(alphaMult != 256) data[i + 3] = data[i + 3] * alphaMult >> 8;
				}
			} else {
				throw new js__$Boot_HaxeError(" to be optimized");
				var _g11 = 0;
				var _g2 = imgData.width * imgData.height;
				while(_g11 < _g2) {
					var p1 = _g11++;
					var i1 = p1 << 2;
					var red = data[i1];
					red = Math.round(red * ct.redMultiplier + ct.redOffset);
					data[i1] = red;
					var gre = data[i1 + 1];
					gre = Math.round(gre * ct.greenMultiplier + ct.greenOffset);
					data[i1 + 1] = gre;
					var blu = data[i1 + 2];
					blu = Math.round(blu * ct.blueMultiplier + ct.blueOffset);
					data[i1 + 2] = blu;
					var alp = data[i1 + 3];
					alp = Math.round(alp * ct.alphaMultiplier + ct.alphaOffset);
					data[i1 + 3] = alp;
				}
			}
			ctx.putImageData(imgData,Math.round(rect.x),Math.round(rect.y),0,0,Math.round(rect.width),Math.round(rect.height));
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			com_oddcast_util_Utils.releaseError("Coloring Not available ( x domain ?) " + this.canvas.id,{ fileName : "JSCanvas.hx", lineNumber : 679, className : "com.oddcast.util.js.JSCanvas", methodName : "setColorTransform"});
		}
	}
	,getNative: function() {
		return this.canvas;
	}
	,getBoundingClientRect: function() {
		return this.canvas.getBoundingClientRect();
	}
	,get_width: function() {
		return this.canvas.width;
	}
	,get_height: function() {
		return this.canvas.height;
	}
	,setX: function(x) {
	}
	,setY: function(y) {
	}
	,getAssetSize: function() {
		return this.get_width() * this.get_height() * 4;
	}
	,getDownloadSize: function() {
		return 0;
	}
	,DEBUG_String: function() {
		return "pixSize : " + this.get_width() + "*" + this.get_height() + "=" + this.get_width() * this.get_height() + " name:" + this.canvas.id;
	}
	,DEBUGcanvasCount: function(inc) {
		com_oddcast_util_js_JSCanvas.DEBUGCanvasCount += inc;
		var s;
		if(inc > 0) s = " new "; else s = "dispose";
	}
	,hasMask: function() {
		return false;
	}
	,getMaskNative: function() {
		return null;
	}
	,dispose: function() {
		if(this.canvas != null) {
			this.DEBUGcanvasCount(-1);
			if(!this.bExternalSuppliedCanvas) {
				this.canvas.width = 1;
				this.canvas.height = 1;
			}
			this.canvas = null;
		}
		null;
		return;
	}
	,fontSize: function(pointSize) {
		var ctx = this.getContext2D();
		ctx.font = pointSize + "px Arial";
	}
	,strokeText: function(text,x,y) {
		var ctx = this.getContext2D();
		ctx.fillStyle = "white";
		ctx.fillText(text,x,y);
		ctx.strokeText(text,x,y);
	}
	,hasAlpha: function(drawable,context2D) {
		if(drawable.hasMask()) {
			context2D.globalCompositeOperation = "xor";
			return true;
		}
		return false;
	}
	,setAlphaCompositionOp: function(context2D) {
		context2D.globalCompositeOperation = "xor";
	}
	,setScreenCompositionOp: function() {
		this.getContext2D().globalCompositeOperation = "screen";
	}
	,resetCompositionOp: function() {
		this.getContext2D().globalCompositeOperation = "source-over";
	}
	,__class__: com_oddcast_util_js_JSCanvas
};
var com_oddcast_util_js_JSImage = function(name) {
	this.image = com_oddcast_util_js_elements_JSImageElement.create();
	this.image.id = new String(name);
	this.DEBUGimageCount(1);
};
$hxClasses["com.oddcast.util.js.JSImage"] = com_oddcast_util_js_JSImage;
com_oddcast_util_js_JSImage.__name__ = ["com","oddcast","util","js","JSImage"];
com_oddcast_util_js_JSImage.__interfaces__ = [com_oddcast_util_js_IJSdrawable,com_oddcast_util_IDisposable];
com_oddcast_util_js_JSImage.loadImage = function(url,maskURL,callbackIfNullURL,loadedCallback,fileErrorCallback) {
	if(callbackIfNullURL == null) callbackIfNullURL = false;
	if(url == null && callbackIfNullURL) loadedCallback(null); else {
		var jsImage = new com_oddcast_util_js_JSImage(com_oddcast_util_UtilsLite.stripFileDirectory(url));
		jsImage.load(url,maskURL,loadedCallback,fileErrorCallback);
	}
};
com_oddcast_util_js_JSImage.getTextureAsset = function(color,bDelete) {
	return color;
};
com_oddcast_util_js_JSImage.prototype = {
	id: function() {
		return this.image.id;
	}
	,load: function(url,maskURL,loadedCallback,fileErrorCallback) {
		var _g = this;
		this.image.onload = function(event) {
			_g.image.onload = null;
			if(maskURL != null) com_oddcast_util_js_JSImage.loadImage(maskURL,null,false,function(jsMaskImage) {
				_g.mask = jsMaskImage;
				_g.maskDownLoadSize = _g.mask.downloadSize;
				if(loadedCallback != null) loadedCallback(_g);
			},fileErrorCallback); else if(loadedCallback != null) loadedCallback(_g);
		};
		this.image.onerror = function(event1) {
			_g.image.onerror = null;
			if(fileErrorCallback != null) fileErrorCallback(url,event1.type);
		};
		if(maskURL != null) {
			var maskSrc = "url(" + maskURL + ")";
		}
		this.image.src = url;
	}
	,getNative: function() {
		return this.image;
	}
	,getMaskNative: function() {
		if(this.mask != null) return this.mask.image; else return null;
	}
	,newCanvasFromImage: function(id) {
		var jscanvas = new com_oddcast_util_js_JSCanvas(id,this.image.width,this.image.height);
		jscanvas.drawImageIntXY(this,0,0);
		return jscanvas;
	}
	,newCanvasFromImageRect: function(id,x,y,width,height) {
		var jscanvas = new com_oddcast_util_js_JSCanvas(id,width,height);
		jscanvas.drawImageIntsXsYsWsHdXdYdWdH(this,x,y,width,height,0,0,width,height);
		return jscanvas;
	}
	,get_width: function() {
		return this.image.width;
	}
	,get_height: function() {
		return this.image.height;
	}
	,getWidth: function() {
		return this.image.width;
	}
	,getHeight: function() {
		return this.image.height;
	}
	,getAssetSize: function() {
		return this.image.width * this.image.height * 4;
	}
	,DEBUGimageCount: function(inc) {
		com_oddcast_util_js_JSImage.DEBUGImageCount += inc;
		var s;
		if(inc > 0) s = " new "; else s = "dispose";
	}
	,dispose: function() {
		if(this.image != null) {
			this.DEBUGimageCount(-1);
			this.image.width = 1;
			this.image.height = 1;
			this.image = null;
		}
		this.mask = com_oddcast_util_Disposable.disposeIfValid(this.mask);
	}
	,DEBUG_String: function() {
		return "pixSize : " + this.image.width + "*" + this.image.height + "=" + this.image.width * this.image.height + " name:" + this.id();
	}
	,hasMask: function() {
		return this.mask != null;
	}
	,getDownloadSize: function() {
		return this.downloadSize + this.maskDownLoadSize;
	}
	,__class__: com_oddcast_util_js_JSImage
};
var com_oddcast_util_js_MatrixStack = function() {
	this.resetMatrix();
};
$hxClasses["com.oddcast.util.js.MatrixStack"] = com_oddcast_util_js_MatrixStack;
com_oddcast_util_js_MatrixStack.__name__ = ["com","oddcast","util","js","MatrixStack"];
com_oddcast_util_js_MatrixStack.prototype = {
	resetMatrix: function() {
		this.matrixList = new List();
		this.currMatrix = new com_oddcast_util_js_geom_Matrix(1,0,0,1,0,0);
	}
	,pushMatrix: function(m) {
		this.matrixList.push(this.currMatrix.clone());
		var t = m.clone();
		t.concat(this.currMatrix);
		this.currMatrix = t;
		return t;
	}
	,popMatrix: function() {
		this.currMatrix = this.matrixList.pop();
		if(this.currMatrix == null) throw new js__$Boot_HaxeError("invalid stack");
	}
	,__class__: com_oddcast_util_js_MatrixStack
};
var com_oddcast_util_js_OStype = $hxClasses["com.oddcast.util.js.OStype"] = { __ename__ : ["com","oddcast","util","js","OStype"], __constructs__ : ["Debug","iOS","Android","Flash","Generic"] };
com_oddcast_util_js_OStype.Debug = ["Debug",0];
com_oddcast_util_js_OStype.Debug.toString = $estr;
com_oddcast_util_js_OStype.Debug.__enum__ = com_oddcast_util_js_OStype;
com_oddcast_util_js_OStype.iOS = ["iOS",1];
com_oddcast_util_js_OStype.iOS.toString = $estr;
com_oddcast_util_js_OStype.iOS.__enum__ = com_oddcast_util_js_OStype;
com_oddcast_util_js_OStype.Android = ["Android",2];
com_oddcast_util_js_OStype.Android.toString = $estr;
com_oddcast_util_js_OStype.Android.__enum__ = com_oddcast_util_js_OStype;
com_oddcast_util_js_OStype.Flash = ["Flash",3];
com_oddcast_util_js_OStype.Flash.toString = $estr;
com_oddcast_util_js_OStype.Flash.__enum__ = com_oddcast_util_js_OStype;
com_oddcast_util_js_OStype.Generic = ["Generic",4];
com_oddcast_util_js_OStype.Generic.toString = $estr;
com_oddcast_util_js_OStype.Generic.__enum__ = com_oddcast_util_js_OStype;
com_oddcast_util_js_OStype.__empty_constructs__ = [com_oddcast_util_js_OStype.Debug,com_oddcast_util_js_OStype.iOS,com_oddcast_util_js_OStype.Android,com_oddcast_util_js_OStype.Flash,com_oddcast_util_js_OStype.Generic];
var com_oddcast_util_js_OS = function() { };
$hxClasses["com.oddcast.util.js.OS"] = com_oddcast_util_js_OS;
com_oddcast_util_js_OS.__name__ = ["com","oddcast","util","js","OS"];
com_oddcast_util_js_OS.typeIterator = function() {
	return Type.allEnums(com_oddcast_util_js_OStype);
};
com_oddcast_util_js_OS.getColor = function(osType,imageName) {
	switch(osType[1]) {
	case 0:
		return imageName + ".png";
	case 1:
		return imageName + ".jp2";
	case 2:
		return imageName + ".webp";
	case 3:case 4:
		return imageName + ".jpg";
	}
};
com_oddcast_util_js_OS.getMask = function(osType,imageName) {
	switch(osType[1]) {
	case 0:case 1:case 2:
		return null;
	case 3:
		return imageName + "_mask." + com_oddcast_util_js_OS.PNG_TRANS_DEPTH + ".png";
	case 4:
		return imageName + "_imask." + com_oddcast_util_js_OS.PNG_TRANS_DEPTH + ".png";
	}
};
com_oddcast_util_js_OS.getStoreColor = function(osType,imageName) {
	switch(osType[1]) {
	case 4:case 3:case 0:case 1:case 2:
		return imageName + ".png";
	}
};
com_oddcast_util_js_OS.getStoreMask = function(osType,imageName) {
	switch(osType[1]) {
	case 0:case 1:case 2:
		return null;
	case 3:
		return imageName + "_mask.png";
	case 4:
		return imageName + "_imask.png";
	}
};
var com_oddcast_util_js_RegisteredListener = function(target,eventString,eventListener) {
	this.target = target;
	this.eventListener = eventListener;
	this.eventString = eventString;
	target.addEventListener(eventString,eventListener);
};
$hxClasses["com.oddcast.util.js.RegisteredListener"] = com_oddcast_util_js_RegisteredListener;
com_oddcast_util_js_RegisteredListener.__name__ = ["com","oddcast","util","js","RegisteredListener"];
com_oddcast_util_js_RegisteredListener.__interfaces__ = [com_oddcast_util_IDisposable];
com_oddcast_util_js_RegisteredListener.prototype = {
	dispose: function() {
		if(this.target != null) {
			this.target.removeEventListener(this.eventString,this.eventListener);
			this.target = null;
			this.eventListener = null;
		}
	}
	,__class__: com_oddcast_util_js_RegisteredListener
};
var com_oddcast_util_js_RequestAnimationFramePolyfil = function() { };
$hxClasses["com.oddcast.util.js.RequestAnimationFramePolyfil"] = com_oddcast_util_js_RequestAnimationFramePolyfil;
com_oddcast_util_js_RequestAnimationFramePolyfil.__name__ = ["com","oddcast","util","js","RequestAnimationFramePolyfil"];
com_oddcast_util_js_RequestAnimationFramePolyfil.init = function() {
	
		var lastTime = 0;
		var vendors = ['ms', 'moz', 'webkit', 'o'];
		for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
			window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
			window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
									   || window[vendors[x]+'CancelRequestAnimationFrame'];
		}
	 
		if (!window.requestAnimationFrame)
			window.requestAnimationFrame = function(callback, element) {
				var currTime = new Date().getTime();
				var timeToCall = Math.max(0, 16 - (currTime - lastTime));
				var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
				  timeToCall);
				lastTime = currTime + timeToCall;
				return id;
			};
	 
		if (!window.cancelAnimationFrame)
			window.cancelAnimationFrame = function(id) {
				clearTimeout(id);
			};
		;
};
var com_oddcast_util_js_elements_JSCanvasElement = function() { };
$hxClasses["com.oddcast.util.js.elements.JSCanvasElement"] = com_oddcast_util_js_elements_JSCanvasElement;
com_oddcast_util_js_elements_JSCanvasElement.__name__ = ["com","oddcast","util","js","elements","JSCanvasElement"];
com_oddcast_util_js_elements_JSCanvasElement.create = function() {
	return window.document.createElement("canvas");
};
com_oddcast_util_js_elements_JSCanvasElement.__super__ = HTMLCanvasElement;
com_oddcast_util_js_elements_JSCanvasElement.prototype = $extend(HTMLCanvasElement.prototype,{
	__class__: com_oddcast_util_js_elements_JSCanvasElement
});
var com_oddcast_util_js_elements_JSImageElement = function(width,height) {
	Image.call(this,width,height);
};
$hxClasses["com.oddcast.util.js.elements.JSImageElement"] = com_oddcast_util_js_elements_JSImageElement;
com_oddcast_util_js_elements_JSImageElement.__name__ = ["com","oddcast","util","js","elements","JSImageElement"];
com_oddcast_util_js_elements_JSImageElement.create = function() {
	return new Image();
};
com_oddcast_util_js_elements_JSImageElement.load = function(url,callbackFunc,error) {
	var image = com_oddcast_util_js_elements_JSImageElement.create();
	image.onload = function(event) {
		image.onload = null;
		if(callbackFunc != null) callbackFunc(image);
	};
	image.onerror = function(event1) {
		image.onerror = null;
		if(error != null) error(url,event1.type);
	};
	image.src = url;
};
com_oddcast_util_js_elements_JSImageElement.__super__ = Image;
com_oddcast_util_js_elements_JSImageElement.prototype = $extend(Image.prototype,{
	__class__: com_oddcast_util_js_elements_JSImageElement
});
var com_oddcast_util_js_geom_ColorTransform = function(redMultiplier,greenMultiplier,blueMultiplier,alphaMultiplier,redOffset,greenOffset,blueOffset,alphaOffset) {
	if(alphaOffset == null) alphaOffset = 0;
	if(blueOffset == null) blueOffset = 0;
	if(greenOffset == null) greenOffset = 0;
	if(redOffset == null) redOffset = 0;
	if(alphaMultiplier == null) alphaMultiplier = 1.0;
	if(blueMultiplier == null) blueMultiplier = 1.0;
	if(greenMultiplier == null) greenMultiplier = 1.0;
	if(redMultiplier == null) redMultiplier = 1.0;
	this.redMultiplier = redMultiplier;
	this.greenMultiplier = greenMultiplier;
	this.blueMultiplier = blueMultiplier;
	this.alphaMultiplier = alphaMultiplier;
	this.redOffset = redOffset;
	this.greenOffset = greenOffset;
	this.blueOffset = blueOffset;
	this.alphaOffset = alphaOffset;
};
$hxClasses["com.oddcast.util.js.geom.ColorTransform"] = com_oddcast_util_js_geom_ColorTransform;
com_oddcast_util_js_geom_ColorTransform.__name__ = ["com","oddcast","util","js","geom","ColorTransform"];
com_oddcast_util_js_geom_ColorTransform.prototype = {
	concat: function(second) {
		this.redMultiplier *= second.redMultiplier;
		this.greenMultiplier *= second.greenMultiplier;
		this.blueMultiplier *= second.blueMultiplier;
		this.alphaMultiplier *= second.alphaMultiplier;
		this.redOffset = second.redMultiplier * this.redOffset + second.redOffset;
		this.greenOffset = second.greenMultiplier * this.greenOffset + second.greenOffset;
		this.blueOffset = second.blueMultiplier * this.blueOffset + second.blueOffset;
		this.alphaOffset = second.alphaMultiplier * this.alphaOffset + second.alphaOffset;
	}
	,__class__: com_oddcast_util_js_geom_ColorTransform
};
var com_oddcast_util_js_geom_Matrix = function(a,b,c,d,tx,ty) {
	if(ty == null) ty = 0;
	if(tx == null) tx = 0;
	if(d == null) d = 1;
	if(c == null) c = 0;
	if(b == null) b = 0;
	if(a == null) a = 1;
	this.a = a;
	this.b = b;
	this.c = c;
	this.d = d;
	this.tx = tx;
	this.ty = ty;
};
$hxClasses["com.oddcast.util.js.geom.Matrix"] = com_oddcast_util_js_geom_Matrix;
com_oddcast_util_js_geom_Matrix.__name__ = ["com","oddcast","util","js","geom","Matrix"];
com_oddcast_util_js_geom_Matrix.prototype = {
	clone: function() {
		return new com_oddcast_util_js_geom_Matrix(this.a,this.b,this.c,this.d,this.tx,this.ty);
	}
	,concat: function(m) {
		var na = this.a * m.a + this.b * m.c;
		var nc = this.c * m.a + this.d * m.c;
		var ntx = this.tx * m.a + this.ty * m.c + m.tx;
		var nb = this.a * m.b + this.b * m.d;
		var nd = this.c * m.b + this.d * m.d;
		var nty = this.tx * m.b + this.ty * m.d + m.ty;
		this.a = na;
		this.b = nb;
		this.c = nc;
		this.d = nd;
		this.tx = ntx;
		this.ty = nty;
	}
	,scale: function(sx,sy) {
		this.a *= sx;
		this.d *= sy;
		this.tx *= sx;
		this.ty *= sy;
	}
	,identity: function() {
		this.a = 1;
		this.b = 0;
		this.c = 0;
		this.d = 1;
		this.tx = 0;
		this.ty = 0;
	}
	,rotate: function(angle) {
		var sin = Math.sin(angle);
		var cos = Math.cos(angle);
		var na = this.a * cos - this.b * sin;
		var nc = this.c * cos - this.d * sin;
		var ntx = this.tx * cos - this.ty * sin;
		var nb = this.a * sin + this.b * cos;
		var nd = this.c * sin + this.d * cos;
		this.ty = this.tx * sin + this.ty * cos;
		this.a = na;
		this.b = nb;
		this.c = nc;
		this.d = nd;
		this.tx = ntx;
	}
	,translate: function(dx,dy) {
		this.tx += dx;
		this.ty += dy;
	}
	,transformPoint: function(point) {
		return new com_oddcast_neko_geom_Point(this.a * point.x + this.c * point.y + this.tx,this.b * point.x + this.d * point.y + this.ty);
	}
	,deltaTransformPoint: function(point) {
		return new com_oddcast_neko_geom_Point(this.a * point.x + this.c * point.y,this.b * point.x + this.d * point.y);
	}
	,invert: function() {
		var det = -this.b * this.c + this.a * this.d;
		if(det == 0.0) throw new js__$Boot_HaxeError("Zero Determinant");
		var idet = 1 / det;
		var t11 = this.d * idet;
		var t12 = -this.c * idet;
		var t13 = (this.c * this.ty - this.tx * this.d) * idet;
		var t21 = -this.b * idet;
		var t22 = this.a * idet;
		var t23 = -(-this.b * this.tx + this.a * this.ty) * idet;
		this.a = t11;
		this.c = t12;
		this.tx = t13;
		this.b = t21;
		this.d = t22;
		this.ty = t23;
	}
	,toString: function() {
		return "(a=" + this.a + ", b=" + this.b + ", c=" + this.c + ", d=" + this.d + ", tx=" + this.tx + ", ty=" + this.ty + ")";
	}
	,setTo: function(aa,ba,ca,da,txa,tya) {
		this.a = aa;
		this.c = ca;
		this.tx = txa;
		this.b = ba;
		this.d = da;
		this.ty = tya;
	}
	,__class__: com_oddcast_util_js_geom_Matrix
};
var format_tools_Adler32 = function() {
	this.a1 = 1;
	this.a2 = 0;
};
$hxClasses["format.tools.Adler32"] = format_tools_Adler32;
format_tools_Adler32.__name__ = ["format","tools","Adler32"];
format_tools_Adler32.read = function(i) {
	var a = new format_tools_Adler32();
	var a2a = i.readByte();
	var a2b = i.readByte();
	var a1a = i.readByte();
	var a1b = i.readByte();
	a.a1 = a1a << 8 | a1b;
	a.a2 = a2a << 8 | a2b;
	return a;
};
format_tools_Adler32.prototype = {
	update: function(b,pos,len) {
		var a1 = this.a1;
		var a2 = this.a2;
		var _g1 = pos;
		var _g = pos + len;
		while(_g1 < _g) {
			var p = _g1++;
			var c = b.b[p];
			a1 = (a1 + c) % 65521;
			a2 = (a2 + a1) % 65521;
		}
		this.a1 = a1;
		this.a2 = a2;
	}
	,equals: function(a) {
		return a.a1 == this.a1 && a.a2 == this.a2;
	}
	,__class__: format_tools_Adler32
};
var format_tools_Huffman = $hxClasses["format.tools.Huffman"] = { __ename__ : ["format","tools","Huffman"], __constructs__ : ["Found","NeedBit","NeedBits"] };
format_tools_Huffman.Found = function(i) { var $x = ["Found",0,i]; $x.__enum__ = format_tools_Huffman; $x.toString = $estr; return $x; };
format_tools_Huffman.NeedBit = function(left,right) { var $x = ["NeedBit",1,left,right]; $x.__enum__ = format_tools_Huffman; $x.toString = $estr; return $x; };
format_tools_Huffman.NeedBits = function(n,table) { var $x = ["NeedBits",2,n,table]; $x.__enum__ = format_tools_Huffman; $x.toString = $estr; return $x; };
format_tools_Huffman.__empty_constructs__ = [];
var format_tools_HuffTools = function() {
};
$hxClasses["format.tools.HuffTools"] = format_tools_HuffTools;
format_tools_HuffTools.__name__ = ["format","tools","HuffTools"];
format_tools_HuffTools.prototype = {
	treeDepth: function(t) {
		switch(t[1]) {
		case 0:
			return 0;
		case 2:
			throw new js__$Boot_HaxeError("assert");
			break;
		case 1:
			var b = t[3];
			var a = t[2];
			var da = this.treeDepth(a);
			var db = this.treeDepth(b);
			return 1 + (da < db?da:db);
		}
	}
	,treeCompress: function(t) {
		var d = this.treeDepth(t);
		if(d == 0) return t;
		if(d == 1) switch(t[1]) {
		case 1:
			var b = t[3];
			var a = t[2];
			return format_tools_Huffman.NeedBit(this.treeCompress(a),this.treeCompress(b));
		default:
			throw new js__$Boot_HaxeError("assert");
		}
		var size = 1 << d;
		var table = [];
		var _g = 0;
		while(_g < size) {
			var i = _g++;
			table.push(format_tools_Huffman.Found(-1));
		}
		this.treeWalk(table,0,0,d,t);
		return format_tools_Huffman.NeedBits(d,table);
	}
	,treeWalk: function(table,p,cd,d,t) {
		switch(t[1]) {
		case 1:
			var b = t[3];
			var a = t[2];
			if(d > 0) {
				this.treeWalk(table,p,cd + 1,d - 1,a);
				this.treeWalk(table,p | 1 << cd,cd + 1,d - 1,b);
			} else table[p] = this.treeCompress(t);
			break;
		default:
			table[p] = this.treeCompress(t);
		}
	}
	,treeMake: function(bits,maxbits,v,len) {
		if(len > maxbits) throw new js__$Boot_HaxeError("Invalid huffman");
		var idx = v << 5 | len;
		if(bits.h.hasOwnProperty(idx)) return format_tools_Huffman.Found(bits.h[idx]);
		v <<= 1;
		len += 1;
		return format_tools_Huffman.NeedBit(this.treeMake(bits,maxbits,v,len),this.treeMake(bits,maxbits,v | 1,len));
	}
	,make: function(lengths,pos,nlengths,maxbits) {
		var counts = [];
		var tmp = [];
		if(maxbits > 32) throw new js__$Boot_HaxeError("Invalid huffman");
		var _g = 0;
		while(_g < maxbits) {
			var i = _g++;
			counts.push(0);
			tmp.push(0);
		}
		var _g1 = 0;
		while(_g1 < nlengths) {
			var i1 = _g1++;
			var p = lengths[i1 + pos];
			if(p >= maxbits) throw new js__$Boot_HaxeError("Invalid huffman");
			counts[p]++;
		}
		var code = 0;
		var _g11 = 1;
		var _g2 = maxbits - 1;
		while(_g11 < _g2) {
			var i2 = _g11++;
			code = code + counts[i2] << 1;
			tmp[i2] = code;
		}
		var bits = new haxe_ds_IntMap();
		var _g3 = 0;
		while(_g3 < nlengths) {
			var i3 = _g3++;
			var l = lengths[i3 + pos];
			if(l != 0) {
				var n = tmp[l - 1];
				tmp[l - 1] = n + 1;
				bits.h[n << 5 | l] = i3;
			}
		}
		return this.treeCompress(format_tools_Huffman.NeedBit(this.treeMake(bits,maxbits,0,1),this.treeMake(bits,maxbits,1,1)));
	}
	,__class__: format_tools_HuffTools
};
var format_tools__$InflateImpl_Window = function(hasCrc) {
	this.buffer = haxe_io_Bytes.alloc(65536);
	this.pos = 0;
	if(hasCrc) this.crc = new format_tools_Adler32();
};
$hxClasses["format.tools._InflateImpl.Window"] = format_tools__$InflateImpl_Window;
format_tools__$InflateImpl_Window.__name__ = ["format","tools","_InflateImpl","Window"];
format_tools__$InflateImpl_Window.prototype = {
	slide: function() {
		if(this.crc != null) this.crc.update(this.buffer,0,32768);
		var b = haxe_io_Bytes.alloc(65536);
		this.pos -= 32768;
		b.blit(0,this.buffer,32768,this.pos);
		this.buffer = b;
	}
	,addBytes: function(b,p,len) {
		if(this.pos + len > 65536) this.slide();
		this.buffer.blit(this.pos,b,p,len);
		this.pos += len;
	}
	,addByte: function(c) {
		if(this.pos == 65536) this.slide();
		this.buffer.b[this.pos] = c & 255;
		this.pos++;
	}
	,getLastChar: function() {
		return this.buffer.b[this.pos - 1];
	}
	,available: function() {
		return this.pos;
	}
	,checksum: function() {
		if(this.crc != null) this.crc.update(this.buffer,0,this.pos);
		return this.crc;
	}
	,__class__: format_tools__$InflateImpl_Window
};
var format_tools__$InflateImpl_State = $hxClasses["format.tools._InflateImpl.State"] = { __ename__ : ["format","tools","_InflateImpl","State"], __constructs__ : ["Head","Block","CData","Flat","Crc","Dist","DistOne","Done"] };
format_tools__$InflateImpl_State.Head = ["Head",0];
format_tools__$InflateImpl_State.Head.toString = $estr;
format_tools__$InflateImpl_State.Head.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.Block = ["Block",1];
format_tools__$InflateImpl_State.Block.toString = $estr;
format_tools__$InflateImpl_State.Block.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.CData = ["CData",2];
format_tools__$InflateImpl_State.CData.toString = $estr;
format_tools__$InflateImpl_State.CData.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.Flat = ["Flat",3];
format_tools__$InflateImpl_State.Flat.toString = $estr;
format_tools__$InflateImpl_State.Flat.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.Crc = ["Crc",4];
format_tools__$InflateImpl_State.Crc.toString = $estr;
format_tools__$InflateImpl_State.Crc.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.Dist = ["Dist",5];
format_tools__$InflateImpl_State.Dist.toString = $estr;
format_tools__$InflateImpl_State.Dist.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.DistOne = ["DistOne",6];
format_tools__$InflateImpl_State.DistOne.toString = $estr;
format_tools__$InflateImpl_State.DistOne.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.Done = ["Done",7];
format_tools__$InflateImpl_State.Done.toString = $estr;
format_tools__$InflateImpl_State.Done.__enum__ = format_tools__$InflateImpl_State;
format_tools__$InflateImpl_State.__empty_constructs__ = [format_tools__$InflateImpl_State.Head,format_tools__$InflateImpl_State.Block,format_tools__$InflateImpl_State.CData,format_tools__$InflateImpl_State.Flat,format_tools__$InflateImpl_State.Crc,format_tools__$InflateImpl_State.Dist,format_tools__$InflateImpl_State.DistOne,format_tools__$InflateImpl_State.Done];
var format_tools_InflateImpl = function(i,header,crc) {
	if(crc == null) crc = true;
	if(header == null) header = true;
	this["final"] = false;
	this.htools = new format_tools_HuffTools();
	this.huffman = this.buildFixedHuffman();
	this.huffdist = null;
	this.len = 0;
	this.dist = 0;
	if(header) this.state = format_tools__$InflateImpl_State.Head; else this.state = format_tools__$InflateImpl_State.Block;
	this.input = i;
	this.bits = 0;
	this.nbits = 0;
	this.needed = 0;
	this.output = null;
	this.outpos = 0;
	this.lengths = [];
	var _g = 0;
	while(_g < 19) {
		var i1 = _g++;
		this.lengths.push(-1);
	}
	this.window = new format_tools__$InflateImpl_Window(crc);
};
$hxClasses["format.tools.InflateImpl"] = format_tools_InflateImpl;
format_tools_InflateImpl.__name__ = ["format","tools","InflateImpl"];
format_tools_InflateImpl.run = function(i,bufsize) {
	if(bufsize == null) bufsize = 65536;
	var buf = haxe_io_Bytes.alloc(bufsize);
	var output = new haxe_io_BytesBuffer();
	var inflate = new format_tools_InflateImpl(i);
	while(true) {
		var len = inflate.readBytes(buf,0,bufsize);
		output.addBytes(buf,0,len);
		if(len < bufsize) break;
	}
	return output.getBytes();
};
format_tools_InflateImpl.prototype = {
	buildFixedHuffman: function() {
		if(format_tools_InflateImpl.FIXED_HUFFMAN != null) return format_tools_InflateImpl.FIXED_HUFFMAN;
		var a = [];
		var _g = 0;
		while(_g < 288) {
			var n = _g++;
			a.push(n <= 143?8:n <= 255?9:n <= 279?7:8);
		}
		format_tools_InflateImpl.FIXED_HUFFMAN = this.htools.make(a,0,288,10);
		return format_tools_InflateImpl.FIXED_HUFFMAN;
	}
	,readBytes: function(b,pos,len) {
		this.needed = len;
		this.outpos = pos;
		this.output = b;
		if(len > 0) while(this.inflateLoop()) {
		}
		return len - this.needed;
	}
	,getBits: function(n) {
		while(this.nbits < n) {
			this.bits |= this.input.readByte() << this.nbits;
			this.nbits += 8;
		}
		var b = this.bits & (1 << n) - 1;
		this.nbits -= n;
		this.bits >>= n;
		return b;
	}
	,getBit: function() {
		if(this.nbits == 0) {
			this.nbits = 8;
			this.bits = this.input.readByte();
		}
		var b = (this.bits & 1) == 1;
		this.nbits--;
		this.bits >>= 1;
		return b;
	}
	,getRevBits: function(n) {
		if(n == 0) return 0; else if(this.getBit()) return 1 << n - 1 | this.getRevBits(n - 1); else return this.getRevBits(n - 1);
	}
	,resetBits: function() {
		this.bits = 0;
		this.nbits = 0;
	}
	,addBytes: function(b,p,len) {
		this.window.addBytes(b,p,len);
		this.output.blit(this.outpos,b,p,len);
		this.needed -= len;
		this.outpos += len;
	}
	,addByte: function(b) {
		this.window.addByte(b);
		this.output.b[this.outpos] = b & 255;
		this.needed--;
		this.outpos++;
	}
	,addDistOne: function(n) {
		var c = this.window.getLastChar();
		var _g = 0;
		while(_g < n) {
			var i = _g++;
			this.addByte(c);
		}
	}
	,addDist: function(d,len) {
		this.addBytes(this.window.buffer,this.window.pos - d,len);
	}
	,applyHuffman: function(h) {
		switch(h[1]) {
		case 0:
			var n = h[2];
			return n;
		case 1:
			var b = h[3];
			var a = h[2];
			return this.applyHuffman(this.getBit()?b:a);
		case 2:
			var tbl = h[3];
			var n1 = h[2];
			return this.applyHuffman(tbl[this.getBits(n1)]);
		}
	}
	,inflateLengths: function(a,max) {
		var i = 0;
		var prev = 0;
		while(i < max) {
			var n = this.applyHuffman(this.huffman);
			switch(n) {
			case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:case 8:case 9:case 10:case 11:case 12:case 13:case 14:case 15:
				prev = n;
				a[i] = n;
				i++;
				break;
			case 16:
				var end = i + 3 + this.getBits(2);
				if(end > max) throw new js__$Boot_HaxeError("Invalid data");
				while(i < end) {
					a[i] = prev;
					i++;
				}
				break;
			case 17:
				i += 3 + this.getBits(3);
				if(i > max) throw new js__$Boot_HaxeError("Invalid data");
				break;
			case 18:
				i += 11 + this.getBits(7);
				if(i > max) throw new js__$Boot_HaxeError("Invalid data");
				break;
			default:
				throw new js__$Boot_HaxeError("Invalid data");
			}
		}
	}
	,inflateLoop: function() {
		var _g = this.state;
		switch(_g[1]) {
		case 0:
			var cmf = this.input.readByte();
			var cm = cmf & 15;
			var cinfo = cmf >> 4;
			if(cm != 8) throw new js__$Boot_HaxeError("Invalid data");
			var flg = this.input.readByte();
			var fdict = (flg & 32) != 0;
			if(((cmf << 8) + flg) % 31 != 0) throw new js__$Boot_HaxeError("Invalid data");
			if(fdict) throw new js__$Boot_HaxeError("Unsupported dictionary");
			this.state = format_tools__$InflateImpl_State.Block;
			return true;
		case 4:
			var calc = this.window.checksum();
			if(calc == null) {
				this.state = format_tools__$InflateImpl_State.Done;
				return true;
			}
			var crc = format_tools_Adler32.read(this.input);
			if(!calc.equals(crc)) throw new js__$Boot_HaxeError("Invalid CRC");
			this.state = format_tools__$InflateImpl_State.Done;
			return true;
		case 7:
			return false;
		case 1:
			this["final"] = this.getBit();
			var _g1 = this.getBits(2);
			switch(_g1) {
			case 0:
				this.len = this.input.readUInt16();
				var nlen = this.input.readUInt16();
				if(nlen != 65535 - this.len) throw new js__$Boot_HaxeError("Invalid data");
				this.state = format_tools__$InflateImpl_State.Flat;
				var r = this.inflateLoop();
				this.resetBits();
				return r;
			case 1:
				this.huffman = this.buildFixedHuffman();
				this.huffdist = null;
				this.state = format_tools__$InflateImpl_State.CData;
				return true;
			case 2:
				var hlit = this.getBits(5) + 257;
				var hdist = this.getBits(5) + 1;
				var hclen = this.getBits(4) + 4;
				var _g2 = 0;
				while(_g2 < hclen) {
					var i = _g2++;
					this.lengths[format_tools_InflateImpl.CODE_LENGTHS_POS[i]] = this.getBits(3);
				}
				var _g21 = hclen;
				while(_g21 < 19) {
					var i1 = _g21++;
					this.lengths[format_tools_InflateImpl.CODE_LENGTHS_POS[i1]] = 0;
				}
				this.huffman = this.htools.make(this.lengths,0,19,8);
				var lengths = [];
				var _g3 = 0;
				var _g22 = hlit + hdist;
				while(_g3 < _g22) {
					var i2 = _g3++;
					lengths.push(0);
				}
				this.inflateLengths(lengths,hlit + hdist);
				this.huffdist = this.htools.make(lengths,hlit,hdist,16);
				this.huffman = this.htools.make(lengths,0,hlit,16);
				this.state = format_tools__$InflateImpl_State.CData;
				return true;
			default:
				throw new js__$Boot_HaxeError("Invalid data");
			}
			break;
		case 3:
			var rlen;
			if(this.len < this.needed) rlen = this.len; else rlen = this.needed;
			var bytes = this.input.read(rlen);
			this.len -= rlen;
			this.addBytes(bytes,0,rlen);
			if(this.len == 0) if(this["final"]) this.state = format_tools__$InflateImpl_State.Crc; else this.state = format_tools__$InflateImpl_State.Block;
			return this.needed > 0;
		case 6:
			var rlen1;
			if(this.len < this.needed) rlen1 = this.len; else rlen1 = this.needed;
			this.addDistOne(rlen1);
			this.len -= rlen1;
			if(this.len == 0) this.state = format_tools__$InflateImpl_State.CData;
			return this.needed > 0;
		case 5:
			while(this.len > 0 && this.needed > 0) {
				var rdist;
				if(this.len < this.dist) rdist = this.len; else rdist = this.dist;
				var rlen2;
				if(this.needed < rdist) rlen2 = this.needed; else rlen2 = rdist;
				this.addDist(this.dist,rlen2);
				this.len -= rlen2;
			}
			if(this.len == 0) this.state = format_tools__$InflateImpl_State.CData;
			return this.needed > 0;
		case 2:
			var n = this.applyHuffman(this.huffman);
			if(n < 256) {
				this.addByte(n);
				return this.needed > 0;
			} else if(n == 256) {
				if(this["final"]) this.state = format_tools__$InflateImpl_State.Crc; else this.state = format_tools__$InflateImpl_State.Block;
				return true;
			} else {
				n -= 257;
				var extra_bits = format_tools_InflateImpl.LEN_EXTRA_BITS_TBL[n];
				if(extra_bits == -1) throw new js__$Boot_HaxeError("Invalid data");
				this.len = format_tools_InflateImpl.LEN_BASE_VAL_TBL[n] + this.getBits(extra_bits);
				var dist_code;
				if(this.huffdist == null) dist_code = this.getRevBits(5); else dist_code = this.applyHuffman(this.huffdist);
				extra_bits = format_tools_InflateImpl.DIST_EXTRA_BITS_TBL[dist_code];
				if(extra_bits == -1) throw new js__$Boot_HaxeError("Invalid data");
				this.dist = format_tools_InflateImpl.DIST_BASE_VAL_TBL[dist_code] + this.getBits(extra_bits);
				if(this.dist > this.window.available()) throw new js__$Boot_HaxeError("Invalid data");
				if(this.dist == 1) this.state = format_tools__$InflateImpl_State.DistOne; else this.state = format_tools__$InflateImpl_State.Dist;
				return true;
			}
			break;
		}
	}
	,__class__: format_tools_InflateImpl
};
var haxe_StackItem = $hxClasses["haxe.StackItem"] = { __ename__ : ["haxe","StackItem"], __constructs__ : ["CFunction","Module","FilePos","Method","LocalFunction"] };
haxe_StackItem.CFunction = ["CFunction",0];
haxe_StackItem.CFunction.toString = $estr;
haxe_StackItem.CFunction.__enum__ = haxe_StackItem;
haxe_StackItem.Module = function(m) { var $x = ["Module",1,m]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.FilePos = function(s,file,line) { var $x = ["FilePos",2,s,file,line]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.Method = function(classname,method) { var $x = ["Method",3,classname,method]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.LocalFunction = function(v) { var $x = ["LocalFunction",4,v]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.__empty_constructs__ = [haxe_StackItem.CFunction];
var haxe_CallStack = function() { };
$hxClasses["haxe.CallStack"] = haxe_CallStack;
haxe_CallStack.__name__ = ["haxe","CallStack"];
haxe_CallStack.getStack = function(e) {
	if(e == null) return [];
	var oldValue = Error.prepareStackTrace;
	Error.prepareStackTrace = function(error,callsites) {
		var stack = [];
		var _g = 0;
		while(_g < callsites.length) {
			var site = callsites[_g];
			++_g;
			if(haxe_CallStack.wrapCallSite != null) site = haxe_CallStack.wrapCallSite(site);
			var method = null;
			var fullName = site.getFunctionName();
			if(fullName != null) {
				var idx = fullName.lastIndexOf(".");
				if(idx >= 0) {
					var className = HxOverrides.substr(fullName,0,idx);
					var methodName = HxOverrides.substr(fullName,idx + 1,null);
					method = haxe_StackItem.Method(className,methodName);
				}
			}
			stack.push(haxe_StackItem.FilePos(method,site.getFileName(),site.getLineNumber()));
		}
		return stack;
	};
	var a = haxe_CallStack.makeStack(e.stack);
	Error.prepareStackTrace = oldValue;
	return a;
};
haxe_CallStack.callStack = function() {
	try {
		throw new Error();
	} catch( e ) {
		haxe_CallStack.lastException = e;
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		var a = haxe_CallStack.getStack(e);
		a.shift();
		return a;
	}
};
haxe_CallStack.exceptionStack = function() {
	return haxe_CallStack.getStack(haxe_CallStack.lastException);
};
haxe_CallStack.toString = function(stack) {
	var b = new StringBuf();
	var _g = 0;
	while(_g < stack.length) {
		var s = stack[_g];
		++_g;
		b.b += "\nCalled from ";
		haxe_CallStack.itemToString(b,s);
	}
	return b.b;
};
haxe_CallStack.itemToString = function(b,s) {
	switch(s[1]) {
	case 0:
		b.b += "a C function";
		break;
	case 1:
		var m = s[2];
		b.b += "module ";
		if(m == null) b.b += "null"; else b.b += "" + m;
		break;
	case 2:
		var line = s[4];
		var file = s[3];
		var s1 = s[2];
		if(s1 != null) {
			haxe_CallStack.itemToString(b,s1);
			b.b += " (";
		}
		if(file == null) b.b += "null"; else b.b += "" + file;
		b.b += " line ";
		if(line == null) b.b += "null"; else b.b += "" + line;
		if(s1 != null) b.b += ")";
		break;
	case 3:
		var meth = s[3];
		var cname = s[2];
		if(cname == null) b.b += "null"; else b.b += "" + cname;
		b.b += ".";
		if(meth == null) b.b += "null"; else b.b += "" + meth;
		break;
	case 4:
		var n = s[2];
		b.b += "local function #";
		if(n == null) b.b += "null"; else b.b += "" + n;
		break;
	}
};
haxe_CallStack.makeStack = function(s) {
	if(s == null) return []; else if(typeof(s) == "string") {
		var stack = s.split("\n");
		if(stack[0] == "Error") stack.shift();
		var m = [];
		var rie10 = new EReg("^   at ([A-Za-z0-9_. ]+) \\(([^)]+):([0-9]+):([0-9]+)\\)$","");
		var _g = 0;
		while(_g < stack.length) {
			var line = stack[_g];
			++_g;
			if(rie10.match(line)) {
				var path = rie10.matched(1).split(".");
				var meth = path.pop();
				var file = rie10.matched(2);
				var line1 = Std.parseInt(rie10.matched(3));
				m.push(haxe_StackItem.FilePos(meth == "Anonymous function"?haxe_StackItem.LocalFunction():meth == "Global code"?null:haxe_StackItem.Method(path.join("."),meth),file,line1));
			} else m.push(haxe_StackItem.Module(StringTools.trim(line)));
		}
		return m;
	} else return s;
};
var haxe_IMap = function() { };
$hxClasses["haxe.IMap"] = haxe_IMap;
haxe_IMap.__name__ = ["haxe","IMap"];
haxe_IMap.prototype = {
	__class__: haxe_IMap
};
var haxe__$Int64__$_$_$Int64 = function(high,low) {
	this.high = high;
	this.low = low;
};
$hxClasses["haxe._Int64.___Int64"] = haxe__$Int64__$_$_$Int64;
haxe__$Int64__$_$_$Int64.__name__ = ["haxe","_Int64","___Int64"];
haxe__$Int64__$_$_$Int64.prototype = {
	__class__: haxe__$Int64__$_$_$Int64
};
var haxe_Log = function() { };
$hxClasses["haxe.Log"] = haxe_Log;
haxe_Log.__name__ = ["haxe","Log"];
haxe_Log.trace = function(v,infos) {
	js_Boot.__trace(v,infos);
};
var haxe_Serializer = function() {
	this.buf = new StringBuf();
	this.cache = [];
	this.useCache = haxe_Serializer.USE_CACHE;
	this.useEnumIndex = haxe_Serializer.USE_ENUM_INDEX;
	this.shash = new haxe_ds_StringMap();
	this.scount = 0;
};
$hxClasses["haxe.Serializer"] = haxe_Serializer;
haxe_Serializer.__name__ = ["haxe","Serializer"];
haxe_Serializer.run = function(v) {
	var s = new haxe_Serializer();
	s.serialize(v);
	return s.toString();
};
haxe_Serializer.prototype = {
	toString: function() {
		return this.buf.b;
	}
	,serializeString: function(s) {
		var x = this.shash.get(s);
		if(x != null) {
			this.buf.b += "R";
			if(x == null) this.buf.b += "null"; else this.buf.b += "" + x;
			return;
		}
		this.shash.set(s,this.scount++);
		this.buf.b += "y";
		s = encodeURIComponent(s);
		if(s.length == null) this.buf.b += "null"; else this.buf.b += "" + s.length;
		this.buf.b += ":";
		if(s == null) this.buf.b += "null"; else this.buf.b += "" + s;
	}
	,serializeRef: function(v) {
		var vt = typeof(v);
		var _g1 = 0;
		var _g = this.cache.length;
		while(_g1 < _g) {
			var i = _g1++;
			var ci = this.cache[i];
			if(typeof(ci) == vt && ci == v) {
				this.buf.b += "r";
				if(i == null) this.buf.b += "null"; else this.buf.b += "" + i;
				return true;
			}
		}
		this.cache.push(v);
		return false;
	}
	,serializeFields: function(v) {
		var _g = 0;
		var _g1 = Reflect.fields(v);
		while(_g < _g1.length) {
			var f = _g1[_g];
			++_g;
			this.serializeString(f);
			this.serialize(Reflect.field(v,f));
		}
		this.buf.b += "g";
	}
	,serialize: function(v) {
		{
			var _g = Type["typeof"](v);
			switch(_g[1]) {
			case 0:
				this.buf.b += "n";
				break;
			case 1:
				var v1 = v;
				if(v1 == 0) {
					this.buf.b += "z";
					return;
				}
				this.buf.b += "i";
				if(v1 == null) this.buf.b += "null"; else this.buf.b += "" + v1;
				break;
			case 2:
				var v2 = v;
				if(isNaN(v2)) this.buf.b += "k"; else if(!isFinite(v2)) if(v2 < 0) this.buf.b += "m"; else this.buf.b += "p"; else {
					this.buf.b += "d";
					if(v2 == null) this.buf.b += "null"; else this.buf.b += "" + v2;
				}
				break;
			case 3:
				if(v) this.buf.b += "t"; else this.buf.b += "f";
				break;
			case 6:
				var c = _g[2];
				if(c == String) {
					this.serializeString(v);
					return;
				}
				if(this.useCache && this.serializeRef(v)) return;
				switch(c) {
				case Array:
					var ucount = 0;
					this.buf.b += "a";
					var l = v.length;
					var _g1 = 0;
					while(_g1 < l) {
						var i = _g1++;
						if(v[i] == null) ucount++; else {
							if(ucount > 0) {
								if(ucount == 1) this.buf.b += "n"; else {
									this.buf.b += "u";
									if(ucount == null) this.buf.b += "null"; else this.buf.b += "" + ucount;
								}
								ucount = 0;
							}
							this.serialize(v[i]);
						}
					}
					if(ucount > 0) {
						if(ucount == 1) this.buf.b += "n"; else {
							this.buf.b += "u";
							if(ucount == null) this.buf.b += "null"; else this.buf.b += "" + ucount;
						}
					}
					this.buf.b += "h";
					break;
				case List:
					this.buf.b += "l";
					var v3 = v;
					var _g1_head = v3.h;
					var _g1_val = null;
					while(_g1_head != null) {
						var i1;
						_g1_val = _g1_head[0];
						_g1_head = _g1_head[1];
						i1 = _g1_val;
						this.serialize(i1);
					}
					this.buf.b += "h";
					break;
				case Date:
					var d = v;
					this.buf.b += "v";
					this.buf.add(d.getTime());
					break;
				case haxe_ds_StringMap:
					this.buf.b += "b";
					var v4 = v;
					var $it0 = v4.keys();
					while( $it0.hasNext() ) {
						var k = $it0.next();
						this.serializeString(k);
						this.serialize(__map_reserved[k] != null?v4.getReserved(k):v4.h[k]);
					}
					this.buf.b += "h";
					break;
				case haxe_ds_IntMap:
					this.buf.b += "q";
					var v5 = v;
					var $it1 = v5.keys();
					while( $it1.hasNext() ) {
						var k1 = $it1.next();
						this.buf.b += ":";
						if(k1 == null) this.buf.b += "null"; else this.buf.b += "" + k1;
						this.serialize(v5.h[k1]);
					}
					this.buf.b += "h";
					break;
				case haxe_ds_ObjectMap:
					this.buf.b += "M";
					var v6 = v;
					var $it2 = v6.keys();
					while( $it2.hasNext() ) {
						var k2 = $it2.next();
						var id = Reflect.field(k2,"__id__");
						Reflect.deleteField(k2,"__id__");
						this.serialize(k2);
						k2.__id__ = id;
						this.serialize(v6.h[k2.__id__]);
					}
					this.buf.b += "h";
					break;
				case haxe_io_Bytes:
					var v7 = v;
					var i2 = 0;
					var max = v7.length - 2;
					var charsBuf = new StringBuf();
					var b64 = haxe_Serializer.BASE64;
					while(i2 < max) {
						var b1 = v7.get(i2++);
						var b2 = v7.get(i2++);
						var b3 = v7.get(i2++);
						charsBuf.add(b64.charAt(b1 >> 2));
						charsBuf.add(b64.charAt((b1 << 4 | b2 >> 4) & 63));
						charsBuf.add(b64.charAt((b2 << 2 | b3 >> 6) & 63));
						charsBuf.add(b64.charAt(b3 & 63));
					}
					if(i2 == max) {
						var b11 = v7.get(i2++);
						var b21 = v7.get(i2++);
						charsBuf.add(b64.charAt(b11 >> 2));
						charsBuf.add(b64.charAt((b11 << 4 | b21 >> 4) & 63));
						charsBuf.add(b64.charAt(b21 << 2 & 63));
					} else if(i2 == max + 1) {
						var b12 = v7.get(i2++);
						charsBuf.add(b64.charAt(b12 >> 2));
						charsBuf.add(b64.charAt(b12 << 4 & 63));
					}
					var chars = charsBuf.b;
					this.buf.b += "s";
					if(chars.length == null) this.buf.b += "null"; else this.buf.b += "" + chars.length;
					this.buf.b += ":";
					if(chars == null) this.buf.b += "null"; else this.buf.b += "" + chars;
					break;
				default:
					if(this.useCache) this.cache.pop();
					if(v.hxSerialize != null) {
						this.buf.b += "C";
						this.serializeString(Type.getClassName(c));
						if(this.useCache) this.cache.push(v);
						v.hxSerialize(this);
						this.buf.b += "g";
					} else {
						this.buf.b += "c";
						this.serializeString(Type.getClassName(c));
						if(this.useCache) this.cache.push(v);
						this.serializeFields(v);
					}
				}
				break;
			case 4:
				if(js_Boot.__instanceof(v,Class)) {
					var className = Type.getClassName(v);
					this.buf.b += "A";
					this.serializeString(className);
				} else if(js_Boot.__instanceof(v,Enum)) {
					this.buf.b += "B";
					this.serializeString(Type.getEnumName(v));
				} else {
					if(this.useCache && this.serializeRef(v)) return;
					this.buf.b += "o";
					this.serializeFields(v);
				}
				break;
			case 7:
				var e = _g[2];
				if(this.useCache) {
					if(this.serializeRef(v)) return;
					this.cache.pop();
				}
				if(this.useEnumIndex) this.buf.b += "j"; else this.buf.b += "w";
				this.serializeString(Type.getEnumName(e));
				if(this.useEnumIndex) {
					this.buf.b += ":";
					this.buf.b += Std.string(v[1]);
				} else this.serializeString(v[0]);
				this.buf.b += ":";
				var l1 = v.length;
				this.buf.b += Std.string(l1 - 2);
				var _g11 = 2;
				while(_g11 < l1) {
					var i3 = _g11++;
					this.serialize(v[i3]);
				}
				if(this.useCache) this.cache.push(v);
				break;
			case 5:
				throw new js__$Boot_HaxeError("Cannot serialize function");
				break;
			default:
				throw new js__$Boot_HaxeError("Cannot serialize " + Std.string(v));
			}
		}
	}
	,__class__: haxe_Serializer
};
var haxe_Timer = function() { };
$hxClasses["haxe.Timer"] = haxe_Timer;
haxe_Timer.__name__ = ["haxe","Timer"];
haxe_Timer.stamp = function() {
	return new Date().getTime() / 1000;
};
var haxe_Unserializer = function(buf) {
	this.buf = buf;
	this.length = buf.length;
	this.pos = 0;
	this.scache = [];
	this.cache = [];
	var r = haxe_Unserializer.DEFAULT_RESOLVER;
	if(r == null) {
		r = Type;
		haxe_Unserializer.DEFAULT_RESOLVER = r;
	}
	this.setResolver(r);
};
$hxClasses["haxe.Unserializer"] = haxe_Unserializer;
haxe_Unserializer.__name__ = ["haxe","Unserializer"];
haxe_Unserializer.initCodes = function() {
	var codes = [];
	var _g1 = 0;
	var _g = haxe_Unserializer.BASE64.length;
	while(_g1 < _g) {
		var i = _g1++;
		codes[haxe_Unserializer.BASE64.charCodeAt(i)] = i;
	}
	return codes;
};
haxe_Unserializer.run = function(v) {
	return new haxe_Unserializer(v).unserialize();
};
haxe_Unserializer.prototype = {
	setResolver: function(r) {
		if(r == null) this.resolver = { resolveClass : function(_) {
			return null;
		}, resolveEnum : function(_1) {
			return null;
		}}; else this.resolver = r;
	}
	,get: function(p) {
		return this.buf.charCodeAt(p);
	}
	,readDigits: function() {
		var k = 0;
		var s = false;
		var fpos = this.pos;
		while(true) {
			var c = this.buf.charCodeAt(this.pos);
			if(c != c) break;
			if(c == 45) {
				if(this.pos != fpos) break;
				s = true;
				this.pos++;
				continue;
			}
			if(c < 48 || c > 57) break;
			k = k * 10 + (c - 48);
			this.pos++;
		}
		if(s) k *= -1;
		return k;
	}
	,readFloat: function() {
		var p1 = this.pos;
		while(true) {
			var c = this.buf.charCodeAt(this.pos);
			if(c >= 43 && c < 58 || c == 101 || c == 69) this.pos++; else break;
		}
		return Std.parseFloat(HxOverrides.substr(this.buf,p1,this.pos - p1));
	}
	,unserializeObject: function(o) {
		while(true) {
			if(this.pos >= this.length) throw new js__$Boot_HaxeError("Invalid object");
			if(this.buf.charCodeAt(this.pos) == 103) break;
			var k = this.unserialize();
			if(!(typeof(k) == "string")) throw new js__$Boot_HaxeError("Invalid object key");
			var v = this.unserialize();
			o[k] = v;
		}
		this.pos++;
	}
	,unserializeEnum: function(edecl,tag) {
		if(this.get(this.pos++) != 58) throw new js__$Boot_HaxeError("Invalid enum format");
		var nargs = this.readDigits();
		if(nargs == 0) return Type.createEnum(edecl,tag);
		var args = [];
		while(nargs-- > 0) args.push(this.unserialize());
		return Type.createEnum(edecl,tag,args);
	}
	,unserialize: function() {
		var _g = this.get(this.pos++);
		switch(_g) {
		case 110:
			return null;
		case 116:
			return true;
		case 102:
			return false;
		case 122:
			return 0;
		case 105:
			return this.readDigits();
		case 100:
			return this.readFloat();
		case 121:
			var len = this.readDigits();
			if(this.get(this.pos++) != 58 || this.length - this.pos < len) throw new js__$Boot_HaxeError("Invalid string length");
			var s = HxOverrides.substr(this.buf,this.pos,len);
			this.pos += len;
			s = decodeURIComponent(s.split("+").join(" "));
			this.scache.push(s);
			return s;
		case 107:
			return NaN;
		case 109:
			return -Infinity;
		case 112:
			return Infinity;
		case 97:
			var buf = this.buf;
			var a = [];
			this.cache.push(a);
			while(true) {
				var c = this.buf.charCodeAt(this.pos);
				if(c == 104) {
					this.pos++;
					break;
				}
				if(c == 117) {
					this.pos++;
					var n = this.readDigits();
					a[a.length + n - 1] = null;
				} else a.push(this.unserialize());
			}
			return a;
		case 111:
			var o = { };
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 114:
			var n1 = this.readDigits();
			if(n1 < 0 || n1 >= this.cache.length) throw new js__$Boot_HaxeError("Invalid reference");
			return this.cache[n1];
		case 82:
			var n2 = this.readDigits();
			if(n2 < 0 || n2 >= this.scache.length) throw new js__$Boot_HaxeError("Invalid string reference");
			return this.scache[n2];
		case 120:
			throw new js__$Boot_HaxeError(this.unserialize());
			break;
		case 99:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw new js__$Boot_HaxeError("Class not found " + name);
			var o1 = Type.createEmptyInstance(cl);
			this.cache.push(o1);
			this.unserializeObject(o1);
			return o1;
		case 119:
			var name1 = this.unserialize();
			var edecl = this.resolver.resolveEnum(name1);
			if(edecl == null) throw new js__$Boot_HaxeError("Enum not found " + name1);
			var e = this.unserializeEnum(edecl,this.unserialize());
			this.cache.push(e);
			return e;
		case 106:
			var name2 = this.unserialize();
			var edecl1 = this.resolver.resolveEnum(name2);
			if(edecl1 == null) throw new js__$Boot_HaxeError("Enum not found " + name2);
			this.pos++;
			var index = this.readDigits();
			var tag = Type.getEnumConstructs(edecl1)[index];
			if(tag == null) throw new js__$Boot_HaxeError("Unknown enum index " + name2 + "@" + index);
			var e1 = this.unserializeEnum(edecl1,tag);
			this.cache.push(e1);
			return e1;
		case 108:
			var l = new List();
			this.cache.push(l);
			var buf1 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) l.add(this.unserialize());
			this.pos++;
			return l;
		case 98:
			var h = new haxe_ds_StringMap();
			this.cache.push(h);
			var buf2 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) {
				var s1 = this.unserialize();
				h.set(s1,this.unserialize());
			}
			this.pos++;
			return h;
		case 113:
			var h1 = new haxe_ds_IntMap();
			this.cache.push(h1);
			var buf3 = this.buf;
			var c1 = this.get(this.pos++);
			while(c1 == 58) {
				var i = this.readDigits();
				h1.set(i,this.unserialize());
				c1 = this.get(this.pos++);
			}
			if(c1 != 104) throw new js__$Boot_HaxeError("Invalid IntMap format");
			return h1;
		case 77:
			var h2 = new haxe_ds_ObjectMap();
			this.cache.push(h2);
			var buf4 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) {
				var s2 = this.unserialize();
				h2.set(s2,this.unserialize());
			}
			this.pos++;
			return h2;
		case 118:
			var d;
			if(this.buf.charCodeAt(this.pos) >= 48 && this.buf.charCodeAt(this.pos) <= 57 && this.buf.charCodeAt(this.pos + 1) >= 48 && this.buf.charCodeAt(this.pos + 1) <= 57 && this.buf.charCodeAt(this.pos + 2) >= 48 && this.buf.charCodeAt(this.pos + 2) <= 57 && this.buf.charCodeAt(this.pos + 3) >= 48 && this.buf.charCodeAt(this.pos + 3) <= 57 && this.buf.charCodeAt(this.pos + 4) == 45) {
				var s3 = HxOverrides.substr(this.buf,this.pos,19);
				d = HxOverrides.strDate(s3);
				this.pos += 19;
			} else {
				var t = this.readFloat();
				var d1 = new Date();
				d1.setTime(t);
				d = d1;
			}
			this.cache.push(d);
			return d;
		case 115:
			var len1 = this.readDigits();
			var buf5 = this.buf;
			if(this.get(this.pos++) != 58 || this.length - this.pos < len1) throw new js__$Boot_HaxeError("Invalid bytes length");
			var codes = haxe_Unserializer.CODES;
			if(codes == null) {
				codes = haxe_Unserializer.initCodes();
				haxe_Unserializer.CODES = codes;
			}
			var i1 = this.pos;
			var rest = len1 & 3;
			var size;
			size = (len1 >> 2) * 3 + (rest >= 2?rest - 1:0);
			var max = i1 + (len1 - rest);
			var bytes = haxe_io_Bytes.alloc(size);
			var bpos = 0;
			while(i1 < max) {
				var c11 = codes[StringTools.fastCodeAt(buf5,i1++)];
				var c2 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c11 << 2 | c2 >> 4);
				var c3 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c2 << 4 | c3 >> 2);
				var c4 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c3 << 6 | c4);
			}
			if(rest >= 2) {
				var c12 = codes[StringTools.fastCodeAt(buf5,i1++)];
				var c21 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c12 << 2 | c21 >> 4);
				if(rest == 3) {
					var c31 = codes[StringTools.fastCodeAt(buf5,i1++)];
					bytes.set(bpos++,c21 << 4 | c31 >> 2);
				}
			}
			this.pos += len1;
			this.cache.push(bytes);
			return bytes;
		case 67:
			var name3 = this.unserialize();
			var cl1 = this.resolver.resolveClass(name3);
			if(cl1 == null) throw new js__$Boot_HaxeError("Class not found " + name3);
			var o2 = Type.createEmptyInstance(cl1);
			this.cache.push(o2);
			o2.hxUnserialize(this);
			if(this.get(this.pos++) != 103) throw new js__$Boot_HaxeError("Invalid custom data");
			return o2;
		case 65:
			var name4 = this.unserialize();
			var cl2 = this.resolver.resolveClass(name4);
			if(cl2 == null) throw new js__$Boot_HaxeError("Class not found " + name4);
			return cl2;
		case 66:
			var name5 = this.unserialize();
			var e2 = this.resolver.resolveEnum(name5);
			if(e2 == null) throw new js__$Boot_HaxeError("Enum not found " + name5);
			return e2;
		default:
		}
		this.pos--;
		throw new js__$Boot_HaxeError("Invalid char " + this.buf.charAt(this.pos) + " at position " + this.pos);
	}
	,__class__: haxe_Unserializer
};
var haxe_ds_BalancedTree = function() {
};
$hxClasses["haxe.ds.BalancedTree"] = haxe_ds_BalancedTree;
haxe_ds_BalancedTree.__name__ = ["haxe","ds","BalancedTree"];
haxe_ds_BalancedTree.prototype = {
	set: function(key,value) {
		this.root = this.setLoop(key,value,this.root);
	}
	,get: function(key) {
		var node = this.root;
		while(node != null) {
			var c = this.compare(key,node.key);
			if(c == 0) return node.value;
			if(c < 0) node = node.left; else node = node.right;
		}
		return null;
	}
	,remove: function(key) {
		try {
			this.root = this.removeLoop(key,this.root);
			return true;
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			if( js_Boot.__instanceof(e,String) ) {
				return false;
			} else throw(e);
		}
	}
	,exists: function(key) {
		var node = this.root;
		while(node != null) {
			var c = this.compare(key,node.key);
			if(c == 0) return true; else if(c < 0) node = node.left; else node = node.right;
		}
		return false;
	}
	,iterator: function() {
		var ret = [];
		this.iteratorLoop(this.root,ret);
		return HxOverrides.iter(ret);
	}
	,keys: function() {
		var ret = [];
		this.keysLoop(this.root,ret);
		return HxOverrides.iter(ret);
	}
	,setLoop: function(k,v,node) {
		if(node == null) return new haxe_ds_TreeNode(null,k,v,null);
		var c = this.compare(k,node.key);
		if(c == 0) return new haxe_ds_TreeNode(node.left,k,v,node.right,node == null?0:node._height); else if(c < 0) {
			var nl = this.setLoop(k,v,node.left);
			return this.balance(nl,node.key,node.value,node.right);
		} else {
			var nr = this.setLoop(k,v,node.right);
			return this.balance(node.left,node.key,node.value,nr);
		}
	}
	,removeLoop: function(k,node) {
		if(node == null) throw new js__$Boot_HaxeError("Not_found");
		var c = this.compare(k,node.key);
		if(c == 0) return this.merge(node.left,node.right); else if(c < 0) return this.balance(this.removeLoop(k,node.left),node.key,node.value,node.right); else return this.balance(node.left,node.key,node.value,this.removeLoop(k,node.right));
	}
	,iteratorLoop: function(node,acc) {
		if(node != null) {
			this.iteratorLoop(node.left,acc);
			acc.push(node.value);
			this.iteratorLoop(node.right,acc);
		}
	}
	,keysLoop: function(node,acc) {
		if(node != null) {
			this.keysLoop(node.left,acc);
			acc.push(node.key);
			this.keysLoop(node.right,acc);
		}
	}
	,merge: function(t1,t2) {
		if(t1 == null) return t2;
		if(t2 == null) return t1;
		var t = this.minBinding(t2);
		return this.balance(t1,t.key,t.value,this.removeMinBinding(t2));
	}
	,minBinding: function(t) {
		if(t == null) throw new js__$Boot_HaxeError("Not_found"); else if(t.left == null) return t; else return this.minBinding(t.left);
	}
	,removeMinBinding: function(t) {
		if(t.left == null) return t.right; else return this.balance(this.removeMinBinding(t.left),t.key,t.value,t.right);
	}
	,balance: function(l,k,v,r) {
		var hl;
		if(l == null) hl = 0; else hl = l._height;
		var hr;
		if(r == null) hr = 0; else hr = r._height;
		if(hl > hr + 2) {
			if((function($this) {
				var $r;
				var _this = l.left;
				$r = _this == null?0:_this._height;
				return $r;
			}(this)) >= (function($this) {
				var $r;
				var _this1 = l.right;
				$r = _this1 == null?0:_this1._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(l.left,l.key,l.value,new haxe_ds_TreeNode(l.right,k,v,r)); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l.left,l.key,l.value,l.right.left),l.right.key,l.right.value,new haxe_ds_TreeNode(l.right.right,k,v,r));
		} else if(hr > hl + 2) {
			if((function($this) {
				var $r;
				var _this2 = r.right;
				$r = _this2 == null?0:_this2._height;
				return $r;
			}(this)) > (function($this) {
				var $r;
				var _this3 = r.left;
				$r = _this3 == null?0:_this3._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left),r.key,r.value,r.right); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left.left),r.left.key,r.left.value,new haxe_ds_TreeNode(r.left.right,r.key,r.value,r.right));
		} else return new haxe_ds_TreeNode(l,k,v,r,(hl > hr?hl:hr) + 1);
	}
	,compare: function(k1,k2) {
		return Reflect.compare(k1,k2);
	}
	,__class__: haxe_ds_BalancedTree
};
var haxe_ds_TreeNode = function(l,k,v,r,h) {
	if(h == null) h = -1;
	this.left = l;
	this.key = k;
	this.value = v;
	this.right = r;
	if(h == -1) this._height = ((function($this) {
		var $r;
		var _this = $this.left;
		$r = _this == null?0:_this._height;
		return $r;
	}(this)) > (function($this) {
		var $r;
		var _this1 = $this.right;
		$r = _this1 == null?0:_this1._height;
		return $r;
	}(this))?(function($this) {
		var $r;
		var _this2 = $this.left;
		$r = _this2 == null?0:_this2._height;
		return $r;
	}(this)):(function($this) {
		var $r;
		var _this3 = $this.right;
		$r = _this3 == null?0:_this3._height;
		return $r;
	}(this))) + 1; else this._height = h;
};
$hxClasses["haxe.ds.TreeNode"] = haxe_ds_TreeNode;
haxe_ds_TreeNode.__name__ = ["haxe","ds","TreeNode"];
haxe_ds_TreeNode.prototype = {
	__class__: haxe_ds_TreeNode
};
var haxe_ds_EnumValueMap = function() {
	haxe_ds_BalancedTree.call(this);
};
$hxClasses["haxe.ds.EnumValueMap"] = haxe_ds_EnumValueMap;
haxe_ds_EnumValueMap.__name__ = ["haxe","ds","EnumValueMap"];
haxe_ds_EnumValueMap.__interfaces__ = [haxe_IMap];
haxe_ds_EnumValueMap.__super__ = haxe_ds_BalancedTree;
haxe_ds_EnumValueMap.prototype = $extend(haxe_ds_BalancedTree.prototype,{
	compare: function(k1,k2) {
		var d = k1[1] - k2[1];
		if(d != 0) return d;
		var p1 = k1.slice(2);
		var p2 = k2.slice(2);
		if(p1.length == 0 && p2.length == 0) return 0;
		return this.compareArgs(p1,p2);
	}
	,compareArgs: function(a1,a2) {
		var ld = a1.length - a2.length;
		if(ld != 0) return ld;
		var _g1 = 0;
		var _g = a1.length;
		while(_g1 < _g) {
			var i = _g1++;
			var d = this.compareArg(a1[i],a2[i]);
			if(d != 0) return d;
		}
		return 0;
	}
	,compareArg: function(v1,v2) {
		if(Reflect.isEnumValue(v1) && Reflect.isEnumValue(v2)) return this.compare(v1,v2); else if((v1 instanceof Array) && v1.__enum__ == null && ((v2 instanceof Array) && v2.__enum__ == null)) return this.compareArgs(v1,v2); else return Reflect.compare(v1,v2);
	}
	,__class__: haxe_ds_EnumValueMap
});
var haxe_ds_IntMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.IntMap"] = haxe_ds_IntMap;
haxe_ds_IntMap.__name__ = ["haxe","ds","IntMap"];
haxe_ds_IntMap.__interfaces__ = [haxe_IMap];
haxe_ds_IntMap.prototype = {
	set: function(key,value) {
		this.h[key] = value;
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key | 0);
		}
		return HxOverrides.iter(a);
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i];
		}};
	}
	,__class__: haxe_ds_IntMap
};
var haxe_ds_ObjectMap = function() {
	this.h = { };
	this.h.__keys__ = { };
};
$hxClasses["haxe.ds.ObjectMap"] = haxe_ds_ObjectMap;
haxe_ds_ObjectMap.__name__ = ["haxe","ds","ObjectMap"];
haxe_ds_ObjectMap.__interfaces__ = [haxe_IMap];
haxe_ds_ObjectMap.prototype = {
	set: function(key,value) {
		var id = key.__id__ || (key.__id__ = ++haxe_ds_ObjectMap.count);
		this.h[id] = value;
		this.h.__keys__[id] = key;
	}
	,keys: function() {
		var a = [];
		for( var key in this.h.__keys__ ) {
		if(this.h.hasOwnProperty(key)) a.push(this.h.__keys__[key]);
		}
		return HxOverrides.iter(a);
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i.__id__];
		}};
	}
	,__class__: haxe_ds_ObjectMap
};
var haxe_ds__$StringMap_StringMapIterator = function(map,keys) {
	this.map = map;
	this.keys = keys;
	this.index = 0;
	this.count = keys.length;
};
$hxClasses["haxe.ds._StringMap.StringMapIterator"] = haxe_ds__$StringMap_StringMapIterator;
haxe_ds__$StringMap_StringMapIterator.__name__ = ["haxe","ds","_StringMap","StringMapIterator"];
haxe_ds__$StringMap_StringMapIterator.prototype = {
	hasNext: function() {
		return this.index < this.count;
	}
	,next: function() {
		return this.map.get(this.keys[this.index++]);
	}
	,__class__: haxe_ds__$StringMap_StringMapIterator
};
var haxe_ds_StringMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.StringMap"] = haxe_ds_StringMap;
haxe_ds_StringMap.__name__ = ["haxe","ds","StringMap"];
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,exists: function(key) {
		if(__map_reserved[key] != null) return this.existsReserved(key);
		return this.h.hasOwnProperty(key);
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,existsReserved: function(key) {
		if(this.rh == null) return false;
		return this.rh.hasOwnProperty("$" + key);
	}
	,keys: function() {
		var _this = this.arrayKeys();
		return HxOverrides.iter(_this);
	}
	,arrayKeys: function() {
		var out = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) out.push(key);
		}
		if(this.rh != null) {
			for( var key in this.rh ) {
			if(key.charCodeAt(0) == 36) out.push(key.substr(1));
			}
		}
		return out;
	}
	,iterator: function() {
		return new haxe_ds__$StringMap_StringMapIterator(this,this.arrayKeys());
	}
	,__class__: haxe_ds_StringMap
};
var haxe_io_Bytes = function(data) {
	this.length = data.byteLength;
	this.b = new Uint8Array(data);
	this.b.bufferValue = data;
	data.hxBytes = this;
	data.bytes = this.b;
};
$hxClasses["haxe.io.Bytes"] = haxe_io_Bytes;
haxe_io_Bytes.__name__ = ["haxe","io","Bytes"];
haxe_io_Bytes.alloc = function(length) {
	return new haxe_io_Bytes(new ArrayBuffer(length));
};
haxe_io_Bytes.ofData = function(b) {
	var hb = b.hxBytes;
	if(hb != null) return hb;
	return new haxe_io_Bytes(b);
};
haxe_io_Bytes.prototype = {
	get: function(pos) {
		return this.b[pos];
	}
	,set: function(pos,v) {
		this.b[pos] = v & 255;
	}
	,blit: function(pos,src,srcpos,len) {
		if(pos < 0 || srcpos < 0 || len < 0 || pos + len > this.length || srcpos + len > src.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
		if(srcpos == 0 && len == src.length) this.b.set(src.b,pos); else this.b.set(src.b.subarray(srcpos,srcpos + len),pos);
	}
	,getString: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
		var s = "";
		var b = this.b;
		var fcc = String.fromCharCode;
		var i = pos;
		var max = pos + len;
		while(i < max) {
			var c = b[i++];
			if(c < 128) {
				if(c == 0) break;
				s += fcc(c);
			} else if(c < 224) s += fcc((c & 63) << 6 | b[i++] & 127); else if(c < 240) {
				var c2 = b[i++];
				s += fcc((c & 31) << 12 | (c2 & 127) << 6 | b[i++] & 127);
			} else {
				var c21 = b[i++];
				var c3 = b[i++];
				var u = (c & 15) << 18 | (c21 & 127) << 12 | (c3 & 127) << 6 | b[i++] & 127;
				s += fcc((u >> 10) + 55232);
				s += fcc(u & 1023 | 56320);
			}
		}
		return s;
	}
	,toString: function() {
		return this.getString(0,this.length);
	}
	,__class__: haxe_io_Bytes
};
var haxe_io_BytesBuffer = function() {
	this.b = [];
};
$hxClasses["haxe.io.BytesBuffer"] = haxe_io_BytesBuffer;
haxe_io_BytesBuffer.__name__ = ["haxe","io","BytesBuffer"];
haxe_io_BytesBuffer.prototype = {
	addBytes: function(src,pos,len) {
		if(pos < 0 || len < 0 || pos + len > src.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
		var b1 = this.b;
		var b2 = src.b;
		var _g1 = pos;
		var _g = pos + len;
		while(_g1 < _g) {
			var i = _g1++;
			this.b.push(b2[i]);
		}
	}
	,getBytes: function() {
		var bytes = new haxe_io_Bytes(new Uint8Array(this.b).buffer);
		this.b = null;
		return bytes;
	}
	,__class__: haxe_io_BytesBuffer
};
var haxe_io_BytesInput = function(b,pos,len) {
	if(pos == null) pos = 0;
	if(len == null) len = b.length - pos;
	if(pos < 0 || len < 0 || pos + len > b.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
	this.b = b.b;
	this.pos = pos;
	this.len = len;
	this.totlen = len;
};
$hxClasses["haxe.io.BytesInput"] = haxe_io_BytesInput;
haxe_io_BytesInput.__name__ = ["haxe","io","BytesInput"];
haxe_io_BytesInput.__super__ = haxe_io_Input;
haxe_io_BytesInput.prototype = $extend(haxe_io_Input.prototype,{
	readByte: function() {
		if(this.len == 0) throw new js__$Boot_HaxeError(new haxe_io_Eof());
		this.len--;
		return this.b[this.pos++];
	}
	,readBytes: function(buf,pos,len) {
		if(pos < 0 || len < 0 || pos + len > buf.length) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
		if(this.len == 0 && len > 0) throw new js__$Boot_HaxeError(new haxe_io_Eof());
		if(this.len < len) len = this.len;
		var b1 = this.b;
		var b2 = buf.b;
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			b2[pos + i] = b1[this.pos + i];
		}
		this.pos += len;
		this.len -= len;
		return len;
	}
	,__class__: haxe_io_BytesInput
});
var haxe_io_Eof = function() {
};
$hxClasses["haxe.io.Eof"] = haxe_io_Eof;
haxe_io_Eof.__name__ = ["haxe","io","Eof"];
haxe_io_Eof.prototype = {
	toString: function() {
		return "Eof";
	}
	,__class__: haxe_io_Eof
};
var haxe_io_Error = $hxClasses["haxe.io.Error"] = { __ename__ : ["haxe","io","Error"], __constructs__ : ["Blocked","Overflow","OutsideBounds","Custom"] };
haxe_io_Error.Blocked = ["Blocked",0];
haxe_io_Error.Blocked.toString = $estr;
haxe_io_Error.Blocked.__enum__ = haxe_io_Error;
haxe_io_Error.Overflow = ["Overflow",1];
haxe_io_Error.Overflow.toString = $estr;
haxe_io_Error.Overflow.__enum__ = haxe_io_Error;
haxe_io_Error.OutsideBounds = ["OutsideBounds",2];
haxe_io_Error.OutsideBounds.toString = $estr;
haxe_io_Error.OutsideBounds.__enum__ = haxe_io_Error;
haxe_io_Error.Custom = function(e) { var $x = ["Custom",3,e]; $x.__enum__ = haxe_io_Error; $x.toString = $estr; return $x; };
haxe_io_Error.__empty_constructs__ = [haxe_io_Error.Blocked,haxe_io_Error.Overflow,haxe_io_Error.OutsideBounds];
var haxe_io_FPHelper = function() { };
$hxClasses["haxe.io.FPHelper"] = haxe_io_FPHelper;
haxe_io_FPHelper.__name__ = ["haxe","io","FPHelper"];
haxe_io_FPHelper.i32ToFloat = function(i) {
	var sign = 1 - (i >>> 31 << 1);
	var exp = i >>> 23 & 255;
	var sig = i & 8388607;
	if(sig == 0 && exp == 0) return 0.0;
	return sign * (1 + Math.pow(2,-23) * sig) * Math.pow(2,exp - 127);
};
haxe_io_FPHelper.floatToI32 = function(f) {
	if(f == 0) return 0;
	var af;
	if(f < 0) af = -f; else af = f;
	var exp = Math.floor(Math.log(af) / 0.6931471805599453);
	if(exp < -127) exp = -127; else if(exp > 128) exp = 128;
	var sig = Math.round((af / Math.pow(2,exp) - 1) * 8388608) & 8388607;
	return (f < 0?-2147483648:0) | exp + 127 << 23 | sig;
};
haxe_io_FPHelper.i64ToDouble = function(low,high) {
	var sign = 1 - (high >>> 31 << 1);
	var exp = (high >> 20 & 2047) - 1023;
	var sig = (high & 1048575) * 4294967296. + (low >>> 31) * 2147483648. + (low & 2147483647);
	if(sig == 0 && exp == -1023) return 0.0;
	return sign * (1.0 + Math.pow(2,-52) * sig) * Math.pow(2,exp);
};
haxe_io_FPHelper.doubleToI64 = function(v) {
	var i64 = haxe_io_FPHelper.i64tmp;
	if(v == 0) {
		i64.low = 0;
		i64.high = 0;
	} else {
		var av;
		if(v < 0) av = -v; else av = v;
		var exp = Math.floor(Math.log(av) / 0.6931471805599453);
		var sig;
		var v1 = (av / Math.pow(2,exp) - 1) * 4503599627370496.;
		sig = Math.round(v1);
		var sig_l = sig | 0;
		var sig_h = sig / 4294967296.0 | 0;
		i64.low = sig_l;
		i64.high = (v < 0?-2147483648:0) | exp + 1023 << 20 | sig_h;
	}
	return i64;
};
var haxe_xml__$Fast_NodeAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.NodeAccess"] = haxe_xml__$Fast_NodeAccess;
haxe_xml__$Fast_NodeAccess.__name__ = ["haxe","xml","_Fast","NodeAccess"];
haxe_xml__$Fast_NodeAccess.prototype = {
	__class__: haxe_xml__$Fast_NodeAccess
};
var haxe_xml__$Fast_AttribAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.AttribAccess"] = haxe_xml__$Fast_AttribAccess;
haxe_xml__$Fast_AttribAccess.__name__ = ["haxe","xml","_Fast","AttribAccess"];
haxe_xml__$Fast_AttribAccess.prototype = {
	__class__: haxe_xml__$Fast_AttribAccess
};
var haxe_xml__$Fast_HasAttribAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.HasAttribAccess"] = haxe_xml__$Fast_HasAttribAccess;
haxe_xml__$Fast_HasAttribAccess.__name__ = ["haxe","xml","_Fast","HasAttribAccess"];
haxe_xml__$Fast_HasAttribAccess.prototype = {
	__class__: haxe_xml__$Fast_HasAttribAccess
};
var haxe_xml__$Fast_HasNodeAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.HasNodeAccess"] = haxe_xml__$Fast_HasNodeAccess;
haxe_xml__$Fast_HasNodeAccess.__name__ = ["haxe","xml","_Fast","HasNodeAccess"];
haxe_xml__$Fast_HasNodeAccess.prototype = {
	__class__: haxe_xml__$Fast_HasNodeAccess
};
var haxe_xml__$Fast_NodeListAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.NodeListAccess"] = haxe_xml__$Fast_NodeListAccess;
haxe_xml__$Fast_NodeListAccess.__name__ = ["haxe","xml","_Fast","NodeListAccess"];
haxe_xml__$Fast_NodeListAccess.prototype = {
	__class__: haxe_xml__$Fast_NodeListAccess
};
var haxe_xml_Fast = function(x) {
	if(x.nodeType != Xml.Document && x.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Invalid nodeType " + x.nodeType);
	this.x = x;
	this.node = new haxe_xml__$Fast_NodeAccess(x);
	this.nodes = new haxe_xml__$Fast_NodeListAccess(x);
	this.att = new haxe_xml__$Fast_AttribAccess(x);
	this.has = new haxe_xml__$Fast_HasAttribAccess(x);
	this.hasNode = new haxe_xml__$Fast_HasNodeAccess(x);
};
$hxClasses["haxe.xml.Fast"] = haxe_xml_Fast;
haxe_xml_Fast.__name__ = ["haxe","xml","Fast"];
haxe_xml_Fast.prototype = {
	get_name: function() {
		if(this.x.nodeType == Xml.Document) return "Document"; else return this.x.get_nodeName();
	}
	,get_innerData: function() {
		var it = this.x.iterator();
		if(!it.hasNext()) throw new js__$Boot_HaxeError(this.get_name() + " does not have data");
		var v = it.next();
		var n = it.next();
		if(n != null) {
			if(v.nodeType == Xml.PCData && n.nodeType == Xml.CData && StringTools.trim((function($this) {
				var $r;
				if(v.nodeType == Xml.Document || v.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + v.nodeType);
				$r = v.nodeValue;
				return $r;
			}(this))) == "") {
				var n2 = it.next();
				if(n2 == null || n2.nodeType == Xml.PCData && StringTools.trim((function($this) {
					var $r;
					if(n2.nodeType == Xml.Document || n2.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + n2.nodeType);
					$r = n2.nodeValue;
					return $r;
				}(this))) == "" && it.next() == null) {
					if(n.nodeType == Xml.Document || n.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + n.nodeType);
					return n.nodeValue;
				}
			}
			throw new js__$Boot_HaxeError(this.get_name() + " does not only have data");
		}
		if(v.nodeType != Xml.PCData && v.nodeType != Xml.CData) throw new js__$Boot_HaxeError(this.get_name() + " does not have data");
		if(v.nodeType == Xml.Document || v.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + v.nodeType);
		return v.nodeValue;
	}
	,get_elements: function() {
		var it = this.x.elements();
		return { hasNext : $bind(it,it.hasNext), next : function() {
			var x = it.next();
			if(x == null) return null;
			return new haxe_xml_Fast(x);
		}};
	}
	,__class__: haxe_xml_Fast
};
var haxe_xml_Parser = function() { };
$hxClasses["haxe.xml.Parser"] = haxe_xml_Parser;
haxe_xml_Parser.__name__ = ["haxe","xml","Parser"];
haxe_xml_Parser.parse = function(str,strict) {
	if(strict == null) strict = false;
	var doc = Xml.createDocument();
	haxe_xml_Parser.doParse(str,strict,0,doc);
	return doc;
};
haxe_xml_Parser.doParse = function(str,strict,p,parent) {
	if(p == null) p = 0;
	var xml = null;
	var state = 1;
	var next = 1;
	var aname = null;
	var start = 0;
	var nsubs = 0;
	var nbrackets = 0;
	var c = str.charCodeAt(p);
	var buf = new StringBuf();
	var escapeNext = 1;
	var attrValQuote = -1;
	while(!(c != c)) {
		switch(state) {
		case 0:
			switch(c) {
			case 10:case 13:case 9:case 32:
				break;
			default:
				state = next;
				continue;
			}
			break;
		case 1:
			switch(c) {
			case 60:
				state = 0;
				next = 2;
				break;
			default:
				start = p;
				state = 13;
				continue;
			}
			break;
		case 13:
			if(c == 60) {
				buf.addSub(str,start,p - start);
				var child = Xml.createPCData(buf.b);
				buf = new StringBuf();
				parent.addChild(child);
				nsubs++;
				state = 0;
				next = 2;
			} else if(c == 38) {
				buf.addSub(str,start,p - start);
				state = 18;
				escapeNext = 13;
				start = p + 1;
			}
			break;
		case 17:
			if(c == 93 && str.charCodeAt(p + 1) == 93 && str.charCodeAt(p + 2) == 62) {
				var child1 = Xml.createCData(HxOverrides.substr(str,start,p - start));
				parent.addChild(child1);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 2:
			switch(c) {
			case 33:
				if(str.charCodeAt(p + 1) == 91) {
					p += 2;
					if(HxOverrides.substr(str,p,6).toUpperCase() != "CDATA[") throw new js__$Boot_HaxeError("Expected <![CDATA[");
					p += 5;
					state = 17;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) == 68 || str.charCodeAt(p + 1) == 100) {
					if(HxOverrides.substr(str,p + 2,6).toUpperCase() != "OCTYPE") throw new js__$Boot_HaxeError("Expected <!DOCTYPE");
					p += 8;
					state = 16;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) != 45 || str.charCodeAt(p + 2) != 45) throw new js__$Boot_HaxeError("Expected <!--"); else {
					p += 2;
					state = 15;
					start = p + 1;
				}
				break;
			case 63:
				state = 14;
				start = p;
				break;
			case 47:
				if(parent == null) throw new js__$Boot_HaxeError("Expected node name");
				start = p + 1;
				state = 0;
				next = 10;
				break;
			default:
				state = 3;
				start = p;
				continue;
			}
			break;
		case 3:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(p == start) throw new js__$Boot_HaxeError("Expected node name");
				xml = Xml.createElement(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml);
				nsubs++;
				state = 0;
				next = 4;
				continue;
			}
			break;
		case 4:
			switch(c) {
			case 47:
				state = 11;
				break;
			case 62:
				state = 9;
				break;
			default:
				state = 5;
				start = p;
				continue;
			}
			break;
		case 5:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				var tmp;
				if(start == p) throw new js__$Boot_HaxeError("Expected attribute name");
				tmp = HxOverrides.substr(str,start,p - start);
				aname = tmp;
				if(xml.exists(aname)) throw new js__$Boot_HaxeError("Duplicate attribute");
				state = 0;
				next = 6;
				continue;
			}
			break;
		case 6:
			switch(c) {
			case 61:
				state = 0;
				next = 7;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected =");
			}
			break;
		case 7:
			switch(c) {
			case 34:case 39:
				buf = new StringBuf();
				state = 8;
				start = p + 1;
				attrValQuote = c;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected \"");
			}
			break;
		case 8:
			switch(c) {
			case 38:
				buf.addSub(str,start,p - start);
				state = 18;
				escapeNext = 8;
				start = p + 1;
				break;
			case 62:
				if(strict) throw new js__$Boot_HaxeError("Invalid unescaped " + String.fromCharCode(c) + " in attribute value"); else if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val = buf.b;
					buf = new StringBuf();
					xml.set(aname,val);
					state = 0;
					next = 4;
				}
				break;
			case 60:
				if(strict) throw new js__$Boot_HaxeError("Invalid unescaped " + String.fromCharCode(c) + " in attribute value"); else if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val1 = buf.b;
					buf = new StringBuf();
					xml.set(aname,val1);
					state = 0;
					next = 4;
				}
				break;
			default:
				if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val2 = buf.b;
					buf = new StringBuf();
					xml.set(aname,val2);
					state = 0;
					next = 4;
				}
			}
			break;
		case 9:
			p = haxe_xml_Parser.doParse(str,strict,p,xml);
			start = p;
			state = 1;
			break;
		case 11:
			switch(c) {
			case 62:
				state = 1;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected >");
			}
			break;
		case 12:
			switch(c) {
			case 62:
				if(nsubs == 0) parent.addChild(Xml.createPCData(""));
				return p;
			default:
				throw new js__$Boot_HaxeError("Expected >");
			}
			break;
		case 10:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(start == p) throw new js__$Boot_HaxeError("Expected node name");
				var v = HxOverrides.substr(str,start,p - start);
				if(v != (function($this) {
					var $r;
					if(parent.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + parent.nodeType);
					$r = parent.nodeName;
					return $r;
				}(this))) throw new js__$Boot_HaxeError("Expected </" + (function($this) {
					var $r;
					if(parent.nodeType != Xml.Element) throw "Bad node type, expected Element but found " + parent.nodeType;
					$r = parent.nodeName;
					return $r;
				}(this)) + ">");
				state = 0;
				next = 12;
				continue;
			}
			break;
		case 15:
			if(c == 45 && str.charCodeAt(p + 1) == 45 && str.charCodeAt(p + 2) == 62) {
				var xml1 = Xml.createComment(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml1);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 16:
			if(c == 91) nbrackets++; else if(c == 93) nbrackets--; else if(c == 62 && nbrackets == 0) {
				var xml2 = Xml.createDocType(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml2);
				nsubs++;
				state = 1;
			}
			break;
		case 14:
			if(c == 63 && str.charCodeAt(p + 1) == 62) {
				p++;
				var str1 = HxOverrides.substr(str,start + 1,p - start - 2);
				var xml3 = Xml.createProcessingInstruction(str1);
				parent.addChild(xml3);
				nsubs++;
				state = 1;
			}
			break;
		case 18:
			if(c == 59) {
				var s = HxOverrides.substr(str,start,p - start);
				if(s.charCodeAt(0) == 35) {
					var c1;
					if(s.charCodeAt(1) == 120) c1 = Std.parseInt("0" + HxOverrides.substr(s,1,s.length - 1)); else c1 = Std.parseInt(HxOverrides.substr(s,1,s.length - 1));
					buf.b += String.fromCharCode(c1);
				} else if(!haxe_xml_Parser.escapes.exists(s)) {
					if(strict) throw new js__$Boot_HaxeError("Undefined entity: " + s);
					buf.b += Std.string("&" + s + ";");
				} else buf.add(haxe_xml_Parser.escapes.get(s));
				start = p + 1;
				state = escapeNext;
			} else if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45) && c != 35) {
				if(strict) throw new js__$Boot_HaxeError("Invalid character in entity: " + String.fromCharCode(c));
				buf.b += "&";
				buf.addSub(str,start,p - start);
				p--;
				start = p + 1;
				state = escapeNext;
			}
			break;
		}
		c = StringTools.fastCodeAt(str,++p);
	}
	if(state == 1) {
		start = p;
		state = 13;
	}
	if(state == 13) {
		if(p != start || nsubs == 0) {
			buf.addSub(str,start,p - start);
			var xml4 = Xml.createPCData(buf.b);
			parent.addChild(xml4);
			nsubs++;
		}
		return p;
	}
	if(!strict && state == 18 && escapeNext == 13) {
		buf.b += "&";
		buf.addSub(str,start,p - start);
		var xml5 = Xml.createPCData(buf.b);
		parent.addChild(xml5);
		nsubs++;
		return p;
	}
	throw new js__$Boot_HaxeError("Unexpected end");
};
var haxe_xml_Printer = function(pretty) {
	this.output = new StringBuf();
	this.pretty = pretty;
};
$hxClasses["haxe.xml.Printer"] = haxe_xml_Printer;
haxe_xml_Printer.__name__ = ["haxe","xml","Printer"];
haxe_xml_Printer.print = function(xml,pretty) {
	if(pretty == null) pretty = false;
	var printer = new haxe_xml_Printer(pretty);
	printer.writeNode(xml,"");
	return printer.output.b;
};
haxe_xml_Printer.prototype = {
	writeNode: function(value,tabs) {
		var _g = value.nodeType;
		switch(_g) {
		case 2:
			this.output.b += Std.string(tabs + "<![CDATA[");
			this.write(StringTools.trim((function($this) {
				var $r;
				if(value.nodeType == Xml.Document || value.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + value.nodeType);
				$r = value.nodeValue;
				return $r;
			}(this))));
			this.output.b += "]]>";
			if(this.pretty) this.output.b += "";
			break;
		case 3:
			var commentContent;
			if(value.nodeType == Xml.Document || value.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + value.nodeType);
			commentContent = value.nodeValue;
			commentContent = new EReg("[\n\r\t]+","g").replace(commentContent,"");
			commentContent = "<!--" + commentContent + "-->";
			if(tabs == null) this.output.b += "null"; else this.output.b += "" + tabs;
			this.write(StringTools.trim(commentContent));
			if(this.pretty) this.output.b += "";
			break;
		case 6:
			var $it0 = (function($this) {
				var $r;
				if(value.nodeType != Xml.Document && value.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + value.nodeType);
				$r = HxOverrides.iter(value.children);
				return $r;
			}(this));
			while( $it0.hasNext() ) {
				var child = $it0.next();
				this.writeNode(child,tabs);
			}
			break;
		case 0:
			this.output.b += Std.string(tabs + "<");
			this.write((function($this) {
				var $r;
				if(value.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + value.nodeType);
				$r = value.nodeName;
				return $r;
			}(this)));
			var $it1 = value.attributes();
			while( $it1.hasNext() ) {
				var attribute = $it1.next();
				this.output.b += Std.string(" " + attribute + "=\"");
				this.write(StringTools.htmlEscape(value.get(attribute),true));
				this.output.b += "\"";
			}
			if(this.hasChildren(value)) {
				this.output.b += ">";
				if(this.pretty) this.output.b += "";
				var $it2 = (function($this) {
					var $r;
					if(value.nodeType != Xml.Document && value.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + value.nodeType);
					$r = HxOverrides.iter(value.children);
					return $r;
				}(this));
				while( $it2.hasNext() ) {
					var child1 = $it2.next();
					this.writeNode(child1,this.pretty?tabs + "\t":tabs);
				}
				this.output.b += Std.string(tabs + "</");
				this.write((function($this) {
					var $r;
					if(value.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + value.nodeType);
					$r = value.nodeName;
					return $r;
				}(this)));
				this.output.b += ">";
				if(this.pretty) this.output.b += "";
			} else {
				this.output.b += "/>";
				if(this.pretty) this.output.b += "";
			}
			break;
		case 1:
			var nodeValue;
			if(value.nodeType == Xml.Document || value.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + value.nodeType);
			nodeValue = value.nodeValue;
			if(nodeValue.length != 0) {
				this.write(tabs + StringTools.htmlEscape(nodeValue));
				if(this.pretty) this.output.b += "";
			}
			break;
		case 5:
			this.write("<?" + (function($this) {
				var $r;
				if(value.nodeType == Xml.Document || value.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + value.nodeType);
				$r = value.nodeValue;
				return $r;
			}(this)) + "?>");
			break;
		case 4:
			this.write("<!DOCTYPE " + (function($this) {
				var $r;
				if(value.nodeType == Xml.Document || value.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + value.nodeType);
				$r = value.nodeValue;
				return $r;
			}(this)) + ">");
			break;
		}
	}
	,write: function(input) {
		if(input == null) this.output.b += "null"; else this.output.b += "" + input;
	}
	,hasChildren: function(value) {
		var $it0 = (function($this) {
			var $r;
			if(value.nodeType != Xml.Document && value.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + value.nodeType);
			$r = HxOverrides.iter(value.children);
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var child = $it0.next();
			var _g = child.nodeType;
			switch(_g) {
			case 0:case 1:
				return true;
			case 2:case 3:
				if(StringTools.ltrim((function($this) {
					var $r;
					if(child.nodeType == Xml.Document || child.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + child.nodeType);
					$r = child.nodeValue;
					return $r;
				}(this))).length != 0) return true;
				break;
			default:
			}
		}
		return false;
	}
	,__class__: haxe_xml_Printer
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
$hxClasses["js._Boot.HaxeError"] = js__$Boot_HaxeError;
js__$Boot_HaxeError.__name__ = ["js","_Boot","HaxeError"];
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
$hxClasses["js.Boot"] = js_Boot;
js_Boot.__name__ = ["js","Boot"];
js_Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
};
js_Boot.__trace = function(v,i) {
	var msg;
	if(i != null) msg = i.fileName + ":" + i.lineNumber + ": "; else msg = "";
	msg += js_Boot.__string_rec(v,"");
	if(i != null && i.customParams != null) {
		var _g = 0;
		var _g1 = i.customParams;
		while(_g < _g1.length) {
			var v1 = _g1[_g];
			++_g;
			msg += "," + js_Boot.__string_rec(v1,"");
		}
	}
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js_Boot.__unhtml(msg) + "<br/>"; else if(typeof console != "undefined" && console.log != null) console.log(msg);
};
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return $global[name];
};
var js_Browser = function() { };
$hxClasses["js.Browser"] = js_Browser;
js_Browser.__name__ = ["js","Browser"];
js_Browser.createXMLHttpRequest = function() {
	if(typeof XMLHttpRequest != "undefined") return new XMLHttpRequest();
	if(typeof ActiveXObject != "undefined") return new ActiveXObject("Microsoft.XMLHTTP");
	throw new js__$Boot_HaxeError("Unable to create XMLHttpRequest object.");
};
var js_html_compat_ArrayBuffer = function(a) {
	if((a instanceof Array) && a.__enum__ == null) {
		this.a = a;
		this.byteLength = a.length;
	} else {
		var len = a;
		this.a = [];
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			this.a[i] = 0;
		}
		this.byteLength = len;
	}
};
$hxClasses["js.html.compat.ArrayBuffer"] = js_html_compat_ArrayBuffer;
js_html_compat_ArrayBuffer.__name__ = ["js","html","compat","ArrayBuffer"];
js_html_compat_ArrayBuffer.sliceImpl = function(begin,end) {
	var u = new Uint8Array(this,begin,end == null?null:end - begin);
	var result = new ArrayBuffer(u.byteLength);
	var resultArray = new Uint8Array(result);
	resultArray.set(u);
	return result;
};
js_html_compat_ArrayBuffer.prototype = {
	slice: function(begin,end) {
		return new js_html_compat_ArrayBuffer(this.a.slice(begin,end));
	}
	,__class__: js_html_compat_ArrayBuffer
};
var js_html_compat_DataView = function(buffer,byteOffset,byteLength) {
	this.buf = buffer;
	if(byteOffset == null) this.offset = 0; else this.offset = byteOffset;
	if(byteLength == null) this.length = buffer.byteLength - this.offset; else this.length = byteLength;
	if(this.offset < 0 || this.length < 0 || this.offset + this.length > buffer.byteLength) throw new js__$Boot_HaxeError(haxe_io_Error.OutsideBounds);
};
$hxClasses["js.html.compat.DataView"] = js_html_compat_DataView;
js_html_compat_DataView.__name__ = ["js","html","compat","DataView"];
js_html_compat_DataView.prototype = {
	getInt8: function(byteOffset) {
		var v = this.buf.a[this.offset + byteOffset];
		if(v >= 128) return v - 256; else return v;
	}
	,getUint8: function(byteOffset) {
		return this.buf.a[this.offset + byteOffset];
	}
	,getInt16: function(byteOffset,littleEndian) {
		var v = this.getUint16(byteOffset,littleEndian);
		if(v >= 32768) return v - 65536; else return v;
	}
	,getUint16: function(byteOffset,littleEndian) {
		if(littleEndian) return this.buf.a[this.offset + byteOffset] | this.buf.a[this.offset + byteOffset + 1] << 8; else return this.buf.a[this.offset + byteOffset] << 8 | this.buf.a[this.offset + byteOffset + 1];
	}
	,getInt32: function(byteOffset,littleEndian) {
		var p = this.offset + byteOffset;
		var a = this.buf.a[p++];
		var b = this.buf.a[p++];
		var c = this.buf.a[p++];
		var d = this.buf.a[p++];
		if(littleEndian) return a | b << 8 | c << 16 | d << 24; else return d | c << 8 | b << 16 | a << 24;
	}
	,getUint32: function(byteOffset,littleEndian) {
		var v = this.getInt32(byteOffset,littleEndian);
		if(v < 0) return v + 4294967296.; else return v;
	}
	,getFloat32: function(byteOffset,littleEndian) {
		return haxe_io_FPHelper.i32ToFloat(this.getInt32(byteOffset,littleEndian));
	}
	,getFloat64: function(byteOffset,littleEndian) {
		var a = this.getInt32(byteOffset,littleEndian);
		var b = this.getInt32(byteOffset + 4,littleEndian);
		return haxe_io_FPHelper.i64ToDouble(littleEndian?a:b,littleEndian?b:a);
	}
	,setInt8: function(byteOffset,value) {
		if(value < 0) this.buf.a[byteOffset + this.offset] = value + 128 & 255; else this.buf.a[byteOffset + this.offset] = value & 255;
	}
	,setUint8: function(byteOffset,value) {
		this.buf.a[byteOffset + this.offset] = value & 255;
	}
	,setInt16: function(byteOffset,value,littleEndian) {
		this.setUint16(byteOffset,value < 0?value + 65536:value,littleEndian);
	}
	,setUint16: function(byteOffset,value,littleEndian) {
		var p = byteOffset + this.offset;
		if(littleEndian) {
			this.buf.a[p] = value & 255;
			this.buf.a[p++] = value >> 8 & 255;
		} else {
			this.buf.a[p++] = value >> 8 & 255;
			this.buf.a[p] = value & 255;
		}
	}
	,setInt32: function(byteOffset,value,littleEndian) {
		this.setUint32(byteOffset,value,littleEndian);
	}
	,setUint32: function(byteOffset,value,littleEndian) {
		var p = byteOffset + this.offset;
		if(littleEndian) {
			this.buf.a[p++] = value & 255;
			this.buf.a[p++] = value >> 8 & 255;
			this.buf.a[p++] = value >> 16 & 255;
			this.buf.a[p++] = value >>> 24;
		} else {
			this.buf.a[p++] = value >>> 24;
			this.buf.a[p++] = value >> 16 & 255;
			this.buf.a[p++] = value >> 8 & 255;
			this.buf.a[p++] = value & 255;
		}
	}
	,setFloat32: function(byteOffset,value,littleEndian) {
		this.setUint32(byteOffset,haxe_io_FPHelper.floatToI32(value),littleEndian);
	}
	,setFloat64: function(byteOffset,value,littleEndian) {
		var i64 = haxe_io_FPHelper.doubleToI64(value);
		if(littleEndian) {
			this.setUint32(byteOffset,i64.low);
			this.setUint32(byteOffset,i64.high);
		} else {
			this.setUint32(byteOffset,i64.high);
			this.setUint32(byteOffset,i64.low);
		}
	}
	,__class__: js_html_compat_DataView
};
var js_html_compat_Uint8Array = function() { };
$hxClasses["js.html.compat.Uint8Array"] = js_html_compat_Uint8Array;
js_html_compat_Uint8Array.__name__ = ["js","html","compat","Uint8Array"];
js_html_compat_Uint8Array._new = function(arg1,offset,length) {
	var arr;
	if(typeof(arg1) == "number") {
		arr = [];
		var _g = 0;
		while(_g < arg1) {
			var i = _g++;
			arr[i] = 0;
		}
		arr.byteLength = arr.length;
		arr.byteOffset = 0;
		arr.buffer = new js_html_compat_ArrayBuffer(arr);
	} else if(js_Boot.__instanceof(arg1,js_html_compat_ArrayBuffer)) {
		var buffer = arg1;
		if(offset == null) offset = 0;
		if(length == null) length = buffer.byteLength - offset;
		if(offset == 0) arr = buffer.a; else arr = buffer.a.slice(offset,offset + length);
		arr.byteLength = arr.length;
		arr.byteOffset = offset;
		arr.buffer = buffer;
	} else if((arg1 instanceof Array) && arg1.__enum__ == null) {
		arr = arg1.slice();
		arr.byteLength = arr.length;
		arr.byteOffset = 0;
		arr.buffer = new js_html_compat_ArrayBuffer(arr);
	} else throw new js__$Boot_HaxeError("TODO " + Std.string(arg1));
	arr.subarray = js_html_compat_Uint8Array._subarray;
	arr.set = js_html_compat_Uint8Array._set;
	return arr;
};
js_html_compat_Uint8Array._set = function(arg,offset) {
	var t = this;
	if(js_Boot.__instanceof(arg.buffer,js_html_compat_ArrayBuffer)) {
		var a = arg;
		if(arg.byteLength + offset > t.byteLength) throw new js__$Boot_HaxeError("set() outside of range");
		var _g1 = 0;
		var _g = arg.byteLength;
		while(_g1 < _g) {
			var i = _g1++;
			t[i + offset] = a[i];
		}
	} else if((arg instanceof Array) && arg.__enum__ == null) {
		var a1 = arg;
		if(a1.length + offset > t.byteLength) throw new js__$Boot_HaxeError("set() outside of range");
		var _g11 = 0;
		var _g2 = a1.length;
		while(_g11 < _g2) {
			var i1 = _g11++;
			t[i1 + offset] = a1[i1];
		}
	} else throw new js__$Boot_HaxeError("TODO");
};
js_html_compat_Uint8Array._subarray = function(start,end) {
	var t = this;
	var a = js_html_compat_Uint8Array._new(t.slice(start,end));
	a.byteOffset = start;
	return a;
};
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; }
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
if(Array.prototype.indexOf) HxOverrides.indexOf = function(a,o,i) {
	return Array.prototype.indexOf.call(a,o,i);
};
$hxClasses.Math = Math;
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
$hxClasses.Array = Array;
Array.__name__ = ["Array"];
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = $hxClasses.Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
if(Array.prototype.filter == null) Array.prototype.filter = function(f1) {
	var a1 = [];
	var _g11 = 0;
	var _g2 = this.length;
	while(_g11 < _g2) {
		var i1 = _g11++;
		var e = this[i1];
		if(f1(e)) a1.push(e);
	}
	return a1;
};
var __map_reserved = {}
var ArrayBuffer = $global.ArrayBuffer || js_html_compat_ArrayBuffer;
if(ArrayBuffer.prototype.slice == null) ArrayBuffer.prototype.slice = js_html_compat_ArrayBuffer.sliceImpl;
var DataView = $global.DataView || js_html_compat_DataView;
var Uint8Array = $global.Uint8Array || js_html_compat_Uint8Array._new;
Xml.Element = 0;
Xml.PCData = 1;
Xml.CData = 2;
Xml.Comment = 3;
Xml.DocType = 4;
Xml.ProcessingInstruction = 5;
Xml.Document = 6;
com_oddcast_app_FrameUpdate.NOT_YET_INITIATED = 5;
com_oddcast_app_js_sitepalFullbodyLite_AudioManager.NO_AUDIO_TO_RETURN_PROGRESS = -1.0;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_undefined = -1;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_model = 0;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_costume = 1;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_mouth = 2;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_hair = 3;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_fhair = 4;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_hat = 5;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_necklace = 6;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_glasses = 7;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_bottom = 8;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_shoes = 9;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_props = 10;
com_oddcast_app_vhss_$extraction_AccTypeTools.ACC_INDEX_TOTAL = 11;
com_oddcast_app_vhss_$extraction_EngineV5Constants.VERSION = 1.1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.VERSION_INFO = "JS engine";
com_oddcast_app_vhss_$extraction_EngineV5Constants.DEFAULT_FPS = 30;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AUDIO_FPS = 12;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ATTACH_MODEL_TIMEOUT = 80;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AGE_FRAMES = 45;
com_oddcast_app_vhss_$extraction_EngineV5Constants.MOUTH_FRAMES = 16;
com_oddcast_app_vhss_$extraction_EngineV5Constants.MOUTH_FRAMES_OLD = 12;
com_oddcast_app_vhss_$extraction_EngineV5Constants.HEAD_Y_MOVEMENT_DURING_SPEECH_RATIO = 0.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.HEAD_X_MOVEMENT_DURING_SPEECH_RATIO = 0.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.HEAD_NEW_GAZE_DURING_SPEECH_COUNT = 30;
com_oddcast_app_vhss_$extraction_EngineV5Constants.MOUSE_IDLE_RECENTER_TIME = 2500;
com_oddcast_app_vhss_$extraction_EngineV5Constants.DECIMAL_PLACES_OF_MODEL_MCS = 2;
com_oddcast_app_vhss_$extraction_EngineV5Constants.POSSIBLY_MISSING_MODEL_MCS = ["browl","browr","hairl","hair_r"];
com_oddcast_app_vhss_$extraction_EngineV5Constants.ENGINE_SOUND_UPDATE_INTERVAL = 40;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ENGINE_SOUND_SAMPLE_RATE_RATIO = 0.25;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ENGINE_SOUND_WORD_END_PERCENT = 0.25;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_X_BOUND = 300;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_Y_BOUND = 100;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_CRACK = 0.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_SPEED = 8;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_BREATH_SPEED = 0.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_BREATH_SIGN = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_DEFAULT_SCALE = 100;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_DIFF = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_MAX_X = 27;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_MIN_X = 20;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_MAX_Y = 6;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_MIN_Y = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.ANIM_EYE_CENTER = 31;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EM = 0.67;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EM_RATIO = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_MM_RATIO = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_BM_RATIO = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_WHITE_LINE_COMPENSATION = 0.8;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_WHITE_LIVE_PIXEL_COMPENSATION = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_RECIPROCAL_SCALING = false;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_SCALING = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_RESTRICT_TURNING = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_XSCALE_FACTOR = 30;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_BACK_HAIR_X_DAMPEN = 5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_BACK_HAIR_ROTATION_DAMPEN = 1.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_OVERALL_FACTOR = 0.5;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_HEAD_MOVE_FACTOR = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_YSCALE_FACTOR = 8;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_FEATURE_MOVE_FACTOR = 0.35;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_EM_RATIO = 2;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_BM_RATIO = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_NM_RATIO = 2;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_MM_RATIO = 2;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_NOSE_SCALE_RATIO = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_NOSE_ROTATION_FACTOR = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_Y_EYE_SCALE = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_MAX_X = 12;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_MAX_Y = 3;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_X_FACTOR = 25;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_Y_FACTOR = 13;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_ELLIPTIC_MOVE = -1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_GLINT_MOVE = -1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_PUPIL = 85;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_EYE_PUPIL_SCALE = 60;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_FACTOR = -1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_LEFT_SCALE = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_RIGHT_SCALE = 1;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_LEFT_POS = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_RIGHT_POS = 0;
com_oddcast_app_vhss_$extraction_EngineV5Constants.AP_ANIM_GLASSES_Y_EYES_OFFSET = 0;
com_oddcast_app_vhss_$extraction_VHSSAccessories.attachmentIndex = [com_oddcast_app_vhss_$extraction_AccFragType.Front,com_oddcast_app_vhss_$extraction_AccFragType.Back,com_oddcast_app_vhss_$extraction_AccFragType.Left,com_oddcast_app_vhss_$extraction_AccFragType.Right];
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.CANVAS_SIZE_UNITY = 512;
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.B_FROZEN_LAST_FRAME = true;
com_oddcast_app_vhss_$extraction_VHSSSitePalFullBodyEngine.DEFAULT_HOST_SCALE = 1.8;
com_oddcast_io_archive_iArchive_ArchiveLiteAbstract.USER_ABORTED = "UserAborted";
com_oddcast_app_vhss_$extraction_VHSSHostApp.DEFAULT_NORMALIZED_FACEHEIGHT = 300;
com_oddcast_app_vhss_$extraction_VHSSHostApp.createArchive = function(url,osType,archiveLoadStats) {
	return new com_oddcast_io_archive_iArchive_ArchiveLiteDirectory(url,osType,archiveLoadStats);
};
com_oddcast_app_vhss_$extraction_VHSSHostApp.SMILE_VISEME_FRAME_0b = 9;
com_oddcast_io_xmlIO_XmlIOAbstract.__rtti = "<class path=\"com.oddcast.io.xmlIO.XmlIOAbstract\" params=\"\" module=\"com.oddcast.io.xmlIO.IXmlIO\">\n\t<implements path=\"com.oddcast.io.xmlIO.IXmlIO\"/>\n\t<saved public=\"1\" set=\"method\" line=\"50\"><f a=\"archive\">\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchive\"/>\n\t<x path=\"Void\"/>\n</f></saved>\n\t<loaded public=\"1\" set=\"method\" line=\"51\"><f a=\"allAssetsLoaded:archive:xmlName\">\n\t<c path=\"com.oddcast.io.xmlIO.AllAssetsLoaded\"><c path=\"com.oddcast.io.xmlIO.IXmlIO\"/></c>\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchiveReadOnly\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></loaded>\n\t<allAssetsLoaded public=\"1\" set=\"method\" line=\"52\"><f a=\"\"><x path=\"Void\"/></f></allAssetsLoaded>\n\t<meta><m n=\":rtti\"/></meta>\n</class>";
com_oddcast_io_xmlIO_XmlIOVersioned.__rtti = "<class path=\"com.oddcast.io.xmlIO.XmlIOVersioned\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOAbstract\"/>\n\t<setVersion public=\"1\" set=\"method\" line=\"30\"><f a=\"ver\">\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></setVersion>\n\t<loaded public=\"1\" set=\"method\" line=\"32\" override=\"1\"><f a=\"allAssetsLoaded:archive:xmlName\">\n\t<c path=\"com.oddcast.io.xmlIO.AllAssetsLoaded\"><c path=\"com.oddcast.io.xmlIO.IXmlIO\"/></c>\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchiveReadOnly\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></loaded>\n\t<parse public=\"1\" set=\"method\" line=\"43\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<version_ public=\"1\"><x path=\"Int\"/></version_>\n</class>";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.animation.EngineConstantValues\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<PARAM_em_ public=\"1\" get=\"inline\" set=\"null\" line=\"35\" static=\"1\"><c path=\"String\"/></PARAM_em_>\n\t<PARAM_EyeMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"36\" static=\"1\"><c path=\"String\"/></PARAM_EyeMovementRatio_>\n\t<PARAM_MouthMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"37\" static=\"1\"><c path=\"String\"/></PARAM_MouthMovementRatio_>\n\t<PARAM_BrowMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"38\" static=\"1\"><c path=\"String\"/></PARAM_BrowMovementRatio_>\n\t<PARAM_BackHairXDampen_ public=\"1\" get=\"inline\" set=\"null\" line=\"39\" static=\"1\"><c path=\"String\"/></PARAM_BackHairXDampen_>\n\t<PARAM_BackHairRotationDampen_ public=\"1\" get=\"inline\" set=\"null\" line=\"40\" static=\"1\"><c path=\"String\"/></PARAM_BackHairRotationDampen_>\n\t<PARAM_YOverallFactor_ public=\"1\" get=\"inline\" set=\"null\" line=\"42\" static=\"1\"><c path=\"String\"/></PARAM_YOverallFactor_>\n\t<PARAM_YScaleFactor_ public=\"1\" get=\"inline\" set=\"null\" line=\"43\" static=\"1\"><c path=\"String\"/></PARAM_YScaleFactor_>\n\t<PARAM_YFeatureMovementFactor_ public=\"1\" get=\"inline\" set=\"null\" line=\"44\" static=\"1\"><c path=\"String\"/></PARAM_YFeatureMovementFactor_>\n\t<PARAM_YEyeMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"45\" static=\"1\"><c path=\"String\"/></PARAM_YEyeMovementRatio_>\n\t<PARAM_YBrowMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"46\" static=\"1\"><c path=\"String\"/></PARAM_YBrowMovementRatio_>\n\t<PARAM_YNoseMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"47\" static=\"1\"><c path=\"String\"/></PARAM_YNoseMovementRatio_>\n\t<PARAM_YMouthMovementRatio_ public=\"1\" get=\"inline\" set=\"null\" line=\"48\" static=\"1\"><c path=\"String\"/></PARAM_YMouthMovementRatio_>\n\t<PARAM_YNoseScaleFactor_ public=\"1\" get=\"inline\" set=\"null\" line=\"49\" static=\"1\"><c path=\"String\"/></PARAM_YNoseScaleFactor_>\n\t<PARAM_YeyeScale_ public=\"1\" get=\"inline\" set=\"null\" line=\"50\" static=\"1\"><c path=\"String\"/></PARAM_YeyeScale_>\n\t<PARAM_yOffsetEyes_ public=\"1\" get=\"inline\" set=\"null\" line=\"51\" static=\"1\"><c path=\"String\"/></PARAM_yOffsetEyes_>\n\t<PARAM_YHeadMoveFactor_ public=\"1\" get=\"inline\" set=\"null\" line=\"52\" static=\"1\"><c path=\"String\"/></PARAM_YHeadMoveFactor_>\n\t<PARAM_xBound_ public=\"1\" get=\"inline\" set=\"null\" line=\"54\" static=\"1\"><c path=\"String\"/></PARAM_xBound_>\n\t<PARAM_yBound_ public=\"1\" get=\"inline\" set=\"null\" line=\"55\" static=\"1\"><c path=\"String\"/></PARAM_yBound_>\n\t<PARAM_eyeMaxX_ public=\"1\" get=\"inline\" set=\"null\" line=\"57\" static=\"1\"><c path=\"String\"/></PARAM_eyeMaxX_>\n\t<PARAM_eyeMaxY_ public=\"1\" get=\"inline\" set=\"null\" line=\"58\" static=\"1\"><c path=\"String\"/></PARAM_eyeMaxY_>\n\t<PARAM_eyeFactorX_ public=\"1\" get=\"inline\" set=\"null\" line=\"59\" static=\"1\"><c path=\"String\"/></PARAM_eyeFactorX_>\n\t<PARAM_eyeFactorY_ public=\"1\" get=\"inline\" set=\"null\" line=\"60\" static=\"1\"><c path=\"String\"/></PARAM_eyeFactorY_>\n\t<PARAM_r_iris_y_ public=\"1\" get=\"inline\" set=\"null\" line=\"62\" static=\"1\"><c path=\"String\"/></PARAM_r_iris_y_>\n\t<PARAM_r_iris_x_ public=\"1\" get=\"inline\" set=\"null\" line=\"63\" static=\"1\"><c path=\"String\"/></PARAM_r_iris_x_>\n\t<PARAM_WhiteLineCompensation_ public=\"1\" get=\"inline\" set=\"null\" line=\"65\" static=\"1\"><c path=\"String\"/></PARAM_WhiteLineCompensation_>\n\t<PARAM_RestrictTotalTurning_ public=\"1\" get=\"inline\" set=\"null\" line=\"66\" static=\"1\"><c path=\"String\"/></PARAM_RestrictTotalTurning_>\n\t<parse public=\"1\" set=\"method\" line=\"113\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<getFloat public=\"1\" set=\"method\" line=\"123\"><f a=\"param\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</f></getFloat>\n\t<setFloat public=\"1\" set=\"method\" line=\"138\"><f a=\"param:value\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setFloat>\n\t<floatMap_ public=\"1\"><x path=\"Map\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</x></floatMap_>\n\t<defaultMap public=\"1\"><x path=\"Map\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</x></defaultMap>\n\t<dispose public=\"1\" set=\"method\" line=\"149\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<new public=\"1\" set=\"method\" line=\"70\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_em_ = "em";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_EyeMovementRatio_ = "EyeMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_MouthMovementRatio_ = "MouthMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_BrowMovementRatio_ = "BrowMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_BackHairXDampen_ = "BackHairXDampen";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_BackHairRotationDampen_ = "BackHairRotationDampen";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YOverallFactor_ = "YOverallFactor";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YScaleFactor_ = "YScaleFactor";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YFeatureMovementFactor_ = "YFeatureMovementFactor";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YEyeMovementRatio_ = "YEyeMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YBrowMovementRatio_ = "YBrowMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YNoseMovementRatio_ = "YNoseMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YMouthMovementRatio_ = "YMouthMovementRatio";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YNoseScaleFactor_ = "YNoseScaleFactor";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YeyeScale_ = "YeyeScale";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_yOffsetEyes_ = "yOffsetEyes";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_YHeadMoveFactor_ = "YHeadMoveFactor";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_xBound_ = "xBound";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_yBound_ = "yBound";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_eyeMaxX_ = "eyeMaxX";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_eyeMaxY_ = "eyeMaxY";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_eyeFactorX_ = "eyeFactorX";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_eyeFactorY_ = "eyeFactorY";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_r_iris_y_ = "r_iris_y";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_r_iris_x_ = "r_iris_x";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_WhiteLineCompensation_ = "WhiteLineCompensation";
com_oddcast_app_vhss_$extraction_animation_EngineConstantValues.PARAM_RestrictTotalTurning_ = "RestrictTotalTurning";
com_oddcast_app_vhss_$extraction_animation_Gaze.FOLLOW_STILL_MOUSE_MILLIS = 2500;
com_oddcast_app_vhss_$extraction_animation_Gaze.FOLLOW_NONE_BORED_MILLIS = 3000;
com_oddcast_app_vhss_$extraction_animation_Gaze.AWAKEN_AFTER_MOUSE_STILL_MILLIS = 3000;
com_oddcast_app_vhss_$extraction_animation_Gaze.NOD_DAMPEN = 4.0;
com_oddcast_app_vhss_$extraction_animation_Gaze.TURN_DAMPEN = 4.0;
com_oddcast_app_vhss_$extraction_animation_Gaze.TWIST_DAMPEN = 4.0;
com_oddcast_app_vhss_$extraction_animation_Gaze.DEFAULT_SPEECH_MOVEMENT = 80.0;
com_oddcast_app_vhss_$extraction_color_ColorBase.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.color.ColorBase\" params=\"\" module=\"com.oddcast.app.vhss_extraction.color.Coloring\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<FIRST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"126\" static=\"1\"><x path=\"Int\"/></FIRST_VERSION>\n\t<LATEST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"127\" static=\"1\"><x path=\"Int\"/></LATEST_VERSION>\n\t<COL_PART_INDEX_UNDEFINED public=\"1\" get=\"inline\" set=\"null\" line=\"129\" static=\"1\"><x path=\"Int\"/></COL_PART_INDEX_UNDEFINED>\n\t<COL_PART_INDEX_HAIR_HAIR public=\"1\" get=\"inline\" set=\"null\" line=\"131\" static=\"1\"><x path=\"Int\"/></COL_PART_INDEX_HAIR_HAIR>\n\t<COL_PART_INDEX_HAIR_FACIAL public=\"1\" get=\"inline\" set=\"null\" line=\"133\" static=\"1\"><x path=\"Int\"/></COL_PART_INDEX_HAIR_FACIAL>\n\t<COL_PART_INDEX_HAIR_BROW public=\"1\" get=\"inline\" set=\"null\" line=\"134\" static=\"1\"><x path=\"Int\"/></COL_PART_INDEX_HAIR_BROW>\n\t<parse public=\"1\" set=\"method\" line=\"141\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<sumColoring public=\"1\" set=\"method\" line=\"148\"><f a=\"rgbTotal:weight\">\n\t<c path=\"com.oddcast.app.vhss_extraction.color.RGB_Total\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></sumColoring>\n\t<adjustColorTransform public=\"1\" set=\"method\" line=\"156\"><f a=\"colorTransform\">\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n</f></adjustColorTransform>\n\t<toString public=\"1\" set=\"method\" line=\"174\"><f a=\"\"><c path=\"String\"/></f></toString>\n\t<baseColor_ public=\"1\"><t path=\"com.oddcast.util.UInt\"/></baseColor_>\n\t<baseWeight_ public=\"1\"><x path=\"Float\"/></baseWeight_>\n\t<averageColorBase public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.color.ColorBase\"/></averageColorBase>\n\t<new public=\"1\" set=\"method\" line=\"137\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_color_ColorBase.FIRST_VERSION = 1;
com_oddcast_app_vhss_$extraction_color_ColorBase.LATEST_VERSION = 1;
com_oddcast_app_vhss_$extraction_color_ColorBase.COL_PART_INDEX_UNDEFINED = 0;
com_oddcast_app_vhss_$extraction_color_ColorBase.COL_PART_INDEX_HAIR_HAIR = 1;
com_oddcast_app_vhss_$extraction_color_ColorBase.COL_PART_INDEX_HAIR_FACIAL = 3;
com_oddcast_app_vhss_$extraction_color_ColorBase.COL_PART_INDEX_HAIR_BROW = 4;
com_oddcast_app_vhss_$extraction_color_Coloring.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.color.Coloring\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<FIRST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"186\" static=\"1\"><x path=\"Int\"/></FIRST_VERSION>\n\t<LATEST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"187\" static=\"1\"><x path=\"Int\"/></LATEST_VERSION>\n\t<str2ColorGroup public=\"1\" set=\"method\" line=\"212\" static=\"1\"><f a=\"colorGroupStr\">\n\t<c path=\"String\"/>\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n</f></str2ColorGroup>\n\t<str2AlphaGroup public=\"1\" set=\"method\" line=\"217\" static=\"1\"><f a=\"alphaGroupStr\">\n\t<c path=\"String\"/>\n\t<e path=\"com.oddcast.app.vhss_extraction.color.AlphaGroup\"/>\n</f></str2AlphaGroup>\n\t<colorGroup_ public=\"1\" set=\"null\"><e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/></colorGroup_>\n\t<colorGroupStr public=\"1\"><c path=\"String\"/></colorGroupStr>\n\t<setGroupFromString public=\"1\" set=\"method\" line=\"194\"><f a=\"colorGroupStr\">\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></setGroupFromString>\n\t<parse public=\"1\" set=\"method\" line=\"200\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<toString public=\"1\" set=\"method\" line=\"208\"><f a=\"\"><c path=\"String\"/></f></toString>\n\t<new public=\"1\" set=\"method\" line=\"192\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_color_Coloring.FIRST_VERSION = 1;
com_oddcast_app_vhss_$extraction_color_Coloring.LATEST_VERSION = 1;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<FIRST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"45\" static=\"1\"><x path=\"Int\"/></FIRST_VERSION>\n\t<BLEND_MODE_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"46\" static=\"1\"><x path=\"Int\"/></BLEND_MODE_VERSION>\n\t<LATEST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"47\" static=\"1\"><x path=\"Int\"/></LATEST_VERSION>\n\t<DEBUG_DRAW_NORMAL public=\"1\" get=\"inline\" set=\"null\" line=\"757\" static=\"1\"><x path=\"Int\"/></DEBUG_DRAW_NORMAL>\n\t<DEBUG_DRAW_NOIMAGE public=\"1\" get=\"inline\" set=\"null\" line=\"758\" static=\"1\"><x path=\"Int\"/></DEBUG_DRAW_NOIMAGE>\n\t<DEBUG_DRAW_REG public=\"1\" get=\"inline\" set=\"null\" line=\"759\" static=\"1\"><x path=\"Int\"/></DEBUG_DRAW_REG>\n\t<DEBUG_DRAW_BOUND public=\"1\" get=\"inline\" set=\"null\" line=\"760\" static=\"1\"><x path=\"Int\"/></DEBUG_DRAW_BOUND>\n\t<DEBUG_RegColorNULL public=\"1\" get=\"inline\" set=\"null\" line=\"763\" static=\"1\"><x path=\"Int\"/></DEBUG_RegColorNULL>\n\t<addMatrix public=\"1\" set=\"method\" line=\"70\"><f a=\"name\">\n\t<c path=\"String\"/>\n\t<t path=\"com.oddcast.util.Matrix23\"/>\n</f></addMatrix>\n\t<setBaseMatrix public=\"1\" set=\"method\" line=\"80\"><f a=\"m\">\n\t<t path=\"com.oddcast.util.Matrix23\"/>\n\t<x path=\"Void\"/>\n</f></setBaseMatrix>\n\t<getBaseMatrix public=\"1\" set=\"method\" line=\"84\"><f a=\"\"><t path=\"com.oddcast.util.Matrix23\"/></f></getBaseMatrix>\n\t<getTotalMatrix public=\"1\" set=\"method\" line=\"86\"><f a=\"\"><t path=\"com.oddcast.util.Matrix23\"/></f></getTotalMatrix>\n\t<parse public=\"1\" set=\"method\" line=\"88\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<getTransformFrom public=\"1\" set=\"method\" line=\"128\"><f a=\"otherDA\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<t path=\"com.oddcast.util.Matrix23\"/>\n</f></getTransformFrom>\n\t<afterRead public=\"1\" set=\"method\" line=\"164\"><f a=\"afterReadData\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/>\n\t<x path=\"Void\"/>\n</f></afterRead>\n\t<setDepths public=\"1\" set=\"method\" line=\"167\"><f a=\"\"><x path=\"Void\"/></f></setDepths>\n\t<isSameColoringGroup public=\"1\" set=\"method\" line=\"179\"><f a=\"other\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<x path=\"Bool\"/>\n</f></isSameColoringGroup>\n\t<isOfColorGroup public=\"1\" set=\"method\" line=\"185\"><f a=\"cg\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<x path=\"Bool\"/>\n</f></isOfColorGroup>\n\t<sumColoring public=\"1\" set=\"method\" line=\"193\"><f a=\"cg:rgbTotals:?forceTrue:?usedColorIndex\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<t path=\"com.oddcast.app.vhss_extraction.color.RGB_Totals\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></sumColoring>\n\t<sumColoringGroup public=\"1\" set=\"method\" line=\"207\"><f a=\"rgbTotals:usedColorIndex\">\n\t<t path=\"com.oddcast.app.vhss_extraction.color.RGB_Totals\"/>\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></sumColoringGroup>\n\t<getColorAverageWeighting set=\"method\" line=\"219\"><f a=\"\"><x path=\"Float\"/></f></getColorAverageWeighting>\n\t<finalfixup public=\"1\" set=\"method\" line=\"223\"><f a=\"ev\">\n\t<t path=\"com.oddcast.app.vhss_extraction.animation.EV\"/>\n\t<x path=\"Void\"/>\n</f></finalfixup>\n\t<addChild public=\"1\" set=\"method\" line=\"225\"><f a=\"child:?pos\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></addChild>\n\t<addChildAfter public=\"1\" set=\"method\" line=\"236\"><f a=\"child:after\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<x path=\"Bool\"/>\n</f></addChildAfter>\n\t<switchChild public=\"1\" set=\"method\" line=\"248\"><f a=\"old:replace\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<x path=\"Bool\"/>\n</f></switchChild>\n\t<removeChild public=\"1\" set=\"method\" line=\"261\"><f a=\"rChild\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<x path=\"Void\"/>\n</f></removeChild>\n\t<removeChildren public=\"1\" set=\"method\" line=\"267\"><f a=\"\"><x path=\"Void\"/></f></removeChildren>\n\t<getChildByName public=\"1\" set=\"method\" line=\"273\"><f a=\"childName\">\n\t<c path=\"String\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n</f></getChildByName>\n\t<initDisplayTree public=\"1\" set=\"method\" line=\"280\"><f a=\"\"><x path=\"Void\"/></f></initDisplayTree>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"282\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<frameUpdate2 public=\"1\" set=\"method\" line=\"288\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate2>\n\t<pushMatrix set=\"method\" line=\"294\"><f a=\"canvas\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<t path=\"com.oddcast.util.Matrix23\"/>\n</f></pushMatrix>\n\t<popMatrix set=\"method\" line=\"304\"><f a=\"canvas\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<x path=\"Void\"/>\n</f></popMatrix>\n\t<draw public=\"1\" set=\"method\" line=\"313\"><f a=\"canvas:bDEBUG_DrawFlags\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<c path=\"com.oddcast.util.Flags\"/>\n\t<x path=\"Void\"/>\n</f></draw>\n\t<getCurrImage set=\"method\" line=\"339\"><f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/></f></getCurrImage>\n\t<drawImage set=\"method\" line=\"346\"><f a=\"canvas:bDEBUG_DrawFlags\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<c path=\"com.oddcast.util.Flags\"/>\n\t<x path=\"Void\"/>\n</f></drawImage>\n\t<getBoundsInParentSpace public=\"1\" set=\"method\" line=\"374\"><f a=\"?bRecursive\">\n\t<x path=\"Bool\"/>\n\t<t path=\"com.oddcast.util.Rectangle\"/>\n</f></getBoundsInParentSpace>\n\t<getBounds public=\"1\" set=\"method\" line=\"378\"><f a=\"?mat:?bRecursive\">\n\t<t path=\"com.oddcast.util.Matrix23\"/>\n\t<x path=\"Bool\"/>\n\t<t path=\"com.oddcast.util.Rectangle\"/>\n</f></getBounds>\n\t<getBoundsInRoot public=\"1\" set=\"method\" line=\"405\"><f a=\"?bAndChildren\">\n\t<x path=\"Bool\"/>\n\t<t path=\"com.oddcast.util.Rectangle\"/>\n</f></getBoundsInRoot>\n\t<toString public=\"1\" set=\"method\" line=\"413\"><f a=\"\"><c path=\"String\"/></f></toString>\n\t<hasImage public=\"1\" set=\"method\" line=\"457\"><f a=\"\"><x path=\"Bool\"/></f></hasImage>\n\t<loadFromSpriteSheet public=\"1\" set=\"method\" line=\"459\"><f a=\"spriteImages\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.SpriteImages\"/>\n\t<x path=\"Void\"/>\n</f></loadFromSpriteSheet>\n\t<effectGroupColors public=\"1\" set=\"method\" line=\"471\"><f a=\"groupColorDelta:groupAlpha:parentColorTransform:bUseColorBase:bOn:editable:applyImmediatly:?pad\">\n\t<t path=\"com.oddcast.app.vhss_extraction.color.GroupColorDelta\"/>\n\t<t path=\"com.oddcast.app.vhss_extraction.color.GroupAlpha\"/>\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></effectGroupColors>\n\t<isColorableOrAlphable public=\"1\" set=\"method\" line=\"507\"><f a=\"colorableGroup:alphableGroup:?bOn\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<e path=\"com.oddcast.app.vhss_extraction.color.AlphaGroup\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n</f></isColorableOrAlphable>\n\t<applyColorTransform public=\"1\" set=\"method\" line=\"528\"><f a=\"bUseColorBase:editable:applyImmediatly\">\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></applyColorTransform>\n\t<spanDisplay public=\"1\" set=\"method\" line=\"538\"><f a=\"cb\">\n\t<f a=\"\">\n\t\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t\t<x path=\"Bool\"/>\n\t</f>\n\t<x path=\"Bool\"/>\n</f></spanDisplay>\n\t<spanDisplayChildrenFirst public=\"1\" set=\"method\" line=\"547\"><f a=\"cb\">\n\t<f a=\"\">\n\t\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t\t<x path=\"Bool\"/>\n\t</f>\n\t<x path=\"Bool\"/>\n</f></spanDisplayChildrenFirst>\n\t<spanDisplayArraySafe public=\"1\" set=\"method\" line=\"558\"><f a=\"cb\">\n\t<f a=\"\">\n\t\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t\t<x path=\"Bool\"/>\n\t</f>\n\t<x path=\"Bool\"/>\n</f></spanDisplayArraySafe>\n\t<getRoot public=\"1\" set=\"method\" line=\"568\"><f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/></f></getRoot>\n\t<findDisplayAssetNamed public=\"1\" set=\"method\" line=\"574\"><f a=\"findName\">\n\t<c path=\"String\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n</f></findDisplayAssetNamed>\n\t<getMouse public=\"1\" set=\"method\" line=\"586\"><f a=\"rootMouse\">\n\t<t path=\"com.oddcast.util.Point\"/>\n\t<t path=\"com.oddcast.util.Point\"/>\n</f></getMouse>\n\t<setScale set=\"method\" line=\"614\"><f a=\"xScale:yScale\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setScale>\n\t<isNearOne set=\"method\" line=\"618\"><f a=\"a:?error\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Bool\"/>\n</f></isNearOne>\n\t<shiftOrigin public=\"1\" set=\"method\" line=\"622\"><f a=\"x:y\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></shiftOrigin>\n\t<seperateJaw public=\"1\" set=\"method\" line=\"644\"><f a=\"afterReadData\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/>\n\t<x path=\"Void\"/>\n</f></seperateJaw>\n\t<replaceImageAssets public=\"1\" set=\"method\" line=\"687\"><f a=\"inImageAsset\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\"/>\n\t<x path=\"Void\"/>\n</f></replaceImageAssets>\n\t<dispose public=\"1\" set=\"method\" line=\"693\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<coloringPartIndex line=\"733\"><x path=\"Int\"/></coloringPartIndex>\n\t<name public=\"1\"><c path=\"String\"/></name>\n\t<matrixLL public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.display.MatrixLL\"/></matrixLL>\n\t<imageAssets_ public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\"/></imageAssets_>\n\t<colorTransform public=\"1\"><t path=\"com.oddcast.util.ColorTransform\"/></colorTransform>\n\t<transformMatrix23 public=\"1\"><t path=\"com.oddcast.util.Matrix23\"/></transformMatrix23>\n\t<iTransformMatrix23 public=\"1\"><t path=\"com.oddcast.util.Matrix23\"/></iTransformMatrix23>\n\t<children public=\"1\"><c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/></c></children>\n\t<parent public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/></parent>\n\t<coloring_ public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.color.Coloring\"/></coloring_>\n\t<blendMode_><x path=\"Int\"/></blendMode_>\n\t<setBlendMode public=\"1\" set=\"method\" line=\"745\"><f a=\"bm\">\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></setBlendMode>\n\t<alphaGroup_ public=\"1\"><e path=\"com.oddcast.app.vhss_extraction.color.AlphaGroup\"/></alphaGroup_>\n\t<noRender public=\"1\"><x path=\"Bool\"/></noRender>\n\t<currFrame public=\"1\"><x path=\"Int\"/></currFrame>\n\t<treeDepth public=\"1\"><x path=\"Int\"/></treeDepth>\n\t<DEBUG_RegColor public=\"1\"><x path=\"Int\"/></DEBUG_RegColor>\n\t<new public=\"1\" set=\"method\" line=\"52\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset.FIRST_VERSION = 1;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.BLEND_MODE_VERSION = 2;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.LATEST_VERSION = 2;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.DEBUG_DRAW_NORMAL = 0;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.DEBUG_DRAW_NOIMAGE = 1;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.DEBUG_DRAW_REG = 2;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.DEBUG_DRAW_BOUND = 4;
com_oddcast_app_vhss_$extraction_display_DisplayAsset.DEBUG_RegColorNULL = 0;
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$MC.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<autoAniType_><e path=\"com.oddcast.app.vhss_extraction.display.AutoAniType\"/></autoAniType_>\n\t<setAniType public=\"1\" set=\"method\" line=\"798\"><f a=\"autoAniType:?bOnlyifNone\">\n\t<e path=\"com.oddcast.app.vhss_extraction.display.AutoAniType\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></setAniType>\n\t<parse public=\"1\" set=\"method\" line=\"803\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"813\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<setToFrame_0b set=\"method\" line=\"837\"><f a=\"f:?loop:?allowNulls\">\n\t<x path=\"Int\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></setToFrame_0b>\n\t<new public=\"1\" set=\"method\" line=\"792\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyebrowArt.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_eyebrowArt\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"868\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<currEnergy><x path=\"Float\"/></currEnergy>\n\t<new public=\"1\" set=\"method\" line=\"862\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Trans.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<shiftX set=\"method\" line=\"894\"><f a=\"newValue\">\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></shiftX>\n\t<shiftY set=\"method\" line=\"898\"><f a=\"newValue\">\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></shiftY>\n\t<translateMatrix><t path=\"com.oddcast.util.Matrix23\"/></translateMatrix>\n\t<new public=\"1\" set=\"method\" line=\"889\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$Left_$Trans.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans\"/>\n\t<parse public=\"1\" set=\"method\" line=\"908\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<shiftX set=\"method\" line=\"913\" override=\"1\"><f a=\"newValue\">\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></shiftX>\n\t<getMultiplicand set=\"method\" line=\"917\"><f a=\"\"><x path=\"Float\"/></f></getMultiplicand>\n\t<isLeft_ public=\"1\"><x path=\"Bool\"/></isLeft_>\n\t<new public=\"1\" set=\"method\" line=\"907\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eye.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_eye\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"929\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"924\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$eyeball.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_eyeball\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"945\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<invBase><t path=\"com.oddcast.util.Matrix23\"/></invBase>\n\t<new public=\"1\" set=\"method\" line=\"939\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$brow.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_brow\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"961\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"959\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_mouth\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"975\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"970\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$glasses.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_glasses\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Left_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"988\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"984\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$nose.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_nose\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans\"/>\n\t<isLeft public=\"1\"><x path=\"Int\"/></isLeft>\n\t<afterRead public=\"1\" set=\"method\" line=\"1018\" override=\"1\"><f a=\"afterReadData\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/>\n\t<x path=\"Void\"/>\n</f></afterRead>\n\t<makeNoseTip set=\"method\" line=\"1076\"><f a=\"div:crackAvoid\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></makeNoseTip>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"1103\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<scaleable><x path=\"Bool\"/></scaleable>\n\t<new public=\"1\" set=\"method\" line=\"1011\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$noseTip.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_noseTip\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_Trans\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"1147\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"1138\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$sock.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_sock\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"1158\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<new public=\"1\" set=\"method\" line=\"1156\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$mouth_$attached.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_mouth_attached\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<new public=\"1\" set=\"method\" line=\"1187\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$body.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_body\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"1203\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<breathingMatrix><t path=\"com.oddcast.util.Matrix23\"/></breathingMatrix>\n\t<new public=\"1\" set=\"method\" line=\"1195\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accfrag.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_accfrag\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<new public=\"1\" set=\"method\" line=\"1235\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$accmouth.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_accmouth\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<new public=\"1\" set=\"method\" line=\"1240\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$host.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\" params=\"\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<finalfixup public=\"1\" set=\"method\" line=\"45\" override=\"1\"><f a=\"ev\">\n\t<t path=\"com.oddcast.app.vhss_extraction.animation.EV\"/>\n\t<x path=\"Void\"/>\n</f></finalfixup>\n\t<centerArt set=\"method\" line=\"58\"><f a=\"artl:artr:ev:name\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/>\n\t<t path=\"com.oddcast.app.vhss_extraction.animation.EV\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></centerArt>\n\t<initDisplayTree public=\"1\" set=\"method\" line=\"82\" override=\"1\"><f a=\"\"><x path=\"Void\"/></f></initDisplayTree>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"183\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<translateMatrix><t path=\"com.oddcast.util.Matrix23\"/></translateMatrix>\n\t<dispose public=\"1\" set=\"method\" line=\"238\" override=\"1\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<new public=\"1\" set=\"method\" line=\"38\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$turn.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_turn\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<turnMatrix><t path=\"com.oddcast.util.Matrix23\"/></turnMatrix>\n\t<new public=\"1\" set=\"method\" line=\"613\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$SimpleTurn.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_SimpleTurn\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_turn\"/>\n\t<parse public=\"1\" set=\"method\" line=\"395\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"410\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<finalfixup public=\"1\" set=\"method\" line=\"425\" override=\"1\"><f a=\"ev\">\n\t<t path=\"com.oddcast.app.vhss_extraction.animation.EV\"/>\n\t<x path=\"Void\"/>\n</f></finalfixup>\n\t<isLeft_ public=\"1\"><x path=\"Bool\"/></isLeft_>\n\t<scaleFactor><x path=\"Float\"/></scaleFactor>\n\t<new public=\"1\" set=\"method\" line=\"388\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$face.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_face\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_SimpleTurn\"/>\n\t<afterRead public=\"1\" set=\"method\" line=\"486\" override=\"1\"><f a=\"afterReadData\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/>\n\t<x path=\"Void\"/>\n</f></afterRead>\n\t<new public=\"1\" set=\"method\" line=\"484\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$jaw.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_jaw\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate2 public=\"1\" set=\"method\" line=\"500\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate2>\n\t<mouth public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/></mouth>\n\t<mouthBottom public=\"1\"><x path=\"Float\"/></mouthBottom>\n\t<jawHeight public=\"1\"><x path=\"Float\"/></jawHeight>\n\t<new public=\"1\" set=\"method\" line=\"495\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$hairArt.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_hairArt\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<new public=\"1\" set=\"method\" line=\"514\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHair.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_facialHair\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<replaceImageAssets public=\"1\" set=\"method\" line=\"528\" override=\"1\"><f a=\"inImageAsset\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\"/>\n\t<x path=\"Void\"/>\n</f></replaceImageAssets>\n\t<afterRead public=\"1\" set=\"method\" line=\"534\" override=\"1\"><f a=\"afterReadData\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/>\n\t<x path=\"Void\"/>\n</f></afterRead>\n\t<afterReadData><t path=\"com.oddcast.app.vhss_extraction.display.AfterReadData\"/></afterReadData>\n\t<jaw><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_facialHairJaw\"/></jaw>\n\t<new public=\"1\" set=\"method\" line=\"523\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$facialHairJaw.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_facialHairJaw\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate2 public=\"1\" set=\"method\" line=\"603\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate2>\n\t<new public=\"1\" set=\"method\" line=\"598\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhairArt.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_backhairArt\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<new public=\"1\" set=\"method\" line=\"636\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_DisplayAsset_$backhair.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_backhair\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_host\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset_MC\"/>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"652\" override=\"1\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<rotateMatrix><t path=\"com.oddcast.util.Matrix23\"/></rotateMatrix>\n\t<new public=\"1\" set=\"method\" line=\"647\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_app_vhss_$extraction_display_ImageAssets.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\" params=\"\" module=\"com.oddcast.app.vhss_extraction.display.ImageAsset\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<FIRST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"34\" static=\"1\"><x path=\"Int\"/></FIRST_VERSION>\n\t<LATEST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"35\" static=\"1\"><x path=\"Int\"/></LATEST_VERSION>\n\t<reset public=\"1\" set=\"method\" line=\"39\"><f a=\"\"><x path=\"Void\"/></f></reset>\n\t<parse public=\"1\" set=\"method\" line=\"44\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<sumColoring public=\"1\" set=\"method\" line=\"74\"><f a=\"rgbTotal:weight\">\n\t<c path=\"com.oddcast.app.vhss_extraction.color.RGB_Total\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></sumColoring>\n\t<effectColorTransform public=\"1\" set=\"method\" line=\"79\"><f a=\"colorTransform:editable:bUseColorBase:name:applyImmediatly\">\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></effectColorTransform>\n\t<clone public=\"1\" set=\"method\" line=\"105\"><f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\"/></f></clone>\n\t<dispose public=\"1\" set=\"method\" line=\"114\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<assets public=\"1\"><c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/></c></assets>\n\t<colorBase_ public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.color.ColorBase\"/></colorBase_>\n\t<new public=\"1\" set=\"method\" line=\"37\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_display_ImageAssets.FIRST_VERSION = 1;
com_oddcast_app_vhss_$extraction_display_ImageAssets.LATEST_VERSION = 1;
com_oddcast_app_vhss_$extraction_display_ImageAsset.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<forEachImageAsset public=\"1\" set=\"method\" line=\"275\" static=\"1\"><f a=\"imageAssets:cb\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.ImageAssets\"/>\n\t<f a=\"\">\n\t\t<c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/>\n\t\t<x path=\"Bool\"/>\n\t</f>\n\t<x path=\"Void\"/>\n</f></forEachImageAsset>\n\t<imageX_><x path=\"Float\"/></imageX_>\n\t<imageY_><x path=\"Float\"/></imageY_>\n\t<spriteIndex_ public=\"1\"><x path=\"Int\"/></spriteIndex_>\n\t<name_ public=\"1\"><c path=\"String\"/></name_>\n\t<scale_ public=\"1\"><x path=\"Float\"/></scale_>\n\t<spriteImages><t path=\"com.oddcast.app.vhss_extraction.display.SpriteImages\"/></spriteImages>\n\t<clone public=\"1\" set=\"method\" line=\"143\"><f a=\"?cloneNameSuffix\">\n\t<c path=\"String\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/>\n</f></clone>\n\t<setOffsetAndScale public=\"1\" set=\"method\" line=\"159\"><f a=\"scaledImageX:scaledImageY:scale\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setOffsetAndScale>\n\t<shiftOrigin public=\"1\" set=\"method\" line=\"167\"><f a=\"x:y\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></shiftOrigin>\n\t<parse public=\"1\" set=\"method\" line=\"172\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<checkScale public=\"1\" set=\"method\" line=\"184\"><f a=\"\"><x path=\"Void\"/></f></checkScale>\n\t<loadFromSpriteSheet public=\"1\" set=\"method\" line=\"191\"><f a=\"spriteImages\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.SpriteImages\"/>\n\t<x path=\"Void\"/>\n</f></loadFromSpriteSheet>\n\t<effectColorTransform public=\"1\" set=\"method\" line=\"195\"><f a=\"colorTransform:editable:applyImmediatly\">\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n</f></effectColorTransform>\n\t<draw public=\"1\" set=\"method\" line=\"215\"><f a=\"canvas:blendMode\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></draw>\n\t<getSpriteImage public=\"1\" set=\"method\" line=\"225\"><f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\"/></f></getSpriteImage>\n\t<splitY public=\"1\" set=\"method\" line=\"230\"><f a=\"y:splitName\">\n\t<x path=\"Float\"/>\n\t<c path=\"String\"/>\n\t<c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/></c>\n</f></splitY>\n\t<splitX public=\"1\" set=\"method\" line=\"245\"><f a=\"x:splitName\">\n\t<x path=\"Float\"/>\n\t<c path=\"String\"/>\n\t<c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.ImageAsset\"/></c>\n</f></splitX>\n\t<getBounds public=\"1\" set=\"method\" line=\"261\"><f a=\"\"><t path=\"com.oddcast.util.Rectangle\"/></f></getBounds>\n\t<dispose public=\"1\" set=\"method\" line=\"283\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<new public=\"1\" set=\"method\" line=\"141\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_display_SpriteImage.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<BLENDMODE_SOURCE_OVER public=\"1\" get=\"inline\" set=\"null\" line=\"51\" static=\"1\"><x path=\"Int\"/></BLENDMODE_SOURCE_OVER>\n\t<BLENDMODE_SCREEN public=\"1\" get=\"inline\" set=\"null\" line=\"52\" static=\"1\"><x path=\"Int\"/></BLENDMODE_SCREEN>\n\t<dontCare line=\"77\" static=\"1\"><x path=\"Bool\"/></dontCare>\n\t<inSpriteSheetRect_ public=\"1\"><t path=\"com.oddcast.util.Rectangle\"/></inSpriteSheetRect_>\n\t<name_ public=\"1\"><c path=\"String\"/></name_>\n\t<assetWholeImage public=\"1\"><t path=\"com.oddcast.app.vhss_extraction.display.SpriteSheetType\"/></assetWholeImage>\n\t<currColorTransform><t path=\"com.oddcast.util.ColorTransform\"/></currColorTransform>\n\t<waitingColorTransform><t path=\"com.oddcast.util.ColorTransform\"/></waitingColorTransform>\n\t<colorTransformedCanvas><t path=\"com.oddcast.util.VCanvas\"/></colorTransformedCanvas>\n\t<clonedFrom><c path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\"/></clonedFrom>\n\t<parse public=\"1\" set=\"method\" line=\"60\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<loadFromSpriteSheet public=\"1\" set=\"method\" line=\"67\"><f a=\"spriteSheetImage\">\n\t<t path=\"com.oddcast.app.vhss_extraction.display.SpriteSheetType\"/>\n\t<x path=\"Void\"/>\n</f></loadFromSpriteSheet>\n\t<effectColorTransform public=\"1\" set=\"method\" line=\"79\"><f a=\"colorTransform:editable:applyImmediatly\">\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n</f></effectColorTransform>\n\t<_applyColorTransform set=\"method\" line=\"86\"><f a=\"ct:editable\">\n\t<t path=\"com.oddcast.util.ColorTransform\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></_applyColorTransform>\n\t<draw public=\"1\" set=\"method\" line=\"130\"><f a=\"canvas:imageX:imageY:invScale:blendMode\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Int\"/>\n\t<x path=\"Void\"/>\n</f></draw>\n\t<getBounds public=\"1\" set=\"method\" line=\"153\"><f a=\"\"><t path=\"com.oddcast.util.Rectangle\"/></f></getBounds>\n\t<clone public=\"1\" set=\"method\" line=\"170\"><f a=\"?cloneNameSuffix\">\n\t<c path=\"String\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\"/>\n</f></clone>\n\t<splitY public=\"1\" set=\"method\" line=\"184\"><f a=\"y:splitName\">\n\t<x path=\"Float\"/>\n\t<c path=\"String\"/>\n\t<c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\"/></c>\n</f></splitY>\n\t<splitX public=\"1\" set=\"method\" line=\"195\"><f a=\"x:splitName\">\n\t<x path=\"Float\"/>\n\t<c path=\"String\"/>\n\t<c path=\"Array\"><c path=\"com.oddcast.app.vhss_extraction.display.SpriteImage\"/></c>\n</f></splitX>\n\t<dispose public=\"1\" set=\"method\" line=\"204\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<new public=\"1\" set=\"method\" line=\"55\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_display_SpriteImage.BLENDMODE_SOURCE_OVER = 0;
com_oddcast_app_vhss_$extraction_display_SpriteImage.BLENDMODE_SCREEN = 1;
com_oddcast_app_vhss_$extraction_display_SpriteImage.dontCare = true;
com_oddcast_io_xmlIO_XmlIODated.__rtti = "<class path=\"com.oddcast.io.xmlIO.XmlIODated\" params=\"\" module=\"com.oddcast.io.xmlIO.XmlIOVersioned\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<setCreationDate public=\"1\" set=\"method\" line=\"17\"><f a=\"\"><x path=\"Void\"/></f></setCreationDate>\n\t<setCompiledDate public=\"1\" set=\"method\" line=\"18\"><f a=\"\"><x path=\"Void\"/></f></setCompiledDate>\n\t<parseDates public=\"1\" set=\"method\" line=\"21\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parseDates>\n\t<createdDate_><c path=\"String\"/></createdDate_>\n\t<compiledDate_><c path=\"String\"/></compiledDate_>\n</class>";
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\" params=\"\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIODated\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<FIRST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"49\" static=\"1\"><x path=\"Int\"/></FIRST_VERSION>\n\t<LATEST_VERSION public=\"1\" get=\"inline\" set=\"null\" line=\"50\" static=\"1\"><x path=\"Int\"/></LATEST_VERSION>\n\t<SPRITESHEET_LAYOUT public=\"1\" get=\"inline\" set=\"null\" line=\"53\" static=\"1\"><c path=\"String\"/></SPRITESHEET_LAYOUT>\n\t<SPRITESHEET_IMAGE public=\"1\" get=\"inline\" set=\"null\" line=\"54\" static=\"1\"><c path=\"String\"/></SPRITESHEET_IMAGE>\n\t<SPRITESHEET_EXTENSION public=\"1\" get=\"inline\" set=\"null\" line=\"55\" static=\"1\"><c path=\"String\"/></SPRITESHEET_EXTENSION>\n\t<readFromSpriteSheetImage public=\"1\" set=\"method\" line=\"91\" static=\"1\"><f a=\"archive:imageName:construct:completeCB:errorCB\">\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchive\"/>\n\t<c path=\"String\"/>\n\t<f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/></f>\n\t<f a=\"\">\n\t\t<c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/>\n\t\t<x path=\"Void\"/>\n\t</f>\n\t<f a=\":\">\n\t\t<c path=\"String\"/>\n\t\t<c path=\"String\"/>\n\t\t<x path=\"Void\"/>\n\t</f>\n\t<x path=\"Void\"/>\n</f></readFromSpriteSheetImage>\n\t<processMetaData set=\"method\" line=\"195\" static=\"1\"><f a=\"metaData:totalSize:spriteSheetImage:construct\">\n\t<t path=\"com.oddcast.io.pngEncoder.PNGMetadata\"/>\n\t<x path=\"Int\"/>\n\t<t path=\"com.oddcast.util.VImage\"/>\n\t<f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/></f>\n\t<c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/>\n</f></processMetaData>\n\t<spriteStartTime static=\"1\"><x path=\"Float\"/></spriteStartTime>\n\t<processSpriteSheetLayoutData set=\"method\" line=\"209\" static=\"1\"><f a=\"dataString:construct:totalSize:spriteSheetImage\">\n\t<c path=\"String\"/>\n\t<f a=\"\"><c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/></f>\n\t<x path=\"Int\"/>\n\t<t path=\"com.oddcast.util.VImage\"/>\n\t<c path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/>\n</f></processSpriteSheetLayoutData>\n\t<afterRead set=\"method\" line=\"234\"><f a=\"\"><x path=\"Void\"/></f></afterRead>\n\t<cleanBeforeSaving public=\"1\" set=\"method\" line=\"249\"><f a=\"\"><x path=\"Void\"/></f></cleanBeforeSaving>\n\t<parse public=\"1\" set=\"method\" line=\"252\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<setSpriteSheet public=\"1\" set=\"method\" line=\"267\"><f a=\"spriteSheetImage\">\n\t<t path=\"com.oddcast.util.VImage\"/>\n\t<x path=\"Void\"/>\n</f></setSpriteSheet>\n\t<initDisplayTree public=\"1\" set=\"method\" line=\"290\"><f a=\"\"><x path=\"Void\"/></f></initDisplayTree>\n\t<dispose public=\"1\" set=\"method\" line=\"300\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<imageStr_ public=\"1\"><c path=\"String\"/></imageStr_>\n\t<root public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.display.DisplayAsset\"/></root>\n\t<spriteImages_ public=\"1\"><t path=\"com.oddcast.app.vhss_extraction.display.SpriteImages\"/></spriteImages_>\n\t<scale_ public=\"1\"><x path=\"Float\"/></scale_>\n\t<dataSize public=\"1\"><x path=\"Int\"/></dataSize>\n\t<mamName_ public=\"1\"><c path=\"String\"/></mamName_>\n\t<lspriteSheet><t path=\"com.oddcast.app.vhss_extraction.display.SpriteSheetType\"/></lspriteSheet>\n\t<defaultFileExtension public=\"1\"><c path=\"String\"/></defaultFileExtension>\n\t<new public=\"1\" set=\"method\" line=\"58\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.FIRST_VERSION = 1;
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.LATEST_VERSION = 1;
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.SPRITESHEET_LAYOUT = "htmlSpriteSheet.vhssLayout";
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.SPRITESHEET_IMAGE = "ohv2.png";
com_oddcast_app_vhss_$extraction_rasterdataset_RasterDataSet.SPRITESHEET_EXTENSION = "ohv2.png";
com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.rasterdataset.AccFragDataSet\" params=\"\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/>\n\t<totalCount line=\"43\" static=\"1\"><x path=\"Int\"/></totalCount>\n\t<parse public=\"1\" set=\"method\" line=\"45\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<isNull public=\"1\" set=\"method\" line=\"60\"><f a=\"\"><x path=\"Bool\"/></f></isNull>\n\t<dispose public=\"1\" set=\"method\" line=\"64\" override=\"1\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<new public=\"1\" set=\"method\" line=\"38\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_rasterdataset_AccFragDataSet.totalCount = 0;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.__rtti = "<class path=\"com.oddcast.app.vhss_extraction.rasterdataset.HostDataSet\" params=\"\">\n\t<extends path=\"com.oddcast.app.vhss_extraction.rasterdataset.RasterDataSet\"/>\n\t<eyesRGB get=\"inline\" set=\"null\" line=\"61\" static=\"1\"><x path=\"Int\"/></eyesRGB>\n\t<hairRGB get=\"inline\" set=\"null\" line=\"62\" static=\"1\"><x path=\"Int\"/></hairRGB>\n\t<mouthRGB get=\"inline\" set=\"null\" line=\"63\" static=\"1\"><x path=\"Int\"/></mouthRGB>\n\t<skinRGB get=\"inline\" set=\"null\" line=\"64\" static=\"1\"><x path=\"Int\"/></skinRGB>\n\t<makeupRGB get=\"inline\" set=\"null\" line=\"65\" static=\"1\"><x path=\"Int\"/></makeupRGB>\n\t<hyscale get=\"inline\" set=\"null\" line=\"67\" static=\"1\"><x path=\"Int\"/></hyscale>\n\t<hxscale get=\"inline\" set=\"null\" line=\"68\" static=\"1\"><x path=\"Int\"/></hxscale>\n\t<mscale get=\"inline\" set=\"null\" line=\"69\" static=\"1\"><x path=\"Int\"/></mscale>\n\t<nscale get=\"inline\" set=\"null\" line=\"70\" static=\"1\"><x path=\"Int\"/></nscale>\n\t<bscale get=\"inline\" set=\"null\" line=\"71\" static=\"1\"><x path=\"Int\"/></bscale>\n\t<age get=\"inline\" set=\"null\" line=\"73\" static=\"1\"><x path=\"Int\"/></age>\n\t<blush get=\"inline\" set=\"null\" line=\"75\" static=\"1\"><x path=\"Int\"/></blush>\n\t<makeup get=\"inline\" set=\"null\" line=\"76\" static=\"1\"><x path=\"Int\"/></makeup>\n\t<SCALE_HY public=\"1\" get=\"inline\" set=\"null\" line=\"87\" static=\"1\"><c path=\"String\"/></SCALE_HY>\n\t<SCALE_HX public=\"1\" get=\"inline\" set=\"null\" line=\"88\" static=\"1\"><c path=\"String\"/></SCALE_HX>\n\t<SCALE_M public=\"1\" get=\"inline\" set=\"null\" line=\"89\" static=\"1\"><c path=\"String\"/></SCALE_M>\n\t<SCALE_N public=\"1\" get=\"inline\" set=\"null\" line=\"90\" static=\"1\"><c path=\"String\"/></SCALE_N>\n\t<SCALE_B public=\"1\" get=\"inline\" set=\"null\" line=\"91\" static=\"1\"><c path=\"String\"/></SCALE_B>\n\t<SCALE_GROUPS line=\"93\" static=\"1\"><c path=\"Array\"><c path=\"String\"/></c></SCALE_GROUPS>\n\t<COLOR_GROUPS line=\"101\" static=\"1\"><c path=\"Array\"><e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/></c></COLOR_GROUPS>\n\t<ALTER_Y_OFFSET public=\"1\" get=\"inline\" set=\"null\" line=\"442\" static=\"1\"><x path=\"Float\"/></ALTER_Y_OFFSET>\n\t<ageStr><c path=\"String\"/></ageStr>\n\t<mouthVersion_ public=\"1\"><x path=\"Int\"/></mouthVersion_>\n\t<breathRate_ public=\"1\"><x path=\"Float\"/></breathRate_>\n\t<ev_ public=\"1\"><c path=\"com.oddcast.app.vhss_extraction.animation.EngineConstantValues\"/></ev_>\n\t<hostID_ public=\"1\"><x path=\"Int\"/></hostID_>\n\t<mouthID_ public=\"1\"><x path=\"Int\"/></mouthID_>\n\t<hostXoffset_ public=\"1\"><x path=\"Float\"/></hostXoffset_>\n\t<hostYoffset_ public=\"1\"><x path=\"Float\"/></hostYoffset_>\n\t<colorEditable><x path=\"Bool\"/></colorEditable>\n\t<cleanBeforeSaving public=\"1\" set=\"method\" line=\"138\" override=\"1\"><f a=\"\"><x path=\"Void\"/></f></cleanBeforeSaving>\n\t<parse public=\"1\" set=\"method\" line=\"142\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<setScale public=\"1\" set=\"method\" line=\"170\"><f a=\"scaleName:scale\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setScale>\n\t<getScale public=\"1\" set=\"method\" line=\"180\"><f a=\"scaleName\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</f></getScale>\n\t<isAlphable public=\"1\" set=\"method\" line=\"190\"><f a=\"part\">\n\t<c path=\"String\"/>\n\t<x path=\"Bool\"/>\n</f></isAlphable>\n\t<isColorable public=\"1\" set=\"method\" line=\"194\"><f a=\"part\">\n\t<c path=\"String\"/>\n\t<x path=\"Bool\"/>\n</f></isColorable>\n\t<setColor public=\"1\" set=\"method\" line=\"199\"><f a=\"part:hex\">\n\t<c path=\"String\"/>\n\t<t path=\"com.oddcast.util.UInt\"/>\n\t<x path=\"Void\"/>\n</f></setColor>\n\t<getColor public=\"1\" set=\"method\" line=\"220\"><f a=\"part\">\n\t<c path=\"String\"/>\n\t<t path=\"com.oddcast.util.UInt\"/>\n</f></getColor>\n\t<colorGroup2Str set=\"method\" line=\"242\"><f a=\"colorGroup\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<c path=\"String\"/>\n</f></colorGroup2Str>\n\t<sumColoring set=\"method\" line=\"252\"><f a=\"colorGroup\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<t path=\"com.oddcast.cv.util.RGB\"/>\n</f></sumColoring>\n\t<getAlpha public=\"1\" set=\"method\" line=\"275\"><f a=\"part\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</f></getAlpha>\n\t<setAlpha public=\"1\" set=\"method\" line=\"281\"><f a=\"part:value\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setAlpha>\n\t<calcGroupColorDelta set=\"method\" line=\"289\"><f a=\"colorGroup\">\n\t<e path=\"com.oddcast.app.vhss_extraction.color.ColorGroup\"/>\n\t<x path=\"Void\"/>\n</f></calcGroupColorDelta>\n\t<primeFromQueryString public=\"1\" set=\"method\" line=\"305\"><f a=\"queryString\">\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></primeFromQueryString>\n\t<getColorURLstring public=\"1\" set=\"method\" line=\"352\"><f a=\"\"><c path=\"String\"/></f></getColorURLstring>\n\t<accessoryLoaded public=\"1\" set=\"method\" line=\"389\"><f a=\"\"><x path=\"Void\"/></f></accessoryLoaded>\n\t<clearScales set=\"method\" line=\"397\"><f a=\"\"><x path=\"Void\"/></f></clearScales>\n\t<frameUpdate public=\"1\" set=\"method\" line=\"424\"><f a=\"appState\">\n\t<c path=\"com.oddcast.app.vhss_extraction.display.VHSSappState\"/>\n\t<x path=\"Void\"/>\n</f></frameUpdate>\n\t<draw public=\"1\" set=\"method\" line=\"445\"><f a=\"canvas\">\n\t<t path=\"com.oddcast.util.VCanvas\"/>\n\t<x path=\"Void\"/>\n</f></draw>\n\t<getHostMouse public=\"1\" set=\"method\" line=\"464\"><f a=\"mouse\">\n\t<t path=\"com.oddcast.util.Point\"/>\n\t<t path=\"com.oddcast.app.vhss_extraction.rasterdataset.HitTest\"/>\n</f></getHostMouse>\n\t<measureHost public=\"1\" set=\"method\" line=\"500\"><f a=\"\"><t path=\"com.oddcast.app.vhss_extraction.rasterdataset.HostMeasurements\"/></f></measureHost>\n\t<effectGroupColors set=\"method\" line=\"519\"><f a=\"groupColorDelta:groupAlpha:bUseColorBase:editable:applyImmediatly\">\n\t<t path=\"com.oddcast.app.vhss_extraction.color.GroupColorDelta\"/>\n\t<t path=\"com.oddcast.app.vhss_extraction.color.GroupAlpha\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Bool\"/>\n\t<x path=\"Void\"/>\n</f></effectGroupColors>\n\t<finalfixup public=\"1\" set=\"method\" line=\"554\"><f a=\"\"><x path=\"Void\"/></f></finalfixup>\n\t<setHostOffsets public=\"1\" set=\"method\" line=\"563\"><f a=\"hostXoffset:hostYoffset\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setHostOffsets>\n\t<setEngineConstant public=\"1\" set=\"method\" line=\"572\"><f a=\"name:value\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n\t<x path=\"Void\"/>\n</f></setEngineConstant>\n\t<getEngineConstant public=\"1\" set=\"method\" line=\"577\"><f a=\"name\">\n\t<c path=\"String\"/>\n\t<x path=\"Float\"/>\n</f></getEngineConstant>\n\t<fHyscale line=\"584\"><x path=\"Float\"/></fHyscale>\n\t<fHxscale line=\"585\"><x path=\"Float\"/></fHxscale>\n\t<mouthScale line=\"586\"><x path=\"Float\"/></mouthScale>\n\t<noseScale line=\"587\"><x path=\"Float\"/></noseScale>\n\t<bodyScale line=\"588\"><x path=\"Float\"/></bodyScale>\n\t<groupColorDelta><t path=\"com.oddcast.app.vhss_extraction.color.GroupColorDelta\"/></groupColorDelta>\n\t<groupAlpha><t path=\"com.oddcast.app.vhss_extraction.color.GroupAlpha\"/></groupAlpha>\n\t<groupColorFromQueryString><t path=\"com.oddcast.app.vhss_extraction.color.GroupColorDelta\"/></groupColorFromQueryString>\n\t<normalRenderFlags><c path=\"com.oddcast.util.Flags\"/></normalRenderFlags>\n\t<new public=\"1\" set=\"method\" line=\"124\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.eyesRGB = 0;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.hairRGB = 1;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.mouthRGB = 2;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.skinRGB = 3;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.makeupRGB = 4;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.hyscale = 5;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.hxscale = 6;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.mscale = 7;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.nscale = 8;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.bscale = 9;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.age = 10;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.blush = 11;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.makeup = 12;
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_HY = "hyscale";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_HX = "hxscale";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_M = "mscale";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_N = "nscale";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_B = "bscale";
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.SCALE_GROUPS = ["hyscale","hxscale","mscale","nscale","bscale"];
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.COLOR_GROUPS = [com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_EYES,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_HAIR,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MOUTH,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_SKIN,com_oddcast_app_vhss_$extraction_color_ColorGroup.ColorGroup_MAKEUP];
com_oddcast_app_vhss_$extraction_rasterdataset_HostDataSet.ALTER_Y_OFFSET = -3.0;
com_oddcast_audio_ID3comment.AUDIO_DURATION = "audio_duration";
com_oddcast_audio_ID3comment.LIP_STRING = "lip_string";
com_oddcast_audio_ID3comment.TIMED_PHONEMES = "timed_phonemes";
com_oddcast_audio_ID3comment.TIMED_BEATS = "timed_beats";
com_oddcast_audio_ID3comment.TEXT = "text";
com_oddcast_audio_ID3comment.DATA_DELIMITER = "\t";
com_oddcast_audio_ID3comment.ITEM_DELIMITER = ";" + "\n";
com_oddcast_audio_ID3comment.FALSE_ITEM_DELIMITER = ";" + "\r" + "\n";
com_oddcast_audio_PlayingVAudioSimpleVisemes.lip2to1Map = [0,1,2,3,4,5,6,5,8,9,10,1,9,9,8,5];
com_oddcast_audio_PlayingVAudioSimpleVisemes.visemeName = ["none","closed","moo","fave","bmp","ah","elle","L","N","Arr","EST","CH"];
com_oddcast_cv_util_COLOR.TRANSPARENT = 0;
com_oddcast_cv_util_COLOR.BLACK = -16777216;
com_oddcast_cv_util_COLOR.RED = -65536;
com_oddcast_cv_util_COLOR.GREEN = -16711936;
com_oddcast_cv_util_COLOR.BLUE = -16776961;
com_oddcast_cv_util_COLOR.YELLOW = -256;
com_oddcast_cv_util_COLOR.CYAN = -16711681;
com_oddcast_cv_util_COLOR.MAGENTA = -65281;
com_oddcast_cv_util_COLOR.ORANGE = -32768;
com_oddcast_cv_util_COLOR.PINK = -32640;
com_oddcast_cv_util_COLOR.WHITE = -1;
com_oddcast_cv_util_COLOR.LIGHT_GRAY = -3355444;
com_oddcast_cv_util_COLOR.DARK_GRAY = -8355712;
com_oddcast_util_Flags.FLAG_NULL = 0;
com_oddcast_host_HXEvent.EVENT_FILE_COMPLETE = 0;
com_oddcast_host_HXEvent.EVENT_ALPHATEX_COMPLETE = 1;
com_oddcast_host_HXEvent.EVENT_TIMER = 2;
com_oddcast_host_HXEvent.FLAG_WILL_CAUSE_PROBLEMS_WITH_NEKO = -2147483648;
com_oddcast_host_HXEvent.FLAG_READY = 1;
com_oddcast_host_HXEvent.FLAG_DIRTY = 2;
com_oddcast_host_HXEvent.FLAG_DEFAULT = 0;
com_oddcast_host_HXEvent.fireList = new List();
com_oddcast_host_HXEvent.bFireEventsBusy = false;
com_oddcast_host_engine3d_math_Float3D.TEMP = new com_oddcast_host_engine3d_math_Float3D(0.0,0.0,0.0);
com_oddcast_host_engine3d_math_Float3D.X = 0;
com_oddcast_host_engine3d_math_Float3D.Y = 1;
com_oddcast_host_engine3d_math_Float3D.Z = 2;
com_oddcast_host_engine3d_math_Matrix34.TEMP = new com_oddcast_host_engine3d_math_Matrix34(null);
com_oddcast_host_engine3d_math_Matrix34.toDEGREES = 180 / Math.PI;
com_oddcast_host_engine3d_math_Matrix34.toRADIANS = Math.PI / 180;
com_oddcast_host_engine3d_math_Quaternion.TEMP = new com_oddcast_host_engine3d_math_Quaternion();
com_oddcast_host_morph_TargetList.MAX_ATTACK = 60.0;
com_oddcast_host_morph_TargetList.MAX_DECAY = com_oddcast_host_morph_TargetList.MAX_ATTACK;
com_oddcast_host_morph_lipsync_Emphasis.OPEN_TAG = "<emphasis>";
com_oddcast_host_morph_lipsync_Emphasis.OPEN_TAG_STRONG = "<emphasis level=\"strong\">";
com_oddcast_host_morph_lipsync_Emphasis.CLOSE_TAG = "</emphasis>";
com_oddcast_host_morph_lipsync_Emphasis.MINIMUM_ACCENT_STRENGTH = 0.2;
com_oddcast_host_morph_lipsync_Emphasis.DEFAULT_ACCENT_STRENGTH = 1.0;
com_oddcast_host_morph_lipsync_Emphasis.allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ";
com_oddcast_host_morph_lipsync_SyncDatumAccentStrength.VISUAL_AHEAD_OF_AUDIO = 200;
com_oddcast_host_morph_lipsync_TimedVisemes.DEFAULT_SYNC_AHEAD_OF_SOUND = 83.333333333333329;
com_oddcast_host_morph_lipsync_TimedVisemes.MIN_ENERGY_LEVEL = 0.60;
com_oddcast_host_morph_lipsync_TimedVisemes.MAX_ENERGY_LEVEL = 2.00;
com_oddcast_host_morph_lipsync_TimedVisemes.POWER_LEVEL = 3.0;
com_oddcast_host_morph_lipsync_TimedVisemes.TOTAL_INTERVALS = 4;
com_oddcast_host_morph_lipsync_TimedVisemes.TRANSCRIPT_MINIMUM_VISEME_DURATION = 0;
com_oddcast_host_morph_lipsync_TimedVisemes.MINIMUM_VISEME_DURATION = 80;
com_oddcast_host_morph_sync_SyncDatum.NO_EVENT = "";
com_oddcast_host_morph_sync_SyncDatum.PHONE = "P";
com_oddcast_host_morph_sync_SyncDatum.WORD = "W";
com_oddcast_host_morph_sync_SyncDatum.SENTENCE = "S";
com_oddcast_host_morph_sync_SyncDatum.GROUP = "G";
com_oddcast_host_morph_sync_SyncDatum.USERTYPE1 = "1";
com_oddcast_host_morph_sync_SyncDatum.USERTYPE2 = "2";
com_oddcast_host_morph_sync_SyncDatum.USERTYPE3 = "3";
com_oddcast_host_morph_sync_SyncDatum.USERTYPE_EXPRESSION = "U";
com_oddcast_host_morph_sync_SyncDatum.NONTEXTGROUP = "N";
com_oddcast_host_morph_sync_SyncDatum.MOOD = "M";
com_oddcast_host_morph_sync_SyncDatum.BEAT = "B";
com_oddcast_host_morph_sync_SyncDatum.DELIMITER = ",";
com_oddcast_host_morph_sync_SyncDatum.BETWEEN_PAUSE = "X";
com_oddcast_host_morph_sync_SyncDatum.USER_GESTURE_HINT = "H";
com_oddcast_io_archive_iArchive_FileLoadStats.INVALID_TIME = -1.0;
com_oddcast_io_pngEncoder_PNGKeywords.TITLE = "Title";
com_oddcast_io_pngEncoder_PNGKeywords.AUTHOR = "Author";
com_oddcast_io_pngEncoder_PNGKeywords.DESCRIPTION = "Description";
com_oddcast_io_pngEncoder_PNGKeywords.COPYRIGHT = "Copyright";
com_oddcast_io_pngEncoder_PNGKeywords.CREATION_TIME = "Creation Time";
com_oddcast_io_pngEncoder_PNGKeywords.SOFTWARE = "Software";
com_oddcast_io_pngEncoder_PNGKeywords.DISCLAIMER = "Disclaimer";
com_oddcast_io_pngEncoder_PNGKeywords.WARNING = "Warning";
com_oddcast_io_pngEncoder_PNGKeywords.SOURCE = "Source";
com_oddcast_io_pngEncoder_PNGKeywords.COMMENT = "Comment";
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.PNG0 = -1991225785;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.PNG1 = 218765834;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.RGBA_32 = 134610944;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.IHDR = 1229472850;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.tEXt = 1950701684;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.iTXt = 1767135348;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.IDAT = 1229209940;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.IEND = 1229278788;
com_oddcast_io_pngEncoder_PNG_$CHUNK_$SIG.zEXt = 2052348020;
com_oddcast_io_xmlIO_AssetURL.__rtti = "<class path=\"com.oddcast.io.xmlIO.AssetURL\" params=\"\" module=\"com.oddcast.io.xmlIO.IXmlIO\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<dispose public=\"1\" set=\"method\" line=\"165\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n</class>";
com_oddcast_io_xmlIO_TextureAssetURL.__rtti = "<class path=\"com.oddcast.io.xmlIO.TextureAssetURL\" params=\"\" module=\"com.oddcast.io.xmlIO.IXmlIO\">\n\t<extends path=\"com.oddcast.io.xmlIO.AssetURL\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<parse public=\"1\" set=\"method\" line=\"216\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<setTexture public=\"1\" set=\"method\" line=\"222\"><f a=\"tex\">\n\t<t path=\"com.oddcast.io.xmlIO.TextureAssetType\"/>\n\t<x path=\"Void\"/>\n</f></setTexture>\n\t<getTexture public=\"1\" set=\"method\" line=\"223\"><f a=\"\"><t path=\"com.oddcast.io.xmlIO.TextureAssetType\"/></f></getTexture>\n\t<dispose public=\"1\" set=\"method\" line=\"224\" override=\"1\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<insertScaleDirectory set=\"method\" line=\"229\"><f a=\"archive:url\">\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchiveReadOnly\"/>\n\t<c path=\"String\"/>\n\t<c path=\"String\"/>\n</f></insertScaleDirectory>\n\t<loaded public=\"1\" set=\"method\" line=\"231\" override=\"1\"><f a=\"allAssetsLoaded:archive:xmlName\">\n\t<c path=\"com.oddcast.io.xmlIO.AllAssetsLoaded\"><c path=\"com.oddcast.io.xmlIO.IXmlIO\"/></c>\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchiveReadOnly\"/>\n\t<c path=\"String\"/>\n\t<x path=\"Void\"/>\n</f></loaded>\n\t<imagePairs_><x path=\"Map\">\n\t<c path=\"String\"/>\n\t<c path=\"com.oddcast.io.xmlIO.ImagePair\"/>\n</x></imagePairs_>\n\t<textureAsset><t path=\"com.oddcast.io.xmlIO.TextureAssetType\"/></textureAsset>\n\t<getAssetSize public=\"1\" set=\"method\" line=\"317\"><f a=\"\"><x path=\"Int\"/></f></getAssetSize>\n\t<getDownloadSize public=\"1\" set=\"method\" line=\"324\"><f a=\"\"><x path=\"Int\"/></f></getDownloadSize>\n\t<new public=\"1\" set=\"method\" line=\"213\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_io_xmlIO_TextureAssetURLScaled.__rtti = "<class path=\"com.oddcast.io.xmlIO.TextureAssetURLScaled\" params=\"\" module=\"com.oddcast.io.xmlIO.IXmlIO\">\n\t<extends path=\"com.oddcast.io.xmlIO.TextureAssetURL\"/>\n\t<parse public=\"1\" set=\"method\" line=\"178\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<findNearestScale set=\"method\" line=\"184\"><f a=\"inscale\">\n\t<x path=\"Float\"/>\n\t<x path=\"Float\"/>\n</f></findNearestScale>\n\t<insertScaleDirectory set=\"method\" line=\"198\" override=\"1\"><f a=\"archive:url\">\n\t<c path=\"com.oddcast.io.archive.iArchive.IArchiveReadOnly\"/>\n\t<c path=\"String\"/>\n\t<c path=\"String\"/>\n</f></insertScaleDirectory>\n\t<scaleArray_><c path=\"Array\"><x path=\"Float\"/></c></scaleArray_>\n\t<scaleSigFig_><x path=\"Int\"/></scaleSigFig_>\n\t<imageScale public=\"1\" set=\"null\"><x path=\"Float\"/></imageScale>\n\t<new public=\"1\" set=\"method\" line=\"177\"><f a=\"\"><x path=\"Void\"/></f></new>\n</class>";
com_oddcast_io_xmlIO_ImagePair.__rtti = "<class path=\"com.oddcast.io.xmlIO.ImagePair\" params=\"\" module=\"com.oddcast.io.xmlIO.IXmlIO\">\n\t<extends path=\"com.oddcast.io.xmlIO.XmlIOVersioned\"/>\n\t<implements path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataStruct\"/>\n\t<implements path=\"com.oddcast.util.IDisposable\"/>\n\t<dispose public=\"1\" set=\"method\" line=\"334\"><f a=\"\"><x path=\"Void\"/></f></dispose>\n\t<colorURL_ public=\"1\"><c path=\"String\"/></colorURL_>\n\t<maskURL_ public=\"1\"><c path=\"String\"/></maskURL_>\n\t<parse public=\"1\" set=\"method\" line=\"339\" override=\"1\"><f a=\"bd\">\n\t<c path=\"com.oddcast.io.xmlIO.binaryData.IBinaryDataIO\"/>\n\t<x path=\"Void\"/>\n</f></parse>\n\t<new public=\"1\" set=\"method\" line=\"333\"><f a=\"\"><x path=\"Void\"/></f></new>\n\t<meta><m n=\":directlyUsed\"/></meta>\n</class>";
com_oddcast_io_xmlIO_binaryData_BinaryDataIObase.ARRAY_SEPERATOR = ",";
com_oddcast_io_xmlIO_binaryData_BinaryDataIObase.DONT_CARE = true;
com_oddcast_util_FloatInterp.SINESQUARED = -2.0;
com_oddcast_util_FloatInterp.SINUSIODAL = -1.0;
com_oddcast_util_FloatInterp.LINEAR = 0.0;
com_oddcast_util_FloatInterp.SMOOTH = 1.0;
com_oddcast_util_FloatInterp.SMOOTHER = 2.0;
com_oddcast_util_FloatInterp.SAWTOOTH = 0;
com_oddcast_util_FloatInterp.OSCILLATE = 1;
com_oddcast_util_FloatInterp.INFINITE_LOOPS = 10000000;
com_oddcast_util_PointTools.ZERO = new com_oddcast_neko_geom_Point(0.0,0.0);
com_oddcast_util_PointTools.POINT_ELEMENT_NAME = "Point";
com_oddcast_util_RectangleTools.RECTANGLE_ELEMENT_NAME = "Rectangle";
com_oddcast_util_SmartTrace.GRAY = "0:";
com_oddcast_util_SmartTrace.BLACK = "1:";
com_oddcast_util_SmartTrace.YELLOW = "2:";
com_oddcast_util_SmartTrace.RED = "3:";
com_oddcast_util_SmartTrace.FUCHSIA = "4:";
com_oddcast_util_SmartTrace.TRACE_UPDATED = "U:";
com_oddcast_util_SmartTrace.INT_GRAY = "0";
com_oddcast_util_SmartTrace.INT_BLACK = "1";
com_oddcast_util_SmartTrace.INT_YELLOW = "2";
com_oddcast_util_SmartTrace.INT_RED = "3";
com_oddcast_util_SmartTrace.INT_FUCHSIA = "4";
com_oddcast_util_SmartTrace.ALL_CODES = "01234";
com_oddcast_util_StringTools2.CR = "\r";
com_oddcast_util_StringTools2.LF = "\n";
com_oddcast_util_StringTools2.CRLF = com_oddcast_util_StringTools2.CR + com_oddcast_util_StringTools2.LF;
com_oddcast_util_StringTools2.TAB = "\t";
com_oddcast_util_StringTools2.SPACE = " ";
com_oddcast_util_Utils.ASSERT_TRUE = "[ASSERT_TRUE]";
com_oddcast_util_Utils.ASSERT_FALSE = "[ASSERT_FALSE]";
com_oddcast_util_XmlTools.NL = "\n";
com_oddcast_util_XmlTools.TAB = "  ";
com_oddcast_util_js_ConsoleTracing.bDisableFilters = false;
com_oddcast_util_js_JSAudio.CHROME_HACK_OVERRUN = 1500;
com_oddcast_util_js_JSAudio.debugEventStrings = ["abort","canplay","canplaythrough","durationchange","emptied","ended","error","loadeddata","loadedmetadata","loadstart","pause","play","playing","ratechange","seeking","seeked","stalled","suspend","timeupdate","volumechange","waiting"];
com_oddcast_util_js_JSCanvas.DEBUGCanvasCount = 0;
com_oddcast_util_js_JSImage.DEBUGImageCount = 0;
com_oddcast_util_js_OS.PNG_TRANS_DEPTH = 16;
format_tools__$InflateImpl_Window.SIZE = 32768;
format_tools__$InflateImpl_Window.BUFSIZE = 65536;
format_tools_InflateImpl.LEN_EXTRA_BITS_TBL = [0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,0,-1,-1];
format_tools_InflateImpl.LEN_BASE_VAL_TBL = [3,4,5,6,7,8,9,10,11,13,15,17,19,23,27,31,35,43,51,59,67,83,99,115,131,163,195,227,258];
format_tools_InflateImpl.DIST_EXTRA_BITS_TBL = [0,0,0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,-1,-1];
format_tools_InflateImpl.DIST_BASE_VAL_TBL = [1,2,3,4,5,7,9,13,17,25,33,49,65,97,129,193,257,385,513,769,1025,1537,2049,3073,4097,6145,8193,12289,16385,24577];
format_tools_InflateImpl.CODE_LENGTHS_POS = [16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15];
haxe_Serializer.USE_CACHE = false;
haxe_Serializer.USE_ENUM_INDEX = false;
haxe_Serializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe_Unserializer.DEFAULT_RESOLVER = Type;
haxe_Unserializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe_ds_ObjectMap.count = 0;
haxe_io_FPHelper.i64tmp = (function($this) {
	var $r;
	var x = new haxe__$Int64__$_$_$Int64(0,0);
	$r = x;
	return $r;
}(this));
haxe_xml_Parser.escapes = (function($this) {
	var $r;
	var h = new haxe_ds_StringMap();
	if(__map_reserved.lt != null) h.setReserved("lt","<"); else h.h["lt"] = "<";
	if(__map_reserved.gt != null) h.setReserved("gt",">"); else h.h["gt"] = ">";
	if(__map_reserved.amp != null) h.setReserved("amp","&"); else h.h["amp"] = "&";
	if(__map_reserved.quot != null) h.setReserved("quot","\""); else h.h["quot"] = "\"";
	if(__map_reserved.apos != null) h.setReserved("apos","'"); else h.h["apos"] = "'";
	$r = h;
	return $r;
}(this));
js_Boot.__toStr = {}.toString;
js_html_compat_Uint8Array.BYTES_PER_ELEMENT = 1;
com_oddcast_app_vhss_$extraction_mobile_$IFrame_Mobile_$IFrameApp.main();
})(typeof console != "undefined" ? console : {log:function(){}}, typeof window != "undefined" ? window : exports, typeof window != "undefined" ? window : typeof global != "undefined" ? global : typeof self != "undefined" ? self : this);

package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oddcast.android.voki.Modal.LanguageModel;
import com.oddcast.android.voki.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 5/17/16.
 */
public class LanguageAdapter  extends BaseAdapter {
    ArrayList<LanguageModel> fxList;
    Context mContext;
    String type;

    public LanguageAdapter(Context mContext, ArrayList<LanguageModel> fxList) {
        this.mContext = mContext;
        this.fxList = fxList;

    }

    @Override
    public int getCount() {
        return fxList.size();
    }

    @Override
    public Object getItem(int position) {
        return fxList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_menu_item, null, true);
            holder = new ViewHolder();
            holder.tv_itemName = (TextView) convertView.findViewById(R.id.tv_itemName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


            holder.tv_itemName.setText(fxList.get(position).getLanguageName());


        return convertView;
    }

    static class ViewHolder {
        TextView tv_itemName;
    }
}

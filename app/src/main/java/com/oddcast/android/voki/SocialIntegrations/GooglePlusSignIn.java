package com.oddcast.android.voki.SocialIntegrations;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.PlusShare;
import com.oddcast.android.voki.MainActivity;

/**
 * Created by brst-pc20 on 6/23/16.
 */
public class GooglePlusSignIn extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener {
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isGooglePlusInstalled())
        {
            defaultSignin();

            signIn();
        }
        else{
            Toast.makeText(GooglePlusSignIn.this, "Please Install Google plus", Toast.LENGTH_SHORT).show();
            finish();
        }


    }
    private void defaultSignin()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        Log.d("apiconect", "signin");
    }

    @Override
    protected void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();
        Log.d("apiconect", "connected");
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        Log.d("apiconect", "connected");
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("apiconect","requestCode"+requestCode);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d("apiconect","requestCode"+requestCode);
            handleSignInResult(result);
        }
        else if (requestCode == 201) {
            Toast.makeText(this, "Your Voki has been shared", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Sign in", "handleSignInResult:" + result.isSuccess());

            try {
                if (result.isSuccess()) {
                Log.d("final_url_for_sharing", "" + MainActivity.final_url_for_sharing);
            Log.d("photo>>>", "" + MainActivity.photo);

            ContentResolver cr = this.getContentResolver();
           Uri selected_photo =Uri.fromFile(MainActivity.photo.getAbsoluteFile());


            Log.d("selected_photo", ":" + selected_photo);
            String mime = cr.getType(selected_photo);
            Log.d("mime", ":" + mime);
            GoogleSignInAccount acct = result.getSignInAccount();
                Intent shareIntent = new PlusShare.Builder(this)
                        . setType("image/*")
                            .setText("My Voki has something to say - check it out:\n\n" + MainActivity.final_url_for_sharing + "\n\nTo create your own Voki go to: www.voki.com\n\n The Voki app here - https://play.google.com/store/apps/details?id=com.oddcast.android.voki&hl=en")
                        .addStream(selected_photo)
                        .getIntent();
                startActivityForResult(shareIntent, 201);

               // finish();
            } }catch (ActivityNotFoundException e) {
                Toast.makeText(GooglePlusSignIn.this, "You haven't installed google+ on your device", Toast.LENGTH_SHORT).show();
                finish();
            }

           // Log.d("final_url_for_sharing", "" + MainActivity.final_url_for_sharing);


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("failure", "handleSignInResult:" + connectionResult);

    }

    @Override
    public void onClick(View v) {

    }
    private boolean isGooglePlusInstalled()
    {
        try
        {
            getPackageManager().getApplicationInfo("com.google.android.apps.plus",0);
            return true;
        }
        catch(PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }
}

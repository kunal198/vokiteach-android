package com.oddcast.android.voki.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.WallpaperListModel;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.WeakHashMap;

/**
 * Created by brst-pc16 on 4/18/16.
 */
public class WallpaperCategoryAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<WallpaperListModel> wallapaperCategory;
    int newPosition = -1;

    ImageLoader imageLoader = ImageLoader.getInstance();
    private CharacterListener listner;
    private WeakHashMap<Integer, WeakReference<View>> hashMap = new WeakHashMap<Integer, WeakReference<View>>();

    public WallpaperCategoryAdapter(Context mContext, ArrayList<WallpaperListModel> wallapaperCategory, CharacterListener listner) {
        this.mContext = mContext;
        this.wallapaperCategory = wallapaperCategory;
        this.listner = listner;

        Log.e("wallapaperCategory", "wallapaperCategory " + wallapaperCategory.toString());
    }

    @Override
    public int getCount() {
        return wallapaperCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return wallapaperCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_wallpaper, null, true);
            holder = new ViewHolder();

            holder.im_thumbanil = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (LinearLayout) convertView.findViewById(R.id.innerContainer);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        hashMap.put(position, new WeakReference<View>(convertView));
        Log.e("insideWallpaperAdpter", "asdasdasd" + wallapaperCategory.get(position));


        WallpaperListModel wallpaperModel = wallapaperCategory.get(position);
        String file_name = wallpaperModel.getFileName();
        Log.e("selection", "asdasdasd" + wallpaperModel.getSelection());
        if (wallpaperModel.getSelection().equals("true")) {

            try {

                String uri = "assets://img/backgrounds/" + file_name + "/thumbnail.png";
                imageLoader.displayImage(uri, holder.im_thumbanil);
                holder.container.setBackgroundResource(R.drawable.selectable_background);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (wallpaperModel.getSelection().equals("false")) {
            try {

                String uri = "assets://img/backgrounds/" + file_name + "/thumbnail.png";
                imageLoader.displayImage(uri, holder.im_thumbanil);
                holder.container.setBackgroundResource(R.drawable.unselectable_background);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.im_thumbanil.setTag(position);
        holder.im_thumbanil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.picturePath="";
                MainActivity.is_there_any_change_in_character=true;
                int positions = (Integer) v.getTag();
                Log.e("positions", "positions " + positions);
                WallpaperListModel wallpaperModel = wallapaperCategory.get(positions);



                WallpaperListModel selectedpos = wallapaperCategory.get(positions);
                for (int i = 0; i < wallapaperCategory.size(); i++) {
                    if (wallapaperCategory.get(i) == selectedpos) {
                        wallapaperCategory.get(i).setSelection("true");
                    } else {
                        wallapaperCategory.get(i).setSelection("false");
                    }

                }

                if (newPosition == -1) {

                    wallapaperCategory.remove(positions);
                    wallapaperCategory.add(positions, wallpaperModel);

                    if (positions != 1) {
                        wallpaperModel = wallapaperCategory.get(1);

                        wallapaperCategory.remove(1);
                        wallapaperCategory.add(1, wallpaperModel);

                    }
                } else {
                    wallpaperModel = wallapaperCategory.get(positions);
                    wallapaperCategory.remove(positions);
                    wallapaperCategory.add(positions, wallpaperModel);

                    if (positions != newPosition) {
                        wallpaperModel = wallapaperCategory.get(newPosition);
                        wallapaperCategory.remove(newPosition);
                        wallapaperCategory.add(newPosition, wallpaperModel);

                    }
                }
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

                if (listner != null)
                    listner.updateWallPapers(positions, "");

                newPosition = positions;
//                }
            }
        });


        holder.im_thumbanil.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int new_pos = (Integer) v.getTag();

                Toast.makeText(mContext, wallapaperCategory.get(new_pos).getFileName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        return convertView;
    }

    static class ViewHolder {
        ImageView im_thumbanil;
        LinearLayout container;
    }
}

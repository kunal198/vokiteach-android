package com.oddcast.android.voki.Modal;

/**
 * Created by brst-pc20 on 5/11/16.
 */
public class WallpaperListModel {
    String fileName, unselectedImage, selection;

    public String getUnselectedImage() {
        return unselectedImage;
    }

    public void setUnselectedImage(String unselectedImage) {
        this.unselectedImage = unselectedImage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }
}

package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.app.Activity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.PopUp.InternetPopUp;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.ViewUtill;
import com.oddcast.android.voki.custom_listeners.IonListners;
import com.oddcast.android.voki.utils.Constants;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * Created by brst-pc16 on 4/8/16.
 */
public class GridShareAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<HashMap<String, String>> shareList;
    int[] unselectedImages;
    int[] selectedImages;
    ImageLoader imageLoader = ImageLoader.getInstance();
    IonListners listners;
    private WeakHashMap<Integer, WeakReference<View>> hashMap = new WeakHashMap<Integer, WeakReference<View>>();
    private SharedPreferences preferences;
    private int newPos = 0;
    Activity activity;
    public GridShareAdapter(Activity mContext, ArrayList<HashMap<String, String>> shareList, IonListners listners, int[] unselectedImages, int[] selectedImages) {
        this.mContext = mContext;
        this.activity=mContext;
        this.shareList = shareList;
        this.unselectedImages = unselectedImages;
        this.selectedImages = selectedImages;
        this.listners = listners;
        preferences = mContext.getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);

    }

    @Override
    public int getCount() {
        return shareList.size();
    }

    @Override
    public Object getItem(int position) {
        return shareList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.sample_grid_share, null, true);

            hashMap.put(position, new WeakReference<View>(convertView));
            holder.imShare = (ImageView) convertView.findViewById(R.id.imageButton);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap map = shareList.get(position);
        int newPos = preferences.getInt(Constants.SAVE_POS, -1);

        if (newPos == position) {
//                imShare.setImageResource(selectedImages[position]);

            String str = "drawable://" + selectedImages[position];
            imageLoader.displayImage(str, holder.imShare);
        } else {
//                imShare.setImageResource(unselectedImages[position]);
            String str = "drawable://" + unselectedImages[position];
            imageLoader.displayImage(str, holder.imShare);
        }
        holder.pos = position;
        convertView.setTag(holder);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ViewUtill.haveNetworkConnection(activity)) {
                    SharedPreferences.Editor editor;
                    editor = preferences.edit();
                    ViewHolder holder = (ViewHolder) v.getTag();
                    editor.putInt(Constants.SAVE_POS, holder.pos);
                    editor.commit();

                    notifyDataSetChanged();
                    if (holder.pos == 0) {
                        if (listners != null)
                            listners.updateFb(0);

                    }
                    if (holder.pos == 2) {
                        if (listners != null)
                            listners.updateFb(2);
                    }
                    if (holder.pos == 3) {
                        if (listners != null)
                            listners.updateFb(3);
                    }
                    if (holder.pos == 1) {
                        if (listners != null)
                            listners.updateFb(1);
                    }


                } else {
                    InternetPopUp internetPopUp = new InternetPopUp(activity, 0);
                    internetPopUp.show();
                    internetPopUp.getWindow().setLayout(activity.getResources().getDisplayMetrics().widthPixels * 70 / 100,
                            activity.getResources().getDisplayMetrics().heightPixels * 50 / 100);
                }
            }
        });

        return convertView;
    }

    static class ViewHolder {
        ImageView imShare;
        int pos;
    }


}

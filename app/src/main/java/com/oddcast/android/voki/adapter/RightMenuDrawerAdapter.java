package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.oddcast.android.voki.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc16 on 4/6/16.
 */
public class RightMenuDrawerAdapter extends BaseAdapter {
    Context mContext;
    int[] menuItems, selectedItems;
    ArrayList<HashMap<String, String>> selection;

    public RightMenuDrawerAdapter(Context mContext, int[] menuItems, ArrayList<HashMap<String, String>> selection, int[] selectedItems){
        this.mContext = mContext;
        this.menuItems = menuItems;
        this.selection = selection;
        this.selectedItems = selectedItems;
    }

    @Override
    public int getCount() {
        return menuItems.length;
    }

    @Override
    public Object getItem(int position) {
        return menuItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_right_drawer_menu, null, true);
        }

        ImageView im_menuItems = (ImageView)convertView.findViewById(R.id.im_menuItem);

        if(selection.get(position).get("is_selection").equals("true")) {
            im_menuItems.setImageResource(selectedItems[position]);
        }else{
            im_menuItems.setImageResource(menuItems[position]);
        }

        return convertView;
    }
}

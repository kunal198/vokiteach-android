package com.oddcast.android.voki.adapter;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.database.DatabaseHandler;
import com.oddcast.android.voki.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc16 on 5/2/16.
 */
public class SavedCharacterAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<HashMap<String, String>> savedCharList;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private CharacterListener mListener;
    private SharedPreferences preferences;
    public SavedCharacterAdapter(Context mContext, ArrayList<HashMap<String, String>> savedCharList, CharacterListener mListener) {
        this.mContext = mContext;

        this.savedCharList = savedCharList;
        this.mListener = mListener;
        preferences = mContext.getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);


    }

    @Override
    public int getCount() {
        return savedCharList.size();
    }

    @Override
    public Object getItem(int position) {
        return savedCharList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_cat_specs, null, true);
            holder = new ViewHolder();
            holder.im_icon = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (LinearLayout) convertView.findViewById(R.id.innerContainer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = savedCharList.get(position);

        int newPos = preferences.getInt(Constants.SAVE_CHAR_POS, -1);


        try {
            String folderName = map.get("char_image");
            String fileName = map.get("char_name");
            byte[] imageAsBytes = Base64.decode(folderName.getBytes(), Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
            holder.im_icon.setImageBitmap(bitmap);
            holder.container.setBackgroundColor(Color.parseColor("#ffffff"));

            if (newPos == position) {
                holder.container.setBackgroundResource(R.drawable.save_category_select_background);
            } else {
                holder.container.setBackgroundResource(R.drawable.save_categry_unselect_background);
            }

        } catch (Exception e) {
            Log.e("char_exception", "" + e);
        }
        holder.container.setTag(holder);
        holder.pos = position;


        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(MainActivity.show_save_character){

                    MainActivity.is_there_any_change_in_character=true;
                    scanForActivity(mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.show_save_character=false;
                        ViewHolder holder = (ViewHolder) v.getTag();

                        SharedPreferences.Editor editor;
                        editor = preferences.edit();
                        editor.putInt(Constants.SAVE_CHAR_POS, holder.pos);
                        editor.commit();


                        try {
                            holder.container.setBackgroundResource(R.drawable.save_category_select_background);
                            Constants.EYES_COLOR = null;
                            Constants.MOUTHCOLOR = null;
                            Constants.HAIRCOLOR = null;
                            Constants.SKINCOLOR = null;
                            JSONObject jsonObject = new JSONObject(savedCharList.get(holder.pos).get(DatabaseHandler.CHAR_DATA));
                            Constants.TEMP_SAVED_CATEGORY = jsonObject.getString(DatabaseHandler.CHARACTER_CATEGORY);
                            Constants.TEMP_SAVED_CHARACTER = jsonObject.getString(DatabaseHandler.CHARACTER_NAME);
                            Constants.TEMP_SAVED_BACKGROUND = jsonObject.getString(DatabaseHandler.CHARACTER_BACKGROUND);
                            Constants.TEMP_CHAR_XPOS = jsonObject.getString(DatabaseHandler.CHAR_X_POSITION);
                            Constants.TEMP_CHAR_YPOS = jsonObject.getString(DatabaseHandler.CHAR_Y_POSITION);
                            Constants.EYES_COLOR = jsonObject.getString(DatabaseHandler.EYES_COLOR);
                            Constants.MOUTHCOLOR = jsonObject.getString(DatabaseHandler.MOUTHCOLOR);
                            Constants.HAIRCOLOR = jsonObject.getString(DatabaseHandler.HAIRCOLOR);
                            Constants.SKINCOLOR = jsonObject.getString(DatabaseHandler.SKINCOLOR);
                            Constants.CHARACTERURL = jsonObject.getString(DatabaseHandler.CHARACTER_URL);
                            String[] defaultPos = jsonObject.getString(DatabaseHandler.CHAR_DEFAULT_POS).split("-");

                            Log.e("Constants.CHARACTERURL", "" + Constants.CHARACTERURL);
                            Constants.CURRENT_CHAR_XPOS = defaultPos[0];
                            Constants.CURRENT_CHAR_YPOS = defaultPos[1];

                            ArrayList<HashMap<String, String>> str_Builder = Constants.readXml(mContext, Constants.TEMP_SAVED_CATEGORY,
                                    Constants.TEMP_SAVED_CHARACTER, true);

                            if (mListener != null) {
                                holder.container.setOnClickListener(null);
                                mListener.deleteSavedCharcters(str_Builder, holder.pos);
                            }


                            Log.e("jsonObject.toString() ", "" + jsonObject.toString() + "  " + str_Builder.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        notifyDataSetChanged();
                    }
                });
            }

            }
        });

        return convertView;
    }
    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }
    static class ViewHolder {
        ImageView im_icon;
        LinearLayout container;
        int pos;
    }
}

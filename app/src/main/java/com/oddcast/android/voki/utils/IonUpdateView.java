package com.oddcast.android.voki.utils;

import com.oddcast.android.voki.Modal.FxMenuModel;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 5/17/16.
 */
public interface IonUpdateView {

    public void updateView(String data, String type, ArrayList<FxMenuModel> fxMenuArraylist);
}

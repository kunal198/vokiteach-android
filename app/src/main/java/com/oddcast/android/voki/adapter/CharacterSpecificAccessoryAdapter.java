package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.ParseAccessoryModel;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.utils.Constants;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by brst-pc16 on 4/29/16.
 */
public class CharacterSpecificAccessoryAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<ParseAccessoryModel> specsSpecificCategory;
    Bitmap bmp = null;
    String file_name;
    ImageLoader imageLoader = ImageLoader.getInstance();
    private boolean isRandomAccess;
    private CharacterListener listener;
    private SharedPreferences preferences;
    String accessory="";

    public CharacterSpecificAccessoryAdapter(Context mContext, ArrayList<ParseAccessoryModel> specsSpecificCategory, CharacterListener listner, String file_name, boolean isRandomAccess) {
        this.mContext = mContext;
        this.specsSpecificCategory = specsSpecificCategory;
        this.listener = listner;
        this.file_name = file_name;
        this.isRandomAccess = isRandomAccess;
        preferences = mContext.getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
        accessory="";
        correctFileName();
        characterSelectedFromRandomizer();

        //Log.e("infile_name", file_name);
    }

    @Override
    public int getCount() {
        return specsSpecificCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return specsSpecificCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_cat_specs, null, true);
            holder = new ViewHolder();

            holder.im_icon = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (LinearLayout) convertView.findViewById(R.id.innerContainer);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ParseAccessoryModel model = specsSpecificCategory.get(position);

        if (isRandomAccess) {
            try{

                Random random = new Random();
                showTest(random.nextInt(position));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        Log.d("isRandomAccess", "" + isRandomAccess);


        try {
            if (model.getSelection()=="true") {

                String icon_path = (String) model.getIcon();
                Log.e("icon_path", "" + icon_path);
                String uri = "assets://" + icon_path;
                imageLoader.displayImage(uri, holder.im_icon);
                holder.container.setBackgroundResource(R.drawable.category_selectable_background);
            } else {
            String icon_path = (String) model.getIcon();
                Log.e("icon_path", "" + icon_path);
            String uri = "assets://" + icon_path;
            Log.e("icon_path", "" + icon_path);
            imageLoader.displayImage(uri, holder.im_icon);
            holder.container.setBackgroundResource(R.drawable.unselectable_background);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("file_namezzzzzz", "" + e);
        }
        holder.container.setTag(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.is_there_any_change_in_character=true;
                int pos = (Integer) v.getTag();
                showTest(pos);
                Log.e("clicked", "" + "clicked");
                selectionHelper();
            }
        });

        return convertView;
    }

    public void showTest(int pos) {
        String id = specsSpecificCategory.get(pos).getId();
        Log.e("idddd", "" + id );
        accessory_getter();
        Constants.COSTUME_NAME = specsSpecificCategory.get(pos).getId();
        for (int i = 0; i < specsSpecificCategory.size(); i++) {
            if (specsSpecificCategory.get(i).getId().equalsIgnoreCase(Constants.COSTUME_NAME)) {
                specsSpecificCategory.get(i).setSelection("true");
            } else {
                specsSpecificCategory.get(i).setSelection("false");
            }

        }
        notifyDataSetChanged();
        Log.e("Constants.COSTUME_NAME", "" + Constants.COSTUME_NAME);
        String cat_id = Constants.readAccessoryXml(mContext, file_name, id);

        if (listener != null)
            listener.updateChangeAccessorries(cat_id, file_name);




    }
    //for checking if accesory is selected from randomizer or manually.if selected manually than it will false the value of so that next time accesory will be selected from show test method.
private void accessory_getter()
{
    if (file_name.equalsIgnoreCase("Costume")) {
        accessory= Constants.Accessary_RANDOM_COSTUME;
    } else if (file_name.equalsIgnoreCase("Hair")) {

        accessory=Constants.Accessary_RANDOM_HAIR;
    } else if (file_name.equalsIgnoreCase("Glasses")) {

        accessory=Constants.Accessary_RANDOM_GLASSES;
    } else if (file_name.equalsIgnoreCase("Necklace")) {

        accessory=Constants.Accessary_RANDOM_NECKLACE;
    } else if (file_name.equalsIgnoreCase("Hat")) {

        accessory=Constants.Accessary_RANDOM_HAT;
    } else if (file_name.equalsIgnoreCase("fhair")) {

        accessory=Constants.Accessary_RANDOM_FHAIR;
    } else if (file_name.equalsIgnoreCase("mouth")) {

        accessory=Constants.Accessary_RANDOM_MOUTH;
    } else if (file_name.equalsIgnoreCase("Props")) {

        accessory=Constants.Accessary_RANDOM_PROPS;
    }
    else{
        return;
    }

}
    private void selectionHelper()
    {
        if (file_name.equalsIgnoreCase("Costume")) {
            MainActivity.costume=false;
        } else if (file_name.equalsIgnoreCase("Hair")) {
            MainActivity.hair=false;
        } else if (file_name.equalsIgnoreCase("Glasses")) {
            MainActivity.glasses=false;
        } else if (file_name.equalsIgnoreCase("Necklace")) {
            MainActivity.necklace=false;
        } else if (file_name.equalsIgnoreCase("Hat")) {
            MainActivity.hat=false;
        } else if (file_name.equalsIgnoreCase("fhair")) {
            MainActivity.fhair=false;
        } else if (file_name.equalsIgnoreCase("mouth")) {
            MainActivity.mouth=false;
        } else if (file_name.equalsIgnoreCase("Props")) {
          MainActivity.props=false;
        }
    }
    private void setRandomAccessory()
    {
        for (int i = 0; i < specsSpecificCategory.size(); i++) {
            if (specsSpecificCategory.get(i).getId().equalsIgnoreCase(accessory)) {
                specsSpecificCategory.get(i).setSelection("true");
            } else {
                specsSpecificCategory.get(i).setSelection("false");
            }

        }

    }
    private void characterSelectedFromRandomizer()
    {
        if (MainActivity.character_selection_way_is_randomiser)
        {
            if (MainActivity.costume&&file_name.equalsIgnoreCase("Costume")) {
                Log.d("check_click","costume");
                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.hair&&file_name.equalsIgnoreCase("Hair")) {
                Log.d("check_click","hair");
                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.hat&&file_name.equalsIgnoreCase("Hat")){
                Log.d("check_click","hat");
                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.fhair&&file_name.equalsIgnoreCase("fhair")){
                Log.d("check_click","fhair");

                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.props&&file_name.equalsIgnoreCase("Props")){
                Log.d("check_click","props");

                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.glasses&&file_name.equalsIgnoreCase("Glasses")){
                Log.d("check_click","glasses");

                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.necklace&&file_name.equalsIgnoreCase("Necklace")){
                Log.d("check_click","necklace");

                accessory_getter();
                setRandomAccessory();
            }
            else if (MainActivity.mouth&&file_name.equalsIgnoreCase("mouth")){
                Log.d("check_click","mouth");
                accessory_getter();
                setRandomAccessory();
            }

        }
    }
    private void correctFileName()
    {
        if (file_name.equalsIgnoreCase("Clothing")) {
            file_name = "Costume";
        }
        else if (file_name.equalsIgnoreCase("Bling")) {
            file_name = "Necklace";
        }else if (file_name.equalsIgnoreCase("Facial Hair")) {
            file_name = "fhair";

        }
        else if (file_name.equalsIgnoreCase("mouth")) {
            file_name = "mouth";

        }
    }
    static class ViewHolder {
        ImageView im_icon;
        LinearLayout container;
    }
}

package com.oddcast.android.voki.utils;

import android.util.Log;
import android.util.Xml;

import com.oddcast.android.voki.MainActivity;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;

public class Extra {

    /*@"English:1",
    @"Spanish:2",
    @"German:3",
    @"French:4",
    @"Catalan:5",
    @"Portuguese:6",
    @"Italian:7",
    @"Greek:8",
    @"Swedish:9",
    @"Chinese:10",
    @"Dutch:11",
    @"Japanese:12",
    @"Korean:13",
    @"Polish:14",
    @"Galician:15",
    @"Turkish:16",
    @"Danish:19",
    @"Norwegian:20",
    @"Russian:21",
    @"Finnish:23",
    @"Arabic:27",
    @"Romanian:30",
    @"Esperanto:31",*/

/*
    "@"1:Alan (Australian):9:2",
    @"1:Allison (US):7:2",
    @"1:Catherine (UK):6:2",
    @"1:Dave (US):2:2",
    @"1:Elizabeth (UK):4:2",
    @"1:Grace (Australian):10:2",
    @"1:Simon (UK):5:2",
    @"1:Steven (US):8:2",
    @"1:Susan (US):1:2",
    @"1:Veena (Indian):11:2",
    @"1:Kate (US):1:3",
    @"1:Paul (US):2:3",
    @"1:Julie (US):3:3",
    @"1:Bridget (UK):4:3",
    @"1:Hugh (UK):5:3",
    @"1:Ashley (US):6:3",
    @"1:James (US):7:3",
    @"2:Violeta:1:3",
    @"2:Francisco (Mexican):2:3",
    @"2:Carmen (Castilian):1:2",
    @"2:Juan (Castilian):2:2",
    @"2:Francisca (Chilean):3:2",
    @"2:Diego (Argentine):4:2",
    @"2:Esperanza (Mexican):5:2",
    @"2:Jorge (Castilian):6:2",
    @"2:Carlos (American):7:2",
    @"2:Soledad (American):8:2",
    @"2:Leonor (Castilian):9:2",
    @"2:Ximena:10:2",
    @"3:Stefan:2:2",215HelloIamVishalSharmamp35603124evt6bu4n!sDk
    @"3:Katrin:3:2",216HelloIamVishalSharmamp35603124evt6bu4n!sDk
    @"4:Bernard:2:2",
    @"4:Jolie:3:2",
    @"4:Chloe (Canadian):1:3",
    @"4:Florence:4:2",
    @"4:Charlotte:5:2",
    @"4:Olivier:6:2",
    @"5:Montserrat:1:2",
    @"5:Jordi:2:2",
    @"5:Empar (Valencian):3:2",
    @"6:Gabriela (Brazilian):1:2",
    @"6:Amalia (European):2:2",
    @"6:Eusebio (European):3:2",
    @"6:Fernanda (Brazilian):4:2",
    @"6:Felipe(Brazilian):5:2",
    @"7:Paola:1:2",
    @"7:Silvana:2:2",
    @"7:Valentina:3:2",
    @"7:Luca:5:2",
    @"7:Marcello:6:2",
    @"7:Roberto:7:2",
    @"7:Matteo:8:2",
    @"7:Giulia:9:2",
    @"7:Federica:10:2",
    @"8:Afroditi:1:2",
    @"8:Nikos:3:2",
    @"9:Annika:1:2",
    @"9:Sven:2:2",
    @"10:Linlin (Mandarin):1:2",
    @"10:Lisheng(Mandarin):2:2",
    @"10:Lily (Mandarin):1:3",
    @"10:Hui (Mandarin):3:3",
    @"10:Liang (Mandarin):4:3",
    @"11:Willem:1:2",
    @"11:Saskia:2:2",
    @"12:Show:2:3",
    @"12:Misaki:3:3",
    @"13:Yumi:1:3",
    @"13:Junwoo:2:3",
    @"14:Zosia:1:2",
    @"14:Krzysztof:2:2",
    @"15:Carmela:1:2",
    @"16:Kerem:1:2",
    @"16:Zeynep:2:2",
    @"16:Selin:3:2",
    @"19:Frida:1:2",
    @"19:Magnus:2:2",
    @"20:Vilde:1:2",
    @"20:Henrik:2:2",
    @"21:Olga:1:2",
    @"21:Dmitri:2:2",
    @"23:Milla:1:2",
    @"23:Marko:2:2",
    @"27:Tarik:1:2",
    @"27:Laila:2:2",
    @"30:Ioana:1:2",
    @"31:Ludoviko:1:2","

    */

    // TTs
    /*<SAVESCENE PHPSESSID="095dfbfd46a3dfac97d0bed789b8a249" TYPE="3" CID="6" ACCOUNTID="37533" PARTNERID="16">
    <SCENE TITLE="" ID="0">
    <SKIN CATID="57" ID="124" />
    <HOST CVERSION="2" XPOS="0" YPOS="-.15" SCALE="0.65"
    URL="oh/492/32398/4912/4915/0/0/0/32474/0/0/0/oh.swf?cs=85f2bd3:5151494:e1f9ab4:94240c0:3da9c04:88:118:56:130:94:0:50:70:0" />
    <BG CATID="53" ID="303435" />
    <AUDIO>
    <TTS VOICE="1" LANG="1" ENGINE="3">This%2520is%2520testing%2520123%252E</TTS>
    </AUDIO>
    <VOKI CEL="0" CHT="0" EML="0" ABT="0" HST="0" CMT="0" ONLOAD="0" CNT="0" />
    </SCENE>
    </SAVESCENE>*/


    public static StringWriter createXmlFile(String session, String character_url, String text_msg, String text_lang, String voice_type, String backgound_id, String fxtype, String fx_level) {
        String lang_id = "", char_name = "", voice_id = "", engine_id = "";
        String[] strArr = {"1:Alan (Australian):9:2", "1:Allison (US):7:2", "1:Catherine (UK):6:2", "1:Dave (US):2:2", "1:Elizabeth (UK):4:2",
                "1:Grace (Australian):10:2", "1:Simon (UK):5:2", "1:Steven (US):8:2", "1:Susan (US):1:2", "1:Veena (Indian):11:2", "2:Carmen (Castilian):1:2",
                "2:Juan (Castilian):2:2", "2:Francisca (Chilean):3:2", "2:Diego (Argentine):4:2", "2:Esperanza (Mexican):5:2", "2:Jorge (Castilian):6:2",
                "2:Carlos (American):7:2", "2:Soledad (American):8:2", "2:Leonor (Castilian):9:2", "2:Ximena:10:2", "3:Stefan:2:2", "3:Katrin:3:2",
                "4:Bernard:2:2", "4:Jolie:3:2", "4:Florence:4:2", "4:Charlotte:5:2", "4:Olivier:6:2", "5:Montserrat:1:2", "5:Jordi:2:2", "5:Empar (Valencian):3:2",
                "6:Gabriela (Brazilian):1:2", "6:Amalia (European):2:2", "6:Eusebio (European):3:2", "6:Fernanda (Brazilian):4:2", "6:Felipe (Brazilian):5:2",
                "7:Paola:1:2", "7:Silvana:2:2", "7:Valentina:3:2", "7:Luca:5:2", "7:Marcello:6:2", "7:Roberto:7:2", "7:Matteo:8:2", "7:Giulia:9:2", "7:Federica:10:2",
                "8:Afroditi:1:2", "8:Nikos:3:2", "9:Annika:1:2", "9:Sven:2:2", "10:Linlin (Mandarin):1:2", "10:Lisheng (Mandarin):2:2", "11:Willem:1:2",
                "11:Saskia:2:2", "14:Zosia:1:2", "14:Krzysztof:2:2", "15:Carmela:1:2", "16:Kerem:1:2", "16:Zeynep:2:2", "16:Selin:3:2", "19:Frida:1:2",
                "19:Magnus:2:2", "20:Vilde:1:2", "20:Henrik:2:2", "21:Olga:1:2", "21:Dmitri:2:2", "23:Milla:1:2", "23:Marko:2:2", "27:Tarik:1:2", "27:Laila:2:2",
                "30:Ioana:1:2", "31:Ludoviko:1:2", "1:Kate (US):1:3", "1:Paul (US):2:3", "1:Julie (US):3:3", "1:Bridget (UK):4:3", "1:Hugh (UK):5:3", "1:Ashley (US):6:3",
                "1:James (US):7:3", "2:Violeta:1:3", "2:Francisco (Mexican):2:3", "4:Chloe (Canadian):1:3", "10:Lily (Mandarin):1:3", "10:Hui (Mandarin):3:3",
                "10:Liang (Mandarin):4:3", "12:Show:2:3", "12:Misaki:3:3", "13:Yumi:1:3", "13:Junwoo:2:3"};

        if (!voice_type.isEmpty()) {
            for (int k = 0; k < strArr.length; k++) {
                if (strArr[k].contains(voice_type)) {
                    String[] arrStr = strArr[k].split(":");
                    lang_id = arrStr[0];
                    char_name = arrStr[1];
                    voice_id = arrStr[2];
                    engine_id = arrStr[3];

                }

            }


        } else {
            lang_id = "1";
            voice_id = "3";
            engine_id = "3";
        }

        if (fxtype==null)
        {
            fx_level="0";
            fx_level="0";
        }
        Log.d("fxMenuResponse", "" + Constants.fxMenuResponse);
        Log.d("langid", "" + lang_id);

        Log.d("char_name", "" + char_name);
        Log.d("voice_id", "" + voice_id);
        Log.d("engine_id", "" + engine_id);

        Log.d("text_msg", "" + text_msg);
        if (text_msg.isEmpty()) {
            text_msg = "Share voki";
        }
        if (backgound_id.isEmpty()) {
            backgound_id = "1464235";
        }
    /*    if (voice_type.isEmpty()) {
            voice_type = "3";
        }
        if (text_lang.isEmpty()) {
            text_lang = "1";
        }*/
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        Log.d("sesion+url", "" + session + "url" + character_url);
        try {


            xmlSerializer.setOutput(writer);

            // start DOCUMENT
            //   xmlSerializer.startDocument("UTF-8", true);
            // open tag: <record>
            xmlSerializer.startTag("", "SAVESCENE");
            xmlSerializer.attribute("", "PHPSESSID", session);
            xmlSerializer.attribute("", "TYPE", "3");
            xmlSerializer.attribute("", "CID", "6");
            xmlSerializer.attribute("", "ACCOUNTID", "5977354");
            xmlSerializer.attribute("", "PARTNERID", "16");
            // open tag: <study>

            xmlSerializer.startTag("", "PARTNER");
            xmlSerializer.attribute("", "partner_thw", "267");
            xmlSerializer.attribute("", "partner_thh", "267");
            xmlSerializer.endTag("", "PARTNER");

            xmlSerializer.startTag("", "SCENE");
            xmlSerializer.attribute("", "TITLE", "myvoki");
            xmlSerializer.attribute("", "ID", "0");

            // open tag: <topic>
            xmlSerializer.startTag("", "SKIN");
            xmlSerializer.attribute("", "CATID", "57");
            xmlSerializer.attribute("", "ID", "124");

            xmlSerializer.endTag("", "SKIN");

            xmlSerializer.startTag("", "HOST");
            xmlSerializer.attribute("", "CVERSION", "2");
            xmlSerializer.attribute("", "XPOS", Constants.XPOS_FOR_SHARING_CHARACTER);
            xmlSerializer.attribute("", "YPOS", Constants.YPOS_FOR_SHARING_CHARACTER);
            xmlSerializer.attribute("", "SCALE", Constants.SCALE_FOR_SHARING_CHARACTER);
            xmlSerializer.attribute("", "URL", character_url);
            xmlSerializer.endTag("", "HOST");

            xmlSerializer.startTag("", "BG");
            xmlSerializer.attribute("", "CATID", "53");
            xmlSerializer.attribute("", "ID", backgound_id);
            xmlSerializer.endTag("", "BG");



           /* <TTS VOICE="3" LANG="1" ENGINE="3">voki.com</TTS>*/

            if (MainActivity.what_to_share_now.equals("SHARE_TEXT_AUDIO")) {
                xmlSerializer.startTag("", "AUDIO");
                xmlSerializer.attribute("", "PLAY", "load");
                xmlSerializer.startTag("", "TTS");
                 xmlSerializer.attribute("", "FX", fxtype+fx_level);
                xmlSerializer.attribute("", "VOICE", voice_id);
                xmlSerializer.attribute("", "LANG", lang_id);
                xmlSerializer.attribute("", "ENGINE", engine_id);
                xmlSerializer.text(text_msg);
                xmlSerializer.endTag("", "TTS");
                xmlSerializer.endTag("", "AUDIO");
            } else if (MainActivity.what_to_share_now.equals("SHARE_RECORDING")){
                xmlSerializer.startTag("", "AUDIO");
                xmlSerializer.attribute("", "PLAY", "load");
                xmlSerializer.attribute("", "ID", MainActivity.getAudioId);
                xmlSerializer.endTag("", "AUDIO");
            }
            else{
                xmlSerializer.startTag("", "AUDIO");
                xmlSerializer.attribute("", "PLAY", "load");
                xmlSerializer.startTag("", "TTS");
                 xmlSerializer.attribute("", "FX", fxtype+fx_level);
                xmlSerializer.attribute("", "VOICE", voice_id);
                xmlSerializer.attribute("", "LANG", lang_id);
                xmlSerializer.attribute("", "ENGINE", engine_id);
                xmlSerializer.text("Hello Voki");
                xmlSerializer.endTag("", "TTS");
                xmlSerializer.endTag("", "AUDIO");
            }

            xmlSerializer.startTag("", "VOKI");
            xmlSerializer.attribute("", "CEL", "0");
            xmlSerializer.attribute("", "CHT", "0");
            xmlSerializer.attribute("", "EML", "0");
            xmlSerializer.attribute("", "ABT", "0");
            xmlSerializer.attribute("", "HST", "0");
            xmlSerializer.attribute("", "CMT", "0");
            xmlSerializer.attribute("", "ONLOAD", "1");
            xmlSerializer.attribute("", "CNT", "0");
            xmlSerializer.endTag("", "VOKI");

            xmlSerializer.endTag("", "SCENE");
            xmlSerializer.endTag("", "SAVESCENE");


            // end DOCUMENT
            xmlSerializer.endDocument();


            Log.e("xmlwriter", "" + writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }


}

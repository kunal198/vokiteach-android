package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.AccessoryListModel;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.utils.Constants;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.WeakHashMap;

/**
 * Created by brst-pc16 on 4/5/16.
 */
public class CharacterAccessoryAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<AccessoryListModel> charactersCategory;
    int select_pos;
    int newPosition = -1;
    ViewHolder holder;
    String folderName;

    ArrayList<HashMap<String, String>> arr = new ArrayList<>();

    boolean isAccSame = false;
    private CharacterListener listner;

    ImageLoader imageLoader = ImageLoader.getInstance();
    private boolean isRandomAccess = false;

    private WeakHashMap<Integer, WeakReference<View>> hashMap = new WeakHashMap<Integer, WeakReference<View>>();

    public CharacterAccessoryAdapter(Context mContext, ArrayList<AccessoryListModel> charactersCategory, CharacterListener listner, boolean isRandomAccess) {
          Log.d("charactersCategory",""+charactersCategory);

        this.mContext = mContext;
        this.charactersCategory = charactersCategory;
        this.listner = listner;
        this.isRandomAccess = isRandomAccess;
    }

    @Override
    public int getCount() {
        return charactersCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return charactersCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_characters, null, true);

            holder = new ViewHolder();

            holder.im_characters = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.innerContainer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        hashMap.put(position, new WeakReference<View>(convertView));

        AccessoryListModel accListModel = charactersCategory.get(position);
        try {

            if (accListModel.getSelection().equals("true")) {
                String name = accListModel.getFile();
                String[] str = name.split("_");
                folderName = str[1];
                String imgName = (String) accListModel.getSelectedImage();
                boolean isActivated = accListModel.getIsActivated();
                String uri = "assets://img/wears/selected/" + name + "/btn_acc.png";
                imageLoader.displayImage(uri, holder.im_characters);

            } else {
                String name = accListModel.getFile();
                String[] str = name.split("_");
                folderName = str[1];//map.get("file_name");
                String imgName = (String) accListModel.getUnselectedImage();
                boolean isActivated = accListModel.getIsActivated();
                Log.e("inADapter", folderName);

                String uri = "assets://img/wears/unselected/" + name + "/btn_acc.png";
                imageLoader.displayImage(uri, holder.im_characters);
            }

            Log.e("accListModel.getFile()", "123 " + accListModel.getFile());

            String f_name = (String) accListModel.getFile();//map.get("file");

            String hmap = Constants.check_list.get(position);
            if (hmap.equals("true")) {
                holder.container.setBackgroundColor(Color.TRANSPARENT);
                Log.e("in_if", "" + hmap);
                holder.im_characters.setEnabled(true);
            } else {
                Log.e("in_else", "" + hmap);
                holder.im_characters.setEnabled(false);
                holder.container.setBackgroundColor(Color.parseColor("#66000000"));
            }

            if (isRandomAccess) {
                Random random = new Random();
                showSpecsList(random.nextInt(position));
            }

            Log.e("select_array", "" + charactersCategory);
            holder.im_characters.setTag(position);
            holder.im_characters.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    select_pos = pos;
                    showSpecsList(select_pos);
                }
            });

            holder.im_characters.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = (Integer) v.getTag();
                    String[] file = charactersCategory.get(pos).getFile().split("_");
                    Toast.makeText(mContext, file[1], Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("inADapter", "" + e);
        }
        return convertView;
    }

    public void showSpecsList(int positions) {

            if (newPosition == positions) {

                charactersCategory.get(newPosition).setSelection("true");

                if (isAccSame) {
                    isAccSame = false;
                } else {
                    isAccSame = true;
                }
            } else {
                Log.e("inside else", "" + newPosition + "---" + positions);

                charactersCategory.get(positions).setSelection("true");
                if (newPosition > -1) {

                    charactersCategory.get(positions).setSelection("false");
                }
                isAccSame = false;
            }

            if (newPosition == -1) {
                charactersCategory.get(positions).setSelection("true");
                isAccSame = false;
            } else {
                charactersCategory.get(positions).setSelection("true");

                if (positions != newPosition) {
                    charactersCategory.get(newPosition).setSelection("false");
                    isAccSame = false;
                } else {
                    if (isAccSame) {
                        isAccSame = false;
                    } else {
                        isAccSame = true;
                    }

                }
            }

        notifyDataSetChanged();
        newPosition = positions;
        String[] file = charactersCategory.get(positions).getFile().split("_");
        String newFile = file[1];



        Log.e("name_file", "" + file + "---" + newFile + " ");
        if (listner != null)
            listner.showAccessorries(newFile, isAccSame);

    }


    static class ViewHolder {
        ImageView im_characters;
        RelativeLayout container;
    }
}

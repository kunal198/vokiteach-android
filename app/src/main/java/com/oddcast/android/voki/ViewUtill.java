package com.oddcast.android.voki;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by brst-pc20 on 6/20/16.
 */
public class ViewUtill {


    public ViewUtill(Activity activity) {

    }

    public static HashMap convertMap(String key1, String value1, String key2, String value2) {

//        String url = URL + "Meal/meals";
        HashMap<String, String> map = new HashMap<>();
        if (value1.length() > 0)
            map.put(key1, value1);

        map.put(key2, value2);

        Log.d("map", "" + map.toString());
        return map;
    }

    public static String performPostCall(String requestURL,
                                         HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        Log.d("postresult", result.toString());
        return result.toString();
    }




    public static boolean haveNetworkConnection(Context context)
    {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo)
        {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
            {
                if (ni.isConnected())
                {
                    haveConnectedWifi = true;
                    Log.v("WIFI CONNECTION ", "AVAILABLE");
                } else
                {
                    Log.v("WIFI CONNECTION ", "NOT AVAILABLE");
                }
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
            {
                if (ni.isConnected())
                {
                    haveConnectedMobile = true;

                } else
                {

                }
            }
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    public void facebookIntialization(ShareDialog shareDialog,CallbackManager callbackManager, final Context context) {


        //LoginManager.getInstance().logInWithReadPermissions(context, Arrays.asList("public_profile", "user_friends", "read_custom_friendlists"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(
                            AccessToken oldAccessToken,
                            AccessToken currentAccessToken) {

                    }
                };
                String accessToken = AccessToken.getCurrentAccessToken().getToken();
                Log.d("tokenis", "" + accessToken);

                GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("jsonobjectt", "" + object);
                        Log.d("graphresponse", "" + response);
                        String data = object.toString();
                        try {
                            JSONObject jsonObject = new JSONObject(data);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle bundle = new Bundle();
                bundle.putString("fields", "id,birthday,link,name,gender,last_name");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();

            }
        });
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("My Voki has something to say - check it out:\n")
                    .setContentDescription("To create your own Voki go to - www.voki.com")
                    .setContentUrl(Uri.parse(MainActivity.final_url_for_sharing))
                    .build();
            Log.d("final_url_for_sharing", "" + MainActivity.final_url_for_sharing);
            shareDialog.show(linkContent);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {


                @Override
                public void onSuccess(Sharer.Result result) {
                    Log.d("tagg", "onSuccess");
                    Toast.makeText(context, "Your Voki has been shared", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onCancel() {
                    //do something
                    Log.d("tagg", "oncancel");
                }

                @Override
                public void onError(FacebookException error) {
                    // TODO Auto-generated method stub

                    Log.d("tagg", "onerror");
                }

            });
            //Toast.makeText(context, "Sucessfully posted on Facebook", Toast.LENGTH_SHORT).show();

        }
    }

}

package com.oddcast.android.voki;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.Async_Thread.Super_AsyncTask;
import com.oddcast.android.voki.Async_Thread.Super_AsyncTask_Interface;
import com.oddcast.android.voki.Modal.AccessoryListModel;
import com.oddcast.android.voki.Modal.CharLanguageModel;
import com.oddcast.android.voki.Modal.CharacterListModel;
import com.oddcast.android.voki.Modal.FxMenuModel;
import com.oddcast.android.voki.Modal.LanguageModel;
import com.oddcast.android.voki.Modal.ParseAccessoryModel;
import com.oddcast.android.voki.Modal.PlaybackAudioModel;
import com.oddcast.android.voki.Modal.ShowCategoryModel;
import com.oddcast.android.voki.Modal.WallpaperListModel;
import com.oddcast.android.voki.PopUp.DeletePopUp;
import com.oddcast.android.voki.PopUp.InternetPopUp;
import com.oddcast.android.voki.SocialIntegrations.GooglePlusSignIn;
import com.oddcast.android.voki.SocialIntegrations.TwitterIntegration;
import com.oddcast.android.voki.adapter.CategorySpcesAdapter;
import com.oddcast.android.voki.adapter.CharLanguageAdapter;
import com.oddcast.android.voki.adapter.CharacterAccessoryAdapter;
import com.oddcast.android.voki.adapter.CharacterSpecificAccessoryAdapter;
import com.oddcast.android.voki.adapter.FxMenuListAdapter;
import com.oddcast.android.voki.adapter.GridShareAdapter;
import com.oddcast.android.voki.adapter.LanguageAdapter;
import com.oddcast.android.voki.adapter.ListCharacterAdapter;
import com.oddcast.android.voki.adapter.RightMenuDrawerAdapter;
import com.oddcast.android.voki.adapter.SavedCharacterAdapter;
import com.oddcast.android.voki.adapter.WallpaperCategoryAdapter;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.custom_listeners.IonListners;
import com.oddcast.android.voki.custom_listeners.UploadAudioListener;
import com.oddcast.android.voki.database.DatabaseHandler;
import com.oddcast.android.voki.utils.Constants;
import com.oddcast.android.voki.utils.Extra;
import com.oddcast.android.voki.utils.IonUpdateView;
import com.oddcast.android.voki.utils.T;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import mp3.Main;

public class MainActivity extends AbsRuntimeMarshmallowPermission implements View.OnClickListener, ColorPickerView.OnColorChangedListener,
        View.OnLongClickListener, IonUpdateView, CharacterListener, Handler.Callback, IonListners, SensorEventListener, UploadAudioListener {
    float margin_xllarge, zero, high_nano, margin_xlarge, grand_size, margin_xsmall, margin_small, medium_grand_size, margin_verybig, on_showchar;
    public static boolean show_save_character = true;
    float minusno = 0;
    String url_to_be_played = "";
    boolean sensor_play1 = true, sensor_play2 = true;
    boolean save_char = true;
    public static String picturePath = "";
    private SensorManager sensorMan;
    private Sensor accelerometer;
    private ProgressBar progress_bar;
    static Handler handlerr = new Handler();
    public static int UNIQUE_KEY = 1;
    //ArrayList<>
    private final int SET_LISTCHARCTER_ADAPTER = 2;
    private final int UPDATE_LISTCAHRCTER_UI = 3;
    private String check_url_for_same_text = "";
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    String img_str = null;
    Animation enter_from_right, enter_from_right1, exit_to_right, enter_from_left, exit_to_left, exit_left, enter_left;
    private final int SHOW_ANIMATION = 4;
    private final int POPULATE_LIST = 5;
    private Boolean initializetts_menu = false;

    volatile TransparentProgressDialog transDialog;
    volatile public static int MEDIA_TYPE_IMAGE = 11;
    volatile private static int GALLERY_CODE = 201;
    volatile private static String IMAGE_DIRECTORY_NAME = "Hello Camera";
    volatile public static RelativeLayout menuContainer, menuContainer2, menuContainer3, menuLibrary, rightDrawerLayout,
            colorPickerContainer, parentLayout, webViewContainer, listContainer, rightMenuContainer, saveCharLay,
            cat_menu;
    volatile public static ImageView hideShowMenu, hideShowMenu2, hideShowMenu3, im_library, im_logo, im_drawer,
            im_share, im_hideDrawer, im_Option1, im_Option2, im_Option3, im_Option4, im_Option5,
            im_catUp, im_catDown, im_catUp2, im_catDown2;

    volatile ImageView im_mouth, im_eyes, im_skin, im_hair;
    boolean sensor_handler = true;
    volatile RelativeLayout rel_container_1, rel_container_2, rel_container_3, rel_container_4, innerColorContainer;
    public static boolean character_selection_way_is_randomiser = false;
    volatile public static ListView lv_characterSpecs;
    volatile public static WebView webView;
    volatile public static String isSelectedKey = "select";
    volatile public static boolean isFirstTime = false;
    volatile public static Bitmap bmp;
    volatile public static boolean isUpClicked = false;
    volatile public static boolean isLibraryClicked = false;
    volatile public static boolean isSaveLibraryClicked = false;
    volatile public static String cat_select;
    volatile public static ArrayList<HashMap<String, String>> wearsCategory;
    volatile public static ArrayList<HashMap<String, String>> specsCategory;
    volatile public static ArrayList<HashMap<String, String>> accessorySpecsCategory;
    volatile public static ArrayList<ParseAccessoryModel> accessoryList;
    volatile public static ArrayList<HashMap<String, String>> wallpaperCategory;
    volatile public static CharacterAccessoryAdapter accessoryAdapter;
    volatile public static CategorySpcesAdapter catSpecsAdapter;
    volatile public static CharacterSpecificAccessoryAdapter charSpecificAdapter;
    volatile public static ArrayList<HashMap<String, String>> colorCatList;
    volatile public Context context;
    volatile ImageView im_saveChar;
    volatile AssetManager assetManager;
    volatile ListView lv_charactersCat, lv_rightDrawer;
    volatile LinearLayout lv_Characters;
    volatile boolean isOpen = false;
    volatile TextView tv_content, tv_contentTitle, tv_content_email;
    volatile ArrayList<Bitmap> imagesArray;
    volatile boolean isDownClicked = false;
    volatile boolean isUpClicked2 = false;
    volatile boolean isDownClicked2 = false;
    volatile int rightPosition = -1;
    volatile ArrayList<HashMap<String, String>> charactersCategory;
    volatile ArrayList<HashMap<String, String>> shareList;
    volatile ArrayList<HashMap<String, String>> menuItems;
    static int dp33;
    volatile ArrayList<ShowCategoryModel> charactersCatList;
    private IonListners mListners;
    private UploadAudioListener uploadAudioListener;
    private int counter_for_saved_character = 0;
    volatile ListCharacterAdapter listAdapter;
    volatile SavedCharacterAdapter savedCharAdapter;
    volatile RightMenuDrawerAdapter rightMenuAdapter;
    volatile GridShareAdapter gridShareAdapter;
    volatile WallpaperCategoryAdapter wallCatAdapter;
    volatile int[] rightMenuImages = {R.drawable.about_unselected, R.drawable.terms_unselected, R.drawable.faq_unselected};
    volatile int[] rightMenuImagesSelected = {R.drawable.about_selected, R.drawable.terms_selected, R.drawable.faq_selected};

    volatile int nextPos = 0;
    //volatile MarshMallowPermission marshPermission;
    volatile String htmlTerms = "<font color=\"white\" size=\"5\">By accessing, registering or using the Voki website and/or the Voki mobile application (collectively referred to as “Voki” ), you agree to the </font>";
    String str = "<a href=\"http://www.voki.com\">www.voki.com</a> ";
    volatile String link = "<a href=\"http://www.voki.com/tos\">Voki Terms of Service</a> and ";
    volatile String link2 = "<a href=\"http://www.voki.com/privacy\">Privacy Policy</a></font>.";
    volatile String link3 = "support@voki.com";
    volatile String html3 = "<font color=\"white\"><br> If under the age of 13, you must have permission from your parent or guardian to use Voki.If you have any questions, please contact our customer support team at </font>";
    volatile String html4 = "<font color=\"white\"><br>Build no:21/font>";

    volatile File mediaFile;
    volatile DatabaseHandler dataHandler;
    // Color Picker ....
    volatile private ColorPickerView mColorPickerView;
    volatile ArrayList<String> checkAvailList;
    volatile ArrayList<HashMap<String, String>> charColorList;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1001;

    volatile public String isHairClicked = "hair_clicked", isEyesClicked = "eyes_clicked", isSkinClicked = "skin_clicked",
            isMouthClicked = "mouth_clicked";
    volatile public String isColorAcc = "skin_clicked";
    volatile boolean icColorChanging = false;
    volatile String getColor;
    volatile int initHairColor, initEyeColor, initMouthColor, initSkinColor;
    volatile ImageView im_tts, im_voice;

    /**
     * Models ......
     */
    volatile ShowCategoryModel showModel;
    volatile public static CharacterListModel charListModel;
    volatile AccessoryListModel accListModel;
    volatile WallpaperListModel wallpaperListModel;

    volatile public static ArrayList<CharacterListModel> finalCharList;
    volatile ArrayList<AccessoryListModel> accessoriesList;
    volatile public static ArrayList<WallpaperListModel> wallpapersList;

    volatile View innerTtsView, innerVoiceView;
    volatile RelativeLayout optionContainer;

    volatile boolean stopped;


    volatile int MAX_DURATION = 60;
    volatile ProgressBar progressBar;
    volatile View newview;
    volatile boolean isRecordClicked = false;
    volatile static String newType;


    volatile static ArrayList<LanguageModel> languageList = null;
    volatile static ArrayList<CharLanguageModel> charlanguageList = null;

    volatile private IonUpdateView ioListner;
    volatile TextView tv_language;
    volatile TextView tv_voiceType, progress_text;
    volatile TextView tv_fx;
    int where_to_share_position = -1;

    volatile public static String isButtonClicked = "";
    volatile public static String FIRSTClicked = "first_clicked";
    volatile public static String SECONDClicked = "second_clicked";
    volatile public static String THIRDClicked = "third_clicked";
    volatile public static String FORTHClicked = "forth_clicked";
    volatile public static String FIFTHClicked = "fifth_clicked";
    volatile public static String PREVIOUSClicked = "";
    volatile CharacterListener listener;
    volatile static String accFileName;
    volatile EditText et_typeMessage;
    volatile String typedMessage;

    volatile File sourceFile;

    volatile public static String isRecording = "";
    volatile public static String inProgress = "progress";
    volatile public static String recStopped = "stoped";

    volatile String responseString = null;
    volatile HttpPost httppost;
    volatile HttpClient httpclient;
    volatile AndroidMultiPartEntity entity;
    String temp_file_name_kitkat = "";
    volatile ListView lv_fxMenu;
    volatile String base64;
    volatile static float screenHeight, screenWidth, first_cat;
    volatile public static float st_pos_width = 0;
    volatile public static float st_pos_height = 0;

    volatile RelativeLayout accMenu;
    volatile RelativeLayout wallMenu;
    volatile RelativeLayout fxLayout;
    volatile RelativeLayout voiceMenu;
    volatile RelativeLayout shareMenu;
    volatile static RelativeLayout selectedAccMenu;
    volatile ListView lv_accList;
    volatile ListView lv_backgrounList;
    volatile static ListView lv_selectedAccessory;
    volatile GridView shareGridView;
    volatile ImageView im_accUp;
    volatile ImageView im_accDown;
    volatile static ImageView im_accHideBtn;
    volatile ImageView im_wallUp;
    volatile ImageView im_wallDown;
    volatile ImageView im_wallHideBtn;
    volatile ImageView im_chooseBack;
    volatile ImageView im_voiceHideBtn;
    volatile ImageView im_shareHideBtn;
    volatile static ImageView im_selectAccHideBtn;
    volatile static ImageView im_accListUp;
    volatile static ImageView im_accListDown;
    volatile ShowCategoryModel mod;
    volatile ImageView imDice;
    static ImageView im_playButton;
    volatile Handler mHandler;
    public String default_voice_id = "3";
    private String capture_thumbnail = "";

    volatile ImageLoader imageLoader = ImageLoader.getInstance();
    volatile ArrayList<PlaybackAudioModel> defaultAudioList;
    volatile ArrayList<PlaybackAudioModel> femaleAudioList;
    volatile ArrayList<PlaybackAudioModel> maleAudioList;
    volatile ArrayList<PlaybackAudioModel> othersAudioList;
    private int dicePosition = 0;
    private int dicePosition11 = 0;
    private boolean isRandom = false;
    int isVisiblePos;
    Timer timer = new Timer();
    String fx_type = null, fx_level = null;
    private final int[] diceImages = new int[]{R.drawable.btn_randomize_anim01, R.drawable.btn_randomize_anim02, R.drawable.btn_randomize_anim03, R.drawable.btn_randomize_anim04, R.drawable.btn_randomize_anim05, R.drawable.btn_randomize_anim06, R.drawable.btn_randomize_anim07, R.drawable.btn_randomize_anim08};
    private Drawable dice[] = new Drawable[8];
    File destination = null;
    private final Random randomGen = new Random();
    private int diceSum;
    private int roll[] = new int[]{8, 8};
    private Thread thread = null;
    private Resources res;
    private final int rollAnimations = 50;
    private final int delayTime = 20;
    public static boolean ISANIM = true;
    private boolean isRandmAccess = true;
    public static boolean isCharacterchangedagainForColorPallete = false;
    private static float xPos, yPos;
    private int timeVal = 0;
    private TextView tvCountDownTime, tvRecordTitle;
    private Dialog myDialog;
    volatile private boolean api_link_flag = false;
    private String str_session_key = "";
    volatile String final_save_xml;
    volatile public static String final_url_for_sharing = "";
    private boolean character_saving = false;
    private boolean uploading_background = false;
    private String backgound_id = "";
    private boolean isMenuOpen = true;
    private String text_to_speak_by_character = " ", voicetype_to_speak_by_character = "", language_to_speak_by_character = "";
    private int femalePos = 0, otherPos = 0, malePos = 0;
    private ProgressDialog progress;
    private ArrayList<HashMap<String, String>> charList = new ArrayList<HashMap<String, String>>();
    private float screenWidthDp, screenHeightDp;
    private boolean randomchecker = true;
    private AudioRecord recorder = null;
    private boolean isRecording1 = false;
    private Thread recordingThread = null;
    public static boolean is_there_any_change_in_character = true;
    public static String what_to_share_now = "";
    /*
    Changes for mp3 Funcationality*/
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    public static final String AUDIO_RECORDER_FOLDER = "AudioRecordermp3";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private int bufferSize = 0;
    private String getUrl = null;
    private String getUrlll = null;
    boolean stop_audio_helper = false;
    StringBuilder sb;
    private Boolean isSayUrl = false;
    private TimerTask mTimerTask;
    public static ImageView ivTrash;
    private int actualPos = 0;
    Boolean isPlay = false;
    ImageView recordButton;
    Boolean isCharSay = false;
    Boolean isTextSpeech = false;
    Boolean isCharText = false;
    public static String getAudioId = null;
    Boolean isRecordEffet = false;
    String curFX = "";
    Boolean isTest = false;
    // private static ImageView loader;
    int BufferElements2Rec = 1024;
    public static boolean costume = false, hair = false, hat = false, fhair = false, mouth = false, props = false, glasses = false, necklace = false;
    int BytesPerElement = 2;
    String fullname = "";
    String recordWavFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + AUDIO_RECORDER_FOLDER + "/testWave" + AUDIO_RECORDER_FILE_EXT_WAV;
    String recordMp3File = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + AUDIO_RECORDER_FOLDER + "/testwave12.mp3";
    Boolean isEffectSay = false;
    public static File photo = null;
    private String playDefault = "";
    private boolean randomiserclicked = false;
    ArrayList<String> saved_char_color_list;


    private boolean hasMore = true;
    private WindowManager mWindowManager;
    private int dp22;
    private boolean centerposition = true;
    private boolean X_VAL = false;

    /**
     * Method for animation of Character Menu i.e third menu ...
     */
    public void animateCharacterMenu() {

        if (menuContainer3.getVisibility() == View.VISIBLE) {


            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);


            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);


            Log.e("one", "if");

        } else {
            Log.e("one", "else");

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);

            menuContainer.setVisibility(View.VISIBLE);
            hideShowMenu.setVisibility(View.GONE);
            menuContainer2.setVisibility(View.VISIBLE);
            hideShowMenu2.setVisibility(View.GONE);
            menuContainer3.setVisibility(View.VISIBLE);
            hideShowMenu3.setVisibility(View.VISIBLE);


        }
        Log.e("isanim", "isanim " + ISANIM);
        if (!ISANIM) {
            Log.e("isanim", "");
            isRandmAccess = true;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    accessoryAdapter = new CharacterAccessoryAdapter(MainActivity.this, accessoriesList, listener, isRandmAccess);
                    lv_accList.setAdapter(accessoryAdapter);
                }
            });
        }

    }


    private void getWallpaperFromXml(int position, String folderName) throws XmlPullParserException, IOException, SAXException {
        String file_name;
        if (position != -1) {
            file_name = wallpapersList.get(position).getFileName();
        } else {
            file_name = folderName;
        }

        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = "//Type";
        InputSource inputSource = new InputSource(this.getAssets().open("img/backgrounds/" + file_name + "/" + "desc.xml"));
        NodeList nodes = null;
        try {
            nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < nodes.getLength(); i++) {
            Node sura = nodes.item(i);
            NamedNodeMap attributes = sura.getAttributes();
            Log.e("in_mainActivity", "" + attributes.getNamedItem("accId").getNodeValue());

            backgound_id = attributes.getNamedItem("accId").getNodeValue();
            String bg_id = attributes.getNamedItem("id").getNodeValue();
            Constants.TEMP_SAVED_BACKGROUND = file_name;
            Log.e("background_id", bg_id + Constants.TEMP_SAVED_BACKGROUND);
            bmp = BitmapFactory.decodeStream(this.getAssets().open("img/backgrounds/" + file_name + "/" + backgound_id + ".jpg"));
            Drawable dr = new BitmapDrawable(bmp);
            webViewContainer.setBackgroundDrawable(dr);
        }
    }

    private void showWearMenu(final String file_name, final boolean isAccSame) {
        accFileName = file_name;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.e("inPreExec", "" + Constants.jsonObject);
            }

            @Override
            protected Void doInBackground(Void... params) {
                if (accFileName.equalsIgnoreCase("Clothing")) {

                    Log.e("costume_array", "" + Constants.costume_array);
                    accessoryList = Constants.costume_array;
                } else if (accFileName.equalsIgnoreCase("Hair")) {


                    Log.e("hair_array", "" + Constants.hair_array);
                    accessoryList = Constants.hair_array;
                } else if (accFileName.equalsIgnoreCase("Hat")) {


                    Log.e("hat_array", "" + Constants.hat_array);
                    accessoryList = Constants.hat_array;
                } else if (accFileName.equalsIgnoreCase("Glasses")) {


                    Log.e("glasses_array", "" + Constants.glasses_array);
                    accessoryList = Constants.glasses_array;
                } else if (accFileName.equalsIgnoreCase("mouth")) {


                    Log.e("mouth_array", "" + Constants.mouth_array);
                    accessoryList = Constants.mouth_array;
                } else if (accFileName.equalsIgnoreCase("Bling")) {


                    Log.e("bling_array", "" + Constants.bling_array);
                    accessoryList = Constants.bling_array;
                } else if (accFileName.equalsIgnoreCase("Facial Hair")) {


                    Log.e("facial_array", "" + Constants.facial_array);
                    accessoryList = Constants.facial_array;
                } else if (accFileName.equalsIgnoreCase("Props")) {

                    Log.e("props_array", "" + Constants.props_array);
                    accessoryList = Constants.props_array;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                Log.e("isAccSame", "" + isAccSame);

                if (isAccSame) {
                    YoYo.with(Techniques.SlideOutLeft)
                            .duration(1000)
                            .playOn(selectedAccMenu);

                    YoYo.with(Techniques.SlideOutLeft)
                            .duration(1000)
                            .playOn(im_selectAccHideBtn);


                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100m
                            selectedAccMenu.setVisibility(View.GONE);
                            im_selectAccHideBtn.setVisibility(View.GONE);
                        }
                    }, 300);
//
                } else {

                    if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                        YoYo.with(Techniques.SlideOutLeft)
                                .duration(1000)
                                .playOn(selectedAccMenu);

                        YoYo.with(Techniques.SlideOutLeft)
                                .duration(1000)
                                .playOn(im_selectAccHideBtn);
                    }


                    YoYo.with(Techniques.SlideInLeft)
                            .duration(1000)
                            .playOn(im_selectAccHideBtn);
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(1000)
                            .playOn(selectedAccMenu);

                    selectedAccMenu.setVisibility(View.VISIBLE);
                    im_selectAccHideBtn.setVisibility(View.VISIBLE);
                    im_accHideBtn.setVisibility(View.GONE);
                }

                if (accessoryList.size() <= 5) {
                    im_accListUp.setVisibility(View.INVISIBLE);
                    im_accListDown.setVisibility(View.INVISIBLE);
                } else {
                    im_accListUp.setVisibility(View.VISIBLE);
                    im_accListDown.setVisibility(View.VISIBLE);
                }

                Log.e("aVoidsss", "" + accessoryList);
                if (!isAccSame) {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            charSpecificAdapter = new CharacterSpecificAccessoryAdapter(MainActivity.this, accessoryList, listener, accFileName, isRandmAccess);
                            lv_selectedAccessory.setAdapter(charSpecificAdapter);
                        }
                    });

                }

            }
        }.execute();
    }

    public void changeAccessories(String cat_id, String file) {

        StringBuilder buffer = null;

        if (file.equalsIgnoreCase("Costume")) {
            buffer = new StringBuilder("javascript:LoadCostumeAccessory('");
        } else if (file.equalsIgnoreCase("Hair")) {
            buffer = new StringBuilder("javascript:LoadHairAccessory('");
        } else if (file.equalsIgnoreCase("Glasses")) {
            buffer = new StringBuilder("javascript:LoadGlassesAccessory('");
        } else if (file.equalsIgnoreCase("Necklace")) {
            buffer = new StringBuilder("javascript:LoadNecklaceAccessory('");
        } else if (file.equalsIgnoreCase("Hat")) {
            buffer = new StringBuilder("javascript:LoadHatAccessory('");
        } else if (file.equalsIgnoreCase("fhair")) {
            buffer = new StringBuilder("javascript:LoadfhairAccessory('");
        } else if (file.equalsIgnoreCase("mouth")) {
            buffer = new StringBuilder("javascript:LoadmouthAccessory('");
        } else if (file.equalsIgnoreCase("Props")) {
            buffer = new StringBuilder("javascript:LoadPropsAccessory('");
        }


        buffer.append(Constants.BUNDLE_PATH);
        buffer.append("','");
        buffer.append(Constants.SPRITESHEET);
        buffer.append("','");
        buffer.append(Constants.COLORSTRING);
        buffer.append("','");
        buffer.append(cat_id);
        buffer.append("');");

        webView.loadUrl(buffer.toString());
        webView.clearCache(true);
        webView.clearFormData();
        webView.clearHistory();
        webView.clearMatches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        Log.d("contentview", "enter");
        setContentView(R.layout.activity_main);
        Constants.freeMemory();
        Constants.trimCache(this);
        Log.d("contentview", "exit");
        context = this;

        bufferSize = AudioRecord.getMinBufferSize(8000,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        if (ViewUtill.haveNetworkConnection(this)) {
            Constants.getSessionId(this);
            str_session_key = Constants.getSharedSessionId(this);
        }

        Log.d("str_session_key", "key" + str_session_key);

        playDefault = "isDefault";
        mHandler = new Handler(this);


        listener = this;
        ioListner = this;
        mListners = this;
        uploadAudioListener = this;
        isFirstTime = true;
        /**
         * Register Resources ...
         */
        initView();
        saved_char_color_list = new ArrayList<String>();
        runOnUiThread(new Runnable() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void run() {
                transDialog.show();
                webView.clearCache(true);
                webView.setBackgroundColor(Color.TRANSPARENT);
                webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
                webView.setScrollContainer(false);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setUseWideViewPort(true);
                webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                webView.getSettings().setAllowFileAccessFromFileURLs(true);
                webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
                webView.getSettings().enableSmoothTransition();
                webView.getSettings().setDomStorageEnabled(true);
                webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
                webView.getSettings().setSupportZoom(false);
                webView.getSettings().setDisplayZoomControls(false);
                webView.getSettings().setBuiltInZoomControls(false);
                webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);


                webView.addJavascriptInterface(new stopAudioInterface(context), "whenStop");
                webView.loadUrl("file:///android_asset/index_mem.html");
                getCategoryMenuData();
                Log.e("inUiThread", "dfkjadsfkabsdkf");
                getWearData();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        wallCatAdapter = new WallpaperCategoryAdapter(MainActivity.this, wallpapersList, listener);
                        lv_backgrounList.setAdapter(wallCatAdapter);
                        getWallpapersData();
                    }
                });


            }
        });
        getSaveCharDimensions();
        getDimensions();

        Constants.readXml(MainActivity.this, Constants.CURRECT_CHARACTER_CATEGORY, Constants.CURRENT_CHARACTER_NAME, false);
        updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_VISIBLE);


        Constants.setDataToLangTable(MainActivity.this);

        lv_rightDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (rightPosition == position) {
                    menuItems.get(rightPosition).put("is_selection", "true");
                } else {
                    Log.e("inside else", "" + rightPosition + "---" + position);
                    if (position != 0) {
                        menuItems.get(0).put("is_selection", "false");
                    }
                    menuItems.get(position).put("is_selection", "true");
                    if (rightPosition > -1) {
                        menuItems.get(rightPosition).put("is_selection", "false");
                    }
                }

                rightDrawerLayout.setBackgroundColor(Color.TRANSPARENT);
                rightDrawerLayout.setBackgroundColor(Color.parseColor("#D9000000"));

                if (rightMenuContainer.getVisibility() == View.GONE) {
                    YoYo.with(Techniques.SlideInLeft)
                            .duration(1000)
                            .playOn(rightMenuContainer);
                    rightMenuContainer.setVisibility(View.VISIBLE);
                    im_hideDrawer.setVisibility(View.GONE);


                    if (position == 0) {
                        tv_contentTitle.setText("About Voki");

                        String text = "Voki is all about fun and engaging teaching and learning experiences for both students and teachers.Millions have created and shared Vokis worldwide.\n\n";
                        String text1 = " \nVoki for Education is a suite of online products developed by Oddcast, a company dedicated to creating innovative animated speaking character solutions for education and businesses.\n";
                        String text2 = " \nThe Voki app is a companion tool to our online platform.  The app was created to mirror the online experience and provide students a convenient &amp; fun tool to work on their assignments and projects.\n";
                        String text4 = " \nLearn more at:";
                        String txt5 = " \n\tBuild no:21";
                        tv_content.setText(Html.fromHtml(text + "<br>" + "<br>" + text1 + "<br>" + "<br>" + text2 + "<br>" + "<br>" + text4 + str + "<br>" + "<br>" + txt5));
                        tv_content_email.setVisibility(View.GONE);
                    } else if (position == 1) {
                        tv_contentTitle.setText("Terms & Privacy");
                        tv_content.setText(Html.fromHtml(htmlTerms + link + link2));
                        tv_content_email.setText(Html.fromHtml(html3) + link3 + ".");
                        tv_content_email.setVisibility(View.VISIBLE);
                    } else if (position == 2) {
                        tv_contentTitle.setText("Voki App FAQ");
                        tv_content.setText(R.string.faq);
                        tv_content_email.setVisibility(View.GONE);
                    }
                } else {
                    im_hideDrawer.setVisibility(View.GONE);
                    if (position == 0) {
                        tv_contentTitle.setText("About Voki");

                        String text = "Voki is all about fun and engaging teaching and learning experiences for both students and teachers. Millions have created and shared Vokis worldwide.\n\n";
                        String text1 = " \nVoki for Education is a suite of online products developed by Oddcast, a company dedicated to creating innovative animated speaking character solutions for education and businesses.\n";
                        String text2 = " \nThe Voki app is a companion tool to our online platform.  The app was created to mirror the online experience and provide students a convenient &amp; fun tool to work on their assignments and projects.\n";
                        String text4 = " \nLearn more at:";
                        String txt5 = " \n\tBuild no:21";
                        tv_content.setText(Html.fromHtml(text + "<br>" + "<br>" + text1 + "<br>" + "<br>" + text2 + "<br>" + "<br>" + text4 + str + "<br>" + "<br>" + txt5));
                        tv_content_email.setVisibility(View.GONE);
                    } else if (position == 1) {
                        tv_contentTitle.setText("Terms & Privacy");

                        tv_content.setText(Html.fromHtml(htmlTerms + link + link2));
                        tv_content_email.setText(Html.fromHtml(html3) + link3 + ".");
                        tv_content_email.setVisibility(View.VISIBLE);
                    } else if (position == 2) {
                        tv_contentTitle.setText("Voki App FAQ");
                        tv_content.setText(R.string.faq);
                        tv_content_email.setVisibility(View.GONE);
                    }
                }

                rightPosition = position;
                rightMenuAdapter.notifyDataSetChanged();
                Log.e("newPosition", "" + rightPosition);
                Log.e("charactersCategory", "" + menuItems);
            }
        });

        hideShowMenu.setOnClickListener(this);
        hideShowMenu2.setOnClickListener(this);
        hideShowMenu3.setOnClickListener(this);
        im_Option1.setOnClickListener(this);
        im_Option2.setOnClickListener(this);
        im_Option3.setOnClickListener(this);
        im_Option4.setOnClickListener(this);
        im_Option5.setOnClickListener(this);
        im_catUp.setOnClickListener(this);
        im_catDown.setOnClickListener(this);
        im_catUp2.setOnClickListener(this);
        im_catDown2.setOnClickListener(this);
        im_drawer.setOnClickListener(this);
        im_hideDrawer.setOnClickListener(this);
        im_share.setOnClickListener(this);
        im_library.setOnClickListener(this);
        im_logo.setOnClickListener(this);
        im_saveChar.setOnClickListener(this);
        im_accHideBtn.setOnClickListener(this);
        im_wallHideBtn.setOnClickListener(this);
        im_chooseBack.setOnClickListener(this);
        im_tts.setOnClickListener(this);
        im_voice.setOnClickListener(this);
        fxLayout.setOnClickListener(this);
        im_voiceHideBtn.setOnClickListener(this);
        im_shareHideBtn.setOnClickListener(this);
        im_wallUp.setOnClickListener(this);
        im_wallDown.setOnClickListener(this);

        im_selectAccHideBtn.setOnClickListener(this);
        im_accListUp.setOnClickListener(this);
        im_accListDown.setOnClickListener(this);

        im_accUp.setOnClickListener(this);
        im_accDown.setOnClickListener(this);

        im_mouth.setOnClickListener(this);
        im_eyes.setOnClickListener(this);
        im_skin.setOnClickListener(this);
        im_hair.setOnClickListener(this);
        dp33 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 33, getResources().getDisplayMetrics());
        dp22 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 27, getResources().getDisplayMetrics());
        mColorPickerView.setOnColorChangedListener(this);
        // Glide.with(context).load(R.drawable.loader).into(loader);
        //  loader.setVisibility(View.INVISIBLE);
        webView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                im_library.setImageResource(R.drawable.btn_library_unselect);
                getDimensions();
                double x = event.getX();
                double y = event.getY();
                double start_point_X = st_pos_width;
                double start_point_Y = st_pos_height;
                double angX = x - start_point_X;
                double angY = y - start_point_Y;
                double angle = getAngle(angX, angY);
                if (angle < 270) {
                    angle += 90;
                }

                angle = angle > 0.0 ? angle : 360.0 + angle;
                final double ang = angle;
                Log.d("start_point_X", "" + "start_point_X" + start_point_X);
                Log.d("start_point_Y", "" + "start_point_Y" + start_point_Y);
                Log.d("angX", "" + "angX" + angX);
                Log.d("angY", "" + "angY" + angY);

                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (webView != null) {
                                    webView.setWebChromeClient(new GazeApiClient());
                                    StringBuilder buffer = new StringBuilder("javascript:alert(setGaze('");
                                    buffer.append(ang);
                                    buffer.append("','");
                                    buffer.append(1);
                                    buffer.append("','");
                                    buffer.append(200);
                                    buffer.append("'));");
                                    webView.loadUrl(buffer.toString());
                                    webView.clearCache(true);
                                    webView.clearFormData();
                                    webView.clearHistory();
                                    webView.clearMatches();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else {
                    handlerr.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                if (webView != null) {
                                    webView.freeMemory();
                                    webView.setWebChromeClient(new GazeApiClient());
                                    StringBuilder buffer = new StringBuilder("javascript:alert(setGaze('");
                                    buffer.append(ang);
                                    buffer.append("','");
                                    buffer.append(1);
                                    buffer.append("','");
                                    buffer.append(200);
                                    buffer.append("'));");
                                    webView.loadUrl(buffer.toString());
                                    webView.clearCache(true);
                                    webView.clearFormData();
                                    webView.clearHistory();
                                    webView.clearMatches();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, 500);


                }

                Log.d("angle", "" + "angle" + angle);
                Log.d("getposofpoint1", "" + "" + event.getX());
                Log.d("getposofpoint2", "" + "" + event.getY());

                if (ivTrash.getVisibility() == View.VISIBLE) {
                    ivTrash.setVisibility(View.GONE);
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    handleWebviewTouch();
                    if (rightMenuContainer.getVisibility() == View.GONE) {
                        handleRightMenuVisibility();
                    }

                }
                return (event.getAction() == MotionEvent.ACTION_MOVE);


            }
        });

        ivTrash.setVisibility(View.GONE);
        im_Option1.setOnLongClickListener(this);
        im_Option2.setOnLongClickListener(this);
        im_Option3.setOnLongClickListener(this);
        im_Option4.setOnLongClickListener(this);
        im_Option5.setOnLongClickListener(this);
        im_library.setOnLongClickListener(this);
        imDice.setOnClickListener(this);
        ivTrash.setOnClickListener(this);

        im_playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           /*     if (ViewUtill.haveNetworkConnection(MainActivity.this)) {*/

                if (isSayUrl == true && getUrl != null) {
                    Log.d("testing", "1");
                    sayurl(getUrl);
                } else if (isCharText == true && playDefault == "showTtsMenu") {
                    typedMessage = et_typeMessage.getText().toString();

                    Log.d("typemsg11", "" + typedMessage);
                    if (typedMessage.isEmpty()) {
                        // loader.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            if (ViewUtill.haveNetworkConnection(MainActivity.this)) {
                                playdefaultaudio();
                            } else {
                                InternetPopUp internetPopUp = new InternetPopUp(MainActivity.this, actualPos);
                                internetPopUp.show();
                                internetPopUp.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 70 / 100,
                                        getResources().getDisplayMetrics().heightPixels * 50 / 100);
                            }

                        } else {
                            playdefaultaudio();
                        }
                    } else {
                        Log.d("testing","3");
                        getDefaultVoiceType(Constants.SELECTED_LANG);
                    }

                } else if (isSayUrl == true && getUrl == null) {
                    Log.d("testing", "2");
                    playAudioFromText(Constants.ENGINE_ID, Constants.LANGUAGE_ID, Constants.VOICE_ID, fx_type, fx_level);
                } else if (isRecordEffet == true && getUrl != null) {

                    sayeffect(fullname);
                } else {

                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                        if (ViewUtill.haveNetworkConnection(MainActivity.this)) {
                            playdefaultaudio();
                        } else {
                            InternetPopUp internetPopUp = new InternetPopUp(MainActivity.this, actualPos);
                            internetPopUp.show();
                            internetPopUp.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 70 / 100,
                                    getResources().getDisplayMetrics().heightPixels * 50 / 100);
                        }

                    } else {
                        playdefaultaudio();
                    }
                }


            }
        });

        initFacebookSdk();
        splashScreen();
        margin_xllarge = getResources().getDimension(R.dimen.margin_xllarge) / getResources().getDisplayMetrics().density;
        zero = getResources().getDimension(R.dimen.zero) / getResources().getDisplayMetrics().density;
        high_nano = getResources().getDimension(R.dimen.high_nano) / getResources().getDisplayMetrics().density;
        margin_xsmall = getResources().getDimension(R.dimen.margin_xsmall) / getResources().getDisplayMetrics().density;
        margin_small = getResources().getDimension(R.dimen.margin_small) / getResources().getDisplayMetrics().density;
        margin_xlarge = getResources().getDimension(R.dimen.margin_xlarge) / getResources().getDisplayMetrics().density;
        grand_size = getResources().getDimension(R.dimen.grand_size) / getResources().getDisplayMetrics().density;
        medium_grand_size = getResources().getDimension(R.dimen.medium_grand_size) / getResources().getDisplayMetrics().density;
        margin_verybig = getResources().getDimension(R.dimen.margin_verybig) / getResources().getDisplayMetrics().density;
        on_showchar = getResources().getDimension(R.dimen.on_showchar) / getResources().getDisplayMetrics().density;
        im_playButton.setX(high_nano);
        Log.d("high_nano", "" + high_nano);
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode==RECORD_PERMISSION)
        {
            if (Mp3AudioRecordActivity.isRecordingg) {
                Log.d("test", "else_recorder");
                recordButton.setImageResource(R.mipmap.recbtn);
                tvCountDownTime.setText("upto 60 sec long");
                tvRecordTitle.setText("record your voice");
                //  stopped = true;
                //  progressBar.setProgress(0);
                recordAudio(false);
                //  im_playButton.setEnabled(false);


            } else {
                Log.d("test", "if_recorder");
                recordButton.setImageResource(R.mipmap.stoprecordd);
                tvRecordTitle.setText("recording...");
                //    stopped = false;
                //supposing u want to give maximum length of 60 seconds
                //     progressBar.setMax(MAX_DURATION);
                recordAudio(true);


            }
        }
        else if (requestCode==SHARE_EXTERNAL_STORAGE_PERMISSION_CODE)
        {
            shareButton();
        }
        else if (requestCode==EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        {
          getYourBackground();
        }
        else if (requestCode==GET_ACCOUNTS)
        {
            Intent signInIntent = new Intent(MainActivity.this, GooglePlusSignIn.class);
            startActivity(signInIntent);

        }
        else if (requestCode==INTERNET_PERMISSION)
        {
            Intent signInIntent = new Intent(MainActivity.this, TwitterIntegration.class);
            startActivity(signInIntent);

        }



    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (sensor_handler) {
            float x = event.values[0];

            float y = event.values[1];

            float z = event.values[2];
            //  Log.d("sensor", "x----" + x + "y----" + y);

            if (Math.abs(x) > Math.abs(y)) {

                if (x < -1) {
                    sensor_play1 = true;
                    sensor_play2 = true;
                    //  iv.setText("righttt");

                }

                if (x > 0) {
                    sensor_play1 = true;
                    sensor_play2 = true;
                    //  iv.setText("left");

                }

            } else {

                if (y < -2) {
                    if (sensor_play1) {
                        try {
                            if (webView != null) {
                                sensor_play1 = false;
                                //  iv.setText("top");
                                webView.setWebChromeClient(new GazeApiClient());
                                StringBuilder buffer = new StringBuilder("javascript:alert(setGaze('");
                                buffer.append(270);
                                buffer.append("','");
                                buffer.append(1);
                                buffer.append("','");
                                buffer.append(200);
                                buffer.append("'));");
                                webView.loadUrl(buffer.toString());
                                webView.clearCache(true);
                                webView.clearFormData();
                                webView.clearHistory();
                                webView.clearMatches();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }

                if (y > 2 && y < 6) {

                    //  iv.setText("bottom");
                    if (sensor_play2) {
                        try {
                            sensor_play2 = false;
                            if (webView != null) {
                                webView.setWebChromeClient(new GazeApiClient());
                                StringBuilder buffer = new StringBuilder("javascript:alert(setGaze('");
                                buffer.append(93);
                                buffer.append("','");
                                buffer.append(1);
                                buffer.append("','");
                                buffer.append(200);
                                buffer.append("'));");
                                webView.loadUrl(buffer.toString());
                                webView.clearCache(true);
                                webView.clearFormData();
                                webView.clearHistory();
                                webView.clearMatches();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            if (x > (-2) && x < (1) && y > (-2) && y < 0) {

                sensor_play1 = true;
                sensor_play2 = true;
                //  iv.setText("center");

            }
          /*  try {
                float[] values = event.values;
                // Log.d("sensorvalues", "" + "x: " + values[0] + "\ny: " + values[1] + "\nz: " + values[2]);
                Log.d("valuesss",""+values[1]);
                if (values[1] > 2) {

                } else if (values[1] < -1) {

                } else {
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void uploadAudioInterface() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                uploadAduio();
            }
        });

    }


    public class stopAudioInterface {
        Context context;

        public stopAudioInterface(Context context) {
            this.context = context;
        }


        @JavascriptInterface
        public void performClick() {
            handler.sendEmptyMessage(0);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            im_playButton.setImageResource(R.drawable.play);
            isPlay = false;
            isCharSay = false;
            isTextSpeech = false;
            isCharText = true;
            isRecordEffet = true;
            isEffectSay = false;
            if (recordButton != null)
                recordButton.setEnabled(true);
            super.handleMessage(msg);
        }

    };


    /**
     * Method for Registering Resources being used ...
     */
    public void initView() {

        webViewContainer = (RelativeLayout) findViewById(R.id.webViewContainer);
        //  loader = (ImageView) webViewContainer.findViewById(R.id.loadernew);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        sensorMan = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        menuContainer = (RelativeLayout) findViewById(R.id.slide_Menu);
        menuContainer2 = (RelativeLayout) findViewById(R.id.menu2);
        menuContainer3 = (RelativeLayout) findViewById(R.id.menu3);
        menuLibrary = (RelativeLayout) findViewById(R.id.extraOption);
        hideShowMenu = (ImageView) findViewById(R.id.im_showHideMenu);
        hideShowMenu2 = (ImageView) findViewById(R.id.im_showHideMenu2);
        hideShowMenu3 = (ImageView) findViewById(R.id.im_showHideMenu3);
        lv_Characters = (LinearLayout) findViewById(R.id.lv_characters);
        lv_charactersCat = (ListView) findViewById(R.id.lv_secondListCharacter);
        lv_characterSpecs = (ListView) findViewById(R.id.lv_secondListCharacter2);
        lv_rightDrawer = (ListView) findViewById(R.id.lv_rightDrawer);
        rightDrawerLayout = (RelativeLayout) findViewById(R.id.drawer_layout);
        webView = (WebView) findViewById(R.id.webView);
        listContainer = (RelativeLayout) findViewById(R.id.listContainer);
        colorPickerContainer = (RelativeLayout) findViewById(R.id.colorPickerContainer);
        rightMenuContainer = (RelativeLayout) findViewById(R.id.rightMenuContainer);
        saveCharLay = (RelativeLayout) findViewById(R.id.extraLibrary);
        cat_menu = (RelativeLayout) findViewById(R.id.catMenu);
        innerColorContainer = (RelativeLayout) findViewById(R.id.colorContainer);
        enter_from_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_right);
        enter_from_right1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_right);
        exit_to_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.exit_to_right);
        enter_from_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_left);
        exit_to_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.exit_to_left);
        enter_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_left);
        exit_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.exit_left);


        ivTrash = (ImageView) findViewById(R.id.iv_trash);
        im_Option1 = (ImageView) findViewById(R.id.im_option1);
        im_Option2 = (ImageView) findViewById(R.id.im_option2);
        im_Option3 = (ImageView) findViewById(R.id.im_option3);
        im_Option4 = (ImageView) findViewById(R.id.im_option4);
        im_Option5 = (ImageView) findViewById(R.id.im_option5);
        im_catUp = (ImageView) findViewById(R.id.im_moveUp);
        im_catDown = (ImageView) findViewById(R.id.im_moveDown);
        im_catUp2 = (ImageView) findViewById(R.id.im_moveUp2);
        im_catDown2 = (ImageView) findViewById(R.id.im_moveDown2);
        im_library = (ImageView) findViewById(R.id.extra_option);
        im_logo = (ImageView) findViewById(R.id.im_logo);
        im_drawer = (ImageView) findViewById(R.id.im_drawer);
        im_share = (ImageView) findViewById(R.id.im_share);
        im_hideDrawer = (ImageView) findViewById(R.id.im_HideMenu);
        im_saveChar = (ImageView) findViewById(R.id.extra_lib);
        im_playButton = (ImageView) findViewById(R.id.im_playBtn);
        webViewContainer.setOnClickListener(this);
        colorPickerContainer.setOnClickListener(null);

        imDice = (ImageView) findViewById(R.id.im_dice);

        res = getResources();
        for (int i = 0; i < 8; i++) {
            dice[i] = res.getDrawable(diceImages[i]);
        }
        try {
            imDice.setImageResource(R.drawable.btn_randomize_anim01);
            imDice.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }


        mColorPickerView = (ColorPickerView) findViewById(R.id.colorpickerview__color_picker_view);

        im_mouth = (ImageView) findViewById(R.id.im_mouth);
        im_eyes = (ImageView) findViewById(R.id.im_eyes);
        im_skin = (ImageView) findViewById(R.id.im_skin);
        im_hair = (ImageView) findViewById(R.id.im_hair);

        rel_container_1 = (RelativeLayout) findViewById(R.id.innerContainer);
        rel_container_2 = (RelativeLayout) findViewById(R.id.innerContainer_1);
        rel_container_3 = (RelativeLayout) findViewById(R.id.innerContainer2);
        rel_container_4 = (RelativeLayout) findViewById(R.id.innerContainer3);

        tv_content = (TextView) findViewById(R.id.tv_content);
        tv_contentTitle = (TextView) findViewById(R.id.tv_ContentTitle);
        tv_content_email = (TextView) findViewById(R.id.tv_content_email);
        tv_content.setMovementMethod(LinkMovementMethod.getInstance());
        accMenu = (RelativeLayout) findViewById(R.id.acc_Menu);
        lv_accList = (ListView) findViewById(R.id.lv_accList);
        im_accUp = (ImageView) findViewById(R.id.im_acc_moveUp);
        im_accDown = (ImageView) findViewById(R.id.im_acc_moveDown);
        im_accHideBtn = (ImageView) findViewById(R.id.im_accHideBtn);

        wallMenu = (RelativeLayout) findViewById(R.id.wallMenu);
        lv_backgrounList = (ListView) findViewById(R.id.lv_wallpaperList);
        im_wallUp = (ImageView) findViewById(R.id.im_wallMoveUp);
        im_wallDown = (ImageView) findViewById(R.id.im_wallMoveDown);
        im_wallHideBtn = (ImageView) findViewById(R.id.im_wallHideBtn);
        im_chooseBack = (ImageView) findViewById(R.id.choose_option);
        RelativeLayout type_chooser = (RelativeLayout) findViewById(R.id.type_chooser);
        im_tts = (ImageView) findViewById(R.id.im_tts);
        im_voice = (ImageView) findViewById(R.id.im_voice);

        type_chooser.setOnClickListener(null);
        optionContainer = (RelativeLayout) findViewById(R.id.option_container);
        optionContainer.setOnClickListener(null);
        fxLayout = (RelativeLayout) findViewById(R.id.fx_layout);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fxLayout.getLayoutParams();
        params.setMargins(12, 10, 12, 35);
        fxLayout.setLayoutParams(params);

        fxLayout.setOnTouchListener(null);
        tv_fx = (TextView) findViewById(R.id.tv_fx);
        tv_fx.setGravity(Gravity.CENTER);

        voiceMenu = (RelativeLayout) findViewById(R.id.voiceMenuContainer);
        voiceMenu.setOnClickListener(null);
        RelativeLayout voiceContainer = (RelativeLayout) findViewById(R.id.voiceContainer);
        voiceContainer.setOnTouchListener(null);
        im_voiceHideBtn = (ImageView) findViewById(R.id.im_voiceMenuHide);

        shareMenu = (RelativeLayout) findViewById(R.id.shareMenu_layout);
        shareGridView = (GridView) findViewById(R.id.gridShare);
        im_shareHideBtn = (ImageView) findViewById(R.id.im_shareHideBtn);


        selectedAccMenu = (RelativeLayout) findViewById(R.id.accmenu3);
        im_selectAccHideBtn = (ImageView) findViewById(R.id.im_hideAccMenuBtn);
        im_accListUp = (ImageView) findViewById(R.id.im_accmoveUp);
        im_accListDown = (ImageView) findViewById(R.id.im_accmoveDown);
        lv_selectedAccessory = (ListView) findViewById(R.id.lv_accCatList);


        assetManager = getAssets();
        imagesArray = new ArrayList<>();
        specsCategory = new ArrayList<>();
        charactersCategory = new ArrayList<>();
        wearsCategory = new ArrayList<>();
        shareList = new ArrayList<>();
        wallpaperCategory = new ArrayList<>();
        accessoryList = new ArrayList<>();
        accessorySpecsCategory = new ArrayList<>();
        charColorList = new ArrayList<>();
        colorCatList = new ArrayList<>();
        defaultAudioList = new ArrayList<>();
        femaleAudioList = new ArrayList<>();
        maleAudioList = new ArrayList<>();
        othersAudioList = new ArrayList<>();


        charactersCatList = new ArrayList<>();
        finalCharList = new ArrayList<>();
        accessoriesList = new ArrayList<>();
        wallpapersList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            HashMap<String, String> map = new HashMap<>();
            if (i == 0) {
                map.put("cat_name", "mouth");
                map.put("file_name", "btn_mouth_128x128.png");
            } else if (i == 1) {
                map.put("cat_name", "eyes");
                map.put("file_name", "btn_eye_128x128.png");
            } else if (i == 2) {
                map.put("cat_name", "skin");
                map.put("file_name", "btn_skin_128x128.png");
            } else if (i == 3) {
                map.put("cat_name", "hair");
                map.put("file_name", "btn_hair_128x128.png");
            }
            charColorList.add(map);
        }

        Log.e("charColorListssssssssss", "" + charColorList);
        checkAvailList = new ArrayList<>();

        //marshPermission = new MarshMallowPermission(MainActivity.this);

        dataHandler = new DatabaseHandler(MainActivity.this);

        transDialog = new TransparentProgressDialog(this, R.drawable.spinner);
        menuItems = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            HashMap map = new HashMap();
            map.put("is_selection", "false");
            menuItems.add(map);
        }

        try {
            mHandler.sendEmptyMessage(SET_LISTCHARCTER_ADAPTER);
            rightMenuAdapter = new RightMenuDrawerAdapter(MainActivity.this, rightMenuImages, menuItems, rightMenuImagesSelected);
            lv_rightDrawer.setAdapter(rightMenuAdapter);


            arrowHandle();
        } catch (Exception e) {
        }


    }

    private void rollDice() {

        thread = new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < rollAnimations; i++) {
                        doRoll();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

    }

    private void doRoll() { // only does a single roll
        roll[0] = randomGen.nextInt(8);
        roll[1] = randomGen.nextInt(8);
        diceSum = roll[0] + roll[1] + 2; // 2 is added because the values of the rolls start with 0 not 1
        synchronized (getLayoutInflater()) {
            mHandler.sendEmptyMessage(SHOW_ANIMATION);
        }
        try { // delay to alloy for smooth animation
            Thread.sleep(delayTime);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        final Handler handler = new Handler();
        switch (v.getId()) {

            case R.id.im_dice:
                if (randomchecker) {
                    is_there_any_change_in_character = true;
                    isCharacterchangedagainForColorPallete = true;
                    randomiserclicked = true;
                    randomchecker = false;
                    character_selection_way_is_randomiser = true;
                    isRandom = true;
                    ISANIM = false;
                    isRandmAccess = true;
                    rollDice();
                    im_library.setImageResource(R.drawable.btn_library_unselect);
                    ivTrash.setVisibility(View.GONE);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                Random random = new Random();
                                dicePosition = random.nextInt(charactersCatList.size());
                                Log.d("characlistsize", "" + charactersCatList.size());
                                Log.d("dicePosition", "" + dicePosition);
                                if (listAdapter != null) {

                                    lv_charactersCat.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            lv_charactersCat.smoothScrollToPosition(dicePosition);
                                            listAdapter.showList(dicePosition, true);

                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            randomchecker = true;

                        }
                    }, 1000);
                    resetChecks();
                }
                break;

            case R.id.im_showHideMenu:

                getYposition();
                Log.d("show", "hidemenu1");
                YoYo.with(Techniques.SlideInLeft)
                        .duration(1000)
                        .playOn(menuContainer);
                menuContainer.setVisibility(View.VISIBLE);
                settingPosition(margin_xsmall, st_pos_width);
                shareMenu.setVisibility(View.GONE);
                im_shareHideBtn.setVisibility(View.GONE);
                isOpen = true;
                Log.d("show", Constants.MENU_CONTAINER_VISIBLE);

                handleRightMenuVisibility();
                updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideShowMenu.setVisibility(View.GONE);
                    }
                }, 300);

                break;

            case R.id.im_showHideMenu2:
                Log.d("show", "hidemenu2");
                getYposition();
                if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                    if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                        colorMenuAnimation();
                    }
                } else {
                    if (menuContainer2.getVisibility() == View.VISIBLE) {
                        charMenuAnimation();
                    }
                }

                setMenuIcons("");
                break;

            case R.id.im_showHideMenu3:
                Log.d("show", "hidemenu3");
                getYposition();
                if (menuContainer3.getVisibility() == View.VISIBLE) {
                    menuContainer3Animation();
                }

                break;

            case R.id.im_option1:
                try {
                    if (webView != null)
                        webView.clearCache(true);
                    Constants.freeMemory();
                    ISANIM = true;
                    isRandom = false;
                    imDice.setVisibility(View.VISIBLE);
                    if (ivTrash.getVisibility() == View.VISIBLE) {
                        ivTrash.setVisibility(View.GONE);
                    }

                    getYposition();


                    if (charactersCatList.size() > 0) {
                        Log.e("SAVED_CHARACTER", Constants.TEMP_SAVED_CHARACTER);
                        Log.e("Inside_Click", "" + charactersCategory);
                        if (listAdapter == null) {
                            mHandler.sendEmptyMessage(SET_LISTCHARCTER_ADAPTER);
                        } else
                            mHandler.sendEmptyMessage(UPDATE_LISTCAHRCTER_UI);

                    } else {
                        Log.e("else_Click", "" + charactersCategory);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getCategoryMenuData();
                            }
                        });

                    }


                    if (accMenu.getVisibility() == View.VISIBLE) {
                        accMenuAnimation();
                        Log.e("else_Click_1", "1223");
                    }

                    if (wallMenu.getVisibility() == View.VISIBLE) {
                        Log.e("else_Click_2", "1223");
                        wallpaperMenuAnimation();
                    }

                    if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                        Log.e("else_Click_3", "1223");
                        colorMenuAnimation();
                    }

                    if (voiceMenu.getVisibility() == View.VISIBLE) {
                        Log.e("else_Click_4", "1223");
                        voiceMenuAnimation();
                    }

                    if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
                        Log.e("else_Click_5", "1223");
                        drawerMenuAnimation();
                    }

                    if (shareMenu.getVisibility() == View.VISIBLE) {
                        Log.e("else_Click_6", "1223");
                        shareMenuAnimation();
                    }

                    if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                        selectedWearMenuAnimation();
                        Log.e("else_Click_7", "1223");
                    }

                    if (menuContainer3.getVisibility() == View.VISIBLE) {
                        menuContainer3Animation();
                        Log.e("else_Click_8", "1223");
                    }

                    charMenuAnimation();


                    wallCatAdapter = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.im_option2:
                if (webView != null)
                    webView.clearCache(true);
                Constants.freeMemory();
                isRandmAccess = false;
                ISANIM = true;
                imDice.setVisibility(View.GONE);
                if (ivTrash.getVisibility() == View.VISIBLE) {
                    ivTrash.setVisibility(View.GONE);
                }

                getYposition();
                float sec_option = st_pos_width + st_pos_width / 10;
                Log.d("sec_option", "" + sec_option);

                if (accessoriesList.size() > 0) {
                    Log.e("Inside_Wear_Click", "" + accessoriesList.size());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            accessoryAdapter = new CharacterAccessoryAdapter(MainActivity.this, accessoriesList, listener, isRandmAccess);
                            lv_accList.setAdapter(accessoryAdapter);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getWearData();
                        }
                    });

                }

                if (menuContainer2.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click1", "");
                    charMenuAnimation();
                }

                if (wallMenu.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click2", "");
                    wallpaperMenuAnimation();
                }

                if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click3", "");
                    colorMenuAnimation();
                }

                if (voiceMenu.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click4", "");
                    voiceMenuAnimation();
                }

                if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click5", "");
                    drawerMenuAnimation();
                }

                if (shareMenu.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click6", "");
                    shareMenuAnimation();
                }

                if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click7", "");
                    selectedWearMenuAnimation();
                }

                if (menuContainer3.getVisibility() == View.VISIBLE) {
                    Log.e("Inside_Wear_Click8", "");
                    menuContainer3Animation();
                }
                accMenuAnimation();

                cat_menu.setVisibility(View.VISIBLE);
                colorPickerContainer.setVisibility(View.GONE);
                menuContainer.setVisibility(View.VISIBLE);
                wallCatAdapter = null;

                break;

            case R.id.im_option3:
                if (webView != null)
                    webView.clearCache(true);
                Constants.freeMemory();
                ;
                ISANIM = true;
                imDice.setVisibility(View.GONE);
                if (ivTrash.getVisibility() == View.VISIBLE) {
                    ivTrash.setVisibility(View.GONE);
                }

                getYposition();
                if (wallCatAdapter == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            wallCatAdapter = new WallpaperCategoryAdapter(MainActivity.this, wallpapersList, listener);
                            Log.e("wallCatAdapter", "" + wallCatAdapter);
                            lv_backgrounList.setAdapter(wallCatAdapter);
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getWallpapersData();
                        }
                    });
                }


                if (menuContainer2.getVisibility() == View.VISIBLE) {
                    charMenuAnimation();
                }

                if (accMenu.getVisibility() == View.VISIBLE) {
                    accMenuAnimation();
                }

                if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                    colorMenuAnimation();
                }

                if (voiceMenu.getVisibility() == View.VISIBLE) {
                    voiceMenuAnimation();
                }

                if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
                    drawerMenuAnimation();
                }

                if (shareMenu.getVisibility() == View.VISIBLE) {
                    shareMenuAnimation();
                }

                if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                    selectedWearMenuAnimation();
                }

                if (menuContainer3.getVisibility() == View.VISIBLE) {
                    menuContainer3Animation();
                }
                wallpaperMenuAnimation();
                break;

            case R.id.im_option4:
                if (webView != null)
                    webView.clearCache(true);
                Constants.freeMemory();
                ISANIM = true;
                imDice.setVisibility(View.GONE);
                getYposition();


                icColorChanging = false;

                if (isCharacterchangedagainForColorPallete || charColorList == null) {


                }
                isCharacterchangedagainForColorPallete = false;

                if (menuContainer2.getVisibility() == View.VISIBLE) {
                    charMenuAnimation();
                }

                if (accMenu.getVisibility() == View.VISIBLE) {
                    accMenuAnimation();
                }

                if (wallMenu.getVisibility() == View.VISIBLE) {
                    wallpaperMenuAnimation();
                }

                if (voiceMenu.getVisibility() == View.VISIBLE) {
                    voiceMenuAnimation();
                }

                if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
                    drawerMenuAnimation();
                }

                if (shareMenu.getVisibility() == View.VISIBLE) {
                    shareMenuAnimation();
                }

                if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                    selectedWearMenuAnimation();
                }

                if (menuContainer3.getVisibility() == View.VISIBLE) {
                    menuContainer3Animation();
                }

                if (checkAvailList.size() > 0)
                    checkAvailList.clear();

                colorMenuAnimation();

                setDefaultColorSelection();

                setColorIcon();
                wallCatAdapter = null;

                break;

            case R.id.im_option5:
                if (ViewUtill.haveNetworkConnection(this)) {
                    Constants.freeMemory();
                    icColorChanging = false;
                    ISANIM = true;
                    imDice.setVisibility(View.GONE);
                    getYposition();
                    if (ivTrash.getVisibility() == View.VISIBLE) {
                        ivTrash.setVisibility(View.GONE);
                    }

                    Constants.getFxMenuData();

                    if (optionContainer.getChildCount() <= 0) {
                        im_tts.setImageResource(R.drawable.btn_tts_128x128_selected);
                        showTtsMenu();
                    }


                    if (menuContainer2.getVisibility() == View.VISIBLE) {
                        charMenuAnimation();
                    }

                    if (accMenu.getVisibility() == View.VISIBLE) {
                        accMenuAnimation();
                    }

                    if (wallMenu.getVisibility() == View.VISIBLE) {
                        wallpaperMenuAnimation();
                    }

                    if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                        colorMenuAnimation();
                    }

                    if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
                        drawerMenuAnimation();
                    }

                    if (shareMenu.getVisibility() == View.VISIBLE) {
                        shareMenuAnimation();
                    }

                    if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                        selectedWearMenuAnimation();
                    }

                    if (menuContainer3.getVisibility() == View.VISIBLE) {
                        menuContainer3Animation();
                    }

                    voiceMenuAnimation();

                    wallCatAdapter = null;
                } else {
                    InternetPopUp internetPopUp = new InternetPopUp(MainActivity.this, actualPos);
                    internetPopUp.show();
                    internetPopUp.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 70 / 100,
                            getResources().getDisplayMetrics().heightPixels * 50 / 100);
                }

                break;

            case R.id.im_moveUp:

                isUpClicked = true;
                isDownClicked = false;
                arrowHandle();
                lv_charactersCat.post(new Runnable() {

                    @Override
                    public void run() {
                        int firstIndex = lv_charactersCat.getFirstVisiblePosition();
                        lv_charactersCat.getAdapter();
                        Log.e("ScrollPos", "" + firstIndex);
                        nextPos = firstIndex - 1;
                        lv_charactersCat.smoothScrollToPosition(nextPos);
                        Log.d("nextPosnextPos", "" + nextPos);
                        if (nextPos == 0 || nextPos == -1) {
                            im_catUp.setImageResource(R.drawable.btn_arrow_up_unsel);
                            isUpClicked = false;
                        }
                    }
                });

                break;

            case R.id.im_moveDown:

                isUpClicked = false;
                isDownClicked = true;
                arrowHandle();

                lv_charactersCat.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = lv_charactersCat.getLastVisiblePosition();
                        ListCharacterAdapter listAdapter = (ListCharacterAdapter) lv_charactersCat.getAdapter();
                        int count = listAdapter.getCount();
                        Log.e("FirstChild", "" + index);
                        lv_charactersCat.smoothScrollToPosition(index + 1);
                        if (index + 1 == count) {
                            im_catDown.setImageResource(R.drawable.btn_arrow_down_unsel);
                            isDownClicked = false;
                        }
                    }
                });


                break;

            case R.id.im_moveUp2:

                isUpClicked2 = true;
                isDownClicked2 = false;
                arrowHandle();

                lv_characterSpecs.post(new Runnable() {
                    @Override
                    public void run() {
                        int menuFirstIndex = lv_characterSpecs.getFirstVisiblePosition();
                        Log.e("FirstVisiblePos", "" + menuFirstIndex);
                        lv_characterSpecs.smoothScrollToPosition(menuFirstIndex - 1);
                        if (menuFirstIndex - 1 == 0 || menuFirstIndex - 1 == -1) {
                            im_catUp2.setImageResource(R.drawable.btn_arrow_up_unsel);
                            isUpClicked = false;
                        }
                    }
                });
                break;

            case R.id.im_acc_moveUp:
                int accFirstindex = lv_accList.getFirstVisiblePosition();
                Log.e("FirstVisiblePos", "" + accFirstindex);
                lv_accList.smoothScrollToPosition(accFirstindex - 1);
                break;

            case R.id.im_acc_moveDown:
                int accLastindex = lv_accList.getLastVisiblePosition();
                Log.e("FirstVisiblePos", "" + accLastindex);
                lv_accList.smoothScrollToPosition(accLastindex + 1);
                break;
            case R.id.im_wallMoveUp:
                int wallFirstindex = lv_backgrounList.getFirstVisiblePosition();
                Log.e("FirstVisiblePos", "" + wallFirstindex);
                lv_backgrounList.smoothScrollToPosition(wallFirstindex - 1);
                break;

            case R.id.im_wallMoveDown:
                int wallLastindex = lv_backgrounList.getLastVisiblePosition();
                Log.e("FirstVisiblePos", "" + wallLastindex);
                lv_backgrounList.smoothScrollToPosition(wallLastindex + 1);

                break;

            case R.id.im_moveDown2:

                isUpClicked2 = false;
                isDownClicked2 = true;
                arrowHandle();
                int menuSecondIndex = lv_characterSpecs.getLastVisiblePosition();
                Log.e("LastVisiblePos", "" + menuSecondIndex);
                lv_characterSpecs.smoothScrollToPosition(menuSecondIndex + 1);

                break;

            case R.id.im_drawer:
                getYposition();
                if (ivTrash.getVisibility() == View.VISIBLE) {
                    ivTrash.setVisibility(View.GONE);
                }

                PREVIOUSClicked = "";
                if (menuContainer2.getVisibility() == View.VISIBLE) {
                    charMenuAnimation();
                }

                if (accMenu.getVisibility() == View.VISIBLE) {
                    accMenuAnimation();
                }

                if (wallMenu.getVisibility() == View.VISIBLE) {
                    wallpaperMenuAnimation();
                }

                if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                    colorMenuAnimation();
                }

                if (voiceMenu.getVisibility() == View.VISIBLE) {
                    voiceMenuAnimation();
                }

                if (shareMenu.getVisibility() == View.VISIBLE) {
                    shareMenuAnimation();
                }

                if (menuContainer3.getVisibility() == View.VISIBLE) {
                    menuContainer3Animation();
                }

                if (selectedAccMenu.getVisibility() == View.VISIBLE) {
                    selectedWearMenuAnimation();
                }

                if (menuContainer.getVisibility() == View.VISIBLE) {
                    categoryMenuAnimation();
                }

                if (rightMenuContainer.getVisibility() == View.VISIBLE) {
                    rightMenuContainer.setVisibility(View.GONE);
                }


                drawerMenuAnimation();
                break;

            case R.id.im_HideMenu:
                getYposition();
                YoYo.with(Techniques.SlideOutRight)
                        .duration(1000)
                        .playOn(rightDrawerLayout);

                YoYo.with(Techniques.SlideOutRight)
                        .duration(1000)
                        .playOn(im_hideDrawer);


                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        rightDrawerLayout.setVisibility(View.GONE);
                        im_hideDrawer.setVisibility(View.GONE);
                    }
                }, 700);

                for (int i = 0; i < menuItems.size(); i++) {
                    menuItems.get(i).put("is_selection", "false");
                }
                rightMenuAdapter.notifyDataSetChanged();
                updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_INVISIBLE);
                break;

            case R.id.im_share:
                if (ViewUtill.haveNetworkConnection(MainActivity.this)) {
                    /*if (!marshPermission.checkPermissionForExternalStorage() || !marshPermission.checkPermissionForREADExternalStorage()) {
                        marshPermission.requestPermissionForShareExternalstorage();
                    } else {
                        shareButton();
                    }*/
                    requestAppPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},R.string.messagee,SHARE_EXTERNAL_STORAGE_PERMISSION_CODE);

                } else {
                    InternetPopUp internetPopUp = new InternetPopUp(MainActivity.this, 0);
                    internetPopUp.show();
                    internetPopUp.getWindow().setLayout(MainActivity.this.getResources().getDisplayMetrics().widthPixels * 70 / 100,
                            MainActivity.this.getResources().getDisplayMetrics().heightPixels * 50 / 100);
                }
                break;

            case R.id.im_logo:

                PREVIOUSClicked = "";
                if (rightMenuContainer.getVisibility() == View.VISIBLE) {
                    im_hideDrawer.setVisibility(View.GONE);

                    menuItems.get(0).put("is_selection", "false");
                    rightMenuAdapter.notifyDataSetChanged();
                    rightMenuContainer.setVisibility(View.GONE);
                    rightDrawerLayout.setVisibility(View.GONE);
                    im_hideDrawer.setVisibility(View.GONE);
                    rightMenuContainer.setBackgroundColor(Color.parseColor("#D9000000"));
                    rightDrawerLayout.setBackgroundColor(Color.TRANSPARENT);
                    rightDrawerLayout.setBackgroundColor(Color.parseColor("#D9000000"));

                } else {


                    for (int i = 0; i < menuItems.size(); i++) {
                        menuItems.get(i).put("is_selection", "false");
                    }
                    rightMenuAdapter.notifyDataSetChanged();

                    lv_rightDrawer.setVisibility(View.VISIBLE);

                    YoYo.with(Techniques.SlideInRight)
                            .duration(1000)
                            .playOn(rightDrawerLayout);
                    rightDrawerLayout.setBackgroundColor(Color.TRANSPARENT);
                    rightDrawerLayout.setBackgroundColor(Color.parseColor("#D9000000"));
                    rightMenuContainer.setBackgroundColor(Color.parseColor("#D9000000"));
                    Log.e("rightDrawerLayout", "" + rightDrawerLayout.getVisibility());
                    rightDrawerLayout.setVisibility(View.VISIBLE);
                    rightMenuContainer.setVisibility(View.VISIBLE);

                    lv_rightDrawer.setVisibility(View.VISIBLE);
                    menuItems.get(0).put("is_selection", "true");
                    rightMenuAdapter.notifyDataSetChanged();
                    im_hideDrawer.setVisibility(View.GONE);
//                    gridShare.setVisibility(View.GONE);

                    tv_contentTitle.setText("About Voki");

                    String text = "Voki is all about fun and engaging teaching and learning experiences for both students and teachers.Millions have created and shared Vokis worldwide.\n\n";
                    String text1 = " \nVoki for Education is a suite of online products developed by Oddcast, a company dedicated to creating innovative animated speaking character solutions for education and businesses.\n";
                    String text2 = " \nThe Voki app is a companion tool to our online platform.  The app was created to mirror the online experience and provide students a convenient &amp; fun tool to work on their assignments and projects.\n";
                    String text4 = " \nLearn more at:";
                    String str = "<font color='#ED872D'> www.voki.com </font>";
                    String txt5 = " \n\tBuild no:21";
                    tv_content.setText(Html.fromHtml(text + "<br>" + "<br>" + text1 + "<br>" + "<br>" + text2 + "<br>" + "<br>" + text4 + str + "<br>" + "<br>" + txt5));
                    tv_content_email.setVisibility(View.GONE);
                }

                show_hideAnimations();
                break;

            case R.id.extra_option:
                try {
                    icColorChanging = false;
                    if (charList.size() > 0)
                        charList.clear();

                    Log.d("charlistclear", "" + charList.size());
                    getYposition();
                    menuContainer3.setVisibility(View.GONE);
                    hideShowMenu3.setVisibility(View.GONE);

                    isLibraryClicked = true;

                    isButtonClicked = FIRSTClicked;
                    setMenuIcons(isButtonClicked);
                    Log.e("clicked", "111");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            savedCharAdapter = new SavedCharacterAdapter(MainActivity.this, charList, listener);
                            lv_characterSpecs.setAdapter(savedCharAdapter);
                            /*if (saveCharLay.getVisibility() == View.VISIBLE) {

                                SaveCharacterMenu();
                            } else {
                                showAllSavedCharacters();
                            }*/
                            SaveCharacterMenu();

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.extra_lib:
                try {
                    Log.d("save_char", "" + save_char);
                    if (save_char) {

                        save_char = false;
                        Log.d("saved_char_color_list>", "" + saved_char_color_list);
                        counter_for_saved_character = 0;
                        character_saving = true;
                        if (saved_char_color_list != null) {

                            saved_char_color_list.clear();
                        }

                        Log.d("saved_char_color_list", "" + saved_char_color_list);

                        setSkinColorForSaveChar();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();

                }


                break;

            case R.id.im_accHideBtn:
                getYposition();
                if (accMenu.getVisibility() == View.VISIBLE) {
                    accMenuAnimation();
                    setMenuIcons("");
                }
                break;

            case R.id.im_shareHideBtn:
                getYposition();

                shareMenuAnimation();
                break;

            case R.id.im_wallHideBtn:
                getYposition();
                if (wallMenu.getVisibility() == View.VISIBLE) {
                    wallpaperMenuAnimation();

                    setMenuIcons("");
                }
                break;

            case R.id.choose_option:
                /*if (!marshPermission.checkPermissionForExternalStorage() || !marshPermission.checkPermissionForREADExternalStorage() || !marshPermission.checkPermissionCamera()) {
                    marshPermission.requestPermissionForExternalStorage1();
                } else {
                    getYourBackground();
                }
*/
                requestAppPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},R.string.messagee,EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);

                break;

            case R.id.im_tts:
                if (optionContainer != null) {
                    optionContainer.removeAllViews();
                }
                im_tts.setImageResource(R.drawable.btn_tts_128x128_selected);
                im_voice.setImageResource(R.drawable.btn_mic_128x128);

                showTtsMenu();
                break;

            case R.id.im_voice:
                if (optionContainer != null) {
                    optionContainer.removeAllViews();
                }
                isCharText = false;
                im_voice.setImageResource(R.drawable.btn_mic_128x128_selected);
                im_tts.setImageResource(R.drawable.btn_tts_128x128);
                showVoiceMenu();
                break;

            case R.id.im_voiceMenuHide:
                getYposition();
                if (voiceMenu.getVisibility() == View.VISIBLE) {
                    voiceMenuAnimation();
                    setMenuIcons("");
                }
                break;

            case R.id.im_hideAccMenuBtn:
                getYposition();
                selectedWearMenuAnimation();
                updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER3_VISIBLE);
                break;

            case R.id.fx_layout:
                showActionSheet("fxs", ioListner, Constants.fxCategories);
                break;

            case R.id.im_mouth:
                isColorAcc = isMouthClicked;
                setColorIcon();
                setMouthColor();
                break;

            case R.id.im_eyes:
                isColorAcc = isEyesClicked;
                setColorIcon();
                setEyesColor();
                break;

            case R.id.im_skin:
                isColorAcc = isSkinClicked;
                setColorIcon();
                setSkinColor();
                break;

            case R.id.im_hair:
                isColorAcc = isHairClicked;
                setColorIcon();
                setHairColor();
                break;

            case R.id.iv_trash:
                Log.e("originalPos", "" + UNIQUE_KEY);
                DeletePopUp deletePopUp = new DeletePopUp(MainActivity.this, mListners, actualPos, true);
                deletePopUp.show();
                deletePopUp.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 80 / 100,
                        WindowManager.LayoutParams.WRAP_CONTENT);
                actualPos = 0;
                break;

            default:
                break;
        }
    }

    private void resetChecks() {
        costume = true;
        hair = true;
        hat = true;
        fhair = true;
        mouth = true;
        props = true;
        glasses = true;
        necklace = true;
    }

    public void setMouthColor() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClient());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("mouth");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setEyesColor() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClient());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("eyes");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setSkinColor() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClient());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("skin");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHairColor() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClient());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("hair");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSkinColorForSaveChar() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClientForSaveChar());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("skin");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHairColorForSaveChar() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClientForSaveChar());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("hair");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMouthColorForSaveChar() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClientForSaveChar());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("mouth");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setEyesColorForSaveChar() {
        try {
            icColorChanging = true;
            webView.setWebChromeClient(new MyWebChromeClientForSaveChar());
            StringBuilder bufer = new StringBuilder("javascript:alert(getBodyPartColor('");
            bufer.append("eyes");
            bufer.append("'));");
            webView.loadUrl(bufer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void SaveCharacterMenu() {

        if (saveCharLay.getVisibility() == View.VISIBLE) {
            counter_for_saved_character = 0;
            save_char = true;
            character_saving = true;

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);

            saveCharLay.setVisibility(View.GONE);
            im_library.setImageResource(R.drawable.btn_library_unselect);
            menuContainer3.setVisibility(View.GONE);
            hideShowMenu3.setVisibility(View.GONE);

            if (ivTrash.getVisibility() == View.VISIBLE)
                ivTrash.setVisibility(View.GONE);
          /*  YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);
*/

            Log.e("one", "if");

        } else {
            Log.e("one", "else");
            showAllSavedCharacters();


        }
    }


    private void helperForSave() {
        YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(menuContainer3);

        YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(hideShowMenu3);
        im_library.setImageResource(R.drawable.btn_library_select);

        menuContainer.setVisibility(View.VISIBLE);
        hideShowMenu.setVisibility(View.GONE);
        menuContainer2.setVisibility(View.VISIBLE);
        hideShowMenu2.setVisibility(View.GONE);
        menuContainer3.setVisibility(View.VISIBLE);
        hideShowMenu3.setVisibility(View.VISIBLE);
        saveCharLay.setVisibility(View.VISIBLE);
        im_catUp2.setVisibility(View.VISIBLE);
        im_catDown2.setVisibility(View.VISIBLE);
    }

    /**
     * Method for setting up Menu icons based on Selection or Deselection ...
     */
    public void setMenuIcons(String clicked) {
        try {
            if (isLibraryClicked) {
                im_library.setImageResource(R.drawable.btn_library_select);
                Log.e("inLibClick", "click");
            } else {
                im_library.setImageResource(R.drawable.btn_library_unselect);
                Log.e("inLibClick", "noclick");
            }

            if (isSaveLibraryClicked) {
                im_saveChar.setImageResource(R.drawable.btn_save_to_library_select);
            } else {
                im_saveChar.setImageResource(R.drawable.btn_save_to_library_unselect);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDefaultColorSelection() {

        if (checkAvailList.size() > 0) {
            if (checkAvailList.get(0).equals("true") && checkAvailList.get(1).equals("false") && checkAvailList.get(2).equals("false")
                    && checkAvailList.get(3).equals("false")) {

                isColorAcc = isMouthClicked;
            } else if (checkAvailList.get(0).equals("false") && checkAvailList.get(1).equals("false") && checkAvailList.get(2).equals("false")
                    && checkAvailList.get(3).equals("true")) {

                isColorAcc = isSkinClicked;
            } else if (checkAvailList.get(0).equals("false") && checkAvailList.get(1).equals("true") && checkAvailList.get(2).equals("false")
                    && checkAvailList.get(3).equals("false")) {

                isColorAcc = isEyesClicked;
            } else if (checkAvailList.get(0).equals("false") && checkAvailList.get(1).equals("false") && checkAvailList.get(2).equals("true")
                    && checkAvailList.get(3).equals("false")) {

                isColorAcc = isHairClicked;
            } else if (checkAvailList.get(0).equals("true") && checkAvailList.get(1).equals("true") && checkAvailList.get(2).equals("true")
                    && checkAvailList.get(3).equals("true")) {

                isColorAcc = isSkinClicked;
            } else if (checkAvailList.get(0).equals("true") || checkAvailList.get(1).equals("true") || checkAvailList.get(3).equals("true")
                    && (checkAvailList.get(2).equals("false"))) {
                isColorAcc = isMouthClicked;
            }
        }
    }

    public static double getAngle(double x, double y) {

        return Math.atan2(y, x) * 180.0 / Math.PI;
        // return 1.5 * Math.PI - Math.atan2(y, x); //note the atan2 call, the order of paramers is y then x
    }

    private void shareButton() {

        boolean share_option = false;
        if (ivTrash.getVisibility() == View.VISIBLE) {
            ivTrash.setVisibility(View.GONE);
        }
        api_link_flag = true;

        getYposition();

        if (shareMenu.getVisibility() == View.GONE || shareMenu.getVisibility() == View.INVISIBLE) {
            if (is_there_any_change_in_character) {
                sensor_handler = false;
                File path = new File(picturePath);
                if (!picturePath.isEmpty() && path.exists()) {
                    Log.d("picture_path", "" + picturePath);
                    uploading_background = true;
                    uploadBackground();
                } else {
                    uploading_background = false;
                    fetChApiLnk(1);
                }
            } else {
                share_option = true;
            }

        } else {
            shareMenuAnimation();
        }

        PREVIOUSClicked = "";
        rightMenuContainer.setVisibility(View.GONE);
        if (rightMenuContainer.getVisibility() == View.VISIBLE) {
            im_hideDrawer.setVisibility(View.GONE);

            menuItems.get(0).put("is_selection", "false");
            rightMenuAdapter.notifyDataSetChanged();
            rightDrawerLayout.startAnimation(exit_to_right);
            im_hideDrawer.startAnimation(exit_to_right);
            exit_to_right.setAnimationListener(new Animation.AnimationListener() {


                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rightMenuContainer.setVisibility(View.GONE);
                    rightDrawerLayout.setVisibility(View.GONE);
                    im_hideDrawer.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }


            });
        }


        if (menuContainer2.getVisibility() == View.VISIBLE) {
            charMenuAnimation();
        }

        if (accMenu.getVisibility() == View.VISIBLE) {
            accMenuAnimation();
        }

        if (wallMenu.getVisibility() == View.VISIBLE) {
            wallpaperMenuAnimation();
        }

        if (colorPickerContainer.getVisibility() == View.VISIBLE) {
            colorMenuAnimation();
        }

        if (voiceMenu.getVisibility() == View.VISIBLE) {
            voiceMenuAnimation();
        }

        if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
            drawerMenuAnimation();
        }


        if (selectedAccMenu.getVisibility() == View.VISIBLE) {
            selectedWearMenuAnimation();
        }

        if (menuContainer3.getVisibility() == View.VISIBLE) {
            menuContainer3Animation();
        }


        if (menuContainer.getVisibility() == View.VISIBLE) {
            categoryMenuAnimation();
        }

        if (share_option) {
            shareMenuAnimation();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                shareOptionMenu();
                if (isMenuOpen) {
                    SharedPreferences preferences = getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
                    preferences.edit().remove(Constants.SAVE_POS).commit();
                    isMenuOpen = false;
                } else
                    isMenuOpen = true;
            }
        });

    }

    public void arrowHandle() {
        if (isUpClicked) {
            im_catUp.setImageResource(R.drawable.btn_arrow_up_half_sel);
        } else {
            im_catUp.setImageResource(R.drawable.btn_arrow_up_unsel);
        }

        if (isDownClicked) {
            im_catDown.setImageResource(R.drawable.btn_arrow_down_sel);
        } else {
            im_catDown.setImageResource(R.drawable.btn_arrow_down_unsel);
        }

        if (isUpClicked2) {
            im_catUp2.setImageResource(R.drawable.btn_arrow_up_half_sel);
        } else {
            im_catUp2.setImageResource(R.drawable.btn_arrow_up_unsel);
        }

        if (isDownClicked2) {
            im_catDown2.setImageResource(R.drawable.btn_arrow_down_sel);
        } else {
            im_catDown2.setImageResource(R.drawable.btn_arrow_down_unsel);
        }

    }

    /**
     * Method for Getting Character Categories ...
     */
    public void getCategoryMenuData() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (charactersCatList.size() > 0)
                    charactersCatList.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String[] list;
                try {
                    list = ReadFromfile("char", "list.txt");
                    Log.e("list.length", "" + list.length);
                    if (list.length > 0) {
                        for (int i = 0; i < list.length; i++) {
                            String file = list[i];
                            showModel = new ShowCategoryModel();
                            showModel.setFileName(file);

                            if (list[i].contains(Constants.NEW_CHARACTER_CATEGORY)) {
                                showModel.setSelection("true");
                            } else {
                                showModel.setSelection("false");
                            }
                            charactersCatList.add(showModel);
                            Log.e("charactersCatList", "" + charactersCatList);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (isButtonClicked.equals(FIRSTClicked)) {
                    mHandler.sendEmptyMessage(SET_LISTCHARCTER_ADAPTER);
                }
            }
        }.execute();

    }

    /**
     * Method for getting Accessories Menu Data ...
     */
    public void getWearData() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (accessoriesList.size() > 0)
                    accessoriesList.clear();

                Log.e("inUiThreaadsdasdasd", "dfkjadsfkabsdkf" + Constants.check_list.size());
            }

            @Override
            protected Void doInBackground(Void... params) {
                String[] list;
                try {
                    list = getAssets().list("img/wears/unselected");
                    Log.e("accFile", "" + list.length);
                    if (list.length > 0) {

                        for (int i = 0; i < list.length; i++) {
                            String file = list[i];
                            accListModel = new AccessoryListModel();
                            accListModel.setFile(file);
                            Log.e("FileName", "" + file);

                            accListModel.setSelection("false");
                            accessoriesList.add(accListModel);
                        }
                        Log.e("FileNameWear", "" + accessoriesList);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        accessoryAdapter = new CharacterAccessoryAdapter(MainActivity.this, accessoriesList, listener, isRandmAccess);
                        lv_accList.setAdapter(accessoryAdapter);
                    }
                });

            }
        }.execute();
    }

    /**
     * Method for Webview Click event ...
     */
    public void handleWebviewTouch() {
        if (menuContainer.getVisibility() == View.VISIBLE && (menuContainer3.getVisibility() == View.GONE && menuContainer2.getVisibility() == View.GONE
                && selectedAccMenu.getVisibility() == View.GONE && accMenu.getVisibility() == View.GONE && wallMenu.getVisibility() == View.GONE
                && colorPickerContainer.getVisibility() == View.GONE && voiceMenu.getVisibility() == View.GONE)) {

            firstMenuAnimation();

        } else {

            if (menuContainer3.getVisibility() == View.VISIBLE) {
                menuContainer3Animation();
            } else {
                if (menuContainer2.getVisibility() == View.VISIBLE) {
                    charMenuAnimation();

                }

            }

            if (selectedAccMenu.getVisibility() == View.VISIBLE) {

                settingPosition(margin_xsmall, st_pos_width);
                selectedWearMenuAnimation();
            } else {
                if (accMenu.getVisibility() == View.VISIBLE) {
                    accMenuAnimation();

                }

            }


            if (wallMenu.getVisibility() == View.VISIBLE) {

                wallpaperMenuAnimation();

            }


            if (colorPickerContainer.getVisibility() == View.VISIBLE) {
                colorMenuAnimation();

            }


            if (voiceMenu.getVisibility() == View.VISIBLE) {
                voiceMenuAnimation();

            }

            if (menuContainer.getVisibility() == View.GONE) {
                firstMenuAnimation();

            }

        }

        if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
            drawerMenuAnimation();
        }

        if (rightMenuContainer.getVisibility() == View.VISIBLE) {
            rightMenuContainer.setVisibility(View.GONE);
        }

        if (shareMenu.getVisibility() == View.VISIBLE) {
            shareMenuAnimation();
        }

        if (selectedAccMenu.getVisibility() == View.VISIBLE) {
            selectedWearMenuAnimation();
        }
        SharedPreferences preferences = getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
        preferences.edit().remove(Constants.SAVE_POS).commit();
        if (myDialog != null)
            myDialog.dismiss();
    }

    /**
     * Right Side Drawer Menu Visibility handler ...
     */
    public void handleRightMenuVisibility() {
        if (rightDrawerLayout.getVisibility() == View.VISIBLE) {

            rightDrawerLayout.startAnimation(exit_to_right);
            im_hideDrawer.startAnimation(exit_to_right);


            exit_to_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rightDrawerLayout.setVisibility(View.GONE);
                    im_hideDrawer.setVisibility(View.GONE);
                    rightMenuContainer.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_INVISIBLE);

        }
    }

    /**
     * Share Button Menu Items ...
     */
    public void shareOptionMenu() {


        String[] arr = {"facebook", "twitter", "googleplus", "email"};
        int[] unslectedImages = {R.drawable.btn_share_facebook, R.drawable.btn_share_twitter,
                R.drawable.btn_share_gplus, R.drawable.btn_share_email};

        int[] slectedImages = {R.drawable.btn_share_facebook_selected, R.drawable.btn_share_twitter_selected,
                R.drawable.btn_share_gplus_selected, R.drawable.btn_share_email_selected};

        if (shareList.size() > 0)
            shareList.clear();

        for (int i = 0; i < 4; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("share_type", arr[i]);
            map.put(isSelectedKey, "false");
            shareList.add(map);
        }

        gridShareAdapter = new GridShareAdapter(MainActivity.this, shareList, this, unslectedImages, slectedImages);
        shareGridView.setAdapter(gridShareAdapter);
    }


    public void setColorIcon() {

        try {
            Log.e("newList", "" + checkAvailList);
            for (int i = 0; i < charColorList.size(); i++) {

                HashMap<String, String> map = charColorList.get(i);

                if (map.get("cat_name").equals("mouth")) {
                    if (isColorAcc.equals(isMouthClicked)) {
                        String uri = "assets://img/ui/icons/over/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_mouth);

                    } else {
                        String uri = "assets://img/ui/icons/base/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_mouth);
                    }
                } else if (map.get("cat_name").equals("eyes")) {
                    if (isColorAcc.equals(isEyesClicked)) {
                        String uri = "assets://img/ui/icons/over/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_eyes);
                    } else {
                        String uri = "assets://img/ui/icons/base/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_eyes);
                    }
                } else if (map.get("cat_name").equals("skin")) {
                    if (isColorAcc.equals(isSkinClicked)) {
                        String uri = "assets://img/ui/icons/over/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_skin);
                    } else {
                        String uri = "assets://img/ui/icons/base/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_skin);
                    }
                } else if (map.get("cat_name").equals("hair")) {
                    if (isColorAcc.equals(isHairClicked)) {
                        String uri = "assets://img/ui/icons/over/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_hair);
                    } else {
                        String uri = "assets://img/ui/icons/base/" + map.get("file_name");
                        imageLoader.displayImage(uri, im_hair);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
        preferences.edit().remove(Constants.SESSION_KEY).commit();
        im_playButton.setImageResource(R.drawable.play);
        if (webView != null) {
            webView.removeAllViews();
            webView.destroy();
        }
        webView = null;
    }

    public void setEnabled() {

        if (checkAvailList.get(0).equals("true")) {

            Log.e("insideTrue", "jsbcjgdsc");
            rel_container_1.setBackgroundColor(Color.TRANSPARENT);
            im_mouth.setEnabled(true);
        } else {
            rel_container_1.setBackgroundColor(Color.parseColor("#66000000"));
            im_mouth.setEnabled(false);
        }

        if (checkAvailList.get(1).equals("true")) {
            rel_container_2.setBackgroundColor(Color.TRANSPARENT);
            im_eyes.setEnabled(true);
        } else {
            rel_container_2.setBackgroundColor(Color.parseColor("#66000000"));
            im_eyes.setEnabled(false);
        }

        if (checkAvailList.get(2).equals("true")) {
            rel_container_3.setBackgroundColor(Color.TRANSPARENT);
            setSkinColor();

            im_skin.setEnabled(true);
        } else {
            rel_container_3.setBackgroundColor(Color.parseColor("#66000000"));
            im_skin.setEnabled(false);
            if (checkAvailList.get(0).equals("true")) {
                isColorAcc = isMouthClicked;
                setMouthColor();
            } else if (checkAvailList.get(1).equals("true")) {
                isColorAcc = isEyesClicked;
                setEyesColor();
            } else if (checkAvailList.get(2).equals("true")) {
                isColorAcc = isHairClicked;
                setHairColor();
            }
        }

        if (checkAvailList.get(3).equals("true")) {
            rel_container_4.setBackgroundColor(Color.TRANSPARENT);
            im_hair.setEnabled(true);
        } else {
            rel_container_4.setBackgroundColor(Color.parseColor("#66000000"));
            im_hair.setEnabled(false);
        }

        setColorIcon();

    }

    public void getValidateList() {

        for (int i = 0; i < charColorList.size(); i++) {
            try {
                Log.e("inLoop", "sbdasbbasbsadbfsdfkjsdbfksdkfbsdkf");
                StringBuilder buffer = new StringBuilder("javascript:alert(iscolorBool('");
                buffer.append(charColorList.get(i).get("cat_name"));
                buffer.append("'));");
                webView.setWebChromeClient(new MyWebChromeClient());
                webView.loadUrl(buffer.toString());
                webView.clearCache(true);
                webView.clearFormData();
                webView.clearHistory();
                webView.clearMatches();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("newCheckList", "" + checkAvailList);
        }
    }

    @Override
    public void updateView(String data, String type, ArrayList<FxMenuModel> fxMenuList) {

        Log.e("selected_Data", data);
        if (fxMenuList == null || fxMenuList.isEmpty()) {
            if (type.equalsIgnoreCase("fxs")) {
                tv_fx.setText(data);
            } else if (type.equalsIgnoreCase("language")) {
                tv_language.setText(" ");
                tv_language.setText(data);
            } else if (type.equalsIgnoreCase("voicetype")) {
                tv_voiceType.setText(data);
            }
        } else {
            FxMenuListAdapter fxListAdapter = new FxMenuListAdapter(MainActivity.this, fxMenuList);
            lv_fxMenu.setAdapter(fxListAdapter);
        }
    }

    private void trackResultOfSaveCharacter() {

        String track_url_for_savechar = "http://data.oddcast.com/event.php/?apt=W&acc=1335&emb=0&uni=1&sm=0&et=0&ev[0][]=ce1&cnt[ce1]=1";

        Constants.execute(new Super_AsyncTask(MainActivity.this, track_url_for_savechar, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                if (progress != null) {
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                }

                Log.d("result", "okkk");

            }

        }, false, false));
    }

    @Override
    public void updateCharaterList(String category_select, final boolean isSame, final boolean fromDice) {
        isRandom = false;
        cat_select = category_select;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (finalCharList.size() > 0) {
                    finalCharList.clear();
                    Log.e("tag", " inside 61");
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    String list[] = ReadFromfile(cat_select, "list.txt");

                    Log.d("ReadFromfile", "" + list[0].toString());

                    if (list.length > 0) {
                        for (int i = 0; i < list.length; i++) {
                            final String file = list[i];
                            charListModel = new CharacterListModel();
                            if (!file.equals("icons")) {

                                charListModel.setSelectedFolder(cat_select);
                                charListModel.setFileName(file);
                                charListModel.setThumbnailImage("");

                                if (file.contains(Constants.CURRENT_CHARACTER_NAME)) {
                                    charListModel.setSelection("true");
                                } else {
                                    charListModel.setSelection("false");
                                }
                                finalCharList.add(charListModel);
                            }
                        }
                        Log.e("SpecsFileName", "" + specsCategory);
                        Log.e("tag", " inside 51");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (isLibraryClicked) {
                    isLibraryClicked = false;
                    saveCharLay.setVisibility(View.GONE);
                    animateCharacterMenu();
                    Log.e("tag", " inside 41");
                }

                if (isSame) {
                    if (menuContainer3.getVisibility() == View.VISIBLE) {
                        YoYo.with(Techniques.SlideOutLeft)
                                .duration(1000)
                                .playOn(menuContainer3);

                        YoYo.with(Techniques.SlideOutLeft)
                                .duration(1000)
                                .playOn(hideShowMenu3);
                        menuContainer3.setVisibility(View.GONE);
                        hideShowMenu3.setVisibility(View.GONE);
                        Log.e("tag", " inside 31");
                    } else {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(1000)
                                .playOn(menuContainer3);

                        YoYo.with(Techniques.SlideInLeft)
                                .duration(1000)
                                .playOn(hideShowMenu3);

                        menuContainer3.setVisibility(View.VISIBLE);
                        hideShowMenu3.setVisibility(View.VISIBLE);
                        Log.e("tag", " inside 32");
                    }


                } else {
                    animateCharacterMenu();
                }

                if (finalCharList.size() <= 5) {
                    im_catUp2.setVisibility(View.INVISIBLE);
                    im_catDown2.setVisibility(View.INVISIBLE);
                    Log.e("tag", " inside 21");
                } else {
                    Log.e("tag", " inside 21");
                    im_catUp2.setVisibility(View.VISIBLE);
                    im_catDown2.setVisibility(View.VISIBLE);
                }

                if (!isSame) {
                    Log.e("tag", " inside 11");
                    Log.e("update_char_list", " isSame " + isSame);
                    if (finalCharList.size() > 0) {


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                catSpecsAdapter = new CategorySpcesAdapter(MainActivity.this, finalCharList, listener);
                                lv_characterSpecs.setAdapter(catSpecsAdapter);
                            }
                        });
                        Log.e("update_char_list", " inside ");

                    }
                }

                if (fromDice) {

                    Log.e("tag", " inside 1");
                    Random random = new Random();
                    isVisiblePos = random.nextInt(finalCharList.size());
                    lv_characterSpecs.post(new Runnable() {
                        @Override
                        public void run() {
                            lv_characterSpecs.smoothScrollToPosition(isVisiblePos);
                        }
                    });

                    catSpecsAdapter.showCharacter(isVisiblePos);
                }


            }
        }.execute();

    }


    @Override
    public void onShowChracters(final ArrayList<HashMap<String, String>> str_Builder) {
        Log.e("TestTestTest", "pospospos");
        try {
            Constants.freeMemory();
            showCharacterss(str_Builder);
            updateAnim();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateAnim() {
        if (thread != null)
            thread.interrupt();
    }

    @Override
    public void showAccessorries(String file, boolean isSame) {

        try {
            Log.d("filenameselected", "" + file);

            showWearMenu(file, isSame);
            StringBuilder buffer = new StringBuilder("javascript:alert(getAcess('");
            buffer.append(file);
            buffer.append("'));");
            webView.setWebChromeClient(new AccessoryIDGetter());
            webView.loadUrl(buffer.toString());
            webView.clearCache(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void updateChangeAccessorries(String id, String filename) {
        changeAccessories(id, filename);
    }

    @Override
    public void updateWallPapers(final int pos, final String name) {

        webViewContainer.post(new Runnable() {
            @Override
            public void run() {
                try {
                    getWallpaperFromXml(pos, name);
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void deleteSavedCharcters(ArrayList<HashMap<String, String>> mapArrayList, int pos) {
        actualPos = pos;
        showSavedCharacterss(mapArrayList);
    }

    private void captureScreenShot() {
        try {
            capture_thumbnail = "SCREEN_SHOT_FOR_SHARING";
            StringBuilder bufferr = new StringBuilder("javascript:alert(CreateThumbnail('");
            bufferr.append(350);
            bufferr.append("','");
            bufferr.append(280);
            bufferr.append("','");
            bufferr.append(0);
            bufferr.append("','");
            bufferr.append(0);
            bufferr.append("'));");
            webView.setWebChromeClient(new ScreenShotClient());
            webView.loadUrl(bufferr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateFb(final int position) {
        where_to_share_position = position;
        captureScreenShot();

    }

    @Override
    public void removeSaveCharcter(int pos) {
        Log.e("position_to_remove", "" + pos);
        try {
            charList.remove(pos);
            removeFromDb(pos);

            mHandler.sendEmptyMessage(POPULATE_LIST);

            if (charList.size() > 0)
                ivTrash.setVisibility(View.VISIBLE);
            else
                ivTrash.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeFromDb(final int pos) {
        new AsyncTask<Void, String, ArrayList<String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //     loader.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<String> doInBackground(Void... params) {

                ArrayList<String> newId = new ArrayList<String>();
                newId = dataHandler.fetchId();
                return newId;

            }

            @Override
            protected void onPostExecute(ArrayList<String> result) {
                super.onPostExecute(result);
                try {
                    String newId = result.get(pos);
                    dataHandler.deletePost(newId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //   loader.setVisibility(View.INVISIBLE);
            }
        }.execute();


    }


    private void sendEmail() {

        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out my Voki");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "My Voki has something to say - check it out:\n\n" + MainActivity.final_url_for_sharing + "\n\nTo create your own Voki go to: www.voki.com\n\n The Voki app here - https://play.google.com/store/apps/details?id=com.oddcast.android.voki&hl=en");

        try {
            startActivityForResult(Intent.createChooser(emailIntent, "Send mail..."), 110);

            Log.d("Finished sending.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }


    }


    private class GazeApiClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, final String url, final String message, final JsResult result) {
            webView.post(new Runnable() {
                @Override
                public void run() {


                    result.confirm();
                }
            });

            return true;
        }
    }

    private class ApiWebChromeClient extends WebChromeClient {
        int switch_pos = 0;

        ApiWebChromeClient(int switch_pos) {
            this.switch_pos = switch_pos;
        }

        @Override
        public boolean onJsAlert(WebView view, final String url, final String message, final JsResult result) {
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Constants.GET_CHARACTER_URL = message;
                    switch (switch_pos) {

                        case 1:

                            if (message != null) {
                                if (initializetts_menu) {

                                    text_to_speak_by_character = et_typeMessage.getText().toString();
                                }
                                if (text_to_speak_by_character.trim().length() > 0) {
                                    Log.d("text_to_speakelse", "" + text_to_speak_by_character);
                                    StringWriter xml = Extra.createXmlFile(str_session_key, message, et_typeMessage.getText().toString(), tv_language.getText().toString(), tv_voiceType.getText().toString(), backgound_id, fx_type, fx_level);
                                    final_save_xml = xml.toString();
                                    if (uploading_background)
                                        progress_text.setText("50%");
                                } else {
                                    Log.d("text_to_speaki", "" + text_to_speak_by_character);
                                    Log.e("str_session_key", "" + str_session_key);
                                    StringWriter xml = Extra.createXmlFile(str_session_key, message, "", "", "", backgound_id, "", "");
                                    final_save_xml = xml.toString();
                                    if (uploading_background)
                                        progress_text.setText("50%");
                                }
                                Log.e("final_save_xml1", "" + final_save_xml);
                                webImageClient();
                                //  apirequest(final_save_xml);


                            }
                            break;

                        case 2:


                            Log.e("main_activity", "" + message);
                            Log.e("main_activity", "" + result);
                            Log.e("main_activity_url", "" + url);

                            if (counter_for_saved_character > 3) {
                                try {
                                    Log.e("valueof>>", " third");
                                    icColorChanging = false;


                                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                                    float testscreenHeightDp = metrics.heightPixels / metrics.density;
                                    float testscreenWidthDp = metrics.widthPixels / metrics.density;
                                    float newXwidth = getResources().getDimension(R.dimen.option_1_width) / metrics.density;

                                    float finalxWidth = newXwidth;
                                    Log.i("getDimensionsPixels", "testscreenHeightDp  = " + testscreenHeightDp + "  testscreenWidthDp=  " + testscreenHeightDp);
                                    testscreenWidthDp = testscreenWidthDp - finalxWidth;
                                    testscreenWidthDp = testscreenWidthDp + 50;
                                    Log.i("getDimensionsPixels", "laydimen1  = " + testscreenWidthDp);
                                    StringBuilder buffer = new StringBuilder("javascript:alert(CreateThumbnail('");
                                    buffer.append(testscreenWidthDp);
                                    buffer.append("','");
                                    buffer.append(testscreenWidthDp);
                                    buffer.append("','");
                                    buffer.append(0);
                                    buffer.append("','");
                                    buffer.append(0);
                                    buffer.append("'));");
                                    Log.e("create_buffer", "" + buffer);
                                    capture_thumbnail = "SAVE_CHARACTER_THUMBNAIL";
                                    webView.setWebChromeClient(new ScreenShotClient());
                                    webView.loadUrl(buffer.toString());
                                    webView.clearCache(true);
                                    webView.clearFormData();
                                    webView.clearHistory();
                                    webView.clearMatches();
                                } catch (Resources.NotFoundException e) {
                                    e.printStackTrace();
                                }

                            }
                            break;


                    }


                    result.confirm();
                }
            });

            return true;
        }
    }

    public void webImageClient() {

        try {
            webView.setWebChromeClient(new webImageChromeClient());
            StringBuilder buffer = new StringBuilder("javascript:alert(image('");
            buffer.append("'));");
            webView.loadUrl(buffer.toString());
            webView.clearCache(true);
            webView.clearFormData();
            webView.clearHistory();
            webView.clearMatches();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class webImageChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, final String url, final String message, final JsResult result) {
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("AccessoryIDGetter1", "" + url);
                    Log.e("webImageChromeClient", "" + message);
                    Log.e("AccessoryIDGetter3", "" + result);

                    apirequest(message);
                    result.confirm();
                }


            });


            return true;
        }
    }


    private class AccessoryIDGetter extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, final String url, final String message, final JsResult result) {
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("AccessoryIDGetter1", "" + url);
                    Log.e("AccessoryIDGetter2", "" + message);
                    Log.e("AccessoryIDGetter3", "" + result);


                    result.confirm();
                }


            });


            return true;
        }
    }

    private void StoringFileImage(String base64ImageData) {


        byte[] decodedString = Base64.decode(base64ImageData, Base64.DEFAULT);

        File dir = new File(Environment.getExternalStorageDirectory() + "/voki");
        dir.mkdirs();
        if (dir.exists() && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        } else {
            dir.mkdirs();
        }

        photo = new File(Environment.getExternalStorageDirectory(), "/voki/photoo" + System.nanoTime() + ".jpeg");
        try {
            FileOutputStream fos = new FileOutputStream(photo.getPath());

            fos.write(decodedString);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        img_str = base64ImageData;

        if (where_to_share_position == 0) {
            ViewUtill viewUtill = new ViewUtill(this);
            viewUtill.facebookIntialization(shareDialog, callbackManager, this);
        } else if (where_to_share_position == 2) {
         /*   if (!marshPermission.checkPermissionForGET_ACCOUNTS() || !marshPermission.checkPermissionForREADExternalStorage() || !marshPermission.checkPermissionForExternalStorage()) {
                marshPermission.requestPermissionGET_ACCOUNTS();
            } else {
                Intent signInIntent = new Intent(MainActivity.this, GooglePlusSignIn.class);
                startActivity(signInIntent);
            }*/
            requestAppPermissions(new String[]{Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},R.string.messagee,GET_ACCOUNTS);

        } else if (where_to_share_position == 1) {
           /* if (!marshPermission.checkPermissionForInternetaccess()) {
                marshPermission.requestPermissionGetInternetAccess();
            } else {
                Intent signInIntent = new Intent(MainActivity.this, TwitterIntegration.class);
                startActivity(signInIntent);
            }*/

            requestAppPermissions(new String[]{Manifest.permission.INTERNET},R.string.messagee, INTERNET_PERMISSION);

        } else if (where_to_share_position == 3) {
            sendEmail();
        }
        where_to_share_position = -1;

    }

    private class ScreenShotClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, final String message, final JsResult result) {

            final String urll = url;
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("main_activity----1", "" + "1" + urll);
                    Log.d("main_activity----2", "" + "2" + message);
                    Log.d("main_activity----3", "" + "3" + result);


                    Log.e("main_activity", "" + message);

                    if (message.contains("image/png;base64") && capture_thumbnail.equals("SCREEN_SHOT_FOR_SHARING")) {
                        Log.e("website_image_data", "" + message);
                        String[] str = message.split(",");
                        String base64 = str[1];
                        StoringFileImage(base64);

                    }
                    if (message.contains("image/png;base64") && capture_thumbnail.equals("SAVE_CHARACTER_THUMBNAIL")) {
                        String[] str = message.split(",");
                        base64 = str[1];
                        saveCurrentCharacter(base64);
                    }


                    result.confirm();
                    Log.e("checkAvail", "" + checkAvailList);
                }
            });

            return true;
        }
    }

    private class MyWebChromeClientForSaveChar extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, final String message, final JsResult result) {

            final String urll = url;
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("main_activity----1", "" + "1" + urll);
                    Log.d("main_activity----2", "" + "2" + message);
                    Log.d("main_activity----3", "" + "3" + result);
                    if (character_saving) {
                        counter_for_saved_character++;
                    }

                    saved_char_color_list.add(message);

                    if (counter_for_saved_character == 1) {
                        setMouthColorForSaveChar();
                    } else if (counter_for_saved_character == 2) {
                        setEyesColorForSaveChar();
                    } else if (counter_for_saved_character == 3) {
                        setHairColorForSaveChar();
                    } else if (counter_for_saved_character == 4) {
                        fetChApiLnk(2);
                    }
                    result.confirm();
                    Log.e("checkAvail", "" + checkAvailList);
                }
            });

            return true;
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, final String message, final JsResult result) {

            final String urll = url;
            webView.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d("main_activity----1", "" + "1" + urll);
                        Log.d("main_activity----2", "" + "2" + message);
                        Log.d("main_activity----3", "" + "3" + result);


                        if (icColorChanging) {
                            getColor = message;
                            Log.e("getColor", getColor);
                            if (getColor != null && !getColor.equals("true") && !getColor.equals("false") && !getColor.equals("NaN") && !getColor.equals("undefined")) {
                                String defaultColor = Constants.intToHexa(Integer.parseInt(getColor));
                                Log.e("defaultColor", defaultColor);
                                mColorPickerView.setColor(Color.parseColor(defaultColor));
                            }
                        } else {
                            if (message.toString().equalsIgnoreCase("true")) {
                                checkAvailList.add(message.toString());
                            } else {
                                checkAvailList.add("false");
                            }
                            if (checkAvailList.size() > 3) {
                                setEnabled();
                            }
                        }

                        result.confirm();
                        Log.e("checkAvail", "" + checkAvailList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            return true;
        }
    }


    @Override
    public void onColorChanged(int newColor) {


        String hexColor = Constants.intToHexa(mColorPickerView.getColor());
        Log.e("onColorChanged", "" + hexColor);

        StringBuilder buffer = new StringBuilder("javascript:changeEyeColorToGreen('");

        if (isColorAcc.equals(isHairClicked)) {
            buffer.append("hair");
            initHairColor = mColorPickerView.getColor();
        } else if (isColorAcc.equals(isEyesClicked)) {
            buffer.append("eyes");
            initEyeColor = mColorPickerView.getColor();
        } else if (isColorAcc.equals(isSkinClicked)) {
            buffer.append("skin");
            initSkinColor = mColorPickerView.getColor();
        } else if (isColorAcc.equals(isMouthClicked)) {
            buffer.append("mouth");
            initMouthColor = mColorPickerView.getColor();
        }
        buffer.append("','");
        buffer.append(hexColor);
        buffer.append("');");

        try {
            webView.loadUrl(buffer.toString());
            webView.clearCache(true);
            webView.clearFormData();
            webView.clearHistory();
            webView.clearMatches();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * OnLongClickListener for Buttons ...
     *
     * @param v
     * @return
     */
    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.im_option1:
                Toast.makeText(MainActivity.this, "Head", Toast.LENGTH_SHORT).show();
                break;

            case R.id.im_option2:
                Toast.makeText(MainActivity.this, "Accessories", Toast.LENGTH_SHORT).show();
                break;

            case R.id.im_option3:
                Toast.makeText(MainActivity.this, "Background", Toast.LENGTH_SHORT).show();
                break;

            case R.id.im_option4:
                Toast.makeText(MainActivity.this, "Color", Toast.LENGTH_SHORT).show();
                break;

            case R.id.im_option5:
                Toast.makeText(MainActivity.this, "Voice", Toast.LENGTH_SHORT).show();
                break;

            case R.id.extra_option:
                Toast.makeText(MainActivity.this, "Library", Toast.LENGTH_SHORT).show();
                break;

        }
        return true;
    }

    /**
     * Method for showing wallpapers options.......
     */

    public void getWallpapersData() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (wallpapersList.size() > 0)
                    wallpapersList.clear();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String[] list;
                try {
                    list = ReadFromfile("backgrounds", "list.txt");
                    if (list.length > 0) {
                        for (int i = 0; i < list.length; i++) {
                            String file = list[i];
                            wallpaperListModel = new WallpaperListModel();
                            Log.e("wallFile", "" + file);
                            wallpaperListModel.setFileName(file);
                            if (i == 1) {
                                wallpaperListModel.setSelection("true");
                            } else {
                                wallpaperListModel.setSelection("false");
                            }
                            wallpapersList.add(wallpaperListModel);
                        }
                        Log.e("wallpaperCategory", "" + wallpapersList);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try {
                    if (transDialog.isShowing())
                        transDialog.dismiss();

                    if (isButtonClicked.equals(THIRDClicked)) {

                    }

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (wallCatAdapter != null)
                                    wallCatAdapter.notifyDataSetChanged();
                            } catch (IllegalStateException ex) {
                                ex.printStackTrace();
                            }

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute();


    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorMan.registerListener((SensorEventListener) this, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }


    public void getYourBackground() {

        final String[] stringItems = {"Choose Option", "Take Photo", "Choose From Gallery"};
        final ActionSheetDialog dialog = new ActionSheetDialog(MainActivity.this, stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                T.showShort(MainActivity.this, stringItems[position]);

                if (position == 1) {
                    Log.e("pos1", "dalsjhdjk");
                    MainActivity.is_there_any_change_in_character = true;
                    captureImage();

                } else if (position == 2) {
                    Log.e("pos2", "dalsjhdjk");

                    try {
                        MainActivity.is_there_any_change_in_character = true;
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_CODE);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }


                }

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("insideActivity", "" + requestCode + "--" + resultCode + "dataq" + data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == GALLERY_CODE && resultCode == RESULT_OK) {
            try {
                picturePath = "";
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                Log.d("gallery_path", "" + picturePath);
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                BitmapFactory.Options options;
                options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);


                Drawable dr = new BitmapDrawable(bitmap);
                webViewContainer.setBackgroundDrawable(dr);

                // uploadBackground();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK
                && requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {


            BitmapFactory.Options options;

            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bitmap = BitmapFactory.decodeFile(
                    destination.getAbsolutePath(), options);

            Drawable dr = new BitmapDrawable(bitmap);
            webViewContainer.setBackgroundDrawable(dr);

            // uploadBackground();
        } else if (requestCode == 110) {

            Toast.makeText(MainActivity.this, "Your Voki has been shared", Toast.LENGTH_SHORT).show();


        }
    }


    public static void updateCharacterPosition(String xpos, String ypos, String menuName) {
        final StringBuilder[] buffer = {null};

        Log.e("charPos", xpos + "--" + ypos);
        String x = null, y = null;


        if (menuName.equals(Constants.MENU_CONTAINER_VISIBLE)) {
            boolean mcv = true;
            x = String.valueOf(Double.parseDouble(xpos) + (40));
            y = ypos;

            xPos = Float.parseFloat(x);
            yPos = Float.parseFloat(y);

            Log.e("xPosYpos1", "xPosyPos1 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);

        } else if (menuName.equals(Constants.NEW_CONTAINER_SHOW_CHARACTER)) {

            x = String.valueOf(Double.parseDouble(xpos) + (105));
            Log.e("new Xpos", "" + x);
            y = ypos;

            xPos = Float.parseFloat(x);
            yPos = Float.parseFloat(y);

            Log.e("xPosYpos2", "xPosyPos2 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);

        } else if (menuName.equals(Constants.MENU_CONTAINER2_VISIBLE)) {

            x = String.valueOf(Double.parseDouble(xpos) + (70));
            Log.e("new Xpos", "" + x);
            y = ypos;

            xPos = Float.parseFloat(x);
            yPos = Float.parseFloat(y);

            Log.e("xPosYpos2", "xPosyPos2 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);

        } else if (menuName.equals(Constants.MENU_CONTAINER3_VISIBLE)) {

            x = String.valueOf(Double.parseDouble(xpos) + (90));
            y = ypos;

            xPos = Float.parseFloat(x);
            yPos = Float.parseFloat(y);

            Log.e("xPosYpos3", "xPosyPos3 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);

        } else if (menuName.equals(Constants.COLORPICKER_CONTAINER_VISIBLE)) {
            Log.e("inColorpick", "asdasd");
            x = String.valueOf(Double.parseDouble(xpos) + (150));
            y = ypos;
            xPos = Float.parseFloat(x);
            yPos = Float.parseFloat(y);
            Log.e("xPosYpos4", "xPosyPos4 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);

        } else if (menuName.equals(Constants.MENU_CONTAINER2_INVISIBLE)) {
            x = String.valueOf(Double.parseDouble(xpos) + 40);
            y = ypos;
            Log.e("xPosYpos5", "xPosyPos5 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        } else if (menuName.equals(Constants.MENU_CONTAINER3_INVISIBLE)) {
            x = String.valueOf(Double.parseDouble(xpos) + 70);
            y = ypos;
            Log.e("xPosYpos6", "xPosyPos6 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        } else if (menuName.equals(Constants.COLORPICKER_CONTAINER_INVISIBLE)) {
            x = xpos;
            y = ypos;
            Log.e("xPosYpos7", "xPosyPos7 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        } else if (menuName.equals(Constants.MENU_CONTAINER_INVISIBLE)) {
            x = xpos;
            y = ypos;
            Log.e("xPosYpos8", "xPosyPos8 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        } else if (menuName.equals(Constants.RIGHT_CONTAINER_VISIBLE)) {
            x = String.valueOf(Double.parseDouble(xpos) + (-150));
            y = ypos;
            Log.e("xPosYpos9", "xPosyPos9 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        } else if (menuName.equals(Constants.RIGHT_CONTAINER_INVISIBLE)) {
            x = xpos;
            y = ypos;
            Log.e("xPosYpos10", "xPosyPos10 " + x + " " + y + " xpos " + xpos + " ypos " + ypos);
        }

        Constants.TEMP_CHAR_XPOS = x;
        Constants.TEMP_CHAR_YPOS = y;


        handlerr.post(new Runnable() {
            @Override
            public void run() {
                if (Constants.TEMP_CHAR_XPOS != null && Constants.TEMP_CHAR_YPOS != null) {
                    buffer[0] = new StringBuilder("javascript:updateBoxPosition('");
                    buffer[0].append(Constants.TEMP_CHAR_XPOS);
                    buffer[0].append("','");
                    buffer[0].append(Constants.TEMP_CHAR_YPOS);
                    buffer[0].append("','");
                    buffer[0].append(screenWidth);
                    buffer[0].append("','");
                    buffer[0].append(screenHeight);
                    buffer[0].append("');");

                    Log.e("bufferbuffer", "buffer " + buffer[0].toString());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            if (webView != null) {
                                webView.evaluateJavascript(buffer[0].toString(), null);
                                webView.clearCache(true);
                                webView.clearFormData();
                                webView.clearHistory();
                                webView.clearFormData();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            if (webView != null) {
                                webView.loadUrl(buffer[0].toString());
                                webView.clearCache(true);
                                webView.clearFormData();
                                webView.clearHistory();
                                webView.clearFormData();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });


    }


    /**
     * Method to show character from list .......
     *
     * @param strBuilder
     */
    private void showCharacterss(final ArrayList<HashMap<String, String>> strBuilder) {
        webView.post(new Runnable() {
            @Override
            public void run() {

                if (randomiserclicked) {
                    showLoaderPosition(1000, 850);
                    X_VAL = true;
                    Log.e("strBuilder", "loading .. " + strBuilder);
                    randomAsync(strBuilder);
                    Constants.X_VALUE = "0.014";
                } else {
                    try {
                        X_VAL = true;
                        MainActivity.character_selection_way_is_randomiser = false;
                        Constants.X_VALUE = "0.0";
                        minusno = 0;
                        showLoaderPosition(1000, 850);
                        for (int i = 0; i < strBuilder.size(); i++) {
                            HashMap<String, String> map = strBuilder.get(i);
                            String url = map.get("str_build");
                            String x_pos = map.get("x_pos");
                            String y_pos = map.get("y_pos");
                            String scale_pos = map.get("scale");
                            Log.e("char_url", "" + url);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                webView.evaluateJavascript(url, null);
                            } else {
                                webView.loadUrl(url);
                            }
                            StringBuilder buffer = new StringBuilder("javascript:positionChar('");

                            buffer.append(x_pos);
                            buffer.append("','");
                            buffer.append(y_pos);
                            buffer.append("','");
                            buffer.append(scale_pos);
                            buffer.append("');");

                            webView.loadUrl(buffer.toString());
                            webView.clearCache(true);
                            webView.clearFormData();
                            webView.clearHistory();
                            webView.clearMatches();
                            Log.e("load_char", "loading .. " + x_pos + " y_pos " + y_pos);
                            if (webView != null)
                                webView.setVisibility(View.INVISIBLE);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    catSpecsAdapter.notifyDataSetChanged();
                                }
                            });
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (webView != null)
                                        webView.setVisibility(View.VISIBLE);

                                }
                            }, 1000);

                     /*   if (menuContainer3.getVisibility() == View.VISIBLE) {
                            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.NEW_CONTAINER_SHOW_CHARACTER);


                        }
*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
                minusno = margin_xllarge;
                settingPosition(on_showchar, st_pos_width);


            }
        });

    }

    private void randomAsync(final ArrayList<HashMap<String, String>> strBuilder) {

        new AsyncTask<Void, Void, String>() {
            String x_pos = null, y_pos = null, scale_pos = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                randomiserclicked = false;

            }

            @Override
            protected void onPostExecute(String url) {
                super.onPostExecute(url);
                Log.e("char_url", "" + url);

                try {
                    doRandom(url, x_pos, y_pos, scale_pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("load_char", "loading .. " + x_pos + " y_pos " + y_pos + "scale" + scale_pos);
            }

            @Override
            protected String doInBackground(Void... params) {
                String url = null;


                for (int i = 0; i < strBuilder.size(); i++) {


                    HashMap<String, String> map = strBuilder.get(i);

                    url = map.get("str_build");
                    x_pos = map.get("x_pos");
                    y_pos = map.get("y_pos");
                    scale_pos = map.get("scale");


                    Log.d("CURRENT_CHAR_XPOS", "" + Constants.CURRENT_CHAR_XPOS);


                }

                return url;
            }

        }.execute();
    }

    private void showSavedCharacterss(ArrayList<HashMap<String, String>> strBuilder) {


        settingPosition(on_showchar, st_pos_width);

        showLoaderPosition(650, 500);
        for (int i = 0; i < strBuilder.size(); i++) {

            HashMap<String, String> map = strBuilder.get(i);

            final String url = map.get("str_build");

            String x_pos = map.get("x_pos");
            String y_pos = map.get("y_pos");
            final String scale_pos = map.get("scale");

            Log.e("char_url", "" + url);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        try {
                            webView.evaluateJavascript(url, null);
                            webView.clearCache(true);
                            webView.clearFormData();
                            webView.clearHistory();
                            webView.clearFormData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            webView.loadUrl(url);
                            webView.clearCache(true);
                            webView.clearFormData();
                            webView.clearHistory();
                            webView.clearFormData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Log.e("pos_charrr1", "x_value" + Constants.X_VALUE + "y_value" + Constants.TEMP_CHAR_YPOS + "scale" + scale_pos);
                        if (X_VAL)
                            Constants.X_VALUE = "0.0";
                        StringBuilder buffer = new StringBuilder("javascript:positionChar('");
                        buffer.append(Constants.X_VALUE);
                        buffer.append("','");
                        buffer.append(Constants.TEMP_CHAR_YPOS);
                        buffer.append("','");
                        buffer.append(scale_pos);
                        buffer.append("');");
                        try {
                            webView.evaluateJavascript(buffer.toString(), null);
                            webView.clearCache(true);
                            webView.clearFormData();
                            webView.clearHistory();
                            webView.clearMatches();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        Log.e("pos_charrr2", "x_value" + Constants.X_VALUE + "y_value" + Constants.TEMP_CHAR_YPOS + "scale" + scale_pos);
                        StringBuilder buffer = new StringBuilder("javascript:positionChar('");
                        buffer.append(Constants.X_VALUE);
                        buffer.append("','");
                        buffer.append(Constants.TEMP_CHAR_YPOS);
                        buffer.append("','");
                        buffer.append(scale_pos);
                        buffer.append("');");
                        try {
                            webView.loadUrl(buffer.toString());
                            webView.clearCache(true);
                            webView.clearFormData();
                            webView.clearHistory();
                            webView.clearMatches();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            webView.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (webView != null) {
                        try {
                            webView.setVisibility(View.VISIBLE);
                            show_save_character = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            show_save_character = true;
                        }
                    }

                }
            }, 1500);


            //  Log.e("load_char", "loading .........." + buffer.toString());
        }
    }

    private void captureImage() {

        picturePath = "";
        destination = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        picturePath = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination = new File(picturePath);

        Log.e("FileDestination", "" + destination);
        Log.e("picturePath", "" + picturePath);


        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    /**
     * Method for saving current character that is showing ......
     *
     * @param img
     */
    private void saveCurrentCharacter(String img) {

        JSONObject jsonObject = new JSONObject();
        try {
            Log.d("saved_char_color_list", "" + saved_char_color_list.size());
            jsonObject.put(DatabaseHandler.CHARACTER_CATEGORY, Constants.TEMP_SAVED_CATEGORY);
            jsonObject.put(DatabaseHandler.CHARACTER_NAME, Constants.TEMP_SAVED_CHARACTER);
            jsonObject.put(DatabaseHandler.CHARACTER_BACKGROUND, Constants.TEMP_SAVED_BACKGROUND);
            jsonObject.put(DatabaseHandler.CHAR_X_POSITION, Constants.TEMP_CHAR_XPOS);
            jsonObject.put(DatabaseHandler.CHAR_Y_POSITION, Constants.TEMP_CHAR_YPOS);
            jsonObject.put(DatabaseHandler.EYES_COLOR, String.valueOf(saved_char_color_list.get(2).toString()));
            jsonObject.put(DatabaseHandler.MOUTHCOLOR, String.valueOf(saved_char_color_list.get(1).toString()));
            jsonObject.put(DatabaseHandler.HAIRCOLOR, String.valueOf(saved_char_color_list.get(3).toString()));
            jsonObject.put(DatabaseHandler.SKINCOLOR, String.valueOf(saved_char_color_list.get(0).toString()));
            jsonObject.put(DatabaseHandler.CHARACTER_URL, Constants.GET_CHARACTER_URL);
            jsonObject.put(DatabaseHandler.CHAR_DEFAULT_POS, Constants.CURRENT_CHAR_XPOS + "-" + Constants.CURRENT_CHAR_YPOS);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        boolean save_status = dataHandler.insertCharacter(jsonObject.toString(), img);


        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(DatabaseHandler.CHAR_DATA, jsonObject.toString());
        hashMap.put(DatabaseHandler.CHAR_IMG, img);
        hashMap.put(Constants.isSelectedKey, "false");


        Message m = new Message();
        m.what = POPULATE_LIST;
        m.obj = hashMap;
        mHandler.sendMessage(m);

        charList.add(hashMap);

        if (charList.size() > 0)
            ivTrash.setVisibility(View.VISIBLE);
        else
            ivTrash.setVisibility(View.GONE);


        Log.d("Insertchar", "" + jsonObject.toString());

        lv_characterSpecs.post(new Runnable() {
            @Override
            public void run() {
                lv_characterSpecs.smoothScrollToPosition(savedCharAdapter.getCount() - 1);
            }
        });


        counter_for_saved_character = 0;
        save_char = true;
        character_saving = true;

    }


    private void showAllSavedCharacters() {
        new AsyncTask<Void, String, ArrayList<HashMap<String, String>>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = ProgressDialog.show(MainActivity.this, "", "");
                progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progress.setContentView(R.layout.progress_dialog);
                progress.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (progress != null) {
                            if (progress.isShowing()) {
                                progress.dismiss();
                            }
                        }
                    }
                }, 13000);
            }

            @Override
            protected ArrayList<HashMap<String, String>> doInBackground(Void... params) {

                ArrayList<HashMap<String, String>> newCharList = new ArrayList<HashMap<String, String>>();
                newCharList = dataHandler.getAllSavedCharacters();
                return newCharList;

            }

            @Override
            protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
                super.onPostExecute(result);
                try {
                    Log.e("current_back", "" + result.toString());

                    charList.addAll(result);
                    Log.e("current_back12", "" + charList.size());

                    savedCharAdapter = new SavedCharacterAdapter(MainActivity.this, charList, listener);
                    lv_characterSpecs.setAdapter(savedCharAdapter);
                    if (progress != null) {
                        if (progress.isShowing())
                            progress.dismiss();
                    }
                    if (charList.size() > 0)
                        ivTrash.setVisibility(View.VISIBLE);
                    else
                        ivTrash.setVisibility(View.GONE);
                    helperForSave();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }.execute();


    }


    public void showTtsMenu() {
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        innerTtsView = vi.inflate(R.layout.tts_menu, null);
        Log.d("innerTtsView", "" + innerTtsView);
        isCharText = true;
        isSayUrl = false;
        isRecordEffet = false;
        playDefault = "showTtsMenu";
        et_typeMessage = (EditText) innerTtsView.findViewById(R.id.et_enterMessage);
        text_to_speak_by_character = et_typeMessage.getText().toString();
        Log.d("et_typeMessage", "av" + et_typeMessage.getText().toString());
        Log.d("et_typeMessage", "av" + text_to_speak_by_character);
        RelativeLayout languageContainer = (RelativeLayout) innerTtsView.findViewById(R.id.language_layout);
        RelativeLayout typeContainer = (RelativeLayout) innerTtsView.findViewById(R.id.type_layout);
        tv_language = (TextView) innerTtsView.findViewById(R.id.tv_language);
        language_to_speak_by_character = tv_language.getText().toString();
        tv_voiceType = (TextView) innerTtsView.findViewById(R.id.tv_voiceType);
        voicetype_to_speak_by_character = tv_voiceType.getText().toString();
        initializetts_menu = true;

        if (ioListner != null)
            ioListner.updateView(Constants.SELECTED_LANG, "language", Constants.blank_arraylist);

        if (ioListner != null)
            ioListner.updateView(Constants.SELECTED_LANG_CHAR, "voicetype", Constants.blank_arraylist);

        optionContainer.addView(innerTtsView);

        languageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typedMessage = et_typeMessage.getText().toString();

                showActionSheet("language", ioListner, Constants.blank_arraylist);
            }
        });

        typeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typedMessage = et_typeMessage.getText().toString();
                showActionSheet("voicetype", ioListner, Constants.blank_arraylist);
            }
        });
        performActionOnEnteringText();
    }

    public void showVoiceMenu() {
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        innerVoiceView = vi.inflate(R.layout.recorder_menu, null);
        isSayUrl = true;
        isRecordEffet = true;
        typedMessage = null;
        progressBar = (ProgressBar) innerVoiceView.findViewById(R.id.progressBar);
        recordButton = (ImageView) innerVoiceView.findViewById(R.id.im_recordBtn);
        tvCountDownTime = (TextView) innerVoiceView.findViewById(R.id.tvCountDownTime);
        tvRecordTitle = (TextView) innerVoiceView.findViewById(R.id.tv_record_title);

        final boolean[] isRecord = {false};
        optionContainer.addView(innerVoiceView);

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ViewUtill.haveNetworkConnection(MainActivity.this)) {
                    isRecordClicked = true;
                    isSayUrl = true;


                  /*  if (!marshPermission.CheckForRecorder()) {*/

                        requestAppPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},R.string.messagee,RECORD_PERMISSION);
                       /* newview = v;

                        if (!marshPermission.checkPermissionForExternalStorage() || !marshPermission.checkPermissionForREADExternalStorage()) {

                            marshPermission.requestPermissionForRecordWriteRead();
                        } else {
                            marshPermission.requestPermissionForRecord();
                        }*/
          /*          } else {
                        if (Mp3AudioRecordActivity.isRecordingg) {
                            Log.d("test", "else_recorder");
                            recordButton.setImageResource(R.mipmap.recbtn);
                            tvCountDownTime.setText("upto 60 sec long");
                            tvRecordTitle.setText("record your voice");
                            //  stopped = true;
                            //  progressBar.setProgress(0);
                            recordAudio(false);
                            //  im_playButton.setEnabled(false);


                        } else {
                            Log.d("test", "if_recorder");
                            recordButton.setImageResource(R.mipmap.stoprecordd);
                            tvRecordTitle.setText("recording...");
                            //    stopped = false;
                            //supposing u want to give maximum length of 60 seconds
                            //     progressBar.setMax(MAX_DURATION);
                            recordAudio(true);


                        }*/


                 /*       if (isRecording.equals("") || isRecording.equals(recStopped)) {
                            recordAudio(true);
                            recordButton.setImageResource(R.mipmap.stoprecordd);
                            tvRecordTitle.setText("recording...");
                            isRecording = inProgress;
                        } else {
                            recordAudio(false);
                            recordButton.setImageResource(R.mipmap.recbtn);
                            tvCountDownTime.setText("upto 60 sec long");
                            tvRecordTitle.setText("record your voice");
                            isRecording = recStopped;
                        }*/
                    //}
                } else {
                    InternetPopUp internetPopUp = new InternetPopUp(MainActivity.this, actualPos);
                    internetPopUp.show();
                    internetPopUp.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 70 / 100,
                            getResources().getDisplayMetrics().heightPixels * 50 / 100);
                }


            }
        });
    }

    protected void splashScreen() {
        try {
            final Dialog dialog_splash = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialog_splash.setContentView(R.layout.splash_screen);
            dialog_splash.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!MainActivity.this.isFinishing() && dialog_splash != null) {
                        if (dialog_splash.isShowing()) {
                            //dialog_splash.dismiss();
                            dialog_splash.hide();
                        }
                    }

                }
            }, 6000);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Methods used for recording ..............
     */


    private void startProgress() {

        mTimerTask = new TimerTask() {
            @Override
            public void run() {

                if (!stopped)  // call ui only when  the progress is not stopped
                {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try {

                                ++timeVal;
                                progressBar.setMax(MAX_DURATION);

                                progressBar.setProgress(timeVal);

                                Log.e("timeVal:", "" + timeVal);
                                Log.e("progress:", "" + progressBar.getProgress());
                                Log.e("getMax:", "" + progressBar.getMax());

                                tvCountDownTime.setText("" + timeVal + " secs");
                                if (timeVal == 60) {
                                    mTimerTask.cancel();
                                    progressBar.setProgress(0);
                                    tvCountDownTime.setText("upto 60 sec long");
                                    tvRecordTitle.setText("record your voice");
                                    timeVal = 0;
                                    // Mp3AudioRecordActivity mp3AudioRecordActivity=new Mp3AudioRecordActivity(MainActivity.this,false,uploadAudioListener);
                                    stopRecordingForMax();
                                }

                            } catch (Exception e) {
                                Log.e("insideCatchh", "" + e);
                            }
                        }
                    });
                } else {
                    Log.e("insideelse", "fsdfsdfds");
                    progressBar.setProgress(0);

                }
            }
        };
        timer.schedule(mTimerTask, 1, 1000);
    }


    /**
     * Recording audio to mp3 Format ....
     *
     * @param isRecord
     */
    public void recordAudio(boolean isRecord) {
        if (isRecord) {
            progressBar.setProgress(0);
            stopped = false;
            //  startRecording();


            // change UI elements here
            new Thread() {
                @Override
                public void run() {
                    Mp3AudioRecordActivity mp3AudioRecordActivity = new Mp3AudioRecordActivity(MainActivity.this, true, uploadAudioListener);
                    mp3AudioRecordActivity.startRecording();
                    //startProgress();
                }
            }.start();
            //supposing u want to give maximum length of 60 seconds
            progressBar.setMax(MAX_DURATION); //supposing u want to give maximum length of 60 seconds

            startProgress();
        } else {

            stopped = true;
            progressBar.setProgress(0);
            stopRecording();


        }

    }


    private void showActionSheet(String type, final IonUpdateView ioListner, ArrayList<FxMenuModel> arraylist) {
        newType = type;
        myDialog = new Dialog(MainActivity.this, R.style.CustomTheme);
        myDialog.setContentView(R.layout.actionsheet);
        myDialog.setCanceledOnTouchOutside(true);

        final LinearLayout parent = (LinearLayout) myDialog.findViewById(R.id.parent);
        lv_fxMenu = (ListView) myDialog.findViewById(R.id.lv_fxMenu);
        TextView tvCancel = (TextView) myDialog.findViewById(R.id.tvCancel);

        Log.e("newType", "newType " + type);


        if (newType.equalsIgnoreCase("fxs")) {

            if (ioListner != null)
                ioListner.updateView(Constants.SELECTED_FX, "fxs", arraylist);
        } else if (newType.equalsIgnoreCase("language")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    languageList = dataHandler.getAllSavedLanguages();
                    lv_fxMenu.setAdapter(new LanguageAdapter(MainActivity.this, languageList));
                }
            });

        } else if (newType.equalsIgnoreCase("voicetype")) {
            if (!Constants.SELECTED_LANG_ID.equals("") && Constants.SELECTED_LANG_ID != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        charlanguageList = dataHandler.getAllCharLanguages(Constants.SELECTED_LANG_ID);
                        lv_fxMenu.setAdapter(new CharLanguageAdapter(MainActivity.this, charlanguageList));
                    }
                });

            }
        }


        lv_fxMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (newType.equalsIgnoreCase("fxs")) {
                    Constants.SELECTED_FX = Constants.fxCategories.get(position).getFxName();
                    Constants.SELECTED_FX_ID = Constants.fxCategories.get(position).getParam();

                    Constants.CURRENT_SELECTED_FX = Constants.SELECTED_FX_ID;
                    if (ioListner != null)
                        ioListner.updateView(Constants.SELECTED_FX, "fxs", Constants.blank_arraylist);


                    if (Constants.SELECTED_FX.equals("Fx")) {
                        Constants.CURRENT_SELECTED_FX = "";

                        fx_type = "";
                        fx_level = "";
                    } else if (!Constants.CURRENT_SELECTED_FX.equals("")) {
                        if (Constants.CURRENT_SELECTED_FX.contains("-")) {
                            String[] strFx = Constants.CURRENT_SELECTED_FX.split("");

                            fx_type = strFx[1];
                            Log.e("fx_type", fx_type);
                            fx_level = strFx[2] + strFx[3];
                            Log.e("fx_level", fx_level);
                        } else {
                            String[] strFx = Constants.CURRENT_SELECTED_FX.split("");
                            fx_type = strFx[1];
                            Log.e("fx_type", fx_type);
                            fx_level = strFx[2];
                            Log.e("fx_level", fx_level);
                        }
                    } else if (Constants.CURRENT_SELECTED_FX.equals("")) {
                        fx_type = "";
                        fx_level = "";
                    }

                    typedMessage = et_typeMessage.getText().toString();

                    if (typedMessage.isEmpty()) {
                        typedMessage = "Enter message";
                    }


                    showLoaderPosition(1000, 700);

                    //  Glide.with(context).load(R.drawable.loader).into(loader);
                    //  loader.setVisibility(View.VISIBLE);
                    curFX = Constants.fxCategories.get(position).getParam();
                    Log.d("fx_typefx_level", "" + fx_type + fx_level);

                    if (isRecordEffet == true && getUrl != null) {
                        effectchar();
                    }
                  /*  else if(isCharText ==true && typedMessage==null){
                        playAudioFromText(Constants.ENGINE_ID, Constants.LANGUAGE_ID, Constants.VOICE_ID, fx_type, fx_level);
                    }*/
                    else if (isCharText == true && playDefault == "showTtsMenu") {
                        getDefaultVoiceType(Constants.SELECTED_LANG);
                    }

                } else if (newType.equalsIgnoreCase("language")) {

                    tv_language.setText(" ");
                    Constants.SELECTED_LANG = " ";

                    Constants.SELECTED_LANG_ID = languageList.get(position).getLanguageId();
                    Constants.SELECTED_LANG = languageList.get(position).getLanguageName();


                    if (ioListner != null)
                        ioListner.updateView(Constants.SELECTED_LANG, "language", Constants.blank_arraylist);

                    Constants.LANGUAGE_ID = Constants.SELECTED_LANG_ID;
                    if (typedMessage.isEmpty()) {
                        typedMessage = "Enter message";
                    }
                    getDefaultVoiceType(Constants.SELECTED_LANG);


                } else if (newType.equalsIgnoreCase("voicetype")) {

                    Constants.ENGINE_ID = charlanguageList.get(position).getEngineId();
                    Constants.SELECTED_LANG_CHAR = charlanguageList.get(position).getCharName();
                    Constants.VOICE_ID = charlanguageList.get(position).getVoiceId();
                    Constants.LANGUAGE_ID = charlanguageList.get(position).getLangId();


                    String l_id = charlanguageList.get(position).getLangId();
                    String e_id = charlanguageList.get(position).getEngineId();
                    String v_id = charlanguageList.get(position).getVoiceId();
                    String fx_type = null, fx_level = null;

                    if (Constants.SELECTED_FX.equals("Fx")) {
                        Constants.CURRENT_SELECTED_FX = "";

                        fx_type = "";
                        fx_level = "";
                    } else {
                        if (!Constants.CURRENT_SELECTED_FX.equals("")) {
                            if (Constants.CURRENT_SELECTED_FX.contains("-")) {
                                String[] strFx = Constants.CURRENT_SELECTED_FX.split("");

                                fx_type = strFx[1];
                                fx_level = strFx[2] + strFx[3];
                            } else {
                                String[] strFx = Constants.CURRENT_SELECTED_FX.split("");
                                fx_type = strFx[1];
                                fx_level = strFx[2];
                            }
                        }
                    }

                    if (ioListner != null)
                        ioListner.updateView(Constants.SELECTED_LANG_CHAR, "voicetype", Constants.blank_arraylist);


                    typedMessage = et_typeMessage.getText().toString();

                    if (typedMessage.equals("")) {
                        typedMessage = "Enter message";
                    }

                    if (typedMessage.isEmpty()) {
                        typedMessage = "Enter message";
                    }
                    showLoaderPosition(1000, 700);

                    playAudioFromText(e_id, l_id, v_id, fx_type, fx_level);

                }


                YoYo.with(Techniques.SlideOutDown)
                        .duration(1000)
                        .playOn(parent);
                myDialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.SlideOutDown)
                        .duration(1000)
                        .playOn(parent);
                myDialog.dismiss();
            }
        });

        myDialog.show();
        YoYo.with(Techniques.SlideInUp)
                .duration(1000)
                .playOn(parent);

        myDialog.getWindow().setGravity(Gravity.BOTTOM);

    }


    private void playAudioFromText(String e_id, String l_id, String v_id, String fx_type, String fx_level) {
        try {
            if (webView != null) {
                try {

                    Log.d("playaudiofromtetxt", " enter");
                    webView.setWebChromeClient(new testWebChromeClientForTextAudio(e_id, l_id, v_id, fx_type, fx_level));
                    final StringBuilder buffer = new StringBuilder("javascript:alert(testFunction('");
                    buffer.append("'));");
                    webView.loadUrl(buffer.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadDataToServer(final String url) {


        Constants.execute(new Super_AsyncTask(this, url, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                try {

                    Log.d("outputresponse", "" + output);
                    what_to_share_now = "SHARE_TEXT_AUDIO";
                    JSONObject jsonObject = new JSONObject(output);
                    String error = jsonObject.optString("ERRORSTR");
                    if (error.isEmpty()) {
                        url_to_be_played = jsonObject.optString("URL");
                        saychartext(url_to_be_played);
                    } else {
                        Toast.makeText(MainActivity.this, "Internet is too slow.Please try again", Toast.LENGTH_LONG);
                    }

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }

            }
        }, true, false));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("orientation changed", "orientation changed");


    }

    private class TransparentProgressDialog extends Dialog {

        private ImageView iv;

        public TransparentProgressDialog(Context context, int resourceIdOfImage) {
            super(context, R.style.TransparentProgressDialog);
            WindowManager.LayoutParams wlmp = getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER_HORIZONTAL;
            getWindow().setAttributes(wlmp);
            setTitle(null);
            setCancelable(false);
            setOnCancelListener(null);
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
            iv = new ImageView(context);
            iv.setImageResource(resourceIdOfImage);
            layout.addView(iv, params);
            addContentView(layout, params);
        }

        @Override
        public void show() {
            super.show();
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.INFINITE);
            anim.setDuration(3000);
            iv.setAnimation(anim);
            iv.startAnimation(anim);
        }
    }

    /**
     * Method for uploading audio to server .....
     */

    public void uploadBackground() {

        Log.e("inaudio", "gfdgf " + Constants.SESSION_ID);


        Log.e("inaudio", "gfdgf " + str_session_key);
        httpclient = new DefaultHttpClient();
        String url = "http://vhss.oddcast.com/vhss_editors/upload.php?";
        String params = "type=bg&name=Koala&PHPSESSID=" + str_session_key;
        httppost = new HttpPost(url + params);
        Log.d("urlparams", "" + url + params);
        entity = new AndroidMultiPartEntity(this);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = ProgressDialog.show(MainActivity.this, "", "");
                progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progress.setContentView(R.layout.progress_dialog);
                progress_text = (TextView) progress.findViewById(R.id.progress_text);
                progress.show();
                if (uploading_background)
                    progress_text.setText("5%");

            }

            @Override
            protected String doInBackground(Void... params) {
                Log.d("galrypicturepath", "" + picturePath);
                if (uploading_background)
                    progress_text.setText("7%");

                File convertedImage = null;
                if (!picturePath.contains("jpg")) {
                    Bitmap bmp = BitmapFactory.decodeFile(picturePath);
                    convertedImage = new File(Environment.getExternalStorageDirectory() + "/convertedd" + System.currentTimeMillis() + ".jpg");
                    FileOutputStream outStream = null;
                    try {
                        outStream = new FileOutputStream(convertedImage);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }


                    try {
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                        outStream.flush();
                        outStream.close();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    File base_file = new File(picturePath);
                    convertedImage = base_file;
                }


                try {

                    Bitmap bmp = BitmapFactory.decodeFile(picturePath);
                    bmp.createScaledBitmap(bmp, 736, 414, true);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                    InputStream in = new ByteArrayInputStream(bos.toByteArray());
                    entity.addPart("Filedata", new InputStreamBody(in, "koala.jpg"));
                    entity.addPart("mimeType", new StringBody("image/jpeg"));
                    httppost.setEntity(entity);
                    Log.e("Response_httppost: ", "" + httppost);
                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();
                    Log.e("r_entity: ", "" + r_entity);
                    int statusCode = response.getStatusLine().getStatusCode();
                    Log.e("status_Code", "" + statusCode);
                    if (statusCode == 200) {
                        // Server response
                        responseString = EntityUtils.toString(r_entity);
                        Log.e("responseString: ", "" + responseString);
                    } else {
                        responseString = "Error occurred! Http Status Code: " + statusCode;
                    }

                } catch (ClientProtocolException e) {
                    responseString = e.toString();
                    Log.e("clientresponseString", "" + responseString);
                } catch (IOException e) {
                    responseString = e.toString();
                    Log.e("ioresponseString", "" + responseString);
                }

                return responseString;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
                try {
                    if (uploading_background)
                        progress_text.setText("25%");

                    Log.d("backround_reslt", "post" + aVoid);
                    Document doc = getDomElement(aVoid);
                    NodeList typesList = doc.getElementsByTagName("UPLOAD");
                    for (int i = 0; i < typesList.getLength(); i++) {
                        Node node = typesList.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("RES").equals("OK")) {
                            backgound_id = elem.getAttribute("ID");
                            Log.d("background_id", "" + backgound_id);
                            fetChApiLnk(1);
                        } else {
                            Toast.makeText(MainActivity.this, "Invalid Image Size", Toast.LENGTH_SHORT).show();
                            if (progress != null) {
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            }
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Something went wrong.Make sure you are connected to Internet", Toast.LENGTH_SHORT).show();
                    if (progress != null) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    }
                }


            }
        }.execute();

    }

    /*
     Changes for mp3 Funcationality*/
    public void uploadAduio() {
        try {
            Log.e("inaudio", "gfdgf " + Constants.SESSION_ID);
            httpclient = new DefaultHttpClient();
            String url = "http://vhss.oddcast.com/vhss_editors/upload.php?";
            String params = url + "type=audio&name=testrecord.mp3&PHPSESSID=" + str_session_key;
            httppost = new HttpPost(params);
            entity = new AndroidMultiPartEntity(this);

        } catch (Exception e) {
            e.printStackTrace();
        }


        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                try {
                    progress = ProgressDialog.show(MainActivity.this, "", "");
                    progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    progress.setContentView(R.layout.progress_dialog);
                    progress.show();
                    im_playButton.setEnabled(false);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                    super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                //  jump();
                sourceFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/voki_audio.mp3");
                try {

                    Log.e("httppost: ", "" + httppost);
                    // Extra parameters if you want to pass to server
                    sb = new StringBuilder();

                    entity.addPart("name", new StringBody("testrecord.mp3"));
                    entity.addPart("Filedata", new FileBody(sourceFile));
                    Log.e("Filedata: ", "" + sourceFile);
                    entity.addPart("mimeType", new StringBody("audio/mp3"));

                    httppost.setEntity(entity);
                    Log.e("Response_httppost: ", "" + httppost);
                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();
                    Log.d("response", "" + response);

                    Log.d("response length", "" + response.getEntity().getContentLength());
                    //  Log.e("r_entity: ", "" + r_entity);
                    int statusCode = response.getStatusLine().getStatusCode();
                    Log.e("status_Code", "" + statusCode);
                    if (statusCode == 200) {
                        // Server response

                        InputStream inpst = r_entity.getContent();
                        try {
                            BufferedReader rd = new BufferedReader(new InputStreamReader(inpst));
                            String line;
                            while ((line = rd.readLine()) != null) {

                                sb.append(line);

                            }

                            // rd.close();
                            System.out.println("finalResult " + sb.toString());


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {

                        responseString = "Error occurred! Http Status Code: " + statusCode;
                    }

                } catch (ClientProtocolException e) {
                    responseString = e.toString();
                    Log.e("clientresponseString", "" + responseString);
                } catch (IOException e) {
                    responseString = e.toString();
                    Log.e("ioresponseString", "" + responseString);
                }
                return sb.toString();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try {
                    if (progress != null) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    }

                    Log.d("crash_detection", "" + s);
                    Document doc = getDomElement(s);
                    NodeList typesList = doc.getElementsByTagName("UPLOAD");
                    for (int i = 0; i < typesList.getLength(); i++) {
                        Node node = typesList.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("RES").equals("OK")) {
                            what_to_share_now = "SHARE_RECORDING";
                            String getUrl = elem.getAttribute("URL");
                            Log.d("getUrl", "" + getUrl);
                            getAudioId = elem.getAttribute("ID");
                            Log.d("getAudioId", "" + getAudioId);
                            sayurl(getUrl);


                            //      loader.setVisibility(View.INVISIBLE);
                            im_playButton.setEnabled(true);
                        }


                    }
                    NodeList typesList1 = doc.getElementsByTagName("ERROR");
                    for (int i = 0; i < typesList1.getLength(); i++) {
                        Node node = typesList1.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("STR").contains("APS")) {

                            if (progress != null) {
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            }
                            Toast.makeText(context, "Server Error, Please Try Again", Toast.LENGTH_LONG).show();
                            //     loader.setVisibility(View.INVISIBLE);
                            im_playButton.setEnabled(true);
                            recordButton.setEnabled(true);
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Something went wrong.Make sure you are connected to Internet1", Toast.LENGTH_SHORT).show();
                    //    loader.setVisibility(View.INVISIBLE);
                    if (progress != null) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    }
                }
            }
        }.execute();


    }

    public void getDefaultVoiceType(String language) {
        String voice = null;
        if (language.equalsIgnoreCase("English")) {
            voice = "Julie";
        } else if (language.equalsIgnoreCase("Spanish")) {
            voice = "Carlos";
        } else if (language.equalsIgnoreCase("German")) {
            voice = "Katrin";
        } else if (language.equalsIgnoreCase("French")) {
            voice = "Bernard";
        } else if (language.equalsIgnoreCase("Catalan")) {
            voice = "Empar (Valencian)";
        } else if (language.equalsIgnoreCase("Portuguese")) {
            voice = "Amalia (European)";
        } else if (language.equalsIgnoreCase("Italian")) {
            voice = "Federica";
        } else if (language.equalsIgnoreCase("Greek")) {
            voice = "Afroditi";
        } else if (language.equalsIgnoreCase("Swedish")) {
            voice = "Annika";
        } else if (language.equalsIgnoreCase("Chinese")) {
            voice = "Hui (Mandarin)";
        } else if (language.equalsIgnoreCase("Dutch")) {
            voice = "Saskia";
        } else if (language.equalsIgnoreCase("Japanese")) {
            voice = "Misaki";
        } else if (language.equalsIgnoreCase("Korean")) {
            voice = "Junwoo";
        } else if (language.equalsIgnoreCase("Polish")) {
            voice = "Krzysztof";
        } else if (language.equalsIgnoreCase("Galician")) {
            voice = "Carmela";
        } else if (language.equalsIgnoreCase("Turkish")) {
            voice = "Kerem";
        } else if (language.equalsIgnoreCase("Danish")) {
            voice = "Frida";
        } else if (language.equalsIgnoreCase("Norwegian")) {
            voice = "Henrik";
        } else if (language.equalsIgnoreCase("Russian")) {
            voice = "Dmitri";
        } else if (language.equalsIgnoreCase("Finnish")) {
            voice = "Marko";
        } else if (language.equalsIgnoreCase("Arabic")) {
            voice = "Laila";
        } else if (language.equalsIgnoreCase("Romanian")) {
            voice = "Ioana";
        } else if (language.equalsIgnoreCase("Esperanto")) {
            voice = "Ludoviko";
        }
        String[] strArr = {"1:Alan (Australian):9:2", "1:Allison (US):7:2", "1:Catherine (UK):6:2", "1:Dave (US):2:2", "1:Elizabeth (UK):4:2",
                "1:Grace (Australian):10:2", "1:Simon (UK):5:2", "1:Steven (US):8:2", "1:Susan (US):1:2", "1:Veena (Indian):11:2", "2:Carmen (Castilian):1:2",
                "2:Juan (Castilian):2:2", "2:Francisca (Chilean):3:2", "2:Diego (Argentine):4:2", "2:Esperanza (Mexican):5:2", "2:Jorge (Castilian):6:2",
                "2:Carlos (American):7:2", "2:Soledad (American):8:2", "2:Leonor (Castilian):9:2", "2:Ximena:10:2", "3:Stefan:2:2", "3:Katrin:3:2",
                "4:Bernard:2:2", "4:Jolie:3:2", "4:Florence:4:2", "4:Charlotte:5:2", "4:Olivier:6:2", "5:Montserrat:1:2", "5:Jordi:2:2", "5:Empar (Valencian):3:2",
                "6:Gabriela (Brazilian):1:2", "6:Amalia (European):2:2", "6:Eusebio (European):3:2", "6:Fernanda (Brazilian):4:2", "6:Felipe (Brazilian):5:2",
                "7:Paola:1:2", "7:Silvana:2:2", "7:Valentina:3:2", "7:Luca:5:2", "7:Marcello:6:2", "7:Roberto:7:2", "7:Matteo:8:2", "7:Giulia:9:2", "7:Federica:10:2",
                "8:Afroditi:1:2", "8:Nikos:3:2", "9:Annika:1:2", "9:Sven:2:2", "10:Linlin (Mandarin):1:2", "10:Lisheng (Mandarin):2:2", "11:Willem:1:2",
                "11:Saskia:2:2", "14:Zosia:1:2", "14:Krzysztof:2:2", "15:Carmela:1:2", "16:Kerem:1:2", "16:Zeynep:2:2", "16:Selin:3:2", "19:Frida:1:2",
                "19:Magnus:2:2", "20:Vilde:1:2", "20:Henrik:2:2", "21:Olga:1:2", "21:Dmitri:2:2", "23:Milla:1:2", "23:Marko:2:2", "27:Tarik:1:2", "27:Laila:2:2",
                "30:Ioana:1:2", "31:Ludoviko:1:2", "1:Kate (US):1:3", "1:Paul (US):2:3", "1:Julie (US):3:3", "1:Bridget (UK):4:3", "1:Hugh (UK):5:3", "1:Ashley (US):6:3",
                "1:James (US):7:3", "2:Violeta:1:3", "2:Francisco (Mexican):2:3", "4:Chloe (Canadian):1:3", "10:Lily (Mandarin):1:3", "10:Hui (Mandarin):3:3",
                "10:Liang (Mandarin):4:3", "12:Show:2:3", "12:Misaki:3:3", "13:Yumi:1:3", "13:Junwoo:2:3"};

        if (!voice.isEmpty()) {
            for (int k = 0; k < strArr.length; k++) {

                if (strArr[k].contains(voice)) {
                    String[] arrStr = strArr[k].split(":");
                    String lang_id = arrStr[0];
                    String char_name = arrStr[1];
                    String voice_id = arrStr[2];
                    String engine_id = arrStr[3];


                    default_voice_id = arrStr[2];

                    playAudioFromText(engine_id, lang_id, default_voice_id, fx_type, fx_level);
                }

            }


        }
        Constants.SELECTED_LANG_CHAR = voice;
        if (ioListner != null) {
            ioListner.updateView(voice, "voicetype", Constants.blank_arraylist);
        }
        Log.d("default_voice_id", "" + default_voice_id);

    }

    private void getSaveCharDimensions() {

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        screenHeightDp = metrics.heightPixels / metrics.density;
        screenWidthDp = metrics.widthPixels / metrics.density;

        float newXwidth = getResources().getDimension(R.dimen.option_1_width) / metrics.density;
        float xWidth = getResources().getDimension(R.dimen.option_2_width) / metrics.density;

        float finalxWidth = newXwidth + xWidth;

        Log.i("getDimensionsPixels", "laydimen  = " + newXwidth + "  " + xWidth + "   " + screenWidthDp + "  " + screenHeightDp + "  " + finalxWidth);

        screenWidthDp = screenWidthDp - finalxWidth;

        screenWidthDp = screenWidthDp + 50;

        Log.i("getDimensionsPixels", "laydimen1  = " + screenWidthDp);

    }

    private void getDimensions() {

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;
        Log.d("dimensions>>>", "screenHeight" + screenHeight + "screenWidth" + screenWidth);

        int densityDpi = metrics.densityDpi;
        st_pos_width = screenWidth / 2;
        st_pos_height = screenHeight / 2;


    }


    public static void showLoaderPosition(int x, int y) {
        StringBuilder buffersss = new StringBuilder("javascript:updateLoaderPosition('");
        buffersss.append(x);
        buffersss.append("','");
        buffersss.append(y);
        buffersss.append("');");

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript(buffersss.toString(), null);
            } else {
                webView.loadUrl(buffersss.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void charMenuAnimation() {
        if (menuContainer2.getVisibility() == View.VISIBLE) {
            settingPosition(margin_small, st_pos_width);
            im_Option1.setImageResource(R.drawable.btn_head2_unselect);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer2);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(hideShowMenu2);

            Log.d("first_cat", "" + first_cat);


            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_INVISIBLE);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    menuContainer2.setVisibility(View.GONE);
                    hideShowMenu2.setVisibility(View.GONE);
                }
            }, 400);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    for (int i = 0; i < charactersCatList.size(); i++) {
                        charactersCatList.get(i).setSelection("false");
                    }
                    Log.e("Inside_Click", "" + charactersCategory);
                    listAdapter.notifyDataSetChanged();

                }
            });

            // I comment it
            imDice.setVisibility(View.GONE);
        } else {
            settingPosition(margin_xlarge, st_pos_width);
            im_Option1.setImageResource(R.drawable.btn_head2_select);
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer2);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(hideShowMenu2);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);
            menuContainer2.setVisibility(View.VISIBLE);
            hideShowMenu2.setVisibility(View.VISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < charactersCatList.size(); i++) {
                        if (Constants.TEMP_SAVED_CATEGORY.equals(Constants.NEW_CHARACTER_CATEGORY)) {
                            if (charactersCatList.get(i).getFileName().equals(Constants.TEMP_SAVED_CATEGORY)) {
                                charactersCatList.get(i).setSelection("true");
                            } else {
                                charactersCatList.get(i).setSelection("false");
                            }
                        } else {
                            if (charactersCatList.get(i).getFileName().equals(Constants.TEMP_SAVED_CATEGORY)) {

                                charactersCatList.get(i).setSelection("true");
                            } else {
                                charactersCatList.get(i).setSelection("false");
                            }
                        }
                    }

                    listAdapter.notifyDataSetChanged();
                }
            });

            PREVIOUSClicked = FIRSTClicked;
        }
    }


    private void accMenuAnimation() {

        if (accMenu.getVisibility() == View.VISIBLE) {
            settingPosition(margin_small, st_pos_width);
            im_Option2.setImageResource(R.drawable.btn_accessories_unselect);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(accMenu);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(im_accHideBtn);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_INVISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    accMenu.setVisibility(View.GONE);
                    im_accHideBtn.setVisibility(View.GONE);
                }
            }, 400);

            Log.e("Inside_Wear_Click9", "");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < accessoriesList.size(); i++) {
                        accessoriesList.get(i).setSelection("false");
                    }

                    accessoryAdapter.notifyDataSetChanged();
                }
            });

        } else {
            settingPosition(margin_xlarge, st_pos_width);
            im_Option2.setImageResource(R.drawable.btn_accessories_select);
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(accMenu);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(im_accHideBtn);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);
            accMenu.setVisibility(View.VISIBLE);
            im_accHideBtn.setVisibility(View.VISIBLE);
            PREVIOUSClicked = SECONDClicked;
            Log.e("Inside_Wear_Click10", "");
        }
    }

    private void wallpaperMenuAnimation() {

        if (wallMenu.getVisibility() == View.VISIBLE) {
            settingPosition(margin_small, st_pos_width);
            im_Option3.setImageResource(R.drawable.btn_background_unselect);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(wallMenu);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(im_wallHideBtn);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_INVISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    wallMenu.setVisibility(View.GONE);
                    im_wallHideBtn.setVisibility(View.GONE);
                }
            }, 400);


        } else {
            settingPosition(margin_xlarge, st_pos_width);
            im_Option3.setImageResource(R.drawable.btn_background_select);
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(wallMenu);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(im_wallHideBtn);
            wallMenu.setVisibility(View.VISIBLE);
            im_wallHideBtn.setVisibility(View.VISIBLE);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);


            PREVIOUSClicked = THIRDClicked;
        }
    }


    private void categoryMenuAnimation() {
        if (menuContainer.getVisibility() == View.VISIBLE) {

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_INVISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    menuContainer.setVisibility(View.GONE);
                    hideShowMenu.setVisibility(View.VISIBLE);
                }
            }, 400);


        } else {


            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_VISIBLE);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    menuContainer.setVisibility(View.VISIBLE);
                    hideShowMenu.setVisibility(View.GONE);
                }
            }, 200);


        }
    }

    private void colorMenuAnimation() {
        if (colorPickerContainer.getVisibility() == View.VISIBLE) {
            settingPosition(zero, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.COLORPICKER_CONTAINER_INVISIBLE);
            im_Option4.setImageResource(R.drawable.btn_color_unselect);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(colorPickerContainer);
            ivTrash.setVisibility(View.GONE);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(hideShowMenu2);
            colorPickerContainer.setVisibility(View.GONE);
            hideShowMenu2.setVisibility(View.GONE);

        } else {
            MainActivity.is_there_any_change_in_character = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getValidateList();
                }
            });
            settingPosition(medium_grand_size, st_pos_width);
            im_Option4.setImageResource(R.drawable.btn_color_select);
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(colorPickerContainer);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(hideShowMenu2);
            colorPickerContainer.setVisibility(View.VISIBLE);
            hideShowMenu2.setVisibility(View.VISIBLE);
            colorPickerContainer.post(new Runnable() {
                @Override
                public void run() {
                    updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.COLORPICKER_CONTAINER_VISIBLE);


                }
            });

            ivTrash.setVisibility(View.GONE);
            Log.e("insidePosChnage", "sjdfnk");

        }
    }

    private void voiceMenuAnimation() {
        if (voiceMenu.getVisibility() == View.VISIBLE) {
            settingPosition(zero, st_pos_width);
            im_Option5.setImageResource(R.drawable.btn_voice_unselect);
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(voiceMenu);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(im_voiceHideBtn);


            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.COLORPICKER_CONTAINER_INVISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    voiceMenu.setVisibility(View.GONE);
                    im_voiceHideBtn.setVisibility(View.GONE);
                }
            }, 400);


        } else {
            MainActivity.is_there_any_change_in_character = true;
            settingPosition(medium_grand_size, st_pos_width);
            im_Option5.setImageResource(R.drawable.btn_voice_select);
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(voiceMenu);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(im_voiceHideBtn);

            voiceMenu.setVisibility(View.VISIBLE);
            im_voiceHideBtn.setVisibility(View.VISIBLE);

            voiceMenu.post(new Runnable() {
                @Override
                public void run() {
                    // getwidtth();
                    updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.COLORPICKER_CONTAINER_VISIBLE);
                    float voiceMenuwidth = voiceMenu.getWidth();
                    Log.d("allparams", "1  " + st_pos_width + "2  " + voiceMenuwidth);
                    float fourth_opt = st_pos_width - (st_pos_width / 10) + voiceMenuwidth / 2;
                    Log.d("allparams", "1  " + st_pos_width + "2  " + voiceMenuwidth + "3  " + fourth_opt);

                }
            });
            PREVIOUSClicked = FIFTHClicked;

        }
    }

    private void shareMenuAnimation() {
        if (shareMenu.getVisibility() == View.VISIBLE) {

            settingPosition(zero, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_INVISIBLE);
            shareMenu.startAnimation(exit_to_right);
            im_shareHideBtn.startAnimation(exit_to_right);
            //  settingPosition(st_pos_width - 20);

            exit_to_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    shareMenu.setVisibility(View.GONE);
                    im_shareHideBtn.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


        } else {

            settingPosition(-grand_size, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_VISIBLE);
            shareMenu.startAnimation(enter_from_right);
            shareMenu.setVisibility(View.VISIBLE);

            //Do something after 100ms

            enter_from_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {


                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    im_shareHideBtn.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            shareMenu.post(new Runnable() {
                @Override
                public void run() {
                    float shareMenuwidth = shareMenu.getWidth();
                    float share_opt = st_pos_width - shareMenuwidth / 2;


                }
            });
        }
    }

    private void initFacebookSdk() {
        FacebookSdk.sdkInitialize(context.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

    }

    private void drawerMenuAnimation() {
        if (rightDrawerLayout.getVisibility() == View.VISIBLE) {
            settingPosition(zero, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_INVISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (rightPosition > -1) {
                        menuItems.get(rightPosition).put("is_selection", "false");
                    }
                    rightMenuAdapter.notifyDataSetChanged();
                }
            });

            rightDrawerLayout.startAnimation(exit_to_right);
            im_hideDrawer.startAnimation(exit_to_right);


            exit_to_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rightDrawerLayout.setVisibility(View.GONE);
                    im_hideDrawer.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


        } else {
            settingPosition(-grand_size, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.RIGHT_CONTAINER_VISIBLE);
            rightDrawerLayout.startAnimation(enter_from_right1);

            rightDrawerLayout.setVisibility(View.VISIBLE);

            enter_from_right1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {


                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    im_hideDrawer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < menuItems.size(); i++) {
                        menuItems.get(i).put("is_selection", "false");
                    }
                    rightMenuAdapter.notifyDataSetChanged();
                }
            });

        }

    }

    private void show_hideAnimations() {
        if (menuContainer2.getVisibility() == View.VISIBLE) {
            charMenuAnimation();
        }

        if (accMenu.getVisibility() == View.VISIBLE) {
            accMenuAnimation();
        }

        if (wallMenu.getVisibility() == View.VISIBLE) {
            wallpaperMenuAnimation();
        }

        if (colorPickerContainer.getVisibility() == View.VISIBLE) {
            colorMenuAnimation();
        }

        if (voiceMenu.getVisibility() == View.VISIBLE) {
            voiceMenuAnimation();
        }


        if (shareMenu.getVisibility() == View.VISIBLE) {
            shareMenuAnimation();
        }

        if (menuContainer3.getVisibility() == View.VISIBLE) {
            menuContainer3Animation();
        }
    }


    private static void selectedWearMenuAnimation() {

        if (selectedAccMenu.getVisibility() == View.VISIBLE) {
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(selectedAccMenu);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(im_selectAccHideBtn);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_INVISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    selectedAccMenu.setVisibility(View.GONE);
                    im_selectAccHideBtn.setVisibility(View.GONE);
                    im_accHideBtn.setVisibility(View.VISIBLE);
                }
            }, 400);


        } else {
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(selectedAccMenu);

            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(im_selectAccHideBtn);

            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    selectedAccMenu.setVisibility(View.VISIBLE);
                    im_selectAccHideBtn.setVisibility(View.VISIBLE);
                    im_accHideBtn.setVisibility(View.GONE);
                }
            }, 400);


        }

    }

    private void menuContainer3Animation() {
        if (menuContainer3.getVisibility() == View.VISIBLE) {
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer3);

            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(hideShowMenu3);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            menuContainer3.setVisibility(View.GONE);
                            hideShowMenu3.setVisibility(View.GONE);
                            hideShowMenu2.setVisibility(View.VISIBLE);
                        }
                    }, 1000);
                }
            });


        }
    }


    private void firstMenuAnimation() {

        if (menuContainer.getVisibility() == View.VISIBLE) {
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(1000)
                    .playOn(menuContainer);
            settingPosition(zero, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_INVISIBLE);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms

                    menuContainer.setVisibility(View.GONE);
                    hideShowMenu.setVisibility(View.VISIBLE);
                }
            }, 400);


        } else {
            YoYo.with(Techniques.SlideInLeft)
                    .duration(1000)
                    .playOn(menuContainer);
            settingPosition(margin_xsmall, st_pos_width);
            updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER_VISIBLE);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    menuContainer.setVisibility(View.VISIBLE);
                    hideShowMenu.setVisibility(View.GONE);
                }
            }, 400);


        }

    }

    public void getYposition() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (webView != null) {
                        float new_xpos = xPos + 30;
                        webView.setWebChromeClient(new MyWebChromeClient());
                        StringBuilder buffer = new StringBuilder("javascript:getYPos()");
                        webView.loadUrl(buffer.toString());
                        webView.clearCache(true);
                        webView.clearFormData();
                        webView.clearHistory();
                        webView.clearMatches();
                        Log.d("str_buffer_reslt", "" + buffer.toString());
                        Log.d("str_buffer_resltyPos", "" + yPos);
                        Log.d("str_buffer_resltxPos", "" + xPos);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });


    }

    public void copyFileFromAssets(String subpath, String pathh) throws Exception {
        InputStream in = null;
        OutputStream fout = null;
        int count = 0;

        try {
            Log.d("subpath", "" + subpath);
            Log.d("pathhhh", "" + pathh);
            temp_file_name_kitkat = "testwave121.mp3";
            in = getAssets().open("defaultAudios" + subpath + pathh);
            fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + temp_file_name_kitkat));

            byte data[] = new byte[1024];
            while ((count = in.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void settingPosition(float dp, float midpoint) {
        float pxl = dpToPx(dp);
        Log.e("settingpixel", ">>1 " + pxl);
        pxl = pxl + midpoint;
        pxl = pxl - minusno;
        Log.e("settingpixel", ">>3 " + pxl);
        YoYo.with(Techniques.FadeIn).duration(1000).playOn(im_playButton);
        im_playButton.setX(pxl);
        //loader.setX(pxl);


    }

    private float dpToPx(float dp) {
        return (float) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public void kitkatDefaultAudio(final String subpath, final String path) {

        Log.e("inaudio", "gfdgf " + Constants.SESSION_ID);


        httpclient = new DefaultHttpClient();
        String url = "http://vhss.oddcast.com/vhss_editors/upload.php?";
        String params = "type=audio&name=testrecord.mp3&PHPSESSID=" + Constants.SESSION_ID;
        httppost = new HttpPost(url + params);

        entity = new AndroidMultiPartEntity(this);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {

                try {
                    copyFileFromAssets(subpath, path);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                im_playButton.setEnabled(false);
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {

                sourceFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + temp_file_name_kitkat);
                try {

                    Log.e("httppost: ", "" + httppost);
                    // Extra parameters if you want to pass to server
                    sb = new StringBuilder();

                    entity.addPart("name", new StringBody("testrecord.mp3"));
                    entity.addPart("Filedata", new FileBody(sourceFile));
                    Log.e("Filedata: ", "" + sourceFile);
                    entity.addPart("mimeType", new StringBody("audio/mp3"));

                    httppost.setEntity(entity);
                    Log.e("Response_httppost: ", "" + httppost);
                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();
                    Log.d("response", "" + response);

                    Log.d("response length", "" + response.getEntity().getContentLength());
                    //  Log.e("r_entity: ", "" + r_entity);
                    int statusCode = response.getStatusLine().getStatusCode();
                    Log.e("status_Code", "" + statusCode);
                    if (statusCode == 200) {
                        // Server response

                        InputStream inpst = r_entity.getContent();
                        try {
                            BufferedReader rd = new BufferedReader(new InputStreamReader(inpst));
                            String line;
                            while ((line = rd.readLine()) != null) {

                                sb.append(line);

                            }


                            System.out.println("finalResult " + sb.toString());


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


              /*          responseString = EntityUtils.toString(r_entity);
                        Log.e("responseString: ", "" + responseString);*/

                    } else {

                        responseString = "Error occurred! Http Status Code: " + statusCode;
                    }

                } catch (ClientProtocolException e) {
                    responseString = e.toString();
                    Log.e("clientresponseString", "" + responseString);
                } catch (IOException e) {
                    responseString = e.toString();
                    Log.e("ioresponseString", "" + responseString);
                }

                return sb.toString();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    if (s != null) {
                        Log.d("docresponse", "" + s);
                        Document doc = getDomElement(s);
                        NodeList typesList = doc.getElementsByTagName("UPLOAD");
                        for (int i = 0; i < typesList.getLength(); i++) {
                            Node node = typesList.item(i);

                            Element elem = (Element) node;
                            if (elem.getAttribute("RES").equals("OK")) {
                                what_to_share_now = "SHARE_RECORDING";
                                getUrl = elem.getAttribute("URL");
                                Log.d("getUrl", "" + getUrl);
                                getAudioId = elem.getAttribute("ID");
                                Log.d("getAudioId", "" + getAudioId);
                                sayurlforkitkat(getUrl);
                                getUrl = null;
                                //  loader.setVisibility(View.INVISIBLE);
                                im_playButton.setEnabled(true);
                            }
                        }
                        NodeList typesList1 = doc.getElementsByTagName("ERROR");
                        for (int i = 0; i < typesList1.getLength(); i++) {
                            Node node = typesList1.item(i);

                            Element elem = (Element) node;
                            if (elem.getAttribute("STR").contains("APS")) {

                                Toast.makeText(context, "Server Error, Please Try Again", Toast.LENGTH_LONG).show();
                                //  loader.setVisibility(View.INVISIBLE);
                                im_playButton.setEnabled(true);

                            }


                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        //  loader.setVisibility(View.INVISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                    //  loader.setVisibility(View.INVISIBLE);
                }
            }
        }.execute();


    }

    public void playdefaultaudioforkitkat() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String[] list;


                if (Constants.CURRENT_CHAR_GENDER.equals("1")) {
                    try {
                        list = getAssets().list("defaultAudios/audiosFemale");
                        if (list.length > 0) {
                            for (int i = 0; i < list.length; i++) {

                                String file = list[i];
                                PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                playbackModel.setFileName(file);
                                playbackModel.setFolderName("audiosFemale");
                                Log.e("audioFile", "" + file);

                                femaleAudioList.add(playbackModel);

                            }

                            Random rand = new Random();
                            femalePos = rand.nextInt(femaleAudioList.size());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (Constants.CURRENT_CHAR_GENDER.equals("2")) {
                    try {
                        list = getAssets().list("defaultAudios/audiosMale");
                        if (list.length > 0) {
                            for (int i = 0; i < list.length; i++) {
                                String file = list[i];
                                PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                playbackModel.setFileName(file);
                                playbackModel.setFolderName("audiosMale");
                                Log.e("audioFile", "" + file);

                                maleAudioList.add(playbackModel);
                            }

                            Random rand = new Random();
                            malePos = rand.nextInt(maleAudioList.size());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        list = getAssets().list("defaultAudios/audiosOther");
                        if (list.length > 0) {
                            for (int i = 0; i < list.length; i++) {
                                String file = list[i];
                                PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                playbackModel.setFileName(file);
                                playbackModel.setFolderName("audiosOther");
                                Log.e("audioFile", "" + file);

                                othersAudioList.add(playbackModel);
                            }
                            Random rand = new Random();
                            otherPos = rand.nextInt(othersAudioList.size());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


        });
        String fileName = "";
        String path = "";
        String subPath = "";
        String assestPath = "";
        if (Constants.CURRENT_CHAR_GENDER.equals("1")) {
            subPath = "/audiosFemale/";
            fileName = femaleAudioList.get(femalePos).getFileName();
            Log.e("filename", fileName);
            try {
                kitkatDefaultAudio(subPath, fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (Constants.CURRENT_CHAR_GENDER.equals("2")) {
            subPath = "/audiosMale/";
            fileName = maleAudioList.get(malePos).getFileName();
            try {
                kitkatDefaultAudio(subPath, fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            subPath = "/audiosOther/";
            fileName = othersAudioList.get(otherPos).getFileName();
            try {
                kitkatDefaultAudio(subPath, fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    public void playdefaultaudio() {
        try {
            if (webView != null) {
                webView.setWebChromeClient(new testWebChromeClient());
                final StringBuilder buffer = new StringBuilder("javascript:alert(testFunction('");
                buffer.append("'));");
                webView.loadUrl(buffer.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void fetChApiLnk(int pos) {
        try {
            Log.d("fetchapi", "enter");
            webView.setWebChromeClient(new ApiWebChromeClient(pos));
            StringBuilder buffer = new StringBuilder("javascript:alert(fetchAPILink('");
            buffer.append("'));");
            webView.loadUrl(buffer.toString());
            webView.clearCache(true);
            webView.clearFormData();
            webView.clearHistory();
            webView.clearMatches();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Async task for loading the images from the SD card.
     *
     * @author Android Example
     */

    // Class with extends AsyncTask class
    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {

            case SET_LISTCHARCTER_ADAPTER:
                listAdapter = new ListCharacterAdapter(MainActivity.this, charactersCatList, listener, isRandom);
                lv_charactersCat.setAdapter(listAdapter);
                break;

            case UPDATE_LISTCAHRCTER_UI:
                listAdapter.notifyDataSetChanged();
                break;

            case SHOW_ANIMATION:
                imDice.setImageDrawable(dice[roll[0]]);
                //animDraw.start();
                break;

            case POPULATE_LIST:
                if (savedCharAdapter != null)
                    savedCharAdapter.notifyDataSetChanged();
                break;
        }
        return true;
    }


    private void apirequest(String image) {
        boolean helper = true;
        if (uploading_background) {
            helper = false;
            progress_text.setText("90%");
        }
        String recordurl = "http://vhss.oddcast.com/vhss_editors/saveVokiScene.php";
        HashMap<String, String> map = new HashMap<>();
        map.put("saveXML", final_save_xml);
        map.put("dataStr", image);
        map.put("height", "267");
        map.put("width", "267");
        Log.d("img_str", "" + image);
        Constants.execute(new Super_AsyncTask(this, map, recordurl, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                Log.d("postexecution", "" + output);

                try {
                    if (uploading_background)
                        progress_text.setText("100%");
                    Document doc = getDomElement(output);
                    NodeList typesList = doc.getElementsByTagName("SAVESCENE");
                    for (int i = 0; i < typesList.getLength(); i++) {
                        Node node = typesList.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("RES").equals("OK")) {

                            String id = elem.getAttribute("ID");
                            final_url_for_sharing = "http://www.voki.com/pickup.php?scid=" + id + "&height=323&width=575";
                            is_there_any_change_in_character = false;
                            sensor_handler = true;
                            shareMenuAnimation();
                            if (ViewUtill.haveNetworkConnection(MainActivity.this)) {
                                trackResultOfSaveCharacter();
                            }
                            if (progress != null) {
                                if (progress.isShowing()) {
                                    progress.dismiss();
                                }
                            }
                            Log.d("final_url_for_sharing", "" + final_url_for_sharing);
                        }


                    }

                    Log.d("doccc", "" + typesList.getLength());


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Your Internet Connection is too Slow.Pls try Again", Toast.LENGTH_SHORT).show();
                    //  if (MainActivity.loader.getVisibility() == View.VISIBLE)
                    //    loader.setVisibility(View.INVISIBLE);
                    if (progress != null) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                    }
                    sensor_handler = true;
                }
            }

        }, true, helper));

    }

    public Document getDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }



     /*Method to Stop AudioRecorder*/

    private void stopRecordingForMax() {

      /*  if (null != recorder) {
            isRecording1 = false;

            int i = recorder.getState();
            if (i == 1)
                recorder.stop();
           // recorder.release();

          //  recorder = null;
            //recordingThread = null;

            //progressBar.setProgress(0);
        }*/
        Mp3AudioRecordActivity mp3AudioRecordActivity = new Mp3AudioRecordActivity(MainActivity.this, false, uploadAudioListener);

        //    isRecording = recStopped;
        recordButton.setImageResource(R.mipmap.recbtn);
        recordButton.setEnabled(false);
        //    copyWaveFile(getTempFilename(), getFilename());
        //    deleteTempFile();
        //jump();
        //   uploadAduio();


    }

     /*Method to convert the recorded raw file to wav format*/

    private void copyWaveFile(String inFilename, String outFilename) {

        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 1;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;
            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);
            while (in.read(data) != -1) {
                out.write(data);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     /*Method wav format header*/

    private void WriteWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R'; // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f'; // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16; // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1; // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8); // block align
        header[33] = 0;
        header[34] = RECORDER_BPP; // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        out.write(header, 0, 44);
    }

     /*Method to get access to temp file where raw data is stored*/

    private String getTempFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath, AUDIO_RECORDER_TEMP_FILE);

        if (tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

     /*Method to get the wav file*/

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }
        Log.e("Filepath:>>", file.getAbsolutePath() + "/" + "testWave" + AUDIO_RECORDER_FILE_EXT_WAV);
        return (file.getAbsolutePath() + "/" + "testWave" + AUDIO_RECORDER_FILE_EXT_WAV);
    }

    /*Method to delete temp file*/

    private void deleteTempFile() {
        File file = new File(getTempFilename());

        file.delete();
    }

    /*Method to convert wav to mp3*/

    public void jump() {

        String[] mp3Args = {"--preset", "standard",
                "-q", "0",
                "-m", "s",
                recordWavFile,
                recordMp3File,
        };
        Main m = new Main();

        try {
            m.run(mp3Args);
        } catch (IOException e) {
            System.out.println("ERROR processing MP3 " + e);
        }

    }

    /*Method where character speaks your recorded audio*/

    public void sayurlforkitkat(String getUrl) {

        if (isPlay == false) {

            im_playButton.setImageResource(R.drawable.pause);
            StringBuilder buffer = new StringBuilder("javascript:play_mp3_2('");
            buffer.append(getUrl);

            buffer.append("');");
            Log.e("bufferr", "buffer " + buffer.toString());
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(buffer.toString(), null);
                    webView.clearCache(true);
                    webView.clearFormData();
                    webView.clearHistory();
                    webView.clearMatches();
                } else {
                    webView.loadUrl(buffer.toString());
                    webView.clearCache(true);
                    webView.clearFormData();
                    webView.clearHistory();
                    webView.clearMatches();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            isPlay = true;


        } else {

            try {
                im_playButton.setImageResource(R.drawable.play);
                StringBuilder buffer = new StringBuilder("javascript:_clickPause()");
                webView.loadUrl(buffer.toString());
                webView.clearCache(true);
                webView.clearFormData();
                webView.clearHistory();
                webView.clearMatches();
                isPlay = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //recordButton.setEnabled(true);

    }

    public void sayurl(String getUrl) {

        if (isPlay == false) {

            im_playButton.setImageResource(R.drawable.pause);
            StringBuilder buffer = new StringBuilder("javascript:play_mp3_2('");

            buffer.append(getUrl);
            buffer.append("');");
            Log.e("bufferr", "buffer " + buffer.toString());
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(buffer.toString(), null);
                } else {
                    if (webView != null) {
                        webView.loadUrl(buffer.toString());
                        webView.clearCache(true);
                        webView.clearFormData();
                        webView.clearHistory();
                        webView.clearMatches();
                    }

                }
                recordButton.setEnabled(false);
                isPlay = true;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            try {
                im_playButton.setImageResource(R.drawable.play);
                StringBuilder buffer = new StringBuilder("javascript:_clickPause()");
                webView.loadUrl(buffer.toString());
                webView.clearCache(true);
                webView.clearFormData();
                webView.clearHistory();
                webView.clearMatches();
                recordButton.setEnabled(true);
                isPlay = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    /*Method to read the recorded raw file*/

    private void writeAudioDataToFile() {
        short sData[] = new short[BufferElements2Rec];

        String filename = getTempFilename();
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int read = 0;

        if (null != os) {
            while (isRecording1) {
                read = recorder.read(sData, 0, BufferElements2Rec);

                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    try {
                        byte data[] = short2byte(sData);
                        os.write(data, 0, BufferElements2Rec * BytesPerElement);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*Method to start AudioRecorder*/

    private void startRecording() {
        try {
            if (recorder == null) {
                recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                        RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);

            }

            int i = recorder.getState();
            if (i == 1)
                recorder.startRecording();

            isRecording1 = true;

            recordingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    writeAudioDataToFile();
                }
            }, "AudioRecorder Thread");

            recordingThread.start();
            im_playButton.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*Method to Stop AudioRecorder*/

    private void stopRecording() {

   /*     if (null != recorder) {


            int i = recorder.getState();
            if (i == 1)
                recorder.stop();
            recorder.release();

            recorder = null;
            recordingThread = null;



        }*/
        Mp3AudioRecordActivity mp3AudioRecordActivity = new Mp3AudioRecordActivity(MainActivity.this, false, uploadAudioListener);
        recordButton.setEnabled(false);
        if (timeVal > 0) {
            timeVal = 0;
            mTimerTask.cancel();
        }
        //  copyWaveFile(getTempFilename(), getFilename());
        // deleteTempFile();

        //uploadAduio();


    }

    public void recordaudioeffect(final String recordurl) {

        Constants.execute(new Super_AsyncTask(this, recordurl, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                try {

                    Log.d("outputresponse", "" + output);
                    recordButton.setEnabled(false);
                    Document doc = getDomElement(output);
                    NodeList typesList = doc.getElementsByTagName("PREVIEWSWF");
                    for (int i = 0; i < typesList.getLength(); i++) {
                        Node node = typesList.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("FULLNAME").endsWith(".mp3")) {
                            fullname = elem.getAttribute("FULLNAME");
                            Log.e("Fullname==", fullname);
                            sayeffect(fullname);
                            //    loader.setVisibility(View.INVISIBLE);
                        }

                    }
                    NodeList typesList1 = doc.getElementsByTagName("ERROR");
                    for (int i = 0; i < typesList1.getLength(); i++) {
                        Node node = typesList1.item(i);

                        Element elem = (Element) node;
                        if (elem.getAttribute("MSG").endsWith("!")) ;
                        {
                            //   loader.setVisibility(View.INVISIBLE);
                            im_playButton.setImageResource(R.drawable.play);
                            Toast.makeText(context, "Server Error, Please Try Again", Toast.LENGTH_LONG).show();

                        }


                    }


                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                    Toast.makeText(MainActivity.this, "Something went wrong.Make sure you are connected to Internet3", Toast.LENGTH_SHORT).show();
                    //  loader.setVisibility(View.INVISIBLE);
                }
            }

        }, true, false));

    }

    public void effectchar() {
        String audioId = getAudioId;
        String buttonPressed = "1";
        String acc_id = "37533";
        String url = "curFX=" + curFX + "&audioId=" + audioId + "&buttonPressed=" + buttonPressed + "&acc_id=" + acc_id;
        String urlEncoded = "http://vhss.oddcast.com/admin/saveFXAudio.php?" + url;
        Log.e("urlEncoded", urlEncoded);
        recordaudioeffect(urlEncoded);
    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];

        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    public void saychartext(String url) {
        im_playButton.setImageResource(R.drawable.pause);
        isTextSpeech = true;
        StringBuilder buffer = new StringBuilder("javascript:play_mp3_2('");//"javascript:play_mp3_2('"
        buffer.append(url);
        buffer.append("');");

        Log.e("bufferr", "buffer " + buffer.toString());


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript(buffer.toString(), null);
                Log.d("aaaaaa", "aaaaaa");
            } else {
                webView.loadUrl(buffer.toString());
                webView.clearCache(true);
                webView.clearFormData();
                webView.clearHistory();
                webView.clearMatches();
                Log.d("bbbbbbbb", "bbbbbbb");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //   loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            im_playButton.setImageResource(R.drawable.play);
            StringBuilder buffer = new StringBuilder("javascript:_clickPause()");
            if (webView != null)
                webView.loadUrl(buffer.toString());
            sensorMan.unregisterListener((SensorEventListener) this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String[] ReadFromfile(String character, String filename) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = getResources().getAssets()
                    .open(filename, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            Log.e("start", "<--Exception-->" + e);
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }

        String json_string = returnString.toString();
        Log.e("json_string", "<-->" + json_string);
        String[] arr = parseJsonData(json_string, character);
        return arr;
    }

    private String[] parseJsonData(String json_string, String character) {
        try {
            JSONObject jsonObject = new JSONObject(json_string);
            Log.e("jsonObject>>1", "" + jsonObject);

            jsonObject.optString(character);
            Log.e("jsonObject>>2", "" + jsonObject.optString(character));
            JSONArray jArray = jsonObject.getJSONArray(character);
            Log.e("jArray", "" + jArray);
            return getStringArray(jArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getStringArray(JSONArray jsonArray) {
        String[] stringArray = null;
        int length = jsonArray.length();
        if (jsonArray != null) {
            stringArray = new String[length];
            for (int i = 0; i < length; i++) {
                stringArray[i] = jsonArray.optString(i);
            }
        }
        Log.e("stringArray", "<-->" + stringArray);
        return stringArray;
    }


    public void sayeffect(String fullname)

    {
        if (isEffectSay == false) {
            im_playButton.setImageResource(R.drawable.pause);

            StringBuilder buffer = new StringBuilder("javascript:play_mp3_2('");
            buffer.append(fullname);
            buffer.append("');");
            Log.e("bufferr", "buffer " + buffer.toString());
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(buffer.toString(), null);
                    Log.d("aaaaaa", "aaaaaa");
                } else {
                    webView.loadUrl(buffer.toString());
                    webView.clearCache(true);
                    webView.clearFormData();
                    webView.clearHistory();
                    webView.clearMatches();
                    Log.d("bbbbbbbb", "bbbbbbb");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            isEffectSay = true;
        } else if (isEffectSay == true) {
            im_playButton.setImageResource(R.drawable.play);

            try {
                StringBuilder buffer = new StringBuilder("javascript:_clickPause()");
                webView.loadUrl(buffer.toString());
                webView.clearCache(true);
                webView.clearFormData();
                webView.clearHistory();
                webView.clearMatches();
                isEffectSay = false;
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        isSayUrl = false;
    }

    public void doRandom(final String url, final String xpos, final String ypos, final String scalepos) throws JSONException {

        new AsyncTask<Void, Void, Void>() {
            int no = 0;
            String glasses = "", costume = "", necklace = "", hair = "", hat = "", mouth = "", fhair = "", Props = "";
            String sprite_sheet = "", color_string = "", name = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Constants.Accessary_RANDOM_GLASSES = "";
                Constants.Accessary_RANDOM_COSTUME = "";
                Constants.Accessary_RANDOM_NECKLACE = "";
                Constants.Accessary_RANDOM_HAIR = "";
                Constants.Accessary_RANDOM_HAT = "";
                Constants.Accessary_RANDOM_MOUTH = "";
                Constants.Accessary_RANDOM_FHAIR = "";
                Constants.Accessary_RANDOM_PROPS = "";

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject str = Constants.jsonObject;
                Log.e("JSONObjectJSONObject", str.toString());
                try {

                    JSONObject types_json_obj = str.optJSONObject("Host");
                    name = types_json_obj.optString("name");
                    sprite_sheet = types_json_obj.optString("spriteSheet");
                    color_string = types_json_obj.optString("colorString");
                    JSONObject types_json = types_json_obj.optJSONObject("Types");
                    JSONArray type_array = types_json.optJSONArray("Type");
                    final String cat_id1[] = new String[type_array.length()];
                    Log.e("type_array", "" + type_array.length());
                    if (type_array.length() > 0) {
                        no = 1;
                        for (int i = 0; i < type_array.length(); i++) {

                            JSONObject inner_json = type_array.optJSONObject(i);
                            cat_id1[i] = inner_json.getString("id");
                            Log.e("Values of Cat_ids", cat_id1[i]);
                            if (cat_id1[i].equalsIgnoreCase("Glasses")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                glasses = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_GLASSES = glasses;
                            } else if (cat_id1[i].equalsIgnoreCase("Costume")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                costume = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_COSTUME = costume;
                            } else if (cat_id1[i].equalsIgnoreCase("Necklace")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                necklace = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_NECKLACE = necklace;
                            } else if (cat_id1[i].equalsIgnoreCase("Hair")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                hair = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_HAIR = hair;
                            } else if (cat_id1[i].equalsIgnoreCase("Hat")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                hat = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_HAT = hat;
                            } else if (cat_id1[i].equalsIgnoreCase("mouth")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                mouth = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_MOUTH = mouth;
                            } else if (cat_id1[i].equalsIgnoreCase("fhair")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                fhair = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_FHAIR = fhair;
                            } else if (cat_id1[i].equalsIgnoreCase("Props")) {
                                JSONArray subcat_array = inner_json.optJSONArray("SubCat");
                                JSONObject subcat_obj = subcat_array.optJSONObject(0);
                                JSONArray acc_array = subcat_obj.optJSONArray("Acc");
                                Random r = new Random();
                                int no = r.nextInt(acc_array.length() - 0) + 0;
                                JSONObject acc_id_obj = acc_array.optJSONObject(no);
                                Props = acc_id_obj.optString("id");
                                Constants.Accessary_RANDOM_PROPS = Props;
                            }
                        }

                        Log.d("accessoryyy", "glases" + glasses + "costume" + costume + "hair" + hair + "props" + Props + "fhair" + "mouth" + mouth + "fhair" + fhair + "hat" + hat + "necklace" + necklace);
                    } else {
                        no = 2;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try {
                    if (no == 1) {
                        if (name.equals("Masaru")) {
                            costume = "35212";
                        }
                        StringBuilder buf = new StringBuilder("javascript:RunXMLfile('");
                        buf.append("file:///android_asset/");
                        buf.append("','");
                        buf.append(sprite_sheet);
                        buf.append("','");
                        buf.append(color_string);
                        buf.append("','");
                        buf.append(costume);
                        buf.append("','");
                        buf.append(fhair);
                        buf.append("','");
                        buf.append(glasses);
                        buf.append("','");
                        buf.append(hair);
                        buf.append("','");
                        buf.append(hat);
                        buf.append("','");
                        buf.append(mouth);
                        buf.append("','");
                        buf.append(necklace);
                        buf.append("','");
                        buf.append(Props);
                        buf.append("');");
                        try {
                            webView.loadUrl(buf.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("onpost", "here");
                    } else if (no == 2) {
                        try {
                            webView.loadUrl(url);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    StringBuilder buffer = new StringBuilder("javascript:positionChar('");
                    buffer.append(xpos);
                    buffer.append("','");
                    buffer.append(ypos);
                    buffer.append("','");
                    buffer.append(scalepos);
                    buffer.append("');");
                    webView.loadUrl(buffer.toString());
                    webView.clearCache(true);
                    webView.clearFormData();
                    webView.clearHistory();
                    webView.clearMatches();
                    webView.setVisibility(View.INVISIBLE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            catSpecsAdapter.notifyDataSetChanged();
                        }
                    });
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (webView != null) {
                                webView.setVisibility(View.VISIBLE);
                            }
                        }
                    }, 1000);

             /*   if (menuContainer3.getVisibility() == View.VISIBLE) {
                    updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER3_VISIBLE);
                }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute();

    }

    protected void onNewIntent(Intent intent) {
        try {
            if (intent.getScheme().equalsIgnoreCase(("terms"))) {

                Toast.makeText(getBaseContext(), "Error: No Accelerometer.", Toast.LENGTH_LONG).show();

            } else if (intent.getScheme().equalsIgnoreCase(("privacy"))) {

            } else {
                super.onNewIntent(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class testWebChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, final String message, final JsResult result) {

            Log.d("testweb", "" + message + "result" + result);

            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("testwebb", "" + message + "result" + result);

                    if (message.contains("-1")) {
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            playdefaultaudioforkitkat();
                        } else {
                            Log.d("if_da", "iff");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {


                                    String[] list;


                                    if (Constants.CURRENT_CHAR_GENDER.equals("1")) {
                                        try {
                                            list = getAssets().list("defaultAudios/audiosFemale");
                                            // getAssets().openFd("file:///android_asset/defaultAudios/audiosFemale/");
                                            if (list.length > 0) {
                                                for (int i = 0; i < list.length; i++) {
//                                Random random = new Random();
//                                int ran = random.nextInt(i);
                                                    String file = list[i];
                                                    PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                                    playbackModel.setFileName(file);
                                                    playbackModel.setFolderName("audiosFemale");
                                                    Log.e("audioFile", "" + file);

                                                    femaleAudioList.add(playbackModel);
                                                    //break;
                                                }

                                                Random rand = new Random();
                                                femalePos = rand.nextInt(femaleAudioList.size());
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (Constants.CURRENT_CHAR_GENDER.equals("2")) {
                                        try {
                                            list = getAssets().list("defaultAudios/audiosMale");
                                            if (list.length > 0) {
                                                for (int i = 0; i < list.length; i++) {
                                                    String file = list[i];
                                                    PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                                    playbackModel.setFileName(file);
                                                    playbackModel.setFolderName("audiosMale");
                                                    Log.e("audioFile", "" + file);

                                                    maleAudioList.add(playbackModel);
                                                }

                                                Random rand = new Random();
                                                malePos = rand.nextInt(maleAudioList.size());
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        try {
                                            list = getAssets().list("defaultAudios/audiosOther");
                                            if (list.length > 0) {
                                                for (int i = 0; i < list.length; i++) {
                                                    String file = list[i];
                                                    PlaybackAudioModel playbackModel = new PlaybackAudioModel();
                                                    playbackModel.setFileName(file);
                                                    playbackModel.setFolderName("audiosOther");
                                                    Log.e("audioFile", "" + file);

                                                    othersAudioList.add(playbackModel);
                                                }
                                                Random rand = new Random();
                                                otherPos = rand.nextInt(othersAudioList.size());
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }


                            });
                            String fileName = "";
                            String path = "";
                            String subPath = "";
                            String assestPath = "";
                            if (Constants.CURRENT_CHAR_GENDER.equals("1")) {
                                subPath = "/audiosFemale/";
                                fileName = femaleAudioList.get(femalePos).getFileName();
                                assestPath = "defaultAudios" + subPath + fileName;
                                Log.e("filename", fileName);
                                path = "file:///android_asset/defaultAudios" + subPath + fileName;
                            } else if (Constants.CURRENT_CHAR_GENDER.equals("2")) {
                                subPath = "/audiosMale/";
                                fileName = maleAudioList.get(malePos).getFileName();
                                assestPath = "defaultAudios" + subPath + fileName;
                                path = "file:///android_asset/defaultAudios" + subPath + fileName;
                            } else {
                                subPath = "/audiosOther/";
                                fileName = othersAudioList.get(otherPos).getFileName();
                                assestPath = "defaultAudios" + subPath + fileName;
                                path = "file:///android_asset/defaultAudios" + subPath + fileName;
                            }


                            try {
                                Uri uri = Uri.parse(path);
                                FileManager fileManager = new FileManager();
                                String kitkat_path = fileManager.getPath(MainActivity.this, uri);
                                Log.e("bufferr", "buffer " + path + "   " + assestPath);
                                final StringBuilder buffer = new StringBuilder("javascript:play_mp3_2('");
                                buffer.append(path);
                                buffer.append("');");
                                webView.getSettings().setJavaScriptEnabled(true);
                                Log.e("bufferrrrrr", "buffer " + buffer.toString());
                                im_playButton.setImageResource(R.drawable.pause);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    webView.evaluateJavascript(buffer.toString(), null);
                                } else {

                                    webView.loadUrl(buffer.toString());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Uri uri = Uri.parse(path);
                            // path = "file:///android_asset/defaultAudios/audiosFemale/F-0111.aac";

                            //  webView.getSettings().setJavaScriptEnabled(true);
                            //webView.setWebViewClient(new SpeakWebChrome(MainActivity.this, path));

                            //webView.setWebChromeClient(new MyWebChromeClient());


                            //   webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                            //  webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
// register class containing methods to be exposed to JavaScript
                            //  JavascriptInterface myJSInterface=new JavascriptInterface(this);
                            // webView.addJavascriptInterface(myJSInterface, "JSInterface");
                            //webView.loadDataWithBaseURL(buffer.toString(),null, "audio/mpeg3", "UTF-8", null);


//        if (Constants.LAST_AUDIO_PLAYED < 31) {
//            Constants.LAST_AUDIO_PLAYED = Constants.LAST_AUDIO_PLAYED + 1;
//        } else {
//            Constants.LAST_AUDIO_PLAYED = 0;
//        }
                        }
                    } else {
                        Log.d("else_da", "else");
                        im_playButton.setImageResource(R.drawable.play);
                        final StringBuilder buffer = new StringBuilder("javascript:stop_mp3('");
                        buffer.append("');");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            webView.evaluateJavascript(buffer.toString(), null);
                        } else {

                            webView.loadUrl(buffer.toString());
                        }

                    }

                    result.confirm();

                }
            });

            return true;
        }
    }

    private class testWebChromeClientForTextAudio extends WebChromeClient {
        String e_id, l_id, fx_type, fx_level, v_id;

        testWebChromeClientForTextAudio(String e_id, String l_id, String v_id, String fx_type, String fx_level) {
            this.e_id = e_id;
            this.l_id = l_id;
            this.v_id = v_id;
            this.fx_type = fx_type;
            this.fx_level = fx_level;

        }

        @Override
        public boolean onJsAlert(WebView view, String url, final String message, final JsResult result) {

            webView.post(new Runnable() {
                @Override
                public void run() {


                    if (message.contains("-1")) {
                        Log.d("v_id", "" + v_id);
                        Random random = new Random();
                        int rd = random.nextInt();
                        String md5Convert = e_id + l_id + v_id + typedMessage + "mp3" + fx_type + fx_level + "voki" + rd +
                                "5892990" + str_session_key + "h3498nt42t266";
                        Log.e("md5Convert", md5Convert);
                        String match_string = e_id + l_id + v_id + typedMessage + fx_type + fx_level;
                        Log.e("random", "" + rd);

                        String finalMd5String = Constants.convertPassMd5(md5Convert);
                        Log.e("finalMd5String", finalMd5String);


                        Log.d("str_session_key", "" + str_session_key);

                        String url = e_id + "&LID=" + l_id + "&VID=" + v_id + "&TXT=" + typedMessage
                                + "&EXT=mp3&FX_TYPE=" + fx_type + "&FX_LEVEL=" + fx_level + "&FNAME=voki" + rd + "&ACC=5892990&SESSION=" + str_session_key + "&CS=" + finalMd5String.trim();
                        String urlEncoded = "http://cache.oddcast.com/tts/gen.php?EID=" + url;
                        if (check_url_for_same_text.equals(match_string)) {
                            what_to_share_now = "SHARE_TEXT_AUDIO";
                            if (!url_to_be_played.isEmpty())
                                saychartext(url_to_be_played);
                        } else {
                            uploadDataToServer(urlEncoded);
                        }
                        check_url_for_same_text = e_id + l_id + v_id + typedMessage + fx_type + fx_level;
                        Log.d("playurl", "" + urlEncoded);

                        stop_audio_helper = true;
                    } else {
                        stop_audio_helper = false;
                        im_playButton.setImageResource(R.drawable.play);
                        final StringBuilder buffer = new StringBuilder("javascript:stop_mp3('");
                        buffer.append("');");
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                webView.evaluateJavascript(buffer.toString(), null);
                            } else {

                                webView.loadUrl(buffer.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    result.confirm();
                }
            });
            return true;
        }
    }




    private void performActionOnEnteringText() {
        et_typeMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                   // Toast.makeText(MainActivity.this, et_typeMessage.getText(), Toast.LENGTH_SHORT).show();
                   // typedMessage = et_typeMessage.getText().toString();
                   // playAudioFromText(Constants.ENGINE_ID, Constants.LANGUAGE_ID, Constants.VOICE_ID, fx_type, fx_level);
                    im_playButton.performClick();
                    InputMethodManager imm= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_typeMessage.getWindowToken(), 0);
                    return true;
                }
                return false;

            }

        });
    }
}

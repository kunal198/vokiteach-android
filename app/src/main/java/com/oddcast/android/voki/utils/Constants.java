package com.oddcast.android.voki.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.oddcast.android.voki.Async_Thread.Super_AsyncTask;
import com.oddcast.android.voki.Async_Thread.Super_AsyncTask_Interface;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.CharLanguageModel;
import com.oddcast.android.voki.Modal.FxMenuModel;
import com.oddcast.android.voki.Modal.LanguageModel;
import com.oddcast.android.voki.Modal.ParseAccessoryModel;
import com.oddcast.android.voki.database.DatabaseHandler;
import com.oddcast.android.voki.httprequests.HttpConnections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by brst-pc16 on 4/27/16.
 */
public class Constants {
    ;

    /*
    Keys ....ACCESSORY_SAVE_POS
     */

      public static String Accessary_RANDOM_GLASSES="";
      public static String Accessary_RANDOM_COSTUME="";
      public static String Accessary_RANDOM_NECKLACE="";
      public static String Accessary_RANDOM_HAIR="";
      public static String Accessary_RANDOM_HAT="";
      public static String Accessary_RANDOM_MOUTH="";
      public static String Accessary_RANDOM_FHAIR="";
      public static String Accessary_RANDOM_PROPS="";



    public static String XPOS_FOR_SHARING_CHARACTER="0.010";
    public static String YPOS_FOR_SHARING_CHARACTER="0.3";
    public static String SCALE_FOR_SHARING_CHARACTER="0.45";


    public static String ACCESSORY_SAVE_POS = "accessory_save_pos";
    public static String SAVE_POS = "save_pos";
    public static String SAVE_CHAR_POS = "save_char_pos";
    public static String NEW_CONTAINER_SHOW_CHARACTER = "showcharacter";
    public static String MENU_CONTAINER_VISIBLE = "menuContainer_Visible";
    public static String MENU_CONTAINER_INVISIBLE = "menuContainer_Invisible";
    public static String MENU_CONTAINER2_VISIBLE = "menuContainer2_Visible";
    public static String MENU_CONTAINER2_INVISIBLE = "menuContainer2_Invisible";
    public static String MENU_CONTAINER3_VISIBLE = "menuContainer3_Visible";
    public static String MENU_CONTAINER3_INVISIBLE = "menuContainer3_Invisible";
    public static String COLORPICKER_CONTAINER_VISIBLE = "colorpickerContainer_Visible";
    public static String COLORPICKER_CONTAINER_INVISIBLE = "colorpickerContainer_Invisible";
    public static String RIGHT_CONTAINER_VISIBLE = "rightContainer_Visible";
    public static String RIGHT_CONTAINER_INVISIBLE = "rightContainer_Invisible";
    public static String GET_CHARACTER_URL = null;
    public static String VOKI_KEY = "voki_key";
    public static String SESSION_KEY = "session_key";
    public static String TEMP_SAVED_CHARACTER = "Aiko_217";
    public static String TEMP_SAVED_CATEGORY = "Anime";
    public static String TEMP_SAVED_BACKGROUND = "Blue Wave";
    public static String CURRENT_CHAR_XPOS = "0";
    public static String CURRENT_CHAR_YPOS = "-0.15";
    public static String TEMP_CHAR_XPOS = "0";
    public static String TEMP_CHAR_YPOS = "-0.15";
    public static String GENDER_ID = "1";

    public static String X_VALUE = "0.04";
    public static String SELECTED_LANG_ID = "1";
    public static String SELECTED_LANG = "English";

    public static String SELECTED_FX = "Fx";
    public static String SELECTED_FX_ID = "";

    public static String CURRENT_SELECTED_FX = "";

    public static String SELECTED_LANG_CHAR = "Julie";


//    public static String
    /**
     * TTs Api variables ...
     */

    public static String ENGINE_ID = "3";
    public static String LANGUAGE_ID = "1";
    public static String VOICE_ID = "3";
    public static String ACC = "";
    public static String SECRET_KEY = "evt6bu4n!sDk";


    ProgressDialog pDialog;
    public static String fxMenuResponse = "";

    public static String isSelectedKey = "select";

    public static String CURRENT_CHARACTER_NAME = "Aiko_217";
    public static String PREVIOUS_CHARACTER_NAME = "Aiko_217";
    public static String CURRECT_CHARACTER_CATEGORY = "Anime";
    public static String NEW_CHARACTER_CATEGORY = "Anime";
    public static String COSTUME_NAME = "anime_green_costume";
    public static String CURRENT_CHAR_GENDER = "1";

    public static ArrayList<ParseAccessoryModel> costume_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> glasses_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> hair_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> facial_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> bling_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> props_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> mouth_array = new ArrayList<>();
    public static ArrayList<ParseAccessoryModel> hat_array = new ArrayList<>();
    public static ArrayList<HashMap<String, String>> arr_default = new ArrayList<>();
    public static ArrayList<FxMenuModel> blank_arraylist = new ArrayList<>();

    public static ArrayList<String> check_list = new ArrayList<>();

    public static JSONObject jsonObject = null;

    public static String BUNDLE_PATH = "";
    public static String SPRITESHEET = "";
    public static String COLORSTRING = "";

    public static ArrayList<FxMenuModel> fxCategories = new ArrayList<>();

    public static URL url;
    public static HttpURLConnection connection = null;
    public static StringBuffer response;
    public static String finalResponse;

    static HttpConnections con = new HttpConnections();
    static String newData;

    public static String SESSION_ID = "";
    static ArrayList<FxMenuModel> fxResponseArray = new ArrayList<>();

    public static String GENDER_MALE = "2";
    public static String GENDER_FEMALE = "1";
    public static int LAST_AUDIO_PLAYED = 0;
    public static String EYES_COLOR = "etc";
    public static String MOUTHCOLOR = "etc";
    public static String HAIRCOLOR = "etc";
    public static String SKINCOLOR = "etc";
    public static String CHARACTERURL = "etc";
    /**
     * Method for reading data from xml file ............
     *
     * @param mContext
     * @param select_folder
     * @param file_name
     * @return
     */
    public static ArrayList<HashMap<String, String>> readXml(Context mContext, String select_folder, String file_name, boolean isSavedChar) {

        PREVIOUS_CHARACTER_NAME = CURRENT_CHARACTER_NAME;

        String folder_select;
        String name_file;
        String bundlePath = "file:///android_asset/";
        String sprite_sheet = "0";
        String colorString = "0";
        String Costume = "0";
        String fhair = "0";
        String Glasses = "0";
        String Hair = "0";
        String Hat = "0";
        String mouth = "0";
        String Necklace = "969";
        String Props = "0";

        String xPos = "0";
        String yPos = "0";
        String scale = "0";
        Context context;
        StringBuilder buf;

        folder_select = select_folder;
        name_file = file_name;
        jsonObject = new JSONObject();
        context = mContext;

        BUNDLE_PATH = bundlePath;

        try {
            InputStream is = null;
            is = context.getAssets().open("img/char/" + folder_select + "/" + name_file + "/desc.xml");


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            doc.getDocumentElement().normalize();

            Log.e("Rootelement :", "faefwe   " + doc.getDocumentElement().getNodeName());

            JSONObject nodeObject = new JSONObject();
            nodeObject.put("gender", doc.getDocumentElement().getAttribute("gender"));
            GENDER_ID = doc.getDocumentElement().getAttribute("gender");
            CURRENT_CHAR_GENDER = doc.getDocumentElement().getAttribute("gender");
            nodeObject.put("catagory", doc.getDocumentElement().getAttribute("catagory"));
            nodeObject.put("name", doc.getDocumentElement().getAttribute("name"));
            nodeObject.put("spriteSheet", doc.getDocumentElement().getAttribute("spriteSheet"));
            SPRITESHEET = doc.getDocumentElement().getAttribute("spriteSheet");

            CURRENT_CHARACTER_NAME = doc.getDocumentElement().getAttribute("name");

            sprite_sheet = doc.getDocumentElement().getAttribute("spriteSheet");

            nodeObject.put("colorString", doc.getDocumentElement().getAttribute("colorString"));
            colorString = doc.getDocumentElement().getAttribute("colorString");
            COLORSTRING = doc.getDocumentElement().getAttribute("colorString");
            nodeObject.put("accString", doc.getDocumentElement().getAttribute("accString"));
            nodeObject.put("id", doc.getDocumentElement().getAttribute("id"));

            jsonObject.put("Host", nodeObject);
            Log.e("node_json", "" + nodeObject);

            NodeList posList = doc.getElementsByTagName("Position");

            for (int t = 0; t < posList.getLength(); t++) {
                Node node = posList.item(t);

                Element elem = (Element) node;

                JSONObject positionJSONObject = new JSONObject();
                positionJSONObject.put("FLOATING", elem.getAttribute("FLOATING"));
                positionJSONObject.put("XPOS", elem.getAttribute("XPOS"));
                positionJSONObject.put("YPOS", elem.getAttribute("YPOS"));
                positionJSONObject.put("SCALE", elem.getAttribute("SCALE"));


                xPos = elem.getAttribute("XPOS");
                yPos = elem.getAttribute("YPOS");
                scale = elem.getAttribute("SCALE");
                Log.e("nodeList", "" + elem.getAttribute("FLOATING") + "--" + elem.getAttribute("XPOS") +
                        "---" + elem.getAttribute("YPOS") + "---" + elem.getAttribute("SCALE"));
                nodeObject.put("Position", positionJSONObject);
            }


            /**
             *  Default Nodes List ...........
             */
            NodeList nList = doc.getElementsByTagName("Default");
            Log.e("node_length", "" + nList.getLength());

            if (nList.getLength() > 0) {
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    JSONArray defaultNodeArray = new JSONArray();
                    Log.e("Current_Element :", "current : " + nNode.getNodeName());
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;


                        int nodeCount = eElement.getElementsByTagName("Type").getLength();
                        for (int m = 0; m < nodeCount; m++) {
                            JSONObject jsObject = new JSONObject();
                            Element ee = (Element) eElement.getElementsByTagName("Type").item(m);
                            Log.e("new_attr", "" + ee.getAttribute("id") + "--------" + ee.getAttribute("accId"));
                            jsObject.put("accId", ee.getAttribute("accId"));
                            jsObject.put("id", ee.getAttribute("id"));

                            if (ee.getAttribute("id").equals("Costume")) {
                                Costume = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equals("Hair")) {
                                Hair = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equals("Necklace")) {
                                Necklace = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equals("Hat")) {
                                Hat = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equals("fhair")) {
                                fhair = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equals("Glasses")) {
                                Glasses = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equalsIgnoreCase("Mouth")) {
                                mouth = ee.getAttribute("accId");
                            } else if (ee.getAttribute("id").equalsIgnoreCase("Props")) {
                                Props = ee.getAttribute("accId");
                            }

                            defaultNodeArray.put(jsObject);
                        }
                        Log.e("default_node", "" + defaultNodeArray);

                    }

                    nodeObject.put("Default", defaultNodeArray);
                }
            }

            /*-------------------------------------------------------------------------------------------------------*/
            NodeList typesList = doc.getElementsByTagName("Types");
            Log.e("types_node", "" + typesList.getLength());

            /**
             * Types Node ...
             */

            JSONObject typesJsonObject = new JSONObject();

            for (int i = 0; i < typesList.getLength(); i++) {
                Node innerNode = typesList.item(i);

                JSONObject typeJsonObject = new JSONObject();
                JSONArray typesJSOnArray = new JSONArray();
                if (innerNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element elm = (Element) innerNode;

                    // count for how many type tags are there ....
                    int innerCount = elm.getElementsByTagName("Type").getLength();
//                    Toast.makeText(MainActivity.this, "count : "+innerCount, Toast.LENGTH_SHORT).show();
                    Log.e("inner_Count", "count : " + innerCount);

                    for (int j = 0; j < innerCount; j++) {
                        Element ee = (Element) elm.getElementsByTagName("Type").item(j);

                        JSONObject typeJSOnObject = new JSONObject();
                        typeJSOnObject.put("id", ee.getAttribute("id"));

                        JSONArray subCatArray = new JSONArray();

                        JSONObject subCatJsonObject = new JSONObject();

                        int subCount = ee.getElementsByTagName("SubCat").getLength();
                        Log.e("subCount", "count : " + subCount);

                        for (int k = 0; k < subCount; k++) {
                            Element subCatElement = (Element) ee.getElementsByTagName("SubCat").item(k);
                            JSONObject subCatJson = new JSONObject();
                            subCatJson.put("id", subCatElement.getAttribute("id"));
                            int accCount = subCatElement.getElementsByTagName("Acc").getLength();
                            Log.e("accCount", "count : " + accCount);

                            JSONArray accJSonArray = new JSONArray();


                            for (int l = 0; l < accCount; l++) {
                                Element accElement = (Element) subCatElement.getElementsByTagName("Acc").item(l);

                                JSONObject accJsonObject = new JSONObject();
                                accJsonObject.put("name", accElement.getAttribute("name"));
                                accJsonObject.put("id", accElement.getAttribute("id"));


                                int iconCount = accElement.getElementsByTagName("Icons").getLength();
                                Log.e("iconCount", "" + iconCount);


                                for (int m = 0; m < iconCount; m++) {
                                    Element iconElement = (Element) accElement.getElementsByTagName("Icons").item(m);

                                    JSONObject iconJSOnObject = new JSONObject();
                                    int icCount = iconElement.getElementsByTagName("Icon").getLength();
                                    for (int n = 0; n < icCount; n++) {
                                        Element icElement = (Element) iconElement.getElementsByTagName("Icon").item(n);

                                        iconJSOnObject.put("file", icElement.getAttribute("file"));
                                        iconJSOnObject.put("width", icElement.getAttribute("width"));

                                        Log.e("ic_Element", "file : " + icElement.getAttribute("file"));
                                        Log.e("ic_Element", "width : " + icElement.getAttribute("width"));

                                        accJsonObject.put("Icon", iconJSOnObject);
                                    }
                                }

                                accJSonArray.put(accJsonObject);
                                Log.e("accJSonArray", "" + accJSonArray.length());
                            }
                            subCatJson.put("Acc", accJSonArray);
                            subCatArray.put(subCatJson);
                        }
                        typeJSOnObject.put("SubCat", subCatArray);
                        typesJSOnArray.put(typeJSOnObject);
                    }
                }
                typesJsonObject.put("Type", typesJSOnArray);


                nodeObject.put("Types", typesJsonObject);

            }

            jsonObject.put("Host", nodeObject);
            Log.e("jsonObject", "" + jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("new_exception", "" + e);
        }
        Log.e("run_xml", "bundlePath : " + bundlePath + "--" + "Costume : " + Costume + "----" + "sprite_sheet : " + sprite_sheet + "---" +
                "fhair : " + fhair + "---" + "Glasses : " + Glasses + "---" + "Hair : " + Hair + "---" + "Hat : " + Hat + "---" + "Mouth" + mouth);

        CURRENT_CHAR_XPOS = xPos;
        CURRENT_CHAR_YPOS = yPos;

        if (isSavedChar &&!Constants.CHARACTERURL.isEmpty()) {
Log.d("Constants.CHARACTERURL","Constants.CHARACTERURL"+Constants.CHARACTERURL);
            String arr[]=Constants.CHARACTERURL.split("/");
            if (!arr[2].equals("0"))
            {
                Log.e("Costume2",""+arr[2]);
                Costume=arr[2];
            }
            if (!arr[3].equals("0"))
            {

                Log.e("Costume3",""+arr[3]);
                mouth=arr[3];
            }
            if (!arr[4].equals("0"))
            {
                Log.e("Costume4",""+arr[4]);
                Hair=arr[4];
            }
            else {
                Hair="858";
            }
            if (!arr[5].equals("0"))
            {
                Log.e("Costume5",""+arr[5]);
                fhair=arr[5];
            }
            if (!arr[6].equals("0"))
            {
                Log.e("Costume6",""+arr[6]);
                Hat=arr[6];
            }
            if (!arr[7].equals("0"))
            {
                Log.e("Costume7",""+arr[7]);
                Necklace=arr[7];
            }
            else{
                Necklace="969";
            }
            if (!arr[8].equals("0"))
            {
                Log.e("Costume8",""+arr[8]);
                Glasses=arr[8];
            }
            if (!arr[11].equals("0"))
            {
                Log.e("Costume11",""+arr[11]);
                Props=arr[11];
            }
            buf = new StringBuilder("javascript:RunXMLfileWithColor('");
            buf.append(bundlePath);
            buf.append("','");
            buf.append(sprite_sheet);
            buf.append("','");
            buf.append(colorString);
            buf.append("','");
            buf.append(Costume);
            buf.append("','");
            buf.append(fhair);
            buf.append("','");
            buf.append(Glasses);
            buf.append("','");
            buf.append(Hair);
            buf.append("','");
            buf.append(Hat);
            buf.append("','");
            buf.append(mouth);
            buf.append("','");
            buf.append(Necklace);
            buf.append("','");
            buf.append(Props);

            if (Constants.EYES_COLOR.contains("#"))
            {
                buf.append("','");
                buf.append("eyes");
                buf.append("','");
                buf.append(intToHexa(Integer.parseInt(Constants.EYES_COLOR)));

            }
            if (Constants.SKINCOLOR.matches("[0-9]+"))
            {
                buf.append("','");
                buf.append("skin");
                buf.append("','");
                buf.append(intToHexa(Integer.parseInt(Constants.SKINCOLOR)));
            }
            if (Constants.HAIRCOLOR.matches("[0-9]+"))
            {
                buf.append("','");
                buf.append("hair");
                buf.append("','");
                buf.append(intToHexa(Integer.parseInt(Constants.HAIRCOLOR)));
            }
            if (Constants.EYES_COLOR.matches("[0-9]+"))
            {
                buf.append("','");
                buf.append("eyes");
                buf.append("','");
                buf.append(intToHexa(Integer.parseInt(Constants.EYES_COLOR)));

            }
            if (Constants.MOUTHCOLOR.matches("[0-9]+"))
            {
                buf.append("','");
                buf.append("mouth");
                buf.append("','");
                buf.append(intToHexa(Integer.parseInt(Constants.MOUTHCOLOR)));
            }
            buf.append("');");
           MainActivity.updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);

            Log.e("RunXMLfileWithColor", buf.toString());
        } else {
            buf = new StringBuilder("javascript:RunXMLfile('");
            buf.append(bundlePath);
            buf.append("','");
            buf.append(sprite_sheet);
            buf.append("','");
            buf.append(colorString);
            buf.append("','");
            buf.append(Costume);
            buf.append("','");
            buf.append(fhair);
            buf.append("','");
            buf.append(Glasses);
            buf.append("','");
            buf.append(Hair);
            buf.append("','");
            buf.append(Hat);
            buf.append("','");
            buf.append(mouth);
            buf.append("','");
            buf.append(Necklace);
            buf.append("','");
            buf.append(Props);
            buf.append("');");
            MainActivity.updateCharacterPosition(Constants.CURRENT_CHAR_XPOS, Constants.CURRENT_CHAR_YPOS, Constants.MENU_CONTAINER2_VISIBLE);

        }
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();

        map.put("str_build", buf.toString());
        map.put("x_pos", xPos);
        map.put("y_pos", yPos);
        map.put("scale", scale);

        arrayList.add(map);
        parseAccessory(jsonObject);

        Log.e("costume_array", "" + costume_array);
        Log.e("glasses_array", "" + glasses_array);
        Log.e("hair_array", "" + hair_array);
        Log.e("facial_array", "" + facial_array);
        Log.e("bling_array", "" + bling_array);
        Log.e("props_array", "" + props_array);
        Log.e("mouth_array", "" + mouth_array);
        Log.e("hat_array", "" + hat_array);


        return arrayList;

    }


    public static void execute(Super_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }

    }

    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else {
            return false;
        }
    }

    public static void parseAccessory(JSONObject jsonObject) {

        ArrayList<String> arr = new ArrayList<>();

        if (arr_default.size() > 0) {
            arr_default.clear();
        }

        if (costume_array.size() > 0) {
            costume_array.clear();
        }
        if (glasses_array.size() > 0) {
            glasses_array.clear();
        }
        if (hair_array.size() > 0) {
            hair_array.clear();
        }
        if (facial_array.size() > 0) {
            facial_array.clear();
        }
        if (bling_array.size() > 0) {
            bling_array.clear();
        }
        if (props_array.size() > 0) {
            props_array.clear();
        }
        if (mouth_array.size() > 0) {
            mouth_array.clear();
        }
        if (hat_array.size() > 0) {
            hat_array.clear();
        }

        if (check_list.size() > 0) {
            check_list.clear();
        }
        try {
            JSONObject jsnObject = jsonObject.getJSONObject("Host");
            String char_name = jsnObject.getString("name");
            String category = jsnObject.getString("catagory");
             JSONObject jsonObject1=jsnObject.optJSONObject("Position");
            XPOS_FOR_SHARING_CHARACTER= jsonObject1.optString("XPOS");
            YPOS_FOR_SHARING_CHARACTER= jsonObject1.optString("YPOS");
            SCALE_FOR_SHARING_CHARACTER= jsonObject1.optString("SCALE");

            Log.e("check_list", "check_list " + jsnObject.toString());

            JSONArray default_json = jsnObject.getJSONArray("Default");
            for (int m = 0; m < default_json.length(); m++) {
                JSONObject inner_default = default_json.getJSONObject(m);
                HashMap<String, String> map = new HashMap<>();
                map.put("id", inner_default.getString("id"));
                map.put("accId", inner_default.getString("accId"));
                arr_default.add(map);
                arr.add(inner_default.getString("accId"));
            }


            JSONObject types_json = jsnObject.getJSONObject("Types");
            JSONArray type_array = types_json.getJSONArray("Type");

            for (int i = 0; i < type_array.length(); i++) {
                JSONObject inner_json = type_array.getJSONObject(i);

                String cat_id = inner_json.getString("id");
                Log.e("cat_ids", "" + cat_id);
                JSONArray subcat_array = inner_json.getJSONArray("SubCat");
                for (int j = 0; j < subcat_array.length(); j++) {
                    JSONObject subcat_object = subcat_array.getJSONObject(j);

                    String subcat_id = subcat_object.getString("id");

                    JSONArray acc_array = subcat_object.getJSONArray("Acc");

                    for (int k = 0; k < acc_array.length(); k++) {
                        JSONObject acc_object = acc_array.getJSONObject(k);


                        String acc_name = acc_object.getString("name");
                        String acc_id = acc_object.getString("id");


                        JSONObject icon_object = acc_object.getJSONObject("Icon");


                        String icon_fileName = icon_object.getString("file");
                        if (cat_id.equalsIgnoreCase("Glasses")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);


                            glasses_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("Costume")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                                model.setSelection("false");
                            }
                            costume_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("Necklace")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            bling_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("Hair")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            hair_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("Hat")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            hat_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("mouth")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            mouth_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("fhair")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            facial_array.add(model);
                        } else if (cat_id.equalsIgnoreCase("Props")) {
                            ParseAccessoryModel model = new ParseAccessoryModel();
                            model.setName(acc_name);
                            model.setId(acc_id);
                            model.setIcon(icon_fileName);
                            if (arr.contains(acc_id)) {
                                model.setSelection("true");
                            } else {
                                model.setSelection("false");
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("name", acc_name);
                            map.put("id", acc_id);
                            map.put("icon", icon_fileName);

                            if (arr.contains(acc_id)) {
                                map.put(isSelectedKey, "true");
                            } else {
                                map.put(isSelectedKey, "false");
                            }
                            props_array.add(model);


                        }

                    }
                }
            }

            if (costume_array.size() < 2) {
                check_list.add("false");
            } else {
                check_list.add("true");
            }

            if (hair_array.size() < 2) {
                check_list.add("false");
            } else {
                check_list.add("true");
            }

            if (hat_array.size() < 2) {
                check_list.add("false");

            } else {
                check_list.add("true");
            }

            if (glasses_array.size() < 2) {

                check_list.add("false");
            } else {
                check_list.add("true");
            }

            if (mouth_array.size() < 2) {

                check_list.add("false");
            } else {

                check_list.add("true");
            }

            if (bling_array.size() < 2) {

                check_list.add("false");
            } else {

                check_list.add("true");
            }

            if (facial_array.size() < 2) {

                check_list.add("false");
            } else {

                check_list.add("true");
            }

            if (props_array.size() < 2) {

                check_list.add("false");
            } else {

                check_list.add("true");
            }

            Log.e("check_list", "" + check_list);
            Log.e("costume_list", "" + hat_array);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("new_exception", "" + e);
        }
    }



    public static String readAccessoryXml(Context context, String file_name, String accId) {
        String cat_id = "";
        InputStream is = null;
        JSONObject jsObject = new JSONObject();
        try {
            is = context.getAssets().open("img/acc/id/" + file_name + "/" + accId + "/desc.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            doc.getDocumentElement().normalize();

            Log.e("Rootelement :", "faefwe   " + doc.getDocumentElement().getNodeName());

            JSONObject nodeObject = new JSONObject();
            nodeObject.put("name", doc.getDocumentElement().getAttribute("name"));
            nodeObject.put("accType", doc.getDocumentElement().getAttribute("accType"));
            nodeObject.put("compatId", doc.getDocumentElement().getAttribute("compatId"));
            nodeObject.put("id", doc.getDocumentElement().getAttribute("id"));
            cat_id = doc.getDocumentElement().getAttribute("id");
            jsObject.put("Acc", nodeObject);


            Log.e("new_json_length", "" + jsObject);
            NodeList nList = doc.getElementsByTagName("Frag");
            Log.e("node_lengthssssss", "" + nList.getLength());
            if (nList.getLength() > 0) {
                JSONArray fragArray = new JSONArray();
                for (int t = 0; t < nList.getLength(); t++) {
                    JSONObject jObj = new JSONObject();
                    Node nd = nList.item(t);
                    Element elem = (Element) nd;
                    Log.e("new_node_length", "" + elem);

                    jObj.put("file", elem.getAttribute("file"));
                    jObj.put("fragType", elem.getAttribute("fragType"));

                    fragArray.put(jObj);
                }
                Log.e("fragArray", "" + fragArray);
                jsObject.put("frag", fragArray);

            }

            NodeList icList = doc.getElementsByTagName("Icons");
            Log.e("icList", "" + icList.getLength());

            if (icList.getLength() > 0) {
                JSONArray iconsJSONArray = new JSONArray();
                for (int m = 0; m < icList.getLength(); m++) {
                    Element iconElement = (Element) icList.item(m);

                    Log.e("iconElement", "" + iconElement.getElementsByTagName("Icon"));
                    NodeList iconNodeList = iconElement.getElementsByTagName("Icon");

                    for (int ic = 0; ic < iconNodeList.getLength(); ic++) {
                        JSONObject icJSONObject = new JSONObject();
                        Element iconNodeElement = (Element) iconNodeList.item(ic);

                        Log.e("iconNodeElement", "file : " + iconNodeElement.getAttribute("file"));
                        Log.e("iconNodeElement", "width : " + iconNodeElement.getAttribute("width"));
                        icJSONObject.put("file", iconNodeElement.getAttribute("file"));
                        icJSONObject.put("width", iconNodeElement.getAttribute("width"));

                        iconsJSONArray.put(icJSONObject);
                    }
                }
                jsObject.put("icon", iconsJSONArray);

            }
            Log.e("finalJsonResponse", "" + jsObject);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("IOException", "" + e);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            Log.e("ParserConfigurException", "" + e);
        } catch (SAXException e) {
            e.printStackTrace();
            Log.e("SAXException", "" + e);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", "" + e);
        }

        return cat_id;
    }

    public static String intToHexa(int color) {
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        Log.e("onColorChanged", "" + hexColor);

        return hexColor;
    }


    public static ArrayList<FxMenuModel> getFxMenuData() {


        new AsyncTask<Void, String, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {

                    URL url = new URL("http://vhss-d.oddcast.com/vhss_editors/xml/getVokiFX.xml");

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    String urlParameters = "FX_TYPE";
                    String urlParameters1 = "FX_LEVEL";
                    connection.setRequestMethod("GET");
                    connection.setDoOutput(true);
                    DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                    dStream.writeBytes("");
                    dStream.flush();
                    dStream.close();
                    int responseCode = connection.getResponseCode();

                    final StringBuilder output = new StringBuilder("Request URL " + url);
                    output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters + urlParameters1);
                    output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                    output.append(System.getProperty("line.separator") + "Type " + "GET");
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder responseOutput = new StringBuilder();
                    System.out.println("output===============" + br);
                    while ((line = br.readLine()) != null) {
                        responseOutput.append(line);
                    }
                    br.close();

                    output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());
                    Log.e("inside", "" + responseOutput.toString());
                    fxMenuResponse = responseOutput.toString();

                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return fxMenuResponse;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);

                Log.e("insidepost", "" + aVoid);
                fxResponseArray = getFxMenu(aVoid);
            }
        }.execute();

        return fxResponseArray;
    }


    public static String excutePost(final String targetURL, final String urlParameters, String methodType) {


        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    //Create connection
                    url = new URL("http://vhss.oddcast.com/vhss_editors/getSession.php");
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Disposition", "");
                    connection.setRequestProperty("Content-Type",
                            "application/octet-stream");

                    connection.setRequestProperty("Content-Length", "" +
                            Integer.toString(urlParameters.getBytes().length));
                    connection.setRequestProperty("Content-Language", "en-US");

                    connection.setDoOutput(true);
//                    DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
//                    dStream.writeBytes(urlParameters);
//                    dStream.flush();
//                    dStream.close();
//                    int responseCode = connection.getResponseCode();
                    //Send request
                    DataOutputStream wr = new DataOutputStream(
                            connection.getOutputStream());
                    wr.writeBytes("");
                    wr.flush();
                    wr.close();

                    //Get Response
                    InputStream is = connection.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();


                } catch (Exception e) {

                    e.printStackTrace();
                    return null;

                } finally {

                    if (connection != null) {
                        connection.disconnect();
                    }
                }
                Log.e("fnResponse", "dshfhdf" + response.toString());

                return response.toString();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                finalResponse = s;
                Log.d("seesion", "" + finalResponse);
            }
        }.execute();

        return finalResponse;
    }

    public static ArrayList<FxMenuModel> getFxMenu(String response) {

        if (fxCategories.size() > 0)
            fxCategories.clear();

        Log.e("inIOExcdrgszdthbteption", "argftaergvedzvfrgdzvfrdf" + response);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(response.getBytes());


            Document doc = dBuilder.parse(stream);

            doc.getDocumentElement().normalize();

            Log.e("NewRootelement :", "faefwe   " + doc.getDocumentElement().getNodeName());

            NodeList posList = doc.getElementsByTagName("FX");
            Log.e("NodeLength", "" + posList.getLength());
            for (int t = 0; t < posList.getLength(); t++) {
                Node node = posList.item(t);
                FxMenuModel fxModel = new FxMenuModel();
                Element elem = (Element) node;
                Log.e("Elem" + t, "" + elem.getAttribute("NAME"));
                Log.e("Elem" + t, "" + elem.getAttribute("PARAM"));

                fxModel.setFxName(elem.getAttribute("NAME"));
                fxModel.setParam(elem.getAttribute("PARAM"));

                fxCategories.add(fxModel);

            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            Log.e("inParser", "" + e);
        } catch (SAXException e) {
            e.printStackTrace();
            Log.e("inParserSax", "" + e);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("inIOException", "" + e);
        }

        Log.e("fxCategories", "" + fxCategories);
//        MainActivity mn = new MainActivity();
//        MainActivity.showActionSheet("fxs");
        return fxCategories;
    }

    //    "Arabic:27",@"Catalan:5",@"Chinese:10",@"Danish:19",@"Dutch:11",@"English:1",@"Esperanto:31",@"Finnish:23",@"French:4",
//    @"Galician:15",@"German:3",@"Greek:8",@"Italian:7",
//    @"Japanese:12",@"Korean:13",@"Norwegian:20",@"Polish:14",@"Portuguese:6",@"Romanian:30",@"Russian:21",@"Spanish:2",@"Swedish:9",@"Turkish:16", nil

    public static void setDataToLangTable(Context mContext) {

        String[] arr = {"Arabic", "Catalan", "Chinese", "Danish", "Dutch", "English", "Esperanto", "Finnish", "French", "Galician",
                "German", "Greek", "Italian", "Japanese", "Korean", "Norwegian", "Polish", "Portuguese", "Romanian", "Russian", "Spanish",
                "Swedish", "Turkish"};
        String[] langid = {"27", "5", "10", "19", "11", "1", "31", "23", "4", "15", "3", "8", "7", "12", "13", "20", "14", "6",
                "30", "21", "2", "9", "16"};

        ArrayList<String> arrString = new ArrayList<>();


        DatabaseHandler db = new DatabaseHandler(mContext);
        ArrayList<LanguageModel> bList = db.getAllSavedLanguages();

        if (bList.size() <= 0) {

            ArrayList<HashMap<String, String>> arrlist = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                HashMap<String, String> map = new HashMap<>();
                map.put("lang", arr[i]);
                map.put("id", langid[i]);

                arrlist.add(map);
            }

            for (int j = 0; j < arrlist.size(); j++) {
                boolean bool = db.insertLanguage(arrlist.get(j).get("lang"), arrlist.get(j).get("id"));
                Log.e("insertedBool", "" + bool);
            }
        }

        Log.e("listDb", "" + db.getAllSavedLanguages());


        ArrayList<CharLanguageModel> arrayModel = db.getAllLanguages();

        String[] strArr = {"1:Alan (Australian):9:2", "1:Allison (US):7:2", "1:Catherine (UK):6:2", "1:Dave (US):2:2", "1:Elizabeth (UK):4:2",
                "1:Grace (Australian):10:2", "1:Simon (UK):5:2", "1:Steven (US):8:2", "1:Susan (US):1:2", "1:Veena (Indian):11:2", "2:Carmen (Castilian):1:2",
                "2:Juan (Castilian):2:2", "2:Francisca (Chilean):3:2", "2:Diego (Argentine):4:2", "2:Esperanza (Mexican):5:2", "2:Jorge (Castilian):6:2",
                "2:Carlos (American):7:2", "2:Soledad (American):8:2", "2:Leonor (Castilian):9:2", "2:Ximena:10:2", "3:Stefan:2:2", "3:Katrin:3:2",
                "4:Bernard:2:2", "4:Jolie:3:2", "4:Florence:4:2", "4:Charlotte:5:2", "4:Olivier:6:2", "5:Montserrat:1:2", "5:Jordi:2:2", "5:Empar (Valencian):3:2",
                "6:Gabriela (Brazilian):1:2", "6:Amalia (European):2:2", "6:Eusebio (European):3:2", "6:Fernanda (Brazilian):4:2", "6:Felipe (Brazilian):5:2",
                "7:Paola:1:2", "7:Silvana:2:2", "7:Valentina:3:2", "7:Luca:5:2", "7:Marcello:6:2", "7:Roberto:7:2", "7:Matteo:8:2", "7:Giulia:9:2", "7:Federica:10:2",
                "8:Afroditi:1:2", "8:Nikos:3:2", "9:Annika:1:2", "9:Sven:2:2", "10:Linlin (Mandarin):1:2", "10:Lisheng (Mandarin):2:2", "11:Willem:1:2",
                "11:Saskia:2:2", "14:Zosia:1:2", "14:Krzysztof:2:2", "15:Carmela:1:2", "16:Kerem:1:2", "16:Zeynep:2:2", "16:Selin:3:2", "19:Frida:1:2",
                "19:Magnus:2:2", "20:Vilde:1:2", "20:Henrik:2:2", "21:Olga:1:2", "21:Dmitri:2:2", "23:Milla:1:2", "23:Marko:2:2", "27:Tarik:1:2", "27:Laila:2:2",
                "30:Ioana:1:2", "31:Ludoviko:1:2", "1:Kate (US):1:3", "1:Paul (US):2:3", "1:Julie (US):3:3", "1:Bridget (UK):4:3", "1:Hugh (UK):5:3", "1:Ashley (US):6:3",
                "1:James (US):7:3", "2:Violeta:1:3", "2:Francisco (Mexican):2:3", "4:Chloe (Canadian):1:3", "10:Lily (Mandarin):1:3", "10:Hui (Mandarin):3:3",
                "10:Liang (Mandarin):4:3", "12:Show:2:3", "12:Misaki:3:3", "13:Yumi:1:3", "13:Junwoo:2:3"};

        if (arrayModel.size() <= 0) {
            for (int k = 0; k < strArr.length; k++) {
                String[] arrStr = strArr[k].split(":");

                String lang_id = arrStr[0];
                String char_name = arrStr[1];
                String voice_id = arrStr[2];
                String engine_id = arrStr[3];
                boolean bool = db.insertCharLanguage(lang_id, char_name, voice_id, engine_id);
                Log.e("allLangBool" + "-" + k, "" + bool);
            }
        }
    }

    public static  void getSessionId(final Context context)
    {

        String session_url  ="http://vhss.oddcast.com/vhss_editors/getSession.php";

        Constants.execute(new Super_AsyncTask(context, session_url, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                Log.d("sessionid", "" + output);

                if(!output.isEmpty() && output.contains("="))
                {
                    String[] sessionId = output.split("=");
                    SESSION_ID = sessionId[1];
                    Log.e("newData", "adkjas" + SESSION_ID);
                    Constants.storeSessionId(SESSION_ID, context);
                    trackrequest(context);
                }



            }

        }, false,false));

    }
/*    public static void getSessionId(final Context context) {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                con = new HttpConnections();
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                newData = con.getSessionID();
                return newData;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);

                String[] sessionId = aVoid.split("=");
                SESSION_ID = sessionId[1];
                Log.e("newData", "adkjas" + SESSION_ID);
                Constants.storeSessionId(SESSION_ID, context);
                trackrequest(context);

            }
        }.execute();

    }*/

    private static void trackrequest(Context context)//Jagdeep 13th July
    {

        String trackurl  ="http://data.oddcast.com/event.php/?apt=W&acc=1335&emb=0&uni=1&sm=0&et=0&ev[0][]=tss";

        Constants.execute(new Super_AsyncTask(context, trackurl, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                Log.d("trackresult", "" + output);


            }

        }, false,false));

    }
    public static void getSCID(final String xml) {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                con = new HttpConnections();
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                Log.d("xmldo", "" + xml);
                newData = con.getSCID(xml);
                return newData;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);

                Log.d("avoid", "" + aVoid);


            }
        }.execute();

    }

    public static void storeSessionId(String session, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SESSION_KEY, session);
        editor.commit();

    }

    public static String getSharedSessionId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.VOKI_KEY, Context.MODE_PRIVATE);
        String session_key = sharedPreferences.getString(Constants.SESSION_KEY, "");
        return session_key;

    }

    public static void getTtsAudio(final String engineId, final String langId, final String voiceId, final String text, final String extension,
                                   final String fxType, final String fxLevel, final String acc, final String sessionId, final String secretKey) {

        Log.e("getTTs", engineId + "--" + langId + "--" + voiceId + "--" + text + "--" + extension + "--" + fxType + "--" + fxLevel + "--" + acc + "--" + secretKey);

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                con = new HttpConnections();
            }

            @Override
            protected String doInBackground(Void... params) {
                String str = con.getTts(engineId, langId, voiceId, text, extension,
                        fxType, fxLevel, acc, sessionId, secretKey);
                return str;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                Log.e("fxSTr", "" + s);
            }
        }.execute();
    }

    /**
     * COnvert String to MD5 ....
     *
     * @param s
     * @return
     */
    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String convertPassMd5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }


}

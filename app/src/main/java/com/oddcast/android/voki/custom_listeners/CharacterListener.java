package com.oddcast.android.voki.custom_listeners;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 5/18/16.
 */
public interface CharacterListener {

    public void updateCharaterList(String category_select, boolean isSame, boolean fromDice);

    public void onShowChracters(ArrayList<HashMap<String, String>> str_Builder);

    public void updateAnim();

    public void showAccessorries(String file, boolean isSame);

    public void updateChangeAccessorries(String id, String filename);

    public void updateWallPapers(int pos, String name);

    public void deleteSavedCharcters(ArrayList<HashMap<String, String>> mapArrayList, int actualPos);
}

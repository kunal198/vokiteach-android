package com.oddcast.android.voki;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.app.Activity;
import com.naman14.androidlame.AndroidLame;
import com.naman14.androidlame.LameBuilder;
import com.oddcast.android.voki.custom_listeners.UploadAudioListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by brst-pc20 on 10/21/16.
 */

public class Mp3AudioRecordActivity {

    int minBuffer;
    int inSamplerate = 8000;

    String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/voki_audio.mp3";

    public static boolean isRecordingg = false;

    AudioRecord audioRecord;
    AndroidLame androidLame;
    FileOutputStream outputStream;
    UploadAudioListener mUploadAudioListener;
    Activity context;

    Mp3AudioRecordActivity(Activity context, boolean isRecordingg, UploadAudioListener mUploadAudioListener) {
        this.context=context;
        this.isRecordingg = isRecordingg;
        this.mUploadAudioListener = mUploadAudioListener;
    }


    public  void startRecording() {


        Log.d("test"," audio recorder status"+isRecordingg);
        minBuffer = AudioRecord.getMinBufferSize(inSamplerate, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        Log.d("test","Initialising audio recorder..");
     //   addLog("Initialising audio recorder..");
        audioRecord = new AudioRecord(
                MediaRecorder.AudioSource.MIC, inSamplerate,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, minBuffer * 2);

        //5 seconds data
        Log.d("test","creating short buffer array");
      //  addLog("creating short buffer array");
        short[] buffer = new short[inSamplerate * 2 * 5];

        // 'mp3buf' should be at least 7200 bytes long
        // to hold all possible emitted data.
     //   addLog("creating mp3 buffer");
        Log.d("test","creating mp3 buffer");
        byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];

        try {
            outputStream = new FileOutputStream(new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("test","Initialising Andorid Lame");
     //   addLog("Initialising Andorid Lame");
        androidLame = new LameBuilder()
                .setInSampleRate(inSamplerate)
                .setOutChannels(1)
                .setOutBitrate(32)
                .setOutSampleRate(inSamplerate)
                .build();
        Log.d("test","started audio recording");
      //  addLog("started audio recording");
       // updateStatus("Recording...");
        audioRecord.startRecording();

        int bytesRead = 0;

        while (isRecordingg) {
            Log.d("test","reading to short array buffer, buffer sze- " + minBuffer);
      //      addLog("reading to short array buffer, buffer sze- " + minBuffer);
            bytesRead = audioRecord.read(buffer, 0, minBuffer);
      //      addLog("bytes read=" + bytesRead);
            Log.d("test","bytes read=" + bytesRead);
            if (bytesRead > 0) {

                int bytesEncoded = androidLame.encode(buffer, buffer, bytesRead, mp3buffer);

                if (bytesEncoded > 0) {
                    try {
                        outputStream.write(mp3buffer, 0, bytesEncoded);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        int outputMp3buf = androidLame.flush(mp3buffer);

        if (outputMp3buf > 0) {
            try {
                Log.d("test","writing final mp3buffer to outputstream");
       //         addLog("writing final mp3buffer to outputstream");
                outputStream.write(mp3buffer, 0, outputMp3buf);
                Log.d("test","closing output stream");
       //         addLog("closing output stream");
                outputStream.close();
                Log.d("test","Output recording saved in " + filePath);
       //         updateStatus("Output recording saved in " + filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("test","releasing audio recorder");
       // addLog("releasing audio recorder");
        audioRecord.stop();
        audioRecord.release();
        Log.d("test","closing android lame");
       // addLog("closing android lame");
        androidLame.close();

        isRecordingg = false;

        if (mUploadAudioListener != null)
            mUploadAudioListener.uploadAudioInterface();

    }


}
